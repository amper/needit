
needit.files.ayuda.url = "https://neoportal.telecom.com.ar/irj/servlet/prt/portal/prtmode/preview/prtroot/pcd!3aportal_content!2fproyectos!2fIntranet!2fIviews!2fAplicaciones!2fNeedIT_Ayuda!2fIviews!2fDocumentacion"
needit.files.instructivo.url = "https://neoportal.telecom.com.ar/irj/go/km/docs/TECO_FSDB_Intranet/Acceso%20Privado/Todos%20somos%20TELECOM/NeedIT%20Ayuda/Documentos/03.%20Materiales%20de%20Consulta/Solicitud%20de%20perfiles%20por%20TUid.pdf"

environments{
	development{
		ume.service.url = "http://10.11.33.22:8080/UMEServices/export"
//		descomentarlo para salida a prod
//		grails.plugins.springsecurity.secureChannel.definition = [
//			'/login/**':         'REQUIRES_SECURE_CHANNEL',
//			'/inbox/**':         'REQUIRES_INSECURE_CHANNEL',
//			'/inboxMobile/**':         'REQUIRES_INSECURE_CHANNEL',
//			'/inboxAprobaciones/**':         'REQUIRES_INSECURE_CHANNEL',
//			'/registroPedido/**':  'REQUIRES_INSECURE_CHANNEL'
//		]
		grails.mail.host = "10.4.33.105"
		grails.mail.port = 25
		
	}
	test{
		grails.plugins.springsecurity.portMapper.httpsPort='8081'
		ume.service.url = "http://10.11.33.90:8080/UMEServices/export"
		grails.plugins.springsecurity.secureChannel.definition = [
			'/login/**':         'REQUIRES_SECURE_CHANNEL',
			'/inbox/**':         'REQUIRES_INSECURE_CHANNEL',
			'/inboxMobile/**':         'REQUIRES_INSECURE_CHANNEL',
			'/inboxAprobaciones/**':         'REQUIRES_INSECURE_CHANNEL',
			'/registroPedido/**':  'REQUIRES_INSECURE_CHANNEL'
		]
		grails.mail.host = "10.4.33.102"
		grails.mail.port = 25
	}
	production{
		grails.plugins.springsecurity.portMapper.httpsPort='8081'
		ume.service.url = "http://10.77.100.225:8080/UMEServices/export"
		grails.plugins.springsecurity.secureChannel.definition = [
			'/login/**':         'REQUIRES_SECURE_CHANNEL',
			'/inbox/**':         'REQUIRES_INSECURE_CHANNEL',
			'/inboxMobile/**':         'REQUIRES_INSECURE_CHANNEL',
			'/inboxAprobaciones/**':         'REQUIRES_INSECURE_CHANNEL',
			'/registroPedido/**':  'REQUIRES_INSECURE_CHANNEL'
		]
		grails.mail.host = "10.4.33.105"
		grails.mail.port = 25
	}
}