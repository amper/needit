package ar.com.telecom

import ar.com.telecom.pcs.entities.PedidoHijo
import ar.com.telecom.pcs.entities.TipoAnexo

class NormalizacionHijoController extends ActividadController{

	private def getMapaDatosPedido(pedido) {
		def usuarioLogueado = umeService.getUsuarioLogueado()
		def pedidoHijo
		//if (params.pedidoHijo?.isInteger()) {
		if (params.impacto?.isInteger()) {
			//pedidoHijo = PedidoHijo.findById(Integer.parseInt(params.pedidoHijo))
			pedidoHijo = pedidoService.obtenerPedidoHijo(params, params.impacto)
		}
		
		[usuarioLogueado:usuarioLogueado, pedidoHijo: pedidoHijo, faseATrabajar: FaseMultiton.NORMALIZACION_HIJO]
	}
	
	def editarActividadAnexo = {
		def pedidoHijo = PedidoHijo.findById(params.idPedidoHijo)
		def actividad = this.getActividad(params.idActividad)
		
		def tiposDeAnexo = TipoAnexo.findAllByFaseInList([
			FaseMultiton.getFase(FaseMultiton.NORMALIZACION_HIJO)
			])
		
		def usuarioLogueado = umeService.getUsuarioLogueado()
		
		render (template: "/templates/anexoPorTipoActividad", model:[pedidoHijo: pedidoHijo, actividad: actividad, tiposDeAnexo:tiposDeAnexo, usuarioLogueado:usuarioLogueado])
	}
	
}
