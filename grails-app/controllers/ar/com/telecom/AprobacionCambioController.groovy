package ar.com.telecom

class AprobacionCambioController extends WorkflowController{
	
	def webUserAgentService
	
    def index = { 
		if (webUserAgentService.isBlackberry()) {
			redirect(controller:"inboxMobile", action: "index")
		} else {
			def pedido = llenarDatosPedido(params, true)
			def result = [pedidoInstance: pedido]
			result.putAll(getMapaDatosPedido(pedido))
			result
		}
	}
	
	def getMapaDatosPedido(pedido) {
		def usuarioLogueado = umeService.getUsuarioLogueado()
		def aprobacionesList = pedido?.aprobaciones.findAll { aprobacion -> aprobacion.fase.equals(pedido.faseAprobacionCambio()) }
		def aprobacionesPendientes = pedido?.aprobacionesPendientes(FaseMultiton.getFase(FaseMultiton.APROBACION_CAMBIO), usuarioLogueado)
		def justificacionAprobacion = ""
		if (!aprobacionesPendientes.isEmpty()) {
			justificacionAprobacion = aprobacionesPendientes.last()?.justificacion
		}
		return [usuarioLogueado: usuarioLogueado, aprobacionesList: aprobacionesList, justificacionAprobacion: justificacionAprobacion]
	}
	
	def faseFinalizada(pedido) {
		def result = [pedidoInstance: pedido]
		result.putAll(getMapaDatosPedido(pedido))
		render(view: "index", model: result)
	}

}
