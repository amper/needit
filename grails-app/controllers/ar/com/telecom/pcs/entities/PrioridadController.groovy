package ar.com.telecom.pcs.entities

class PrioridadController {
	
	static adminMenu = true

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    def index = {
        redirect(action: "list", params: params)
    }

    def list = {
        params.max = Math.min(params.max ? params.int('max') : 10, 100)
        [prioridadInstanceList: Prioridad.list(params), prioridadInstanceTotal: Prioridad.count()]
    }

    def create = {
        def prioridadInstance = new Prioridad()
        prioridadInstance.properties = params
        return [prioridadInstance: prioridadInstance]
    }

    def save = {
        def prioridadInstance = new Prioridad(params)
        if (prioridadInstance.save(flush: true)) {
            flash.message = "${message(code: 'default.created.message', args: [message(code: 'prioridad.label', default: 'Prioridad'), prioridadInstance.id])}"
            redirect(action: "show", id: prioridadInstance.id)
        }
        else {
            render(view: "create", model: [prioridadInstance: prioridadInstance])
        }
    }

    def show = {
        def prioridadInstance = Prioridad.get(params.id)
        if (!prioridadInstance) {
            flash.message = "${message(code: 'default.not.found.message', args: [message(code: 'prioridad.label', default: 'Prioridad'), params.id])}"
            redirect(action: "list")
        }
        else {
            [prioridadInstance: prioridadInstance]
        }
    }

    def edit = {
        def prioridadInstance = Prioridad.get(params.id)
        if (!prioridadInstance) {
            flash.message = "${message(code: 'default.not.found.message', args: [message(code: 'prioridad.label', default: 'Prioridad'), params.id])}"
            redirect(action: "list")
        }
        else {
            return [prioridadInstance: prioridadInstance]
        }
    }

    def update = {
        def prioridadInstance = Prioridad.get(params.id)
        if (prioridadInstance) {
            if (params.version) {
                def version = params.version.toLong()
                if (prioridadInstance.version > version) {
                    
                    prioridadInstance.errors.rejectValue("version", "default.optimistic.locking.failure", [message(code: 'prioridad.label', default: 'Prioridad')] as Object[], "Another user has updated this Prioridad while you were editing")
                    render(view: "edit", model: [prioridadInstance: prioridadInstance])
                    return
                }
            }
            prioridadInstance.properties = params
            if (!prioridadInstance.hasErrors() && prioridadInstance.save(flush: true)) {
                flash.message = "${message(code: 'default.updated.message', args: [message(code: 'prioridad.label', default: 'Prioridad'), prioridadInstance.id])}"
                redirect(action: "show", id: prioridadInstance.id)
            }
            else {
                render(view: "edit", model: [prioridadInstance: prioridadInstance])
            }
        }
        else {
            flash.message = "${message(code: 'default.not.found.message', args: [message(code: 'prioridad.label', default: 'Prioridad'), params.id])}"
            redirect(action: "list")
        }
    }

    def delete = {
        def prioridadInstance = Prioridad.get(params.id)
        if (prioridadInstance) {
            try {
                prioridadInstance.delete(flush: true)
                flash.message = "${message(code: 'default.deleted.message', args: [message(code: 'prioridad.label', default: 'Prioridad'), params.id])}"
                redirect(action: "list")
            }
            catch (org.springframework.dao.DataIntegrityViolationException e) {
                flash.message = "${message(code: 'default.not.deleted.message', args: [message(code: 'prioridad.label', default: 'Prioridad'), params.id])}"
                redirect(action: "show", id: params.id)
            }
        }
        else {
            flash.message = "${message(code: 'default.not.found.message', args: [message(code: 'prioridad.label', default: 'Prioridad'), params.id])}"
            redirect(action: "list")
        }
    }
}
