package ar.com.telecom.pcs.entities

class UmbralAprobacionEconomicaController {
	
	static adminMenu = true

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    def index = {
        redirect(action: "list", params: params)
    }

    def list = {
        params.max = Math.min(params.max ? params.int('max') : 10, 100)
        [umbralAprobacionEconomicaInstanceList: UmbralAprobacionEconomica.list(params), umbralAprobacionEconomicaInstanceTotal: UmbralAprobacionEconomica.count()]
    }

    def create = {
        def umbralAprobacionEconomicaInstance = new UmbralAprobacionEconomica()
        umbralAprobacionEconomicaInstance.properties = params
        return [umbralAprobacionEconomicaInstance: umbralAprobacionEconomicaInstance]
    }

    def save = {
        def umbralAprobacionEconomicaInstance = new UmbralAprobacionEconomica(params)
        if (umbralAprobacionEconomicaInstance.save(flush: true)) {
            flash.message = "${message(code: 'default.created.message', args: [message(code: 'umbralAprobacionEconomica.label', default: 'UmbralAprobacionEconomica'), umbralAprobacionEconomicaInstance.id])}"
            redirect(action: "show", id: umbralAprobacionEconomicaInstance.id)
        }
        else {
            render(view: "create", model: [umbralAprobacionEconomicaInstance: umbralAprobacionEconomicaInstance])
        }
    }

    def show = {
        def umbralAprobacionEconomicaInstance = UmbralAprobacionEconomica.get(params.id)
        if (!umbralAprobacionEconomicaInstance) {
            flash.message = "${message(code: 'default.not.found.message', args: [message(code: 'umbralAprobacionEconomica.label', default: 'UmbralAprobacionEconomica'), params.id])}"
            redirect(action: "list")
        }
        else {
            [umbralAprobacionEconomicaInstance: umbralAprobacionEconomicaInstance]
        }
    }

    def edit = {
        def umbralAprobacionEconomicaInstance = UmbralAprobacionEconomica.get(params.id)
        if (!umbralAprobacionEconomicaInstance) {
            flash.message = "${message(code: 'default.not.found.message', args: [message(code: 'umbralAprobacionEconomica.label', default: 'UmbralAprobacionEconomica'), params.id])}"
            redirect(action: "list")
        }
        else {
            return [umbralAprobacionEconomicaInstance: umbralAprobacionEconomicaInstance]
        }
    }

    def update = {
        def umbralAprobacionEconomicaInstance = UmbralAprobacionEconomica.get(params.id)
        if (umbralAprobacionEconomicaInstance) {
            if (params.version) {
                def version = params.version.toLong()
                if (umbralAprobacionEconomicaInstance.version > version) {
                    
                    umbralAprobacionEconomicaInstance.errors.rejectValue("version", "default.optimistic.locking.failure", [message(code: 'umbralAprobacionEconomica.label', default: 'UmbralAprobacionEconomica')] as Object[], "Another user has updated this UmbralAprobacionEconomica while you were editing")
                    render(view: "edit", model: [umbralAprobacionEconomicaInstance: umbralAprobacionEconomicaInstance])
                    return
                }
            }
            umbralAprobacionEconomicaInstance.properties = params
            if (!umbralAprobacionEconomicaInstance.hasErrors() && umbralAprobacionEconomicaInstance.save(flush: true)) {
                flash.message = "${message(code: 'default.updated.message', args: [message(code: 'umbralAprobacionEconomica.label', default: 'UmbralAprobacionEconomica'), umbralAprobacionEconomicaInstance.id])}"
                redirect(action: "show", id: umbralAprobacionEconomicaInstance.id)
            }
            else {
                render(view: "edit", model: [umbralAprobacionEconomicaInstance: umbralAprobacionEconomicaInstance])
            }
        }
        else {
            flash.message = "${message(code: 'default.not.found.message', args: [message(code: 'umbralAprobacionEconomica.label', default: 'UmbralAprobacionEconomica'), params.id])}"
            redirect(action: "list")
        }
    }

    def delete = {
        def umbralAprobacionEconomicaInstance = UmbralAprobacionEconomica.get(params.id)
        if (umbralAprobacionEconomicaInstance) {
            try {
                umbralAprobacionEconomicaInstance.delete(flush: true)
                flash.message = "${message(code: 'default.deleted.message', args: [message(code: 'umbralAprobacionEconomica.label', default: 'UmbralAprobacionEconomica'), params.id])}"
                redirect(action: "list")
            }
            catch (org.springframework.dao.DataIntegrityViolationException e) {
                flash.message = "${message(code: 'default.not.deleted.message', args: [message(code: 'umbralAprobacionEconomica.label', default: 'UmbralAprobacionEconomica'), params.id])}"
                redirect(action: "show", id: params.id)
            }
        }
        else {
            flash.message = "${message(code: 'default.not.found.message', args: [message(code: 'umbralAprobacionEconomica.label', default: 'UmbralAprobacionEconomica'), params.id])}"
            redirect(action: "list")
        }
    }
}
