package ar.com.telecom.pcs.entities

class ConfiguracionEmailTipoEventoController {

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    def index = {
        redirect(action: "list", params: params)
    }

    def list = {
        params.max = Math.min(params.max ? params.int('max') : 10, 100)
        [configuracionEmailTipoEventoInstanceList: ConfiguracionEmailTipoEvento.list(params), configuracionEmailTipoEventoInstanceTotal: ConfiguracionEmailTipoEvento.count()]
    }

    def create = {
        def configuracionEmailTipoEventoInstance = new ConfiguracionEmailTipoEvento()
        configuracionEmailTipoEventoInstance.properties = params
        return [configuracionEmailTipoEventoInstance: configuracionEmailTipoEventoInstance]
    }

    def save = {
        def configuracionEmailTipoEventoInstance = new ConfiguracionEmailTipoEvento(params)
        if (configuracionEmailTipoEventoInstance.save(flush: true)) {
            flash.message = "${message(code: 'default.created.message', args: [message(code: 'configuracionEmailTipoEvento.label', default: 'ConfiguracionEmailTipoEvento'), configuracionEmailTipoEventoInstance.id])}"
            redirect(action: "show", id: configuracionEmailTipoEventoInstance.id)
        }
        else {
            render(view: "create", model: [configuracionEmailTipoEventoInstance: configuracionEmailTipoEventoInstance])
        }
    }

    def show = {
        def configuracionEmailTipoEventoInstance = ConfiguracionEmailTipoEvento.get(params.id)
        if (!configuracionEmailTipoEventoInstance) {
            flash.message = "${message(code: 'default.not.found.message', args: [message(code: 'configuracionEmailTipoEvento.label', default: 'ConfiguracionEmailTipoEvento'), params.id])}"
            redirect(action: "list")
        }
        else {
            [configuracionEmailTipoEventoInstance: configuracionEmailTipoEventoInstance]
        }
    }

    def edit = {
        def configuracionEmailTipoEventoInstance = ConfiguracionEmailTipoEvento.get(params.id)
        if (!configuracionEmailTipoEventoInstance) {
            flash.message = "${message(code: 'default.not.found.message', args: [message(code: 'configuracionEmailTipoEvento.label', default: 'ConfiguracionEmailTipoEvento'), params.id])}"
            redirect(action: "list")
        }
        else {
            return [configuracionEmailTipoEventoInstance: configuracionEmailTipoEventoInstance]
        }
    }

    def update = {
        def configuracionEmailTipoEventoInstance = ConfiguracionEmailTipoEvento.get(params.id)
        if (configuracionEmailTipoEventoInstance) {
            if (params.version) {
                def version = params.version.toLong()
                if (configuracionEmailTipoEventoInstance.version > version) {
                    
                    configuracionEmailTipoEventoInstance.errors.rejectValue("version", "default.optimistic.locking.failure", [message(code: 'configuracionEmailTipoEvento.label', default: 'ConfiguracionEmailTipoEvento')] as Object[], "Another user has updated this ConfiguracionEmailTipoEvento while you were editing")
                    render(view: "edit", model: [configuracionEmailTipoEventoInstance: configuracionEmailTipoEventoInstance])
                    return
                }
            }
            configuracionEmailTipoEventoInstance.properties = params
            if (!configuracionEmailTipoEventoInstance.hasErrors() && configuracionEmailTipoEventoInstance.save(flush: true)) {
                flash.message = "${message(code: 'default.updated.message', args: [message(code: 'configuracionEmailTipoEvento.label', default: 'ConfiguracionEmailTipoEvento'), configuracionEmailTipoEventoInstance.id])}"
                redirect(action: "show", id: configuracionEmailTipoEventoInstance.id)
            }
            else {
                render(view: "edit", model: [configuracionEmailTipoEventoInstance: configuracionEmailTipoEventoInstance])
            }
        }
        else {
            flash.message = "${message(code: 'default.not.found.message', args: [message(code: 'configuracionEmailTipoEvento.label', default: 'ConfiguracionEmailTipoEvento'), params.id])}"
            redirect(action: "list")
        }
    }

    def delete = {
        def configuracionEmailTipoEventoInstance = ConfiguracionEmailTipoEvento.get(params.id)
        if (configuracionEmailTipoEventoInstance) {
            try {
                configuracionEmailTipoEventoInstance.delete(flush: true)
                flash.message = "${message(code: 'default.deleted.message', args: [message(code: 'configuracionEmailTipoEvento.label', default: 'ConfiguracionEmailTipoEvento'), params.id])}"
                redirect(action: "list")
            }
            catch (org.springframework.dao.DataIntegrityViolationException e) {
                flash.message = "${message(code: 'default.not.deleted.message', args: [message(code: 'configuracionEmailTipoEvento.label', default: 'ConfiguracionEmailTipoEvento'), params.id])}"
                redirect(action: "show", id: params.id)
            }
        }
        else {
            flash.message = "${message(code: 'default.not.found.message', args: [message(code: 'configuracionEmailTipoEvento.label', default: 'ConfiguracionEmailTipoEvento'), params.id])}"
            redirect(action: "list")
        }
    }
}
