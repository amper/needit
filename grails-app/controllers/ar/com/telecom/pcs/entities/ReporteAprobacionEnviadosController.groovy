package ar.com.telecom.pcs.entities

class ReporteAprobacionEnviadosController {

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    def index = {
        redirect(action: "list", params: params)
    }

    def list = {
        params.max = Math.min(params.max ? params.int('max') : 10, 100)
        [reporteAprobacionEnviadosInstanceList: ReporteAprobacionEnviados.list(params), reporteAprobacionEnviadosInstanceTotal: ReporteAprobacionEnviados.count()]
    }

    def create = {
        def reporteAprobacionEnviadosInstance = new ReporteAprobacionEnviados()
        reporteAprobacionEnviadosInstance.properties = params
        return [reporteAprobacionEnviadosInstance: reporteAprobacionEnviadosInstance]
    }

    def save = {
        def reporteAprobacionEnviadosInstance = new ReporteAprobacionEnviados(params)
        if (reporteAprobacionEnviadosInstance.save(flush: true)) {
            flash.message = "${message(code: 'default.created.message', args: [message(code: 'reporteAprobacionEnviados.label', default: 'ReporteAprobacionEnviados'), reporteAprobacionEnviadosInstance.id])}"
            redirect(action: "show", id: reporteAprobacionEnviadosInstance.id)
        }
        else {
            render(view: "create", model: [reporteAprobacionEnviadosInstance: reporteAprobacionEnviadosInstance])
        }
    }

    def show = {
        def reporteAprobacionEnviadosInstance = ReporteAprobacionEnviados.get(params.id)
        if (!reporteAprobacionEnviadosInstance) {
            flash.message = "${message(code: 'default.not.found.message', args: [message(code: 'reporteAprobacionEnviados.label', default: 'ReporteAprobacionEnviados'), params.id])}"
            redirect(action: "list")
        }
        else {
            [reporteAprobacionEnviadosInstance: reporteAprobacionEnviadosInstance]
        }
    }

    def edit = {
        def reporteAprobacionEnviadosInstance = ReporteAprobacionEnviados.get(params.id)
        if (!reporteAprobacionEnviadosInstance) {
            flash.message = "${message(code: 'default.not.found.message', args: [message(code: 'reporteAprobacionEnviados.label', default: 'ReporteAprobacionEnviados'), params.id])}"
            redirect(action: "list")
        }
        else {
            return [reporteAprobacionEnviadosInstance: reporteAprobacionEnviadosInstance]
        }
    }

    def update = {
        def reporteAprobacionEnviadosInstance = ReporteAprobacionEnviados.get(params.id)
        if (reporteAprobacionEnviadosInstance) {
            if (params.version) {
                def version = params.version.toLong()
                if (reporteAprobacionEnviadosInstance.version > version) {
                    
                    reporteAprobacionEnviadosInstance.errors.rejectValue("version", "default.optimistic.locking.failure", [message(code: 'reporteAprobacionEnviados.label', default: 'ReporteAprobacionEnviados')] as Object[], "Another user has updated this ReporteAprobacionEnviados while you were editing")
                    render(view: "edit", model: [reporteAprobacionEnviadosInstance: reporteAprobacionEnviadosInstance])
                    return
                }
            }
            reporteAprobacionEnviadosInstance.properties = params
            if (!reporteAprobacionEnviadosInstance.hasErrors() && reporteAprobacionEnviadosInstance.save(flush: true)) {
                flash.message = "${message(code: 'default.updated.message', args: [message(code: 'reporteAprobacionEnviados.label', default: 'ReporteAprobacionEnviados'), reporteAprobacionEnviadosInstance.id])}"
                redirect(action: "show", id: reporteAprobacionEnviadosInstance.id)
            }
            else {
                render(view: "edit", model: [reporteAprobacionEnviadosInstance: reporteAprobacionEnviadosInstance])
            }
        }
        else {
            flash.message = "${message(code: 'default.not.found.message', args: [message(code: 'reporteAprobacionEnviados.label', default: 'ReporteAprobacionEnviados'), params.id])}"
            redirect(action: "list")
        }
    }

    def delete = {
        def reporteAprobacionEnviadosInstance = ReporteAprobacionEnviados.get(params.id)
        if (reporteAprobacionEnviadosInstance) {
            try {
                reporteAprobacionEnviadosInstance.delete(flush: true)
                flash.message = "${message(code: 'default.deleted.message', args: [message(code: 'reporteAprobacionEnviados.label', default: 'ReporteAprobacionEnviados'), params.id])}"
                redirect(action: "list")
            }
            catch (org.springframework.dao.DataIntegrityViolationException e) {
                flash.message = "${message(code: 'default.not.deleted.message', args: [message(code: 'reporteAprobacionEnviados.label', default: 'ReporteAprobacionEnviados'), params.id])}"
                redirect(action: "show", id: params.id)
            }
        }
        else {
            flash.message = "${message(code: 'default.not.found.message', args: [message(code: 'reporteAprobacionEnviados.label', default: 'ReporteAprobacionEnviados'), params.id])}"
            redirect(action: "list")
        }
    }
}
