package ar.com.telecom.pcs.entities

class CodigoCierreController {
	
	static adminMenu = true

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    def index = {
        redirect(action: "list", params: params)
    }

    def list = {
        params.max = Math.min(params.max ? params.int('max') : 10, 100)
        [codigoCierreInstanceList: CodigoCierre.list(params), codigoCierreInstanceTotal: CodigoCierre.count()]
    }

    def create = {
        def codigoCierreInstance = new CodigoCierre()
        codigoCierreInstance.properties = params
        return [codigoCierreInstance: codigoCierreInstance]
    }

    def save = {
        def codigoCierreInstance = new CodigoCierre(params)
        if (codigoCierreInstance.save(flush: true)) {
            flash.message = "${message(code: 'default.created.message', args: [message(code: 'codigoCierre.label', default: 'CodigoCierre'), codigoCierreInstance.id])}"
            redirect(action: "show", id: codigoCierreInstance.id)
        }
        else {
            render(view: "create", model: [codigoCierreInstance: codigoCierreInstance])
        }
    }

    def show = {
        def codigoCierreInstance = CodigoCierre.get(params.id)
        if (!codigoCierreInstance) {
            flash.message = "${message(code: 'default.not.found.message', args: [message(code: 'codigoCierre.label', default: 'CodigoCierre'), params.id])}"
            redirect(action: "list")
        }
        else {
            [codigoCierreInstance: codigoCierreInstance]
        }
    }

    def edit = {
        def codigoCierreInstance = CodigoCierre.get(params.id)
        if (!codigoCierreInstance) {
            flash.message = "${message(code: 'default.not.found.message', args: [message(code: 'codigoCierre.label', default: 'CodigoCierre'), params.id])}"
            redirect(action: "list")
        }
        else {
            return [codigoCierreInstance: codigoCierreInstance]
        }
    }

    def update = {
        def codigoCierreInstance = CodigoCierre.get(params.id)
        if (codigoCierreInstance) {
            if (params.version) {
                def version = params.version.toLong()
                if (codigoCierreInstance.version > version) {
                    
                    codigoCierreInstance.errors.rejectValue("version", "default.optimistic.locking.failure", [message(code: 'codigoCierre.label', default: 'CodigoCierre')] as Object[], "Another user has updated this CodigoCierre while you were editing")
                    render(view: "edit", model: [codigoCierreInstance: codigoCierreInstance])
                    return
                }
            }
            codigoCierreInstance.properties = params
            if (!codigoCierreInstance.hasErrors() && codigoCierreInstance.save(flush: true)) {
                flash.message = "${message(code: 'default.updated.message', args: [message(code: 'codigoCierre.label', default: 'CodigoCierre'), codigoCierreInstance.id])}"
                redirect(action: "show", id: codigoCierreInstance.id)
            }
            else {
                render(view: "edit", model: [codigoCierreInstance: codigoCierreInstance])
            }
        }
        else {
            flash.message = "${message(code: 'default.not.found.message', args: [message(code: 'codigoCierre.label', default: 'CodigoCierre'), params.id])}"
            redirect(action: "list")
        }
    }

    def delete = {
        def codigoCierreInstance = CodigoCierre.get(params.id)
        if (codigoCierreInstance) {
            try {
                codigoCierreInstance.delete(flush: true)
                flash.message = "${message(code: 'default.deleted.message', args: [message(code: 'codigoCierre.label', default: 'CodigoCierre'), params.id])}"
                redirect(action: "list")
            }
            catch (org.springframework.dao.DataIntegrityViolationException e) {
                flash.message = "${message(code: 'default.not.deleted.message', args: [message(code: 'codigoCierre.label', default: 'CodigoCierre'), params.id])}"
                redirect(action: "show", id: params.id)
            }
        }
        else {
            flash.message = "${message(code: 'default.not.found.message', args: [message(code: 'codigoCierre.label', default: 'CodigoCierre'), params.id])}"
            redirect(action: "list")
        }
    }
}
