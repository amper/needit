package ar.com.telecom.pcs.entities

class AnexoPorTipoController {

	static adminMenu = false

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    def index = {
        redirect(action: "list", params: params)
    }

    def list = {
        params.max = Math.min(params.max ? params.int('max') : 10, 100)
        [anexoPorTipoInstanceList: AnexoPorTipo.list(params), anexoPorTipoInstanceTotal: AnexoPorTipo.count()]
    }

    def create = {
        def anexoPorTipoInstance = new AnexoPorTipo()
        anexoPorTipoInstance.properties = params
        return [anexoPorTipoInstance: anexoPorTipoInstance]
    }

    def save = {
        def anexoPorTipoInstance = new AnexoPorTipo(params)
        if (anexoPorTipoInstance.save(flush: true)) {
            flash.message = "${message(code: 'default.created.message', args: [message(code: 'anexoPorTipo.label', default: 'AnexoPorTipo'), anexoPorTipoInstance.id])}"
            redirect(action: "show", id: anexoPorTipoInstance.id)
        }
        else {
            render(view: "create", model: [anexoPorTipoInstance: anexoPorTipoInstance])
        }
    }

    def show = {
        def anexoPorTipoInstance = AnexoPorTipo.get(params.id)
        if (!anexoPorTipoInstance) {
            flash.message = "${message(code: 'default.not.found.message', args: [message(code: 'anexoPorTipo.label', default: 'AnexoPorTipo'), params.id])}"
            redirect(action: "list")
        }
        else {
            [anexoPorTipoInstance: anexoPorTipoInstance]
        }
    }

    def edit = {
        def anexoPorTipoInstance = AnexoPorTipo.get(params.id)
        if (!anexoPorTipoInstance) {
            flash.message = "${message(code: 'default.not.found.message', args: [message(code: 'anexoPorTipo.label', default: 'AnexoPorTipo'), params.id])}"
            redirect(action: "list")
        }
        else {
            return [anexoPorTipoInstance: anexoPorTipoInstance]
        }
    }

    def update = {
        def anexoPorTipoInstance = AnexoPorTipo.get(params.id)
        if (anexoPorTipoInstance) {
            if (params.version) {
                def version = params.version.toLong()
                if (anexoPorTipoInstance.version > version) {
                    
                    anexoPorTipoInstance.errors.rejectValue("version", "default.optimistic.locking.failure", [message(code: 'anexoPorTipo.label', default: 'AnexoPorTipo')] as Object[], "Another user has updated this AnexoPorTipo while you were editing")
                    render(view: "edit", model: [anexoPorTipoInstance: anexoPorTipoInstance])
                    return
                }
            }
            anexoPorTipoInstance.properties = params
            if (!anexoPorTipoInstance.hasErrors() && anexoPorTipoInstance.save(flush: true)) {
                flash.message = "${message(code: 'default.updated.message', args: [message(code: 'anexoPorTipo.label', default: 'AnexoPorTipo'), anexoPorTipoInstance.id])}"
                redirect(action: "show", id: anexoPorTipoInstance.id)
            }
            else {
                render(view: "edit", model: [anexoPorTipoInstance: anexoPorTipoInstance])
            }
        }
        else {
            flash.message = "${message(code: 'default.not.found.message', args: [message(code: 'anexoPorTipo.label', default: 'AnexoPorTipo'), params.id])}"
            redirect(action: "list")
        }
    }

    def delete = {
        def anexoPorTipoInstance = AnexoPorTipo.get(params.id)
        if (anexoPorTipoInstance) {
            try {
                anexoPorTipoInstance.delete(flush: true)
                flash.message = "${message(code: 'default.deleted.message', args: [message(code: 'anexoPorTipo.label', default: 'AnexoPorTipo'), params.id])}"
                redirect(action: "list")
            }
            catch (org.springframework.dao.DataIntegrityViolationException e) {
                flash.message = "${message(code: 'default.not.deleted.message', args: [message(code: 'anexoPorTipo.label', default: 'AnexoPorTipo'), params.id])}"
                redirect(action: "show", id: params.id)
            }
        }
        else {
            flash.message = "${message(code: 'default.not.found.message', args: [message(code: 'anexoPorTipo.label', default: 'AnexoPorTipo'), params.id])}"
            redirect(action: "list")
        }
    }
}
