package ar.com.telecom.pcs.entities

class ControlHorasPedidoController {
	
	static adminMenu = false

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    def index = {
        redirect(action: "list", params: params)
    }

    def list = {
        params.max = Math.min(params.max ? params.int('max') : 10, 100)
        [controlHorasPedidoInstanceList: ControlHorasPedido.list(params), controlHorasPedidoInstanceTotal: ControlHorasPedido.count()]
    }

    def create = {
        def controlHorasPedidoInstance = new ControlHorasPedido()
        controlHorasPedidoInstance.properties = params
        return [controlHorasPedidoInstance: controlHorasPedidoInstance]
    }

    def save = {
        def controlHorasPedidoInstance = new ControlHorasPedido(params)
        if (controlHorasPedidoInstance.save(flush: true)) {
            flash.message = "${message(code: 'default.created.message', args: [message(code: 'controlHorasPedido.label', default: 'ControlHorasPedido'), controlHorasPedidoInstance.id])}"
            redirect(action: "show", id: controlHorasPedidoInstance.id)
        }
        else {
            render(view: "create", model: [controlHorasPedidoInstance: controlHorasPedidoInstance])
        }
    }

    def show = {
        def controlHorasPedidoInstance = ControlHorasPedido.get(params.id)
        if (!controlHorasPedidoInstance) {
            flash.message = "${message(code: 'default.not.found.message', args: [message(code: 'controlHorasPedido.label', default: 'ControlHorasPedido'), params.id])}"
            redirect(action: "list")
        }
        else {
            [controlHorasPedidoInstance: controlHorasPedidoInstance]
        }
    }

    def edit = {
        def controlHorasPedidoInstance = ControlHorasPedido.get(params.id)
        if (!controlHorasPedidoInstance) {
            flash.message = "${message(code: 'default.not.found.message', args: [message(code: 'controlHorasPedido.label', default: 'ControlHorasPedido'), params.id])}"
            redirect(action: "list")
        }
        else {
            return [controlHorasPedidoInstance: controlHorasPedidoInstance]
        }
    }

    def update = {
        def controlHorasPedidoInstance = ControlHorasPedido.get(params.id)
        if (controlHorasPedidoInstance) {
            if (params.version) {
                def version = params.version.toLong()
                if (controlHorasPedidoInstance.version > version) {
                    
                    controlHorasPedidoInstance.errors.rejectValue("version", "default.optimistic.locking.failure", [message(code: 'controlHorasPedido.label', default: 'ControlHorasPedido')] as Object[], "Another user has updated this ControlHorasPedido while you were editing")
                    render(view: "edit", model: [controlHorasPedidoInstance: controlHorasPedidoInstance])
                    return
                }
            }
            controlHorasPedidoInstance.properties = params
            if (!controlHorasPedidoInstance.hasErrors() && controlHorasPedidoInstance.save(flush: true)) {
                flash.message = "${message(code: 'default.updated.message', args: [message(code: 'controlHorasPedido.label', default: 'ControlHorasPedido'), controlHorasPedidoInstance.id])}"
                redirect(action: "show", id: controlHorasPedidoInstance.id)
            }
            else {
                render(view: "edit", model: [controlHorasPedidoInstance: controlHorasPedidoInstance])
            }
        }
        else {
            flash.message = "${message(code: 'default.not.found.message', args: [message(code: 'controlHorasPedido.label', default: 'ControlHorasPedido'), params.id])}"
            redirect(action: "list")
        }
    }

    def delete = {
        def controlHorasPedidoInstance = ControlHorasPedido.get(params.id)
        if (controlHorasPedidoInstance) {
            try {
                controlHorasPedidoInstance.delete(flush: true)
                flash.message = "${message(code: 'default.deleted.message', args: [message(code: 'controlHorasPedido.label', default: 'ControlHorasPedido'), params.id])}"
                redirect(action: "list")
            }
            catch (org.springframework.dao.DataIntegrityViolationException e) {
                flash.message = "${message(code: 'default.not.deleted.message', args: [message(code: 'controlHorasPedido.label', default: 'ControlHorasPedido'), params.id])}"
                redirect(action: "show", id: params.id)
            }
        }
        else {
            flash.message = "${message(code: 'default.not.found.message', args: [message(code: 'controlHorasPedido.label', default: 'ControlHorasPedido'), params.id])}"
            redirect(action: "list")
        }
    }
}
