package ar.com.telecom.pcs.entities

class ECSOtrasPruebasController {
	
	static adminMenu = false

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    def index = {
        redirect(action: "list", params: params)
    }

    def list = {
        params.max = Math.min(params.max ? params.int('max') : 10, 100)
        [ECSOtrasPruebasInstanceList: ECSOtrasPruebas.list(params), ECSOtrasPruebasInstanceTotal: ECSOtrasPruebas.count()]
    }

    def create = {
        def ECSOtrasPruebasInstance = new ECSOtrasPruebas()
        ECSOtrasPruebasInstance.properties = params
        return [ECSOtrasPruebasInstance: ECSOtrasPruebasInstance]
    }

    def save = {
        def ECSOtrasPruebasInstance = new ECSOtrasPruebas(params)
        if (ECSOtrasPruebasInstance.save(flush: true)) {
            flash.message = "${message(code: 'default.created.message', args: [message(code: 'ECSOtrasPruebas.label', default: 'ECSOtrasPruebas'), ECSOtrasPruebasInstance.id])}"
            redirect(action: "show", id: ECSOtrasPruebasInstance.id)
        }
        else {
            render(view: "create", model: [ECSOtrasPruebasInstance: ECSOtrasPruebasInstance])
        }
    }

    def show = {
        def ECSOtrasPruebasInstance = ECSOtrasPruebas.get(params.id)
        if (!ECSOtrasPruebasInstance) {
            flash.message = "${message(code: 'default.not.found.message', args: [message(code: 'ECSOtrasPruebas.label', default: 'ECSOtrasPruebas'), params.id])}"
            redirect(action: "list")
        }
        else {
            [ECSOtrasPruebasInstance: ECSOtrasPruebasInstance]
        }
    }

    def edit = {
        def ECSOtrasPruebasInstance = ECSOtrasPruebas.get(params.id)
        if (!ECSOtrasPruebasInstance) {
            flash.message = "${message(code: 'default.not.found.message', args: [message(code: 'ECSOtrasPruebas.label', default: 'ECSOtrasPruebas'), params.id])}"
            redirect(action: "list")
        }
        else {
            return [ECSOtrasPruebasInstance: ECSOtrasPruebasInstance]
        }
    }

    def update = {
        def ECSOtrasPruebasInstance = ECSOtrasPruebas.get(params.id)
        if (ECSOtrasPruebasInstance) {
            if (params.version) {
                def version = params.version.toLong()
                if (ECSOtrasPruebasInstance.version > version) {
                    
                    ECSOtrasPruebasInstance.errors.rejectValue("version", "default.optimistic.locking.failure", [message(code: 'ECSOtrasPruebas.label', default: 'ECSOtrasPruebas')] as Object[], "Another user has updated this ECSOtrasPruebas while you were editing")
                    render(view: "edit", model: [ECSOtrasPruebasInstance: ECSOtrasPruebasInstance])
                    return
                }
            }
            ECSOtrasPruebasInstance.properties = params
            if (!ECSOtrasPruebasInstance.hasErrors() && ECSOtrasPruebasInstance.save(flush: true)) {
                flash.message = "${message(code: 'default.updated.message', args: [message(code: 'ECSOtrasPruebas.label', default: 'ECSOtrasPruebas'), ECSOtrasPruebasInstance.id])}"
                redirect(action: "show", id: ECSOtrasPruebasInstance.id)
            }
            else {
                render(view: "edit", model: [ECSOtrasPruebasInstance: ECSOtrasPruebasInstance])
            }
        }
        else {
            flash.message = "${message(code: 'default.not.found.message', args: [message(code: 'ECSOtrasPruebas.label', default: 'ECSOtrasPruebas'), params.id])}"
            redirect(action: "list")
        }
    }

    def delete = {
        def ECSOtrasPruebasInstance = ECSOtrasPruebas.get(params.id)
        if (ECSOtrasPruebasInstance) {
            try {
                ECSOtrasPruebasInstance.delete(flush: true)
                flash.message = "${message(code: 'default.deleted.message', args: [message(code: 'ECSOtrasPruebas.label', default: 'ECSOtrasPruebas'), params.id])}"
                redirect(action: "list")
            }
            catch (org.springframework.dao.DataIntegrityViolationException e) {
                flash.message = "${message(code: 'default.not.deleted.message', args: [message(code: 'ECSOtrasPruebas.label', default: 'ECSOtrasPruebas'), params.id])}"
                redirect(action: "show", id: params.id)
            }
        }
        else {
            flash.message = "${message(code: 'default.not.found.message', args: [message(code: 'ECSOtrasPruebas.label', default: 'ECSOtrasPruebas'), params.id])}"
            redirect(action: "list")
        }
    }
}
