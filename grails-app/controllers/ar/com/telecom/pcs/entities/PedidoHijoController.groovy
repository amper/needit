package ar.com.telecom.pcs.entities

class PedidoHijoController {
	
	static adminMenu = false

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    def index = {
        redirect(action: "list", params: params)
    }

    def list = {
        params.max = Math.min(params.max ? params.int('max') : 10, 100)
        [pedidoHijoInstanceList: PedidoHijo.list(params), pedidoHijoInstanceTotal: PedidoHijo.count()]
    }

    def create = {
        def pedidoHijoInstance = new PedidoHijo()
        pedidoHijoInstance.properties = params
        return [pedidoHijoInstance: pedidoHijoInstance]
    }

    def save = {
        def pedidoHijoInstance = new PedidoHijo(params)
        if (pedidoHijoInstance.save(flush: true)) {
            flash.message = "${message(code: 'default.created.message', args: [message(code: 'pedidoHijo.label', default: 'PedidoHijo'), pedidoHijoInstance.id])}"
            redirect(action: "show", id: pedidoHijoInstance.id)
        }
        else {
            render(view: "create", model: [pedidoHijoInstance: pedidoHijoInstance])
        }
    }

    def show = {
        def pedidoHijoInstance = PedidoHijo.get(params.id)
        if (!pedidoHijoInstance) {
            flash.message = "${message(code: 'default.not.found.message', args: [message(code: 'pedidoHijo.label', default: 'PedidoHijo'), params.id])}"
            redirect(action: "list")
        }
        else {
            [pedidoHijoInstance: pedidoHijoInstance]
        }
    }

    def edit = {
        def pedidoHijoInstance = PedidoHijo.get(params.id)
        if (!pedidoHijoInstance) {
            flash.message = "${message(code: 'default.not.found.message', args: [message(code: 'pedidoHijo.label', default: 'PedidoHijo'), params.id])}"
            redirect(action: "list")
        }
        else {
            return [pedidoHijoInstance: pedidoHijoInstance]
        }
    }

    def update = {
        def pedidoHijoInstance = PedidoHijo.get(params.id)
        if (pedidoHijoInstance) {
            if (params.version) {
                def version = params.version.toLong()
                if (pedidoHijoInstance.version > version) {
                    
                    pedidoHijoInstance.errors.rejectValue("version", "default.optimistic.locking.failure", [message(code: 'pedidoHijo.label', default: 'PedidoHijo')] as Object[], "Another user has updated this PedidoHijo while you were editing")
                    render(view: "edit", model: [pedidoHijoInstance: pedidoHijoInstance])
                    return
                }
            }
            pedidoHijoInstance.properties = params
            if (!pedidoHijoInstance.hasErrors() && pedidoHijoInstance.save(flush: true)) {
                flash.message = "${message(code: 'default.updated.message', args: [message(code: 'pedidoHijo.label', default: 'PedidoHijo'), pedidoHijoInstance.id])}"
                redirect(action: "show", id: pedidoHijoInstance.id)
            }
            else {
                render(view: "edit", model: [pedidoHijoInstance: pedidoHijoInstance])
            }
        }
        else {
            flash.message = "${message(code: 'default.not.found.message', args: [message(code: 'pedidoHijo.label', default: 'PedidoHijo'), params.id])}"
            redirect(action: "list")
        }
    }

    def delete = {
        def pedidoHijoInstance = PedidoHijo.get(params.id)
        if (pedidoHijoInstance) {
            try {
                pedidoHijoInstance.delete(flush: true)
                flash.message = "${message(code: 'default.deleted.message', args: [message(code: 'pedidoHijo.label', default: 'PedidoHijo'), params.id])}"
                redirect(action: "list")
            }
            catch (org.springframework.dao.DataIntegrityViolationException e) {
                flash.message = "${message(code: 'default.not.deleted.message', args: [message(code: 'pedidoHijo.label', default: 'PedidoHijo'), params.id])}"
                redirect(action: "show", id: params.id)
            }
        }
        else {
            flash.message = "${message(code: 'default.not.found.message', args: [message(code: 'pedidoHijo.label', default: 'PedidoHijo'), params.id])}"
            redirect(action: "list")
        }
    }
}
