package ar.com.telecom.pcs.entities

class DetallePlanificacionSistemaController {
	
	static adminMenu = false

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    def index = {
        redirect(action: "list", params: params)
    }

    def list = {
        params.max = Math.min(params.max ? params.int('max') : 10, 100)
        [detallePlanificacionSistemaInstanceList: DetallePlanificacionSistema.list(params), detallePlanificacionSistemaInstanceTotal: DetallePlanificacionSistema.count()]
    }

    def create = {
        def detallePlanificacionSistemaInstance = new DetallePlanificacionSistema()
        detallePlanificacionSistemaInstance.properties = params
        return [detallePlanificacionSistemaInstance: detallePlanificacionSistemaInstance]
    }

    def save = {
        def detallePlanificacionSistemaInstance = new DetallePlanificacionSistema(params)
        if (detallePlanificacionSistemaInstance.save(flush: true)) {
            flash.message = "${message(code: 'default.created.message', args: [message(code: 'detallePlanificacionSistema.label', default: 'DetallePlanificacionSistema'), detallePlanificacionSistemaInstance.id])}"
            redirect(action: "show", id: detallePlanificacionSistemaInstance.id)
        }
        else {
            render(view: "create", model: [detallePlanificacionSistemaInstance: detallePlanificacionSistemaInstance])
        }
    }

    def show = {
        def detallePlanificacionSistemaInstance = DetallePlanificacionSistema.get(params.id)
        if (!detallePlanificacionSistemaInstance) {
            flash.message = "${message(code: 'default.not.found.message', args: [message(code: 'detallePlanificacionSistema.label', default: 'DetallePlanificacionSistema'), params.id])}"
            redirect(action: "list")
        }
        else {
            [detallePlanificacionSistemaInstance: detallePlanificacionSistemaInstance]
        }
    }

    def edit = {
        def detallePlanificacionSistemaInstance = DetallePlanificacionSistema.get(params.id)
        if (!detallePlanificacionSistemaInstance) {
            flash.message = "${message(code: 'default.not.found.message', args: [message(code: 'detallePlanificacionSistema.label', default: 'DetallePlanificacionSistema'), params.id])}"
            redirect(action: "list")
        }
        else {
            return [detallePlanificacionSistemaInstance: detallePlanificacionSistemaInstance]
        }
    }

    def update = {
        def detallePlanificacionSistemaInstance = DetallePlanificacionSistema.get(params.id)
        if (detallePlanificacionSistemaInstance) {
            if (params.version) {
                def version = params.version.toLong()
                if (detallePlanificacionSistemaInstance.version > version) {
                    
                    detallePlanificacionSistemaInstance.errors.rejectValue("version", "default.optimistic.locking.failure", [message(code: 'detallePlanificacionSistema.label', default: 'DetallePlanificacionSistema')] as Object[], "Another user has updated this DetallePlanificacionSistema while you were editing")
                    render(view: "edit", model: [detallePlanificacionSistemaInstance: detallePlanificacionSistemaInstance])
                    return
                }
            }
            detallePlanificacionSistemaInstance.properties = params
            if (!detallePlanificacionSistemaInstance.hasErrors() && detallePlanificacionSistemaInstance.save(flush: true)) {
                flash.message = "${message(code: 'default.updated.message', args: [message(code: 'detallePlanificacionSistema.label', default: 'DetallePlanificacionSistema'), detallePlanificacionSistemaInstance.id])}"
                redirect(action: "show", id: detallePlanificacionSistemaInstance.id)
            }
            else {
                render(view: "edit", model: [detallePlanificacionSistemaInstance: detallePlanificacionSistemaInstance])
            }
        }
        else {
            flash.message = "${message(code: 'default.not.found.message', args: [message(code: 'detallePlanificacionSistema.label', default: 'DetallePlanificacionSistema'), params.id])}"
            redirect(action: "list")
        }
    }

    def delete = {
        def detallePlanificacionSistemaInstance = DetallePlanificacionSistema.get(params.id)
        if (detallePlanificacionSistemaInstance) {
            try {
                detallePlanificacionSistemaInstance.delete(flush: true)
                flash.message = "${message(code: 'default.deleted.message', args: [message(code: 'detallePlanificacionSistema.label', default: 'DetallePlanificacionSistema'), params.id])}"
                redirect(action: "list")
            }
            catch (org.springframework.dao.DataIntegrityViolationException e) {
                flash.message = "${message(code: 'default.not.deleted.message', args: [message(code: 'detallePlanificacionSistema.label', default: 'DetallePlanificacionSistema'), params.id])}"
                redirect(action: "show", id: params.id)
            }
        }
        else {
            flash.message = "${message(code: 'default.not.found.message', args: [message(code: 'detallePlanificacionSistema.label', default: 'DetallePlanificacionSistema'), params.id])}"
            redirect(action: "list")
        }
    }
}
