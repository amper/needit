package ar.com.telecom.pcs.entities

class RolAplicacionController {
	
	static adminMenu = true

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    def index = {
        redirect(action: "list", params: params)
    }

    def list = {
        params.max = Math.min(params.max ? params.int('max') : 10, 100)
        [rolAplicacionInstanceList: RolAplicacion.list(params), rolAplicacionInstanceTotal: RolAplicacion.count()]
    }

    def create = {
        def rolAplicacionInstance = new RolAplicacion()
        rolAplicacionInstance.properties = params
        return [rolAplicacionInstance: rolAplicacionInstance]
    }

    def save = {
        def rolAplicacionInstance = new RolAplicacion(params)
        if (rolAplicacionInstance.save(flush: true)) {
            flash.message = "${message(code: 'default.created.message', args: [message(code: 'rolAplicacion.label', default: 'RolAplicacion'), rolAplicacionInstance.id])}"
            redirect(action: "show", id: rolAplicacionInstance.id)
        }
        else {
            render(view: "create", model: [rolAplicacionInstance: rolAplicacionInstance])
        }
    }

    def show = {
        def rolAplicacionInstance = RolAplicacion.get(params.id)
        if (!rolAplicacionInstance) {
            flash.message = "${message(code: 'default.not.found.message', args: [message(code: 'rolAplicacion.label', default: 'RolAplicacion'), params.id])}"
            redirect(action: "list")
        }
        else {
            [rolAplicacionInstance: rolAplicacionInstance]
        }
    }

    def edit = {
        def rolAplicacionInstance = RolAplicacion.get(params.id)
        if (!rolAplicacionInstance) {
            flash.message = "${message(code: 'default.not.found.message', args: [message(code: 'rolAplicacion.label', default: 'RolAplicacion'), params.id])}"
            redirect(action: "list")
        }
        else {
            return [rolAplicacionInstance: rolAplicacionInstance]
        }
    }

    def update = {
        def rolAplicacionInstance = RolAplicacion.get(params.id)
        if (rolAplicacionInstance) {
            if (params.version) {
                def version = params.version.toLong()
                if (rolAplicacionInstance.version > version) {
                    
                    rolAplicacionInstance.errors.rejectValue("version", "default.optimistic.locking.failure", [message(code: 'rolAplicacion.label', default: 'RolAplicacion')] as Object[], "Another user has updated this RolAplicacion while you were editing")
                    render(view: "edit", model: [rolAplicacionInstance: rolAplicacionInstance])
                    return
                }
            }
            rolAplicacionInstance.properties = params
            if (!rolAplicacionInstance.hasErrors() && rolAplicacionInstance.save(flush: true)) {
                flash.message = "${message(code: 'default.updated.message', args: [message(code: 'rolAplicacion.label', default: 'RolAplicacion'), rolAplicacionInstance.id])}"
                redirect(action: "show", id: rolAplicacionInstance.id)
            }
            else {
                render(view: "edit", model: [rolAplicacionInstance: rolAplicacionInstance])
            }
        }
        else {
            flash.message = "${message(code: 'default.not.found.message', args: [message(code: 'rolAplicacion.label', default: 'RolAplicacion'), params.id])}"
            redirect(action: "list")
        }
    }

    def delete = {
        def rolAplicacionInstance = RolAplicacion.get(params.id)
        if (rolAplicacionInstance) {
            try {
                rolAplicacionInstance.delete(flush: true)
                flash.message = "${message(code: 'default.deleted.message', args: [message(code: 'rolAplicacion.label', default: 'RolAplicacion'), params.id])}"
                redirect(action: "list")
            }
            catch (org.springframework.dao.DataIntegrityViolationException e) {
                flash.message = "${message(code: 'default.not.deleted.message', args: [message(code: 'rolAplicacion.label', default: 'RolAplicacion'), params.id])}"
                redirect(action: "show", id: params.id)
            }
        }
        else {
            flash.message = "${message(code: 'default.not.found.message', args: [message(code: 'rolAplicacion.label', default: 'RolAplicacion'), params.id])}"
            redirect(action: "list")
        }
    }
}
