package ar.com.telecom.pcs.entities

class TipoReferenciaController {
	
	static adminMenu = true

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    def index = {
        redirect(action: "list", params: params)
    }

    def list = {
        params.max = Math.min(params.max ? params.int('max') : 10, 100)
        [tipoReferenciaInstanceList: TipoReferencia.list(params), tipoReferenciaInstanceTotal: TipoReferencia.count()]
    }

    def create = {
        def tipoReferenciaInstance = new TipoReferencia()
        tipoReferenciaInstance.properties = params
        return [tipoReferenciaInstance: tipoReferenciaInstance]
    }

    def save = {
        def tipoReferenciaInstance = new TipoReferencia(params)
        if (tipoReferenciaInstance.save(flush: true)) {
            flash.message = "${message(code: 'default.created.message', args: [message(code: 'tipoReferencia.label', default: 'TipoReferencia'), tipoReferenciaInstance.id])}"
            redirect(action: "show", id: tipoReferenciaInstance.id)
        }
        else {
            render(view: "create", model: [tipoReferenciaInstance: tipoReferenciaInstance])
        }
    }

    def show = {
        def tipoReferenciaInstance = TipoReferencia.get(params.id)
        if (!tipoReferenciaInstance) {
            flash.message = "${message(code: 'default.not.found.message', args: [message(code: 'tipoReferencia.label', default: 'TipoReferencia'), params.id])}"
            redirect(action: "list")
        }
        else {
            [tipoReferenciaInstance: tipoReferenciaInstance]
        }
    }

    def edit = {
        def tipoReferenciaInstance = TipoReferencia.get(params.id)
        if (!tipoReferenciaInstance) {
            flash.message = "${message(code: 'default.not.found.message', args: [message(code: 'tipoReferencia.label', default: 'TipoReferencia'), params.id])}"
            redirect(action: "list")
        }
        else {
            return [tipoReferenciaInstance: tipoReferenciaInstance]
        }
    }

    def update = {
        def tipoReferenciaInstance = TipoReferencia.get(params.id)
        if (tipoReferenciaInstance) {
            if (params.version) {
                def version = params.version.toLong()
                if (tipoReferenciaInstance.version > version) {
                    
                    tipoReferenciaInstance.errors.rejectValue("version", "default.optimistic.locking.failure", [message(code: 'tipoReferencia.label', default: 'TipoReferencia')] as Object[], "Another user has updated this TipoReferencia while you were editing")
                    render(view: "edit", model: [tipoReferenciaInstance: tipoReferenciaInstance])
                    return
                }
            }
            tipoReferenciaInstance.properties = params
            if (!tipoReferenciaInstance.hasErrors() && tipoReferenciaInstance.save(flush: true)) {
                flash.message = "${message(code: 'default.updated.message', args: [message(code: 'tipoReferencia.label', default: 'TipoReferencia'), tipoReferenciaInstance.id])}"
                redirect(action: "show", id: tipoReferenciaInstance.id)
            }
            else {
                render(view: "edit", model: [tipoReferenciaInstance: tipoReferenciaInstance])
            }
        }
        else {
            flash.message = "${message(code: 'default.not.found.message', args: [message(code: 'tipoReferencia.label', default: 'TipoReferencia'), params.id])}"
            redirect(action: "list")
        }
    }

    def delete = {
        def tipoReferenciaInstance = TipoReferencia.get(params.id)
        if (tipoReferenciaInstance) {
            try {
                tipoReferenciaInstance.delete(flush: true)
                flash.message = "${message(code: 'default.deleted.message', args: [message(code: 'tipoReferencia.label', default: 'TipoReferencia'), params.id])}"
                redirect(action: "list")
            }
            catch (org.springframework.dao.DataIntegrityViolationException e) {
                flash.message = "${message(code: 'default.not.deleted.message', args: [message(code: 'tipoReferencia.label', default: 'TipoReferencia'), params.id])}"
                redirect(action: "show", id: params.id)
            }
        }
        else {
            flash.message = "${message(code: 'default.not.found.message', args: [message(code: 'tipoReferencia.label', default: 'TipoReferencia'), params.id])}"
            redirect(action: "list")
        }
    }
}
