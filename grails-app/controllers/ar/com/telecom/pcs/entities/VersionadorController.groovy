package ar.com.telecom.pcs.entities

class VersionadorController {
	
	static adminMenu = true

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    def index = {
        redirect(action: "list", params: params)
    }

    def list = {
        params.max = Math.min(params.max ? params.int('max') : 10, 100)
        [versionadorInstanceList: Versionador.list(params), versionadorInstanceTotal: Versionador.count()]
    }

    def create = {
        def versionadorInstance = new Versionador()
        versionadorInstance.properties = params
        return [versionadorInstance: versionadorInstance]
    }

    def save = {
        def versionadorInstance = new Versionador(params)
        if (versionadorInstance.save(flush: true)) {
            flash.message = "${message(code: 'default.created.message', args: [message(code: 'versionador.label', default: 'Versionador'), versionadorInstance.id])}"
            redirect(action: "show", id: versionadorInstance.id)
        }
        else {
            render(view: "create", model: [versionadorInstance: versionadorInstance])
        }
    }

    def show = {
        def versionadorInstance = Versionador.get(params.id)
        if (!versionadorInstance) {
            flash.message = "${message(code: 'default.not.found.message', args: [message(code: 'versionador.label', default: 'Versionador'), params.id])}"
            redirect(action: "list")
        }
        else {
            [versionadorInstance: versionadorInstance]
        }
    }

    def edit = {
        def versionadorInstance = Versionador.get(params.id)
        if (!versionadorInstance) {
            flash.message = "${message(code: 'default.not.found.message', args: [message(code: 'versionador.label', default: 'Versionador'), params.id])}"
            redirect(action: "list")
        }
        else {
            return [versionadorInstance: versionadorInstance]
        }
    }

    def update = {
        def versionadorInstance = Versionador.get(params.id)
        if (versionadorInstance) {
            if (params.version) {
                def version = params.version.toLong()
                if (versionadorInstance.version > version) {
                    
                    versionadorInstance.errors.rejectValue("version", "default.optimistic.locking.failure", [message(code: 'versionador.label', default: 'Versionador')] as Object[], "Another user has updated this Versionador while you were editing")
                    render(view: "edit", model: [versionadorInstance: versionadorInstance])
                    return
                }
            }
            versionadorInstance.properties = params
            if (!versionadorInstance.hasErrors() && versionadorInstance.save(flush: true)) {
                flash.message = "${message(code: 'default.updated.message', args: [message(code: 'versionador.label', default: 'Versionador'), versionadorInstance.id])}"
                redirect(action: "show", id: versionadorInstance.id)
            }
            else {
                render(view: "edit", model: [versionadorInstance: versionadorInstance])
            }
        }
        else {
            flash.message = "${message(code: 'default.not.found.message', args: [message(code: 'versionador.label', default: 'Versionador'), params.id])}"
            redirect(action: "list")
        }
    }

    def delete = {
        def versionadorInstance = Versionador.get(params.id)
        if (versionadorInstance) {
            try {
                versionadorInstance.delete(flush: true)
                flash.message = "${message(code: 'default.deleted.message', args: [message(code: 'versionador.label', default: 'Versionador'), params.id])}"
                redirect(action: "list")
            }
            catch (org.springframework.dao.DataIntegrityViolationException e) {
                flash.message = "${message(code: 'default.not.deleted.message', args: [message(code: 'versionador.label', default: 'Versionador'), params.id])}"
                redirect(action: "show", id: params.id)
            }
        }
        else {
            flash.message = "${message(code: 'default.not.found.message', args: [message(code: 'versionador.label', default: 'Versionador'), params.id])}"
            redirect(action: "list")
        }
    }
}
