package ar.com.telecom.pcs.entities

class MailAuditController {
	
	static adminMenu = false

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    def index = {
        redirect(action: "list", params: params)
    }

    def list = {
        params.max = Math.min(params.max ? params.int('max') : 10, 100)
        [mailAuditInstanceList: MailAudit.list(params), mailAuditInstanceTotal: MailAudit.count()]
    }

    def create = {
        def mailAuditInstance = new MailAudit()
        mailAuditInstance.properties = params
        return [mailAuditInstance: mailAuditInstance]
    }

    def save = {
        def mailAuditInstance = new MailAudit(params)
        if (mailAuditInstance.save(flush: true)) {
            flash.message = "${message(code: 'default.created.message', args: [message(code: 'mailAudit.label', default: 'MailAudit'), mailAuditInstance.id])}"
            redirect(action: "show", id: mailAuditInstance.id)
        }
        else {
            render(view: "create", model: [mailAuditInstance: mailAuditInstance])
        }
    }

    def show = {
        def mailAuditInstance = MailAudit.get(params.id)
        if (!mailAuditInstance) {
            flash.message = "${message(code: 'default.not.found.message', args: [message(code: 'mailAudit.label', default: 'MailAudit'), params.id])}"
            redirect(action: "list")
        }
        else {
            [mailAuditInstance: mailAuditInstance]
        }
    }

    def edit = {
        def mailAuditInstance = MailAudit.get(params.id)
        if (!mailAuditInstance) {
            flash.message = "${message(code: 'default.not.found.message', args: [message(code: 'mailAudit.label', default: 'MailAudit'), params.id])}"
            redirect(action: "list")
        }
        else {
            return [mailAuditInstance: mailAuditInstance]
        }
    }

    def update = {
        def mailAuditInstance = MailAudit.get(params.id)
        if (mailAuditInstance) {
            if (params.version) {
                def version = params.version.toLong()
                if (mailAuditInstance.version > version) {
                    
                    mailAuditInstance.errors.rejectValue("version", "default.optimistic.locking.failure", [message(code: 'mailAudit.label', default: 'MailAudit')] as Object[], "Another user has updated this MailAudit while you were editing")
                    render(view: "edit", model: [mailAuditInstance: mailAuditInstance])
                    return
                }
            }
            mailAuditInstance.properties = params
            if (!mailAuditInstance.hasErrors() && mailAuditInstance.save(flush: true)) {
                flash.message = "${message(code: 'default.updated.message', args: [message(code: 'mailAudit.label', default: 'MailAudit'), mailAuditInstance.id])}"
                redirect(action: "show", id: mailAuditInstance.id)
            }
            else {
                render(view: "edit", model: [mailAuditInstance: mailAuditInstance])
            }
        }
        else {
            flash.message = "${message(code: 'default.not.found.message', args: [message(code: 'mailAudit.label', default: 'MailAudit'), params.id])}"
            redirect(action: "list")
        }
    }

    def delete = {
        def mailAuditInstance = MailAudit.get(params.id)
        if (mailAuditInstance) {
            try {
                mailAuditInstance.delete(flush: true)
                flash.message = "${message(code: 'default.deleted.message', args: [message(code: 'mailAudit.label', default: 'MailAudit'), params.id])}"
                redirect(action: "list")
            }
            catch (org.springframework.dao.DataIntegrityViolationException e) {
                flash.message = "${message(code: 'default.not.deleted.message', args: [message(code: 'mailAudit.label', default: 'MailAudit'), params.id])}"
                redirect(action: "show", id: params.id)
            }
        }
        else {
            flash.message = "${message(code: 'default.not.found.message', args: [message(code: 'mailAudit.label', default: 'MailAudit'), params.id])}"
            redirect(action: "list")
        }
    }
}
