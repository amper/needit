package ar.com.telecom.pcs.entities

class AprobacionController {
	
	static adminMenu = false

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    def index = {
        redirect(action: "list", params: params)
    }

    def list = {
        params.max = Math.min(params.max ? params.int('max') : 10, 100)
        [aprobacionInstanceList: Aprobacion.list(params), aprobacionInstanceTotal: Aprobacion.count()]
    }

    def create = {
        def aprobacionInstance = new Aprobacion()
        aprobacionInstance.properties = params
        return [aprobacionInstance: aprobacionInstance]
    }

    def save = {
        def aprobacionInstance = new Aprobacion(params)
        if (aprobacionInstance.save(flush: true)) {
            flash.message = "${message(code: 'default.created.message', args: [message(code: 'aprobacion.label', default: 'Aprobacion'), aprobacionInstance.id])}"
            redirect(action: "show", id: aprobacionInstance.id)
        }
        else {
            render(view: "create", model: [aprobacionInstance: aprobacionInstance])
        }
    }

    def show = {
        def aprobacionInstance = Aprobacion.get(params.id)
        if (!aprobacionInstance) {
            flash.message = "${message(code: 'default.not.found.message', args: [message(code: 'aprobacion.label', default: 'Aprobacion'), params.id])}"
            redirect(action: "list")
        }
        else {
            [aprobacionInstance: aprobacionInstance]
        }
    }

    def edit = {
        def aprobacionInstance = Aprobacion.get(params.id)
        if (!aprobacionInstance) {
            flash.message = "${message(code: 'default.not.found.message', args: [message(code: 'aprobacion.label', default: 'Aprobacion'), params.id])}"
            redirect(action: "list")
        }
        else {
            return [aprobacionInstance: aprobacionInstance]
        }
    }

    def update = {
        def aprobacionInstance = Aprobacion.get(params.id)
        if (aprobacionInstance) {
            if (params.version) {
                def version = params.version.toLong()
                if (aprobacionInstance.version > version) {
                    
                    aprobacionInstance.errors.rejectValue("version", "default.optimistic.locking.failure", [message(code: 'aprobacion.label', default: 'Aprobacion')] as Object[], "Another user has updated this Aprobacion while you were editing")
                    render(view: "edit", model: [aprobacionInstance: aprobacionInstance])
                    return
                }
            }
            aprobacionInstance.properties = params
            if (!aprobacionInstance.hasErrors() && aprobacionInstance.save(flush: true)) {
                flash.message = "${message(code: 'default.updated.message', args: [message(code: 'aprobacion.label', default: 'Aprobacion'), aprobacionInstance.id])}"
                redirect(action: "show", id: aprobacionInstance.id)
            }
            else {
                render(view: "edit", model: [aprobacionInstance: aprobacionInstance])
            }
        }
        else {
            flash.message = "${message(code: 'default.not.found.message', args: [message(code: 'aprobacion.label', default: 'Aprobacion'), params.id])}"
            redirect(action: "list")
        }
    }

    def delete = {
        def aprobacionInstance = Aprobacion.get(params.id)
        if (aprobacionInstance) {
            try {
                aprobacionInstance.delete(flush: true)
                flash.message = "${message(code: 'default.deleted.message', args: [message(code: 'aprobacion.label', default: 'Aprobacion'), params.id])}"
                redirect(action: "list")
            }
            catch (org.springframework.dao.DataIntegrityViolationException e) {
                flash.message = "${message(code: 'default.not.deleted.message', args: [message(code: 'aprobacion.label', default: 'Aprobacion'), params.id])}"
                redirect(action: "show", id: params.id)
            }
        }
        else {
            flash.message = "${message(code: 'default.not.found.message', args: [message(code: 'aprobacion.label', default: 'Aprobacion'), params.id])}"
            redirect(action: "list")
        }
    }
}
