package ar.com.telecom.pcs.entities

class GrupoGestionCapacidadController {
	
	static adminMenu = false

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    def index = {
        redirect(action: "list", params: params)
    }

    def list = {
        params.max = Math.min(params.max ? params.int('max') : 10, 100)
        [grupoGestionCapacidadInstanceList: SoftwareFactory.list(params), grupoGestionCapacidadInstanceTotal: SoftwareFactory.count()]
    }

    def create = {
        def grupoGestionCapacidadInstance = new SoftwareFactory()
        grupoGestionCapacidadInstance.properties = params
        return [grupoGestionCapacidadInstance: grupoGestionCapacidadInstance]
    }

    def save = {
        def grupoGestionCapacidadInstance = new SoftwareFactory(params)
        if (grupoGestionCapacidadInstance.save(flush: true)) {
            flash.message = "${message(code: 'default.created.message', args: [message(code: 'grupoGestionCapacidad.label', default: 'GrupoGestionCapacidad'), grupoGestionCapacidadInstance.id])}"
            redirect(action: "show", id: grupoGestionCapacidadInstance.id)
        }
        else {
            render(view: "create", model: [grupoGestionCapacidadInstance: grupoGestionCapacidadInstance])
        }
    }

    def show = {
        def grupoGestionCapacidadInstance = SoftwareFactory.get(params.id)
        if (!grupoGestionCapacidadInstance) {
            flash.message = "${message(code: 'default.not.found.message', args: [message(code: 'grupoGestionCapacidad.label', default: 'GrupoGestionCapacidad'), params.id])}"
            redirect(action: "list")
        }
        else {
            [grupoGestionCapacidadInstance: grupoGestionCapacidadInstance]
        }
    }

    def edit = {
        def grupoGestionCapacidadInstance = SoftwareFactory.get(params.id)
        if (!grupoGestionCapacidadInstance) {
            flash.message = "${message(code: 'default.not.found.message', args: [message(code: 'grupoGestionCapacidad.label', default: 'GrupoGestionCapacidad'), params.id])}"
            redirect(action: "list")
        }
        else {
            return [grupoGestionCapacidadInstance: grupoGestionCapacidadInstance]
        }
    }

    def update = {
        def grupoGestionCapacidadInstance = SoftwareFactory.get(params.id)
        if (grupoGestionCapacidadInstance) {
            if (params.version) {
                def version = params.version.toLong()
                if (grupoGestionCapacidadInstance.version > version) {
                    
                    grupoGestionCapacidadInstance.errors.rejectValue("version", "default.optimistic.locking.failure", [message(code: 'grupoGestionCapacidad.label', default: 'GrupoGestionCapacidad')] as Object[], "Another user has updated this GrupoGestionCapacidad while you were editing")
                    render(view: "edit", model: [grupoGestionCapacidadInstance: grupoGestionCapacidadInstance])
                    return
                }
            }
            grupoGestionCapacidadInstance.properties = params
            if (!grupoGestionCapacidadInstance.hasErrors() && grupoGestionCapacidadInstance.save(flush: true)) {
                flash.message = "${message(code: 'default.updated.message', args: [message(code: 'grupoGestionCapacidad.label', default: 'GrupoGestionCapacidad'), grupoGestionCapacidadInstance.id])}"
                redirect(action: "show", id: grupoGestionCapacidadInstance.id)
            }
            else {
                render(view: "edit", model: [grupoGestionCapacidadInstance: grupoGestionCapacidadInstance])
            }
        }
        else {
            flash.message = "${message(code: 'default.not.found.message', args: [message(code: 'grupoGestionCapacidad.label', default: 'GrupoGestionCapacidad'), params.id])}"
            redirect(action: "list")
        }
    }

    def delete = {
        def grupoGestionCapacidadInstance = SoftwareFactory.get(params.id)
        if (grupoGestionCapacidadInstance) {
            try {
                grupoGestionCapacidadInstance.delete(flush: true)
                flash.message = "${message(code: 'default.deleted.message', args: [message(code: 'grupoGestionCapacidad.label', default: 'GrupoGestionCapacidad'), params.id])}"
                redirect(action: "list")
            }
            catch (org.springframework.dao.DataIntegrityViolationException e) {
                flash.message = "${message(code: 'default.not.deleted.message', args: [message(code: 'grupoGestionCapacidad.label', default: 'GrupoGestionCapacidad'), params.id])}"
                redirect(action: "show", id: params.id)
            }
        }
        else {
            flash.message = "${message(code: 'default.not.found.message', args: [message(code: 'grupoGestionCapacidad.label', default: 'GrupoGestionCapacidad'), params.id])}"
            redirect(action: "list")
        }
    }
}
