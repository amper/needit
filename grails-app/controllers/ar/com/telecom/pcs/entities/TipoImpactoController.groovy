package ar.com.telecom.pcs.entities

class TipoImpactoController {

	static adminMenu = true

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    def index = {
        redirect(action: "list", params: params)
    }

    def list = {
        params.max = Math.min(params.max ? params.int('max') : 10, 100)
        [tipoImpactoInstanceList: TipoImpacto.list(params), tipoImpactoInstanceTotal: TipoImpacto.count()]
    }

    def create = {
        def tipoImpactoInstance = new TipoImpacto()
        tipoImpactoInstance.properties = params
        return [tipoImpactoInstance: tipoImpactoInstance]
    }

    def save = {
        def tipoImpactoInstance = new TipoImpacto(params)
        if (tipoImpactoInstance.save(flush: true)) {
            flash.message = "${message(code: 'default.created.message', args: [message(code: 'tipoImpacto.label', default: 'TipoImpacto'), tipoImpactoInstance.id])}"
            redirect(action: "show", id: tipoImpactoInstance.id)
        }
        else {
            render(view: "create", model: [tipoImpactoInstance: tipoImpactoInstance])
        }
    }

    def show = {
        def tipoImpactoInstance = TipoImpacto.get(params.id)
        if (!tipoImpactoInstance) {
            flash.message = "${message(code: 'default.not.found.message', args: [message(code: 'tipoImpacto.label', default: 'TipoImpacto'), params.id])}"
            redirect(action: "list")
        }
        else {
            [tipoImpactoInstance: tipoImpactoInstance]
        }
    }

    def edit = {
        def tipoImpactoInstance = TipoImpacto.get(params.id)
        if (!tipoImpactoInstance) {
            flash.message = "${message(code: 'default.not.found.message', args: [message(code: 'tipoImpacto.label', default: 'TipoImpacto'), params.id])}"
            redirect(action: "list")
        }
        else {
            return [tipoImpactoInstance: tipoImpactoInstance]
        }
    }

    def update = {
        def tipoImpactoInstance = TipoImpacto.get(params.id)
        if (tipoImpactoInstance) {
            if (params.version) {
                def version = params.version.toLong()
                if (tipoImpactoInstance.version > version) {
                    
                    tipoImpactoInstance.errors.rejectValue("version", "default.optimistic.locking.failure", [message(code: 'tipoImpacto.label', default: 'TipoImpacto')] as Object[], "Another user has updated this TipoImpacto while you were editing")
                    render(view: "edit", model: [tipoImpactoInstance: tipoImpactoInstance])
                    return
                }
            }
            tipoImpactoInstance.properties = params
            if (!tipoImpactoInstance.hasErrors() && tipoImpactoInstance.save(flush: true)) {
                flash.message = "${message(code: 'default.updated.message', args: [message(code: 'tipoImpacto.label', default: 'TipoImpacto'), tipoImpactoInstance.id])}"
                redirect(action: "show", id: tipoImpactoInstance.id)
            }
            else {
                render(view: "edit", model: [tipoImpactoInstance: tipoImpactoInstance])
            }
        }
        else {
            flash.message = "${message(code: 'default.not.found.message', args: [message(code: 'tipoImpacto.label', default: 'TipoImpacto'), params.id])}"
            redirect(action: "list")
        }
    }

    def delete = {
        def tipoImpactoInstance = TipoImpacto.get(params.id)
        if (tipoImpactoInstance) {
            try {
                tipoImpactoInstance.delete(flush: true)
                flash.message = "${message(code: 'default.deleted.message', args: [message(code: 'tipoImpacto.label', default: 'TipoImpacto'), params.id])}"
                redirect(action: "list")
            }
            catch (org.springframework.dao.DataIntegrityViolationException e) {
                flash.message = "${message(code: 'default.not.deleted.message', args: [message(code: 'tipoImpacto.label', default: 'TipoImpacto'), params.id])}"
                redirect(action: "show", id: params.id)
            }
        }
        else {
            flash.message = "${message(code: 'default.not.found.message', args: [message(code: 'tipoImpacto.label', default: 'TipoImpacto'), params.id])}"
            redirect(action: "list")
        }
    }
}
