package ar.com.telecom.pcs.entities

class MacroEstadoController {
	
	static adminMenu = true

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    def index = {
        redirect(action: "list", params: params)
    }

    def list = {
        params.max = Math.min(params.max ? params.int('max') : 10, 100)
        [macroEstadoInstanceList: MacroEstado.list(params), macroEstadoInstanceTotal: MacroEstado.count()]
    }

    def create = {
        def macroEstadoInstance = new MacroEstado()
        macroEstadoInstance.properties = params
        return [macroEstadoInstance: macroEstadoInstance]
    }

    def save = {
        def macroEstadoInstance = new MacroEstado(params)
        if (macroEstadoInstance.save(flush: true)) {
            flash.message = "${message(code: 'default.created.message', args: [message(code: 'macroEstado.label', default: 'MacroEstado'), macroEstadoInstance.id])}"
            redirect(action: "show", id: macroEstadoInstance.id)
        }
        else {
            render(view: "create", model: [macroEstadoInstance: macroEstadoInstance])
        }
    }

    def show = {
        def macroEstadoInstance = MacroEstado.get(params.id)
        if (!macroEstadoInstance) {
            flash.message = "${message(code: 'default.not.found.message', args: [message(code: 'macroEstado.label', default: 'MacroEstado'), params.id])}"
            redirect(action: "list")
        }
        else {
            [macroEstadoInstance: macroEstadoInstance]
        }
    }

    def edit = {
        def macroEstadoInstance = MacroEstado.get(params.id)
        if (!macroEstadoInstance) {
            flash.message = "${message(code: 'default.not.found.message', args: [message(code: 'macroEstado.label', default: 'MacroEstado'), params.id])}"
            redirect(action: "list")
        }
        else {
            return [macroEstadoInstance: macroEstadoInstance]
        }
    }

    def update = {
        def macroEstadoInstance = MacroEstado.get(params.id)
        if (macroEstadoInstance) {
            if (params.version) {
                def version = params.version.toLong()
                if (macroEstadoInstance.version > version) {
                    
                    macroEstadoInstance.errors.rejectValue("version", "default.optimistic.locking.failure", [message(code: 'macroEstado.label', default: 'MacroEstado')] as Object[], "Another user has updated this MacroEstado while you were editing")
                    render(view: "edit", model: [macroEstadoInstance: macroEstadoInstance])
                    return
                }
            }
            macroEstadoInstance.properties = params
            if (!macroEstadoInstance.hasErrors() && macroEstadoInstance.save(flush: true)) {
                flash.message = "${message(code: 'default.updated.message', args: [message(code: 'macroEstado.label', default: 'MacroEstado'), macroEstadoInstance.id])}"
                redirect(action: "show", id: macroEstadoInstance.id)
            }
            else {
                render(view: "edit", model: [macroEstadoInstance: macroEstadoInstance])
            }
        }
        else {
            flash.message = "${message(code: 'default.not.found.message', args: [message(code: 'macroEstado.label', default: 'MacroEstado'), params.id])}"
            redirect(action: "list")
        }
    }

    def delete = {
        def macroEstadoInstance = MacroEstado.get(params.id)
        if (macroEstadoInstance) {
            try {
                macroEstadoInstance.delete(flush: true)
                flash.message = "${message(code: 'default.deleted.message', args: [message(code: 'macroEstado.label', default: 'MacroEstado'), params.id])}"
                redirect(action: "list")
            }
            catch (org.springframework.dao.DataIntegrityViolationException e) {
                flash.message = "${message(code: 'default.not.deleted.message', args: [message(code: 'macroEstado.label', default: 'MacroEstado'), params.id])}"
                redirect(action: "show", id: params.id)
            }
        }
        else {
            flash.message = "${message(code: 'default.not.found.message', args: [message(code: 'macroEstado.label', default: 'MacroEstado'), params.id])}"
            redirect(action: "list")
        }
    }
}
