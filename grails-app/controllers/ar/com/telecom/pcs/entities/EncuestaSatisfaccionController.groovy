package ar.com.telecom.pcs.entities

class EncuestaSatisfaccionController {
	
	static adminMenu = false

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    def index = {
        redirect(action: "list", params: params)
    }

    def list = {
        params.max = Math.min(params.max ? params.int('max') : 10, 100)
        [encuestaSatisfaccionInstanceList: EncuestaSatisfaccion.list(params), encuestaSatisfaccionInstanceTotal: EncuestaSatisfaccion.count()]
    }

    def create = {
        def encuestaSatisfaccionInstance = new EncuestaSatisfaccion()
        encuestaSatisfaccionInstance.properties = params
        return [encuestaSatisfaccionInstance: encuestaSatisfaccionInstance]
    }

    def save = {
        def encuestaSatisfaccionInstance = new EncuestaSatisfaccion(params)
        if (encuestaSatisfaccionInstance.save(flush: true)) {
            flash.message = "${message(code: 'default.created.message', args: [message(code: 'encuestaSatisfaccion.label', default: 'EncuestaSatisfaccion'), encuestaSatisfaccionInstance.id])}"
            redirect(action: "show", id: encuestaSatisfaccionInstance.id)
        }
        else {
            render(view: "create", model: [encuestaSatisfaccionInstance: encuestaSatisfaccionInstance])
        }
    }

    def show = {
        def encuestaSatisfaccionInstance = EncuestaSatisfaccion.get(params.id)
        if (!encuestaSatisfaccionInstance) {
            flash.message = "${message(code: 'default.not.found.message', args: [message(code: 'encuestaSatisfaccion.label', default: 'EncuestaSatisfaccion'), params.id])}"
            redirect(action: "list")
        }
        else {
            [encuestaSatisfaccionInstance: encuestaSatisfaccionInstance]
        }
    }

    def edit = {
        def encuestaSatisfaccionInstance = EncuestaSatisfaccion.get(params.id)
        if (!encuestaSatisfaccionInstance) {
            flash.message = "${message(code: 'default.not.found.message', args: [message(code: 'encuestaSatisfaccion.label', default: 'EncuestaSatisfaccion'), params.id])}"
            redirect(action: "list")
        }
        else {
            return [encuestaSatisfaccionInstance: encuestaSatisfaccionInstance]
        }
    }

    def update = {
        def encuestaSatisfaccionInstance = EncuestaSatisfaccion.get(params.id)
        if (encuestaSatisfaccionInstance) {
            if (params.version) {
                def version = params.version.toLong()
                if (encuestaSatisfaccionInstance.version > version) {
                    
                    encuestaSatisfaccionInstance.errors.rejectValue("version", "default.optimistic.locking.failure", [message(code: 'encuestaSatisfaccion.label', default: 'EncuestaSatisfaccion')] as Object[], "Another user has updated this EncuestaSatisfaccion while you were editing")
                    render(view: "edit", model: [encuestaSatisfaccionInstance: encuestaSatisfaccionInstance])
                    return
                }
            }
            encuestaSatisfaccionInstance.properties = params
            if (!encuestaSatisfaccionInstance.hasErrors() && encuestaSatisfaccionInstance.save(flush: true)) {
                flash.message = "${message(code: 'default.updated.message', args: [message(code: 'encuestaSatisfaccion.label', default: 'EncuestaSatisfaccion'), encuestaSatisfaccionInstance.id])}"
                redirect(action: "show", id: encuestaSatisfaccionInstance.id)
            }
            else {
                render(view: "edit", model: [encuestaSatisfaccionInstance: encuestaSatisfaccionInstance])
            }
        }
        else {
            flash.message = "${message(code: 'default.not.found.message', args: [message(code: 'encuestaSatisfaccion.label', default: 'EncuestaSatisfaccion'), params.id])}"
            redirect(action: "list")
        }
    }

    def delete = {
        def encuestaSatisfaccionInstance = EncuestaSatisfaccion.get(params.id)
        if (encuestaSatisfaccionInstance) {
            try {
                encuestaSatisfaccionInstance.delete(flush: true)
                flash.message = "${message(code: 'default.deleted.message', args: [message(code: 'encuestaSatisfaccion.label', default: 'EncuestaSatisfaccion'), params.id])}"
                redirect(action: "list")
            }
            catch (org.springframework.dao.DataIntegrityViolationException e) {
                flash.message = "${message(code: 'default.not.deleted.message', args: [message(code: 'encuestaSatisfaccion.label', default: 'EncuestaSatisfaccion'), params.id])}"
                redirect(action: "show", id: params.id)
            }
        }
        else {
            flash.message = "${message(code: 'default.not.found.message', args: [message(code: 'encuestaSatisfaccion.label', default: 'EncuestaSatisfaccion'), params.id])}"
            redirect(action: "list")
        }
    }
}
