package ar.com.telecom.pcs.entities

class VarianteController {
	
	static adminMenu = false

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    def index = {
        redirect(action: "list", params: params)
    }

    def list = {
        params.max = Math.min(params.max ? params.int('max') : 10, 100)
        [varianteInstanceList: Variante.list(params), varianteInstanceTotal: Variante.count()]
    }

    def create = {
        def varianteInstance = new Variante()
        varianteInstance.properties = params
        return [varianteInstance: varianteInstance]
    }

    def save = {
        def varianteInstance = new Variante(params)
        if (varianteInstance.save(flush: true)) {
            flash.message = "${message(code: 'default.created.message', args: [message(code: 'variante.label', default: 'Variante'), varianteInstance.id])}"
            redirect(action: "show", id: varianteInstance.id)
        }
        else {
            render(view: "create", model: [varianteInstance: varianteInstance])
        }
    }

    def show = {
        def varianteInstance = Variante.get(params.id)
        if (!varianteInstance) {
            flash.message = "${message(code: 'default.not.found.message', args: [message(code: 'variante.label', default: 'Variante'), params.id])}"
            redirect(action: "list")
        }
        else {
            [varianteInstance: varianteInstance]
        }
    }

    def edit = {
        def varianteInstance = Variante.get(params.id)
        if (!varianteInstance) {
            flash.message = "${message(code: 'default.not.found.message', args: [message(code: 'variante.label', default: 'Variante'), params.id])}"
            redirect(action: "list")
        }
        else {
            return [varianteInstance: varianteInstance]
        }
    }

    def update = {
        def varianteInstance = Variante.get(params.id)
        if (varianteInstance) {
            if (params.version) {
                def version = params.version.toLong()
                if (varianteInstance.version > version) {
                    
                    varianteInstance.errors.rejectValue("version", "default.optimistic.locking.failure", [message(code: 'variante.label', default: 'Variante')] as Object[], "Another user has updated this Variante while you were editing")
                    render(view: "edit", model: [varianteInstance: varianteInstance])
                    return
                }
            }
            varianteInstance.properties = params
            if (!varianteInstance.hasErrors() && varianteInstance.save(flush: true)) {
                flash.message = "${message(code: 'default.updated.message', args: [message(code: 'variante.label', default: 'Variante'), varianteInstance.id])}"
                redirect(action: "show", id: varianteInstance.id)
            }
            else {
                render(view: "edit", model: [varianteInstance: varianteInstance])
            }
        }
        else {
            flash.message = "${message(code: 'default.not.found.message', args: [message(code: 'variante.label', default: 'Variante'), params.id])}"
            redirect(action: "list")
        }
    }

    def delete = {
        def varianteInstance = Variante.get(params.id)
        if (varianteInstance) {
            try {
                varianteInstance.delete(flush: true)
                flash.message = "${message(code: 'default.deleted.message', args: [message(code: 'variante.label', default: 'Variante'), params.id])}"
                redirect(action: "list")
            }
            catch (org.springframework.dao.DataIntegrityViolationException e) {
                flash.message = "${message(code: 'default.not.deleted.message', args: [message(code: 'variante.label', default: 'Variante'), params.id])}"
                redirect(action: "show", id: params.id)
            }
        }
        else {
            flash.message = "${message(code: 'default.not.found.message', args: [message(code: 'variante.label', default: 'Variante'), params.id])}"
            redirect(action: "list")
        }
    }
}
