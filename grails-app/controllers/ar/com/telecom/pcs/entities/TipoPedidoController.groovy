package ar.com.telecom.pcs.entities

class TipoPedidoController {
	
	static adminMenu = true

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    def index = {
        redirect(action: "list", params: params)
    }

    def list = {
        params.max = Math.min(params.max ? params.int('max') : 10, 100)
        [tipoPedidoInstanceList: TipoPedido.list(params), tipoPedidoInstanceTotal: TipoPedido.count()]
    }

    def create = {
        def tipoPedidoInstance = new TipoPedido()
        tipoPedidoInstance.properties = params
        return [tipoPedidoInstance: tipoPedidoInstance]
    }

    def save = {
        def tipoPedidoInstance = new TipoPedido(params)
        if (tipoPedidoInstance.save(flush: true)) {
            flash.message = "${message(code: 'default.created.message', args: [message(code: 'tipoPedido.label', default: 'TipoPedido'), tipoPedidoInstance.id])}"
            redirect(action: "show", id: tipoPedidoInstance.id)
        }
        else {
            render(view: "create", model: [tipoPedidoInstance: tipoPedidoInstance])
        }
    }

    def show = {
        def tipoPedidoInstance = TipoPedido.get(params.id)
        if (!tipoPedidoInstance) {
            flash.message = "${message(code: 'default.not.found.message', args: [message(code: 'tipoPedido.label', default: 'TipoPedido'), params.id])}"
            redirect(action: "list")
        }
        else {
            [tipoPedidoInstance: tipoPedidoInstance]
        }
    }

    def edit = {
        def tipoPedidoInstance = TipoPedido.get(params.id)
        if (!tipoPedidoInstance) {
            flash.message = "${message(code: 'default.not.found.message', args: [message(code: 'tipoPedido.label', default: 'TipoPedido'), params.id])}"
            redirect(action: "list")
        }
        else {
            return [tipoPedidoInstance: tipoPedidoInstance]
        }
    }

    def update = {
        def tipoPedidoInstance = TipoPedido.get(params.id)
        if (tipoPedidoInstance) {
            if (params.version) {
                def version = params.version.toLong()
                if (tipoPedidoInstance.version > version) {
                    
                    tipoPedidoInstance.errors.rejectValue("version", "default.optimistic.locking.failure", [message(code: 'tipoPedido.label', default: 'TipoPedido')] as Object[], "Another user has updated this TipoPedido while you were editing")
                    render(view: "edit", model: [tipoPedidoInstance: tipoPedidoInstance])
                    return
                }
            }
            tipoPedidoInstance.properties = params
            if (!tipoPedidoInstance.hasErrors() && tipoPedidoInstance.save(flush: true)) {
                flash.message = "${message(code: 'default.updated.message', args: [message(code: 'tipoPedido.label', default: 'TipoPedido'), tipoPedidoInstance.id])}"
                redirect(action: "show", id: tipoPedidoInstance.id)
            }
            else {
                render(view: "edit", model: [tipoPedidoInstance: tipoPedidoInstance])
            }
        }
        else {
            flash.message = "${message(code: 'default.not.found.message', args: [message(code: 'tipoPedido.label', default: 'TipoPedido'), params.id])}"
            redirect(action: "list")
        }
    }

    def delete = {
        def tipoPedidoInstance = TipoPedido.get(params.id)
        if (tipoPedidoInstance) {
            try {
                tipoPedidoInstance.delete(flush: true)
                flash.message = "${message(code: 'default.deleted.message', args: [message(code: 'tipoPedido.label', default: 'TipoPedido'), params.id])}"
                redirect(action: "list")
            }
            catch (org.springframework.dao.DataIntegrityViolationException e) {
                flash.message = "${message(code: 'default.not.deleted.message', args: [message(code: 'tipoPedido.label', default: 'TipoPedido'), params.id])}"
                redirect(action: "show", id: params.id)
            }
        }
        else {
            flash.message = "${message(code: 'default.not.found.message', args: [message(code: 'tipoPedido.label', default: 'TipoPedido'), params.id])}"
            redirect(action: "list")
        }
    }
}
