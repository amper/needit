package ar.com.telecom.pcs.entities

class ECSController {
	
	static adminMenu = false

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    def index = {
        redirect(action: "list", params: params)
    }

    def list = {
        params.max = Math.min(params.max ? params.int('max') : 10, 100)
        [ECSInstanceList: ECS.list(params), ECSInstanceTotal: ECS.count()]
    }

    def create = {
        def ECSInstance = new ECS()
        ECSInstance.properties = params
        return [ECSInstance: ECSInstance]
    }

    def save = {
        def ECSInstance = new ECS(params)
        if (ECSInstance.save(flush: true)) {
            flash.message = "${message(code: 'default.created.message', args: [message(code: 'ECS.label', default: 'ECS'), ECSInstance.id])}"
            redirect(action: "show", id: ECSInstance.id)
        }
        else {
            render(view: "create", model: [ECSInstance: ECSInstance])
        }
    }

    def show = {
        def ECSInstance = ECS.get(params.id)
        if (!ECSInstance) {
            flash.message = "${message(code: 'default.not.found.message', args: [message(code: 'ECS.label', default: 'ECS'), params.id])}"
            redirect(action: "list")
        }
        else {
            [ECSInstance: ECSInstance]
        }
    }

    def edit = {
        def ECSInstance = ECS.get(params.id)
        if (!ECSInstance) {
            flash.message = "${message(code: 'default.not.found.message', args: [message(code: 'ECS.label', default: 'ECS'), params.id])}"
            redirect(action: "list")
        }
        else {
            return [ECSInstance: ECSInstance]
        }
    }

    def update = {
        def ECSInstance = ECS.get(params.id)
        if (ECSInstance) {
            if (params.version) {
                def version = params.version.toLong()
                if (ECSInstance.version > version) {
                    
                    ECSInstance.errors.rejectValue("version", "default.optimistic.locking.failure", [message(code: 'ECS.label', default: 'ECS')] as Object[], "Another user has updated this ECS while you were editing")
                    render(view: "edit", model: [ECSInstance: ECSInstance])
                    return
                }
            }
            ECSInstance.properties = params
            if (!ECSInstance.hasErrors() && ECSInstance.save(flush: true)) {
                flash.message = "${message(code: 'default.updated.message', args: [message(code: 'ECS.label', default: 'ECS'), ECSInstance.id])}"
                redirect(action: "show", id: ECSInstance.id)
            }
            else {
                render(view: "edit", model: [ECSInstance: ECSInstance])
            }
        }
        else {
            flash.message = "${message(code: 'default.not.found.message', args: [message(code: 'ECS.label', default: 'ECS'), params.id])}"
            redirect(action: "list")
        }
    }

    def delete = {
        def ECSInstance = ECS.get(params.id)
        if (ECSInstance) {
            try {
                ECSInstance.delete(flush: true)
                flash.message = "${message(code: 'default.deleted.message', args: [message(code: 'ECS.label', default: 'ECS'), params.id])}"
                redirect(action: "list")
            }
            catch (org.springframework.dao.DataIntegrityViolationException e) {
                flash.message = "${message(code: 'default.not.deleted.message', args: [message(code: 'ECS.label', default: 'ECS'), params.id])}"
                redirect(action: "show", id: params.id)
            }
        }
        else {
            flash.message = "${message(code: 'default.not.found.message', args: [message(code: 'ECS.label', default: 'ECS'), params.id])}"
            redirect(action: "list")
        }
    }
}
