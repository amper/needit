package ar.com.telecom.pcs.entities

class GrupoGestionDemandaController {
	
	static adminMenu = true

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    def index = {
        redirect(action: "list", params: params)
    }

    def list = {
        params.max = Math.min(params.max ? params.int('max') : 10, 100)
        [grupoGestionDemandaInstanceList: GrupoGestionDemanda.list(params), grupoGestionDemandaInstanceTotal: GrupoGestionDemanda.count()]
    }

    def create = {
        def grupoGestionDemandaInstance = new GrupoGestionDemanda()
        grupoGestionDemandaInstance.properties = params
        return [grupoGestionDemandaInstance: grupoGestionDemandaInstance]
    }

    def save = {
        def grupoGestionDemandaInstance = new GrupoGestionDemanda(params)
        if (grupoGestionDemandaInstance.save(flush: true)) {
            flash.message = "${message(code: 'default.created.message', args: [message(code: 'grupoGestionDemanda.label', default: 'GrupoGestionDemanda'), grupoGestionDemandaInstance.id])}"
            redirect(action: "show", id: grupoGestionDemandaInstance.id)
        }
        else {
            render(view: "create", model: [grupoGestionDemandaInstance: grupoGestionDemandaInstance])
        }
    }

    def show = {
        def grupoGestionDemandaInstance = GrupoGestionDemanda.get(params.id)
        if (!grupoGestionDemandaInstance) {
            flash.message = "${message(code: 'default.not.found.message', args: [message(code: 'grupoGestionDemanda.label', default: 'GrupoGestionDemanda'), params.id])}"
            redirect(action: "list")
        }
        else {
            [grupoGestionDemandaInstance: grupoGestionDemandaInstance]
        }
    }

    def edit = {
        def grupoGestionDemandaInstance = GrupoGestionDemanda.get(params.id)
        if (!grupoGestionDemandaInstance) {
            flash.message = "${message(code: 'default.not.found.message', args: [message(code: 'grupoGestionDemanda.label', default: 'GrupoGestionDemanda'), params.id])}"
            redirect(action: "list")
        }
        else {
            return [grupoGestionDemandaInstance: grupoGestionDemandaInstance]
        }
    }

    def update = {
        def grupoGestionDemandaInstance = GrupoGestionDemanda.get(params.id)
        if (grupoGestionDemandaInstance) {
            if (params.version) {
                def version = params.version.toLong()
                if (grupoGestionDemandaInstance.version > version) {
                    
                    grupoGestionDemandaInstance.errors.rejectValue("version", "default.optimistic.locking.failure", [message(code: 'grupoGestionDemanda.label', default: 'GrupoGestionDemanda')] as Object[], "Another user has updated this GrupoGestionDemanda while you were editing")
                    render(view: "edit", model: [grupoGestionDemandaInstance: grupoGestionDemandaInstance])
                    return
                }
            }
            grupoGestionDemandaInstance.properties = params
            if (!grupoGestionDemandaInstance.hasErrors() && grupoGestionDemandaInstance.save(flush: true)) {
                flash.message = "${message(code: 'default.updated.message', args: [message(code: 'grupoGestionDemanda.label', default: 'GrupoGestionDemanda'), grupoGestionDemandaInstance.id])}"
                redirect(action: "show", id: grupoGestionDemandaInstance.id)
            }
            else {
                render(view: "edit", model: [grupoGestionDemandaInstance: grupoGestionDemandaInstance])
            }
        }
        else {
            flash.message = "${message(code: 'default.not.found.message', args: [message(code: 'grupoGestionDemanda.label', default: 'GrupoGestionDemanda'), params.id])}"
            redirect(action: "list")
        }
    }

    def delete = {
        def grupoGestionDemandaInstance = GrupoGestionDemanda.get(params.id)
        if (grupoGestionDemandaInstance) {
            try {
                grupoGestionDemandaInstance.delete(flush: true)
                flash.message = "${message(code: 'default.deleted.message', args: [message(code: 'grupoGestionDemanda.label', default: 'GrupoGestionDemanda'), params.id])}"
                redirect(action: "list")
            }
            catch (org.springframework.dao.DataIntegrityViolationException e) {
                flash.message = "${message(code: 'default.not.deleted.message', args: [message(code: 'grupoGestionDemanda.label', default: 'GrupoGestionDemanda'), params.id])}"
                redirect(action: "show", id: params.id)
            }
        }
        else {
            flash.message = "${message(code: 'default.not.found.message', args: [message(code: 'grupoGestionDemanda.label', default: 'GrupoGestionDemanda'), params.id])}"
            redirect(action: "list")
        }
    }
}
