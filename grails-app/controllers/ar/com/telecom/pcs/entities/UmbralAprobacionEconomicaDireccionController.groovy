package ar.com.telecom.pcs.entities

class UmbralAprobacionEconomicaDireccionController {
	
	static adminMenu = true

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    def index = {
        redirect(action: "list", params: params)
    }

    def list = {
        params.max = Math.min(params.max ? params.int('max') : 10, 100)
        [umbralAprobacionEconomicaDireccionInstanceList: UmbralAprobacionEconomicaDireccion.list(params), umbralAprobacionEconomicaDireccionInstanceTotal: UmbralAprobacionEconomicaDireccion.count()]
    }

    def create = {
        def umbralAprobacionEconomicaDireccionInstance = new UmbralAprobacionEconomicaDireccion()
        umbralAprobacionEconomicaDireccionInstance.properties = params
        return [umbralAprobacionEconomicaDireccionInstance: umbralAprobacionEconomicaDireccionInstance]
    }

    def save = {
        def umbralAprobacionEconomicaDireccionInstance = new UmbralAprobacionEconomicaDireccion(params)
        if (umbralAprobacionEconomicaDireccionInstance.save(flush: true)) {
            flash.message = "${message(code: 'default.created.message', args: [message(code: 'umbralAprobacionEconomicaDireccion.label', default: 'UmbralAprobacionEconomicaDireccion'), umbralAprobacionEconomicaDireccionInstance.id])}"
            redirect(action: "show", id: umbralAprobacionEconomicaDireccionInstance.id)
        }
        else {
            render(view: "create", model: [umbralAprobacionEconomicaDireccionInstance: umbralAprobacionEconomicaDireccionInstance])
        }
    }

    def show = {
        def umbralAprobacionEconomicaDireccionInstance = UmbralAprobacionEconomicaDireccion.get(params.id)
        if (!umbralAprobacionEconomicaDireccionInstance) {
            flash.message = "${message(code: 'default.not.found.message', args: [message(code: 'umbralAprobacionEconomicaDireccion.label', default: 'UmbralAprobacionEconomicaDireccion'), params.id])}"
            redirect(action: "list")
        }
        else {
            [umbralAprobacionEconomicaDireccionInstance: umbralAprobacionEconomicaDireccionInstance]
        }
    }

    def edit = {
        def umbralAprobacionEconomicaDireccionInstance = UmbralAprobacionEconomicaDireccion.get(params.id)
        if (!umbralAprobacionEconomicaDireccionInstance) {
            flash.message = "${message(code: 'default.not.found.message', args: [message(code: 'umbralAprobacionEconomicaDireccion.label', default: 'UmbralAprobacionEconomicaDireccion'), params.id])}"
            redirect(action: "list")
        }
        else {
            return [umbralAprobacionEconomicaDireccionInstance: umbralAprobacionEconomicaDireccionInstance]
        }
    }

    def update = {
        def umbralAprobacionEconomicaDireccionInstance = UmbralAprobacionEconomicaDireccion.get(params.id)
        if (umbralAprobacionEconomicaDireccionInstance) {
            if (params.version) {
                def version = params.version.toLong()
                if (umbralAprobacionEconomicaDireccionInstance.version > version) {
                    
                    umbralAprobacionEconomicaDireccionInstance.errors.rejectValue("version", "default.optimistic.locking.failure", [message(code: 'umbralAprobacionEconomicaDireccion.label', default: 'UmbralAprobacionEconomicaDireccion')] as Object[], "Another user has updated this UmbralAprobacionEconomicaDireccion while you were editing")
                    render(view: "edit", model: [umbralAprobacionEconomicaDireccionInstance: umbralAprobacionEconomicaDireccionInstance])
                    return
                }
            }
            umbralAprobacionEconomicaDireccionInstance.properties = params
            if (!umbralAprobacionEconomicaDireccionInstance.hasErrors() && umbralAprobacionEconomicaDireccionInstance.save(flush: true)) {
                flash.message = "${message(code: 'default.updated.message', args: [message(code: 'umbralAprobacionEconomicaDireccion.label', default: 'UmbralAprobacionEconomicaDireccion'), umbralAprobacionEconomicaDireccionInstance.id])}"
                redirect(action: "show", id: umbralAprobacionEconomicaDireccionInstance.id)
            }
            else {
                render(view: "edit", model: [umbralAprobacionEconomicaDireccionInstance: umbralAprobacionEconomicaDireccionInstance])
            }
        }
        else {
            flash.message = "${message(code: 'default.not.found.message', args: [message(code: 'umbralAprobacionEconomicaDireccion.label', default: 'UmbralAprobacionEconomicaDireccion'), params.id])}"
            redirect(action: "list")
        }
    }

    def delete = {
        def umbralAprobacionEconomicaDireccionInstance = UmbralAprobacionEconomicaDireccion.get(params.id)
        if (umbralAprobacionEconomicaDireccionInstance) {
            try {
                umbralAprobacionEconomicaDireccionInstance.delete(flush: true)
                flash.message = "${message(code: 'default.deleted.message', args: [message(code: 'umbralAprobacionEconomicaDireccion.label', default: 'UmbralAprobacionEconomicaDireccion'), params.id])}"
                redirect(action: "list")
            }
            catch (org.springframework.dao.DataIntegrityViolationException e) {
                flash.message = "${message(code: 'default.not.deleted.message', args: [message(code: 'umbralAprobacionEconomicaDireccion.label', default: 'UmbralAprobacionEconomicaDireccion'), params.id])}"
                redirect(action: "show", id: params.id)
            }
        }
        else {
            flash.message = "${message(code: 'default.not.found.message', args: [message(code: 'umbralAprobacionEconomicaDireccion.label', default: 'UmbralAprobacionEconomicaDireccion'), params.id])}"
            redirect(action: "list")
        }
    }
}
