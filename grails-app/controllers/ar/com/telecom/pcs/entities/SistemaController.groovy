package ar.com.telecom.pcs.entities

class SistemaController {

	static adminMenu = true

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    def index = {
        redirect(action: "list", params: params)
    }

    def list = {
        params.max = Math.min(params.max ? params.int('max') : 10, 100)
        [sistemaInstanceList: Sistema.list(params), sistemaInstanceTotal: Sistema.count()]
    }

    def create = {
        def sistemaInstance = new Sistema()
        sistemaInstance.properties = params
        return [sistemaInstance: sistemaInstance]
    }

    def save = {
        def sistemaInstance = new Sistema(params)
        if (sistemaInstance.save(flush: true)) {
            flash.message = "${message(code: 'default.created.message', args: [message(code: 'sistema.label', default: 'Sistema'), sistemaInstance.id])}"
            redirect(action: "show", id: sistemaInstance.id)
        }
        else {
            render(view: "create", model: [sistemaInstance: sistemaInstance])
        }
    }

    def show = {
        def sistemaInstance = Sistema.get(params.id)
        if (!sistemaInstance) {
            flash.message = "${message(code: 'default.not.found.message', args: [message(code: 'sistema.label', default: 'Sistema'), params.id])}"
            redirect(action: "list")
        }
        else {
            [sistemaInstance: sistemaInstance]
        }
    }

    def edit = {
        def sistemaInstance = Sistema.get(params.id)
        if (!sistemaInstance) {
            flash.message = "${message(code: 'default.not.found.message', args: [message(code: 'sistema.label', default: 'Sistema'), params.id])}"
            redirect(action: "list")
        }
        else {
            return [sistemaInstance: sistemaInstance]
        }
    }

    def update = {
        def sistemaInstance = Sistema.get(params.id)
        if (sistemaInstance) {
            if (params.version) {
                def version = params.version.toLong()
                if (sistemaInstance.version > version) {
                    
                    sistemaInstance.errors.rejectValue("version", "default.optimistic.locking.failure", [message(code: 'sistema.label', default: 'Sistema')] as Object[], "Another user has updated this Sistema while you were editing")
                    render(view: "edit", model: [sistemaInstance: sistemaInstance])
                    return
                }
            }
            sistemaInstance.properties = params
            if (!sistemaInstance.hasErrors() && sistemaInstance.save(flush: true)) {
                flash.message = "${message(code: 'default.updated.message', args: [message(code: 'sistema.label', default: 'Sistema'), sistemaInstance.id])}"
                redirect(action: "show", id: sistemaInstance.id)
            }
            else {
                render(view: "edit", model: [sistemaInstance: sistemaInstance])
            }
        }
        else {
            flash.message = "${message(code: 'default.not.found.message', args: [message(code: 'sistema.label', default: 'Sistema'), params.id])}"
            redirect(action: "list")
        }
    }

    def delete = {
        def sistemaInstance = Sistema.get(params.id)
        if (sistemaInstance) {
            try {
                sistemaInstance.delete(flush: true)
                flash.message = "${message(code: 'default.deleted.message', args: [message(code: 'sistema.label', default: 'Sistema'), params.id])}"
                redirect(action: "list")
            }
            catch (org.springframework.dao.DataIntegrityViolationException e) {
                flash.message = "${message(code: 'default.not.deleted.message', args: [message(code: 'sistema.label', default: 'Sistema'), params.id])}"
                redirect(action: "show", id: params.id)
            }
        }
        else {
            flash.message = "${message(code: 'default.not.found.message', args: [message(code: 'sistema.label', default: 'Sistema'), params.id])}"
            redirect(action: "list")
        }
    }
}
