package ar.com.telecom.pcs.entities

class ActividadPlanificacionController {

	static adminMenu = true

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    def index = {
        redirect(action: "list", params: params)
    }

    def list = {
        params.max = Math.min(params.max ? params.int('max') : 10, 100)
        [actividadPlanificacionInstanceList: ActividadPlanificacion.list(params), actividadPlanificacionInstanceTotal: ActividadPlanificacion.count()]
    }

    def create = {
        def actividadPlanificacionInstance = new ActividadPlanificacion()
        actividadPlanificacionInstance.properties = params
        return [actividadPlanificacionInstance: actividadPlanificacionInstance]
    }

    def save = {
        def actividadPlanificacionInstance = new ActividadPlanificacion(params)
        if (actividadPlanificacionInstance.save(flush: true)) {
            flash.message = "${message(code: 'default.created.message', args: [message(code: 'actividadPlanificacion.label', default: 'ActividadPlanificacion'), actividadPlanificacionInstance.id])}"
            redirect(action: "show", id: actividadPlanificacionInstance.id)
        }
        else {
            render(view: "create", model: [actividadPlanificacionInstance: actividadPlanificacionInstance])
        }
    }

    def show = {
        def actividadPlanificacionInstance = ActividadPlanificacion.get(params.id)
        if (!actividadPlanificacionInstance) {
            flash.message = "${message(code: 'default.not.found.message', args: [message(code: 'actividadPlanificacion.label', default: 'ActividadPlanificacion'), params.id])}"
            redirect(action: "list")
        }
        else {
            [actividadPlanificacionInstance: actividadPlanificacionInstance]
        }
    }

    def edit = {
        def actividadPlanificacionInstance = ActividadPlanificacion.get(params.id)
        if (!actividadPlanificacionInstance) {
            flash.message = "${message(code: 'default.not.found.message', args: [message(code: 'actividadPlanificacion.label', default: 'ActividadPlanificacion'), params.id])}"
            redirect(action: "list")
        }
        else {
            return [actividadPlanificacionInstance: actividadPlanificacionInstance]
        }
    }

    def update = {
        def actividadPlanificacionInstance = ActividadPlanificacion.get(params.id)
        if (actividadPlanificacionInstance) {
            if (params.version) {
                def version = params.version.toLong()
                if (actividadPlanificacionInstance.version > version) {
                    
                    actividadPlanificacionInstance.errors.rejectValue("version", "default.optimistic.locking.failure", [message(code: 'actividadPlanificacion.label', default: 'ActividadPlanificacion')] as Object[], "Another user has updated this ActividadPlanificacion while you were editing")
                    render(view: "edit", model: [actividadPlanificacionInstance: actividadPlanificacionInstance])
                    return
                }
            }
            actividadPlanificacionInstance.properties = params
            if (!actividadPlanificacionInstance.hasErrors() && actividadPlanificacionInstance.save(flush: true)) {
                flash.message = "${message(code: 'default.updated.message', args: [message(code: 'actividadPlanificacion.label', default: 'ActividadPlanificacion'), actividadPlanificacionInstance.id])}"
                redirect(action: "show", id: actividadPlanificacionInstance.id)
            }
            else {
                render(view: "edit", model: [actividadPlanificacionInstance: actividadPlanificacionInstance])
            }
        }
        else {
            flash.message = "${message(code: 'default.not.found.message', args: [message(code: 'actividadPlanificacion.label', default: 'ActividadPlanificacion'), params.id])}"
            redirect(action: "list")
        }
    }

    def delete = {
        def actividadPlanificacionInstance = ActividadPlanificacion.get(params.id)
        if (actividadPlanificacionInstance) {
            try {
                actividadPlanificacionInstance.delete(flush: true)
                flash.message = "${message(code: 'default.deleted.message', args: [message(code: 'actividadPlanificacion.label', default: 'ActividadPlanificacion'), params.id])}"
                redirect(action: "list")
            }
            catch (org.springframework.dao.DataIntegrityViolationException e) {
                flash.message = "${message(code: 'default.not.deleted.message', args: [message(code: 'actividadPlanificacion.label', default: 'ActividadPlanificacion'), params.id])}"
                redirect(action: "show", id: params.id)
            }
        }
        else {
            flash.message = "${message(code: 'default.not.found.message', args: [message(code: 'actividadPlanificacion.label', default: 'ActividadPlanificacion'), params.id])}"
            redirect(action: "list")
        }
    }
}
