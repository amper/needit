package ar.com.telecom.pcs.entities

class ValidacionCruceCamposAnexosController {
	
	static adminMenu = true

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    def index = {
        redirect(action: "list", params: params)
    }

    def list = {
        params.max = Math.min(params.max ? params.int('max') : 10, 100)
        [validacionCruceCamposAnexosInstanceList: ValidacionCruceCamposAnexos.list(params), validacionCruceCamposAnexosInstanceTotal: ValidacionCruceCamposAnexos.count()]
    }

    def create = {
        def validacionCruceCamposAnexosInstance = new ValidacionCruceCamposAnexos()
        validacionCruceCamposAnexosInstance.properties = params
        return [validacionCruceCamposAnexosInstance: validacionCruceCamposAnexosInstance]
    }

    def save = {
        def validacionCruceCamposAnexosInstance = new ValidacionCruceCamposAnexos(params)
        if (validacionCruceCamposAnexosInstance.save(flush: true)) {
            flash.message = "${message(code: 'default.created.message', args: [message(code: 'validacionCruceCamposAnexos.label', default: 'ValidacionCruceCamposAnexos'), validacionCruceCamposAnexosInstance.id])}"
            redirect(action: "show", id: validacionCruceCamposAnexosInstance.id)
        }
        else {
            render(view: "create", model: [validacionCruceCamposAnexosInstance: validacionCruceCamposAnexosInstance])
        }
    }

    def show = {
        def validacionCruceCamposAnexosInstance = ValidacionCruceCamposAnexos.get(params.id)
        if (!validacionCruceCamposAnexosInstance) {
            flash.message = "${message(code: 'default.not.found.message', args: [message(code: 'validacionCruceCamposAnexos.label', default: 'ValidacionCruceCamposAnexos'), params.id])}"
            redirect(action: "list")
        }
        else {
            [validacionCruceCamposAnexosInstance: validacionCruceCamposAnexosInstance]
        }
    }

    def edit = {
        def validacionCruceCamposAnexosInstance = ValidacionCruceCamposAnexos.get(params.id)
        if (!validacionCruceCamposAnexosInstance) {
            flash.message = "${message(code: 'default.not.found.message', args: [message(code: 'validacionCruceCamposAnexos.label', default: 'ValidacionCruceCamposAnexos'), params.id])}"
            redirect(action: "list")
        }
        else {
            return [validacionCruceCamposAnexosInstance: validacionCruceCamposAnexosInstance]
        }
    }

    def update = {
        def validacionCruceCamposAnexosInstance = ValidacionCruceCamposAnexos.get(params.id)
        if (validacionCruceCamposAnexosInstance) {
            if (params.version) {
                def version = params.version.toLong()
                if (validacionCruceCamposAnexosInstance.version > version) {
                    
                    validacionCruceCamposAnexosInstance.errors.rejectValue("version", "default.optimistic.locking.failure", [message(code: 'validacionCruceCamposAnexos.label', default: 'ValidacionCruceCamposAnexos')] as Object[], "Another user has updated this ValidacionCruceCamposAnexos while you were editing")
                    render(view: "edit", model: [validacionCruceCamposAnexosInstance: validacionCruceCamposAnexosInstance])
                    return
                }
            }
            validacionCruceCamposAnexosInstance.properties = params
            if (!validacionCruceCamposAnexosInstance.hasErrors() && validacionCruceCamposAnexosInstance.save(flush: true)) {
                flash.message = "${message(code: 'default.updated.message', args: [message(code: 'validacionCruceCamposAnexos.label', default: 'ValidacionCruceCamposAnexos'), validacionCruceCamposAnexosInstance.id])}"
                redirect(action: "show", id: validacionCruceCamposAnexosInstance.id)
            }
            else {
                render(view: "edit", model: [validacionCruceCamposAnexosInstance: validacionCruceCamposAnexosInstance])
            }
        }
        else {
            flash.message = "${message(code: 'default.not.found.message', args: [message(code: 'validacionCruceCamposAnexos.label', default: 'ValidacionCruceCamposAnexos'), params.id])}"
            redirect(action: "list")
        }
    }

    def delete = {
        def validacionCruceCamposAnexosInstance = ValidacionCruceCamposAnexos.get(params.id)
        if (validacionCruceCamposAnexosInstance) {
            try {
                validacionCruceCamposAnexosInstance.delete(flush: true)
                flash.message = "${message(code: 'default.deleted.message', args: [message(code: 'validacionCruceCamposAnexos.label', default: 'ValidacionCruceCamposAnexos'), params.id])}"
                redirect(action: "list")
            }
            catch (org.springframework.dao.DataIntegrityViolationException e) {
                flash.message = "${message(code: 'default.not.deleted.message', args: [message(code: 'validacionCruceCamposAnexos.label', default: 'ValidacionCruceCamposAnexos'), params.id])}"
                redirect(action: "show", id: params.id)
            }
        }
        else {
            flash.message = "${message(code: 'default.not.found.message', args: [message(code: 'validacionCruceCamposAnexos.label', default: 'ValidacionCruceCamposAnexos'), params.id])}"
            redirect(action: "list")
        }
    }
}
