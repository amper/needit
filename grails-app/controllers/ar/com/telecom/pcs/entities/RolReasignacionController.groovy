package ar.com.telecom.pcs.entities

class RolReasignacionController {

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    def index = {
        redirect(action: "list", params: params)
    }

    def list = {
        params.max = Math.min(params.max ? params.int('max') : 10, 100)
        [rolReasignacionInstanceList: RolReasignacion.list(params), rolReasignacionInstanceTotal: RolReasignacion.count()]
    }

    def create = {
        def rolReasignacionInstance = new RolReasignacion()
        rolReasignacionInstance.properties = params
        return [rolReasignacionInstance: rolReasignacionInstance]
    }

    def save = {
        def rolReasignacionInstance = new RolReasignacion(params)
        if (rolReasignacionInstance.save(flush: true)) {
            flash.message = "${message(code: 'default.created.message', args: [message(code: 'rolReasignacion.label', default: 'RolReasignacion'), rolReasignacionInstance.id])}"
            redirect(action: "show", id: rolReasignacionInstance.id)
        }
        else {
            render(view: "create", model: [rolReasignacionInstance: rolReasignacionInstance])
        }
    }

    def show = {
        def rolReasignacionInstance = RolReasignacion.get(params.id)
        if (!rolReasignacionInstance) {
            flash.message = "${message(code: 'default.not.found.message', args: [message(code: 'rolReasignacion.label', default: 'RolReasignacion'), params.id])}"
            redirect(action: "list")
        }
        else {
            [rolReasignacionInstance: rolReasignacionInstance]
        }
    }

    def edit = {
        def rolReasignacionInstance = RolReasignacion.get(params.id)
        if (!rolReasignacionInstance) {
            flash.message = "${message(code: 'default.not.found.message', args: [message(code: 'rolReasignacion.label', default: 'RolReasignacion'), params.id])}"
            redirect(action: "list")
        }
        else {
            return [rolReasignacionInstance: rolReasignacionInstance]
        }
    }

    def update = {
        def rolReasignacionInstance = RolReasignacion.get(params.id)
        if (rolReasignacionInstance) {
            if (params.version) {
                def version = params.version.toLong()
                if (rolReasignacionInstance.version > version) {
                    
                    rolReasignacionInstance.errors.rejectValue("version", "default.optimistic.locking.failure", [message(code: 'rolReasignacion.label', default: 'RolReasignacion')] as Object[], "Another user has updated this RolReasignacion while you were editing")
                    render(view: "edit", model: [rolReasignacionInstance: rolReasignacionInstance])
                    return
                }
            }
            rolReasignacionInstance.properties = params
            if (!rolReasignacionInstance.hasErrors() && rolReasignacionInstance.save(flush: true)) {
                flash.message = "${message(code: 'default.updated.message', args: [message(code: 'rolReasignacion.label', default: 'RolReasignacion'), rolReasignacionInstance.id])}"
                redirect(action: "show", id: rolReasignacionInstance.id)
            }
            else {
                render(view: "edit", model: [rolReasignacionInstance: rolReasignacionInstance])
            }
        }
        else {
            flash.message = "${message(code: 'default.not.found.message', args: [message(code: 'rolReasignacion.label', default: 'RolReasignacion'), params.id])}"
            redirect(action: "list")
        }
    }

    def delete = {
        def rolReasignacionInstance = RolReasignacion.get(params.id)
        if (rolReasignacionInstance) {
            try {
                rolReasignacionInstance.delete(flush: true)
                flash.message = "${message(code: 'default.deleted.message', args: [message(code: 'rolReasignacion.label', default: 'RolReasignacion'), params.id])}"
                redirect(action: "list")
            }
            catch (org.springframework.dao.DataIntegrityViolationException e) {
                flash.message = "${message(code: 'default.not.deleted.message', args: [message(code: 'rolReasignacion.label', default: 'RolReasignacion'), params.id])}"
                redirect(action: "show", id: params.id)
            }
        }
        else {
            flash.message = "${message(code: 'default.not.found.message', args: [message(code: 'rolReasignacion.label', default: 'RolReasignacion'), params.id])}"
            redirect(action: "list")
        }
    }
}
