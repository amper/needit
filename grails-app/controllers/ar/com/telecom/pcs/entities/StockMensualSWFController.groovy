package ar.com.telecom.pcs.entities

class StockMensualSWFController {
	
	static adminMenu = true

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    def index = {
        redirect(action: "list", params: params)
    }

    def list = {
        params.max = Math.min(params.max ? params.int('max') : 10, 100)
        [stockMensualSWFInstanceList: StockMensualSWF.list(params), stockMensualSWFInstanceTotal: StockMensualSWF.count()]
    }

    def create = {
        def stockMensualSWFInstance = new StockMensualSWF()
        stockMensualSWFInstance.properties = params
        return [stockMensualSWFInstance: stockMensualSWFInstance]
    }

    def save = {
		def swf = SoftwareFactory.findById(params.softwareFactory.id)
        def stockMensualSWFInstance = new StockMensualSWF(params)
		swf.addToStockMensualHoras(stockMensualSWFInstance)
        if (swf.save(flush: true)) {
            flash.message = "${message(code: 'default.created.message', args: [message(code: 'stockMensualSWF.label', default: 'StockMensualSWF'), stockMensualSWFInstance.id])}"
            redirect(action: "show", id: stockMensualSWFInstance.id)
        }
        else {
            render(view: "create", model: [stockMensualSWFInstance: stockMensualSWFInstance])
        }
    }

    def show = {
        def stockMensualSWFInstance = StockMensualSWF.get(params.id)
        if (!stockMensualSWFInstance) {
            flash.message = "${message(code: 'default.not.found.message', args: [message(code: 'stockMensualSWF.label', default: 'StockMensualSWF'), params.id])}"
            redirect(action: "list")
        }
        else {
            [stockMensualSWFInstance: stockMensualSWFInstance]
        }
    }

    def edit = {
        def stockMensualSWFInstance = StockMensualSWF.get(params.id)
        if (!stockMensualSWFInstance) {
            flash.message = "${message(code: 'default.not.found.message', args: [message(code: 'stockMensualSWF.label', default: 'StockMensualSWF'), params.id])}"
            redirect(action: "list")
        }
        else {
            return [stockMensualSWFInstance: stockMensualSWFInstance]
        }
    }

    def update = {
        def stockMensualSWFInstance = StockMensualSWF.get(params.id)
        if (stockMensualSWFInstance) {
            if (params.version) {
                def version = params.version.toLong()
                if (stockMensualSWFInstance.version > version) {
                    
                    stockMensualSWFInstance.errors.rejectValue("version", "default.optimistic.locking.failure", [message(code: 'stockMensualSWF.label', default: 'StockMensualSWF')] as Object[], "Another user has updated this StockMensualSWF while you were editing")
                    render(view: "edit", model: [stockMensualSWFInstance: stockMensualSWFInstance])
                    return
                }
            }
            stockMensualSWFInstance.properties = params
            if (!stockMensualSWFInstance.hasErrors() && stockMensualSWFInstance.save(flush: true)) {
                flash.message = "${message(code: 'default.updated.message', args: [message(code: 'stockMensualSWF.label', default: 'StockMensualSWF'), stockMensualSWFInstance.id])}"
                redirect(action: "show", id: stockMensualSWFInstance.id)
            }
            else {
                render(view: "edit", model: [stockMensualSWFInstance: stockMensualSWFInstance])
            }
        }
        else {
            flash.message = "${message(code: 'default.not.found.message', args: [message(code: 'stockMensualSWF.label', default: 'StockMensualSWF'), params.id])}"
            redirect(action: "list")
        }
    }

    def delete = {
        def stockMensualSWFInstance = StockMensualSWF.get(params.id)
        if (stockMensualSWFInstance) {
            try {
                stockMensualSWFInstance.delete(flush: true)
                flash.message = "${message(code: 'default.deleted.message', args: [message(code: 'stockMensualSWF.label', default: 'StockMensualSWF'), params.id])}"
                redirect(action: "list")
            }
            catch (org.springframework.dao.DataIntegrityViolationException e) {
                flash.message = "${message(code: 'default.not.deleted.message', args: [message(code: 'stockMensualSWF.label', default: 'StockMensualSWF'), params.id])}"
                redirect(action: "show", id: params.id)
            }
        }
        else {
            flash.message = "${message(code: 'default.not.found.message', args: [message(code: 'stockMensualSWF.label', default: 'StockMensualSWF'), params.id])}"
            redirect(action: "list")
        }
    }
}
