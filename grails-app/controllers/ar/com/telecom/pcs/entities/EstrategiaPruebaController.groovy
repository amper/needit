package ar.com.telecom.pcs.entities

class EstrategiaPruebaController {
	
	static adminMenu = false

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    def index = {
        redirect(action: "list", params: params)
    }

    def list = {
        params.max = Math.min(params.max ? params.int('max') : 10, 100)
        [estrategiaPruebaInstanceList: EstrategiaPrueba.list(params), estrategiaPruebaInstanceTotal: EstrategiaPrueba.count()]
    }

    def create = {
        def estrategiaPruebaInstance = new EstrategiaPrueba()
        estrategiaPruebaInstance.properties = params
        return [estrategiaPruebaInstance: estrategiaPruebaInstance]
    }

    def save = {
        def estrategiaPruebaInstance = new EstrategiaPrueba(params)
        if (estrategiaPruebaInstance.save(flush: true)) {
            flash.message = "${message(code: 'default.created.message', args: [message(code: 'estrategiaPrueba.label', default: 'EstrategiaPrueba'), estrategiaPruebaInstance.id])}"
            redirect(action: "show", id: estrategiaPruebaInstance.id)
        }
        else {
            render(view: "create", model: [estrategiaPruebaInstance: estrategiaPruebaInstance])
        }
    }

    def show = {
        def estrategiaPruebaInstance = EstrategiaPrueba.get(params.id)
        if (!estrategiaPruebaInstance) {
            flash.message = "${message(code: 'default.not.found.message', args: [message(code: 'estrategiaPrueba.label', default: 'EstrategiaPrueba'), params.id])}"
            redirect(action: "list")
        }
        else {
            [estrategiaPruebaInstance: estrategiaPruebaInstance]
        }
    }

    def edit = {
        def estrategiaPruebaInstance = EstrategiaPrueba.get(params.id)
        if (!estrategiaPruebaInstance) {
            flash.message = "${message(code: 'default.not.found.message', args: [message(code: 'estrategiaPrueba.label', default: 'EstrategiaPrueba'), params.id])}"
            redirect(action: "list")
        }
        else {
            return [estrategiaPruebaInstance: estrategiaPruebaInstance]
        }
    }

    def update = {
        def estrategiaPruebaInstance = EstrategiaPrueba.get(params.id)
        if (estrategiaPruebaInstance) {
            if (params.version) {
                def version = params.version.toLong()
                if (estrategiaPruebaInstance.version > version) {
                    
                    estrategiaPruebaInstance.errors.rejectValue("version", "default.optimistic.locking.failure", [message(code: 'estrategiaPrueba.label', default: 'EstrategiaPrueba')] as Object[], "Another user has updated this EstrategiaPrueba while you were editing")
                    render(view: "edit", model: [estrategiaPruebaInstance: estrategiaPruebaInstance])
                    return
                }
            }
            estrategiaPruebaInstance.properties = params
            if (!estrategiaPruebaInstance.hasErrors() && estrategiaPruebaInstance.save(flush: true)) {
                flash.message = "${message(code: 'default.updated.message', args: [message(code: 'estrategiaPrueba.label', default: 'EstrategiaPrueba'), estrategiaPruebaInstance.id])}"
                redirect(action: "show", id: estrategiaPruebaInstance.id)
            }
            else {
                render(view: "edit", model: [estrategiaPruebaInstance: estrategiaPruebaInstance])
            }
        }
        else {
            flash.message = "${message(code: 'default.not.found.message', args: [message(code: 'estrategiaPrueba.label', default: 'EstrategiaPrueba'), params.id])}"
            redirect(action: "list")
        }
    }

    def delete = {
        def estrategiaPruebaInstance = EstrategiaPrueba.get(params.id)
        if (estrategiaPruebaInstance) {
            try {
                estrategiaPruebaInstance.delete(flush: true)
                flash.message = "${message(code: 'default.deleted.message', args: [message(code: 'estrategiaPrueba.label', default: 'EstrategiaPrueba'), params.id])}"
                redirect(action: "list")
            }
            catch (org.springframework.dao.DataIntegrityViolationException e) {
                flash.message = "${message(code: 'default.not.deleted.message', args: [message(code: 'estrategiaPrueba.label', default: 'EstrategiaPrueba'), params.id])}"
                redirect(action: "show", id: params.id)
            }
        }
        else {
            flash.message = "${message(code: 'default.not.found.message', args: [message(code: 'estrategiaPrueba.label', default: 'EstrategiaPrueba'), params.id])}"
            redirect(action: "list")
        }
    }
}
