package ar.com.telecom.pcs.entities

class TipoAnexoController {
	
	static adminMenu = true

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    def index = {
        redirect(action: "list", params: params)
    }

    def list = {
        params.max = Math.min(params.max ? params.int('max') : 10, 100)
        [tipoAnexoInstanceList: TipoAnexo.list(params), tipoAnexoInstanceTotal: TipoAnexo.count()]
    }

    def create = {
        def tipoAnexoInstance = new TipoAnexo()
        tipoAnexoInstance.properties = params
        return [tipoAnexoInstance: tipoAnexoInstance]
    }

    def save = {
        def tipoAnexoInstance = new TipoAnexo(params)
        if (tipoAnexoInstance.save(flush: true)) {
            flash.message = "${message(code: 'default.created.message', args: [message(code: 'tipoAnexo.label', default: 'TipoAnexo'), tipoAnexoInstance.id])}"
            redirect(action: "show", id: tipoAnexoInstance.id)
        }
        else {
            render(view: "create", model: [tipoAnexoInstance: tipoAnexoInstance])
        }
    }

    def show = {
        def tipoAnexoInstance = TipoAnexo.get(params.id)
        if (!tipoAnexoInstance) {
            flash.message = "${message(code: 'default.not.found.message', args: [message(code: 'tipoAnexo.label', default: 'TipoAnexo'), params.id])}"
            redirect(action: "list")
        }
        else {
            [tipoAnexoInstance: tipoAnexoInstance]
        }
    }

    def edit = {
        def tipoAnexoInstance = TipoAnexo.get(params.id)
        if (!tipoAnexoInstance) {
            flash.message = "${message(code: 'default.not.found.message', args: [message(code: 'tipoAnexo.label', default: 'TipoAnexo'), params.id])}"
            redirect(action: "list")
        }
        else {
            return [tipoAnexoInstance: tipoAnexoInstance]
        }
    }

    def update = {
        def tipoAnexoInstance = TipoAnexo.get(params.id)
        if (tipoAnexoInstance) {
            if (params.version) {
                def version = params.version.toLong()
                if (tipoAnexoInstance.version > version) {
                    
                    tipoAnexoInstance.errors.rejectValue("version", "default.optimistic.locking.failure", [message(code: 'tipoAnexo.label', default: 'TipoAnexo')] as Object[], "Another user has updated this TipoAnexo while you were editing")
                    render(view: "edit", model: [tipoAnexoInstance: tipoAnexoInstance])
                    return
                }
            }
            tipoAnexoInstance.properties = params
            if (!tipoAnexoInstance.hasErrors() && tipoAnexoInstance.save(flush: true)) {
                flash.message = "${message(code: 'default.updated.message', args: [message(code: 'tipoAnexo.label', default: 'TipoAnexo'), tipoAnexoInstance.id])}"
                redirect(action: "show", id: tipoAnexoInstance.id)
            }
            else {
                render(view: "edit", model: [tipoAnexoInstance: tipoAnexoInstance])
            }
        }
        else {
            flash.message = "${message(code: 'default.not.found.message', args: [message(code: 'tipoAnexo.label', default: 'TipoAnexo'), params.id])}"
            redirect(action: "list")
        }
    }

    def delete = {
        def tipoAnexoInstance = TipoAnexo.get(params.id)
        if (tipoAnexoInstance) {
            try {
                tipoAnexoInstance.delete(flush: true)
                flash.message = "${message(code: 'default.deleted.message', args: [message(code: 'tipoAnexo.label', default: 'TipoAnexo'), params.id])}"
                redirect(action: "list")
            }
            catch (org.springframework.dao.DataIntegrityViolationException e) {
                flash.message = "${message(code: 'default.not.deleted.message', args: [message(code: 'tipoAnexo.label', default: 'TipoAnexo'), params.id])}"
                redirect(action: "show", id: params.id)
            }
        }
        else {
            flash.message = "${message(code: 'default.not.found.message', args: [message(code: 'tipoAnexo.label', default: 'TipoAnexo'), params.id])}"
            redirect(action: "list")
        }
    }
}
