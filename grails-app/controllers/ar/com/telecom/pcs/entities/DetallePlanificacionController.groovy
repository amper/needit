package ar.com.telecom.pcs.entities

class DetallePlanificacionController {
	
	static adminMenu = false

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    def index = {
        redirect(action: "list", params: params)
    }

    def list = {
        params.max = Math.min(params.max ? params.int('max') : 10, 100)
        [detallePlanificacionInstanceList: DetallePlanificacion.list(params), detallePlanificacionInstanceTotal: DetallePlanificacion.count()]
    }

    def create = {
        def detallePlanificacionInstance = new DetallePlanificacion()
        detallePlanificacionInstance.properties = params
        return [detallePlanificacionInstance: detallePlanificacionInstance]
    }

    def save = {
        def detallePlanificacionInstance = new DetallePlanificacion(params)
        if (detallePlanificacionInstance.save(flush: true)) {
            flash.message = "${message(code: 'default.created.message', args: [message(code: 'detallePlanificacion.label', default: 'DetallePlanificacion'), detallePlanificacionInstance.id])}"
            redirect(action: "show", id: detallePlanificacionInstance.id)
        }
        else {
            render(view: "create", model: [detallePlanificacionInstance: detallePlanificacionInstance])
        }
    }

    def show = {
        def detallePlanificacionInstance = DetallePlanificacion.get(params.id)
        if (!detallePlanificacionInstance) {
            flash.message = "${message(code: 'default.not.found.message', args: [message(code: 'detallePlanificacion.label', default: 'DetallePlanificacion'), params.id])}"
            redirect(action: "list")
        }
        else {
            [detallePlanificacionInstance: detallePlanificacionInstance]
        }
    }

    def edit = {
        def detallePlanificacionInstance = DetallePlanificacion.get(params.id)
        if (!detallePlanificacionInstance) {
            flash.message = "${message(code: 'default.not.found.message', args: [message(code: 'detallePlanificacion.label', default: 'DetallePlanificacion'), params.id])}"
            redirect(action: "list")
        }
        else {
            return [detallePlanificacionInstance: detallePlanificacionInstance]
        }
    }

    def update = {
        def detallePlanificacionInstance = DetallePlanificacion.get(params.id)
        if (detallePlanificacionInstance) {
            if (params.version) {
                def version = params.version.toLong()
                if (detallePlanificacionInstance.version > version) {
                    
                    detallePlanificacionInstance.errors.rejectValue("version", "default.optimistic.locking.failure", [message(code: 'detallePlanificacion.label', default: 'DetallePlanificacion')] as Object[], "Another user has updated this DetallePlanificacion while you were editing")
                    render(view: "edit", model: [detallePlanificacionInstance: detallePlanificacionInstance])
                    return
                }
            }
            detallePlanificacionInstance.properties = params
            if (!detallePlanificacionInstance.hasErrors() && detallePlanificacionInstance.save(flush: true)) {
                flash.message = "${message(code: 'default.updated.message', args: [message(code: 'detallePlanificacion.label', default: 'DetallePlanificacion'), detallePlanificacionInstance.id])}"
                redirect(action: "show", id: detallePlanificacionInstance.id)
            }
            else {
                render(view: "edit", model: [detallePlanificacionInstance: detallePlanificacionInstance])
            }
        }
        else {
            flash.message = "${message(code: 'default.not.found.message', args: [message(code: 'detallePlanificacion.label', default: 'DetallePlanificacion'), params.id])}"
            redirect(action: "list")
        }
    }

    def delete = {
        def detallePlanificacionInstance = DetallePlanificacion.get(params.id)
        if (detallePlanificacionInstance) {
            try {
                detallePlanificacionInstance.delete(flush: true)
                flash.message = "${message(code: 'default.deleted.message', args: [message(code: 'detallePlanificacion.label', default: 'DetallePlanificacion'), params.id])}"
                redirect(action: "list")
            }
            catch (org.springframework.dao.DataIntegrityViolationException e) {
                flash.message = "${message(code: 'default.not.deleted.message', args: [message(code: 'detallePlanificacion.label', default: 'DetallePlanificacion'), params.id])}"
                redirect(action: "show", id: params.id)
            }
        }
        else {
            flash.message = "${message(code: 'default.not.found.message', args: [message(code: 'detallePlanificacion.label', default: 'DetallePlanificacion'), params.id])}"
            redirect(action: "list")
        }
    }
}
