package ar.com.telecom.pcs.entities

class AuditoriaController {

	static adminMenu = true

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    def index = {
        redirect(action: "list", params: params)
    }

    def list = {
        params.max = Math.min(params.max ? params.int('max') : 10, 100)
        [auditoriaInstanceList: Auditoria.list(params), auditoriaInstanceTotal: Auditoria.count()]
    }

    def create = {
        def auditoriaInstance = new Auditoria()
        auditoriaInstance.properties = params
        return [auditoriaInstance: auditoriaInstance]
    }

    def save = {
        def auditoriaInstance = new Auditoria(params)
        if (auditoriaInstance.save(flush: true)) {
            flash.message = "${message(code: 'default.created.message', args: [message(code: 'auditoria.label', default: 'Auditoria'), auditoriaInstance.id])}"
            redirect(action: "show", id: auditoriaInstance.id)
        }
        else {
            render(view: "create", model: [auditoriaInstance: auditoriaInstance])
        }
    }

    def show = {
        def auditoriaInstance = Auditoria.get(params.id)
        if (!auditoriaInstance) {
            flash.message = "${message(code: 'default.not.found.message', args: [message(code: 'auditoria.label', default: 'Auditoria'), params.id])}"
            redirect(action: "list")
        }
        else {
            [auditoriaInstance: auditoriaInstance]
        }
    }

    def edit = {
        def auditoriaInstance = Auditoria.get(params.id)
        if (!auditoriaInstance) {
            flash.message = "${message(code: 'default.not.found.message', args: [message(code: 'auditoria.label', default: 'Auditoria'), params.id])}"
            redirect(action: "list")
        }
        else {
            return [auditoriaInstance: auditoriaInstance]
        }
    }

    def update = {
        def auditoriaInstance = Auditoria.get(params.id)
        if (auditoriaInstance) {
            if (params.version) {
                def version = params.version.toLong()
                if (auditoriaInstance.version > version) {
                    
                    auditoriaInstance.errors.rejectValue("version", "default.optimistic.locking.failure", [message(code: 'auditoria.label', default: 'Auditoria')] as Object[], "Another user has updated this Auditoria while you were editing")
                    render(view: "edit", model: [auditoriaInstance: auditoriaInstance])
                    return
                }
            }
            auditoriaInstance.properties = params
            if (!auditoriaInstance.hasErrors() && auditoriaInstance.save(flush: true)) {
                flash.message = "${message(code: 'default.updated.message', args: [message(code: 'auditoria.label', default: 'Auditoria'), auditoriaInstance.id])}"
                redirect(action: "show", id: auditoriaInstance.id)
            }
            else {
                render(view: "edit", model: [auditoriaInstance: auditoriaInstance])
            }
        }
        else {
            flash.message = "${message(code: 'default.not.found.message', args: [message(code: 'auditoria.label', default: 'Auditoria'), params.id])}"
            redirect(action: "list")
        }
    }

    def delete = {
        def auditoriaInstance = Auditoria.get(params.id)
        if (auditoriaInstance) {
            try {
                auditoriaInstance.delete(flush: true)
                flash.message = "${message(code: 'default.deleted.message', args: [message(code: 'auditoria.label', default: 'Auditoria'), params.id])}"
                redirect(action: "list")
            }
            catch (org.springframework.dao.DataIntegrityViolationException e) {
                flash.message = "${message(code: 'default.not.deleted.message', args: [message(code: 'auditoria.label', default: 'Auditoria'), params.id])}"
                redirect(action: "show", id: params.id)
            }
        }
        else {
            flash.message = "${message(code: 'default.not.found.message', args: [message(code: 'auditoria.label', default: 'Auditoria'), params.id])}"
            redirect(action: "list")
        }
    }
}
