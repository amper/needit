package ar.com.telecom.pcs.entities

class DetalleEstrategiaPruebaController {
	
	static adminMenu = false

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    def index = {
        redirect(action: "list", params: params)
    }

    def list = {
        params.max = Math.min(params.max ? params.int('max') : 10, 100)
        [detalleEstrategiaPruebaInstanceList: DetalleEstrategiaPrueba.list(params), detalleEstrategiaPruebaInstanceTotal: DetalleEstrategiaPrueba.count()]
    }

    def create = {
        def detalleEstrategiaPruebaInstance = new DetalleEstrategiaPrueba()
        detalleEstrategiaPruebaInstance.properties = params
        return [detalleEstrategiaPruebaInstance: detalleEstrategiaPruebaInstance]
    }

    def save = {
        def detalleEstrategiaPruebaInstance = new DetalleEstrategiaPrueba(params)
        if (detalleEstrategiaPruebaInstance.save(flush: true)) {
            flash.message = "${message(code: 'default.created.message', args: [message(code: 'detalleEstrategiaPrueba.label', default: 'DetalleEstrategiaPrueba'), detalleEstrategiaPruebaInstance.id])}"
            redirect(action: "show", id: detalleEstrategiaPruebaInstance.id)
        }
        else {
            render(view: "create", model: [detalleEstrategiaPruebaInstance: detalleEstrategiaPruebaInstance])
        }
    }

    def show = {
        def detalleEstrategiaPruebaInstance = DetalleEstrategiaPrueba.get(params.id)
        if (!detalleEstrategiaPruebaInstance) {
            flash.message = "${message(code: 'default.not.found.message', args: [message(code: 'detalleEstrategiaPrueba.label', default: 'DetalleEstrategiaPrueba'), params.id])}"
            redirect(action: "list")
        }
        else {
            [detalleEstrategiaPruebaInstance: detalleEstrategiaPruebaInstance]
        }
    }

    def edit = {
        def detalleEstrategiaPruebaInstance = DetalleEstrategiaPrueba.get(params.id)
        if (!detalleEstrategiaPruebaInstance) {
            flash.message = "${message(code: 'default.not.found.message', args: [message(code: 'detalleEstrategiaPrueba.label', default: 'DetalleEstrategiaPrueba'), params.id])}"
            redirect(action: "list")
        }
        else {
            return [detalleEstrategiaPruebaInstance: detalleEstrategiaPruebaInstance]
        }
    }

    def update = {
        def detalleEstrategiaPruebaInstance = DetalleEstrategiaPrueba.get(params.id)
        if (detalleEstrategiaPruebaInstance) {
            if (params.version) {
                def version = params.version.toLong()
                if (detalleEstrategiaPruebaInstance.version > version) {
                    
                    detalleEstrategiaPruebaInstance.errors.rejectValue("version", "default.optimistic.locking.failure", [message(code: 'detalleEstrategiaPrueba.label', default: 'DetalleEstrategiaPrueba')] as Object[], "Another user has updated this DetalleEstrategiaPrueba while you were editing")
                    render(view: "edit", model: [detalleEstrategiaPruebaInstance: detalleEstrategiaPruebaInstance])
                    return
                }
            }
            detalleEstrategiaPruebaInstance.properties = params
            if (!detalleEstrategiaPruebaInstance.hasErrors() && detalleEstrategiaPruebaInstance.save(flush: true)) {
                flash.message = "${message(code: 'default.updated.message', args: [message(code: 'detalleEstrategiaPrueba.label', default: 'DetalleEstrategiaPrueba'), detalleEstrategiaPruebaInstance.id])}"
                redirect(action: "show", id: detalleEstrategiaPruebaInstance.id)
            }
            else {
                render(view: "edit", model: [detalleEstrategiaPruebaInstance: detalleEstrategiaPruebaInstance])
            }
        }
        else {
            flash.message = "${message(code: 'default.not.found.message', args: [message(code: 'detalleEstrategiaPrueba.label', default: 'DetalleEstrategiaPrueba'), params.id])}"
            redirect(action: "list")
        }
    }

    def delete = {
        def detalleEstrategiaPruebaInstance = DetalleEstrategiaPrueba.get(params.id)
        if (detalleEstrategiaPruebaInstance) {
            try {
                detalleEstrategiaPruebaInstance.delete(flush: true)
                flash.message = "${message(code: 'default.deleted.message', args: [message(code: 'detalleEstrategiaPrueba.label', default: 'DetalleEstrategiaPrueba'), params.id])}"
                redirect(action: "list")
            }
            catch (org.springframework.dao.DataIntegrityViolationException e) {
                flash.message = "${message(code: 'default.not.deleted.message', args: [message(code: 'detalleEstrategiaPrueba.label', default: 'DetalleEstrategiaPrueba'), params.id])}"
                redirect(action: "show", id: params.id)
            }
        }
        else {
            flash.message = "${message(code: 'default.not.found.message', args: [message(code: 'detalleEstrategiaPrueba.label', default: 'DetalleEstrategiaPrueba'), params.id])}"
            redirect(action: "list")
        }
    }
}
