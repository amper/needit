package ar.com.telecom.pcs.entities

class ValidacionCamposActividadController {
	
	static adminMenu = true

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    def index = {
        redirect(action: "list", params: params)
    }

    def list = {
        params.max = Math.min(params.max ? params.int('max') : 10, 100)
        [validacionCamposActividadInstanceList: ValidacionCamposActividad.list(params), validacionCamposActividadInstanceTotal: ValidacionCamposActividad.count()]
    }

    def create = {
        def validacionCamposActividadInstance = new ValidacionCamposActividad()
        validacionCamposActividadInstance.properties = params
        return [validacionCamposActividadInstance: validacionCamposActividadInstance]
    }

    def save = {
        def validacionCamposActividadInstance = new ValidacionCamposActividad(params)
        if (validacionCamposActividadInstance.save(flush: true)) {
            flash.message = "${message(code: 'default.created.message', args: [message(code: 'validacionCamposActividad.label', default: 'ValidacionCamposActividad'), validacionCamposActividadInstance.id])}"
            redirect(action: "show", id: validacionCamposActividadInstance.id)
        }
        else {
            render(view: "create", model: [validacionCamposActividadInstance: validacionCamposActividadInstance])
        }
    }

    def show = {
        def validacionCamposActividadInstance = ValidacionCamposActividad.get(params.id)
        if (!validacionCamposActividadInstance) {
            flash.message = "${message(code: 'default.not.found.message', args: [message(code: 'validacionCamposActividad.label', default: 'ValidacionCamposActividad'), params.id])}"
            redirect(action: "list")
        }
        else {
            [validacionCamposActividadInstance: validacionCamposActividadInstance]
        }
    }

    def edit = {
        def validacionCamposActividadInstance = ValidacionCamposActividad.get(params.id)
        if (!validacionCamposActividadInstance) {
            flash.message = "${message(code: 'default.not.found.message', args: [message(code: 'validacionCamposActividad.label', default: 'ValidacionCamposActividad'), params.id])}"
            redirect(action: "list")
        }
        else {
            return [validacionCamposActividadInstance: validacionCamposActividadInstance]
        }
    }

    def update = {
        def validacionCamposActividadInstance = ValidacionCamposActividad.get(params.id)
        if (validacionCamposActividadInstance) {
            if (params.version) {
                def version = params.version.toLong()
                if (validacionCamposActividadInstance.version > version) {
                    
                    validacionCamposActividadInstance.errors.rejectValue("version", "default.optimistic.locking.failure", [message(code: 'validacionCamposActividad.label', default: 'ValidacionCamposActividad')] as Object[], "Another user has updated this ValidacionCamposActividad while you were editing")
                    render(view: "edit", model: [validacionCamposActividadInstance: validacionCamposActividadInstance])
                    return
                }
            }
            validacionCamposActividadInstance.properties = params
            if (!validacionCamposActividadInstance.hasErrors() && validacionCamposActividadInstance.save(flush: true)) {
                flash.message = "${message(code: 'default.updated.message', args: [message(code: 'validacionCamposActividad.label', default: 'ValidacionCamposActividad'), validacionCamposActividadInstance.id])}"
                redirect(action: "show", id: validacionCamposActividadInstance.id)
            }
            else {
                render(view: "edit", model: [validacionCamposActividadInstance: validacionCamposActividadInstance])
            }
        }
        else {
            flash.message = "${message(code: 'default.not.found.message', args: [message(code: 'validacionCamposActividad.label', default: 'ValidacionCamposActividad'), params.id])}"
            redirect(action: "list")
        }
    }

    def delete = {
        def validacionCamposActividadInstance = ValidacionCamposActividad.get(params.id)
        if (validacionCamposActividadInstance) {
            try {
                validacionCamposActividadInstance.delete(flush: true)
                flash.message = "${message(code: 'default.deleted.message', args: [message(code: 'validacionCamposActividad.label', default: 'ValidacionCamposActividad'), params.id])}"
                redirect(action: "list")
            }
            catch (org.springframework.dao.DataIntegrityViolationException e) {
                flash.message = "${message(code: 'default.not.deleted.message', args: [message(code: 'validacionCamposActividad.label', default: 'ValidacionCamposActividad'), params.id])}"
                redirect(action: "show", id: params.id)
            }
        }
        else {
            flash.message = "${message(code: 'default.not.found.message', args: [message(code: 'validacionCamposActividad.label', default: 'ValidacionCamposActividad'), params.id])}"
            redirect(action: "list")
        }
    }
}
