package ar.com.telecom.pcs.entities

class AreaSoporteAprobarFaseController {
	
	static adminMenu = true

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    def index = {
        redirect(action: "list", params: params)
    }

    def list = {
        params.max = Math.min(params.max ? params.int('max') : 10, 100)
        [areaSoporteAprobarFaseInstanceList: AreaSoporteAprobarFase.list(params), areaSoporteAprobarFaseInstanceTotal: AreaSoporteAprobarFase.count()]
    }

    def create = {
        def areaSoporteAprobarFaseInstance = new AreaSoporteAprobarFase()
        areaSoporteAprobarFaseInstance.properties = params
        return [areaSoporteAprobarFaseInstance: areaSoporteAprobarFaseInstance]
    }

    def save = {
        def areaSoporteAprobarFaseInstance = new AreaSoporteAprobarFase(params)
        if (areaSoporteAprobarFaseInstance.save(flush: true)) {
            flash.message = "${message(code: 'default.created.message', args: [message(code: 'areaSoporteAprobarFase.label', default: 'AreaSoporteAprobarFase'), areaSoporteAprobarFaseInstance.id])}"
            redirect(action: "show", id: areaSoporteAprobarFaseInstance.id)
        }
        else {
            render(view: "create", model: [areaSoporteAprobarFaseInstance: areaSoporteAprobarFaseInstance])
        }
    }

    def show = {
        def areaSoporteAprobarFaseInstance = AreaSoporteAprobarFase.get(params.id)
        if (!areaSoporteAprobarFaseInstance) {
            flash.message = "${message(code: 'default.not.found.message', args: [message(code: 'areaSoporteAprobarFase.label', default: 'AreaSoporteAprobarFase'), params.id])}"
            redirect(action: "list")
        }
        else {
            [areaSoporteAprobarFaseInstance: areaSoporteAprobarFaseInstance]
        }
    }

    def edit = {
        def areaSoporteAprobarFaseInstance = AreaSoporteAprobarFase.get(params.id)
        if (!areaSoporteAprobarFaseInstance) {
            flash.message = "${message(code: 'default.not.found.message', args: [message(code: 'areaSoporteAprobarFase.label', default: 'AreaSoporteAprobarFase'), params.id])}"
            redirect(action: "list")
        }
        else {
            return [areaSoporteAprobarFaseInstance: areaSoporteAprobarFaseInstance]
        }
    }

    def update = {
        def areaSoporteAprobarFaseInstance = AreaSoporteAprobarFase.get(params.id)
        if (areaSoporteAprobarFaseInstance) {
            if (params.version) {
                def version = params.version.toLong()
                if (areaSoporteAprobarFaseInstance.version > version) {
                    
                    areaSoporteAprobarFaseInstance.errors.rejectValue("version", "default.optimistic.locking.failure", [message(code: 'areaSoporteAprobarFase.label', default: 'AreaSoporteAprobarFase')] as Object[], "Another user has updated this AreaSoporteAprobarFase while you were editing")
                    render(view: "edit", model: [areaSoporteAprobarFaseInstance: areaSoporteAprobarFaseInstance])
                    return
                }
            }
            areaSoporteAprobarFaseInstance.properties = params
            if (!areaSoporteAprobarFaseInstance.hasErrors() && areaSoporteAprobarFaseInstance.save(flush: true)) {
                flash.message = "${message(code: 'default.updated.message', args: [message(code: 'areaSoporteAprobarFase.label', default: 'AreaSoporteAprobarFase'), areaSoporteAprobarFaseInstance.id])}"
                redirect(action: "show", id: areaSoporteAprobarFaseInstance.id)
            }
            else {
                render(view: "edit", model: [areaSoporteAprobarFaseInstance: areaSoporteAprobarFaseInstance])
            }
        }
        else {
            flash.message = "${message(code: 'default.not.found.message', args: [message(code: 'areaSoporteAprobarFase.label', default: 'AreaSoporteAprobarFase'), params.id])}"
            redirect(action: "list")
        }
    }

    def delete = {
        def areaSoporteAprobarFaseInstance = AreaSoporteAprobarFase.get(params.id)
        if (areaSoporteAprobarFaseInstance) {
            try {
                areaSoporteAprobarFaseInstance.delete(flush: true)
                flash.message = "${message(code: 'default.deleted.message', args: [message(code: 'areaSoporteAprobarFase.label', default: 'AreaSoporteAprobarFase'), params.id])}"
                redirect(action: "list")
            }
            catch (org.springframework.dao.DataIntegrityViolationException e) {
                flash.message = "${message(code: 'default.not.deleted.message', args: [message(code: 'areaSoporteAprobarFase.label', default: 'AreaSoporteAprobarFase'), params.id])}"
                redirect(action: "show", id: params.id)
            }
        }
        else {
            flash.message = "${message(code: 'default.not.found.message', args: [message(code: 'areaSoporteAprobarFase.label', default: 'AreaSoporteAprobarFase'), params.id])}"
            redirect(action: "list")
        }
    }
}
