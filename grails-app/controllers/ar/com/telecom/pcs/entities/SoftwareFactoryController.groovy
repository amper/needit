package ar.com.telecom.pcs.entities

class SoftwareFactoryController {
	
	static adminMenu = true

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    def index = {
        redirect(action: "list", params: params)
    }

    def list = {
        params.max = Math.min(params.max ? params.int('max') : 10, 100)
        [softwareFactoryInstanceList: SoftwareFactory.list(params), softwareFactoryInstanceTotal: SoftwareFactory.count()]
    }

    def create = {
        def softwareFactoryInstance = new SoftwareFactory()
        softwareFactoryInstance.properties = params
        return [softwareFactoryInstance: softwareFactoryInstance]
    }

    def save = {
        def softwareFactoryInstance = new SoftwareFactory(params)
        if (softwareFactoryInstance.save(flush: true)) {
            flash.message = "${message(code: 'default.created.message', args: [message(code: 'softwareFactory.label', default: 'SoftwareFactory'), softwareFactoryInstance.id])}"
            redirect(action: "show", id: softwareFactoryInstance.id)
        }
        else {
            render(view: "create", model: [softwareFactoryInstance: softwareFactoryInstance])
        }
    }

    def show = {
        def softwareFactoryInstance = SoftwareFactory.get(params.id)
        if (!softwareFactoryInstance) {
            flash.message = "${message(code: 'default.not.found.message', args: [message(code: 'softwareFactory.label', default: 'SoftwareFactory'), params.id])}"
            redirect(action: "list")
        }
        else {
            [softwareFactoryInstance: softwareFactoryInstance]
        }
    }

    def edit = {
        def softwareFactoryInstance = SoftwareFactory.get(params.id)
        if (!softwareFactoryInstance) {
            flash.message = "${message(code: 'default.not.found.message', args: [message(code: 'softwareFactory.label', default: 'SoftwareFactory'), params.id])}"
            redirect(action: "list")
        }
        else {
            return [softwareFactoryInstance: softwareFactoryInstance]
        }
    }

    def update = {
        def softwareFactoryInstance = SoftwareFactory.get(params.id)
        if (softwareFactoryInstance) {
            if (params.version) {
                def version = params.version.toLong()
                if (softwareFactoryInstance.version > version) {
                    
                    softwareFactoryInstance.errors.rejectValue("version", "default.optimistic.locking.failure", [message(code: 'softwareFactory.label', default: 'SoftwareFactory')] as Object[], "Another user has updated this SoftwareFactory while you were editing")
                    render(view: "edit", model: [softwareFactoryInstance: softwareFactoryInstance])
                    return
                }
            }
            softwareFactoryInstance.properties = params
            if (!softwareFactoryInstance.hasErrors() && softwareFactoryInstance.save(flush: true)) {
                flash.message = "${message(code: 'default.updated.message', args: [message(code: 'softwareFactory.label', default: 'SoftwareFactory'), softwareFactoryInstance.id])}"
                redirect(action: "show", id: softwareFactoryInstance.id)
            }
            else {
                render(view: "edit", model: [softwareFactoryInstance: softwareFactoryInstance])
            }
        }
        else {
            flash.message = "${message(code: 'default.not.found.message', args: [message(code: 'softwareFactory.label', default: 'SoftwareFactory'), params.id])}"
            redirect(action: "list")
        }
    }

    def delete = {
        def softwareFactoryInstance = SoftwareFactory.get(params.id)
        if (softwareFactoryInstance) {
            try {
                softwareFactoryInstance.delete(flush: true)
                flash.message = "${message(code: 'default.deleted.message', args: [message(code: 'softwareFactory.label', default: 'SoftwareFactory'), params.id])}"
                redirect(action: "list")
            }
            catch (org.springframework.dao.DataIntegrityViolationException e) {
                flash.message = "${message(code: 'default.not.deleted.message', args: [message(code: 'softwareFactory.label', default: 'SoftwareFactory'), params.id])}"
                redirect(action: "show", id: params.id)
            }
        }
        else {
            flash.message = "${message(code: 'default.not.found.message', args: [message(code: 'softwareFactory.label', default: 'SoftwareFactory'), params.id])}"
            redirect(action: "list")
        }
    }
}
