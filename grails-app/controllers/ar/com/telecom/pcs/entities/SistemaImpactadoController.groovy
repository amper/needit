package ar.com.telecom.pcs.entities

class SistemaImpactadoController {
	
	static adminMenu = false

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    def index = {
        redirect(action: "list", params: params)
    }

    def list = {
        params.max = Math.min(params.max ? params.int('max') : 10, 100)
        [sistemaImpactadoInstanceList: SistemaImpactado.list(params), sistemaImpactadoInstanceTotal: SistemaImpactado.count()]
    }

    def create = {
        def sistemaImpactadoInstance = new SistemaImpactado()
        sistemaImpactadoInstance.properties = params
        return [sistemaImpactadoInstance: sistemaImpactadoInstance]
    }

    def save = {
        def sistemaImpactadoInstance = new SistemaImpactado(params)
        if (sistemaImpactadoInstance.save(flush: true)) {
            flash.message = "${message(code: 'default.created.message', args: [message(code: 'sistemaImpactado.label', default: 'SistemaImpactado'), sistemaImpactadoInstance.id])}"
            redirect(action: "show", id: sistemaImpactadoInstance.id)
        }
        else {
            render(view: "create", model: [sistemaImpactadoInstance: sistemaImpactadoInstance])
        }
    }

    def show = {
        def sistemaImpactadoInstance = SistemaImpactado.get(params.id)
        if (!sistemaImpactadoInstance) {
            flash.message = "${message(code: 'default.not.found.message', args: [message(code: 'sistemaImpactado.label', default: 'SistemaImpactado'), params.id])}"
            redirect(action: "list")
        }
        else {
            [sistemaImpactadoInstance: sistemaImpactadoInstance]
        }
    }

    def edit = {
        def sistemaImpactadoInstance = SistemaImpactado.get(params.id)
        if (!sistemaImpactadoInstance) {
            flash.message = "${message(code: 'default.not.found.message', args: [message(code: 'sistemaImpactado.label', default: 'SistemaImpactado'), params.id])}"
            redirect(action: "list")
        }
        else {
            return [sistemaImpactadoInstance: sistemaImpactadoInstance]
        }
    }

    def update = {
        def sistemaImpactadoInstance = SistemaImpactado.get(params.id)
        if (sistemaImpactadoInstance) {
            if (params.version) {
                def version = params.version.toLong()
                if (sistemaImpactadoInstance.version > version) {
                    
                    sistemaImpactadoInstance.errors.rejectValue("version", "default.optimistic.locking.failure", [message(code: 'sistemaImpactado.label', default: 'SistemaImpactado')] as Object[], "Another user has updated this SistemaImpactado while you were editing")
                    render(view: "edit", model: [sistemaImpactadoInstance: sistemaImpactadoInstance])
                    return
                }
            }
            sistemaImpactadoInstance.properties = params
            if (!sistemaImpactadoInstance.hasErrors() && sistemaImpactadoInstance.save(flush: true)) {
                flash.message = "${message(code: 'default.updated.message', args: [message(code: 'sistemaImpactado.label', default: 'SistemaImpactado'), sistemaImpactadoInstance.id])}"
                redirect(action: "show", id: sistemaImpactadoInstance.id)
            }
            else {
                render(view: "edit", model: [sistemaImpactadoInstance: sistemaImpactadoInstance])
            }
        }
        else {
            flash.message = "${message(code: 'default.not.found.message', args: [message(code: 'sistemaImpactado.label', default: 'SistemaImpactado'), params.id])}"
            redirect(action: "list")
        }
    }

    def delete = {
        def sistemaImpactadoInstance = SistemaImpactado.get(params.id)
        if (sistemaImpactadoInstance) {
            try {
                sistemaImpactadoInstance.delete(flush: true)
                flash.message = "${message(code: 'default.deleted.message', args: [message(code: 'sistemaImpactado.label', default: 'SistemaImpactado'), params.id])}"
                redirect(action: "list")
            }
            catch (org.springframework.dao.DataIntegrityViolationException e) {
                flash.message = "${message(code: 'default.not.deleted.message', args: [message(code: 'sistemaImpactado.label', default: 'SistemaImpactado'), params.id])}"
                redirect(action: "show", id: params.id)
            }
        }
        else {
            flash.message = "${message(code: 'default.not.found.message', args: [message(code: 'sistemaImpactado.label', default: 'SistemaImpactado'), params.id])}"
            redirect(action: "list")
        }
    }
}
