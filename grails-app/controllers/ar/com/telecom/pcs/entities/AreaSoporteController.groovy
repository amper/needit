package ar.com.telecom.pcs.entities

class AreaSoporteController {
	
	static adminMenu = true

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    def index = {
        redirect(action: "list", params: params)
    }

    def list = {
        params.max = Math.min(params.max ? params.int('max') : 10, 100)
        [areaSoporteInstanceList: AreaSoporte.list(params), areaSoporteInstanceTotal: AreaSoporte.count()]
    }

    def create = {
        def areaSoporteInstance = new AreaSoporte()
        areaSoporteInstance.properties = params
        return [areaSoporteInstance: areaSoporteInstance]
    }

    def save = {
        def areaSoporteInstance = new AreaSoporte(params)
        if (areaSoporteInstance.save(flush: true)) {
            flash.message = "${message(code: 'default.created.message', args: [message(code: 'areaSoporte.label', default: 'AreaSoporte'), areaSoporteInstance.id])}"
            redirect(action: "show", id: areaSoporteInstance.id)
        }
        else {
            render(view: "create", model: [areaSoporteInstance: areaSoporteInstance])
        }
    }

    def show = {
        def areaSoporteInstance = AreaSoporte.get(params.id)
        if (!areaSoporteInstance) {
            flash.message = "${message(code: 'default.not.found.message', args: [message(code: 'areaSoporte.label', default: 'AreaSoporte'), params.id])}"
            redirect(action: "list")
        }
        else {
            [areaSoporteInstance: areaSoporteInstance]
        }
    }

    def edit = {
        def areaSoporteInstance = AreaSoporte.get(params.id)
        if (!areaSoporteInstance) {
            flash.message = "${message(code: 'default.not.found.message', args: [message(code: 'areaSoporte.label', default: 'AreaSoporte'), params.id])}"
            redirect(action: "list")
        }
        else {
            return [areaSoporteInstance: areaSoporteInstance]
        }
    }

    def update = {
        def areaSoporteInstance = AreaSoporte.get(params.id)
        if (areaSoporteInstance) {
            if (params.version) {
                def version = params.version.toLong()
                if (areaSoporteInstance.version > version) {
                    
                    areaSoporteInstance.errors.rejectValue("version", "default.optimistic.locking.failure", [message(code: 'areaSoporte.label', default: 'AreaSoporte')] as Object[], "Another user has updated this AreaSoporte while you were editing")
                    render(view: "edit", model: [areaSoporteInstance: areaSoporteInstance])
                    return
                }
            }
            areaSoporteInstance.properties = params
            if (!areaSoporteInstance.hasErrors() && areaSoporteInstance.save(flush: true)) {
                flash.message = "${message(code: 'default.updated.message', args: [message(code: 'areaSoporte.label', default: 'AreaSoporte'), areaSoporteInstance.id])}"
                redirect(action: "show", id: areaSoporteInstance.id)
            }
            else {
                render(view: "edit", model: [areaSoporteInstance: areaSoporteInstance])
            }
        }
        else {
            flash.message = "${message(code: 'default.not.found.message', args: [message(code: 'areaSoporte.label', default: 'AreaSoporte'), params.id])}"
            redirect(action: "list")
        }
    }

    def delete = {
        def areaSoporteInstance = AreaSoporte.get(params.id)
        if (areaSoporteInstance) {
            try {
                areaSoporteInstance.delete(flush: true)
                flash.message = "${message(code: 'default.deleted.message', args: [message(code: 'areaSoporte.label', default: 'AreaSoporte'), params.id])}"
                redirect(action: "list")
            }
            catch (org.springframework.dao.DataIntegrityViolationException e) {
                flash.message = "${message(code: 'default.not.deleted.message', args: [message(code: 'areaSoporte.label', default: 'AreaSoporte'), params.id])}"
                redirect(action: "show", id: params.id)
            }
        }
        else {
            flash.message = "${message(code: 'default.not.found.message', args: [message(code: 'areaSoporte.label', default: 'AreaSoporte'), params.id])}"
            redirect(action: "list")
        }
    }
}
