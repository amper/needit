package ar.com.telecom.pcs.entities

class JustificacionDisenioExternoController {
	
	static adminMenu = true

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    def index = {
        redirect(action: "list", params: params)
    }

    def list = {
        params.max = Math.min(params.max ? params.int('max') : 10, 100)
        [justificacionDisenioExternoInstanceList: JustificacionDisenioExterno.list(params), justificacionDisenioExternoInstanceTotal: JustificacionDisenioExterno.count()]
    }

    def create = {
        def justificacionDisenioExternoInstance = new JustificacionDisenioExterno()
        justificacionDisenioExternoInstance.properties = params
        return [justificacionDisenioExternoInstance: justificacionDisenioExternoInstance]
    }

    def save = {
        def justificacionDisenioExternoInstance = new JustificacionDisenioExterno(params)
        if (justificacionDisenioExternoInstance.save(flush: true)) {
            flash.message = "${message(code: 'default.created.message', args: [message(code: 'justificacionDisenioExterno.label', default: 'JustificacionDisenioExterno'), justificacionDisenioExternoInstance.id])}"
            redirect(action: "show", id: justificacionDisenioExternoInstance.id)
        }
        else {
            render(view: "create", model: [justificacionDisenioExternoInstance: justificacionDisenioExternoInstance])
        }
    }

    def show = {
        def justificacionDisenioExternoInstance = JustificacionDisenioExterno.get(params.id)
        if (!justificacionDisenioExternoInstance) {
            flash.message = "${message(code: 'default.not.found.message', args: [message(code: 'justificacionDisenioExterno.label', default: 'JustificacionDisenioExterno'), params.id])}"
            redirect(action: "list")
        }
        else {
            [justificacionDisenioExternoInstance: justificacionDisenioExternoInstance]
        }
    }

    def edit = {
        def justificacionDisenioExternoInstance = JustificacionDisenioExterno.get(params.id)
        if (!justificacionDisenioExternoInstance) {
            flash.message = "${message(code: 'default.not.found.message', args: [message(code: 'justificacionDisenioExterno.label', default: 'JustificacionDisenioExterno'), params.id])}"
            redirect(action: "list")
        }
        else {
            return [justificacionDisenioExternoInstance: justificacionDisenioExternoInstance]
        }
    }

    def update = {
        def justificacionDisenioExternoInstance = JustificacionDisenioExterno.get(params.id)
        if (justificacionDisenioExternoInstance) {
            if (params.version) {
                def version = params.version.toLong()
                if (justificacionDisenioExternoInstance.version > version) {
                    
                    justificacionDisenioExternoInstance.errors.rejectValue("version", "default.optimistic.locking.failure", [message(code: 'justificacionDisenioExterno.label', default: 'JustificacionDisenioExterno')] as Object[], "Another user has updated this JustificacionDisenioExterno while you were editing")
                    render(view: "edit", model: [justificacionDisenioExternoInstance: justificacionDisenioExternoInstance])
                    return
                }
            }
            justificacionDisenioExternoInstance.properties = params
            if (!justificacionDisenioExternoInstance.hasErrors() && justificacionDisenioExternoInstance.save(flush: true)) {
                flash.message = "${message(code: 'default.updated.message', args: [message(code: 'justificacionDisenioExterno.label', default: 'JustificacionDisenioExterno'), justificacionDisenioExternoInstance.id])}"
                redirect(action: "show", id: justificacionDisenioExternoInstance.id)
            }
            else {
                render(view: "edit", model: [justificacionDisenioExternoInstance: justificacionDisenioExternoInstance])
            }
        }
        else {
            flash.message = "${message(code: 'default.not.found.message', args: [message(code: 'justificacionDisenioExterno.label', default: 'JustificacionDisenioExterno'), params.id])}"
            redirect(action: "list")
        }
    }

    def delete = {
        def justificacionDisenioExternoInstance = JustificacionDisenioExterno.get(params.id)
        if (justificacionDisenioExternoInstance) {
            try {
                justificacionDisenioExternoInstance.delete(flush: true)
                flash.message = "${message(code: 'default.deleted.message', args: [message(code: 'justificacionDisenioExterno.label', default: 'JustificacionDisenioExterno'), params.id])}"
                redirect(action: "list")
            }
            catch (org.springframework.dao.DataIntegrityViolationException e) {
                flash.message = "${message(code: 'default.not.deleted.message', args: [message(code: 'justificacionDisenioExterno.label', default: 'JustificacionDisenioExterno'), params.id])}"
                redirect(action: "show", id: params.id)
            }
        }
        else {
            flash.message = "${message(code: 'default.not.found.message', args: [message(code: 'justificacionDisenioExterno.label', default: 'JustificacionDisenioExterno'), params.id])}"
            redirect(action: "list")
        }
    }
}
