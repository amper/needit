package ar.com.telecom.pcs.entities

class ParametrizacionTipoPruebaController {
	
	static adminMenu = true

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    def index = {
        redirect(action: "list", params: params)
    }

    def list = {
        params.max = Math.min(params.max ? params.int('max') : 10, 100)
        [parametrizacionTipoPruebaInstanceList: ParametrizacionTipoPrueba.list(params), parametrizacionTipoPruebaInstanceTotal: ParametrizacionTipoPrueba.count()]
    }

    def create = {
        def parametrizacionTipoPruebaInstance = new ParametrizacionTipoPrueba()
        parametrizacionTipoPruebaInstance.properties = params
        return [parametrizacionTipoPruebaInstance: parametrizacionTipoPruebaInstance]
    }

    def save = {
        def parametrizacionTipoPruebaInstance = new ParametrizacionTipoPrueba(params)
        if (parametrizacionTipoPruebaInstance.save(flush: true)) {
            flash.message = "${message(code: 'default.created.message', args: [message(code: 'parametrizacionTipoPrueba.label', default: 'ParametrizacionTipoPrueba'), parametrizacionTipoPruebaInstance.id])}"
            redirect(action: "show", id: parametrizacionTipoPruebaInstance.id)
        }
        else {
            render(view: "create", model: [parametrizacionTipoPruebaInstance: parametrizacionTipoPruebaInstance])
        }
    }

    def show = {
        def parametrizacionTipoPruebaInstance = ParametrizacionTipoPrueba.get(params.id.toLong())
        if (!parametrizacionTipoPruebaInstance) {
            flash.message = "${message(code: 'default.not.found.message', args: [message(code: 'parametrizacionTipoPrueba.label', default: 'ParametrizacionTipoPrueba'), params.id])}"
            redirect(action: "list")
        }
        else {
            [parametrizacionTipoPruebaInstance: parametrizacionTipoPruebaInstance]
        }
    }

    def edit = {
        def parametrizacionTipoPruebaInstance = ParametrizacionTipoPrueba.get(params.id)
        if (!parametrizacionTipoPruebaInstance) {
            flash.message = "${message(code: 'default.not.found.message', args: [message(code: 'parametrizacionTipoPrueba.label', default: 'ParametrizacionTipoPrueba'), params.id])}"
            redirect(action: "list")
        }
        else {
            return [parametrizacionTipoPruebaInstance: parametrizacionTipoPruebaInstance]
        }
    }

    def update = {
        def parametrizacionTipoPruebaInstance = ParametrizacionTipoPrueba.get(params.id)
        if (parametrizacionTipoPruebaInstance) {
            if (params.version) {
                def version = params.version.toLong()
                if (parametrizacionTipoPruebaInstance.version > version) {
                    
                    parametrizacionTipoPruebaInstance.errors.rejectValue("version", "default.optimistic.locking.failure", [message(code: 'parametrizacionTipoPrueba.label', default: 'ParametrizacionTipoPrueba')] as Object[], "Another user has updated this ParametrizacionTipoPrueba while you were editing")
                    render(view: "edit", model: [parametrizacionTipoPruebaInstance: parametrizacionTipoPruebaInstance])
                    return
                }
            }
            parametrizacionTipoPruebaInstance.properties = params
            if (!parametrizacionTipoPruebaInstance.hasErrors() && parametrizacionTipoPruebaInstance.save(flush: true)) {
                flash.message = "${message(code: 'default.updated.message', args: [message(code: 'parametrizacionTipoPrueba.label', default: 'ParametrizacionTipoPrueba'), parametrizacionTipoPruebaInstance.id])}"
                redirect(action: "show", id: parametrizacionTipoPruebaInstance.id)
            }
            else {
                render(view: "edit", model: [parametrizacionTipoPruebaInstance: parametrizacionTipoPruebaInstance])
            }
        }
        else {
            flash.message = "${message(code: 'default.not.found.message', args: [message(code: 'parametrizacionTipoPrueba.label', default: 'ParametrizacionTipoPrueba'), params.id])}"
            redirect(action: "list")
        }
    }

    def delete = {
        def parametrizacionTipoPruebaInstance = ParametrizacionTipoPrueba.get(params.id)
        if (parametrizacionTipoPruebaInstance) {
            try {
                parametrizacionTipoPruebaInstance.delete(flush: true)
                flash.message = "${message(code: 'default.deleted.message', args: [message(code: 'parametrizacionTipoPrueba.label', default: 'ParametrizacionTipoPrueba'), params.id])}"
                redirect(action: "list")
            }
            catch (org.springframework.dao.DataIntegrityViolationException e) {
                flash.message = "${message(code: 'default.not.deleted.message', args: [message(code: 'parametrizacionTipoPrueba.label', default: 'ParametrizacionTipoPrueba'), params.id])}"
                redirect(action: "show", id: params.id)
            }
        }
        else {
            flash.message = "${message(code: 'default.not.found.message', args: [message(code: 'parametrizacionTipoPrueba.label', default: 'ParametrizacionTipoPrueba'), params.id])}"
            redirect(action: "list")
        }
    }
}
