package ar.com.telecom.pcs.entities

class LogModificacionesController {

	static adminMenu = false

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    def index = {
        redirect(action: "list", params: params)
    }

    def list = {
        params.max = Math.min(params.max ? params.int('max') : 10, 100)
        [logModificacionesInstanceList: LogModificaciones.list(params), logModificacionesInstanceTotal: LogModificaciones.count()]
    }

    def create = {
        def logModificacionesInstance = new LogModificaciones()
        logModificacionesInstance.properties = params
        return [logModificacionesInstance: logModificacionesInstance]
    }

    def save = {
        def logModificacionesInstance = new LogModificaciones(params)
        if (logModificacionesInstance.save(flush: true)) {
            flash.message = "${message(code: 'default.created.message', args: [message(code: 'logModificaciones.label', default: 'LogModificaciones'), logModificacionesInstance.id])}"
            redirect(action: "show", id: logModificacionesInstance.id)
        }
        else {
            render(view: "create", model: [logModificacionesInstance: logModificacionesInstance])
        }
    }

    def show = {
        def logModificacionesInstance = LogModificaciones.get(params.id)
        if (!logModificacionesInstance) {
            flash.message = "${message(code: 'default.not.found.message', args: [message(code: 'logModificaciones.label', default: 'LogModificaciones'), params.id])}"
            redirect(action: "list")
        }
        else {
            [logModificacionesInstance: logModificacionesInstance]
        }
    }

    def edit = {
        def logModificacionesInstance = LogModificaciones.get(params.id)
        if (!logModificacionesInstance) {
            flash.message = "${message(code: 'default.not.found.message', args: [message(code: 'logModificaciones.label', default: 'LogModificaciones'), params.id])}"
            redirect(action: "list")
        }
        else {
            return [logModificacionesInstance: logModificacionesInstance]
        }
    }

    def update = {
        def logModificacionesInstance = LogModificaciones.get(params.id)
        if (logModificacionesInstance) {
            if (params.version) {
                def version = params.version.toLong()
                if (logModificacionesInstance.version > version) {
                    
                    logModificacionesInstance.errors.rejectValue("version", "default.optimistic.locking.failure", [message(code: 'logModificaciones.label', default: 'LogModificaciones')] as Object[], "Another user has updated this LogModificaciones while you were editing")
                    render(view: "edit", model: [logModificacionesInstance: logModificacionesInstance])
                    return
                }
            }
            logModificacionesInstance.properties = params
            if (!logModificacionesInstance.hasErrors() && logModificacionesInstance.save(flush: true)) {
                flash.message = "${message(code: 'default.updated.message', args: [message(code: 'logModificaciones.label', default: 'LogModificaciones'), logModificacionesInstance.id])}"
                redirect(action: "show", id: logModificacionesInstance.id)
            }
            else {
                render(view: "edit", model: [logModificacionesInstance: logModificacionesInstance])
            }
        }
        else {
            flash.message = "${message(code: 'default.not.found.message', args: [message(code: 'logModificaciones.label', default: 'LogModificaciones'), params.id])}"
            redirect(action: "list")
        }
    }

    def delete = {
        def logModificacionesInstance = LogModificaciones.get(params.id)
        if (logModificacionesInstance) {
            try {
                logModificacionesInstance.delete(flush: true)
                flash.message = "${message(code: 'default.deleted.message', args: [message(code: 'logModificaciones.label', default: 'LogModificaciones'), params.id])}"
                redirect(action: "list")
            }
            catch (org.springframework.dao.DataIntegrityViolationException e) {
                flash.message = "${message(code: 'default.not.deleted.message', args: [message(code: 'logModificaciones.label', default: 'LogModificaciones'), params.id])}"
                redirect(action: "show", id: params.id)
            }
        }
        else {
            flash.message = "${message(code: 'default.not.found.message', args: [message(code: 'logModificaciones.label', default: 'LogModificaciones'), params.id])}"
            redirect(action: "list")
        }
    }
}
