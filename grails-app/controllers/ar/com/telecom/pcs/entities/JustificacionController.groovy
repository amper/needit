package ar.com.telecom.pcs.entities

class JustificacionController {
	
	static adminMenu = true

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    def index = {
        redirect(action: "list", params: params)
    }

    def list = {
        params.max = Math.min(params.max ? params.int('max') : 10, 100)
        [justificacionInstanceList: Justificacion.list(params), justificacionInstanceTotal: Justificacion.count()]
    }

    def create = {
        def justificacionInstance = new Justificacion()
        justificacionInstance.properties = params
        return [justificacionInstance: justificacionInstance]
    }

    def save = {
        def justificacionInstance = new Justificacion(params)
        if (justificacionInstance.save(flush: true)) {
            flash.message = "${message(code: 'default.created.message', args: [message(code: 'justificacion.label', default: 'Justificacion'), justificacionInstance.id])}"
            redirect(action: "show", id: justificacionInstance.id)
        }
        else {
            render(view: "create", model: [justificacionInstance: justificacionInstance])
        }
    }

    def show = {
        def justificacionInstance = Justificacion.get(params.id)
        if (!justificacionInstance) {
            flash.message = "${message(code: 'default.not.found.message', args: [message(code: 'justificacion.label', default: 'Justificacion'), params.id])}"
            redirect(action: "list")
        }
        else {
            [justificacionInstance: justificacionInstance]
        }
    }

    def edit = {
        def justificacionInstance = Justificacion.get(params.id)
        if (!justificacionInstance) {
            flash.message = "${message(code: 'default.not.found.message', args: [message(code: 'justificacion.label', default: 'Justificacion'), params.id])}"
            redirect(action: "list")
        }
        else {
            return [justificacionInstance: justificacionInstance]
        }
    }

    def update = {
        def justificacionInstance = Justificacion.get(params.id)
        if (justificacionInstance) {
            if (params.version) {
                def version = params.version.toLong()
                if (justificacionInstance.version > version) {
                    
                    justificacionInstance.errors.rejectValue("version", "default.optimistic.locking.failure", [message(code: 'justificacion.label', default: 'Justificacion')] as Object[], "Another user has updated this Justificacion while you were editing")
                    render(view: "edit", model: [justificacionInstance: justificacionInstance])
                    return
                }
            }
            justificacionInstance.properties = params
            if (!justificacionInstance.hasErrors() && justificacionInstance.save(flush: true)) {
                flash.message = "${message(code: 'default.updated.message', args: [message(code: 'justificacion.label', default: 'Justificacion'), justificacionInstance.id])}"
                redirect(action: "show", id: justificacionInstance.id)
            }
            else {
                render(view: "edit", model: [justificacionInstance: justificacionInstance])
            }
        }
        else {
            flash.message = "${message(code: 'default.not.found.message', args: [message(code: 'justificacion.label', default: 'Justificacion'), params.id])}"
            redirect(action: "list")
        }
    }

    def delete = {
        def justificacionInstance = Justificacion.get(params.id)
        if (justificacionInstance) {
            try {
                justificacionInstance.delete(flush: true)
                flash.message = "${message(code: 'default.deleted.message', args: [message(code: 'justificacion.label', default: 'Justificacion'), params.id])}"
                redirect(action: "list")
            }
            catch (org.springframework.dao.DataIntegrityViolationException e) {
                flash.message = "${message(code: 'default.not.deleted.message', args: [message(code: 'justificacion.label', default: 'Justificacion'), params.id])}"
                redirect(action: "show", id: params.id)
            }
        }
        else {
            flash.message = "${message(code: 'default.not.found.message', args: [message(code: 'justificacion.label', default: 'Justificacion'), params.id])}"
            redirect(action: "list")
        }
    }
}
