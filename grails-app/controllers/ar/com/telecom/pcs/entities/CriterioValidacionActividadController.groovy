package ar.com.telecom.pcs.entities

class CriterioValidacionActividadController {

	static adminMenu = true
	
    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    def index = {
        redirect(action: "list", params: params)
    }

    def list = {
        params.max = Math.min(params.max ? params.int('max') : 10, 100)
        [criterioValidacionActividadInstanceList: CriterioValidacionActividad.list(params), criterioValidacionActividadInstanceTotal: CriterioValidacionActividad.count()]
    }

    def create = {
        def criterioValidacionActividadInstance = new CriterioValidacionActividad()
        criterioValidacionActividadInstance.properties = params
        return [criterioValidacionActividadInstance: criterioValidacionActividadInstance]
    }

    def save = {
        def criterioValidacionActividadInstance = new CriterioValidacionActividad(params)
        if (criterioValidacionActividadInstance.save(flush: true)) {
            flash.message = "${message(code: 'default.created.message', args: [message(code: 'criterioValidacionActividad.label', default: 'CriterioValidacionActividad'), criterioValidacionActividadInstance.id])}"
            redirect(action: "show", id: criterioValidacionActividadInstance.id)
        }
        else {
            render(view: "create", model: [criterioValidacionActividadInstance: criterioValidacionActividadInstance])
        }
    }

    def show = {
        def criterioValidacionActividadInstance = CriterioValidacionActividad.get(params.id)
        if (!criterioValidacionActividadInstance) {
            flash.message = "${message(code: 'default.not.found.message', args: [message(code: 'criterioValidacionActividad.label', default: 'CriterioValidacionActividad'), params.id])}"
            redirect(action: "list")
        }
        else {
            [criterioValidacionActividadInstance: criterioValidacionActividadInstance]
        }
    }

    def edit = {
        def criterioValidacionActividadInstance = CriterioValidacionActividad.get(params.id)
        if (!criterioValidacionActividadInstance) {
            flash.message = "${message(code: 'default.not.found.message', args: [message(code: 'criterioValidacionActividad.label', default: 'CriterioValidacionActividad'), params.id])}"
            redirect(action: "list")
        }
        else {
            return [criterioValidacionActividadInstance: criterioValidacionActividadInstance]
        }
    }

    def update = {
        def criterioValidacionActividadInstance = CriterioValidacionActividad.get(params.id)
        if (criterioValidacionActividadInstance) {
            if (params.version) {
                def version = params.version.toLong()
                if (criterioValidacionActividadInstance.version > version) {
                    
                    criterioValidacionActividadInstance.errors.rejectValue("version", "default.optimistic.locking.failure", [message(code: 'criterioValidacionActividad.label', default: 'CriterioValidacionActividad')] as Object[], "Another user has updated this CriterioValidacionActividad while you were editing")
                    render(view: "edit", model: [criterioValidacionActividadInstance: criterioValidacionActividadInstance])
                    return
                }
            }
            criterioValidacionActividadInstance.properties = params
            if (!criterioValidacionActividadInstance.hasErrors() && criterioValidacionActividadInstance.save(flush: true)) {
                flash.message = "${message(code: 'default.updated.message', args: [message(code: 'criterioValidacionActividad.label', default: 'CriterioValidacionActividad'), criterioValidacionActividadInstance.id])}"
                redirect(action: "show", id: criterioValidacionActividadInstance.id)
            }
            else {
                render(view: "edit", model: [criterioValidacionActividadInstance: criterioValidacionActividadInstance])
            }
        }
        else {
            flash.message = "${message(code: 'default.not.found.message', args: [message(code: 'criterioValidacionActividad.label', default: 'CriterioValidacionActividad'), params.id])}"
            redirect(action: "list")
        }
    }

    def delete = {
        def criterioValidacionActividadInstance = CriterioValidacionActividad.get(params.id)
        if (criterioValidacionActividadInstance) {
            try {
                criterioValidacionActividadInstance.delete(flush: true)
                flash.message = "${message(code: 'default.deleted.message', args: [message(code: 'criterioValidacionActividad.label', default: 'CriterioValidacionActividad'), params.id])}"
                redirect(action: "list")
            }
            catch (org.springframework.dao.DataIntegrityViolationException e) {
                flash.message = "${message(code: 'default.not.deleted.message', args: [message(code: 'criterioValidacionActividad.label', default: 'CriterioValidacionActividad'), params.id])}"
                redirect(action: "show", id: params.id)
            }
        }
        else {
            flash.message = "${message(code: 'default.not.found.message', args: [message(code: 'criterioValidacionActividad.label', default: 'CriterioValidacionActividad'), params.id])}"
            redirect(action: "list")
        }
    }
}
