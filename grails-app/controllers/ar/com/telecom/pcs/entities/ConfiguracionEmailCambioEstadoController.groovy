package ar.com.telecom.pcs.entities

class ConfiguracionEmailCambioEstadoController {
	
	static adminMenu = true

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    def index = {
        redirect(action: "list", params: params)
    }

    def list = {
        params.max = Math.min(params.max ? params.int('max') : 10, 100)
        [configuracionEmailCambioEstadoInstanceList: ConfiguracionEmailCambioEstado.list(params), configuracionEmailCambioEstadoInstanceTotal: ConfiguracionEmailCambioEstado.count()]
    }

    def create = {
        def configuracionEmailCambioEstadoInstance = new ConfiguracionEmailCambioEstado()
        configuracionEmailCambioEstadoInstance.properties = params
        return [configuracionEmailCambioEstadoInstance: configuracionEmailCambioEstadoInstance]
    }

    def save = {
        def configuracionEmailCambioEstadoInstance = new ConfiguracionEmailCambioEstado(params)
        if (configuracionEmailCambioEstadoInstance.save(flush: true)) {
            flash.message = "${message(code: 'default.created.message', args: [message(code: 'configuracionEmailCambioEstado.label', default: 'ConfiguracionEmailCambioEstado'), configuracionEmailCambioEstadoInstance.id])}"
            redirect(action: "show", id: configuracionEmailCambioEstadoInstance.id)
        }
        else {
            render(view: "create", model: [configuracionEmailCambioEstadoInstance: configuracionEmailCambioEstadoInstance])
        }
    }

    def show = {
        def configuracionEmailCambioEstadoInstance = ConfiguracionEmailCambioEstado.get(params.id)
        if (!configuracionEmailCambioEstadoInstance) {
            flash.message = "${message(code: 'default.not.found.message', args: [message(code: 'configuracionEmailCambioEstado.label', default: 'ConfiguracionEmailCambioEstado'), params.id])}"
            redirect(action: "list")
        }
        else {
            [configuracionEmailCambioEstadoInstance: configuracionEmailCambioEstadoInstance]
        }
    }

    def edit = {
        def configuracionEmailCambioEstadoInstance = ConfiguracionEmailCambioEstado.get(params.id)
        if (!configuracionEmailCambioEstadoInstance) {
            flash.message = "${message(code: 'default.not.found.message', args: [message(code: 'configuracionEmailCambioEstado.label', default: 'ConfiguracionEmailCambioEstado'), params.id])}"
            redirect(action: "list")
        }
        else {
            return [configuracionEmailCambioEstadoInstance: configuracionEmailCambioEstadoInstance]
        }
    }

    def update = {
        def configuracionEmailCambioEstadoInstance = ConfiguracionEmailCambioEstado.get(params.id)
        if (configuracionEmailCambioEstadoInstance) {
            if (params.version) {
                def version = params.version.toLong()
                if (configuracionEmailCambioEstadoInstance.version > version) {
                    
                    configuracionEmailCambioEstadoInstance.errors.rejectValue("version", "default.optimistic.locking.failure", [message(code: 'configuracionEmailCambioEstado.label', default: 'ConfiguracionEmailCambioEstado')] as Object[], "Another user has updated this ConfiguracionEmailCambioEstado while you were editing")
                    render(view: "edit", model: [configuracionEmailCambioEstadoInstance: configuracionEmailCambioEstadoInstance])
                    return
                }
            }
            configuracionEmailCambioEstadoInstance.properties = params
            if (!configuracionEmailCambioEstadoInstance.hasErrors() && configuracionEmailCambioEstadoInstance.save(flush: true)) {
                flash.message = "${message(code: 'default.updated.message', args: [message(code: 'configuracionEmailCambioEstado.label', default: 'ConfiguracionEmailCambioEstado'), configuracionEmailCambioEstadoInstance.id])}"
                redirect(action: "show", id: configuracionEmailCambioEstadoInstance.id)
            }
            else {
                render(view: "edit", model: [configuracionEmailCambioEstadoInstance: configuracionEmailCambioEstadoInstance])
            }
        }
        else {
            flash.message = "${message(code: 'default.not.found.message', args: [message(code: 'configuracionEmailCambioEstado.label', default: 'ConfiguracionEmailCambioEstado'), params.id])}"
            redirect(action: "list")
        }
    }

    def delete = {
        def configuracionEmailCambioEstadoInstance = ConfiguracionEmailCambioEstado.get(params.id)
        if (configuracionEmailCambioEstadoInstance) {
            try {
                configuracionEmailCambioEstadoInstance.delete(flush: true)
                flash.message = "${message(code: 'default.deleted.message', args: [message(code: 'configuracionEmailCambioEstado.label', default: 'ConfiguracionEmailCambioEstado'), params.id])}"
                redirect(action: "list")
            }
            catch (org.springframework.dao.DataIntegrityViolationException e) {
                flash.message = "${message(code: 'default.not.deleted.message', args: [message(code: 'configuracionEmailCambioEstado.label', default: 'ConfiguracionEmailCambioEstado'), params.id])}"
                redirect(action: "show", id: params.id)
            }
        }
        else {
            flash.message = "${message(code: 'default.not.found.message', args: [message(code: 'configuracionEmailCambioEstado.label', default: 'ConfiguracionEmailCambioEstado'), params.id])}"
            redirect(action: "list")
        }
    }
}
