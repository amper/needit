package ar.com.telecom.pcs.entities

class AbstractPedidoController {

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    def index = {
        redirect(action: "list", params: params)
    }

    def list = {
        params.max = Math.min(params.max ? params.int('max') : 10, 100)
        [abstractPedidoInstanceList: AbstractPedido.list(params), abstractPedidoInstanceTotal: AbstractPedido.count()]
    }

    def create = {
        def abstractPedidoInstance = new AbstractPedido()
        abstractPedidoInstance.properties = params
        return [abstractPedidoInstance: abstractPedidoInstance]
    }

    def save = {
        def abstractPedidoInstance = new AbstractPedido(params)
        if (abstractPedidoInstance.save(flush: true)) {
            flash.message = "${message(code: 'default.created.message', args: [message(code: 'abstractPedido.label', default: 'AbstractPedido'), abstractPedidoInstance.id])}"
            redirect(action: "show", id: abstractPedidoInstance.id)
        }
        else {
            render(view: "create", model: [abstractPedidoInstance: abstractPedidoInstance])
        }
    }

    def show = {
        def abstractPedidoInstance = AbstractPedido.get(params.id)
        if (!abstractPedidoInstance) {
            flash.message = "${message(code: 'default.not.found.message', args: [message(code: 'abstractPedido.label', default: 'AbstractPedido'), params.id])}"
            redirect(action: "list")
        }
        else {
            [abstractPedidoInstance: abstractPedidoInstance]
        }
    }

    def edit = {
        def abstractPedidoInstance = AbstractPedido.get(params.id)
        if (!abstractPedidoInstance) {
            flash.message = "${message(code: 'default.not.found.message', args: [message(code: 'abstractPedido.label', default: 'AbstractPedido'), params.id])}"
            redirect(action: "list")
        }
        else {
            return [abstractPedidoInstance: abstractPedidoInstance]
        }
    }

    def update = {
        def abstractPedidoInstance = AbstractPedido.get(params.id)
        if (abstractPedidoInstance) {
            if (params.version) {
                def version = params.version.toLong()
                if (abstractPedidoInstance.version > version) {
                    
                    abstractPedidoInstance.errors.rejectValue("version", "default.optimistic.locking.failure", [message(code: 'abstractPedido.label', default: 'AbstractPedido')] as Object[], "Another user has updated this AbstractPedido while you were editing")
                    render(view: "edit", model: [abstractPedidoInstance: abstractPedidoInstance])
                    return
                }
            }
            abstractPedidoInstance.properties = params
            if (!abstractPedidoInstance.hasErrors() && abstractPedidoInstance.save(flush: true)) {
                flash.message = "${message(code: 'default.updated.message', args: [message(code: 'abstractPedido.label', default: 'AbstractPedido'), abstractPedidoInstance.id])}"
                redirect(action: "show", id: abstractPedidoInstance.id)
            }
            else {
                render(view: "edit", model: [abstractPedidoInstance: abstractPedidoInstance])
            }
        }
        else {
            flash.message = "${message(code: 'default.not.found.message', args: [message(code: 'abstractPedido.label', default: 'AbstractPedido'), params.id])}"
            redirect(action: "list")
        }
    }

    def delete = {
        def abstractPedidoInstance = AbstractPedido.get(params.id)
        if (abstractPedidoInstance) {
            try {
                abstractPedidoInstance.delete(flush: true)
                flash.message = "${message(code: 'default.deleted.message', args: [message(code: 'abstractPedido.label', default: 'AbstractPedido'), params.id])}"
                redirect(action: "list")
            }
            catch (org.springframework.dao.DataIntegrityViolationException e) {
                flash.message = "${message(code: 'default.not.deleted.message', args: [message(code: 'abstractPedido.label', default: 'AbstractPedido'), params.id])}"
                redirect(action: "show", id: params.id)
            }
        }
        else {
            flash.message = "${message(code: 'default.not.found.message', args: [message(code: 'abstractPedido.label', default: 'AbstractPedido'), params.id])}"
            redirect(action: "list")
        }
    }
}
