package ar.com.telecom.pcs.entities

class FaseController {
	
	static adminMenu = true

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    def index = {
        redirect(action: "list", params: params)
    }

    def list = {
        params.max = Math.min(params.max ? params.int('max') : 10, 100)
        [faseInstanceList: Fase.list(params), faseInstanceTotal: Fase.count()]
    }

    def create = {
        def faseInstance = new Fase()
        faseInstance.properties = params
        return [faseInstance: faseInstance]
    }

    def save = {
        def faseInstance = new Fase(params)
        if (faseInstance.save(flush: true)) {
            flash.message = "${message(code: 'default.created.message', args: [message(code: 'fase.label', default: 'Fase'), faseInstance.id])}"
            redirect(action: "show", id: faseInstance.id)
        }
        else {
            render(view: "create", model: [faseInstance: faseInstance])
        }
    }

    def show = {
        def faseInstance = Fase.get(params.id)
        if (!faseInstance) {
            flash.message = "${message(code: 'default.not.found.message', args: [message(code: 'fase.label', default: 'Fase'), params.id])}"
            redirect(action: "list")
        }
        else {
            [faseInstance: faseInstance]
        }
    }

    def edit = {
        def faseInstance = Fase.get(params.id)
        if (!faseInstance) {
            flash.message = "${message(code: 'default.not.found.message', args: [message(code: 'fase.label', default: 'Fase'), params.id])}"
            redirect(action: "list")
        }
        else {
            return [faseInstance: faseInstance]
        }
    }

    def update = {
        def faseInstance = Fase.get(params.id)
        if (faseInstance) {
            if (params.version) {
                def version = params.version.toLong()
                if (faseInstance.version > version) {
                    
                    faseInstance.errors.rejectValue("version", "default.optimistic.locking.failure", [message(code: 'fase.label', default: 'Fase')] as Object[], "Another user has updated this Fase while you were editing")
                    render(view: "edit", model: [faseInstance: faseInstance])
                    return
                }
            }
            faseInstance.properties = params
            if (!faseInstance.hasErrors() && faseInstance.save(flush: true)) {
                flash.message = "${message(code: 'default.updated.message', args: [message(code: 'fase.label', default: 'Fase'), faseInstance.id])}"
                redirect(action: "show", id: faseInstance.id)
            }
            else {
                render(view: "edit", model: [faseInstance: faseInstance])
            }
        }
        else {
            flash.message = "${message(code: 'default.not.found.message', args: [message(code: 'fase.label', default: 'Fase'), params.id])}"
            redirect(action: "list")
        }
    }

    def delete = {
        def faseInstance = Fase.get(params.id)
        if (faseInstance) {
            try {
                faseInstance.delete(flush: true)
                flash.message = "${message(code: 'default.deleted.message', args: [message(code: 'fase.label', default: 'Fase'), params.id])}"
                redirect(action: "list")
            }
            catch (org.springframework.dao.DataIntegrityViolationException e) {
                flash.message = "${message(code: 'default.not.deleted.message', args: [message(code: 'fase.label', default: 'Fase'), params.id])}"
                redirect(action: "show", id: params.id)
            }
        }
        else {
            flash.message = "${message(code: 'default.not.found.message', args: [message(code: 'fase.label', default: 'Fase'), params.id])}"
            redirect(action: "list")
        }
    }
}
