package ar.com.telecom.pcs.entities

class TailoringEstrategiaPruebaController {
	
	static adminMenu = false

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    def index = {
        redirect(action: "list", params: params)
    }

    def list = {
        params.max = Math.min(params.max ? params.int('max') : 10, 100)
        [tailoringEstrategiaPruebaInstanceList: TailoringEstrategiaPrueba.list(params), tailoringEstrategiaPruebaInstanceTotal: TailoringEstrategiaPrueba.count()]
    }

    def create = {
        def tailoringEstrategiaPruebaInstance = new TailoringEstrategiaPrueba()
        tailoringEstrategiaPruebaInstance.properties = params
        return [tailoringEstrategiaPruebaInstance: tailoringEstrategiaPruebaInstance]
    }

    def save = {
        def tailoringEstrategiaPruebaInstance = new TailoringEstrategiaPrueba(params)
        if (tailoringEstrategiaPruebaInstance.save(flush: true)) {
            flash.message = "${message(code: 'default.created.message', args: [message(code: 'tailoringEstrategiaPrueba.label', default: 'TailoringEstrategiaPrueba'), tailoringEstrategiaPruebaInstance.id])}"
            redirect(action: "show", id: tailoringEstrategiaPruebaInstance.id)
        }
        else {
            render(view: "create", model: [tailoringEstrategiaPruebaInstance: tailoringEstrategiaPruebaInstance])
        }
    }

    def show = {
        def tailoringEstrategiaPruebaInstance = TailoringEstrategiaPrueba.get(params.id)
        if (!tailoringEstrategiaPruebaInstance) {
            flash.message = "${message(code: 'default.not.found.message', args: [message(code: 'tailoringEstrategiaPrueba.label', default: 'TailoringEstrategiaPrueba'), params.id])}"
            redirect(action: "list")
        }
        else {
            [tailoringEstrategiaPruebaInstance: tailoringEstrategiaPruebaInstance]
        }
    }

    def edit = {
        def tailoringEstrategiaPruebaInstance = TailoringEstrategiaPrueba.get(params.id)
        if (!tailoringEstrategiaPruebaInstance) {
            flash.message = "${message(code: 'default.not.found.message', args: [message(code: 'tailoringEstrategiaPrueba.label', default: 'TailoringEstrategiaPrueba'), params.id])}"
            redirect(action: "list")
        }
        else {
            return [tailoringEstrategiaPruebaInstance: tailoringEstrategiaPruebaInstance]
        }
    }

    def update = {
        def tailoringEstrategiaPruebaInstance = TailoringEstrategiaPrueba.get(params.id)
        if (tailoringEstrategiaPruebaInstance) {
            if (params.version) {
                def version = params.version.toLong()
                if (tailoringEstrategiaPruebaInstance.version > version) {
                    
                    tailoringEstrategiaPruebaInstance.errors.rejectValue("version", "default.optimistic.locking.failure", [message(code: 'tailoringEstrategiaPrueba.label', default: 'TailoringEstrategiaPrueba')] as Object[], "Another user has updated this TailoringEstrategiaPrueba while you were editing")
                    render(view: "edit", model: [tailoringEstrategiaPruebaInstance: tailoringEstrategiaPruebaInstance])
                    return
                }
            }
            tailoringEstrategiaPruebaInstance.properties = params
            if (!tailoringEstrategiaPruebaInstance.hasErrors() && tailoringEstrategiaPruebaInstance.save(flush: true)) {
                flash.message = "${message(code: 'default.updated.message', args: [message(code: 'tailoringEstrategiaPrueba.label', default: 'TailoringEstrategiaPrueba'), tailoringEstrategiaPruebaInstance.id])}"
                redirect(action: "show", id: tailoringEstrategiaPruebaInstance.id)
            }
            else {
                render(view: "edit", model: [tailoringEstrategiaPruebaInstance: tailoringEstrategiaPruebaInstance])
            }
        }
        else {
            flash.message = "${message(code: 'default.not.found.message', args: [message(code: 'tailoringEstrategiaPrueba.label', default: 'TailoringEstrategiaPrueba'), params.id])}"
            redirect(action: "list")
        }
    }

    def delete = {
        def tailoringEstrategiaPruebaInstance = TailoringEstrategiaPrueba.get(params.id)
        if (tailoringEstrategiaPruebaInstance) {
            try {
                tailoringEstrategiaPruebaInstance.delete(flush: true)
                flash.message = "${message(code: 'default.deleted.message', args: [message(code: 'tailoringEstrategiaPrueba.label', default: 'TailoringEstrategiaPrueba'), params.id])}"
                redirect(action: "list")
            }
            catch (org.springframework.dao.DataIntegrityViolationException e) {
                flash.message = "${message(code: 'default.not.deleted.message', args: [message(code: 'tailoringEstrategiaPrueba.label', default: 'TailoringEstrategiaPrueba'), params.id])}"
                redirect(action: "show", id: params.id)
            }
        }
        else {
            flash.message = "${message(code: 'default.not.found.message', args: [message(code: 'tailoringEstrategiaPrueba.label', default: 'TailoringEstrategiaPrueba'), params.id])}"
            redirect(action: "list")
        }
    }
}
