package ar.com.telecom.pcs.entities

class MotivoSuspensionController {
	
	static adminMenu = true
	
    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    def index = {
        redirect(action: "list", params: params)
    }

    def list = {
        params.max = Math.min(params.max ? params.int('max') : 10, 100)
        [motivoSuspensionInstanceList: MotivoSuspension.list(params), motivoSuspensionInstanceTotal: MotivoSuspension.count()]
    }

    def create = {
        def motivoSuspensionInstance = new MotivoSuspension()
        motivoSuspensionInstance.properties = params
        return [motivoSuspensionInstance: motivoSuspensionInstance]
    }

    def save = {
        def motivoSuspensionInstance = new MotivoSuspension(params)
        if (motivoSuspensionInstance.save(flush: true)) {
            flash.message = "${message(code: 'default.created.message', args: [message(code: 'motivoSuspension.label', default: 'MotivoSuspension'), motivoSuspensionInstance.id])}"
            redirect(action: "show", id: motivoSuspensionInstance.id)
        }
        else {
            render(view: "create", model: [motivoSuspensionInstance: motivoSuspensionInstance])
        }
    }

    def show = {
        def motivoSuspensionInstance = MotivoSuspension.get(params.id)
        if (!motivoSuspensionInstance) {
            flash.message = "${message(code: 'default.not.found.message', args: [message(code: 'motivoSuspension.label', default: 'MotivoSuspension'), params.id])}"
            redirect(action: "list")
        }
        else {
            [motivoSuspensionInstance: motivoSuspensionInstance]
        }
    }

    def edit = {
        def motivoSuspensionInstance = MotivoSuspension.get(params.id)
        if (!motivoSuspensionInstance) {
            flash.message = "${message(code: 'default.not.found.message', args: [message(code: 'motivoSuspension.label', default: 'MotivoSuspension'), params.id])}"
            redirect(action: "list")
        }
        else {
            return [motivoSuspensionInstance: motivoSuspensionInstance]
        }
    }

    def update = {
        def motivoSuspensionInstance = MotivoSuspension.get(params.id)
        if (motivoSuspensionInstance) {
            if (params.version) {
                def version = params.version.toLong()
                if (motivoSuspensionInstance.version > version) {
                    
                    motivoSuspensionInstance.errors.rejectValue("version", "default.optimistic.locking.failure", [message(code: 'motivoSuspension.label', default: 'MotivoSuspension')] as Object[], "Another user has updated this MotivoSuspension while you were editing")
                    render(view: "edit", model: [motivoSuspensionInstance: motivoSuspensionInstance])
                    return
                }
            }
            motivoSuspensionInstance.properties = params
            if (!motivoSuspensionInstance.hasErrors() && motivoSuspensionInstance.save(flush: true)) {
                flash.message = "${message(code: 'default.updated.message', args: [message(code: 'motivoSuspension.label', default: 'MotivoSuspension'), motivoSuspensionInstance.id])}"
                redirect(action: "show", id: motivoSuspensionInstance.id)
            }
            else {
                render(view: "edit", model: [motivoSuspensionInstance: motivoSuspensionInstance])
            }
        }
        else {
            flash.message = "${message(code: 'default.not.found.message', args: [message(code: 'motivoSuspension.label', default: 'MotivoSuspension'), params.id])}"
            redirect(action: "list")
        }
    }

    def delete = {
        def motivoSuspensionInstance = MotivoSuspension.get(params.id)
        if (motivoSuspensionInstance) {
            try {
                motivoSuspensionInstance.delete(flush: true)
                flash.message = "${message(code: 'default.deleted.message', args: [message(code: 'motivoSuspension.label', default: 'MotivoSuspension'), params.id])}"
                redirect(action: "list")
            }
            catch (org.springframework.dao.DataIntegrityViolationException e) {
                flash.message = "${message(code: 'default.not.deleted.message', args: [message(code: 'motivoSuspension.label', default: 'MotivoSuspension'), params.id])}"
                redirect(action: "show", id: params.id)
            }
        }
        else {
            flash.message = "${message(code: 'default.not.found.message', args: [message(code: 'motivoSuspension.label', default: 'MotivoSuspension'), params.id])}"
            redirect(action: "list")
        }
    }
}
