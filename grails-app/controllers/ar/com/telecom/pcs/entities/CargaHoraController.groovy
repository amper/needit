package ar.com.telecom.pcs.entities

class CargaHoraController {
	
	static adminMenu = false

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    def index = {
        redirect(action: "list", params: params)
    }

    def list = {
        params.max = Math.min(params.max ? params.int('max') : 10, 100)
        [cargaHoraInstanceList: CargaHora.list(params), cargaHoraInstanceTotal: CargaHora.count()]
    }

    def create = {
        def cargaHoraInstance = new CargaHora()
        cargaHoraInstance.properties = params
        return [cargaHoraInstance: cargaHoraInstance]
    }

    def save = {
        def cargaHoraInstance = new CargaHora(params)
        if (cargaHoraInstance.save(flush: true)) {
            flash.message = "${message(code: 'default.created.message', args: [message(code: 'cargaHora.label', default: 'CargaHora'), cargaHoraInstance.id])}"
            redirect(action: "show", id: cargaHoraInstance.id)
        }
        else {
            render(view: "create", model: [cargaHoraInstance: cargaHoraInstance])
        }
    }

    def show = {
        def cargaHoraInstance = CargaHora.get(params.id)
        if (!cargaHoraInstance) {
            flash.message = "${message(code: 'default.not.found.message', args: [message(code: 'cargaHora.label', default: 'CargaHora'), params.id])}"
            redirect(action: "list")
        }
        else {
            [cargaHoraInstance: cargaHoraInstance]
        }
    }

    def edit = {
        def cargaHoraInstance = CargaHora.get(params.id)
        if (!cargaHoraInstance) {
            flash.message = "${message(code: 'default.not.found.message', args: [message(code: 'cargaHora.label', default: 'CargaHora'), params.id])}"
            redirect(action: "list")
        }
        else {
            return [cargaHoraInstance: cargaHoraInstance]
        }
    }

    def update = {
        def cargaHoraInstance = CargaHora.get(params.id)
        if (cargaHoraInstance) {
            if (params.version) {
                def version = params.version.toLong()
                if (cargaHoraInstance.version > version) {
                    
                    cargaHoraInstance.errors.rejectValue("version", "default.optimistic.locking.failure", [message(code: 'cargaHora.label', default: 'CargaHora')] as Object[], "Another user has updated this CargaHora while you were editing")
                    render(view: "edit", model: [cargaHoraInstance: cargaHoraInstance])
                    return
                }
            }
            cargaHoraInstance.properties = params
            if (!cargaHoraInstance.hasErrors() && cargaHoraInstance.save(flush: true)) {
                flash.message = "${message(code: 'default.updated.message', args: [message(code: 'cargaHora.label', default: 'CargaHora'), cargaHoraInstance.id])}"
                redirect(action: "show", id: cargaHoraInstance.id)
            }
            else {
                render(view: "edit", model: [cargaHoraInstance: cargaHoraInstance])
            }
        }
        else {
            flash.message = "${message(code: 'default.not.found.message', args: [message(code: 'cargaHora.label', default: 'CargaHora'), params.id])}"
            redirect(action: "list")
        }
    }

    def delete = {
        def cargaHoraInstance = CargaHora.get(params.id)
        if (cargaHoraInstance) {
            try {
                cargaHoraInstance.delete(flush: true)
                flash.message = "${message(code: 'default.deleted.message', args: [message(code: 'cargaHora.label', default: 'CargaHora'), params.id])}"
                redirect(action: "list")
            }
            catch (org.springframework.dao.DataIntegrityViolationException e) {
                flash.message = "${message(code: 'default.not.deleted.message', args: [message(code: 'cargaHora.label', default: 'CargaHora'), params.id])}"
                redirect(action: "show", id: params.id)
            }
        }
        else {
            flash.message = "${message(code: 'default.not.found.message', args: [message(code: 'cargaHora.label', default: 'CargaHora'), params.id])}"
            redirect(action: "list")
        }
    }
}
