package ar.com.telecom.pcs.entities

class ParametrosSistemaController {

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    def index = {
        redirect(action: "list", params: params)
    }

    def list = {
        params.max = Math.min(params.max ? params.int('max') : 10, 100)
        [parametrosSistemaInstanceList: ParametrosSistema.list(params), parametrosSistemaInstanceTotal: ParametrosSistema.count()]
    }

    def create = {
        def parametrosSistemaInstance = new ParametrosSistema()
        parametrosSistemaInstance.properties = params
        return [parametrosSistemaInstance: parametrosSistemaInstance]
    }

    def save = {
        def parametrosSistemaInstance = new ParametrosSistema(params)
        if (parametrosSistemaInstance.save(flush: true)) {
            flash.message = "${message(code: 'default.created.message', args: [message(code: 'parametrosSistema.label', default: 'ParametrosSistema'), parametrosSistemaInstance.id])}"
            redirect(action: "show", id: parametrosSistemaInstance.id)
        }
        else {
            render(view: "create", model: [parametrosSistemaInstance: parametrosSistemaInstance])
        }
    }

    def show = {
        def parametrosSistemaInstance = ParametrosSistema.get(params.id)
        if (!parametrosSistemaInstance) {
            flash.message = "${message(code: 'default.not.found.message', args: [message(code: 'parametrosSistema.label', default: 'ParametrosSistema'), params.id])}"
            redirect(action: "list")
        }
        else {
            [parametrosSistemaInstance: parametrosSistemaInstance]
        }
    }

    def edit = {
        def parametrosSistemaInstance = ParametrosSistema.get(params.id)
        if (!parametrosSistemaInstance) {
            flash.message = "${message(code: 'default.not.found.message', args: [message(code: 'parametrosSistema.label', default: 'ParametrosSistema'), params.id])}"
            redirect(action: "list")
        }
        else {
            return [parametrosSistemaInstance: parametrosSistemaInstance]
        }
    }

    def update = {
        def parametrosSistemaInstance = ParametrosSistema.get(params.id)
        if (parametrosSistemaInstance) {
            if (params.version) {
                def version = params.version.toLong()
                if (parametrosSistemaInstance.version > version) {
                    
                    parametrosSistemaInstance.errors.rejectValue("version", "default.optimistic.locking.failure", [message(code: 'parametrosSistema.label', default: 'ParametrosSistema')] as Object[], "Another user has updated this ParametrosSistema while you were editing")
                    render(view: "edit", model: [parametrosSistemaInstance: parametrosSistemaInstance])
                    return
                }
            }
            parametrosSistemaInstance.properties = params
            if (!parametrosSistemaInstance.hasErrors() && parametrosSistemaInstance.save(flush: true)) {
                flash.message = "${message(code: 'default.updated.message', args: [message(code: 'parametrosSistema.label', default: 'ParametrosSistema'), parametrosSistemaInstance.id])}"
                redirect(action: "show", id: parametrosSistemaInstance.id)
            }
            else {
                render(view: "edit", model: [parametrosSistemaInstance: parametrosSistemaInstance])
            }
        }
        else {
            flash.message = "${message(code: 'default.not.found.message', args: [message(code: 'parametrosSistema.label', default: 'ParametrosSistema'), params.id])}"
            redirect(action: "list")
        }
    }

    def delete = {
        def parametrosSistemaInstance = ParametrosSistema.get(params.id)
        if (parametrosSistemaInstance) {
            try {
                parametrosSistemaInstance.delete(flush: true)
                flash.message = "${message(code: 'default.deleted.message', args: [message(code: 'parametrosSistema.label', default: 'ParametrosSistema'), params.id])}"
                redirect(action: "list")
            }
            catch (org.springframework.dao.DataIntegrityViolationException e) {
                flash.message = "${message(code: 'default.not.deleted.message', args: [message(code: 'parametrosSistema.label', default: 'ParametrosSistema'), params.id])}"
                redirect(action: "show", id: params.id)
            }
        }
        else {
            flash.message = "${message(code: 'default.not.found.message', args: [message(code: 'parametrosSistema.label', default: 'ParametrosSistema'), params.id])}"
            redirect(action: "list")
        }
    }
}
