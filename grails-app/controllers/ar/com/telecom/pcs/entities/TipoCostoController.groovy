package ar.com.telecom.pcs.entities

class TipoCostoController {
	
	static adminMenu = true

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    def index = {
        redirect(action: "list", params: params)
    }

    def list = {
        params.max = Math.min(params.max ? params.int('max') : 10, 100)
        [tipoCostoInstanceList: TipoCosto.list(params), tipoCostoInstanceTotal: TipoCosto.count()]
    }

    def create = {
        def tipoCostoInstance = new TipoCosto()
        tipoCostoInstance.properties = params
        return [tipoCostoInstance: tipoCostoInstance]
    }

    def save = {
        def tipoCostoInstance = new TipoCosto(params)
        if (tipoCostoInstance.save(flush: true)) {
            flash.message = "${message(code: 'default.created.message', args: [message(code: 'tipoCosto.label', default: 'TipoCosto'), tipoCostoInstance.id])}"
            redirect(action: "show", id: tipoCostoInstance.id)
        }
        else {
            render(view: "create", model: [tipoCostoInstance: tipoCostoInstance])
        }
    }

    def show = {
        def tipoCostoInstance = TipoCosto.get(params.id)
        if (!tipoCostoInstance) {
            flash.message = "${message(code: 'default.not.found.message', args: [message(code: 'tipoCosto.label', default: 'TipoCosto'), params.id])}"
            redirect(action: "list")
        }
        else {
            [tipoCostoInstance: tipoCostoInstance]
        }
    }

    def edit = {
        def tipoCostoInstance = TipoCosto.get(params.id)
        if (!tipoCostoInstance) {
            flash.message = "${message(code: 'default.not.found.message', args: [message(code: 'tipoCosto.label', default: 'TipoCosto'), params.id])}"
            redirect(action: "list")
        }
        else {
            return [tipoCostoInstance: tipoCostoInstance]
        }
    }

    def update = {
        def tipoCostoInstance = TipoCosto.get(params.id)
        if (tipoCostoInstance) {
            if (params.version) {
                def version = params.version.toLong()
                if (tipoCostoInstance.version > version) {
                    
                    tipoCostoInstance.errors.rejectValue("version", "default.optimistic.locking.failure", [message(code: 'tipoCosto.label', default: 'TipoCosto')] as Object[], "Another user has updated this TipoCosto while you were editing")
                    render(view: "edit", model: [tipoCostoInstance: tipoCostoInstance])
                    return
                }
            }
            tipoCostoInstance.properties = params
            if (!tipoCostoInstance.hasErrors() && tipoCostoInstance.save(flush: true)) {
                flash.message = "${message(code: 'default.updated.message', args: [message(code: 'tipoCosto.label', default: 'TipoCosto'), tipoCostoInstance.id])}"
                redirect(action: "show", id: tipoCostoInstance.id)
            }
            else {
                render(view: "edit", model: [tipoCostoInstance: tipoCostoInstance])
            }
        }
        else {
            flash.message = "${message(code: 'default.not.found.message', args: [message(code: 'tipoCosto.label', default: 'TipoCosto'), params.id])}"
            redirect(action: "list")
        }
    }

    def delete = {
        def tipoCostoInstance = TipoCosto.get(params.id)
        if (tipoCostoInstance) {
            try {
                tipoCostoInstance.delete(flush: true)
                flash.message = "${message(code: 'default.deleted.message', args: [message(code: 'tipoCosto.label', default: 'TipoCosto'), params.id])}"
                redirect(action: "list")
            }
            catch (org.springframework.dao.DataIntegrityViolationException e) {
                flash.message = "${message(code: 'default.not.deleted.message', args: [message(code: 'tipoCosto.label', default: 'TipoCosto'), params.id])}"
                redirect(action: "show", id: params.id)
            }
        }
        else {
            flash.message = "${message(code: 'default.not.found.message', args: [message(code: 'tipoCosto.label', default: 'TipoCosto'), params.id])}"
            redirect(action: "list")
        }
    }
}
