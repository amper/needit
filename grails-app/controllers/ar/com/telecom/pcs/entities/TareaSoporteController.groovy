package ar.com.telecom.pcs.entities

class TareaSoporteController {
	
	static adminMenu = true

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    def index = {
        redirect(action: "list", params: params)
    }

    def list = {
        params.max = Math.min(params.max ? params.int('max') : 10, 100)
        [tareaSoporteInstanceList: TareaSoporte.list(params), tareaSoporteInstanceTotal: TareaSoporte.count()]
    }

    def create = {
        def tareaSoporteInstance = new TareaSoporte()
        tareaSoporteInstance.properties = params
        return [tareaSoporteInstance: tareaSoporteInstance]
    }

    def save = {
        def tareaSoporteInstance = new TareaSoporte(params)
        if (tareaSoporteInstance.save(flush: true)) {
            flash.message = "${message(code: 'default.created.message', args: [message(code: 'tareaSoporte.label', default: 'TareaSoporte'), tareaSoporteInstance.id])}"
            redirect(action: "show", id: tareaSoporteInstance.id)
        }
        else {
            render(view: "create", model: [tareaSoporteInstance: tareaSoporteInstance])
        }
    }

    def show = {
        def tareaSoporteInstance = TareaSoporte.get(params.id)
        if (!tareaSoporteInstance) {
            flash.message = "${message(code: 'default.not.found.message', args: [message(code: 'tareaSoporte.label', default: 'TareaSoporte'), params.id])}"
            redirect(action: "list")
        }
        else {
            [tareaSoporteInstance: tareaSoporteInstance]
        }
    }

    def edit = {
        def tareaSoporteInstance = TareaSoporte.get(params.id)
        if (!tareaSoporteInstance) {
            flash.message = "${message(code: 'default.not.found.message', args: [message(code: 'tareaSoporte.label', default: 'TareaSoporte'), params.id])}"
            redirect(action: "list")
        }
        else {
            return [tareaSoporteInstance: tareaSoporteInstance]
        }
    }

    def update = {
        def tareaSoporteInstance = TareaSoporte.get(params.id)
        if (tareaSoporteInstance) {
            if (params.version) {
                def version = params.version.toLong()
                if (tareaSoporteInstance.version > version) {
                    
                    tareaSoporteInstance.errors.rejectValue("version", "default.optimistic.locking.failure", [message(code: 'tareaSoporte.label', default: 'TareaSoporte')] as Object[], "Another user has updated this TareaSoporte while you were editing")
                    render(view: "edit", model: [tareaSoporteInstance: tareaSoporteInstance])
                    return
                }
            }
            tareaSoporteInstance.properties = params
            if (!tareaSoporteInstance.hasErrors() && tareaSoporteInstance.save(flush: true)) {
                flash.message = "${message(code: 'default.updated.message', args: [message(code: 'tareaSoporte.label', default: 'TareaSoporte'), tareaSoporteInstance.id])}"
                redirect(action: "show", id: tareaSoporteInstance.id)
            }
            else {
                render(view: "edit", model: [tareaSoporteInstance: tareaSoporteInstance])
            }
        }
        else {
            flash.message = "${message(code: 'default.not.found.message', args: [message(code: 'tareaSoporte.label', default: 'TareaSoporte'), params.id])}"
            redirect(action: "list")
        }
    }

    def delete = {
        def tareaSoporteInstance = TareaSoporte.get(params.id)
        if (tareaSoporteInstance) {
            try {
                tareaSoporteInstance.delete(flush: true)
                flash.message = "${message(code: 'default.deleted.message', args: [message(code: 'tareaSoporte.label', default: 'TareaSoporte'), params.id])}"
                redirect(action: "list")
            }
            catch (org.springframework.dao.DataIntegrityViolationException e) {
                flash.message = "${message(code: 'default.not.deleted.message', args: [message(code: 'tareaSoporte.label', default: 'TareaSoporte'), params.id])}"
                redirect(action: "show", id: params.id)
            }
        }
        else {
            flash.message = "${message(code: 'default.not.found.message', args: [message(code: 'tareaSoporte.label', default: 'TareaSoporte'), params.id])}"
            redirect(action: "list")
        }
    }
}
