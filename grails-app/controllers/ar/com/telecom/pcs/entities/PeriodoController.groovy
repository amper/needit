package ar.com.telecom.pcs.entities

class PeriodoController {
	
	static adminMenu = false

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    def index = {
        redirect(action: "list", params: params)
    }

    def list = {
        params.max = Math.min(params.max ? params.int('max') : 10, 100)
        [periodoInstanceList: Periodo.list(params), periodoInstanceTotal: Periodo.count()]
    }

    def create = {
        def periodoInstance = new Periodo()
        periodoInstance.properties = params
        return [periodoInstance: periodoInstance]
    }

    def save = {
        def periodoInstance = new Periodo(params)
        if (periodoInstance.save(flush: true)) {
            flash.message = "${message(code: 'default.created.message', args: [message(code: 'periodo.label', default: 'Periodo'), periodoInstance.id])}"
            redirect(action: "show", id: periodoInstance.id)
        }
        else {
            render(view: "create", model: [periodoInstance: periodoInstance])
        }
    }

    def show = {
        def periodoInstance = Periodo.get(params.id)
        if (!periodoInstance) {
            flash.message = "${message(code: 'default.not.found.message', args: [message(code: 'periodo.label', default: 'Periodo'), params.id])}"
            redirect(action: "list")
        }
        else {
            [periodoInstance: periodoInstance]
        }
    }

    def edit = {
        def periodoInstance = Periodo.get(params.id)
        if (!periodoInstance) {
            flash.message = "${message(code: 'default.not.found.message', args: [message(code: 'periodo.label', default: 'Periodo'), params.id])}"
            redirect(action: "list")
        }
        else {
            return [periodoInstance: periodoInstance]
        }
    }

    def update = {
        def periodoInstance = Periodo.get(params.id)
        if (periodoInstance) {
            if (params.version) {
                def version = params.version.toLong()
                if (periodoInstance.version > version) {
                    
                    periodoInstance.errors.rejectValue("version", "default.optimistic.locking.failure", [message(code: 'periodo.label', default: 'Periodo')] as Object[], "Another user has updated this Periodo while you were editing")
                    render(view: "edit", model: [periodoInstance: periodoInstance])
                    return
                }
            }
            periodoInstance.properties = params
            if (!periodoInstance.hasErrors() && periodoInstance.save(flush: true)) {
                flash.message = "${message(code: 'default.updated.message', args: [message(code: 'periodo.label', default: 'Periodo'), periodoInstance.id])}"
                redirect(action: "show", id: periodoInstance.id)
            }
            else {
                render(view: "edit", model: [periodoInstance: periodoInstance])
            }
        }
        else {
            flash.message = "${message(code: 'default.not.found.message', args: [message(code: 'periodo.label', default: 'Periodo'), params.id])}"
            redirect(action: "list")
        }
    }

    def delete = {
        def periodoInstance = Periodo.get(params.id)
        if (periodoInstance) {
            try {
                periodoInstance.delete(flush: true)
                flash.message = "${message(code: 'default.deleted.message', args: [message(code: 'periodo.label', default: 'Periodo'), params.id])}"
                redirect(action: "list")
            }
            catch (org.springframework.dao.DataIntegrityViolationException e) {
                flash.message = "${message(code: 'default.not.deleted.message', args: [message(code: 'periodo.label', default: 'Periodo'), params.id])}"
                redirect(action: "show", id: params.id)
            }
        }
        else {
            flash.message = "${message(code: 'default.not.found.message', args: [message(code: 'periodo.label', default: 'Periodo'), params.id])}"
            redirect(action: "list")
        }
    }
}
