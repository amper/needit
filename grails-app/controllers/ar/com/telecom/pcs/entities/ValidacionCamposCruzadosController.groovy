package ar.com.telecom.pcs.entities

class ValidacionCamposCruzadosController {
	
	static adminMenu = true

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    def index = {
        redirect(action: "list", params: params)
    }

    def list = {
        params.max = Math.min(params.max ? params.int('max') : 10, 100)
        [validacionCamposCruzadosInstanceList: ValidacionCamposCruzados.list(params), validacionCamposCruzadosInstanceTotal: ValidacionCamposCruzados.count()]
    }

    def create = {
        def validacionCamposCruzadosInstance = new ValidacionCamposCruzados()
        validacionCamposCruzadosInstance.properties = params
        return [validacionCamposCruzadosInstance: validacionCamposCruzadosInstance]
    }

    def save = {
        def validacionCamposCruzadosInstance = new ValidacionCamposCruzados(params)
        if (validacionCamposCruzadosInstance.save(flush: true)) {
            flash.message = "${message(code: 'default.created.message', args: [message(code: 'validacionCamposCruzados.label', default: 'ValidacionCamposCruzados'), validacionCamposCruzadosInstance.id])}"
            redirect(action: "show", id: validacionCamposCruzadosInstance.id)
        }
        else {
            render(view: "create", model: [validacionCamposCruzadosInstance: validacionCamposCruzadosInstance])
        }
    }

    def show = {
        def validacionCamposCruzadosInstance = ValidacionCamposCruzados.get(params.id)
        if (!validacionCamposCruzadosInstance) {
            flash.message = "${message(code: 'default.not.found.message', args: [message(code: 'validacionCamposCruzados.label', default: 'ValidacionCamposCruzados'), params.id])}"
            redirect(action: "list")
        }
        else {
            [validacionCamposCruzadosInstance: validacionCamposCruzadosInstance]
        }
    }

    def edit = {
        def validacionCamposCruzadosInstance = ValidacionCamposCruzados.get(params.id)
        if (!validacionCamposCruzadosInstance) {
            flash.message = "${message(code: 'default.not.found.message', args: [message(code: 'validacionCamposCruzados.label', default: 'ValidacionCamposCruzados'), params.id])}"
            redirect(action: "list")
        }
        else {
            return [validacionCamposCruzadosInstance: validacionCamposCruzadosInstance]
        }
    }

    def update = {
        def validacionCamposCruzadosInstance = ValidacionCamposCruzados.get(params.id)
        if (validacionCamposCruzadosInstance) {
            if (params.version) {
                def version = params.version.toLong()
                if (validacionCamposCruzadosInstance.version > version) {
                    
                    validacionCamposCruzadosInstance.errors.rejectValue("version", "default.optimistic.locking.failure", [message(code: 'validacionCamposCruzados.label', default: 'ValidacionCamposCruzados')] as Object[], "Another user has updated this ValidacionCamposCruzados while you were editing")
                    render(view: "edit", model: [validacionCamposCruzadosInstance: validacionCamposCruzadosInstance])
                    return
                }
            }
            validacionCamposCruzadosInstance.properties = params
            if (!validacionCamposCruzadosInstance.hasErrors() && validacionCamposCruzadosInstance.save(flush: true)) {
                flash.message = "${message(code: 'default.updated.message', args: [message(code: 'validacionCamposCruzados.label', default: 'ValidacionCamposCruzados'), validacionCamposCruzadosInstance.id])}"
                redirect(action: "show", id: validacionCamposCruzadosInstance.id)
            }
            else {
                render(view: "edit", model: [validacionCamposCruzadosInstance: validacionCamposCruzadosInstance])
            }
        }
        else {
            flash.message = "${message(code: 'default.not.found.message', args: [message(code: 'validacionCamposCruzados.label', default: 'ValidacionCamposCruzados'), params.id])}"
            redirect(action: "list")
        }
    }

    def delete = {
        def validacionCamposCruzadosInstance = ValidacionCamposCruzados.get(params.id)
        if (validacionCamposCruzadosInstance) {
            try {
                validacionCamposCruzadosInstance.delete(flush: true)
                flash.message = "${message(code: 'default.deleted.message', args: [message(code: 'validacionCamposCruzados.label', default: 'ValidacionCamposCruzados'), params.id])}"
                redirect(action: "list")
            }
            catch (org.springframework.dao.DataIntegrityViolationException e) {
                flash.message = "${message(code: 'default.not.deleted.message', args: [message(code: 'validacionCamposCruzados.label', default: 'ValidacionCamposCruzados'), params.id])}"
                redirect(action: "show", id: params.id)
            }
        }
        else {
            flash.message = "${message(code: 'default.not.found.message', args: [message(code: 'validacionCamposCruzados.label', default: 'ValidacionCamposCruzados'), params.id])}"
            redirect(action: "list")
        }
    }
}
