package ar.com.telecom.pcs.entities

class GradoSatisfaccionController {
	
	static adminMenu = false

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    def index = {
        redirect(action: "list", params: params)
    }

    def list = {
        params.max = Math.min(params.max ? params.int('max') : 10, 100)
        [gradoSatisfaccionInstanceList: GradoSatisfaccion.list(params), gradoSatisfaccionInstanceTotal: GradoSatisfaccion.count()]
    }

    def create = {
        def gradoSatisfaccionInstance = new GradoSatisfaccion()
        gradoSatisfaccionInstance.properties = params
        return [gradoSatisfaccionInstance: gradoSatisfaccionInstance]
    }

    def save = {
        def gradoSatisfaccionInstance = new GradoSatisfaccion(params)
        if (gradoSatisfaccionInstance.save(flush: true)) {
            flash.message = "${message(code: 'default.created.message', args: [message(code: 'gradoSatisfaccion.label', default: 'GradoSatisfaccion'), gradoSatisfaccionInstance.id])}"
            redirect(action: "show", id: gradoSatisfaccionInstance.id)
        }
        else {
            render(view: "create", model: [gradoSatisfaccionInstance: gradoSatisfaccionInstance])
        }
    }

    def show = {
        def gradoSatisfaccionInstance = GradoSatisfaccion.get(params.id)
        if (!gradoSatisfaccionInstance) {
            flash.message = "${message(code: 'default.not.found.message', args: [message(code: 'gradoSatisfaccion.label', default: 'GradoSatisfaccion'), params.id])}"
            redirect(action: "list")
        }
        else {
            [gradoSatisfaccionInstance: gradoSatisfaccionInstance]
        }
    }

    def edit = {
        def gradoSatisfaccionInstance = GradoSatisfaccion.get(params.id)
        if (!gradoSatisfaccionInstance) {
            flash.message = "${message(code: 'default.not.found.message', args: [message(code: 'gradoSatisfaccion.label', default: 'GradoSatisfaccion'), params.id])}"
            redirect(action: "list")
        }
        else {
            return [gradoSatisfaccionInstance: gradoSatisfaccionInstance]
        }
    }

    def update = {
        def gradoSatisfaccionInstance = GradoSatisfaccion.get(params.id)
        if (gradoSatisfaccionInstance) {
            if (params.version) {
                def version = params.version.toLong()
                if (gradoSatisfaccionInstance.version > version) {
                    
                    gradoSatisfaccionInstance.errors.rejectValue("version", "default.optimistic.locking.failure", [message(code: 'gradoSatisfaccion.label', default: 'GradoSatisfaccion')] as Object[], "Another user has updated this GradoSatisfaccion while you were editing")
                    render(view: "edit", model: [gradoSatisfaccionInstance: gradoSatisfaccionInstance])
                    return
                }
            }
            gradoSatisfaccionInstance.properties = params
            if (!gradoSatisfaccionInstance.hasErrors() && gradoSatisfaccionInstance.save(flush: true)) {
                flash.message = "${message(code: 'default.updated.message', args: [message(code: 'gradoSatisfaccion.label', default: 'GradoSatisfaccion'), gradoSatisfaccionInstance.id])}"
                redirect(action: "show", id: gradoSatisfaccionInstance.id)
            }
            else {
                render(view: "edit", model: [gradoSatisfaccionInstance: gradoSatisfaccionInstance])
            }
        }
        else {
            flash.message = "${message(code: 'default.not.found.message', args: [message(code: 'gradoSatisfaccion.label', default: 'GradoSatisfaccion'), params.id])}"
            redirect(action: "list")
        }
    }

    def delete = {
        def gradoSatisfaccionInstance = GradoSatisfaccion.get(params.id)
        if (gradoSatisfaccionInstance) {
            try {
                gradoSatisfaccionInstance.delete(flush: true)
                flash.message = "${message(code: 'default.deleted.message', args: [message(code: 'gradoSatisfaccion.label', default: 'GradoSatisfaccion'), params.id])}"
                redirect(action: "list")
            }
            catch (org.springframework.dao.DataIntegrityViolationException e) {
                flash.message = "${message(code: 'default.not.deleted.message', args: [message(code: 'gradoSatisfaccion.label', default: 'GradoSatisfaccion'), params.id])}"
                redirect(action: "show", id: params.id)
            }
        }
        else {
            flash.message = "${message(code: 'default.not.found.message', args: [message(code: 'gradoSatisfaccion.label', default: 'GradoSatisfaccion'), params.id])}"
            redirect(action: "list")
        }
    }
}
