package ar.com.telecom.pcs.entities

class OtroCostoController {
	
	static adminMenu = false

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    def index = {
        redirect(action: "list", params: params)
    }

    def list = {
        params.max = Math.min(params.max ? params.int('max') : 10, 100)
        [otroCostoInstanceList: OtroCosto.list(params), otroCostoInstanceTotal: OtroCosto.count()]
    }

    def create = {
        def otroCostoInstance = new OtroCosto()
        otroCostoInstance.properties = params
        return [otroCostoInstance: otroCostoInstance]
    }

    def save = {
        def otroCostoInstance = new OtroCosto(params)
        if (otroCostoInstance.save(flush: true)) {
            flash.message = "${message(code: 'default.created.message', args: [message(code: 'otroCosto.label', default: 'OtroCosto'), otroCostoInstance.id])}"
            redirect(action: "show", id: otroCostoInstance.id)
        }
        else {
            render(view: "create", model: [otroCostoInstance: otroCostoInstance])
        }
    }

    def show = {
        def otroCostoInstance = OtroCosto.get(params.id)
        if (!otroCostoInstance) {
            flash.message = "${message(code: 'default.not.found.message', args: [message(code: 'otroCosto.label', default: 'OtroCosto'), params.id])}"
            redirect(action: "list")
        }
        else {
            [otroCostoInstance: otroCostoInstance]
        }
    }

    def edit = {
        def otroCostoInstance = OtroCosto.get(params.id)
        if (!otroCostoInstance) {
            flash.message = "${message(code: 'default.not.found.message', args: [message(code: 'otroCosto.label', default: 'OtroCosto'), params.id])}"
            redirect(action: "list")
        }
        else {
            return [otroCostoInstance: otroCostoInstance]
        }
    }

    def update = {
        def otroCostoInstance = OtroCosto.get(params.id)
        if (otroCostoInstance) {
            if (params.version) {
                def version = params.version.toLong()
                if (otroCostoInstance.version > version) {
                    
                    otroCostoInstance.errors.rejectValue("version", "default.optimistic.locking.failure", [message(code: 'otroCosto.label', default: 'OtroCosto')] as Object[], "Another user has updated this OtroCosto while you were editing")
                    render(view: "edit", model: [otroCostoInstance: otroCostoInstance])
                    return
                }
            }
            otroCostoInstance.properties = params
            if (!otroCostoInstance.hasErrors() && otroCostoInstance.save(flush: true)) {
                flash.message = "${message(code: 'default.updated.message', args: [message(code: 'otroCosto.label', default: 'OtroCosto'), otroCostoInstance.id])}"
                redirect(action: "show", id: otroCostoInstance.id)
            }
            else {
                render(view: "edit", model: [otroCostoInstance: otroCostoInstance])
            }
        }
        else {
            flash.message = "${message(code: 'default.not.found.message', args: [message(code: 'otroCosto.label', default: 'OtroCosto'), params.id])}"
            redirect(action: "list")
        }
    }

    def delete = {
        def otroCostoInstance = OtroCosto.get(params.id)
        if (otroCostoInstance) {
            try {
                otroCostoInstance.delete(flush: true)
                flash.message = "${message(code: 'default.deleted.message', args: [message(code: 'otroCosto.label', default: 'OtroCosto'), params.id])}"
                redirect(action: "list")
            }
            catch (org.springframework.dao.DataIntegrityViolationException e) {
                flash.message = "${message(code: 'default.not.deleted.message', args: [message(code: 'otroCosto.label', default: 'OtroCosto'), params.id])}"
                redirect(action: "show", id: params.id)
            }
        }
        else {
            flash.message = "${message(code: 'default.not.found.message', args: [message(code: 'otroCosto.label', default: 'OtroCosto'), params.id])}"
            redirect(action: "list")
        }
    }
}
