package ar.com.telecom.pcs.entities

class EstadoActividadController {
	
	static adminMenu = true

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    def index = {
        redirect(action: "list", params: params)
    }

    def list = {
        params.max = Math.min(params.max ? params.int('max') : 10, 100)
        [estadoActividadInstanceList: EstadoActividad.list(params), estadoActividadInstanceTotal: EstadoActividad.count()]
    }

    def create = {
        def estadoActividadInstance = new EstadoActividad()
        estadoActividadInstance.properties = params
        return [estadoActividadInstance: estadoActividadInstance]
    }

    def save = {
        def estadoActividadInstance = new EstadoActividad(params)
        if (estadoActividadInstance.save(flush: true)) {
            flash.message = "${message(code: 'default.created.message', args: [message(code: 'estadoActividad.label', default: 'EstadoActividad'), estadoActividadInstance.id])}"
            redirect(action: "show", id: estadoActividadInstance.id)
        }
        else {
            render(view: "create", model: [estadoActividadInstance: estadoActividadInstance])
        }
    }

    def show = {
        def estadoActividadInstance = EstadoActividad.get(params.id)
        if (!estadoActividadInstance) {
            flash.message = "${message(code: 'default.not.found.message', args: [message(code: 'estadoActividad.label', default: 'EstadoActividad'), params.id])}"
            redirect(action: "list")
        }
        else {
            [estadoActividadInstance: estadoActividadInstance]
        }
    }

    def edit = {
        def estadoActividadInstance = EstadoActividad.get(params.id)
        if (!estadoActividadInstance) {
            flash.message = "${message(code: 'default.not.found.message', args: [message(code: 'estadoActividad.label', default: 'EstadoActividad'), params.id])}"
            redirect(action: "list")
        }
        else {
            return [estadoActividadInstance: estadoActividadInstance]
        }
    }

    def update = {
        def estadoActividadInstance = EstadoActividad.get(params.id)
        if (estadoActividadInstance) {
            if (params.version) {
                def version = params.version.toLong()
                if (estadoActividadInstance.version > version) {
                    
                    estadoActividadInstance.errors.rejectValue("version", "default.optimistic.locking.failure", [message(code: 'estadoActividad.label', default: 'EstadoActividad')] as Object[], "Another user has updated this EstadoActividad while you were editing")
                    render(view: "edit", model: [estadoActividadInstance: estadoActividadInstance])
                    return
                }
            }
            estadoActividadInstance.properties = params
            if (!estadoActividadInstance.hasErrors() && estadoActividadInstance.save(flush: true)) {
                flash.message = "${message(code: 'default.updated.message', args: [message(code: 'estadoActividad.label', default: 'EstadoActividad'), estadoActividadInstance.id])}"
                redirect(action: "show", id: estadoActividadInstance.id)
            }
            else {
                render(view: "edit", model: [estadoActividadInstance: estadoActividadInstance])
            }
        }
        else {
            flash.message = "${message(code: 'default.not.found.message', args: [message(code: 'estadoActividad.label', default: 'EstadoActividad'), params.id])}"
            redirect(action: "list")
        }
    }

    def delete = {
        def estadoActividadInstance = EstadoActividad.get(params.id)
        if (estadoActividadInstance) {
            try {
                estadoActividadInstance.delete(flush: true)
                flash.message = "${message(code: 'default.deleted.message', args: [message(code: 'estadoActividad.label', default: 'EstadoActividad'), params.id])}"
                redirect(action: "list")
            }
            catch (org.springframework.dao.DataIntegrityViolationException e) {
                flash.message = "${message(code: 'default.not.deleted.message', args: [message(code: 'estadoActividad.label', default: 'EstadoActividad'), params.id])}"
                redirect(action: "show", id: params.id)
            }
        }
        else {
            flash.message = "${message(code: 'default.not.found.message', args: [message(code: 'estadoActividad.label', default: 'EstadoActividad'), params.id])}"
            redirect(action: "list")
        }
    }
}
