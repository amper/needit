package ar.com.telecom.pcs.entities

class CriterioTailoringController {
	
	static adminMenu = true

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    def index = {
        redirect(action: "list", params: params)
    }

    def list = {
        params.max = Math.min(params.max ? params.int('max') : 10, 100)
        [criterioTailoringInstanceList: CriterioTailoring.list(params), criterioTailoringInstanceTotal: CriterioTailoring.count()]
    }

    def create = {
        def criterioTailoringInstance = new CriterioTailoring()
        criterioTailoringInstance.properties = params
        return [criterioTailoringInstance: criterioTailoringInstance]
    }

    def save = {
        def criterioTailoringInstance = new CriterioTailoring(params)
        if (criterioTailoringInstance.save(flush: true)) {
            flash.message = "${message(code: 'default.created.message', args: [message(code: 'criterioTailoring.label', default: 'CriterioTailoring'), criterioTailoringInstance.id])}"
            redirect(action: "show", id: criterioTailoringInstance.id)
        }
        else {
            render(view: "create", model: [criterioTailoringInstance: criterioTailoringInstance])
        }
    }

    def show = {
        def criterioTailoringInstance = CriterioTailoring.get(params.id)
        if (!criterioTailoringInstance) {
            flash.message = "${message(code: 'default.not.found.message', args: [message(code: 'criterioTailoring.label', default: 'CriterioTailoring'), params.id])}"
            redirect(action: "list")
        }
        else {
            [criterioTailoringInstance: criterioTailoringInstance]
        }
    }

    def edit = {
        def criterioTailoringInstance = CriterioTailoring.get(params.id)
        if (!criterioTailoringInstance) {
            flash.message = "${message(code: 'default.not.found.message', args: [message(code: 'criterioTailoring.label', default: 'CriterioTailoring'), params.id])}"
            redirect(action: "list")
        }
        else {
            return [criterioTailoringInstance: criterioTailoringInstance]
        }
    }

    def update = {
        def criterioTailoringInstance = CriterioTailoring.get(params.id)
        if (criterioTailoringInstance) {
            if (params.version) {
                def version = params.version.toLong()
                if (criterioTailoringInstance.version > version) {
                    
                    criterioTailoringInstance.errors.rejectValue("version", "default.optimistic.locking.failure", [message(code: 'criterioTailoring.label', default: 'CriterioTailoring')] as Object[], "Another user has updated this CriterioTailoring while you were editing")
                    render(view: "edit", model: [criterioTailoringInstance: criterioTailoringInstance])
                    return
                }
            }
            criterioTailoringInstance.properties = params
            if (!criterioTailoringInstance.hasErrors() && criterioTailoringInstance.save(flush: true)) {
                flash.message = "${message(code: 'default.updated.message', args: [message(code: 'criterioTailoring.label', default: 'CriterioTailoring'), criterioTailoringInstance.id])}"
                redirect(action: "show", id: criterioTailoringInstance.id)
            }
            else {
                render(view: "edit", model: [criterioTailoringInstance: criterioTailoringInstance])
            }
        }
        else {
            flash.message = "${message(code: 'default.not.found.message', args: [message(code: 'criterioTailoring.label', default: 'CriterioTailoring'), params.id])}"
            redirect(action: "list")
        }
    }

    def delete = {
        def criterioTailoringInstance = CriterioTailoring.get(params.id)
        if (criterioTailoringInstance) {
            try {
                criterioTailoringInstance.delete(flush: true)
                flash.message = "${message(code: 'default.deleted.message', args: [message(code: 'criterioTailoring.label', default: 'CriterioTailoring'), params.id])}"
                redirect(action: "list")
            }
            catch (org.springframework.dao.DataIntegrityViolationException e) {
                flash.message = "${message(code: 'default.not.deleted.message', args: [message(code: 'criterioTailoring.label', default: 'CriterioTailoring'), params.id])}"
                redirect(action: "show", id: params.id)
            }
        }
        else {
            flash.message = "${message(code: 'default.not.found.message', args: [message(code: 'criterioTailoring.label', default: 'CriterioTailoring'), params.id])}"
            redirect(action: "list")
        }
    }
}
