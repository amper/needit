package ar.com.telecom.pcs.entities

class TipoGestionController {
	
	static adminMenu = true

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    def index = {
        redirect(action: "list", params: params)
    }

    def list = {
        params.max = Math.min(params.max ? params.int('max') : 10, 100)
        [tipoGestionInstanceList: TipoGestion.list(params), tipoGestionInstanceTotal: TipoGestion.count()]
    }

    def create = {
        def tipoGestionInstance = new TipoGestion()
        tipoGestionInstance.properties = params
        return [tipoGestionInstance: tipoGestionInstance]
    }

    def save = {
        def tipoGestionInstance = new TipoGestion(params)
        if (tipoGestionInstance.save(flush: true)) {
            flash.message = "${message(code: 'default.created.message', args: [message(code: 'tipoGestion.label', default: 'TipoGestion'), tipoGestionInstance.id])}"
            redirect(action: "show", id: tipoGestionInstance.id)
        }
        else {
            render(view: "create", model: [tipoGestionInstance: tipoGestionInstance])
        }
    }

    def show = {
        def tipoGestionInstance = TipoGestion.get(params.id)
        if (!tipoGestionInstance) {
            flash.message = "${message(code: 'default.not.found.message', args: [message(code: 'tipoGestion.label', default: 'TipoGestion'), params.id])}"
            redirect(action: "list")
        }
        else {
            [tipoGestionInstance: tipoGestionInstance]
        }
    }

    def edit = {
        def tipoGestionInstance = TipoGestion.get(params.id)
        if (!tipoGestionInstance) {
            flash.message = "${message(code: 'default.not.found.message', args: [message(code: 'tipoGestion.label', default: 'TipoGestion'), params.id])}"
            redirect(action: "list")
        }
        else {
            return [tipoGestionInstance: tipoGestionInstance]
        }
    }

    def update = {
        def tipoGestionInstance = TipoGestion.get(params.id)
        if (tipoGestionInstance) {
            if (params.version) {
                def version = params.version.toLong()
                if (tipoGestionInstance.version > version) {
                    
                    tipoGestionInstance.errors.rejectValue("version", "default.optimistic.locking.failure", [message(code: 'tipoGestion.label', default: 'TipoGestion')] as Object[], "Another user has updated this TipoGestion while you were editing")
                    render(view: "edit", model: [tipoGestionInstance: tipoGestionInstance])
                    return
                }
            }
            tipoGestionInstance.properties = params
            if (!tipoGestionInstance.hasErrors() && tipoGestionInstance.save(flush: true)) {
                flash.message = "${message(code: 'default.updated.message', args: [message(code: 'tipoGestion.label', default: 'TipoGestion'), tipoGestionInstance.id])}"
                redirect(action: "show", id: tipoGestionInstance.id)
            }
            else {
                render(view: "edit", model: [tipoGestionInstance: tipoGestionInstance])
            }
        }
        else {
            flash.message = "${message(code: 'default.not.found.message', args: [message(code: 'tipoGestion.label', default: 'TipoGestion'), params.id])}"
            redirect(action: "list")
        }
    }

    def delete = {
        def tipoGestionInstance = TipoGestion.get(params.id)
        if (tipoGestionInstance) {
            try {
                tipoGestionInstance.delete(flush: true)
                flash.message = "${message(code: 'default.deleted.message', args: [message(code: 'tipoGestion.label', default: 'TipoGestion'), params.id])}"
                redirect(action: "list")
            }
            catch (org.springframework.dao.DataIntegrityViolationException e) {
                flash.message = "${message(code: 'default.not.deleted.message', args: [message(code: 'tipoGestion.label', default: 'TipoGestion'), params.id])}"
                redirect(action: "show", id: params.id)
            }
        }
        else {
            flash.message = "${message(code: 'default.not.found.message', args: [message(code: 'tipoGestion.label', default: 'TipoGestion'), params.id])}"
            redirect(action: "list")
        }
    }
}
