package ar.com.telecom.pcs.entities

class TipoActividadController {
	
	static adminMenu = true

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    def index = {
        redirect(action: "list", params: params)
    }

    def list = {
        params.max = Math.min(params.max ? params.int('max') : 10, 100)
        [tipoActividadInstanceList: TipoActividad.list(params), tipoActividadInstanceTotal: TipoActividad.count()]
    }

    def create = {
        def tipoActividadInstance = new TipoActividad()
        tipoActividadInstance.properties = params
        return [tipoActividadInstance: tipoActividadInstance]
    }

    def save = {
        def tipoActividadInstance = new TipoActividad(params)
        if (tipoActividadInstance.save(flush: true)) {
            flash.message = "${message(code: 'default.created.message', args: [message(code: 'tipoActividad.label', default: 'TipoActividad'), tipoActividadInstance.id])}"
            redirect(action: "show", id: tipoActividadInstance.id)
        }
        else {
            render(view: "create", model: [tipoActividadInstance: tipoActividadInstance])
        }
    }

    def show = {
        def tipoActividadInstance = TipoActividad.get(params.id)
        if (!tipoActividadInstance) {
            flash.message = "${message(code: 'default.not.found.message', args: [message(code: 'tipoActividad.label', default: 'TipoActividad'), params.id])}"
            redirect(action: "list")
        }
        else {
            [tipoActividadInstance: tipoActividadInstance]
        }
    }

    def edit = {
        def tipoActividadInstance = TipoActividad.get(params.id)
        if (!tipoActividadInstance) {
            flash.message = "${message(code: 'default.not.found.message', args: [message(code: 'tipoActividad.label', default: 'TipoActividad'), params.id])}"
            redirect(action: "list")
        }
        else {
            return [tipoActividadInstance: tipoActividadInstance]
        }
    }

    def update = {
        def tipoActividadInstance = TipoActividad.get(params.id)
        if (tipoActividadInstance) {
            if (params.version) {
                def version = params.version.toLong()
                if (tipoActividadInstance.version > version) {
                    
                    tipoActividadInstance.errors.rejectValue("version", "default.optimistic.locking.failure", [message(code: 'tipoActividad.label', default: 'TipoActividad')] as Object[], "Another user has updated this TipoActividad while you were editing")
                    render(view: "edit", model: [tipoActividadInstance: tipoActividadInstance])
                    return
                }
            }
            tipoActividadInstance.properties = params
            if (!tipoActividadInstance.hasErrors() && tipoActividadInstance.save(flush: true)) {
                flash.message = "${message(code: 'default.updated.message', args: [message(code: 'tipoActividad.label', default: 'TipoActividad'), tipoActividadInstance.id])}"
                redirect(action: "show", id: tipoActividadInstance.id)
            }
            else {
                render(view: "edit", model: [tipoActividadInstance: tipoActividadInstance])
            }
        }
        else {
            flash.message = "${message(code: 'default.not.found.message', args: [message(code: 'tipoActividad.label', default: 'TipoActividad'), params.id])}"
            redirect(action: "list")
        }
    }

    def delete = {
        def tipoActividadInstance = TipoActividad.get(params.id)
        if (tipoActividadInstance) {
            try {
                tipoActividadInstance.delete(flush: true)
                flash.message = "${message(code: 'default.deleted.message', args: [message(code: 'tipoActividad.label', default: 'TipoActividad'), params.id])}"
                redirect(action: "list")
            }
            catch (org.springframework.dao.DataIntegrityViolationException e) {
                flash.message = "${message(code: 'default.not.deleted.message', args: [message(code: 'tipoActividad.label', default: 'TipoActividad'), params.id])}"
                redirect(action: "show", id: params.id)
            }
        }
        else {
            flash.message = "${message(code: 'default.not.found.message', args: [message(code: 'tipoActividad.label', default: 'TipoActividad'), params.id])}"
            redirect(action: "list")
        }
    }
}
