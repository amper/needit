package ar.com.telecom.pcs.entities

class GrupoLDAPController {
	
	static adminMenu = false

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    def index = {
        redirect(action: "list", params: params)
    }

    def list = {
        params.max = Math.min(params.max ? params.int('max') : 10, 100)
        [grupoLDAPInstanceList: GrupoLDAP.list(params), grupoLDAPInstanceTotal: GrupoLDAP.count()]
    }

    def create = {
        def grupoLDAPInstance = new GrupoLDAP()
        grupoLDAPInstance.properties = params
        return [grupoLDAPInstance: grupoLDAPInstance]
    }

    def save = {
        def grupoLDAPInstance = new GrupoLDAP(params)
        if (grupoLDAPInstance.save(flush: true)) {
            flash.message = "${message(code: 'default.created.message', args: [message(code: 'grupoLDAP.label', default: 'GrupoLDAP'), grupoLDAPInstance.id])}"
            redirect(action: "show", id: grupoLDAPInstance.id)
        }
        else {
            render(view: "create", model: [grupoLDAPInstance: grupoLDAPInstance])
        }
    }

    def show = {
        def grupoLDAPInstance = GrupoLDAP.get(params.id)
        if (!grupoLDAPInstance) {
            flash.message = "${message(code: 'default.not.found.message', args: [message(code: 'grupoLDAP.label', default: 'GrupoLDAP'), params.id])}"
            redirect(action: "list")
        }
        else {
            [grupoLDAPInstance: grupoLDAPInstance]
        }
    }

    def edit = {
        def grupoLDAPInstance = GrupoLDAP.get(params.id)
        if (!grupoLDAPInstance) {
            flash.message = "${message(code: 'default.not.found.message', args: [message(code: 'grupoLDAP.label', default: 'GrupoLDAP'), params.id])}"
            redirect(action: "list")
        }
        else {
            return [grupoLDAPInstance: grupoLDAPInstance]
        }
    }

    def update = {
        def grupoLDAPInstance = GrupoLDAP.get(params.id)
        if (grupoLDAPInstance) {
            if (params.version) {
                def version = params.version.toLong()
                if (grupoLDAPInstance.version > version) {
                    
                    grupoLDAPInstance.errors.rejectValue("version", "default.optimistic.locking.failure", [message(code: 'grupoLDAP.label', default: 'GrupoLDAP')] as Object[], "Another user has updated this GrupoLDAP while you were editing")
                    render(view: "edit", model: [grupoLDAPInstance: grupoLDAPInstance])
                    return
                }
            }
            grupoLDAPInstance.properties = params
            if (!grupoLDAPInstance.hasErrors() && grupoLDAPInstance.save(flush: true)) {
                flash.message = "${message(code: 'default.updated.message', args: [message(code: 'grupoLDAP.label', default: 'GrupoLDAP'), grupoLDAPInstance.id])}"
                redirect(action: "show", id: grupoLDAPInstance.id)
            }
            else {
                render(view: "edit", model: [grupoLDAPInstance: grupoLDAPInstance])
            }
        }
        else {
            flash.message = "${message(code: 'default.not.found.message', args: [message(code: 'grupoLDAP.label', default: 'GrupoLDAP'), params.id])}"
            redirect(action: "list")
        }
    }

    def delete = {
        def grupoLDAPInstance = GrupoLDAP.get(params.id)
        if (grupoLDAPInstance) {
            try {
                grupoLDAPInstance.delete(flush: true)
                flash.message = "${message(code: 'default.deleted.message', args: [message(code: 'grupoLDAP.label', default: 'GrupoLDAP'), params.id])}"
                redirect(action: "list")
            }
            catch (org.springframework.dao.DataIntegrityViolationException e) {
                flash.message = "${message(code: 'default.not.deleted.message', args: [message(code: 'grupoLDAP.label', default: 'GrupoLDAP'), params.id])}"
                redirect(action: "show", id: params.id)
            }
        }
        else {
            flash.message = "${message(code: 'default.not.found.message', args: [message(code: 'grupoLDAP.label', default: 'GrupoLDAP'), params.id])}"
            redirect(action: "list")
        }
    }
}
