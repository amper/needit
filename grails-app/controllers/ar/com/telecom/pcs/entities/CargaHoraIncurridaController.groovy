package ar.com.telecom.pcs.entities

class CargaHoraIncurridaController {
	
	static adminMenu = false

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    def index = {
        redirect(action: "list", params: params)
    }

    def list = {
        params.max = Math.min(params.max ? params.int('max') : 10, 100)
        [cargaHoraIncurridaInstanceList: CargaHoraIncurrida.list(params), cargaHoraIncurridaInstanceTotal: CargaHoraIncurrida.count()]
    }

    def create = {
        def cargaHoraIncurridaInstance = new CargaHoraIncurrida()
        cargaHoraIncurridaInstance.properties = params
        return [cargaHoraIncurridaInstance: cargaHoraIncurridaInstance]
    }

    def save = {
        def cargaHoraIncurridaInstance = new CargaHoraIncurrida(params)
        if (cargaHoraIncurridaInstance.save(flush: true)) {
            flash.message = "${message(code: 'default.created.message', args: [message(code: 'cargaHoraIncurrida.label', default: 'CargaHoraIncurrida'), cargaHoraIncurridaInstance.id])}"
            redirect(action: "show", id: cargaHoraIncurridaInstance.id)
        }
        else {
            render(view: "create", model: [cargaHoraIncurridaInstance: cargaHoraIncurridaInstance])
        }
    }

    def show = {
        def cargaHoraIncurridaInstance = CargaHoraIncurrida.get(params.id)
        if (!cargaHoraIncurridaInstance) {
            flash.message = "${message(code: 'default.not.found.message', args: [message(code: 'cargaHoraIncurrida.label', default: 'CargaHoraIncurrida'), params.id])}"
            redirect(action: "list")
        }
        else {
            [cargaHoraIncurridaInstance: cargaHoraIncurridaInstance]
        }
    }

    def edit = {
        def cargaHoraIncurridaInstance = CargaHoraIncurrida.get(params.id)
        if (!cargaHoraIncurridaInstance) {
            flash.message = "${message(code: 'default.not.found.message', args: [message(code: 'cargaHoraIncurrida.label', default: 'CargaHoraIncurrida'), params.id])}"
            redirect(action: "list")
        }
        else {
            return [cargaHoraIncurridaInstance: cargaHoraIncurridaInstance]
        }
    }

    def update = {
        def cargaHoraIncurridaInstance = CargaHoraIncurrida.get(params.id)
        if (cargaHoraIncurridaInstance) {
            if (params.version) {
                def version = params.version.toLong()
                if (cargaHoraIncurridaInstance.version > version) {
                    
                    cargaHoraIncurridaInstance.errors.rejectValue("version", "default.optimistic.locking.failure", [message(code: 'cargaHoraIncurrida.label', default: 'CargaHoraIncurrida')] as Object[], "Another user has updated this CargaHoraIncurrida while you were editing")
                    render(view: "edit", model: [cargaHoraIncurridaInstance: cargaHoraIncurridaInstance])
                    return
                }
            }
            cargaHoraIncurridaInstance.properties = params
            if (!cargaHoraIncurridaInstance.hasErrors() && cargaHoraIncurridaInstance.save(flush: true)) {
                flash.message = "${message(code: 'default.updated.message', args: [message(code: 'cargaHoraIncurrida.label', default: 'CargaHoraIncurrida'), cargaHoraIncurridaInstance.id])}"
                redirect(action: "show", id: cargaHoraIncurridaInstance.id)
            }
            else {
                render(view: "edit", model: [cargaHoraIncurridaInstance: cargaHoraIncurridaInstance])
            }
        }
        else {
            flash.message = "${message(code: 'default.not.found.message', args: [message(code: 'cargaHoraIncurrida.label', default: 'CargaHoraIncurrida'), params.id])}"
            redirect(action: "list")
        }
    }

    def delete = {
        def cargaHoraIncurridaInstance = CargaHoraIncurrida.get(params.id)
        if (cargaHoraIncurridaInstance) {
            try {
                cargaHoraIncurridaInstance.delete(flush: true)
                flash.message = "${message(code: 'default.deleted.message', args: [message(code: 'cargaHoraIncurrida.label', default: 'CargaHoraIncurrida'), params.id])}"
                redirect(action: "list")
            }
            catch (org.springframework.dao.DataIntegrityViolationException e) {
                flash.message = "${message(code: 'default.not.deleted.message', args: [message(code: 'cargaHoraIncurrida.label', default: 'CargaHoraIncurrida'), params.id])}"
                redirect(action: "show", id: params.id)
            }
        }
        else {
            flash.message = "${message(code: 'default.not.found.message', args: [message(code: 'cargaHoraIncurrida.label', default: 'CargaHoraIncurrida'), params.id])}"
            redirect(action: "list")
        }
    }
}
