package ar.com.telecom.pcs.entities

class DisenioExternoController {
	
	static adminMenu = false

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    def index = {
        redirect(action: "list", params: params)
    }

    def list = {
        params.max = Math.min(params.max ? params.int('max') : 10, 100)
        [disenioExternoInstanceList: DisenioExterno.list(params), disenioExternoInstanceTotal: DisenioExterno.count()]
    }

    def create = {
        def disenioExternoInstance = new DisenioExterno()
        disenioExternoInstance.properties = params
        return [disenioExternoInstance: disenioExternoInstance]
    }

    def save = {
        def disenioExternoInstance = new DisenioExterno(params)
        if (disenioExternoInstance.save(flush: true)) {
            flash.message = "${message(code: 'default.created.message', args: [message(code: 'disenioExterno.label', default: 'DisenioExterno'), disenioExternoInstance.id])}"
            redirect(action: "show", id: disenioExternoInstance.id)
        }
        else {
            render(view: "create", model: [disenioExternoInstance: disenioExternoInstance])
        }
    }

    def show = {
        def disenioExternoInstance = DisenioExterno.get(params.id)
        if (!disenioExternoInstance) {
            flash.message = "${message(code: 'default.not.found.message', args: [message(code: 'disenioExterno.label', default: 'DisenioExterno'), params.id])}"
            redirect(action: "list")
        }
        else {
            [disenioExternoInstance: disenioExternoInstance]
        }
    }

    def edit = {
        def disenioExternoInstance = DisenioExterno.get(params.id)
        if (!disenioExternoInstance) {
            flash.message = "${message(code: 'default.not.found.message', args: [message(code: 'disenioExterno.label', default: 'DisenioExterno'), params.id])}"
            redirect(action: "list")
        }
        else {
            return [disenioExternoInstance: disenioExternoInstance]
        }
    }

    def update = {
        def disenioExternoInstance = DisenioExterno.get(params.id)
        if (disenioExternoInstance) {
            if (params.version) {
                def version = params.version.toLong()
                if (disenioExternoInstance.version > version) {
                    
                    disenioExternoInstance.errors.rejectValue("version", "default.optimistic.locking.failure", [message(code: 'disenioExterno.label', default: 'DisenioExterno')] as Object[], "Another user has updated this DisenioExterno while you were editing")
                    render(view: "edit", model: [disenioExternoInstance: disenioExternoInstance])
                    return
                }
            }
            disenioExternoInstance.properties = params
            if (!disenioExternoInstance.hasErrors() && disenioExternoInstance.save(flush: true)) {
                flash.message = "${message(code: 'default.updated.message', args: [message(code: 'disenioExterno.label', default: 'DisenioExterno'), disenioExternoInstance.id])}"
                redirect(action: "show", id: disenioExternoInstance.id)
            }
            else {
                render(view: "edit", model: [disenioExternoInstance: disenioExternoInstance])
            }
        }
        else {
            flash.message = "${message(code: 'default.not.found.message', args: [message(code: 'disenioExterno.label', default: 'DisenioExterno'), params.id])}"
            redirect(action: "list")
        }
    }

    def delete = {
        def disenioExternoInstance = DisenioExterno.get(params.id)
        if (disenioExternoInstance) {
            try {
                disenioExternoInstance.delete(flush: true)
                flash.message = "${message(code: 'default.deleted.message', args: [message(code: 'disenioExterno.label', default: 'DisenioExterno'), params.id])}"
                redirect(action: "list")
            }
            catch (org.springframework.dao.DataIntegrityViolationException e) {
                flash.message = "${message(code: 'default.not.deleted.message', args: [message(code: 'disenioExterno.label', default: 'DisenioExterno'), params.id])}"
                redirect(action: "show", id: params.id)
            }
        }
        else {
            flash.message = "${message(code: 'default.not.found.message', args: [message(code: 'disenioExterno.label', default: 'DisenioExterno'), params.id])}"
            redirect(action: "list")
        }
    }
}
