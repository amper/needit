package ar.com.telecom.pcs.entities

class AnexoListaBlancaController {

	static adminMenu = true

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    def index = {
        redirect(action: "list", params: params)
    }

    def list = {
        params.max = Math.min(params.max ? params.int('max') : 10, 100)
        [anexoListaBlancaInstanceList: AnexoListaBlanca.list(params), anexoListaBlancaInstanceTotal: AnexoListaBlanca.count()]
    }

    def create = {
        def anexoListaBlancaInstance = new AnexoListaBlanca()
        anexoListaBlancaInstance.properties = params
        return [anexoListaBlancaInstance: anexoListaBlancaInstance]
    }

    def save = {
        def anexoListaBlancaInstance = new AnexoListaBlanca(params)
        if (anexoListaBlancaInstance.save(flush: true)) {
            flash.message = "${message(code: 'default.created.message', args: [message(code: 'anexoListaBlanca.label', default: 'AnexoListaBlanca'), anexoListaBlancaInstance.id])}"
            redirect(action: "show", id: anexoListaBlancaInstance.id)
        }
        else {
            render(view: "create", model: [anexoListaBlancaInstance: anexoListaBlancaInstance])
        }
    }

    def show = {
        def anexoListaBlancaInstance = AnexoListaBlanca.get(params.id)
        if (!anexoListaBlancaInstance) {
            flash.message = "${message(code: 'default.not.found.message', args: [message(code: 'anexoListaBlanca.label', default: 'AnexoListaBlanca'), params.id])}"
            redirect(action: "list")
        }
        else {
            [anexoListaBlancaInstance: anexoListaBlancaInstance]
        }
    }

    def edit = {
        def anexoListaBlancaInstance = AnexoListaBlanca.get(params.id)
        if (!anexoListaBlancaInstance) {
            flash.message = "${message(code: 'default.not.found.message', args: [message(code: 'anexoListaBlanca.label', default: 'AnexoListaBlanca'), params.id])}"
            redirect(action: "list")
        }
        else {
            return [anexoListaBlancaInstance: anexoListaBlancaInstance]
        }
    }

    def update = {
        def anexoListaBlancaInstance = AnexoListaBlanca.get(params.id)
        if (anexoListaBlancaInstance) {
            if (params.version) {
                def version = params.version.toLong()
                if (anexoListaBlancaInstance.version > version) {
                    
                    anexoListaBlancaInstance.errors.rejectValue("version", "default.optimistic.locking.failure", [message(code: 'anexoListaBlanca.label', default: 'AnexoListaBlanca')] as Object[], "Another user has updated this AnexoListaBlanca while you were editing")
                    render(view: "edit", model: [anexoListaBlancaInstance: anexoListaBlancaInstance])
                    return
                }
            }
            anexoListaBlancaInstance.properties = params
            if (!anexoListaBlancaInstance.hasErrors() && anexoListaBlancaInstance.save(flush: true)) {
                flash.message = "${message(code: 'default.updated.message', args: [message(code: 'anexoListaBlanca.label', default: 'AnexoListaBlanca'), anexoListaBlancaInstance.id])}"
                redirect(action: "show", id: anexoListaBlancaInstance.id)
            }
            else {
                render(view: "edit", model: [anexoListaBlancaInstance: anexoListaBlancaInstance])
            }
        }
        else {
            flash.message = "${message(code: 'default.not.found.message', args: [message(code: 'anexoListaBlanca.label', default: 'AnexoListaBlanca'), params.id])}"
            redirect(action: "list")
        }
    }

    def delete = {
        def anexoListaBlancaInstance = AnexoListaBlanca.get(params.id)
        if (anexoListaBlancaInstance) {
            try {
                anexoListaBlancaInstance.delete(flush: true)
                flash.message = "${message(code: 'default.deleted.message', args: [message(code: 'anexoListaBlanca.label', default: 'AnexoListaBlanca'), params.id])}"
                redirect(action: "list")
            }
            catch (org.springframework.dao.DataIntegrityViolationException e) {
                flash.message = "${message(code: 'default.not.deleted.message', args: [message(code: 'anexoListaBlanca.label', default: 'AnexoListaBlanca'), params.id])}"
                redirect(action: "show", id: params.id)
            }
        }
        else {
            flash.message = "${message(code: 'default.not.found.message', args: [message(code: 'anexoListaBlanca.label', default: 'AnexoListaBlanca'), params.id])}"
            redirect(action: "list")
        }
    }
}
