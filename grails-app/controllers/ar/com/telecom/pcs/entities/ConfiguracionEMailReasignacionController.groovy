package ar.com.telecom.pcs.entities

class ConfiguracionEMailReasignacionController {

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    def index = {
        redirect(action: "list", params: params)
    }

    def list = {
        params.max = Math.min(params.max ? params.int('max') : 10, 100)
        [configuracionEMailReasignacionInstanceList: ConfiguracionEMailReasignacion.list(params), configuracionEMailReasignacionInstanceTotal: ConfiguracionEMailReasignacion.count()]
    }

    def create = {
        def configuracionEMailReasignacionInstance = new ConfiguracionEMailReasignacion()
        configuracionEMailReasignacionInstance.properties = params
        return [configuracionEMailReasignacionInstance: configuracionEMailReasignacionInstance]
    }

    def save = {
        def configuracionEMailReasignacionInstance = new ConfiguracionEMailReasignacion(params)
        if (configuracionEMailReasignacionInstance.save(flush: true)) {
            flash.message = "${message(code: 'default.created.message', args: [message(code: 'configuracionEMailReasignacion.label', default: 'ConfiguracionEMailReasignacion'), configuracionEMailReasignacionInstance.id])}"
            redirect(action: "show", id: configuracionEMailReasignacionInstance.id)
        }
        else {
            render(view: "create", model: [configuracionEMailReasignacionInstance: configuracionEMailReasignacionInstance])
        }
    }

    def show = {
        def configuracionEMailReasignacionInstance = ConfiguracionEMailReasignacion.get(params.id)
        if (!configuracionEMailReasignacionInstance) {
            flash.message = "${message(code: 'default.not.found.message', args: [message(code: 'configuracionEMailReasignacion.label', default: 'ConfiguracionEMailReasignacion'), params.id])}"
            redirect(action: "list")
        }
        else {
            [configuracionEMailReasignacionInstance: configuracionEMailReasignacionInstance]
        }
    }

    def edit = {
        def configuracionEMailReasignacionInstance = ConfiguracionEMailReasignacion.get(params.id)
        if (!configuracionEMailReasignacionInstance) {
            flash.message = "${message(code: 'default.not.found.message', args: [message(code: 'configuracionEMailReasignacion.label', default: 'ConfiguracionEMailReasignacion'), params.id])}"
            redirect(action: "list")
        }
        else {
            return [configuracionEMailReasignacionInstance: configuracionEMailReasignacionInstance]
        }
    }

    def update = {
        def configuracionEMailReasignacionInstance = ConfiguracionEMailReasignacion.get(params.id)
        if (configuracionEMailReasignacionInstance) {
            if (params.version) {
                def version = params.version.toLong()
                if (configuracionEMailReasignacionInstance.version > version) {
                    
                    configuracionEMailReasignacionInstance.errors.rejectValue("version", "default.optimistic.locking.failure", [message(code: 'configuracionEMailReasignacion.label', default: 'ConfiguracionEMailReasignacion')] as Object[], "Another user has updated this ConfiguracionEMailReasignacion while you were editing")
                    render(view: "edit", model: [configuracionEMailReasignacionInstance: configuracionEMailReasignacionInstance])
                    return
                }
            }
            configuracionEMailReasignacionInstance.properties = params
            if (!configuracionEMailReasignacionInstance.hasErrors() && configuracionEMailReasignacionInstance.save(flush: true)) {
                flash.message = "${message(code: 'default.updated.message', args: [message(code: 'configuracionEMailReasignacion.label', default: 'ConfiguracionEMailReasignacion'), configuracionEMailReasignacionInstance.id])}"
                redirect(action: "show", id: configuracionEMailReasignacionInstance.id)
            }
            else {
                render(view: "edit", model: [configuracionEMailReasignacionInstance: configuracionEMailReasignacionInstance])
            }
        }
        else {
            flash.message = "${message(code: 'default.not.found.message', args: [message(code: 'configuracionEMailReasignacion.label', default: 'ConfiguracionEMailReasignacion'), params.id])}"
            redirect(action: "list")
        }
    }

    def delete = {
        def configuracionEMailReasignacionInstance = ConfiguracionEMailReasignacion.get(params.id)
        if (configuracionEMailReasignacionInstance) {
            try {
                configuracionEMailReasignacionInstance.delete(flush: true)
                flash.message = "${message(code: 'default.deleted.message', args: [message(code: 'configuracionEMailReasignacion.label', default: 'ConfiguracionEMailReasignacion'), params.id])}"
                redirect(action: "list")
            }
            catch (org.springframework.dao.DataIntegrityViolationException e) {
                flash.message = "${message(code: 'default.not.deleted.message', args: [message(code: 'configuracionEMailReasignacion.label', default: 'ConfiguracionEMailReasignacion'), params.id])}"
                redirect(action: "show", id: params.id)
            }
        }
        else {
            flash.message = "${message(code: 'default.not.found.message', args: [message(code: 'configuracionEMailReasignacion.label', default: 'ConfiguracionEMailReasignacion'), params.id])}"
            redirect(action: "list")
        }
    }
}
