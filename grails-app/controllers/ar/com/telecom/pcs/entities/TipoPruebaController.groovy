package ar.com.telecom.pcs.entities

class TipoPruebaController {
	
	static adminMenu = true

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    def index = {
        redirect(action: "list", params: params)
    }

    def list = {
        params.max = Math.min(params.max ? params.int('max') : 10, 100)
        [tipoPruebaInstanceList: TipoPrueba.list(params), tipoPruebaInstanceTotal: TipoPrueba.count()]
    }

    def create = {
        def tipoPruebaInstance = new TipoPrueba()
        tipoPruebaInstance.properties = params
        return [tipoPruebaInstance: tipoPruebaInstance]
    }

    def save = {
        def tipoPruebaInstance = new TipoPrueba(params)
        if (tipoPruebaInstance.save(flush: true)) {
            flash.message = "${message(code: 'default.created.message', args: [message(code: 'tipoPrueba.label', default: 'TipoPrueba'), tipoPruebaInstance.id])}"
            redirect(action: "show", id: tipoPruebaInstance.id)
        }
        else {
            render(view: "create", model: [tipoPruebaInstance: tipoPruebaInstance])
        }
    }

    def show = {
        def tipoPruebaInstance = TipoPrueba.get(params.id)
        if (!tipoPruebaInstance) {
            flash.message = "${message(code: 'default.not.found.message', args: [message(code: 'tipoPrueba.label', default: 'TipoPrueba'), params.id])}"
            redirect(action: "list")
        }
        else {
            [tipoPruebaInstance: tipoPruebaInstance]
        }
    }

    def edit = {
        def tipoPruebaInstance = TipoPrueba.get(params.id)
        if (!tipoPruebaInstance) {
            flash.message = "${message(code: 'default.not.found.message', args: [message(code: 'tipoPrueba.label', default: 'TipoPrueba'), params.id])}"
            redirect(action: "list")
        }
        else {
            return [tipoPruebaInstance: tipoPruebaInstance]
        }
    }

    def update = {
        def tipoPruebaInstance = TipoPrueba.get(params.id)
        if (tipoPruebaInstance) {
            if (params.version) {
                def version = params.version.toLong()
                if (tipoPruebaInstance.version > version) {
                    
                    tipoPruebaInstance.errors.rejectValue("version", "default.optimistic.locking.failure", [message(code: 'tipoPrueba.label', default: 'TipoPrueba')] as Object[], "Another user has updated this TipoPrueba while you were editing")
                    render(view: "edit", model: [tipoPruebaInstance: tipoPruebaInstance])
                    return
                }
            }
            tipoPruebaInstance.properties = params
            if (!tipoPruebaInstance.hasErrors() && tipoPruebaInstance.save(flush: true)) {
                flash.message = "${message(code: 'default.updated.message', args: [message(code: 'tipoPrueba.label', default: 'TipoPrueba'), tipoPruebaInstance.id])}"
                redirect(action: "show", id: tipoPruebaInstance.id)
            }
            else {
                render(view: "edit", model: [tipoPruebaInstance: tipoPruebaInstance])
            }
        }
        else {
            flash.message = "${message(code: 'default.not.found.message', args: [message(code: 'tipoPrueba.label', default: 'TipoPrueba'), params.id])}"
            redirect(action: "list")
        }
    }

    def delete = {
        def tipoPruebaInstance = TipoPrueba.get(params.id)
        if (tipoPruebaInstance) {
            try {
                tipoPruebaInstance.delete(flush: true)
                flash.message = "${message(code: 'default.deleted.message', args: [message(code: 'tipoPrueba.label', default: 'TipoPrueba'), params.id])}"
                redirect(action: "list")
            }
            catch (org.springframework.dao.DataIntegrityViolationException e) {
                flash.message = "${message(code: 'default.not.deleted.message', args: [message(code: 'tipoPrueba.label', default: 'TipoPrueba'), params.id])}"
                redirect(action: "show", id: params.id)
            }
        }
        else {
            flash.message = "${message(code: 'default.not.found.message', args: [message(code: 'tipoPrueba.label', default: 'TipoPrueba'), params.id])}"
            redirect(action: "list")
        }
    }
}
