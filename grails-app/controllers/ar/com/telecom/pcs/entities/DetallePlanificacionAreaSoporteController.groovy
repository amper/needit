package ar.com.telecom.pcs.entities

class DetallePlanificacionAreaSoporteController {
	
	static adminMenu = false

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    def index = {
        redirect(action: "list", params: params)
    }

    def list = {
        params.max = Math.min(params.max ? params.int('max') : 10, 100)
        [detallePlanificacionAreaSoporteInstanceList: DetallePlanificacionAreaSoporte.list(params), detallePlanificacionAreaSoporteInstanceTotal: DetallePlanificacionAreaSoporte.count()]
    }

    def create = {
        def detallePlanificacionAreaSoporteInstance = new DetallePlanificacionAreaSoporte()
        detallePlanificacionAreaSoporteInstance.properties = params
        return [detallePlanificacionAreaSoporteInstance: detallePlanificacionAreaSoporteInstance]
    }

    def save = {
        def detallePlanificacionAreaSoporteInstance = new DetallePlanificacionAreaSoporte(params)
        if (detallePlanificacionAreaSoporteInstance.save(flush: true)) {
            flash.message = "${message(code: 'default.created.message', args: [message(code: 'detallePlanificacionAreaSoporte.label', default: 'DetallePlanificacionAreaSoporte'), detallePlanificacionAreaSoporteInstance.id])}"
            redirect(action: "show", id: detallePlanificacionAreaSoporteInstance.id)
        }
        else {
            render(view: "create", model: [detallePlanificacionAreaSoporteInstance: detallePlanificacionAreaSoporteInstance])
        }
    }

    def show = {
        def detallePlanificacionAreaSoporteInstance = DetallePlanificacionAreaSoporte.get(params.id)
        if (!detallePlanificacionAreaSoporteInstance) {
            flash.message = "${message(code: 'default.not.found.message', args: [message(code: 'detallePlanificacionAreaSoporte.label', default: 'DetallePlanificacionAreaSoporte'), params.id])}"
            redirect(action: "list")
        }
        else {
            [detallePlanificacionAreaSoporteInstance: detallePlanificacionAreaSoporteInstance]
        }
    }

    def edit = {
        def detallePlanificacionAreaSoporteInstance = DetallePlanificacionAreaSoporte.get(params.id)
        if (!detallePlanificacionAreaSoporteInstance) {
            flash.message = "${message(code: 'default.not.found.message', args: [message(code: 'detallePlanificacionAreaSoporte.label', default: 'DetallePlanificacionAreaSoporte'), params.id])}"
            redirect(action: "list")
        }
        else {
            return [detallePlanificacionAreaSoporteInstance: detallePlanificacionAreaSoporteInstance]
        }
    }

    def update = {
        def detallePlanificacionAreaSoporteInstance = DetallePlanificacionAreaSoporte.get(params.id)
        if (detallePlanificacionAreaSoporteInstance) {
            if (params.version) {
                def version = params.version.toLong()
                if (detallePlanificacionAreaSoporteInstance.version > version) {
                    
                    detallePlanificacionAreaSoporteInstance.errors.rejectValue("version", "default.optimistic.locking.failure", [message(code: 'detallePlanificacionAreaSoporte.label', default: 'DetallePlanificacionAreaSoporte')] as Object[], "Another user has updated this DetallePlanificacionAreaSoporte while you were editing")
                    render(view: "edit", model: [detallePlanificacionAreaSoporteInstance: detallePlanificacionAreaSoporteInstance])
                    return
                }
            }
            detallePlanificacionAreaSoporteInstance.properties = params
            if (!detallePlanificacionAreaSoporteInstance.hasErrors() && detallePlanificacionAreaSoporteInstance.save(flush: true)) {
                flash.message = "${message(code: 'default.updated.message', args: [message(code: 'detallePlanificacionAreaSoporte.label', default: 'DetallePlanificacionAreaSoporte'), detallePlanificacionAreaSoporteInstance.id])}"
                redirect(action: "show", id: detallePlanificacionAreaSoporteInstance.id)
            }
            else {
                render(view: "edit", model: [detallePlanificacionAreaSoporteInstance: detallePlanificacionAreaSoporteInstance])
            }
        }
        else {
            flash.message = "${message(code: 'default.not.found.message', args: [message(code: 'detallePlanificacionAreaSoporte.label', default: 'DetallePlanificacionAreaSoporte'), params.id])}"
            redirect(action: "list")
        }
    }

    def delete = {
        def detallePlanificacionAreaSoporteInstance = DetallePlanificacionAreaSoporte.get(params.id)
        if (detallePlanificacionAreaSoporteInstance) {
            try {
                detallePlanificacionAreaSoporteInstance.delete(flush: true)
                flash.message = "${message(code: 'default.deleted.message', args: [message(code: 'detallePlanificacionAreaSoporte.label', default: 'DetallePlanificacionAreaSoporte'), params.id])}"
                redirect(action: "list")
            }
            catch (org.springframework.dao.DataIntegrityViolationException e) {
                flash.message = "${message(code: 'default.not.deleted.message', args: [message(code: 'detallePlanificacionAreaSoporte.label', default: 'DetallePlanificacionAreaSoporte'), params.id])}"
                redirect(action: "show", id: params.id)
            }
        }
        else {
            flash.message = "${message(code: 'default.not.found.message', args: [message(code: 'detallePlanificacionAreaSoporte.label', default: 'DetallePlanificacionAreaSoporte'), params.id])}"
            redirect(action: "list")
        }
    }
}
