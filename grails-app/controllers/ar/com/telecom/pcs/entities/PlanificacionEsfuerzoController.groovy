package ar.com.telecom.pcs.entities

class PlanificacionEsfuerzoController {

	static adminMenu = false

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    def index = {
        redirect(action: "list", params: params)
    }

    def list = {
        params.max = Math.min(params.max ? params.int('max') : 10, 100)
        [planificacionEsfuerzoInstanceList: PlanificacionEsfuerzo.list(params), planificacionEsfuerzoInstanceTotal: PlanificacionEsfuerzo.count()]
    }

    def create = {
        def planificacionEsfuerzoInstance = new PlanificacionEsfuerzo()
        planificacionEsfuerzoInstance.properties = params
        return [planificacionEsfuerzoInstance: planificacionEsfuerzoInstance]
    }

    def save = {
        def planificacionEsfuerzoInstance = new PlanificacionEsfuerzo(params)
        if (planificacionEsfuerzoInstance.save(flush: true)) {
            flash.message = "${message(code: 'default.created.message', args: [message(code: 'planificacionEsfuerzo.label', default: 'PlanificacionEsfuerzo'), planificacionEsfuerzoInstance.id])}"
            redirect(action: "show", id: planificacionEsfuerzoInstance.id)
        }
        else {
            render(view: "create", model: [planificacionEsfuerzoInstance: planificacionEsfuerzoInstance])
        }
    }

    def show = {
        def planificacionEsfuerzoInstance = PlanificacionEsfuerzo.get(params.id)
        if (!planificacionEsfuerzoInstance) {
            flash.message = "${message(code: 'default.not.found.message', args: [message(code: 'planificacionEsfuerzo.label', default: 'PlanificacionEsfuerzo'), params.id])}"
            redirect(action: "list")
        }
        else {
            [planificacionEsfuerzoInstance: planificacionEsfuerzoInstance]
        }
    }

    def edit = {
        def planificacionEsfuerzoInstance = PlanificacionEsfuerzo.get(params.id)
        if (!planificacionEsfuerzoInstance) {
            flash.message = "${message(code: 'default.not.found.message', args: [message(code: 'planificacionEsfuerzo.label', default: 'PlanificacionEsfuerzo'), params.id])}"
            redirect(action: "list")
        }
        else {
            return [planificacionEsfuerzoInstance: planificacionEsfuerzoInstance]
        }
    }

    def update = {
        def planificacionEsfuerzoInstance = PlanificacionEsfuerzo.get(params.id)
        if (planificacionEsfuerzoInstance) {
            if (params.version) {
                def version = params.version.toLong()
                if (planificacionEsfuerzoInstance.version > version) {
                    
                    planificacionEsfuerzoInstance.errors.rejectValue("version", "default.optimistic.locking.failure", [message(code: 'planificacionEsfuerzo.label', default: 'PlanificacionEsfuerzo')] as Object[], "Another user has updated this PlanificacionEsfuerzo while you were editing")
                    render(view: "edit", model: [planificacionEsfuerzoInstance: planificacionEsfuerzoInstance])
                    return
                }
            }
            planificacionEsfuerzoInstance.properties = params
            if (!planificacionEsfuerzoInstance.hasErrors() && planificacionEsfuerzoInstance.save(flush: true)) {
                flash.message = "${message(code: 'default.updated.message', args: [message(code: 'planificacionEsfuerzo.label', default: 'PlanificacionEsfuerzo'), planificacionEsfuerzoInstance.id])}"
                redirect(action: "show", id: planificacionEsfuerzoInstance.id)
            }
            else {
                render(view: "edit", model: [planificacionEsfuerzoInstance: planificacionEsfuerzoInstance])
            }
        }
        else {
            flash.message = "${message(code: 'default.not.found.message', args: [message(code: 'planificacionEsfuerzo.label', default: 'PlanificacionEsfuerzo'), params.id])}"
            redirect(action: "list")
        }
    }

    def delete = {
        def planificacionEsfuerzoInstance = PlanificacionEsfuerzo.get(params.id)
        if (planificacionEsfuerzoInstance) {
            try {
                planificacionEsfuerzoInstance.delete(flush: true)
                flash.message = "${message(code: 'default.deleted.message', args: [message(code: 'planificacionEsfuerzo.label', default: 'PlanificacionEsfuerzo'), params.id])}"
                redirect(action: "list")
            }
            catch (org.springframework.dao.DataIntegrityViolationException e) {
                flash.message = "${message(code: 'default.not.deleted.message', args: [message(code: 'planificacionEsfuerzo.label', default: 'PlanificacionEsfuerzo'), params.id])}"
                redirect(action: "show", id: params.id)
            }
        }
        else {
            flash.message = "${message(code: 'default.not.found.message', args: [message(code: 'planificacionEsfuerzo.label', default: 'PlanificacionEsfuerzo'), params.id])}"
            redirect(action: "list")
        }
    }
}
