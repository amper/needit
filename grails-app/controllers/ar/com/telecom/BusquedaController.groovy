package ar.com.telecom

import grails.converters.JSON

import org.codehaus.groovy.grails.web.servlet.mvc.SynchronizerToken
import org.hibernate.criterion.CriteriaSpecification

import ar.com.telecom.exceptions.BusinessException
import ar.com.telecom.pcs.entities.AbstractPedido
import ar.com.telecom.pcs.entities.AreaSoporte
import ar.com.telecom.pcs.entities.Fase
import ar.com.telecom.pcs.entities.GrupoGestionDemanda
import ar.com.telecom.pcs.entities.MacroEstado
import ar.com.telecom.pcs.entities.Pedido
import ar.com.telecom.pcs.entities.RolAplicacion
import ar.com.telecom.pcs.entities.Sistema
import ar.com.telecom.pcs.entities.TipoImpacto
import ar.com.telecom.pcs.entities.Variante
import ar.com.telecom.util.DateUtil

class BusquedaController extends WorkflowController {
	def historialService
	def busquedaService

	def index = {
		redirect(action: "list", params: params)
	}

	def list = {
		def usuarioLogueado = umeService.getUsuarioLogueado()
		def pedidosIntancesList // =  pedidoService.busquedaAvanzadaPedidos(params)

		def result = [pedidosIntancesList: pedidosIntancesList]
		result.putAll(getMapaDatosPedido())
		result
	}

	private def getMapaDatosPedido(){
		def usuarioLogueado = umeService.getUsuarioLogueado()
		def rolGrupos = pedidoService.getRolesGrupos(params, usuarioLogueado)
		def tiposDeBusqueda = ["S":"Est\u00E1ndar", "A":"Avanzada", "P":"Personalizada"]
		//def opciones = ["EQ":"Es igual", "CNT":"Contiene", "PERS":"Personalizado"]
		def opciones = ["EQ":"Es igual", "CNT":"Contiene"]
		def opciones_numericas = ["EQ":"Es igual", "GT":"Es mayor", "LT":"Es menor"]
		def opciones_estados = ["Abierto":"Abierto", "Cancelado":"Cancelado", "Suspendido":"Suspendido", "Finalizado":"Finalizado"]

		def grupos = getGrupos()

		def opcionesMacroEstado = new ArrayList()
		opcionesMacroEstado += ['key': 0, 'value':"Personalizado"]
		MacroEstado.list().each { macro -> opcionesMacroEstado.add(['key': macro.id, 'value':macro.descripcion]) }

		def opcionesRol = new ArrayList()
		opcionesRol += ['codigoRol': 0, 'value':"Personalizado"]
		RolAplicacion.list().each { rol -> opcionesRol.add(['codigoRol': rol.codigoRol, 'value':rol.descripcion]) }

		def opcionesAreaSoporte = new ArrayList()
		opcionesAreaSoporte += ['id': 0, 'value':"Personalizado"]
		AreaSoporte.list().each { areaSoporte -> opcionesAreaSoporte.add(['id': areaSoporte.id, 'value':areaSoporte.descripcion]) }


		def opcionesTipoImpacto = new ArrayList()
		opcionesTipoImpacto += ['id': 0, 'value':"Personalizado"]
		TipoImpacto.list().each { tipoImpacto -> opcionesTipoImpacto.add(['id': tipoImpacto.id, 'value':tipoImpacto.descripcion]) }

		def personsAutocomplete = []//TODO: Debe traer TODOS los usuarios: getUmeService().getAllUsuarios()

		[rolGrupos:rolGrupos, usuarioLogueado:usuarioLogueado, tiposDeBusqueda:tiposDeBusqueda,
					opciones:opciones, opciones_numericas:opciones_numericas, opcionesMacroEstado:opcionesMacroEstado, opcionesRol:opcionesRol,
					opcionesAreaSoporte:opcionesAreaSoporte, opcionesTipoImpacto:opcionesTipoImpacto,
					grupos:grupos, personsAutocomplete:personsAutocomplete, opciones_estados:opciones_estados]
	}

	void setGrupos(params, Variante variante){
		variante.grupos = null
		if (params.multiselect_comboboxGrupo){
			def listGruposSeleccionados = []
			def listadoGruposSeleccionados = busquedaService.convertToList(params.multiselect_comboboxGrupo)//params.multiselect_comboboxGrupo.class.isArray() ? params.multiselect_comboboxGrupo as List : [params.multiselect_comboboxGrupo]
			listadoGruposSeleccionados.each { grupo -> listGruposSeleccionados << "'$grupo'"	}
			variante.grupos = listGruposSeleccionados
		} 
		variante.estados = null
		if (params.multiselect_comboboxEstado){
			def listEstadosSeleccionados = []
			def listadoEstadosSeleccionados = busquedaService.convertToList(params.multiselect_comboboxEstado) //params.multiselect_comboboxEstado.class.isArray() ? params.multiselect_comboboxEstado as List : [params.multiselect_comboboxEstado]
			listadoEstadosSeleccionados.each { estado -> listEstadosSeleccionados << "'$estado'".toString()	}
			variante.estados = listEstadosSeleccionados
		}
	}
	
	void setRoles(params, Variante variante){
		def listadoRolesSeleccionados
		def listRolesSeleccionados = []
		if (params.multiselect_comboboxRol){
			variante.roles = null
			listadoRolesSeleccionados = busquedaService.convertToList(params.multiselect_comboboxRol) //params.multiselect_comboboxRol.class.isArray() ? params.multiselect_comboboxRol as List : [params.multiselect_comboboxRol]
			listadoRolesSeleccionados.each { rol ->	listRolesSeleccionados << "'$rol'"	}
		}else{
			variante.roles?.codigoRol.each { rol ->	listRolesSeleccionados << "'$rol'"	}
		}
		variante.roles = listRolesSeleccionados
	}

	void setParams(params, Variante variante){
		// Colecto los MacroEstados
		def listMacroEstados = busquedaService.convertToList(params.multiselect_comboboxMacroestado)
		variante.macroestados = listMacroEstados.collect { itMacroestado ->
			MacroEstado.findById(itMacroestado);
		}
		log.info "Macroestados seleccionados: " + params.macroestados

		// Colecto las Fases
		def listFases = busquedaService.convertToList(params.multiselect_comboboxFase) //params.multiselect_comboboxFase.class.isArray() ? params.multiselect_comboboxFase as List : [params.multiselect_comboboxFase]
		variante.fases = listFases.collect { itFase ->
			Fase.findById(itFase)
		}
		log.info "Fases seleccionadas: " + params.fases

		// Colecto los Tipos de Impacto
		def listTipoImpacto = busquedaService.convertToList(params.multiselect_comboboxTipoImpacto)
		variante.tiposImpacto = listTipoImpacto.collect { itTipoImpacto ->
			TipoImpacto.findById(itTipoImpacto)
		}
		log.info "Tipos Impactos seleccionadas: " + params.tiposImpacto

		// Colecto las Areas de Soporte
		def listAreaSoporte = busquedaService.convertToList(params.multiselect_comboboxAreaSoporte)
		variante.areasSoporte = listAreaSoporte.collect { itAreaSoporte ->
			AreaSoporte.findById(itAreaSoporte)
		}
	}
		
	def getGrupos() {
		SortedSet result = new TreeSet()
		result = result.plus(GrupoGestionDemanda.list().collect { grupoGD -> grupoGD.grupoLDAP })
		result = result.plus(AreaSoporte.list().collect { areaSoporte -> areaSoporte.grupoAdministrador })
		Sistema.list().each { sistema ->
			def grupos = sistema.gruposGestion
			if (!grupos.isEmpty()) {
				result.addAll(grupos)
			}
		}
		return result
	}

	def obtenerFases = {
		withForm {
			try{
				limpiarParams(params)
				def fases
				if (params?.macroEstadoID){
					fases = historialService.obtenerFases(params?.macroEstadoID) //?.split(","))
				}else{
					fases = historialService.obtenerFases()
				}
			render (template: "/templates/comboFasesHistorial", model:[listadoFases:fases, faseID:params?.faseID])
			} catch (Exception e) {
				procesarErrorAjax(e)
			}
		}.invalidToken {
			throw new BusinessException("Operaci\u00F3n no v\u00E1lida. Por favor vuelva a intentarlo.")
		}
	}
	
	def obtenerVariantes = {
		withForm {
			try{
				def listadoVariantes = busquedaService.obtenerVariantes(params?.legajo)
				render (template: "/busqueda/listadoVariantes", model:[listadoVariantes:listadoVariantes])
			} catch (Exception e) {
				procesarErrorAjax(e)
			}
		}.invalidToken {
			throw new BusinessException("Operaci\u00F3n no v\u00E1lida. Por favor vuelva a intentarlo.")
		}
	}
	
	def popUpNuevaVariantes = {
		withForm {
			try{
				render (template: "/busqueda/guardarVariante")
			} catch (Exception e) {
				procesarErrorAjax(e)
			}
		}.invalidToken {
			throw new BusinessException("Operaci\u00F3n no v\u00E1lida. Por favor vuelva a intentarlo.")
		}
	}
	
	def guardarVariante = {
		withForm {
			try{
				def variante = new Variante()
				
				if (params.fechaDesde) {
					params.fechaDesde = DateUtil.toDate(params.fechaDesde)
				}
				if (params.fechaHasta) {
					params.fechaHasta = DateUtil.toDate(params.fechaHasta)
				}
				
				bindData(variante, params)
				
				if (params.multiselect_comboboxMacroestado){
					def listMacroestado = busquedaService.convertToList(params.multiselect_comboboxMacroestado) //params.multiselect_comboboxMacroestado.class.isArray() ? params.multiselect_comboboxMacroestado as List : [params.multiselect_comboboxMacroestado]
					listMacroestado = listMacroestado.collect { itME-> MacroEstado.findById(itME) }
					variante.macroestados = listMacroestado
				}
				if (params.multiselect_comboboxFase){
					def listFases = busquedaService.convertToList(params.multiselect_comboboxFase) //params.multiselect_comboboxFase.class.isArray() ? params.multiselect_comboboxFase as List : [params.multiselect_comboboxFase]
					listFases = listFases.collect { itFase -> Fase.findById(itFase) }
					variante.fases = listFases
				}
				
				if (params.multiselect_comboboxTipoImpacto){
					def listTiposImpacto = busquedaService.convertToList(params.multiselect_comboboxTipoImpacto) //params.multiselect_comboboxTipoImpacto.class.isArray() ? params.multiselect_comboboxTipoImpacto as List : [params.multiselect_comboboxTipoImpacto]
					listTiposImpacto = listTiposImpacto.collect { impacto -> TipoImpacto.findById(impacto) }
					variante.tiposImpacto = listTiposImpacto
				}
				
				if (params.multiselect_comboboxAreaSoporte){
					def listAreaSoporte = busquedaService.convertToList(params.multiselect_comboboxAreaSoporte) //params.multiselect_comboboxAreaSoporte.class.isArray() ? params.multiselect_comboboxAreaSoporte as List : [params.multiselect_comboboxAreaSoporte]
					listAreaSoporte = listAreaSoporte.collect { area -> AreaSoporte.findById(area) }
					variante.areasSoporte = listAreaSoporte
				}
				
				if (params.multiselect_comboboxRol){
					def listRoles = busquedaService.convertToList(params.multiselect_comboboxRol) //params.multiselect_comboboxRol.class.isArray() ? params.multiselect_comboboxRol as List : [params.multiselect_comboboxRol]
					listRoles = listRoles.collect { rol -> RolAplicacion.findByCodigoRol(rol) }
					variante.roles = listRoles
				}
				if(params.multiselect_comboboxGrupo){
					variante.grupos = busquedaService.convertToList(params.multiselect_comboboxGrupo)//params.multiselect_comboboxGrupo.class.isArray() ? params.multiselect_comboboxGrupo as List : [params.multiselect_comboboxGrupo]
				}
				variante.legajoAutorVariante = umeService.getUsuarioLogueado()
				busquedaService.guardarVariante(variante)
				render(template: "/busqueda/guardarVariante")
			} catch (Exception e) {
				procesarErrorAjax(e)
			}
		}.invalidToken {
			throw new BusinessException("Operaci\u00F3n no v\u00E1lida. Por favor vuelva a intentarlo.")
		}
	}
	
	def eliminarVariante = {
		withForm {
			try{
				def usuarioLogueado = umeService.getUsuarioLogueado()
				busquedaService.eliminarVariante(params.varianteId)
				def listadoVariantes = busquedaService.obtenerVariantes(usuarioLogueado)
				render (template: "/busqueda/listadoVariantes", model:[listadoVariantes:listadoVariantes])
			} catch (Exception e) {
				procesarErrorAjax(e)
			}
		}.invalidToken {
			throw new BusinessException("Operaci\u00F3n no v\u00E1lida. Por favor vuelva a intentarlo.")
		}
	}
	
	void limpiarParams(params){
		params.remove("org")
		params.remove("org.codehaus")
		params.remove("org.codehaus.groovy")
		params.remove("org.codehaus.groovy.grails")
		params.remove("org.codehaus.groovy.grails.SYNCHRONIZER_TOKEN")
		params.remove("macroestados")
		params.remove("fases")
		params.remove("roles")
		params.remove("tiposImpacto")
		params.remove("areasSoporte")
		params.remove("grupos")
		params.remove("estados")
	}
	
	def obtenerMacroestados = { render (template: "/busqueda/tablaMacroestadoSeleccionadas") }

	def buscarPedidos = {
		def token = SynchronizerToken.store(session)
		log.info "${token.currentToken.toString()} - ${params}"
		withForm{
//			try{
				limpiarParams(params)
				def variante
				if (params.varianteId && params?.varianteId.isInteger()){
					variante = Variante.findById(params?.varianteId)
					bindData(params, variante)
				}else{
					variante = new Variante()
					variante.properties = params
					setParams(params, variante)
					bindData(variante, params)
					setGrupos(params, variante)
				}
				if (params.fechaDesde && params.fechaHasta){
					variante.fechaDesde = DateUtil.toDate(params.fechaDesde)
					variante.fechaHasta = DateUtil.toDate(params.fechaHasta)
				}
				setRoles(params, variante)
				def pedidosIntancesList =  busquedaService.busquedaAvanzadaPedidos(variante, params)
				pedidosIntancesList = ordenarInbox(pedidosIntancesList, params)
		
				def result = [pedidosIntancesList: pedidosIntancesList, variante:variante]
				result.putAll(getMapaDatosPedido())
				render(view: "list", model: result)
//			} catch (Exception e) {
//				log.info "Exception: ${e.message}"
//				response.sendError org.codehaus.groovy.grails.commons.ConfigurationHolder.config.errorAjax, e.message
//			}
		}.invalidToken {
			throw new BusinessException("Operaci\u00F3n no v\u00E1lida. Por favor vuelva a intentarlo.")
		}
	}

	def ordenarInbox(pedidos, params){
		def pedidosPadres = pedidos.collect {
			it.parent
		}.unique { a, b->
			a.id <=> b.id
		}
		busquedaService.definirPaginacionInbox(params)
//		log.debug "pedidos padres: " + pedidosPadres.id
		def listadoPedidos
		if (pedidosPadres.isEmpty()) {
			params.totalCount = 0
			listadoPedidos = []
		} else {
			listadoPedidos = Pedido.createCriteria().list(max: params.max, offset: params.offset) {
				inList("id", pedidosPadres.id)
				createAlias ('faseActual', 'fasePedido') // Para macroestado
				order (params.sort, params.order)
			}
			params.totalCount = listadoPedidos.totalCount
		}
		listadoPedidos
	}
	
	def jsonOutput(Object json) {
		render json as JSON
	}

}
