package ar.com.telecom

import ar.com.telecom.exceptions.BusinessException
import ar.com.telecom.pcs.entities.EncuestaSatisfaccion
import ar.com.telecom.pcs.entities.LogModificaciones

class FinalizacionPedidoController extends WorkflowController{

	def webUserAgentService
	
	def index = {
		if (webUserAgentService.isBlackberry()) {
			redirect(controller:"inboxMobile", action: "index")
		} else {
			def pedido = llenarDatosPedido(params, true)
			def usuarioLogueado = umeService.getUsuarioLogueado()
			if (pedido.fechaCierre || !pedido.legajoInterlocutorUsuario.equals(usuarioLogueado)){
				redirect (action: "show", params: params)
			}
			def result = [pedidoInstance: pedido, encuestaSatisfaccion: pedido.encuestaSatisfaccion]
			result.putAll(getMapaDatosPedido(pedido))
			result
		}
	}
	
	def show = {
		def pedido = llenarDatosPedido(params, true)
		def result = [pedidoInstance: pedido, encuestaSatisfaccion: pedido.encuestaSatisfaccion]
		result.putAll(getMapaDatosPedido(pedido))
		result
	}
	
	def getMapaDatosPedido(pedido) {
		def usuarioLogueado = umeService.getUsuarioLogueado()
		def permiteLlenarEncuesta =  pedido?.puedeEditar(FaseMultiton.getFase(FaseMultiton.FINALIZACION), usuarioLogueado)
		def listEscalaSatisfaccion = EncuestaSatisfaccion.escalaSatisfaccion
		return [usuarioLogueado: usuarioLogueado, permiteLlenarEncuesta: permiteLlenarEncuesta, listEscalaSatisfaccion:listEscalaSatisfaccion]
	}
	
	def grabarFormulario = {
		withForm {
			try{
				def pedido = llenarDatosPedido(params, true)
				EncuestaSatisfaccion encuesta = new EncuestaSatisfaccion()
				
				bindData(encuesta, params)
				pedido.encuestaSatisfaccion = encuesta
				pedidoService.guardar(params, pedido)
				def result = [pedidoInstance: pedido, encuestaSatisfaccion: encuesta]
				result.putAll(getMapaDatosPedido(pedido))
				render (view: "index", model: result)
			} catch (Exception e) {
				procesarErrorAjax(e)
			}
		}.invalidToken {
			throw new BusinessException("Operaci\u00F3n no v\u00E1lida. Por favor vuelva a intentarlo.")
		}
	}
	
	def aprobarPedido = {
		withForm {
			try{
				def pedido = llenarDatosPedido(params, true)
				def usuarioLogueado = umeService.getUsuarioLogueado()
				EncuestaSatisfaccion encuesta = new EncuestaSatisfaccion(params)
				
				pedido.finalizarPedido(encuesta, usuarioLogueado)
				pedidoService.guardar(params, pedido)
				
				def result = [pedidoInstance: pedido, encuestaSatisfaccion: encuesta]
				result.putAll(getMapaDatosPedido(pedido))
				flash.message = "Se guard\u00F3 la Encuesta de Satisfacci\u00F3n correctamente"
				render (view: "show", model: result)
			} catch (Exception e) {
				procesarErrorAjax(e)
			}
		}.invalidToken {
			throw new BusinessException("Operaci\u00F3n no v\u00E1lida. Por favor vuelva a intentarlo.")
		}
	}
}
