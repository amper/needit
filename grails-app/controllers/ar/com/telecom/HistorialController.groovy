package ar.com.telecom

import ar.com.telecom.pcs.entities.LogModificaciones;
import ar.com.telecom.pcs.entities.LogModificaciones;
import org.codehaus.groovy.grails.commons.ConfigurationHolder

import ar.com.telecom.exceptions.BusinessException
import ar.com.telecom.pcs.entities.LogModificaciones
import ar.com.telecom.pcs.entities.RolAplicacion
import ar.com.telecom.util.DateUtil

class HistorialController extends WorkflowController {

	def exportService
	def historialService
	
	def index = {
		redirect(action: "list", params: params)
	}

	def list = {
		def pedido = pedidoService.obtenerPedido(params, params.pedidoId)
		
		def logModificaciones = historialService.obtenerHistorialPaginado(params, pedido)
		def personsAutocomplete = pedidoService.getUsuariosPedido(pedido?.id)
		def listEventos = LogModificaciones.getTipoEventos()
				  
		[logModificaciones:logModificaciones, pedidoInstance:pedido, personsAutocomplete:personsAutocomplete, listEventos:listEventos, logModificacionesTotal: params.totalCountLog]
	}
	
	def buscarHistorial = {
		withForm {
			def pedido = pedidoService.obtenerPedido(params, params.pedidoId)
			def personsAutocomplete = pedidoService.getUsuariosPedido(params?.pedidoId?.toInteger())
			def logModificaciones = historialService.obtenerHistorialPaginado(params, pedido)
			def listEventos = LogModificaciones.getTipoEventos()
			
			limpiarParams(params)
			
			def listEventosSeleccionados = []
			def listadoTipoEventoSeleccionados = params.multiselect_comboboxTipoEvento as List
			listadoTipoEventoSeleccionados.each { tipoEvento ->	listEventosSeleccionados << "'$tipoEvento'"	}
			
			def modelView = [logModificaciones:logModificaciones, pedidoInstance:pedido, personsAutocomplete:personsAutocomplete, logModificacionesTotal: params.totalCountLog, params:params, listEventos:listEventos, listEventosSeleccionados:listEventosSeleccionados]
			render(view: "list", model: modelView)
		}.invalidToken {
			throw new BusinessException("Operaci\u00F3n no v\u00E1lida. Por favor vuelva a intentarlo.")
		}
	}
	
	void limpiarParams(params){
		params.remove("org")
		params.remove("org.codehaus")
		params.remove("org.codehaus.groovy")
		params.remove("org.codehaus.groovy.grails")
		params.remove("org.codehaus.groovy.grails.SYNCHRONIZER_TOKEN")
	}
	
	def exportFile = {
		def pedido = pedidoService.obtenerPedido(params, params.pedidoId)
		if (params.totalCountLog){
			params.max = params.totalCountLog
		}
		def logModificaciones = historialService.obtenerHistorialPaginado(params, pedido)
		
		if(params?.format && params.format != "html"){
			Date fechaActual = new Date()
			def nameArch = "LogEventos_NroPedido_" + params.pedidoId + "_Fecha_" + formatDate(date: fechaActual, type: "date", style: "MEDIUM")  
			
			response.contentType = ConfigurationHolder.config.grails.mime.types[params.format]
			response.setHeader("Content-disposition", "attachment; filename=$nameArch.${params.extension}")
			List fields = ["idPedido", "macroEstado", "fase", "tipoEvento","legajo", "rol", "fechaDesde","fechaHasta", "duracion", "descripcionEvento"]
			Map labels = ['idPedido':'N\u00FAmero pedido', 'descripcionEvento':'Descripci\u00F3n','fase':'Fase','fechaDesde':'Fecha Inicio','fechaHasta':'Fecha Fin','legajo':'Usuario', 'tipoEvento':'Tipo de Evento','macroEstado':'Macro Estado', 'rol':"Roles", 'duracion':'Duraci\u00F3n']
			
			
			def idPedido = { domain, value ->
				return params.pedidoId
			}
			def macro = { domain, value ->
				return domain?.fase?.macroEstado
			}
			def tipoEvento = { domain, value ->
				return LogModificaciones.getTipoEvento(domain?.tipoEvento)
			}
			def roles = { domain, value ->
				return domain.getRoles()?.flatten()?.unique()
			}
			def legajos = { domain, value ->
				return domain?.legajo + "-" + (umeService.getUsuario(domain?.legajo))?.nombreApellido
			}
			def duraciones = { domain, value ->
				def duration
				use(groovy.time.TimeCategory) {
					if (domain?.fechaHasta && domain?.fechaDesde){
						duration = domain?.fechaHasta - domain?.fechaDesde
						duration = getDuracion(duration)
					}
					else
						duration = "No finaliz\u00F3"
				}
				return duration
			}
			Map formatters = [idPedido: idPedido, macroEstado: macro, tipoEvento: tipoEvento, rol: roles, legajo: legajos, duracion: duraciones]
			
			exportService.export(params.format, response.outputStream,logModificaciones, fields, labels,formatters,[:])
		}
	}
	
	def getDuracion(duration){
		String duracion = ""
		if (duration?.years) duracion+= duration?.years.toString() + (duration?.years==1? " año" : " años ")
		if (duration?.months) duracion+= duration?.months.toString() + (duration?.months==1? " mes ": " meses ")
		if (duration?.days) duracion+= duration?.days.toString() + (duration?.days==1? " d\u00EDa " : " d\u00EDas ")
		if (duration?.hours) duracion+= duration?.hours.toString() + (duration?.hours==1? " hora " : " horas ")
		if (duration?.minutes) duracion+= duration?.minutes.toString() + (duration?.minutes==1? " minuto " : " minutos ")
//		if (duration?.seconds) duracion+= duration?.seconds.toString() + (duration?.seconds==1? " segundos " : " segundo ")
		
		return duracion
	}
	
	def obtenerFases = {
		def fases
		if (params?.macroEstadoID){
			fases = historialService.obtenerFases(params?.macroEstadoID?.split(","))
		}else{
			fases = historialService.obtenerFases()
		}
		render (template: "/templates/comboFasesHistorial", model:[listadoFases:fases, faseID:params?.faseID])
		
	}
	
}
