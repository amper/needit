package ar.com.telecom

class SearchableController {

	def pedidoService

	def index = {
		def pedidoList = pedidoService.consultarPedidosInbox(params)
		if (pedidoList.isEmpty()) {
			flash.message = "No se han encontrado pedidos con ese criterio de b\u00FAsqueda"
			redirect(controller: "inbox", action: "list")
		} else {
			if (pedidoList.size() == 1) {
				def pedido = pedidoList.first()
				redirect(action:"index", controller: pedido?.faseActual?.controllerNombre, id: pedido.id, params: [pedidoId: pedido.id])
			} else {
				render(view:"list", model:[pedidos:pedidoList, pedidosTotal: pedidoList.totalCount, textoBusqueda: params.id])
			}
		}
	}
}
