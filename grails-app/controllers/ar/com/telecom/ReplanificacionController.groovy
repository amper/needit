package ar.com.telecom

import ar.com.telecom.exceptions.BusinessException
import ar.com.telecom.pcs.entities.DetallePlanificacionSistema
import ar.com.telecom.pedido.AprobacionUI
import ar.com.telecom.pedido.especificacionImpacto.Impacto
import ar.com.telecom.util.DateUtil

class ReplanificacionController extends WorkflowController {

    def index = {
		def pedido = llenarDatosPedido(params, true)
		def result = [pedidoInstance: pedido]
		result.putAll(getMapaDatosPedido(pedido))
		result
	}
	
	def cambiaGrupoRSWF = {
		withForm {
			try{
				def pedido = llenarDatosPedido(params, true)
				redirect (action: "index", id: pedido.id, params: [pedidoId: pedido.id, impacto: params.impacto])
			} catch (Exception e) {
				procesarErrorAjax(e)
			}
		}.invalidToken {
			throw new BusinessException("Operaci�n no v�lida. Por favor vuelva a intentarlo.")
		}
	}
	
	def grabaFechasDetalles = {
		withForm {
			try{
				println "grabaFechasDetalles: " + params
				
				def pedidoATrabajar = getPedidoATrabajar(params)
				String [] detalles = params?.detalles?.split(",")
				String [] fechasDesde = params?.fechasDesde?.split(",")
				String [] fechasHasta = params?.fechasHasta?.split(",")
				String [] cargaHoras = params?.horas?.split(",")
				
				detalles.eachWithIndex { idDetalle, i ->
					def detalle = DetallePlanificacionSistema.findById(Integer.parseInt(idDetalle))
					
					// Actualizo fecha desde y hasta del detalle(la validacion esta en el JS pero aca la hago PLD)
					if(fechasDesde[i]){
						detalle.fechaDesde = DateUtil.toDate(fechasDesde[i])
					}
				
					if(fechasHasta[i]){
						detalle.fechaHasta = DateUtil.toDate(fechasHasta[i])
					}
					
					if(cargaHoras[i]){
						println cargaHoras[i]
//						detalle.fechaHasta = DateUtil.toDate(cargaHoras[i])
					}
					
					if (detalle){
						pedidoService.actualizarDetalle(detalle)
					}
				}
				render(template: "/replanificacion/planificacionYEsfuerzoReplanificacion", model:getMapaDatosPedido(pedidoATrabajar.parent))
			} catch (Exception e) {
				procesarErrorAjax(e)
			}
		}.invalidToken {
			throw new BusinessException("Operaci�n no v�lida. Por favor vuelva a intentarlo.")
		}
	}
	
	def enviarReplanificacion = {
		withForm {
			try{
				def pedido = getPedidoATrabajar(params) //llenarDatosPedido(params, true)//getPedidoATrabajar(params)
				pedido.enviarReplanificacion(params?.comentario)
				pedidoService.guardar(params, pedido)
				render (template: "tablaImpactos", model:[pedidoInstance: pedido.parent])
			} catch (Exception e) {
				procesarErrorAjax(e)
			}
		}.invalidToken {
			throw new BusinessException("Operaci�n no v�lida. Por favor vuelva a intentarlo.")
		}
	}
	
	def popCargaHorasSistema = {
		withForm {
			def pedidoATrabajar = getPedidoATrabajar(params)
//			generarAprobacionId(pedidoATrabajar, umeService.usuarioLogueado)

			def detalle = DetallePlanificacionSistema.findById(Integer.parseInt(params.idDetalle))
			
			// Actualizo fecha desde y hasta del detalle(la validacion esta en el JS pero aca la hago PLD)
			if(params.fechaDesde){
				detalle.fechaDesde = DateUtil.toDate(params.fechaDesde)
			}
	
			if(params.fechaHasta){
				detalle.fechaHasta = DateUtil.toDate(params.fechaHasta)
			}
			
			cargarHorasSistema(pedidoATrabajar, detalle, params)
		}.invalidToken {
			throw new BusinessException("Operaci�n no v�lida. Por favor vuelva a intentarlo.")
		}
	}
	
	private def cargarHorasSistema(pedido, detalle, params) {
		def pedidoATrabajar = getPedidoATrabajar(params)
		boolean actualizaDetalle = true
		if (params.show) {
			actualizaDetalle = !params.show.toBoolean()
		}
		if (actualizaDetalle){
			pedidoService.actualizarDetalle(params, generarAprobacionUI(pedidoATrabajar, true), detalle)
		}
		
		def softwareFactories = pedido.softwareFactories()
		def periodos = capacidadService.getPeriodos(detalle.fechaDesde, detalle.fechaHasta)
		def planificacionActual = PlanificacionEsfuerzo.findById(Integer.parseInt(params.idPlanificacion))
		def detallesAEditar = planificacionActual.detallesAEditar()
		def indiceDetalleAEditar = detallesAEditar.indexOf(detalle)
		def esElPrimero = (indiceDetalleAEditar==0)
		def esElUltimo = (indiceDetalleAEditar==(detallesAEditar.size()-1))
		
		render (template: "bodyCargaHoras",
			model:[gruposSWF: softwareFactories, periodos: periodos, detalle : detalle, planificacionActual: planificacionActual,
			esElPrimero:esElPrimero, esElUltimo:esElUltimo, pedidoATrabajar: params.pedidoATrabajar?:params.pedidoId, cargaHora:params.cargaHora,
			impacto:params.impacto, pedido:pedido, show: params?.show ?: false, usuarioLogueado: umeService.usuarioLogueado] + getImpacto(pedidoATrabajar))
	}
	
	private def getMapaDatosPedido(pedido) {
		log.warn "getMapaDatos" + params
		def pedidoATrabajar = getPedidoATrabajar(params)
		def usuarioLogueado = umeService.usuarioLogueado
		def impactoInstance = new Impacto(!Impacto.aplicaAHijo(params.impacto)?params.impacto:Impacto.IMPACTO_HIJO, pedidoATrabajar)
		return [usuarioLogueado: usuarioLogueado,
				impactoInstance:impactoInstance,
				pedidoATrabajar:pedidoATrabajar,
				impacto: params.impacto,
				consolidacionList : pedidoService.planificacionConsolidadaReplanificacion(pedidoATrabajar?.parent, impactoInstance),
				planificacionActual: pedidoATrabajar.planificacionEnReplanificacion, 
				detalleReplanificacion : pedidoATrabajar.detallesParaReplanificacion
			]
	}
}
