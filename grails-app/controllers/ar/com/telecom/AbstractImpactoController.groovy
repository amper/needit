package ar.com.telecom

import ar.com.telecom.pedido.especificacionImpacto.Impacto

class AbstractImpactoController extends WorkflowController {

	def webUserAgentService
	
	def index = {
		if (webUserAgentService.isBlackberry()) {
			redirect(controller:"inboxMobile", action: "index")
		} else {
			def pedido = llenarDatosPedido(params, true)
		
			getMapaDatosPedido(pedido)
			def result = [pedidoInstance: pedido]
			result.putAll(getMapaDatosPedido(pedido))
			result
		}
	}
	
	def faseFinalizada(pedido) {
		def result = [pedidoInstance: pedido]
		result.putAll(getMapaDatosPedido(pedido))
		render(view: "index", model: result)
	}
	
	def getMapaDatosPedido(pedido) {
		def detalle = pedidoService.obtenerDetallesAgrupadosPorTipoDeImpacto(pedido)
		def totalizacion = [totalMontoHoras: pedido.totalGeneral(), totalHoras: pedido.totalHorasPlanificadas(), detalle: detalle]
		def usuario = umeService.getUsuarioLogueado()
		
		def impactoInstance = new Impacto(Impacto.IMPACTO_CONSOLIDADO, pedido)
		def listadoConsolidado = pedidoService.planificacionConsolidada(pedido.parent, impactoInstance)
		def consolidadoImplantacion = listadoConsolidado.find{ consolidado -> consolidado[0].toString().trim().equals("Implantación")}
		def fechaImplementacionDesde = consolidadoImplantacion?.getAt(1)
		def fechaImplementacionHasta = consolidadoImplantacion?.getAt(2)
		
		def result = [
			totalizacion: totalizacion, 
			fechaImplementacionDesde:fechaImplementacionDesde, 
			fechaImplementacionHasta:fechaImplementacionHasta,
			usuarioLogueado: usuario
		]
		result.putAll(doGetMapaDatosPedido(pedido))
		return result 
	}
	
}
