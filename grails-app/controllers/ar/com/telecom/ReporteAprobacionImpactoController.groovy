package ar.com.telecom

import grails.util.Environment

import org.apache.commons.lang.WordUtils

import pdf.PdfService
import ar.com.telecom.pcs.entities.MailAudit
import ar.com.telecom.pcs.entities.ParametrosSistema
import ar.com.telecom.pcs.entities.ReporteAprobacionEnviados;

class ReporteAprobacionImpactoController extends WorkflowController {
	
	PdfService pdfService
	def mailService
	
    def index = {
		def reportes = obtenerReporteAprobacionImpacto()
		
		if (reportes){
			enviarMail(reportes)
		}
		render (view: "emailTodos", model: [reportes:reportes, fechaReporte: getFechaReporte()])
	}
	
	def enviarMail(reportes){
		def parametro = ParametrosSistema.list().first()
		def fechaReporte = getFechaReporte()
		def subjectMail = "Resumen de Cambios Evolutivos (${fechaReporte})"
		log.warn "Enviando email de reportes (${fechaReporte})"
		def fechaPath = fecha
		fechaPath.add(Calendar.MONTH, 1)
		def mesLog = fechaPath.get(Calendar.MONTH)
		def grailsLogs = "d:\\grailsLogs\\${getFecha().get(Calendar.YEAR)}-${mesLog.toString().padLeft(2,'0')}"
		new File(grailsLogs).mkdir()
		def baseUri = request.scheme + "://" + request.serverName + ":" + request.serverPort + grailsAttributes.getApplicationUri(request)
		def test = false
		if ((params.test && params.test.toBoolean()) || Environment.current != Environment.PRODUCTION){
			test = true
		}
		
		reportes.each { reporte ->
			def listaMails = reporte.usuarios.collect { it.email }.unique()
			def htmlMailHeader = g.render(template: "/reporteAprobacionImpacto/email", model: [reporte:reporte, fechaReporte: fechaReporte])
			def fileName = "Detalle de Cambios-${reporte?.usuarios[0]?.direccion}-${fechaReporte}.pdf"
			def path = "${grailsLogs}\\${fileName}"
			File archivo
			
			if (!reporte.pedidosEstandar.isEmpty() || !reporte.pedidosEmergencia.isEmpty()){
				try{
					def htmlMailContent = g.render(template: "/reporteAprobacionImpacto/tablaPedidos", model: [reporte:reporte, fechaReporte: fechaReporte, nota: parametro?.notaReporteAprobacionImpacto])
					byte[] b = pdfService.buildPdfFromString(htmlMailContent.readAsString(), baseUri)
					archivo = new File(path) 
					archivo.setBytes(b) //no se puede hacer en la misma linea de new File porque no crea la instancia, deja archivo en null
				}catch (Throwable e) {
					log.error e
					log.error e?.message
				}
			}
			try{
				if (listaMails){
					if (parametro.enviaMails && Environment.current != Environment.DEVELOPMENT){
						listaMails.each { mail ->
							mailService.sendMail {
								multipart true
								to test?"Base_Evidencias_SOX@ta.telecom.com.ar":mail.toString()
								from parametro.mailEmisorPCS
								subject subjectMail
								html htmlMailHeader
								if (archivo){
									attachBytes fileName, "application/pdf", archivo.readBytes()
								}
							}
						}
					}
				}
				
				if (parametro.auditaMails){
					def mailAudit = new MailAudit(asunto: subjectMail, texto: htmlMailHeader, mails: listaMails.toString())
					mailAudit.save(flush: true, failOnError: true)
				}
			} catch (Exception e) {
				log.fatal e
				e.printStackTrace()
			}
			
		}
		
	}
	
	def obtenerReporteAprobacionImpacto(){
		
		/*
			obtenego los gerentes de todos los pedidos
			a los que tienen direccion, llamo a la UME getGerentesByUsuario 
			y obtengo (y filtro) a lo que me devuelve.
			Si el usuario no tiene direccion, hay que llamar a UsuariosDireccion 
			y obtener los gerentes con el metodo getGerentes()
		*/
		
		log.warn "obtenerReporteAprobacionImpacto: ${params}"
		def parametro = ParametrosSistema.list().first()
		List reportes = []
		
		def listadoPedidos = pedidoService.obtenerPedidosReporteAprobacionImpacto(fecha).unique()
		def gerentesPedidos = listadoPedidos.collect { it.legajoGerenteUsuario }?.unique()
		gerentesPedidos = gerentesPedidos.collect { umeService.getUsuario(it) }
		
		gerentesPedidos.each{ gerente ->
			def direccion = gerente?.direccionCodigo
			Reporte reporte =  reportes.find{it.direccionCodigo.equals(direccion)}
			boolean seAgrega = false
			
			if (!reporte){
				seAgrega = true
				reporte = new Reporte(direccionCodigo: direccion)
			}
			
			if (reporte.usuarios.isEmpty()){
				reporte.usuarios.addAll(obtenerUsuarios(direccion))
			}
			
			def pedidos = listadoPedidos.findAll{ it.legajoGerenteUsuario.equals(gerente.legajo)}
			pedidos.each{ pedido ->
				if (pedido.pedidoDeEmergencia()){
					reporte.pedidosEmergencia.add(pedido)
				}else{
					reporte.pedidosEstandar.add(pedido)
				}
			}
			pedidoService.actualizarReporteAprobacionEnviados(direccion)
			if (seAgrega){
				reportes.add(reporte)
			}
		}
		
		def direccionesSinReporte = pedidoService.obtenerDireccionSinPedidos(fecha)
		direccionesSinReporte.each{ item ->
			def direccion = item.codigoDireccion
			Reporte reporte = new Reporte(direccionCodigo: direccion)
			reporte.usuarios.addAll(obtenerUsuarios(direccion))
			reportes.add(reporte)
			//No agrego pedidos, solo se enviará el header
		}
		
		return reportes
	}
	
	def getFecha(){
		def fecha = Calendar.instance
		if (params.mes && params.anio){
			fecha.set(params.anio.toInteger(),params.mes.toInteger()-1, 1, 0, 0, 0)
		}else{
			fecha.add(Calendar.MONTH, -1)
		}
		return fecha
	}
	
	def getFechaReporte(){
		def fecha = getFecha()
		return "${WordUtils.capitalize(String.format(new Locale("ES"), ,"%tB", fecha))} ${fecha.get(Calendar.YEAR)}"
	}
	
	def obtenerDireccionCodigo(legajoGerenteUsuario, listadoAI){
		def direccionCodigo = listadoAI.find{ it.legajo.equalsIgnoreCase(legajoGerenteUsuario) }?.direccionCodigo
		if (!direccionCodigo){
			direccionCodigo = umeService.getUsuario(legajoGerenteUsuario)?.direccionCodigo
		}
		return direccionCodigo
	}
	
	def obtenerUsuarios(direccion){
		//umeService.getGerentesDireccion(gerente)
		def parametro = ParametrosSistema.list().first()
		def listadoAI = umeService.getGerentesUsuarios()
		listadoAI = listadoAI.findAll { it.direccionCodigo && it.direccionCodigo.equals(direccion) && it.isAprobadorImpacto()}
		listadoAI.addAll(UsuariosDireccion.instance.getAprobadoresImpacto(direccion))
		/*listadoAI.each{
			println it.legajo + " - " + it.jerarquias
		}*/
		return listadoAI.unique()
	}
	
}

//def obtenerReporteAprobacionImpacto(){
//	
//	/*
//		obtenego los gerentes de todos los pedidos
//		a los que tienen direccion, llamo a la UME getGerentesByUsuario
//		y obtengo (y filtro) a lo que me devuelve.
//		Si el usuario no tiene direccion, hay que llamar a UsuariosDireccion
//		y obtener los gerentes con el metodo getGerentes()
//	*/
//	
//	println "obtenerReporteAprobacionImpacto: ${params}"
//	def listadoAI = umeService.getGerentesUsuarios()
//	def parametro = ParametrosSistema.list().first()
//	//TODO: it.direccionCodigo validacion temporal. El metodo de la UME me lo traera bien
//	//TODO 2: hay que tomar el parametro de sistema para la jerarquia
//	def listadoDirecciones = listadoAI.grep { it.direccionCodigo && it.jerarquia &&  (it.jerarquia?.toInteger() >= parametro?.minimoJerarquiaAprobador && it.jerarquia?.toInteger() <= parametro?.maximoJerarquiaAprobador) }
//	listadoDirecciones = listadoDirecciones.unique{ it.direccionCodigo }.sort { it.direccionCodigo }
//	
//	List reportes = []
//	
//	listadoDirecciones.each{ direccion ->
//		Reporte reporte = new Reporte(direccionCodigo: direccion.direccionCodigo)
//		def listaUsuarios =  listadoAI.findAll { it.direccionCodigo.equals(reporte.direccionCodigo) }
//		reporte.usuarios.addAll(listaUsuarios)
//		reportes.add(reporte)
//	}
//	
//	def listadoPedidos = pedidoService.obtenerPedidosReporteAprobacionImpacto(fecha).unique()
//	
//	listadoPedidos.each{ pedido ->
//		def direccionCodigo = obtenerDireccionCodigo(pedido.legajoGerenteUsuario, listadoAI)
//		def reporte = reportes.find{it.direccionCodigo.equals(direccionCodigo)}
//		if (reporte){
//			if (pedido.pedidoDeEmergencia()){
//				reporte.pedidosEmergencia.add(pedido)
//			}else{
//				reporte.pedidosEstandar.add(pedido)
//			}
//		}
//	}
//
//	return reportes
//}
