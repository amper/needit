package ar.com.telecom

import org.codehaus.groovy.grails.commons.ConfigurationHolder

import ar.com.telecom.exceptions.BusinessException
import ar.com.telecom.pcs.entities.TipoAnexo

class AnexoController extends WorkflowController{

	def exportService
	
	def index = {
//		def pedido = llenarDatosPedido(params, true)
		redirect(action: "list", params: params)
	}

	def list = {
		def pedido = pedidoService.obtenerPedido(params, params.pedidoId)
		def usuario = umeService.getUsuarioLogueado()
		def listadoAnexos =  anexoService.obtenerAnexosPaginado(params, pedido)
		def stringParams = returnParametros(params)

		def modelView = [pedidoInstance: pedido, listadoAnexos: listadoAnexos, stringParams:stringParams]
		modelView.putAll(getMapaDatosPedido(pedido))
		return modelView
	}
	
	def definirPaginacion(params) {
		params.sort = params.sort?:'fecha'
		params.order = params.order?:'asc'
		params.max = Math.min(params.max ? params.int('max') : 10, 100)
		params.offset = params.offset? params.offset.toInteger():0
	}
	
	def popupAnexo = {
		log.info "popupAnexo: ${params}"
		withForm {
			def pedido = llenarDatosPedido(params, true)
			def tiposDeAnexo = TipoAnexo.findAllByDescripcion("Global")
			render (template: "/templates/anexoPorTipo", model:[impacto:params?.impacto, pedidoInstance:pedido, tabSeleccionado:params?.tabSeleccionado, tiposDeAnexo:tiposDeAnexo, anexoFrom: params?.anexoFrom])
		}.invalidToken {
			throw new BusinessException("Operaci\u00F3n no v\u00E1lida. Por favor vuelva a intentarlo.")
		}
	}
	
	def returnParametros(parametros){
		def stringParams = ""
		parametros.each{ parametro ->
			parametro = parametro.toString().replace('=',':')
			if (parametro.split(":").size()>1){
				if (!parametro.split(":")[1].equals("null")){
					stringParams += parametro.split(":")[0] + ":'" + parametro.split(":")[1] + "',"
				}else{
					stringParams += parametro.split(":")[0] + ":" + parametro.split(":")[1] + ","
				}
			}else{
				stringParams += parametro.split(":")[0] + ":'',"
			}
		}
		stringParams = stringParams.substring(0, stringParams.length()-1)
		return stringParams
	}
	
	def buscarAnexo = {
		def pedido = pedidoService.obtenerPedido(params, params.pedidoId)
		//def listEventos = LogModificaciones.getTipoEventos()
		def listadoAnexos = anexoService.obtenerAnexosPaginado(params, pedido)
		
		def modelView = [pedidoInstance: pedido, listadoAnexos: listadoAnexos]
		modelView.putAll(getMapaDatosPedido(pedido))
		render(template:  "/anexo/tablaBusqueda", model: modelView)
	}
	
	def eliminarAnexo = {
		def pedido = llenarDatosPedido(params, true)
		anexoService.eliminarAnexo(params, pedido, params.anexoId)
		redirect(action: "index", id: pedido.id, params: [pedidoId: pedido.id])
	}
	
	
	def exportFile = {
		def pedido = pedidoService.obtenerPedido(params, params.pedidoId)
		def listadoAnexos = anexoService.obtenerAnexosPaginado(params, pedido)
		
		if(params?.format && params.format != "html"){
			Date fechaActual = new Date()
			def nameArch = "ListadoAnexos_NroPedido_" + params.pedidoId + "_Fecha_" + formatDate(date: fechaActual, type: "date", style: "MEDIUM")   //"ListadoAnexos_" + fechaActual
			
			response.contentType = ConfigurationHolder.config.grails.mime.types[params.format]
			response.setHeader("Content-disposition", "attachment; filename=$nameArch.${params.extension}")
			List fields = ["fase", "tipoAnexo", "descripcion", "sistema", "documento", "fecha" ,"legajo", "rol"]
			Map labels = ['descripcion':'Descripci\u00F3n','fase':'Fase','fecha':'Fecha','legajo':'Usuario', 'tipoAnexo':'Tipo de Anexo', 'rol':"Roles", 'sistema':'Sistema', 'documento':'Documento']
			
			def fase = { domain, value ->
				return domain?.tipoAnexo?.fase==null ? "Global":domain?.tipoAnexo?.fase 
			}
			def tipoAnexo = { domain, value ->
				return domain?.tipoAnexo
			}
			def roles = { domain, value ->
				return pedido.getRoles(domain?.legajo)
			}
			def legajos = { domain, value ->
				return domain?.legajo + "-" + (umeService.getUsuario(domain?.legajo))?.nombreApellido
			}
			def documento = { domain, value ->
				return domain?.nombreArchivo + domain?.extension
			}
			def sistema = { domain, value ->
				return ""
			}
			
			Map formatters = [fase: fase, tipoAnexo: tipoAnexo, rol: roles, legajo: legajos, documento: documento, sistema: sistema]
			exportService.export(params.format, response.outputStream,listadoAnexos, fields, labels,formatters,[:])
		}
	}
		
	private def getMapaDatosPedido(pedido) {
		def parametros = Constantes.instance.parametros
		def personsAutocompleteInt = umeService.getUsuariosGrupoLDAP(parametros.grupoLDAPInterlocutorUsuario, pedido?.legajoGerenteUsuario)
		def usuarioLogueado = umeService.usuarioLogueado
		def userSesion = umeService.getUsuario(usuarioLogueado)
		def personsAutocomplete = pedidoService.getUsuariosPedido(pedido?.id)
		def tiposDeAnexo = TipoAnexo.findAllByDescripcion("Global")
		
		return [personsAutocompleteInt:personsAutocompleteInt, tiposDeAnexo: tiposDeAnexo, userSesion:userSesion, usuarioLogueado: usuarioLogueado, personsAutocomplete:personsAutocomplete]
	}
}
