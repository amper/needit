package ar.com.telecom

import grails.converters.JSON

import org.codehaus.groovy.grails.web.servlet.mvc.SynchronizerToken

import ar.com.telecom.exceptions.BusinessException
import ar.com.telecom.exceptions.ConcurrentAccessException
import ar.com.telecom.exceptions.SessionExpiredException

class AbstractController {

	def procesarErrorAjax(Throwable e) {
		log.error "Error de Ajax <-------------------"
		log.error e
		e.printStackTrace()

		def mensajeError
		def contenido
		def origenError = e?.cause ? e.cause : e

		if (origenError.class.equals(SessionExpiredException.class)) {
			mensajeError = "Su sesi\u00F3n ha expirado. Debe volver a loguearse."
			contenido = ErrorTagLib.MSG_CODE
		} else {
			if (origenError.class.equals(ConcurrentAccessException.class)) {
				contenido = ErrorTagLib.MSG_CODE
				mensajeError = origenError.message
			} else {
				if (origenError.class.equals(BusinessException.class)) {
					mensajeError = origenError.message
					contenido = ErrorTagLib.WARNING_CODE
				} else {
					mensajeError = "Ha ocurrido un error en la aplicaci\u00F3n. Se gener\u00F3 un registro con la informaci\u00F3n"
					contenido = ErrorTagLib.ERROR_CODE
				}
			}
		}
		response.sendError org.codehaus.groovy.grails.commons.ConfigurationHolder.config.errorAjax + contenido, mensajeError
	}
	
	def updateToken = {
		log.info "Hace updateToken"
		render (template: "/templates/token")
	}
	
	def actualizarToken = {
		def token = SynchronizerToken.store(session).currentToken.toString()
		render token
	}
	
	def jsonOutput(Object json) {
		render json as JSON
	}
	
}
