package ar.com.telecom

import ar.com.telecom.exceptions.BusinessException
import ar.com.telecom.exceptions.ConcurrentAccessException
import ar.com.telecom.pcs.entities.Actividad
import ar.com.telecom.pcs.entities.CodigoCierre
import ar.com.telecom.pcs.entities.EstadoActividad
import ar.com.telecom.pcs.entities.SoftwareFactory
import ar.com.telecom.pcs.entities.TareaSoporte
import ar.com.telecom.pcs.entities.TipoActividad
import ar.com.telecom.pedido.AprobacionUI
import ar.com.telecom.pedido.especificacionImpacto.Impacto
import ar.com.telecom.util.DateUtil

class ActividadController extends WorkflowController {

	def webUserAgentService
	
	def index = {
		if (webUserAgentService.isBlackberry()) {
			redirect(controller:"inboxMobile", action: "index")
		} else {
			def pedido = llenarDatosPedido(params, true)
			def pedidoHijo
			def actividad
	
			if (params.actividad && params.actividad != 'null'){
				actividad = pedidoService.getActividad(params.actividad)
			}
	
			def impactoInstance
			if (!params.impacto || !Impacto.aplicaAHijo(params.impacto)) {
				params.impacto = Impacto.IMPACTO_CONSOLIDADO
				impactoInstance = new Impacto(params.impacto, pedido)
			}else{
				pedidoHijo = pedidoService.obtenerPedidoHijo(params, params.impacto)
				impactoInstance = new Impacto(params.impacto, pedidoHijo)
			}
	
			def result = [pedidoInstance: pedido, impactoInstance: impactoInstance, actividad: actividad]
			result.putAll(getMapaDatosPedido(pedido))
			result
	
			if (params._action_index){
				if (pedidoHijo){
					redirect (controller: pedidoHijo?.faseActual.controllerNombre, action: "index", params: [pedidoId: pedidoHijo?.parent.id, impacto: pedidoHijo?.id, actividad: actividad?.id])
				}else{
					render (view: "/actividades/index", model: result)
				}
			}else{
				render (view: "/actividades/index", model: result)
			}
		}
	}
	
	/************************************************************************************************************************
	*    PEDIDO
	************************************************************************************************************************/
	def recargaBotonera = {
		withForm {
			try{
				def usuarioLogueado = umeService.getUsuarioLogueado()
				def pedidoHijo = pedidoService.obtenerPedidoHijo(params, params.impacto)

				//pedidoHijo.legajoUsuarioResponsable = params.username // Solo porque el onComplete salta antes de que llegue a guardar en metodo de ajax anterior

				def tienePermisos = pedidoHijo.puedeEditarConstruccionPadre(usuarioLogueado)
				def esElResponsable = usuarioLogueado.equals(pedidoHijo.legajoUsuarioResponsable)

				render (template: "/actividades/botonera", model: [impactoInstance: new Impacto(params.impacto, pedidoHijo), esElResponsable:esElResponsable, tienePermisos:tienePermisos, estaEnFase: pedidoHijo.estaEnFase(params.faseATrabajar)])
			} catch (Exception e) {
				procesarErrorAjax(e)
			}
		}.invalidToken { throw new BusinessException("Operaci\u00F3n no v\u00E1lida. Por favor vuelva a intentarlo.") }
	}

	def cambiaResponsable = {
		withForm {
			try{
				def pedidoHijo = pedidoService.obtenerPedidoHijo(params, params.impacto)
				if (params.legajoNuevo) {
					params.aprobacionId = pedidoHijo?.aprobacionPendiente(pedidoHijo?.faseActual, params.legajoNuevo)?.id
					params.actividadId = null
					pedidoHijo.legajoUsuarioResponsable = params.legajoNuevo
				}
				pedidoService.guardarPedido(params, new AprobacionUI(aprueba: true, pedido: pedidoHijo, usuario: umeService.usuarioLogueado, asignatario: params.legajoNuevo),true)
	
				render (template: "/actividades/datosHijoSeleccionado", model: getDatosHijo(true))
			} catch (Exception e) {
				procesarErrorAjax(e)
			}
		}.invalidToken { throw new BusinessException("Operaci\u00F3n no v\u00E1lida. Por favor vuelva a intentarlo.") }
	}

	def seleccionaHijo = {
		withForm {
			try{
				def result = getDatosHijo(false) + [faseATrabajar: params.faseATrabajar]

				render (template: "/actividades/datosHijoSeleccionado", model: result)
			} catch (Exception e) {
				procesarErrorAjax(e)
			}
		}.invalidToken { throw new BusinessException("Operaci\u00F3n no v\u00E1lida. Por favor vuelva a intentarlo.") }
	}

	def recargaTablaActividades = {
		withForm {
			try{
				def usuarioLogueado = umeService.getUsuarioLogueado()
				def pedidoHijo = pedidoService.obtenerPedidoHijo(params, params.impacto)
				def esElResponsable = usuarioLogueado.equals(pedidoHijo.legajoUsuarioResponsable)
				def listaUsuariosGrupo = getUmeService().getUsuariosGrupoLDAP(pedidoHijo.grupoResponsable, null)
				render (template:"/actividades/tablaActividades", model:[impactoInstance: new Impacto(params.impacto, pedidoHijo), esElResponsable:esElResponsable, usuarioLogueado:usuarioLogueado, faseATrabajar: params.faseATrabajar, estaEnFase: pedidoHijo.estaEnFase(params.faseATrabajar), listaUsuariosGrupo:listaUsuariosGrupo, opciones: ["No":false, "Si":true]])
			} catch (Exception e) {
				procesarErrorAjax(e)
			}
		}.invalidToken { throw new BusinessException("Operaci\u00F3n no v\u00E1lida. Por favor vuelva a intentarlo.") }
	}

	def redibujarDatosHijo() {
		def pedidoHijo
		def personsAutocomplete
		def tienePermisos = true
		def esElResponsable = false
		def usuarioLogueado = umeService.getUsuarioLogueado()
		if (params.impacto!='null' && Impacto.aplicaAHijo(params.impacto)){
			pedidoHijo = pedidoService.obtenerPedidoHijo(params, params.impacto)
			personsAutocomplete = getUmeService().getUsuariosGrupoLDAP(pedidoHijo.grupoResponsable, null)
			tienePermisos = pedidoHijo.puedeEditarConstruccionPadre(usuarioLogueado)

			if (!personsAutocomplete.isEmpty()){
				esElResponsable = personsAutocomplete?.legajo.contains(usuarioLogueado)
			}else{
				esElResponsable = usuarioLogueado.equals(pedidoHijo.legajoUsuarioResponsable)
			}
			if (!params.legajoNuevo) {
				params.legajoNuevo = usuarioLogueado
			}
			if (asignarResponsable(pedidoHijo, params.legajoNuevo)) {
				params.aprobacionId = generarAprobacionId(pedidoHijo, params.legajoNuevo)
				pedidoService.guardarPedido(params, new AprobacionUI(aprueba: true, pedido: pedidoHijo, usuario: umeService.usuarioLogueado, asignatario: params.legajoNuevo))
			}
//			if (cambioResponsable) {
//				if (params.legajoNuevo) {
//					pedidoHijo.legajoUsuarioResponsable = params.legajoNuevo
//				}
//				pedidoService.guardarPedido(params, generarAprobacionUI(pedidoHijo, true))
//			}
		} else {
			pedidoHijo = null
			personsAutocomplete = null
		}
		[impactoInstance: new Impacto(params.impacto, pedidoHijo), personsAutocomplete: personsAutocomplete, tienePermisos:tienePermisos, esElResponsable:esElResponsable, usuarioLogueado:usuarioLogueado, faseATrabajar:params.faseATrabajar, estaEnFase: pedidoHijo?.estaEnFase(params.faseATrabajar)]
	}

	/************************************************************************************************************************
	 *    ACTIVIDAD
	 ************************************************************************************************************************/
	def editarActividad = {
		log.info "editarActividad: ${params}"
		withForm {
			try{
				def pedidoHijo = pedidoService.obtenerPedidoHijo(params, params.impacto)
				def actividad = this.getActividad(pedidoHijo, params.idActividad)
				def usuarioLogueado = umeService.getUsuarioLogueado()
				asignarResponsable(actividad, usuarioLogueado)
	
				def responsablesActividad = getResponsablesActividad(actividad)

				def tiposDeActividad = obtenerComboTipoActividad(pedidoHijo)
				def origenActividad = setOrigenActividad(pedidoHijo, actividad)
				def releases = pedidoService.getReleases(pedidoHijo?.getSistema())
				def show = params?.show

				render (template: "/actividades/editarActividad", model:[impactoInstance: new Impacto(params.impacto, pedidoHijo), actividad: actividad, tiposDeActividad:tiposDeActividad, personsAutocomplete:responsablesActividad, show:show, simular:false,usuarioLogueado:usuarioLogueado, origenActividad: origenActividad?.id, releases:releases])
			} catch (Exception e) {
				procesarErrorAjax(e)
			}
		}.invalidToken { throw new BusinessException("Operaci\u00F3n no v\u00E1lida. Por favor vuelva a intentarlo.") }
	}

	def guardarActividadAnexo = {
		withForm {
			try{
				if (params.idActividad){
					def pedidoHijo = pedidoService.obtenerPedidoHijo(params, params.impacto)
					def actividad = this.getActividad(pedidoHijo, params.idActividad)

					llenarDatosPedido(params, false)
					def aprobacionUI = generarAprobacionUI(actividad)
					pedidoService.guardarPedido(params, aprobacionUI)

					redirect(action: "index", params: [pedidoId: pedidoHijo.parent.id, impacto: params.impacto, actividad: actividad.id, hVengoPorAnexos: params.controller])
				}
			} catch (Exception e) {
				procesarErrorAjax(e)
			}
		}.invalidToken { throw new BusinessException("Operaci\u00F3n no v\u00E1lida. Por favor vuelva a intentarlo.") }
	}

	def cambiaGrupoResponsable ={
		withForm {
			try{
				def responsablesActividad = getUmeService().getUsuariosGrupoLDAP(params.grupoResponsable, null)
				def pedido = pedidoService.obtenerPedidoGenerico(params, params.pedidoId)
				def pedidoHijo = pedidoService.obtenerPedidoHijo(params, params.impacto)
				def actividad = this.getActividad(pedidoHijo, params.idActividad)
				
				def usuarioLogueado = umeService.getUsuarioLogueado()

				actividad.grupoResponsable = params.grupoResponsable

				actividad?.legajoUsuarioResponsable = null
				if(umeService.usuarioPerteneceGrupo(params.grupoResponsable, usuarioLogueado)){
					actividad?.legajoUsuarioResponsable = usuarioLogueado
				}

				if(Actividad.esUsuarioFinal(params.grupoResponsable)){
					actividad?.legajoUsuarioResponsable = pedido?.parent.legajoUsuarioCreador
				}
				if(Actividad.esInterlocutorUsuario(params.grupoResponsable)){
					actividad?.legajoUsuarioResponsable = pedido?.parent.legajoInterlocutorUsuario
				}
				
				if (actividad.id){
					params.actividadId = params.idActividad
					pedidoService.cambiarResponsable(params, new AprobacionUI(pedido: actividad.pedido, asignatario: actividad?.legajoUsuarioResponsable, usuario: umeService.usuarioLogueado, grupo: actividad.grupoResponsable))
				}
				
				def origenActividad = getOrigenActividad(pedidoHijo, params.idTipoActividad)
				
				render (template: "/actividades/responsableActividad", model:[pedidoHijo: pedidoHijo, usuarioLogueado: usuarioLogueado, asignatarioFase: usuarioLogueado.equalsIgnoreCase(actividad?.legajoUsuarioResponsable), actividad: actividad, personsAutocomplete: responsablesActividad, grupoResponsable: params.grupoResponsable, show: false, origenActividad:origenActividad?.id])
			} catch (Exception e) {
				procesarErrorAjax(e)
			}
		}.invalidToken { throw new BusinessException("Operaci\u00F3n no v\u00E1lida. Por favor vuelva a intentarlo.") }
	}

	def cambiaFormularioCamposActividades ={
		withForm {
			try{
				def responsablesActividad = getUmeService().getUsuariosGrupoLDAP(params.grupoResponsable, null)
				def pedido = pedidoService.obtenerPedidoGenerico(params, params.pedidoId)
				def pedidoHijo = pedidoService.obtenerPedidoHijo(params, params.impacto)
				def actividad = this.getActividad(pedidoHijo, params.idActividad)
				if (params.responsableActividadTemporal){
					actividad?.legajoUsuarioResponsablePop = params.responsableActividadTemporal
				}
				def usuarioLogueado = umeService.getUsuarioLogueado()
				def releases = pedidoService.getReleases(pedidoHijo?.getSistema())
				def tipoActividad = getTipoActividadSeleccionado(pedidoHijo, params.idTipoActividad)
				def origenActividad = getOrigenActividad(pedidoHijo, params.idTipoActividad)
				def show = params?.show

				actividad.grupoResponsable = params.grupoResponsable
				render (template: "/actividades/formularioCamposActividades", model:[impactoInstance: new Impacto(params.impacto, pedidoHijo), actividad: actividad, tipoActividad:tipoActividad, show:false, usuarioLogueado: usuarioLogueado, impacto: params.impacto, origenActividad: origenActividad?.id, releases:releases, show:show])
			} catch (Exception e) {
				procesarErrorAjax(e)
			}
		}.invalidToken { throw new BusinessException("Operaci\u00F3n no v\u00E1lida. Por favor vuelva a intentarlo.") }
	}

	def cambiaTipoActividad = {
		withForm {
			try{
				def pedidoHijo = pedidoService.obtenerPedidoHijo(params, params.impacto)
				def actividad = this.getActividad(pedidoHijo, params.idActividad)
				def tipoActividad = getTipoActividadSeleccionado(pedidoHijo, params.idTipoActividad)
				def origenActividad = getOrigenActividad(pedidoHijo, params.idTipoActividad)
				def usuarioLogueado = umeService.getUsuarioLogueado()
				def releases = pedidoService.getReleases(pedidoHijo?.getSistema())

				if(!actividad?.grupoResponsable){
					actividad.grupoResponsable = pedidoHijo?.grupoResponsable
				}

				if(!actividad?.legajoUsuarioResponsable){
					actividad.legajoUsuarioResponsable = pedidoHijo.legajoUsuarioResponsable
				}
				render (template: "/actividades/formularioSolapaDescripcion", model:[impactoInstance: new Impacto(params.impacto, pedidoHijo), actividad: actividad, tipoActividad:tipoActividad, show:false, usuarioLogueado: usuarioLogueado, impacto: params.impacto, origenActividad: origenActividad?.id, releases:releases])
			} catch (Exception e) {
				procesarErrorAjax(e)
			}
		}.invalidToken { throw new BusinessException("Operaci\u00F3n no v\u00E1lida. Por favor vuelva a intentarlo.") }
	}

	def grabaActividad = {
		withForm {
			try {
				def pedidoHijo = pedidoService.obtenerPedidoHijo(params, params.impacto)
				def usuarioLogueado = umeService.getUsuarioLogueado()
				def actividad = this.generarActividad(pedidoHijo, false)
				def aprobacionUI = generarAprobacionUI(actividad)
				
				if (pedidoService.guardarActividad(actividad)){
					flash.message = "La actividad se guard\u00F3 correctamente"
				} else {
					actividad.pedido.errors.reject "La actividad no pudo ser guardada"
				}

				if (params.id?.equals("aceptarynuevaActividad")){
					actividad = new Actividad(legajoUsuarioResponsable: usuarioLogueado)
				}

				render (template: "/actividades/editarActividad", model: [impactoInstance: new Impacto(params.impacto, pedidoHijo), actividad: actividad, show: false, tipoActividad: actividad.tipoActividad] + getMapaDatosActividad(pedidoHijo, actividad))
			} catch (Exception e) {
				procesarErrorAjax(e)
			}
		}.invalidToken { throw new BusinessException("Operaci\u00F3n no v\u00E1lida. Por favor vuelva a intentarlo.")  }
	}
	
	def simularActividad = {
		withForm {
			try{
				def pedidoHijo = pedidoService.obtenerPedidoHijo(params, params.impacto)
				def usuarioLogueado = umeService.getUsuarioLogueado()
				def actividad = generarActividad(pedidoHijo, true)

				// TODO: ¿C\u00F3mo puede ser que cambie el objeto receptor?
				if (actividad.validar(pedidoHijo)) {
					flash.message = "La actividad est\u00E1 ok"
				}

				//render (template: "/actividades/editarActividad", model: [impactoInstance: new Impacto(params.impacto, pedidoHijo), actividad: actividad, show: false,simular:true] + getMapaDatosActividad(pedidoHijo, actividad))
				render (template: "/actividades/formularioCamposActividades", model: [impactoInstance: new Impacto(params.impacto, pedidoHijo), actividad: actividad, show: false,simular:true, tipoActividad: actividad.tipoActividad] + getMapaDatosActividad(pedidoHijo, actividad))
			} catch (Exception e) {
				procesarErrorAjax(e)
			}
		}.invalidToken { throw new BusinessException("Operaci\u00F3n no v\u00E1lida. Por favor vuelva a intentarlo.") }
	}

	def popFinalizaActividad = {
		withForm {
			try{
				def pedidoHijo = pedidoService.obtenerPedidoHijo(params, params.impacto)
				def actividad = this.getActividad(pedidoHijo, params.idActividad)
				def codigosCierre = getCodigosDeCierre(params.botonOrigen)

				render (template: "/actividades/finalizarActividad", model:[impactoInstance: new Impacto(params.impacto, pedidoHijo), actividad:actividad, recargaEditarPop:params.recargaEditarPop, botonOrigen:params.botonOrigen, codigosCierre:codigosCierre])
			} catch (Exception e) {
				procesarErrorAjax(e)
			}
		}.invalidToken {
			e.printStackTrace()
			throw new BusinessException("Operaci\u00F3n no v\u00E1lida. Por favor vuelva a intentarlo.")
		}
	}

	def eliminarAnexoActividad = {
		withForm {
			try {
				def actividad
				if (params.idActividad) {
					actividad = Actividad.findById(params.idActividad?.toInteger())
					anexoService.eliminarAnexo(params, actividad.pedido, params.anexoId, actividad)
				}
				def usuarioLogueado = umeService.getUsuarioLogueado()
				render(template: "/actividades/anexoTabla", model:[listAnexos: actividad.anexos.sort{it.id}, actividad: actividad, pedidoHijoID: params.impacto , impacto: params.impacto, anexoFrom: params.anexoFrom, usuarioLogueado:usuarioLogueado])
			} catch (Exception e) {
				procesarErrorAjax(e)
			}
		}.invalidToken { throw new BusinessException("Operaci\u00F3n no v\u00E1lida. Por favor vuelva a intentarlo.") }
	}

	def finalizarActividad = {
		withForm {
			try{
				def pedidoHijo = pedidoService.obtenerPedidoHijo(params, params.impacto)
				def usuarioLogueado = umeService.getUsuarioLogueado()
				params.legajoUsuarioCierre = usuarioLogueado
				def actividad = getActividad(pedidoHijo, params.idActividad)
				bindData(actividad, params)

				if (actividad.validar(pedidoHijo) && pedidoService.finalizarActividad(actividad)){
					flash.message = "La actividad fue finalizada"
				} 
				
				def codigosCierre = getCodigosDeCierre(params.botonOrigen)
				render (template: "/actividades/finalizarActividad", model:[impactoInstance: new Impacto(params.impacto, pedidoHijo), actividad:actividad, recargaEditarPop:params.recargaEditarPop, codigosCierre:codigosCierre, botonOrigen:params.botonOrigen])
			} catch (Exception e) {
				procesarErrorAjax(e)
			}
		}.invalidToken { throw new BusinessException("Operaci\u00F3n no v\u00E1lida. Por favor vuelva a intentarlo.") }
	}

	/*************************************************************************************************************************
	 * METODOS PRIVADOS
	 ************************************************************************************************************************/
	def getResponsablesActividad(actividad){
		def responsablesActividad = null
		if(actividad.grupoResponsable){
			responsablesActividad = getUmeService().getUsuariosGrupoLDAP(actividad.grupoResponsable, null)
		}
		return responsablesActividad
	}

	def getTiposDeActividad(pedidoHijo){
		def tiposDeActividad = TipoActividad.findAllByTipoImpactoAndFase(pedidoHijo.tipoImpacto, pedidoHijo.faseActual)
		def tiposDeActividadFiltradas = tiposDeActividad.findAll { tipoActividad ->
			tipoActividad.tipoGestion.equals(pedidoHijo.pedidoPadre.tipoGestion)
		}
		return tiposDeActividadFiltradas
	}

	def getTareasSoporte(pedidoHijo){
		def tareasSoporte
		if(pedidoHijo.areaSoporte){
			tareasSoporte = TareaSoporte.findAllByAreaSoporte(pedidoHijo.areaSoporte)
		}else{
			// Administradores Funcionales
			tareasSoporte = TareaSoporte.list().findAll { tareaSoporte -> tareaSoporte.areaSoporte == null }
		}
		return tareasSoporte
	}


	def obtenerComboTipoActividad(pedidoHijo){
		if (pedidoHijo.tipoImpacto.asociadoSistemas) {
			return getTiposDeActividad(pedidoHijo)
		}else{
			return getTareasSoporte(pedidoHijo)
		}
	}

	def getActividad(pedidoHijo, idActividad) {
		def actividad
		if (!idActividad || idActividad.equals('')){
			def usuarioLogueado = umeService.usuarioLogueado
			def usuarioResponsable = umeService.getUsuario(pedidoHijo.legajoUsuarioResponsable)
			if (!usuarioLogueado.equalsIgnoreCase(pedidoHijo.legajoUsuarioResponsable)){
				throw new ConcurrentAccessException("El usuario " + usuarioResponsable.nombreApellido + " (" + usuarioResponsable.legajo + ") es el asignatario del pedido " + pedidoHijo)
			}
			
			if(params.responsableActividadTemporal && params.responsableActividadTemporal == ''){
				params.responsableActividadTemporal = null
			}
			
			actividad = new Actividad(legajoUsuarioResponsable: params.responsableActividadTemporal?:usuarioLogueado)
			//pedidoHijo.agregarActividad(actividad, usuarioLogueado, true)
		} else {
			actividad = pedidoService.getActividad(idActividad)
		}
		return actividad
	}

	def getCodigosDeCierre(botonOrigen){
		def codes = new ArrayList()

		if(botonOrigen == 'btnFinalizarGrilla'){
			codes.add(CodigoCierre.exitoso())
			codes.add(CodigoCierre.observaciones())
			codes.add(CodigoCierre.fallida())
		}

		if(botonOrigen == 'btnCancelaGrilla'){
			codes.add(CodigoCierre.cancelada())
		}

		if(botonOrigen == 'btnRechazar'){
			codes.add(CodigoCierre.rechazada())
		}

		return codes
	}

	def generarAprobacionUI(boolean aprueba) {
		def pedidoHijo = pedidoService.obtenerPedidoHijo(params, params.impacto)
		return generarAprobacionUI(pedidoHijo, aprueba)
	}

	def generarAprobacionUI(pedidoHijo, aprueba) {
		// TODO: Llevar el legajoUsuarioResponsable
		// PREGUNTAR A NICO/GATO
		//pedidoHijo.legajoUsuarioResponsable = params.legajoResponsableConstruccionPadre
		return new AprobacionUI(justificacion: params.justificacionAprobacion, aprueba: aprueba, pedido: pedidoHijo, usuario: umeService.usuarioLogueado, asignatario: params.legajoResponsableConstruccionPadre)
	}

	def generarAprobacionUI(Actividad actividad) {
		// TODO: Llevar el legajoUsuarioResponsable
		// PREGUNTAR A NICO/GATO
		//pedidoHijo.legajoUsuarioResponsable = params.legajoResponsableConstruccionPadre
		return new AprobacionUI(justificacion: params.justificacionAprobacion, aprueba: true, pedido: actividad.pedido, usuario: umeService.usuarioLogueado, asignatario: actividad.legajoUsuarioResponsable)
	}

	def faseFinalizada(pedido) {
		def result = [pedidoInstance: pedido.parent]
		result.putAll(getMapaDatosPedido(pedido))
		render(view: "/actividades/index", model: result)
	}

	def mostrarResultados(ok, mensajeOk, aprobacionUI, finaliza) {
		def pedido = aprobacionUI.pedido

		if (ok) {
			flash.message = mensajeOk
		}
		if (ok && finaliza) {
			faseFinalizada(pedido)
		}else{
			// Si hay errores, que vuelva al index
			def modelView = [pedidoInstance: pedido.parent, justificacionAprobacion: params.justificacionAprobacion]
			modelView.putAll(getMapaDatosPedido(pedido))
			render(view: "/actividades/index", model: modelView)
		}
	}

	def getImpactoInstance() {
		if (!params.impacto || !Impacto.aplicaAHijo(params.impacto)) {
			def pedido = pedidoService.obtenerPedido(params, params.pedidoId)
			params.impacto = Impacto.IMPACTO_CONSOLIDADO
			return new Impacto(params.impacto, pedido)
		}else{
			def pedidoHijo = pedidoService.obtenerPedidoHijo(params, params.impacto)
			return new Impacto(params.impacto, pedidoHijo)
		}
	}

	def getTipoActividadSeleccionado(pedidoHijo, idTipoActividad){
		if (pedidoHijo.asociadoSistemas()){
			return TipoActividad.findById(idTipoActividad)
		}else{
			return TareaSoporte.findById(idTipoActividad).tipoActividad
		}
	}

	def getOrigenActividad(pedidoHijo, idOrigen){
		if (pedidoHijo.asociadoSistemas()){
			return TipoActividad.findById(idOrigen)
		}else{
			return TareaSoporte.findById(idOrigen)
		}
	}

	def setOrigenActividad(pedidoHijo, actividad){
		if (pedidoHijo.asociadoSistemas()){
			return actividad.tipoActividad
		}else{
			return TareaSoporte.findByDescripcionAndTipoActividad(actividad.descripcion, actividad.tipoActividad)
		}
	}

	def getDatosHijo(boolean cambiaResponsable) {
		def result = [cambiaResponsable: cambiaResponsable]
		result = result.plus(redibujarDatosHijo())
		result = result.plus(opciones: ["No":false, "Si":true])
		return result
	}

	Actividad generarActividad(pedidoHijo, boolean simular) {
		def actividad = getActividad(pedidoHijo, params.idActividad)

		def usuarioLogueado = umeService.getUsuarioLogueado()

		// Se asigna antes del bindData para evitar java util exception
		if(params.fechaSugerida){
			params.fechaSugerida = DateUtil.toDate(params.fechaSugerida)
		}

		bindData(actividad, params)
		
		if (simular) {
			actividad.legajoUsuarioResponsable = params.legajoUsuarioResponsablePop
		}
		actividad.fase = pedidoHijo.faseActual
		actividad.estado = EstadoActividad.abierta()

		if (params.comentarioNuevo && !simular){
			actividad.agregarComentario(params.comentarioNuevo, usuarioLogueado, umeService.getUsuario(usuarioLogueado).nombreApellido)
			params.comentarioNuevo = ""
		}

		if (params.tipoActividad && params.tipoActividad?.id){
			actividad.tipoActividad = getTipoActividadSeleccionado(pedidoHijo, params.tipoActividad?.id)
			actividad.descripcion = getOrigenActividad(pedidoHijo, params.tipoActividad?.id).descripcion
		}

		def idActividad = actividad.id
		
		if (!idActividad || idActividad.equals('')) {
			pedidoHijo.agregarActividad(actividad, usuarioLogueado, true)
		}
		
		return actividad
	}

	def getMapaDatosActividad(pedidoHijo, actividad) {
		def tiposDeActividad = obtenerComboTipoActividad(pedidoHijo)
		def responsablesActividad = getResponsablesActividad(actividad)
		def origenActividad = setOrigenActividad(pedidoHijo, actividad)
		def releases = pedidoService.getReleases(pedidoHijo?.getSistema())
		def usuarioLogueado = umeService.getUsuarioLogueado()
		
		return [tiposDeActividad:tiposDeActividad, personsAutocomplete:responsablesActividad, origenActividad: origenActividad?.id, releases:releases, usuarioLogueado:usuarioLogueado]
	}
	
	boolean asignarResponsable(origen, usuario) {
		if (!origen.legajoUsuarioResponsable && umeService.usuarioPerteneceGrupo(origen.grupoResponsable, usuario)) {
			origen.legajoUsuarioResponsable = usuario
			return true
		} else {
			return false
		}
	}

}
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                 