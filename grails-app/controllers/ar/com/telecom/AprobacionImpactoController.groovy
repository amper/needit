package ar.com.telecom

import ar.com.telecom.pedido.especificacionImpacto.Impacto

class AprobacionImpactoController extends AbstractImpactoController {

	def doGetMapaDatosPedido(pedido) {
		[aprobacionesList: pedido?.aprobaciones.findAll { aprobacion -> aprobacion.posteriorAFechaYFase(pedido.fechaValidacionImpacto, pedido.faseAprobacionImpacto()) }]
	}

}

