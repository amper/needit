package ar.com.telecom

import ar.com.telecom.pcs.entities.RolAplicacion
import ar.com.telecom.pedido.AprobacionUI

class ReasignacionRolController extends WorkflowController{

    def index = { 
		
	}
	
	def popupReasignar = {
		def pedido = pedidoService.obtenerPedido(params, params.pedidoId)
		Map mapaDirecciones = RolAplicacion.getDirecciones(pedido)
		def usuarioLogueado = umeService.usuarioLogueado
		def rolesAReasignar = pedido.getRolesAReasignar(usuarioLogueado, umeService.esAdministrador(usuarioLogueado))
		
		pedidoService.guardar(params, pedido)
		render (template: "/reasignacionRol/reasignacionRol", model:[pedidoInstance: pedido, usuarioLogueado:usuarioLogueado, rolesAReasignar:rolesAReasignar, esAdmin:umeService.esAdministrador(usuarioLogueado)])
	}
	
	def seleccionaRolAReasignar = {
		def pedido = pedidoService.obtenerPedido(params, params.pedidoId)
		def rol = RolAplicacion.findByCodigoRol(params.rolSeleccionado)
		def nuevosAsignatarios = getNuevosAsignatarios(pedido, rol, null)
		
		render (template: "/reasignacionRol/rolSeleccionado", model:[pedidoInstance: pedido, rol: rol, mapaDirecciones:RolAplicacion.getDirecciones(pedido), nuevosAsignatarios:nuevosAsignatarios])
	}
	
	def seleccionaDireccion = {
		def pedido = pedidoService.obtenerPedido(params, params.pedidoId)
		def rol = RolAplicacion.findByCodigoRol(params.rolSeleccionado)
		def nuevosAsignatarios = getNuevosAsignatarios(pedido, rol, params.direccionSeleccionada)
		
		render (template: "/templates/userBox", model:[box:'nuevoAsignatario',personsAutocomplete : nuevosAsignatarios])
	}
	
	
	def reasignar = {
		def pedido = pedidoService.obtenerPedido(params, params.pedidoId)
		def aprobacionesAReasignar = pedido.aprobacionesPendientes(params.rolAReasignar, params.legajoAReasignar)
		def aprobacionAReasignar = aprobacionesAReasignar.isEmpty() ? null : aprobacionesAReasignar.first()
		def rolAReasignar = RolAplicacion.findByCodigoRol(params.rolAReasignar)
		def nuevoAsignatario = params.nuevoAsignatario
		def usuarioLogueado = umeService.getUsuarioLogueado()
		boolean esAdmin = umeService.esAdministrador(usuarioLogueado)
		
		if(validarReasignacion(pedido, rolAReasignar, nuevoAsignatario, esAdmin)){
			def legajoActual = pedido.getLegajo(rolAReasignar)
			
			if(aprobacionAReasignar){
				params.aprobacionId = aprobacionAReasignar.id
				def aprobacionUI = new AprobacionUI(pedido:pedido, asignatario: nuevoAsignatario)
				pedidoService.cambiarResponsable(params, aprobacionUI)
			}

			def grupoActividad = rolAReasignar.grupoActividad
			if (grupoActividad) {
				pedidoService.reasignarActividades(pedido, grupoActividad, legajoActual, nuevoAsignatario)
			}
		
			pedido.reasignarUsuarioDeRol(rolAReasignar.codigoRol, nuevoAsignatario)
			pedido.comentarioReasignacion = params.comentarioReasignacion
			pedido.rqSimplitReasignacion = params.rqsimplit
			pedidoService.guardar(params, pedido.parent)
			
			render (template: "/reasignacionRol/reasignacionFinalizada", model:[pedidoInstance: pedido, usuarioLogueado:usuarioLogueado])
		}else{
			def nuevosAsignatarios = getNuevosAsignatarios(pedido, rolAReasignar, params.direccion)
			def rolesAReasignar = pedido.getRolesAReasignar(usuarioLogueado, esAdmin)
			
			render (template: "/reasignacionRol/reasignacionRol", model:[pedidoInstance: pedido, usuarioLogueado:usuarioLogueado, rolAReasignar: rolAReasignar, mapaDirecciones: RolAplicacion.getDirecciones(pedido), recarga:true, direccion: params.direccion, legajoNuevo: nuevoAsignatario, comentarioReasignacion: params.comentarioReasignacion, nuevosAsignatarios:nuevosAsignatarios, rolesAReasignar:rolesAReasignar, esAdmin:esAdmin])
		}
	}
	
	private boolean validarReasignacion(pedido, rolAReasignar, nuevoAsignatario, esAdmin){
		if(!rolAReasignar){
			pedido.errors.reject "Debe seleccionar un rol para realizar la reasignación"
		}
		
		if(!nuevoAsignatario){
			pedido.errors.reject "Debe seleccionar un usuario a quien asignarle las aprobaciones"
		}
		
		if(esAdmin &&  (params.rqsimplit == "") ){
			pedido.errors.reject "Para usuarios administradores el tkt Simplit es obligatorio"
		}
		
		return pedido.errors.errorCount == 0
	}
	
	private def getNuevosAsignatarios(pedido, rol, direccion){
		if(!pedido || !rol){
			return []
		}

		boolean esDirector = umeService.getUsuario( pedido.getLegajo(rol))?.isDirector() 
				
		def metodoParaObtenerAsignatarios = rol.getMensajeUsuariosDeRol(pedido, direccion, esDirector)
				
		return metodoParaObtenerAsignatarios.call(umeService)
	}
}
