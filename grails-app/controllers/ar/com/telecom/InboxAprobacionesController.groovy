package ar.com.telecom

import grails.validation.ValidationException
import ar.com.telecom.exceptions.BusinessException
import ar.com.telecom.pcs.entities.Pedido
import ar.com.telecom.pedido.AprobacionUI
import ar.com.telecom.pedido.consulta.Consulta
import ar.com.telecom.pedido.especificacionImpacto.Impacto

class InboxAprobacionesController extends WorkflowController {

	def webUserAgentService

	def index = {
		if (webUserAgentService.isBlackberry()) {
			redirect(controller:"inboxMobile", action: "index")
		} else {
			redirect(action: "list", params: params)
		}
	}

	def list = {
		internalInboxAprobaciones()
	}

	def inboxCount = {
		try {
			def usuarioLogueado = umeService.getUsuarioLogueado()
			def pedidoInstanceList = obtenerPedidos(usuarioLogueado)
			render pedidoInstanceList.size()
		} catch (Exception e) {
			render ""
		}
	}

	def actualizar = {
		//redirect(action: "list", params: params)
		withForm {  
			obtenerListadoInbox(null)  
		}.invalidToken { throw new BusinessException("Operaci\u00F3n no v\u00E1lida. Por favor vuelva a intentarlo.") }
	}
	

	 def aprobarAprobaciones(resultAprobaciones, pedido){
		 def impactoInstance = new Impacto(Impacto.IMPACTO_CONSOLIDADO, pedido.parent)
		 def aprobacionesUI = impactoInstance.generarAprobaciones(true, params.justificacionAprobacion, umeService.usuarioLogueado)
		 aprobacionesUI.each{ aprobacionUI ->
			 params.aprobacionId = pedido.aprobacionPendiente(pedido?.faseActual, umeService.usuarioLogueado)?.id
			 def apruebaOk = pedidoService.avanzarPedido(params, aprobacionUI)
			 generarErrores(resultAprobaciones, pedido, apruebaOk)
		 }
	}

	def aprobarPedido = {
		withForm {
			try{
				def listAprobaciones = aprobacionesSeleccionadas()
				def resultAprobaciones = new Pedido()
				if (listAprobaciones.isEmpty()) {
					resultAprobaciones.errors.reject "Debe seleccionar pedidos"
				} else {
					listAprobaciones.each {
						params.aprobacionId = it.toLong()
						def aprobacion = pedidoService.obtenerAprobacion(params.aprobacionId)
						def pedido = aprobacion.pedido
						if (pedido.esHijo() && !pedido.parent.pedidosHijosQueConsolidanPAU().isEmpty()){
							aprobarAprobaciones(resultAprobaciones, pedido)
						}else{
							boolean avanzo
							if (pedido.estaEnCurso()){
								def aprobacionUI = new AprobacionUI(justificacion: params.justificacionAprobacion, aprueba: true, pedido: pedido, asignatario: umeService.usuarioLogueado, usuario: umeService.usuarioLogueado)
								avanzo = pedidoService.avanzarPedido(params, aprobacionUI)
							}else{
								avanzo = false
								pedido.errors.reject "El pedido no se encuentra en curso"
							}
							generarErrores(resultAprobaciones, pedido, avanzo)
						}
					}
				}
				flash.message = ""
				obtenerListadoInbox(resultAprobaciones)
			} catch (Exception e) {
				procesarErrorAjax(e)
			}
		}.invalidToken { throw new BusinessException("Operaci\u00F3n no v\u00E1lida. Por favor vuelva a intentarlo.") }
	}
	
	def obtenerPedido(pedidoId){
		return pedidoService.obtenerPedidoGenerico(params, params.pedidoId, false)
	}

	def obtenerListadoInbox(resultAprobaciones){
		def model = internalInboxAprobaciones()
		render (template: "/templates/inboxAprobaciones", model: model + [resultAprobaciones: resultAprobaciones])
	}

	def denegarPedido = {
		withForm {
			try{
				def listAprobaciones = aprobacionesSeleccionadas()
				def resultAprobaciones = new Pedido()
				if (listAprobaciones.isEmpty()) {
					resultAprobaciones.errors.reject "Debe seleccionar pedidos"
				} else {
					if (params.justificacionAprobacion){
						listAprobaciones.each {
							params.aprobacionId = it.toLong()
							def aprobacion = pedidoService.obtenerAprobacion(params.aprobacionId)
							def pedido = aprobacion.pedido
							boolean retrocedo
							if (pedido.estaEnCurso()){
								def aprobacionUI = new AprobacionUI(justificacion: params.justificacionAprobacion, aprueba: false, pedido: pedido, asignatario: umeService.usuarioLogueado, usuario: umeService.usuarioLogueado)
								retrocedo = pedidoService.denegarPedido(params, aprobacionUI)
							}else{
								retrocedo = false
								pedido.errors.reject "El pedido no se encuentra en curso"
							}
							generarErrores(resultAprobaciones, pedido, retrocedo)
						}
						params.inbox = []
					} else {
						resultAprobaciones.errors.reject "Debe ingresar una justificaci\u00F3n para denegar"
					}
				}
				flash.message = ""
				obtenerListadoInbox(resultAprobaciones)
			} catch (Exception e) {
				procesarErrorAjax(e)
			}
		}.invalidToken { throw new BusinessException("Operaci\u00F3n no v\u00E1lida. Por favor vuelva a intentarlo.") }
	}

	def internalInboxAprobaciones() {
		try{
			def usuarioLogueado = umeService.getUsuarioLogueado()
			def pedidoInstanceList = obtenerPedidos(usuarioLogueado)
			pedidoService.inboxAprobaciones(pedidoInstanceList, params)
		} catch (Exception e) {
			procesarErrorAjax(e)
		}
	}

	def obtenerPedidos(usuarioLogueado){
		params.tipoConsulta = Consulta.INBOX_APROBACIONES
		return pedidoService.consultarInboxAprobaciones(params, usuarioLogueado)
	}

	def aprobacionesSeleccionadas(){
		def listAprobaciones = []
		if (params.inbox) {
			params.inbox = params.inbox.class.isArray() ? params.inbox as List : [params.inbox]
			listAprobaciones = params.inbox
		}
		listAprobaciones
	}

	def generarErrores(resultAprobaciones, pedido, avanzo) {
		if (!avanzo) {
			def errores = new StringBuffer()
			pedido.errors.each {
				errores.append (it.globalError.code+ " - ")
			}
			if(errores){
				resultAprobaciones.errors.reject "Pedido: " + pedido + "   *** Errores: "+ errores.toString()
			}
		}
	}
}
