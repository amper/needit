package ar.com.telecom

import grails.validation.ValidationException
import ar.com.telecom.pcs.entities.PedidoHijo
import ar.com.telecom.pedido.AprobacionUI
import ar.com.telecom.pedido.especificacionImpacto.Impacto

class AprobacionImplantacionHijoController extends AprobacionPAUHijoController {

	def definirImpacto(pedido) {
		def pedidoHijo
		def impactoInstance
		def impactoClave = params.impacto
		def comboImpacto = Impacto.IMPACTO_CONSOLIDADO

		if (impactoClave && Impacto.aplicaAHijo(impactoClave)) {
			pedidoHijo = pedidoService.obtenerPedidoHijo(params, impactoClave)
			comboImpacto = impactoClave
			impactoInstance = new Impacto(Impacto.IMPACTO_HIJO, pedidoHijo)
		} else {
			impactoInstance = new Impacto(impactoClave, pedido)
		}

		log.info "Impacto: " + impactoClave
		log.info "Combo Impacto: " + comboImpacto

		return [
			impactoInstance: impactoInstance,
			impacto: impactoClave,
			comboImpacto: comboImpacto
		]
	}

	def getMapaDatosPedido(pedido) {
		def result = definirImpacto(pedido)
		def usuarioLogueado = umeService.getUsuarioLogueado() 
		def faseATrabajar = FaseMultiton.APROBACION_IMPLANTACION_HIJO
		def pedidoATrabajar = result?.impactoInstance?.pedido

		def impactos = []
		impactos.addAll(pedido.parent.pedidosConAprobacionImplantacion().collect { pedidoHijo ->
			new Impacto(Impacto.IMPACTO_HIJO, pedidoHijo)
		})
		
		def tienePermisos = pedidoATrabajar?.puedeEditarAprobacionImplantacion(usuarioLogueado)
		def aprobacionInstance = pedidoATrabajar?.primeraAprobacionDeUsuario(usuarioLogueado)
		if (tienePermisos && !aprobacionInstance.usuario) {
			aprobacionInstance.usuario = usuarioLogueado
		}
		def esElAprobador = pedidoATrabajar?.usuarioEsUsuarioDeAprobacion(usuarioLogueado, FaseMultiton.getFase(FaseMultiton.APROBACION_IMPLANTACION_HIJO))

		return  [
			usuarioLogueado: usuarioLogueado, 
			usuarioLogueado: usuarioLogueado,
			impactos: impactos,
			faseATrabajar: faseATrabajar,
			aprobacionInstance: aprobacionInstance, 
			tienePermisos:tienePermisos, 
			esElAprobador:esElAprobador
		] + result
	}

	def getAprobacionesHijo = {
		// Por què otra vez?? pedidoId
		def pedidoHijo = PedidoHijo.findById(params.id)
		def impactoInstance = new Impacto(Impacto.IMPACTO_HIJO, pedidoHijo)
		render (template: "aprobacionesHijo", model: [impactoInstance: impactoInstance])
	}

	def recargaBotonera = {
		def pedido = pedidoService.obtenerPedido(params, params.pedidoId)
		def usuario
		if (params.legajoNuevo){
			usuario = params.legajoNuevo
		} else{
			usuario = umeService.getUsuarioLogueado()
		}
		render (template: "botonera", model: getMapaDatosPedido(pedido))
	}

	def reasignaAprobaciones = {
		def pedidoHijo = pedidoService.obtenerPedidoHijo(params, params.impacto)
		def faseImplantacion = FaseMultiton.getFase(FaseMultiton.APROBACION_IMPLANTACION_HIJO)
		pedidoHijo.reasignarUsuarioEnAprobacion(params.legajoAnterior, params.legajoNuevo, FaseMultiton.getFase(FaseMultiton.APROBACION_IMPLANTACION_HIJO))
		def aprobacionUI = new AprobacionUI(justificacion: "", aprueba: true, pedido: pedidoHijo)
		pedidoService.guardarPedido(params, aprobacionUI)
	}

}
