package ar.com.telecom
import ar.com.telecom.exceptions.BusinessException
import ar.com.telecom.pcs.entities.Actividad
import ar.com.telecom.pcs.entities.TipoAnexo
import ar.com.telecom.pedido.AprobacionUI
import ar.com.telecom.pedido.especificacionImpacto.Impacto

class ConstruccionPadreController extends ActividadController {

	def editarActividadAnexo = {
		def pedidoHijo = pedidoService.obtenerPedidoHijo(params, params.impacto)
		def actividad = this.getActividad(pedidoHijo, params.idActividad)
		def tiposDeAnexo = TipoAnexo.findAllByFaseInList([
			FaseMultiton.getFase(pedidoHijo?.faseConstruccion()?.codigoFase)
		])
		def usuarioLogueado = umeService.getUsuarioLogueado()

		render (template: "/templates/anexoPorTipoActividad", model:[pedidoHijo: pedidoHijo, actividad: actividad, tiposDeAnexo:tiposDeAnexo, usuarioLogueado:usuarioLogueado, impacto: params.impacto])
	}

	private def getMapaDatosPedido(pedido) {
		def usuarioLogueado = umeService.getUsuarioLogueado()

		def pedidoHijo
		if (Impacto.aplicaAHijo(params.impacto)){
			pedidoHijo = pedidoService.obtenerPedidoHijo(params, params.impacto)
		}
		def faseATrabajar = FaseMultiton.CONSTRUCCION
		if (pedidoHijo) {
			faseATrabajar = pedidoHijo?.faseConstruccion()?.codigoFase
		}

		[usuarioLogueado:usuarioLogueado, faseATrabajar: faseATrabajar, impactoInstance: getImpactoInstance()]
	}

	def generarAprobacionUI(aprueba) {
		def pedidoHijo = pedidoService.obtenerPedidoHijo(params, params.impacto)
		if (params.impactaBaseArquitectura.equals("null")){
			pedidoHijo.impactaBaseArquitectura = null
		}else{
			pedidoHijo.impactaBaseArquitectura = new Boolean(params.impactaBaseArquitectura)
		}

		if (params.requiereManualUsuario.equals("null")){
			pedidoHijo.requiereManualUsuario = null
		}else{
			pedidoHijo.requiereManualUsuario =  new Boolean(params.requiereManualUsuario)
		}

		if (params.actualizaCarpetaOperativa.equals("null")) {
			pedidoHijo.actualizaCarpetaOperativa = null
		}else{
			pedidoHijo.actualizaCarpetaOperativa =  new Boolean(params.actualizaCarpetaOperativa)
		}
		return new AprobacionUI(justificacion: params.justificacionAprobacion, aprueba: aprueba, pedido: pedidoHijo, usuario: umeService.usuarioLogueado, asignatario: params.legajoResponsableConstruccionPadre)
	}

}
