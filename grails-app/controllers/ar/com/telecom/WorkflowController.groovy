package ar.com.telecom

import grails.validation.ValidationException
import ar.com.telecom.entitiesume.Usuario
import ar.com.telecom.exceptions.BusinessException
import ar.com.telecom.pcs.entities.Actividad
import ar.com.telecom.pcs.entities.AnexoPorTipo
import ar.com.telecom.pcs.entities.MotivoSuspension
import ar.com.telecom.pcs.entities.ParametrosSistema
import ar.com.telecom.pcs.entities.RolAplicacion
import ar.com.telecom.pcs.entities.TipoAnexo
import ar.com.telecom.pedido.AprobacionUI
import ar.com.telecom.pedido.especificacionImpacto.Impacto
import ar.com.telecom.util.DateUtil
import ar.com.telecom.util.StringUtil

class WorkflowController extends AbstractController {
	def umeService
	def anexoService
	def pedidoService
	def busquedaService

	def index = {
	}

	def show = {
		//def pedido = llenarDatosPedido(params, false)
		def pedido = llenarDatosPedido(params, true)
		def modelView = [pedidoInstance: pedido]
		modelView.putAll(getMapaDatosPedido(pedido))
		if (pedido) {
			modelView
		} else {
			flash.message = "Pedido no encontrado"
		}
	}

	def completarUsuario = {
		withForm {
			try{
				def legajoUsuarioLogueado  = umeService.getUsuarioLogueado()
				def person
				def personasSeleccionables
				def parametros = ParametrosSistema.list().first()

				def actividad
				def pedido
				def impacto
				
				if (params.pedidoId) {
					pedido = pedidoService.obtenerPedido(params, params.pedidoId)
					params.aprobacionId = pedido?.aprobacionPendiente(pedido?.faseActual, legajoUsuarioLogueado)?.id
				}

				if (params.actividadId && !params.actividadId.equalsIgnoreCase("null")) {
					actividad = pedidoService.getActividad(params.actividadId)
				}

				if (params.username){
					person = umeService.getUsuario(params.username)
				} 

				if (params.impacto && params.impacto.isInteger()) {
					impacto = pedidoService.obtenerPedidoHijo(params, params.impacto)
					def aprobacionPendiente = impacto?.aprobacionPendiente(impacto?.faseActual, legajoUsuarioLogueado)?:impacto?.aprobacionActual(impacto?.faseActual, legajoUsuarioLogueado)
					params.aprobacionId = aprobacionPendiente?.id
				}
				def destino = "/templates/userBox"
				if (params.id){
					switch (params.id){
						case RolAplicacion.GERENTE_USUARIO:
							destino = "/templates/userBoxAjax"
							break
						case RolAplicacion.INTERLOCUTOR_USUARIO:
							//personasSeleccionables =  umeService.getUsuariosGrupoLDAP(parametros.grupoLDAPInterlocutorUsuario, params.gerente)
							personasSeleccionables =  umeService.getUsuariosGrupoLDAP(parametros.grupoLDAPInterlocutorUsuario, params.gerente)
							break
						case RolAplicacion.GESTION_DEMANDA:
							def grupoGestionDemanda = pedido.grupoGestionDemanda ?: ""
							personasSeleccionables = umeService.getUsuariosGrupoLDAP(grupoGestionDemanda, null)
							pedido.legajoUsuarioGestionDemanda = null
							pedidoService.cambiarResponsable(params, new AprobacionUI(pedido: pedido, asignatario: null, usuario: umeService.usuarioLogueado, grupo: grupoGestionDemanda))
							break
						case RolAplicacion.COORDINADOR_CAMBIO:
							personasSeleccionables = umeService.getUsuariosGrupoLDAP(parametros.grupoLDAPCoordinadorPedido, null)
							break
						case RolAplicacion.RESPONSABLE_SISTEMA_IMPACTADO:
							personasSeleccionables = umeService.getUsuariosGrupoLDAP(params.grupoReferente, null)
							break
						case RolAplicacion.RESPONSABLE_HIJO:
							personasSeleccionables = umeService.getUsuariosGrupoLDAP(impacto.grupoResponsable, null)
							impacto.legajoUsuarioResponsable = null
							pedidoService.cambiarResponsable(params, new AprobacionUI(pedido: impacto, asignatario: null, usuario: umeService.usuarioLogueado, grupo: impacto.grupoResponsable))
							break
						case RolAplicacion.RESPONSABLE_ACTIVIDAD:
							personasSeleccionables = umeService.getUsuariosGrupoLDAP(params.grupoResponsable, null)
							if (actividad){
								actividad.legajoUsuarioResponsable = null
								pedidoService.cambiarResponsable(params, new AprobacionUI(pedido: actividad.pedido, asignatario: null, usuario: umeService.usuarioLogueado, grupo: actividad.grupoResponsable))
							}
							break
						case "allUsers":
							destino = "/templates/userBoxAjax"
							break
						case RolAplicacion.APROBADOR_IMPACTO:
							personasSeleccionables = pedidoService.allAprobadoresImpacto(pedido)
							if (person) {
								def aprobadorImpactoAnterior = pedido.legajoUsuarioAprobadorEconomico
								def aprobadorImpactoNuevo = person.legajo
								params.aprobacionId = pedido?.aprobacionPendiente(pedido?.faseActual, aprobadorImpactoAnterior)?.id
								if (params.aprobacionId) {
									pedido.legajoUsuarioAprobadorEconomico = aprobadorImpactoNuevo 
									pedidoService.cambiarResponsable(params, new AprobacionUI(pedido: pedido, asignatario: aprobadorImpactoNuevo, usuario: umeService.usuarioLogueado, grupo: null))
								}
							}
							break
						case RolAplicacion.APROBADOR_FASE:
							def primerGrupoAsignado = umeService.getGrupos(legajoUsuarioLogueado)?.first()
							personasSeleccionables = umeService.getUsuariosGrupoLDAP(primerGrupoAsignado, null)
							break
						default:
							personasSeleccionables = pedidoService.getUsuariosPedido(params.pedidoId)
							break
	//					case "responsable":
	//						personasSeleccionables = umeService.getUsuariosGrupoLDAP(params.grupoResponsable, null)
	//						impacto.legajoUsuarioResponsable = null
	//						pedidoService.cambiarResponsable(params, new AprobacionUI(pedido: impacto, asignatario: null, usuario: umeService.usuarioLogueado, grupo: params.grupoResponsable))
	//						break
					}
				}

				log.info "destino: $destino"
				log.info "valores personasSeleccionables: $personasSeleccionables"

				render (template: destino, model:[person:person?.legajo, box:params.box, personsAutocomplete: personasSeleccionables, boxParalelo:params.boxParalelo, usuarioLogueado: legajoUsuarioLogueado, impacto: impacto, pedidoInstance: pedido, pedido: pedido, cambiaResponsable:true, method: params.method, actividad:actividad, origenActividad:params.origenActividad])
			} catch (Exception e) {
				log.warn "completar usuario: $params"
				procesarErrorAjax(e)
			}
		}.invalidToken { throw new BusinessException("Operaci\u00F3n no v\u00E1lida. Por favor vuelva a intentarlo.") }
	}

	def aprobarPedido = {
		withForm {
			def aprobacionUI
			def result = true
			try {
				aprobacionUI = generarAprobacionUI(true)
				
				if (!params.confirmaCambioDireccion) {
					
					// null  -> no contempla direcciones / 
					// true  -> muestra popup confirmacion
					// false -> muestra error por mal validacion
					Boolean confirma = confirmarCambioDireccion(aprobacionUI.pedido, 'aprobarPedido')
					
					if(confirma!=null){
						if(confirma){
							return
						}else{
							aprobacionUI.pedido.validar(aprobacionUI)
							result = false
						}
					}
				}else{
					aprobacionUI.actualizarDireccion()
					result = validarDireccion(aprobacionUI.pedido)
				}
				
				result = result && pedidoService.avanzarPedido(params, aprobacionUI)
			} catch (ValidationException e) {
				log.error "Aprobar pedido"
				log.error e.message
				result = false
			} catch (Exception e) {
				log.error "Error en Aprobar pedido"
				log.error e.message
				throw e
			}
			mostrarResultados(result, "El pedido fue aprobado (fase actual " + aprobacionUI?.pedido?.faseActual + ")", aprobacionUI, true)
		}.invalidToken { throw new BusinessException("Operaci\u00F3n no v\u00E1lida. Por favor vuelva a intentarlo.") }
	}

	def denegarPedido = {
		withForm {
			def aprobacionUI
			def result = true
			try {
				aprobacionUI = generarAprobacionUI(false)
				
				if (!params.confirmaCambioDireccion) {
					
					// null  -> no contempla direcciones /
					// true  -> muestra popup confirmacion
					// false -> muestra error por mal validacion
					Boolean confirma = confirmarCambioDireccion(aprobacionUI.pedido, 'denegarPedido')
					
					if(confirma!=null){
						if(confirma){
							return
						}else{
							aprobacionUI.pedido.validar(aprobacionUI)
							result = false
						}
					}
				}else{
					aprobacionUI.actualizarDireccion()
					result = validarDireccion(aprobacionUI.pedido)
				}
				
				
				result = result && pedidoService.denegarPedido(params, aprobacionUI)
			} catch (ValidationException e) {
				result = false
			}
			mostrarResultados(result, "El pedido ha sido denegado (fase actual " + aprobacionUI?.pedido?.faseActual + ")", aprobacionUI, true)
		}.invalidToken { throw new BusinessException("Operaci\u00F3n no v\u00E1lida. Por favor vuelva a intentarlo.") }
	}

	def simularValidacion = {
		withForm {
			def aprobacionUI
			def result
			try {
				aprobacionUI = generarAprobacionUI(true)
				result = pedidoService.validarPedido(params, aprobacionUI)
			} catch (ValidationException e) {
				result = false
			}
			mostrarResultados(result, "El pedido est\u00E1 ok", aprobacionUI, false)
		}.invalidToken { throw new BusinessException("Operaci\u00F3n no v\u00E1lida. Por favor vuelva a intentarlo.") }
	}

	def grabarFormulario = {
		log.info "Entro a grabarFormulario ${params}"
		withForm {
			def aprobacionUI
			def result = true
			try {
				aprobacionUI = generarAprobacionUI(true)
				
				if (!params.confirmaCambioDireccion) {
					
					// null  -> no contempla direcciones /
					// true  -> muestra popup confirmacion
					// false -> muestra error por mal validacion
					Boolean confirma = confirmarCambioDireccion(aprobacionUI.pedido, 'grabarFormulario')
					
					if(confirma!=null){
						if(confirma){
							return
						}else{
							aprobacionUI.pedido.validar(aprobacionUI)
							result = false
						}
					}
				}else{
					aprobacionUI.pedido.setearDireccionDe(umeService.getUsuario(aprobacionUI.pedido.legajoGerenteUsuario))
					result = validarDireccion(aprobacionUI.pedido)
				}
				
				result = result && pedidoService.guardarPedido(params, aprobacionUI)
			} catch (ValidationException e) {
				result = false
			}
			mostrarResultados(result, "El pedido fue guardado" , aprobacionUI, false)
		}.invalidToken { throw new BusinessException("Operaci\u00F3n no v\u00E1lida. Por favor vuelva a intentarlo.") }
	}

	def guardarSesion = {
		withForm {
			if (!params._action_guardarSesion.equalsIgnoreCase("show")) {
				def pedido = llenarDatosPedido(params, false)
				//try {
				pedidoService.guardar(params, pedido)
				//} catch (ValidationException e) {}
			}
			redirect(action: "index", controller: params.controllerNombre, params: [pedidoId: params.pedidoId, impacto: params.impacto])
		}.invalidToken { throw new BusinessException("Operaci\u00F3n no v\u00E1lida. Por favor vuelva a intentarlo.") }
	}

	def navegarFase = {
		withForm {
			redirect(action: "index", controller: params.controllerNombre, params: [pedidoId: params.pedidoId, impacto: params.impacto])
		}.invalidToken { throw new BusinessException("Operaci\u00F3n no v\u00E1lida. Por favor vuelva a intentarlo.") }
	}

	def guardarCambio = {
		withForm {
			def pedidoOriginal = pedidoService.obtenerPedidoInicial(params.pedidoId, false)
			def gerente = pedidoOriginal.legajoGerenteUsuario
			def interlocutor = pedidoOriginal.legajoInterlocutorUsuario
			def pedido = llenarDatosPedido(params, false)
			if (pedido.fase.equals(pedido.faseValidacionPedido()) && pedido.legajoGerenteUsuario) {
				if (!gerente?.equalsIgnoreCase(pedido.legajoGerenteUsuario)) {
					pedidoService.cambiarResponsable(params, new AprobacionUI(pedido: pedido, asignatario: pedido.legajoGerenteUsuario))
				} else {
					pedidoService.guardar(params, pedido)
				}
			}
			if (pedido.fase.equals(pedido.faseAprobacionPedido()) && pedido.legajoInterlocutorUsuario) {
				if (!interlocutor?.equalsIgnoreCase(pedido.legajoInterlocutorUsuario)) {
					pedidoService.cambiarResponsable(params, new AprobacionUI(pedido: pedido, asignatario: pedido.legajoInterlocutorUsuario))
				} else {
					pedidoService.guardar(params, pedido)
				}
			}
			redirect(controller: params.controllerNombre, action: "index", params: [pedidoId: params.pedidoId, impacto: params.impacto, vista: "index"])
		}.invalidToken { throw new BusinessException("Operaci\u00F3n no v\u00E1lida. Por favor vuelva a intentarlo.") }
	}


	def verAnexo = {
		def anexo = anexoService.verAnexo(params.anexoId)
		if (anexo) {
			// TODO: Encodearlo
			response.setHeader("Content-Disposition","inline; filename=" + anexo.nombreArchivo + anexo.extension)
			response.setContentType(anexo.fileContentType)
			response.setContentLength(anexo.archivo?.size())
			OutputStream out = response.getOutputStream()
			out.write(anexo.archivo)
			out.close();
		}
	}

	def popupAnexo = {
		log.info "popupAnexo: ${params}"
		withForm {
			try{
				def pedido = llenarDatosPedido(params, true)
				def tiposDeAnexo
				
				// Si viene por el popup de suspension, uso tipos de anexo Suspension/Reanudacion
				if(params.popSuspension){
					tiposDeAnexo = pedido.tipoAnexoPopSuspension
					if(params.motivoSuspension){
						pedido.parent.motivoSuspension = MotivoSuspension.findById(params.motivoSuspension)
					}
					pedidoService.guardar(params, pedido.parent)
				}else{
				 	tiposDeAnexo = TipoAnexo.findAllByFase(pedido.faseActual)
				}
				
				
				log.info "tiposDeAnexo: ${tiposDeAnexo}"
				render (template: "/templates/anexoPorTipo", model:[impacto:params?.impacto, pedidoInstance:pedido, tabSeleccionado:params?.tabSeleccionado, tiposDeAnexo:tiposDeAnexo, anexoFrom: params?.anexoFrom])
			} catch (Exception e) {
				procesarErrorAjax(e)
			}
		}.invalidToken { throw new BusinessException("Operaci\u00F3n no v\u00E1lida. Por favor vuelva a intentarlo.") }
	}

//	def eliminarAnexo = {
//		def pedido = llenarDatosPedido(params, true)
//		anexoService.eliminarAnexo(params, pedido, params.anexoId, pedido)
//		redirect(action: "index", id: pedido.id, params: [pedidoId: pedido.id])
//	}
	
	def eliminarAnexo = {
		log.warn "eliminarAnexo: ${params}"
//		def claseOrigen = params?.origen.toString().substring(6,params?.origen.toString().size())
//		def domainClass = this.class.classLoader.loadClass(claseOrigen)
//		def origen = domainClass.findById(Integer.parseInt(params?.origenId)) //TODO: pasar a service
		
		def pedido = llenarDatosPedido(params, true)
		def origen = pedido.getOrigen(params.anexoId)
		 
		generarAprobacionId(pedido, umeService.usuarioLogueado)
		anexoService.eliminarAnexo(params, pedido, params.anexoId)
		render (template: "/templates/tablaAnexos", model:[fase:params?.fase, 'editable': true, pedidoInstance:pedido, origen:origen, nombreDiv:params?.nombreDiv])
	}

	def guardarAnexo = {
		withForm {
			def pedido = llenarDatosPedido(params, true)
			//pedidoService.guardar(params, pedido)
			def aprobacionUI = generarAprobacionUI(pedido, true)
			pedidoService.guardarPedido(params, aprobacionUI)
			redirect(action: "index", controller: params.controller, params: [pedidoId: pedido.id])
		}.invalidToken { throw new BusinessException("Operaci\u00F3n no v\u00E1lida. Por favor vuelva a intentarlo.") }
	}

	def busquedaAutocomplete = {
		try{
			def result = []
			def methodName = params.methodName
			def word = params.word
			def pedidoId = params?.pedidoId
			result = busquedaService."$methodName"(word,pedidoId)
			jsonOutput(result)
		} catch (Exception e) {
			procesarErrorAjax(e)
		}
	}

	/** 
	 * Definiciones privadas de métodos
	 * No se definen como private para que lo puedan usar las subclases
	 * No deberíamos hacer try/catch en ningún método dado que dejamos que lo manejen los métodos que llaman a éstos 
	 */
	def generarAprobacionUI(boolean aprueba) {
		def pedido = llenarDatosPedido(params, false)
		def result = new AprobacionUI(justificacion: params.justificacionAprobacion, aprueba: aprueba, pedido: pedido, asignatario: umeService.usuarioLogueado, usuario: umeService.usuarioLogueado)
		return result
	}
	
	def generarAprobacionUI(pedido, aprueba) {
		def result = new AprobacionUI(justificacion: params.justificacionAprobacion, aprueba: aprueba, pedido: pedido, asignatario: umeService.usuarioLogueado, usuario: umeService.usuarioLogueado)
		return result
	}

	def mostrarResultados(ok, mensajeOk, aprobacionUI, finaliza) {
		def pedido = aprobacionUI.pedido
		// FED: para evitar que no refresque la versi\u00F3n, cambio a
		//def pedido = pedidoService.obtenerPedidoInicial(aprobacionUI.pedido.id)
		if (ok) {
			flash.message = mensajeOk
		}
		if (ok && finaliza) {
			faseFinalizada(pedido)
		}else{
			// Si hay errores, que vuelva al index
			def modelView = [pedidoInstance: pedido.parent, justificacionAprobacion: params.justificacionAprobacion]
			modelView.putAll(getMapaDatosPedido(pedido))
			render(view: "index", model: modelView)
		}
	}

	def faseFinalizada(pedido) {
		def modelView = [pedidoInstance: pedido]
		modelView.putAll(getMapaDatosPedido(pedido))
		render(view: "show", model: modelView)
	}

	/**
	 * Adapta los datos de un pedido
	 */
	def llenarDatosPedido(params, initial) {
		log.info "llenarDatosPedido - initial: " + initial + " params: " + params
		def usuarioLogueado = umeService.getUsuarioLogueado()

		//parseo la fecha
		if (params.fechaDeseadaImplementacion) {
			params.fechaDeseadaImplementacion = DateUtil.toDate(params.fechaDeseadaImplementacion)
		}

		def pedido = pedidoService.obtenerPedido(params, params.pedidoId)

		if (params.legajoGerenteUsuario) {
			if (!params.legajoGerenteUsuario.equalsIgnoreCase(pedido.legajoGerenteUsuario)) {
				Usuario gerente = umeService.getGerente(params.legajoGerenteUsuario)
				//				params.codigoDireccionGerencia = gerente.codigoEstructura
				//				params.descripcionDireccionGerencia = gerente.direccion
				params.codigoGerencia = gerente.gerenciaCodigo
				params.descripcionGerencia = gerente.gerencia
				params.codigoDireccionGerencia = gerente.direccionCodigo
				params.descripcionDireccionGerencia = gerente.direccion
			}
		}

		if ([pedido.faseRegistracionPedido().controllerNombre, pedido.faseValidacionPedido().controllerNombre].contains(params.controller)) {
			if (!params.legajoInterlocutorUsuario && !initial) {
				pedido.legajoInterlocutorUsuario = null
			}	
		}
		
		// Workaround AMPer(Sino al navegar fase, cambio la dirección del pedido)
		def codigoDireccionGerenciaOld = pedido.codigoDireccionGerencia
		def descripcionDireccionGerenciaOld = pedido.descripcionDireccionGerencia
		
		pedido.properties = params

		pedido.codigoDireccionGerencia = codigoDireccionGerenciaOld
		pedido.descripcionDireccionGerencia = descripcionDireccionGerenciaOld
		//
		def anexo
		if (params.anexoArchivo?.size > 0){
			anexo = getAnexo(pedido, usuarioLogueado, params)
		}
		
		if(!pedido.faseActual){
			pedido.faseActual = FaseMultiton.getFase(FaseMultiton.REGISTRACION_PEDIDO)
		}
		if (anexo){
			if (params.idActividad){
				//def pedidoHijo = PedidoHijo.findById(params.pedidoHijoID)
				def actividad = Actividad.findById(params.idActividad)
				if (actividad)
					actividad.addToAnexos(anexo)
			}else{
				pedido.addToAnexos(anexo)
			}
		}

		return pedido
	}

	def getUmeService() {
		return umeService
	}

	def getPedidoService() {
		return pedidoService
	}

	def getImpactosDefault(pedido) {
		def impactos = []
		impactos.addAll(new Impacto(Impacto.IMPACTO_CONSOLIDADO, this))
		impactos.addAll(pedido.pedidosHijos.collect { pedidoHijo -> new Impacto(Impacto.IMPACTO_HIJO, pedidoHijo) })
		return impactos
	}

	def getPedidoATrabajar(params) {
		def trabajoSobreHijo = Impacto.aplicaAHijo(params.impacto)
		if (trabajoSobreHijo) {
			log.info "Impacto: " + params.impacto
			return pedidoService.obtenerPedidoHijo(params, params.impacto)
		} else {
			return pedidoService.obtenerPedido(params, params.pedidoId)
		}
	}

	def getAnexo(pedido, usuarioLogueado, params) {
		String fileName = params?.anexoArchivo?.fileItem?.fileName
		String extension = fileName.substring(fileName.lastIndexOf("."))
		fileName = fileName.substring(fileName.lastIndexOf("\\") + 1, fileName.lastIndexOf("."))
		fileName = StringUtil.hasta(fileName, 197)
		fileName = anexoService.getFileName(fileName, extension, pedido, params.controller)
		String contentType = params?.anexoArchivo?.fileItem?.contentType
		boolean global = false
		if (params.global){
			global = params?.global.toBoolean()
		}
		return new AnexoPorTipo(legajo: usuarioLogueado, fecha: new Date(), versionAnexo: 0, nombreArchivo: fileName, archivo: params.anexoArchivo, fileContentType: contentType, extension: extension, tipoAnexo: TipoAnexo.get(params.tipoAnexo.id), descripcion: params.descripcionAnexo, global:global)
	}

	def obtenerGerentesAjax = {
		log.warn "obtenerGerentesAjax: ${params}"
		def sResult = "<ul class=\"ui-autocomplete ui-menu ui-widget ui-widget-content ui-corner-all\">"
		try{
			def result = []

			def filtro = params.filtro
			result = umeService.getGerentesUsuarios("gerentesByFiltro",filtro)

			result.each { u ->
				sResult += u.toHTML()
			}
		} catch (Exception e) {
			procesarErrorAjax(e)
		}

		sResult+= "</ul>"
		render sResult
	}

	def obtenerUsuariosAjax = {
		log.warn "obtenerUsuariosAjax: ${params}"
		def sResult = "<ul class=\"ui-autocomplete ui-menu ui-widget ui-widget-content ui-corner-all\">"
		try{
			def result = []

			def filtro = params.filtro
			result = umeService.getGerentesUsuarios("usuariosByFiltro",filtro)

			result.each { u ->
				sResult += u.toHTML()
			}
		} catch (Exception e) {
			procesarErrorAjax(e)
		}

		sResult+= "</ul>"
		render sResult
	}

	
	def getImpacto(pedido) {
		if (params.impacto && pedido) {
			return [ impactoInstance: new Impacto(params.impacto, pedido) ]
		} else {
			return []
		}
	}
	
	private def getPedidoInicial(params) {
		def trabajoSobreHijo = Impacto.aplicaAHijo(params.impacto)
		if (trabajoSobreHijo) {
			return pedidoService.obtenerPedidoInicial(params.impacto, false)
		} else {
			return pedidoService.obtenerPedidoInicial(params.pedidoId, false)
		}
	}

	// workaround FED
	def generarAprobacionId(pedido, usuario) {
		if (!params.aprobacionId) {
			def aprobacion = pedido?.aprobacionPendiente(pedido?.faseActual, usuario)
			params.aprobacionId = aprobacion?.id
		}
	}
	//

	def confirmarCambioDireccion(pedido, actionFrom){
		return confirmarCambioDireccion(pedido, actionFrom, false)
	}
	
	def confirmarCambioDireccion(pedido, actionFrom, esAprobacionPAU){
		def direccionPedido = pedido.codigoDireccionGerencia 
		def direccionGU = umeService.getUsuario(pedido.legajoGerenteUsuario)?.direccionCodigo
		def direccionIU = umeService.getUsuario(pedido.legajoInterlocutorUsuario)?.direccionCodigo
		def direccionAI = umeService.getUsuario(pedido.legajoUsuarioAprobadorEconomico)?.direccionCodigo
		
		def errorGUIUDiferentes = "El GU y el IU deben pertencer a la misma dirección, reasigne a quien corresponda"
		
		if(pedido.estaEnFase(FaseMultiton.VALIDACION_PEDIDO)){
			//if(direccionGU.equalsIgnoreCase(direccionPedido) && !direccionGU.equalsIgnoreCase(direccionIU)){
			if(!direccionGU.equalsIgnoreCase(direccionIU)){
				return errorEnPedido(pedido, errorGUIUDiferentes)
			}
			if(!direccionPedido.equalsIgnoreCase(direccionGU)){
				return popupConfirmacion(pedido, actionFrom)
			}
		}
		
		if(pedido.estaEnFase(FaseMultiton.APROBACION_PEDIDO)){
			//if(direccionIU.equalsIgnoreCase(direccionPedido) && !direccionIU.equalsIgnoreCase(direccionGU)){
			if(!direccionIU.equalsIgnoreCase(direccionGU)){
				return errorEnPedido(pedido, errorGUIUDiferentes)
			} 
			if(direccionGU.equalsIgnoreCase(direccionIU) && !direccionGU.equalsIgnoreCase(direccionPedido)){
				return popupConfirmacion(pedido, actionFrom)
			}
		}
		
		if(pedido.estaEnFase(FaseMultiton.APROBACION_CAMBIO)){
			if(direccionIU.equalsIgnoreCase(direccionGU) && !direccionIU.equalsIgnoreCase(direccionPedido)){
				return popupConfirmacion(pedido, actionFrom)
			}
			if(direccionIU.equalsIgnoreCase(direccionPedido) && !direccionIU.equalsIgnoreCase(direccionGU)){
				return errorEnPedido(pedido, errorGUIUDiferentes)
			}
			if(!direccionIU.equalsIgnoreCase(direccionGU) && !direccionIU.equalsIgnoreCase(direccionPedido)){
				return errorEnPedido(pedido, errorGUIUDiferentes)
			}
		}

		if(pedido.estaEnFase(FaseMultiton.VALIDACION_IMPACTO) && !pedido.pedidoDeEmergencia()){
			if(direccionGU.equalsIgnoreCase(direccionAI) && direccionGU.equalsIgnoreCase(direccionIU) && !direccionGU.equalsIgnoreCase(direccionPedido)){
				return popupConfirmacion(pedido, actionFrom)
			}
			if(direccionGU.equalsIgnoreCase(direccionIU) && direccionGU.equalsIgnoreCase(direccionPedido) && !direccionGU.equalsIgnoreCase(direccionAI)){
				return errorEnPedido(pedido, "El AI debe pertenecer a la dirección del GU e IU. Por favor reasignar.")
			}
			if(!(direccionGU.equalsIgnoreCase(direccionIU) && direccionGU.equalsIgnoreCase(direccionAI) && direccionGU.equalsIgnoreCase(direccionPedido))){
				return errorEnPedido(pedido, "El GU, IU y AI, deben pertencer a la misma dirección. Por favor reasigne según corresponda")
			}
		}
		
		if(pedido.estaEnFase(FaseMultiton.APROBACION_IMPACTO) && !pedido.pedidoDeEmergencia()){
			if(direccionGU.equalsIgnoreCase(direccionAI) && direccionGU.equalsIgnoreCase(direccionIU) && !direccionGU.equalsIgnoreCase(direccionPedido)){
				return popupConfirmacion(pedido, actionFrom)
			}
			if(!(direccionGU.equalsIgnoreCase(direccionIU) && direccionGU.equalsIgnoreCase(direccionAI) && direccionGU.equalsIgnoreCase(direccionPedido))){
				return errorEnPedido(pedido, "El GU, IU y AI, deben pertencer a la misma dirección. Por favor reasigne según corresponda")
			}
		}
		
		if(esAprobacionPAU){
			if(direccionGU.equalsIgnoreCase(direccionAI) && direccionGU.equalsIgnoreCase(direccionIU) && !direccionGU.equalsIgnoreCase(direccionPedido)){
				return popupConfirmacion(pedido, actionFrom)
			}
			if(!(direccionGU.equalsIgnoreCase(direccionIU) && direccionGU.equalsIgnoreCase(direccionAI) && direccionGU.equalsIgnoreCase(direccionPedido))){
				return errorEnPedido(pedido, "El GU, IU y AI, deben pertencer a la misma dirección. Por favor reasigne según corresponda")
			}
		}
		
		return null
	}

	public popupConfirmacion(pedido, actionFrom) {
		def modelView = [pedidoInstance: pedido.parent, justificacionAprobacion: params.justificacionAprobacion, muestraPopDireccion:true, actionFrom: actionFrom]
		modelView.putAll(getMapaDatosPedido(pedido))

		render(view: "index", model: modelView)
		return new Boolean(true)
	}
	
	private errorEnPedido(pedido, error){
		pedido.errors.reject(error)
		return new Boolean(false)
	}

	boolean validarDireccion(pedido){
		def direccionGU = umeService.getUsuario(pedido.legajoGerenteUsuario).direccionCodigo
		def direccionIU = umeService.getUsuario(pedido.legajoInterlocutorUsuario).direccionCodigo
		
		if(pedido.estaEnFase(FaseMultiton.VALIDACION_PEDIDO)){
			if(!direccionGU.equalsIgnoreCase(direccionIU)){
				pedido.errors.reject("El GU y el IU deben pertencer a la misma dirección, reasigne a quien corresponda")
				return false
			}
		}
		
		return true
	}
		
}
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                          