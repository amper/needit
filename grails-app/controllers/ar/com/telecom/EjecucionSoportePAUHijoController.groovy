package ar.com.telecom

import ar.com.telecom.pcs.entities.TipoAnexo
import ar.com.telecom.pedido.especificacionImpacto.Impacto

class EjecucionSoportePAUHijoController extends ActividadController{

	private def getMapaDatosPedido(pedido) {
		def usuarioLogueado = umeService.getUsuarioLogueado()
		def pedidoHijo
		if (Impacto.aplicaAHijo(params.impacto)){
			pedidoHijo = pedidoService.obtenerPedidoHijo(params, params.impacto)
		}
//		def impactoInstance	
//		if (params.impacto?.isInteger()) {
//			pedidoHijo = PedidoHijo.findById(Integer.parseInt(params.impacto))
//			impactoInstance = new Impacto(Impacto.IMPACTO_HIJO, pedidoHijo)
//		}
		def impactoInstance = getImpactoInstance()
		
		[usuarioLogueado:usuarioLogueado, pedidoHijo: pedidoHijo, faseATrabajar: FaseMultiton.EJECUCION_SOPORTE_PAU_HIJO, impactoInstance:impactoInstance]
	}
	
	def editarActividadAnexo = {
		//def pedidoHijo = PedidoHijo.findById(params.impacto)
		def pedidoHijo = pedidoService.obtenerPedidoHijo(params, params.impacto)
		def actividad = this.getActividad(pedidoHijo, params.idActividad)
		
		def tiposDeAnexo = TipoAnexo.findAllByFaseInList([
			FaseMultiton.getFase(FaseMultiton.EJECUCION_SOPORTE_PAU_HIJO)
			])
		
		def usuarioLogueado = umeService.getUsuarioLogueado()
		
		render (template: "/templates/anexoPorTipoActividad", model:[pedidoHijo: pedidoHijo, actividad: actividad, tiposDeAnexo:tiposDeAnexo, usuarioLogueado:usuarioLogueado])
	}

}
