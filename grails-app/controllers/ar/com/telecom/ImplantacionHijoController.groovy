package ar.com.telecom

import ar.com.telecom.exceptions.BusinessException
import ar.com.telecom.pcs.entities.ParametrosSistema
import ar.com.telecom.pcs.entities.TipoAnexo
import ar.com.telecom.pedido.AprobacionUI
import ar.com.telecom.pedido.especificacionImpacto.Impacto


class ImplantacionHijoController extends ActividadController{

	private def getMapaDatosPedido(pedido) {
		def usuarioLogueado = umeService.getUsuarioLogueado()
		def pedidoHijo
		if (Impacto.aplicaAHijo(params.impacto)){
			pedidoHijo = pedidoService.obtenerPedidoHijo(params, params.impacto)
		}
		
		[usuarioLogueado:usuarioLogueado, pedidoHijo: pedidoHijo, faseATrabajar: FaseMultiton.IMPLANTACION_HIJO, impactoInstance: getImpactoInstance()]
	}
	
	def editarActividadAnexo = {
		def pedidoHijo = pedidoService.obtenerPedidoHijo(params, params.impacto)//PedidoHijo.findById(params.idPedidoHijo)
		def actividad =  this.getActividad(pedidoHijo, params.idActividad)
		
		def tiposDeAnexo = TipoAnexo.findAllByFaseInList([
			FaseMultiton.getFase(FaseMultiton.IMPLANTACION_HIJO)
		])
		
		def usuarioLogueado = umeService.getUsuarioLogueado()
		
		render (template: "/templates/anexoPorTipoActividad", model:[pedidoHijo: pedidoHijo, actividad: actividad, tiposDeAnexo:tiposDeAnexo, usuarioLogueado:usuarioLogueado])
	}

}
