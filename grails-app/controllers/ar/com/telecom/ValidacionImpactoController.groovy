package ar.com.telecom

import ar.com.telecom.exceptions.BusinessException
import ar.com.telecom.pcs.entities.ActividadPlanificacion
import ar.com.telecom.pedido.especificacionImpacto.Impacto

class ValidacionImpactoController extends AbstractImpactoController {

	def doGetMapaDatosPedido(pedido) {
		println "pedidoService.aprobadorImpactoDefault(pedido): " + pedidoService.aprobadorImpactoDefault(pedido)
		[aprobadorDefault: pedidoService.aprobadorImpactoDefault(pedido)?.legajo,
		 aprobacionesList: pedido?.aprobaciones.findAll { aprobacion -> aprobacion.posteriorAFechaYFase(pedido.fechaAprobacionEspecificacionImpacto, pedido.faseValidacionImpacto()) }
		]
	}

	def recargaBotonera = {
		withForm {
			try{
				def pedido = llenarDatosPedido(params, true)
				render (template: "botonera", model: [pedidoInstance:pedido] + getMapaDatosPedido(pedido))
			} catch (Exception e) {
				procesarErrorAjax(e)
			}
		}.invalidToken { throw new BusinessException("Operaci\u00F3n no v\u00E1lida. Por favor vuelva a intentarlo.") }
	}
}
