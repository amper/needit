package ar.com.telecom

class ActualizarUsuariosDireccionController {

	def umeService
	
	def index = {
		render obtenerUsuariosDireccion()
	}
	
	def list = {
		render(view:"list", model:[lista:obtenerUsuariosDireccion()])
	}
	
	def getDireccion = {
		verificarUsuariosDireccion()
		if (params.usuario) {
			render UsuariosDireccion.instance.getDireccion(params.usuario)
		} else {
			render ""
		}
	}
	
	def getGerentes = {
		verificarUsuariosDireccion()
		if (params.direccion) {
			render UsuariosDireccion.instance.getGerentes(params.direccion)
		} else {
			render ""
		}
	}

	def obtenerUsuariosDireccion() {
		verificarUsuariosDireccion()
		return UsuariosDireccion.instance.usuariosDireccion
	}
	
	def verificarUsuariosDireccion(){
		if (UsuariosDireccion.instance.usuariosDireccion.isEmpty()) {
			umeService.actualizarUsuariosDireccion()
		}
	}
}
