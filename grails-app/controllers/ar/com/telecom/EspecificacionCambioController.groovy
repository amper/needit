package ar.com.telecom
import grails.validation.ValidationException
import ar.com.telecom.exceptions.BusinessException
import ar.com.telecom.pcs.entities.AreaSoporte
import ar.com.telecom.pcs.entities.ECS
import ar.com.telecom.pcs.entities.ECSOtrasPruebas
import ar.com.telecom.pcs.entities.Justificacion
import ar.com.telecom.pcs.entities.Sistema
import ar.com.telecom.pcs.entities.SistemaImpactado
import ar.com.telecom.pcs.entities.TipoAnexo
import ar.com.telecom.pcs.entities.TipoImpacto
import ar.com.telecom.pcs.entities.TipoPrueba
import ar.com.telecom.util.CheckUtil

class EspecificacionCambioController extends WorkflowController {

	def webUserAgentService

	def index = {
		if (webUserAgentService.isBlackberry()) {
			redirect(controller:"inboxMobile", action: "index")
		} else {
			log.info "Llamo a llenar Datos Pedido desde el index"
			def pedido = llenarDatosPedido(params, true)

			def usuarioLogueado = getUmeService().getUsuarioLogueado()

			if(!pedido?.puedeEditarEspecificacionCambio(usuarioLogueado)){
				redirect(action: "show", params: params)
			}

			def result = [pedidoInstance: pedido]
			result.putAll(getMapaDatosPedido())
			result
		}
	}

	def actualizarTablaOtraPrueba = {
		withForm {
			try{
				def pedido = pedidoService.obtenerPedido(params, params.pedidoId)
				def ultimoECS = pedido.obtenerUltimaEspecificacion()
				def pruebasECS = ultimoECS.otrasPruebasECS

				render (template:"/templates/tablaPruebasAdicionales", model:[pruebasOpcionalesSeleccionadas:ultimoECS.otrasPruebasECS, pedidoInstance: pedido, edita: true])
			} catch (Exception e) {
				procesarErrorAjax(e)
			}
		}.invalidToken { throw new BusinessException("Operaci\u00F3n no v\u00E1lida. Por favor vuelva a intentarlo.") }
	}

	def agregarOtraPrueba = {
		withForm {
			try{
				def pedido = pedidoService.obtenerPedido(params, params.pedidoId)
				def ultimoECS = pedido.obtenerUltimaEspecificacion()
				def pruebasECS = ultimoECS.otrasPruebasECS

				if(params.comboOtrasPruebas){
					TipoPrueba tipoPrueba = TipoPrueba.findById(Integer.parseInt(params.comboOtrasPruebas.id))
					boolean consolida = (params.consolida)

					def ecsOtrasPruebas = new ECSOtrasPruebas(tipoPrueba:tipoPrueba, seraConsolidada:consolida)
					if(!pruebasECS.contains(ecsOtrasPruebas)) {
						ultimoECS.agregarOtraPruebaECS(ecsOtrasPruebas)
						flash.message = "Se agreg\u00F3 la prueba adicional"
					} else {
						def index = pruebasECS.indexOf(ecsOtrasPruebas)
						pruebasECS.putAt(index, ecsOtrasPruebas)
						flash.message = "Se modific\u00F3 la configuraci\u00F3n de la prueba adicional"
					}
				}
				pedidoService.guardar(params, pedido)
				def otrasPruebas = getOtrasPruebas()
				render (template:"popUpPruebaOpcional", model:[pedidoInstance:pedido, otrasPruebas:otrasPruebas])
				//render (template:"/templates/tablaPruebasAdicionales", model:[pruebasOpcionalesSeleccionadas:ultimoECS.otrasPruebasECS, pedidoInstance: pedido])
			} catch (Exception e) {
				procesarErrorAjax(e)
			}
		}.invalidToken { throw new BusinessException("Operaci\u00F3n no v\u00E1lida. Por favor vuelva a intentarlo.") }
	}

	def eliminarPrueba = {
		withForm {
			try{
				def pedido = pedidoService.obtenerPedido(params, params.pedidoId)
				def ecs = pedido.obtenerUltimaEspecificacion()
				if (params.pruebaId) {
					def otraPrueba = ECSOtrasPruebas.findById(params.pruebaId)
					pedidoService.eliminarOtraPruebaECS(ecs, otraPrueba)
				}
				render (template:"/templates/tablaPruebasAdicionales", model:[pruebasOpcionalesSeleccionadas: ecs.otrasPruebasECS, pedidoInstance: pedido, edita:true])
			} catch (Exception e) {
				procesarErrorAjax(e)
			}
		}.invalidToken { throw new BusinessException("Operaci\u00F3n no v\u00E1lida. Por favor vuelva a intentarlo.") }
	}

	def agregarNuevaAreaSoporte = {
		withForm {
			try{
				def pedido = pedidoService.obtenerPedido(params, params.pedidoId)
				def areas = pedido.obtenerUltimaEspecificacion().areasImpactadas

				if(params.comboAreasSoporte){
					AreaSoporte areaSoporte = AreaSoporte.findById(params.comboAreasSoporte.id)
					if(!areas.contains(areaSoporte)) {
						areas.add(areaSoporte)
						pedidoService.guardar(params, pedido)
						flash.message = "Se agreg\u00F3 el \u00E1rea de soporte"
					} else {
						pedido.errors.reject("El \u00E1rea de soporte ya fue ingresada al pedido")
					}
				}
				render (template:"areaSoportePopUp", model:[pedidoInstance:pedido])
			} catch (Exception e) {
				e.printStackTrace()
				procesarErrorAjax(e)
			}
		}.invalidToken { throw new BusinessException("Operaci\u00F3n no v\u00E1lida. Por favor vuelva a intentarlo.") }
	}

	def popUpPruebaOpcional = {
		withForm {
			try{
				def pedido = pedidoService.obtenerPedido(params, params.pedidoId)
				def otrasPruebas = getOtrasPruebas()
				render (template:"popUpPruebaOpcional", model:[pedidoInstance:pedido, otrasPruebas:otrasPruebas])
			} catch (Exception e) {
				procesarErrorAjax(e)
			}
		}.invalidToken { throw new BusinessException("Operaci\u00F3n no v\u00E1lida. Por favor vuelva a intentarlo.") }
	}

	def getOtrasPruebas(){
		return TipoPrueba.createCriteria().list {
			createAlias ('fases', 'fase')
			eq("fase.id", FaseMultiton.getFase(FaseMultiton.ESPECIFICACION_CAMBIO).id)
			eq("opcional", true)
		}
	}

	def recargaAreasSoporte = {
		withForm {
			try{
				def pedido = pedidoService.obtenerPedido(params, params.pedidoId)
				def areas = pedido.obtenerUltimaEspecificacion().areasImpactadas
				render (template:"/templates/tablaAreasSoporte", model:[areasSoporteSeleccionadas:areas, pedidoInstance: pedido, visible: true])
			} catch (Exception e) {
				procesarErrorAjax(e)
			}
		}.invalidToken { throw new BusinessException("Operaci\u00F3n no v\u00E1lida. Por favor vuelva a intentarlo.") }
	}

	def recargaSistemasImpactados = {
		withForm {
			try{
				def pedido = pedidoService.obtenerPedido(params, params.pedidoId)
				def sistemasImpactados = pedido.obtenerUltimaEspecificacion().sistemasImpactados
				render (template:"/templates/tablaSistemasImpactados", model:[sistemasImpactadosSeleccionados:sistemasImpactados, pedidoInstance: pedido, visible: true])
			} catch (Exception e) {
				procesarErrorAjax(e)
			}
		}.invalidToken { throw new BusinessException("Operaci\u00F3n no v\u00E1lida. Por favor vuelva a intentarlo.") }
	}

	def eliminarArea = {
		withForm {
			try{
				def pedido = pedidoService.obtenerPedido(params, params.pedidoId)
				def ecs = pedido.obtenerUltimaEspecificacion()
				if (params.areaId){
					def areaSoporte = AreaSoporte.findById(params.areaId)
					pedidoService.eliminarAreaSoporteECS(ecs, areaSoporte)
				}
				render (template:"/templates/tablaAreasSoporte", model:[areasSoporteSeleccionadas: ecs.areasImpactadas, pedidoInstance: pedido, visible: true])
			} catch (Exception e) {
				procesarErrorAjax(e)
			}
		}.invalidToken { throw new BusinessException("Operaci\u00F3n no v\u00E1lida. Por favor vuelva a intentarlo.") }
	}


	def agregarNuevoSistemaImpactado = {
		withForm{
			try{
				def pedido = pedidoService.obtenerPedido(params, params.pedidoId)
				def ecs = pedido.obtenerUltimaEspecificacion()
				def tipoDeImpacto = TipoImpacto.findById(Integer.parseInt(params.tipoImpacto.id))

				def sistemaImpactado = new SistemaImpactado(sistema:Sistema.findById(Integer.parseInt(params.sistemaImpacto.id)),
						tipoImpacto: tipoDeImpacto,
						grupoReferente: params.grupoReferenteLDAP,
						usuarioResponsable: params.legajoUsuarioReferente)

				ecs.addToSistemasImpactados(sistemaImpactado)

				if(sistemaImpactado.validar(pedido)){
					pedidoService.guardar(params, pedido)
					flash.message = "Se agreg\u00F3 el sistema impactado"
				}else{
					ecs.removeFromSistemasImpactados(sistemaImpactado)
				}
				def tiposImpactoSistemas = TipoImpacto.findAllBySistemaImpactado(true)
				render (template:"sistemasImpactadosPopUp", model:[pedidoInstance: pedido, tiposImpactoSistemas:tiposImpactoSistemas])
			} catch (Exception e) {
				procesarErrorAjax(e)
			}
		}.invalidToken { throw new BusinessException("Operaci\u00F3n no v\u00E1lida. Por favor vuelva a intentarlo.") }
	}

	def popupSistemas = {
		withForm {
			try{
				def pedido = pedidoService.obtenerPedido(params, params.pedidoId)
				def tiposImpactoSistemas = TipoImpacto.findAllBySistemaImpactado(true)

				render (template:"sistemasImpactadosPopUp", model:[pedidoInstance: pedido, tiposImpactoSistemas:tiposImpactoSistemas, params:params])
			} catch (Exception e) {
				procesarErrorAjax(e)
			}
		}.invalidToken { throw new BusinessException("Operaci\u00F3n no v\u00E1lida. Por favor vuelva a intentarlo.") }
	}

	def popupAreaSoporte = {
		withForm {
			try{
				//				def cancela
				//				cancela.concat("")
				def pedido = pedidoService.obtenerPedido(params, params.pedidoId)
				render (template:"areaSoportePopUp", model:[pedidoInstance:pedido])
			} catch (Exception e) {
				procesarErrorAjax(e)
			}
		}.invalidToken { throw new BusinessException("Operaci\u00F3n no v\u00E1lida. Por favor vuelva a intentarlo.") }
	}

	def eliminarSistema = {
		withForm {
			try{
				def pedido = pedidoService.obtenerPedido(params, params.pedidoId)
				def ecs = pedido.obtenerUltimaEspecificacion()
				if(params.sistemaId){
					def sistema = SistemaImpactado.findById(params.sistemaId)
					pedidoService.eliminarSistemaECS(pedido, ecs, sistema)
				}
				render (template:"/templates/tablaSistemasImpactados", model:[sistemasImpactadosSeleccionados:ecs?.sistemasImpactados, pedidoInstance: pedido, visible: true, params:params])
			} catch (Exception e) {
				procesarErrorAjax(e)
			}
		}.invalidToken { throw new BusinessException("Operaci\u00F3n no v\u00E1lida. Por favor vuelva a intentarlo.") }
	}

	def cambiaReferente = {
		withForm {
			try{
				def grupoLDAP = pedidoService.obtenerGrupoReferente(params.sistemaImpacto, params.tipoImpacto)
				render (template: "grupoSistema", model:[grupo:grupoLDAP])
			} catch (Exception e) {
				procesarErrorAjax(e)
			}
		}.invalidToken { throw new BusinessException("Operaci\u00F3n no v\u00E1lida. Por favor vuelva a intentarlo.") }
	}

//	def eliminarAnexo = {
//		withForm {
//			try{
//				def pedido = pedidoService.obtenerPedido(params, params.pedidoId)
//				def ecs = pedido.obtenerUltimaEspecificacion()
//				anexoService.eliminarAnexo(params, pedido, params.anexoId, ecs)
//				render(template:"anexoTabla", model:[listAnexos:ecs?.anexosPorTipo, pedidoInstance: pedido])
//			} catch (Exception e) {
//				procesarErrorAjax(e)
//			}
//		}.invalidToken { throw new BusinessException("Operaci\u00F3n no v\u00E1lida. Por favor vuelva a intentarlo.") }
//	}

	def modificaFilaOtraPrueba = {
		withForm {
			try{
				def pedido = pedidoService.obtenerPedido(params, params.pedidoId)
				def ultimoECS = pedido.obtenerUltimaEspecificacion()
				def pruebasECS = ultimoECS.otrasPruebasECS

				ECSOtrasPruebas otraPrueba = pruebasECS.getAt(Integer.parseInt(params.pos))
				otraPrueba.seraConsolidada = (params.consolida.equals("true"))?true:false

				pedidoService.guardar(params, pedido)

				render (template:"filaOtrasPruebas", model:[prueba:otraPrueba, pedidoInstance: pedido, counter:params.pos])
			} catch (Exception e) {
				procesarErrorAjax(e)
			}
		}.invalidToken { throw new BusinessException("Operaci\u00F3n no v\u00E1lida. Por favor vuelva a intentarlo.") }
	}

	def actualizaEstrategia = {
		withForm {
			try{
				def pedido = pedidoService.obtenerPedido(params, params.pedidoId)
				def mapaResult = actualizarEstrategia(pedido, params.tipoGestion)
				def tipoGestion = !params.tipoGestion.equalsIgnoreCase("null") && params.tipoGestion.equalsIgnoreCase("1")
				render (template: "estrategiasPruebas", model:[tipoGestion: tipoGestion] + mapaResult)
			} catch (Exception e) {
				procesarErrorAjax(e)
			}
		}.invalidToken { throw new BusinessException("Operaci\u00F3n no v\u00E1lida. Por favor vuelva a intentarlo.") }
	}

	private def getMapaDatosPedido(pedido) {
		def usuarioLogueado = getUmeService().getUsuarioLogueado()
		def tiposDeAnexo = TipoAnexo.findAllByFase(FaseMultiton.getFase(FaseMultiton.ESPECIFICACION_CAMBIO))
		def tiposImpactoSistemas = TipoImpacto.findAllBySistemaImpactado(true)

		[
			usuarioLogueado: usuarioLogueado,
			tiposDeAnexo:tiposDeAnexo,
			tiposImpactoSistemas: tiposImpactoSistemas
		]
	}

	def completarGrupo = { render params }

	/**
	 * Adapta los datos de un pedido
	 */
	def llenarDatosPedido(params, initial) {
		log.info "llenarDatosPedido - initial: " + initial + " params: " + params
		def usuarioLogueado = umeService.usuarioLogueado

		def pedido = pedidoService.obtenerPedido(params, params.pedidoId)

		pedido.properties = params

		def anexo
		if (params.anexoArchivo?.size > 0){
			anexo = getAnexo(pedido, usuarioLogueado, params)
		}

		ECS ecs = pedido.obtenerUltimaEspecificacion()
		boolean estrategiaDefault = !anexo && pedido.tipoGestion && initial
		boolean pisarEstrategiaConValoresCargados = !anexo && !initial
		log.info "Estrategia default: " + estrategiaDefault
		log.info "pisarEstrategiaConValoresCargados: " + pisarEstrategiaConValoresCargados
		if (estrategiaDefault) {
			actualizarEstrategia(pedido, params.tipoGestion?.id)
		}
		if (pisarEstrategiaConValoresCargados) {
			params.realizaPI = CheckUtil.getValue(params, "realizaPI")
			params.realizaPAU = CheckUtil.getValue(params, "realizaPAU")
			log.info "Realiza PAU 1:" + params.realizaPAU
			log.info "Realiza PAU 2:" + params.realizaPAU
			bindData(ecs, params)
			ecs.actualizarEstrategiaPrueba(pedido)
			log.info "ECS Realiza PAU 4 :" + ecs.realizaPAU
		}
		if (anexo){
			ecs.addToAnexosPorTipo(anexo)
		}

		return pedido
	}

	def actualizarEstrategia(pedido, tipoGestionId) {
		def ecs = pedido.obtenerUltimaEspecificacion()
		def parametrizacionEstrategias
		if (tipoGestionId && !tipoGestionId.equals("null")){
			def idTipoGestion = tipoGestionId as int
			parametrizacionEstrategias = pedidoService.obtenerParametrizacionTipoPrueba(tipoGestionId, null)
			def tipoGestionDelPedido = pedido.tipoGestion?.id
			boolean noExistiaTipoGestion = tipoGestionDelPedido == null && (idTipoGestion > 0)
			boolean cambioTipoGestion = tipoGestionDelPedido != idTipoGestion
			if (noExistiaTipoGestion || cambioTipoGestion) {
				def parametrizacionPI = parametrizacionEstrategias.find { it.tipoPrueba.descripcion.equalsIgnoreCase(TipoPrueba.DESCRIPCION_PI)}
				def parametrizacionPAU = parametrizacionEstrategias.find { it.tipoPrueba.descripcion.equalsIgnoreCase(TipoPrueba.DESCRIPCION_PAU)}
				ecs.initialize(pedido, parametrizacionPI, parametrizacionPAU)
			}
		}
		[ecs: ecs, parametrizacionEstrategias: parametrizacionEstrategias]
	}

	def simularValidacion = {
		withForm {
			def aprobacionUI
			def result
			try {
				log.info "Simular"
				aprobacionUI = generarAprobacionUI(true)
				result = pedidoService.validarPedido(params, aprobacionUI)
				log.info "result" + result
			} catch (ValidationException e) {
				result = false
			}
			mostrarResultados(result, "El pedido est\u00E1 ok", aprobacionUI, false)
		}.invalidToken { throw new BusinessException("Operaci\u00F3n no v\u00E1lida. Por favor vuelva a intentarlo.") }
	}
}