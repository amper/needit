package ar.com.telecom

import org.codehaus.groovy.grails.commons.GrailsClassUtils;

class AdministracionController {

	static adminMenu = true

    def index = {
		def oListaControllers = []

		grailsApplication.controllerClasses.each { cc ->
			if (GrailsClassUtils.getStaticPropertyValue(cc.clazz, 'adminMenu')) {
				oListaControllers << cc
			}
		}

		[listaControllers: oListaControllers.sort({it.logicalPropertyName})]
	}
}
