package ar.com.telecom.mobile

import ar.com.telecom.WorkflowController
import ar.com.telecom.pcs.entities.Aprobacion
import ar.com.telecom.pedido.AprobacionUI
import ar.com.telecom.pedido.consulta.Consulta

class InboxMobileController extends WorkflowController {

	static adminMenu = false

	def umeService
	def pedidoService

	def index = {
		params.max = 7
		def oUsuarioLogueado = umeService.getUsuarioLogueado()
		def oListaPedidos = obtenerPedidos(oUsuarioLogueado)
		def oListaAprobaciones = pedidoService.inboxAprobaciones(oListaPedidos, params)
		[aprobacionLista: oListaAprobaciones.aprobacionInstanceList, aprobacionTotal: params.totalCount, aprobacionUsuario: oUsuarioLogueado]
	}

	def obtenerPedidos(usuarioLogueado) {
		params.tipoConsulta = Consulta.INBOX_APROBACIONES
		pedidoService.consultarInboxAprobaciones(params, usuarioLogueado)
	}

	def acciones = {
		if (params.id) {
			def oAprobacion = Aprobacion.findById(params.id)
			[aprobacion: oAprobacion]
		}
	}

	def aprobarPedido = {
		params.pedidoId = params.pedidoId.toLong()
		def pedido = pedidoService.obtenerPedidoGenerico(params, params.pedidoId, false)
		def aprobacionUI = new AprobacionUI(justificacion: params.justificacionAprobacion, aprueba: true, pedido: pedido)
		pedidoService.avanzarPedido(params, aprobacionUI)
		flash.message = ""
		redirect(action: "index", params: params)
	}

	def denegarPedido = {
		params.pedidoId = params.pedidoId.toLong()
		def pedido = pedidoService.obtenerPedidoGenerico(params, params.pedidoId, false)
		def aprobacionUI = new AprobacionUI(justificacion: params.justificacionAprobacion, aprueba: false, pedido: pedido)
		pedidoService.denegarPedido(params, aprobacionUI)
		params.inbox = []
		flash.message = ""
		redirect(action: "index", params: params)
	}

	def accionMasiva = {

		def listAprobaciones
		if (params.inbox) {
			params.inbox = params.inbox.class.isArray() ? params.inbox as List : [params.inbox]
			listAprobaciones = params.inbox
		}

		if (params.tipoAccion) {
			if (listAprobaciones && listAprobaciones != []) {
				listAprobaciones.each {
					params.aprobacionId = it.toLong()
					def aprobacion = pedidoService.obtenerAprobacion(params.aprobacionId)
					def pedido = aprobacion.pedido //def pedido = pedidoService.obtenerPedidoGenerico(params, params.pedidoId)
					params.pedidoId = pedido.id
					def aprobacionUI
					if (params.tipoAccion.equalsIgnoreCase("denegarMasivo")) {
						aprobacionUI = new AprobacionUI(justificacion: params.justificacionAprobacion, aprueba: false, pedido: pedido)
						pedidoService.denegarPedido(params, aprobacionUI)
					}
					if (params.tipoAccion.equalsIgnoreCase("aprobarMasivo")) {
						aprobacionUI = new AprobacionUI(justificacion: params.justificacionAprobacion, aprueba: true, pedido: pedido)
						pedidoService.avanzarPedido(params, aprobacionUI)
					}
				}
			}
		}

		flash.message = ""
		params.tipoAccion = ""
		params.inbox = ""
		params.justificacionAprobacion = ""
		params.id = ""
		redirect(action: "index", params: params)
	}

}
