package ar.com.telecom

import ar.com.telecom.exceptions.BusinessException
import ar.com.telecom.pedido.AprobacionUI

class EvaluacionPedidoController extends WorkflowController {
	/**
	 * Métodos del controller que usa la vista
	 */
	def webUserAgentService
	
	def index = {
		if (webUserAgentService.isBlackberry()) {
			redirect(controller:"inboxMobile", action: "index")
		} else {
			def pedido = llenarDatosPedido(params, true)
			def usuarioLogueado = umeService.usuarioLogueado
	
			if(!pedido?.puedeReasignarEvaluacionPedido(usuarioLogueado)){
				redirect(action: "show", params: params)
				return
			}
	
			if (!pedido.legajoUsuarioGestionDemanda) {
				pedido.legajoUsuarioGestionDemanda = usuarioLogueado
			}
	
			def result = [pedidoInstance: pedido]
			result.putAll(getMapaDatosPedido(pedido))
			result
		}
	}
	
	def completarAsignatarios =  {
		withForm {
			try{
				def usuarioLogueado = umeService.usuarioLogueado
				def cambiaResponsable = params.cambiaResponsable
				//def pedido = llenarDatosPedido(params, cambiaResponsable)
				def pedido = llenarDatosPedido(params, true)

				if (cambiaResponsable && cambiaResponsable.toBoolean()){
					pedido.legajoUsuarioGestionDemanda = null
					pedido.aprobacionesPendientes(usuarioLogueado).each { aprobacion -> aprobacion.blaquearAsignatario() }

					// AMPer - Probar luego sacar la linea en que blanquea las aprobaciones pendientes, y llamar a guardar pedido con cambiaresponsable como 3er parametro TRUE					
					pedidoService.guardarPedido(params, new AprobacionUI(pedido: pedido, asignatario: null, usuario: usuarioLogueado, grupo: params.grupoGestionDemanda))
				}

				def personsAutocomplete = umeService.getUsuariosGrupoLDAP(params.grupoGestionDemanda, null)
				render (template: "formularioEvaluacion", model: [pedidoInstance: pedido] + getMapaDatosPedido(pedido))
			} catch (Exception e) {
				procesarErrorAjax(e)
			}
		}.invalidToken { throw new BusinessException("Operaci\u00F3n no v\u00E1lida. Por favor vuelva a intentarlo.") }
	}

	private def getMapaDatosPedido(pedido) {
		def usuarioLogueado = umeService.usuarioLogueado
		def grupo_FGD = pedido.grupoGestionDemanda
		def personsAutocompleteAsign

		if (pedido.puedeReasignarEvaluacionPedido(usuarioLogueado)){
			personsAutocompleteAsign = umeService.getUsuariosGrupoLDAP(grupo_FGD, null)
		}

		def grupo_CC = Constantes.instance.parametros.grupoLDAPCoordinadorPedido
		def personsAutocompleteCoord = umeService.getUsuariosGrupoLDAP(grupo_CC, null)

		// Si el usuario logueado pertenece a gestion demanda, lo seteo como asignatario
		if(!pedido.legajoUsuarioGestionDemanda && umeService.usuarioPerteneceGrupo(grupo_FGD, usuarioLogueado)){
			pedido.legajoUsuarioGestionDemanda = usuarioLogueado
		}
		[personsAutocompleteAsign: personsAutocompleteAsign, personsAutocompleteCoord: personsAutocompleteCoord, usuarioLogueado: usuarioLogueado, controller:params.controller]
	}

	/** Definiciones privadas de métodos */
	def generarAprobacionUI(aprueba) {
		def pedido = llenarDatosPedido(params, false)
		def result = new AprobacionUI(justificacion: params.justificacionAprobacion, aprueba: aprueba, pedido: pedido, grupo: params.grupoGestionDemanda, asignatario: params.legajoUsuarioGestionDemanda, usuario: umeService.usuarioLogueado)
		return result
	}


	def recargaBotonera = {
		withForm {
			def usuarioLogueado = umeService.getUsuarioLogueado()
			def pedido = llenarDatosPedido(params, true)
			if (pedido?.puedeEditarEvaluacion(usuarioLogueado)) {
				pedido.legajoUsuarioGestionDemanda = params.legajoNuevo
				pedido.legajoCoordinadorCambio = null
			}
			render (template: "formularioEvaluacion", model:[pedidoInstance: pedido] + getMapaDatosPedido(pedido))
		}.invalidToken { throw new BusinessException("Operaci\u00F3n no v\u00E1lida. Por favor vuelva a intentarlo.") }
	}

	/**
	 * Adapta los datos de un pedido
	 */
	def llenarDatosPedido(params, initial) {
		def usuarioLogueado = umeService.getUsuarioLogueado()
		def pedido = pedidoService.obtenerPedido(params, params.pedidoId)

		pedido.properties = params

		def asignatario = params.legajoUsuarioGestionDemanda
		boolean actualizaCoordinador = umeService.usuarioLogueado.equals(asignatario)

		if (!initial) {
			if (!params.legajoUsuarioGestionDemanda) {
				pedido.legajoUsuarioGestionDemanda = null
			}

			if (actualizaCoordinador) {
				// Si el pedido tiene, y params no, es porque hubo un borrado
				if (!params.legajoCoordinadorCambio) {
					pedido.legajoCoordinadorCambio = null
				}

				if (!params.prioridad) {
					pedido.prioridad = null
				}
			}

		}
		return pedido
	}
}