package ar.com.telecom

import ar.com.telecom.exceptions.BusinessException
import ar.com.telecom.pcs.entities.MotivoSuspension
import ar.com.telecom.pcs.entities.TipoAnexo

class SuspensionController extends WorkflowController{

	def index = {
	}

	def popupSuspender = {
		def pedido = pedidoService.obtenerPedido(params, params.pedidoId)
		render (template: "/suspension/suspensionPedido", model:[pedidoInstance: pedido, usuarioLogueado:umeService.getUsuarioLogueado()])
	}

	def suspenderPedido = {
		def pedido = pedidoService.obtenerPedido(params, params.pedidoId)
		pedido.motivoSuspension = null
		pedido.properties = params
		if (pedido.suspenderTotal(umeService.getUsuarioLogueado())) {
			pedidoService.guardar(params, pedido.parent)
			render (template: "/suspension/suspensionFinalizada", model:[pedidoInstance: pedido, usuarioLogueado:umeService.getUsuarioLogueado(), leyenda: "El pedido fue suspendido correctamente."])
		} else {
			render (template: "/suspension/suspensionPedido", model:[pedidoInstance: pedido, usuarioLogueado:umeService.getUsuarioLogueado()])
		}
	}

	def reanudarPedido = {
		def pedido = pedidoService.obtenerPedido(params, params.pedidoId)
		if (pedido.reanudarTotal(umeService.getUsuarioLogueado(), params.comentarioReanudacion)) {
			pedido.parent.comentarioReanudacion = null
			pedidoService.guardar(params, pedido.parent)
			render (template: "/suspension/suspensionFinalizada", model:[pedidoInstance: pedido, usuarioLogueado:umeService.getUsuarioLogueado(), leyenda: "El pedido fue reanudado correctamente."])
		}else {
			render (template: "/suspension/suspensionPedido", model:[pedidoInstance: pedido, usuarioLogueado:umeService.getUsuarioLogueado()])
		}
	}

	def guardarAnexo = {
		//		withForm {
		def pedido = llenarDatosPedido(params, true)
		pedidoService.guardar(params, pedido.parent)
		redirect(action: "index", controller: params.controllerBackSuspension, params: [pedidoId: pedido.id, muestraSuspensionPop: true])
		//		}.invalidToken { throw new BusinessException("Operación no v\u00E1lida. Por favor vuelva a intentarlo.") }
	}

	def eliminarAnexo = {
		def pedido = llenarDatosPedido(params, true)
		anexoService.eliminarAnexo(params, pedido, params.anexoId)
		redirect(action: "index", controller: pedido.faseActual.controllerNombre, id: pedido.id, params: [pedidoId: pedido.id, muestraSuspensionPop: true])
	}
	def popupAnexo = {
		log.info "popupAnexo : ${params}"
		withForm {
			try{
				def pedido = llenarDatosPedido(params, true)
				def tiposDeAnexo

				// Si viene por el popup de suspension, uso tipos de anexo Suspension/Reanudacion
				if(params.popSuspension){
					tiposDeAnexo = pedido.tipoAnexoPopSuspension
					pedidoService.guardar(params, pedido.parent)
				}else{
					tiposDeAnexo = TipoAnexo.findAllByFase(pedido.faseActual)
				}

				log.info "tiposDeAnexo: ${tiposDeAnexo}"
				render (template: "/templates/anexoPorTipo", model:[impacto:params?.impacto, pedidoInstance:pedido, tabSeleccionado:params?.tabSeleccionado, tiposDeAnexo:tiposDeAnexo, anexoFrom: params?.anexoFrom])
			} catch (Exception e) {
				procesarErrorAjax(e)
			}
		}.invalidToken { throw new BusinessException("Operaci\u00F3n no v\u00E1lida. Por favor vuelva a intentarlo.") }
	}

	def validarSuspension = {
		def pedido = pedidoService.obtenerPedido(params, params.pedidoId)
		pedido.properties = params
		//Suspende -> Valida suspension
		if(!pedido.estaSuspendido()){
			pedido.validaSuspension(umeService.usuarioLogueado)
		}
		render (template: "/suspension/suspensionPedido", model:[pedidoInstance: pedido, usuarioLogueado:umeService.getUsuarioLogueado()])
	}
}
