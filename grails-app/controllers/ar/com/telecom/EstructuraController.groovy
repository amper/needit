package ar.com.telecom

import ar.com.telecom.Estructura;

class EstructuraController {

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    def index = {
        redirect(action: "list", params: params)
    }

    def list = {
        params.max = Math.min(params.max ? params.int('max') : 10, 100)
        [estructuraInstanceList: Estructura.list(params), estructuraInstanceTotal: Estructura.count()]
    }

    def create = {
        def estructuraInstance = new Estructura()
        estructuraInstance.properties = params
        return [estructuraInstance: estructuraInstance]
    }

    def save = {
        def estructuraInstance = new Estructura(params)
        if (estructuraInstance.save(flush: true)) {
            flash.message = "${message(code: 'default.created.message', args: [message(code: 'estructura.label', default: 'Estructura'), estructuraInstance.id])}"
            redirect(action: "show", id: estructuraInstance.id)
        }
        else {
            render(view: "create", model: [estructuraInstance: estructuraInstance])
        }
    }

    def show = {
        def estructuraInstance = Estructura.get(params.id)
        if (!estructuraInstance) {
            flash.message = "${message(code: 'default.not.found.message', args: [message(code: 'estructura.label', default: 'Estructura'), params.id])}"
            redirect(action: "list")
        }
        else {
            [estructuraInstance: estructuraInstance]
        }
    }

    def edit = {
        def estructuraInstance = Estructura.get(params.id)
        if (!estructuraInstance) {
            flash.message = "${message(code: 'default.not.found.message', args: [message(code: 'estructura.label', default: 'Estructura'), params.id])}"
            redirect(action: "list")
        }
        else {
            return [estructuraInstance: estructuraInstance]
        }
    }

    def update = {
        def estructuraInstance = Estructura.get(params.id)
        if (estructuraInstance) {
            if (params.version) {
                def version = params.version.toLong()
                if (estructuraInstance.version > version) {
                    
                    estructuraInstance.errors.rejectValue("version", "default.optimistic.locking.failure", [message(code: 'estructura.label', default: 'Estructura')] as Object[], "Another user has updated this Estructura while you were editing")
                    render(view: "edit", model: [estructuraInstance: estructuraInstance])
                    return
                }
            }
            estructuraInstance.properties = params
            if (!estructuraInstance.hasErrors() && estructuraInstance.save(flush: true)) {
                flash.message = "${message(code: 'default.updated.message', args: [message(code: 'estructura.label', default: 'Estructura'), estructuraInstance.id])}"
                redirect(action: "show", id: estructuraInstance.id)
            }
            else {
                render(view: "edit", model: [estructuraInstance: estructuraInstance])
            }
        }
        else {
            flash.message = "${message(code: 'default.not.found.message', args: [message(code: 'estructura.label', default: 'Estructura'), params.id])}"
            redirect(action: "list")
        }
    }

    def delete = {
        def estructuraInstance = Estructura.get(params.id)
        if (estructuraInstance) {
            try {
                estructuraInstance.delete(flush: true)
                flash.message = "${message(code: 'default.deleted.message', args: [message(code: 'estructura.label', default: 'Estructura'), params.id])}"
                redirect(action: "list")
            }
            catch (org.springframework.dao.DataIntegrityViolationException e) {
                flash.message = "${message(code: 'default.not.deleted.message', args: [message(code: 'estructura.label', default: 'Estructura'), params.id])}"
                redirect(action: "show", id: params.id)
            }
        }
        else {
            flash.message = "${message(code: 'default.not.found.message', args: [message(code: 'estructura.label', default: 'Estructura'), params.id])}"
            redirect(action: "list")
        }
    }
}
