package ar.com.telecom

import grails.validation.ValidationException
import ar.com.telecom.pcs.entities.ParametrosSistema
import ar.com.telecom.pedido.especificacionImpacto.Impacto


class AprobacionPAUHijoController extends WorkflowController {

	/**
	 * Métodos del controller que usa la vista
	 */
	def webUserAgentService

	def index = {
		if (webUserAgentService.isBlackberry()) {
			redirect(controller:"inboxMobile", action: "index")
		} else {
			return getDatosPedido()
		}
	}


	def getDatosPedido() {
		def pedido = getPedidoATrabajar(params)
		def result = [pedidoInstance: pedido]
		result.putAll(getMapaDatosPedido(pedido))
		result
	}

	public popupConfirmacion(pedido, actionFrom) {
		render(view: "index", model: getDatosPedido()+[muestraPopDireccion:true, actionFrom: actionFrom])
		return new Boolean(true)
	}
	
	
	def definirImpacto(pedido) {
		def pedidoHijo
		def impactoInstance
		def impactoClave = params.impacto
		// Por default, el combo va hacia el consolidado
		def comboImpacto = Impacto.IMPACTO_CONSOLIDADO

		log.info "definirImpacto"
		log.info "Impacto: " + impactoClave

		if (impactoClave && Impacto.aplicaAHijo(impactoClave)) {
			pedidoHijo = pedidoService.obtenerPedidoHijo(params, impactoClave)
			// Si el hijo no consolida, lo seteo en el combo
			if (pedidoHijo.consolidaPAU()) {
				impactoInstance = new Impacto(Impacto.IMPACTO_CONSOLIDADO, pedido)
			} else {
				comboImpacto = impactoClave
				impactoInstance = new Impacto(Impacto.IMPACTO_HIJO, pedidoHijo)
			}
		} else {
			impactoInstance = new Impacto(impactoClave, pedido)
		}

		log.info "Combo Impacto: " + comboImpacto

		return [
			impactoInstance: impactoInstance,
			impacto: impactoClave,
			comboImpacto: comboImpacto
		]
	}

	def getMapaDatosPedido(pedido) {
		def result = definirImpacto(pedido)
		def parametros = ParametrosSistema.list().first()
		def usuarioLogueado = getUmeService().getUsuarioLogueado()
		def pedidosHijosConsolidados = pedido?.parent?.pedidosHijosQueConsolidanPAU()
		if (!pedidosHijosConsolidados) {
			pedidosHijosConsolidados = []
		}
		def pedidosHijosNoConsolidados = pedido?.parent?.pedidosHijosQueNoConsolidanPAU()
		if (!pedidosHijosNoConsolidados) {
			pedidosHijosNoConsolidados = []
		}
		def faseATrabajar = FaseMultiton.APROBACION_PAU_HIJO
		def impactos = []
		//	if (!pedidosHijosConsolidados.isEmpty()) {
		impactos.addAll(new Impacto(Impacto.IMPACTO_CONSOLIDADO, pedido))
		//}
		impactos.addAll(pedidosHijosNoConsolidados.collect { hijo ->
			new Impacto(Impacto.IMPACTO_HIJO, hijo)
		})
		// Borrado de tipos de impacto SW SAP
		//def impactosAMostrar = impactos.findAll { impacto -> impacto.pedido.esHijo() && !impacto.pedido.tipoImpacto?.toString().contains("SW Desarrollo SAP") }
		def impactosAMostrar = impactos.findAll { impacto -> impacto.apruebaPAU() }
		//



		log.info " pedidosHijosConsolidados: " + pedidosHijosConsolidados
		log.info " pedidosHijosNoConsolidados: " + pedidosHijosNoConsolidados
		pedido.pedidosHijos.findAll { pedidoHijo ->
			log.info pedidoHijo
			log.info "pedidoHijo.realizaPAU(): " + pedidoHijo.realizaPAU()
			log.info "pedidoHijo.consolidaPAU(): " + pedidoHijo.consolidaPAU()
			log.info "pedidoHijo.apruebaPAU():" + pedidoHijo.apruebaPAU()
			pedidoHijo.consolidaPAU() && pedidoHijo.apruebaPAU()
		}



		return [
			msgAdvertencia: parametros.mensajeAdvertenciaValidar,
			usuarioLogueado: usuarioLogueado,
			impactos: impactosAMostrar,
			pedidosHijosConsolidados: pedidosHijosConsolidados,
			pedidosHijosNoConsolidados: pedidosHijosNoConsolidados,
			faseATrabajar: faseATrabajar,
			impacto: params.impacto
		] + result
	}

	/**
	 * AMPer: En este caso params.impacto no es el hidden, es el valor del combo que viene por AJAX
	 * del js. Se deja así para que no pinche el getPedidoATrabajar del WFController.
	 */
	def seleccionaImpacto = {
		revisarImpactoModificado()
		def result = getDatosPedido()
		def destino
		if (Impacto.aplicaAHijo(params.comboImpacto)) {
			destino = "pedidoHijoNoConsolidado"
		} else {
			destino = "tablaHijosConsolidados"
		}
		log.info "seleccion impacto"
		log.info "result: " + result
		log.info "destino: " + destino
		render (template: destino, model: result)
	}

	def aprobarPedido = {
		def result = true
		def pedidoATrabajar = getPedidoATrabajar(params)
		def usuarioLogueado = umeService.usuarioLogueado
		def impactoInstance = new Impacto(!Impacto.aplicaAHijo(params.comboImpacto)?params.comboImpacto:Impacto.IMPACTO_HIJO, pedidoATrabajar)
		StringBuffer salida = new StringBuffer()

		def aprobacionUIPadre = generarAprobacionUI(true)

		if (!params.confirmaCambioDireccion) {
			// null  -> no contempla direcciones /
			// true  -> muestra popup confirmacion
			// false -> muestra error por mal validacion
			Boolean confirma = confirmarCambioDireccion(aprobacionUIPadre.pedido, 'aprobarPedido', true)

			if (confirma!=null){
				if(confirma){
					return
				}else{
					aprobacionUIPadre.pedido.validar(aprobacionUIPadre)
					result = false
				}
			}
		} else {
			aprobacionUIPadre.actualizarDireccion()
			result = validarDireccion(aprobacionUIPadre.pedido)
		}

		// Si las validaciones de direccion dan OK, continua aprobando los hijos
		if(result){
			def aprobacionesUI = impactoInstance.generarAprobaciones(true, params.justificacionAprobacion, usuarioLogueado)
			if (aprobacionesUI.isEmpty()) {
				flash.message "No hay pedidos para aprobar"
				return
			}
			aprobacionesUI.each{ aprobacionUI ->
				// Si algun pedido da error, el impacto esta mal
				try {
					def pedido = aprobacionUI.pedido
					params.aprobacionId = pedido.aprobacionPendiente(pedido?.faseActual, usuarioLogueado)?.id
					def apruebaOk = pedidoService.avanzarPedido(params, aprobacionUI)
	
					if(!apruebaOk && result){
						// Si algun pedido no pudo ser aprobado, el resultado final es false
						result = false
						salida.append("El pedido "+ aprobacionUI?.pedido +" no pudo ser aprobado (fase actual " + aprobacionUI?.pedido?.faseActual + ")\n")
					}else{
						salida.append("El pedido "+ aprobacionUI?.pedido +" fue aprobado (fase actual " + aprobacionUI?.pedido?.faseActual + ")\n")
					}
	
				} catch (ValidationException e) {
					log.error "Aprobar pedido"
					log.error e.message
	
					salida.append("El pedido " + aprobacionUI?.pedido + " no pudo ser aprobado: "+e.message)
	
					result = false
				} catch (Exception e) {
					log.error "Error en Aprobar pedido"
					log.error e.message
					throw e
				}
			}
		}
		mostrarResultados(result, salida.toString(), aprobacionUIPadre, true)
	}

	def denegarPedido = {
		def aprobacionesUI
		def result = true
		def pedidoATrabajar = getPedidoATrabajar(params)
		def usuarioLogueado = umeService.usuarioLogueado
		def impactoInstance = new Impacto(!Impacto.aplicaAHijo(params.comboImpacto)?params.comboImpacto:Impacto.IMPACTO_HIJO, pedidoATrabajar)
		StringBuffer salida = new StringBuffer()

		def aprobacionUIPadre = generarAprobacionUI(true)
		
		
		if (!params.confirmaCambioDireccion) {
			// null  -> no contempla direcciones /
			// true  -> muestra popup confirmacion
			// false -> muestra error por mal validacion
			Boolean confirma = confirmarCambioDireccion(aprobacionUIPadre.pedido, 'aprobarPedido', true)

			if (confirma!=null){
				if(confirma){
					return
				}else{
					aprobacionUIPadre.pedido.validar(aprobacionUIPadre)
					result = false
				}
			}
		} else {
			aprobacionUIPadre.actualizarDireccion()
			result = validarDireccion(aprobacionUIPadre.pedido)
		}

		// Si las validaciones de direccion dan OK, continua aprobando los hijos
		if(result){
		
			aprobacionesUI = impactoInstance.generarAprobaciones(false, params.justificacionAprobacion, usuarioLogueado)
	
			if (aprobacionesUI.isEmpty()) {
				flash.message "No hay pedidos para aprobar"
				return
			}
	
			aprobacionesUI.each{ aprobacionUI ->
				// Si algun pedido da error, el impacto esta mal
				try {
					def pedido = aprobacionUI.pedido
					params.aprobacionId = pedido.aprobacionPendiente(pedido?.faseActual, usuarioLogueado)?.id
					def deniegaOk = pedidoService.denegarPedido(params, aprobacionUI)
					if(!deniegaOk){
						// Si algun pedido no pudo ser denegado, el resultado final es false
						aprobacionUI.pedido.errors.each {
							aprobacionUIPadre.pedido.errors.reject it.globalError.code
						}
						result = false
						salida.append("El pedido "+ aprobacionUI?.pedido +" no pudo ser denegado (fase actual " + aprobacionUI?.pedido?.faseActual + ")\n")
					}else{
						salida.append("El pedido "+ aprobacionUI?.pedido +" fue denegado (fase actual " + aprobacionUI?.pedido?.faseActual + ")\n")
					}
				} catch (ValidationException e) {
					log.error "Aprobar pedido"
					log.error e.message
					salida.append("El pedido " + aprobacionUI?.pedido + " no pudo ser denegado: "+e.message)
					result = false
				} catch (Exception e) {
					log.error "Error en denegar pedido"
					log.error e.message
					throw e
				}
			}
		}
		mostrarResultados(result, salida.toString(), aprobacionUIPadre, true)
	}


	def simularValidacion = {
		def aprobacionesUI
		def result = true
		def pedidoATrabajar = getPedidoATrabajar(params)
		def usuarioLogueado = umeService.usuarioLogueado
		def impactoInstance = new Impacto(!Impacto.aplicaAHijo(params.comboImpacto)?params.comboImpacto:Impacto.IMPACTO_HIJO, pedidoATrabajar)
		StringBuffer salida = new StringBuffer()

		def aprobacionUIPadre = generarAprobacionUI(true)

		aprobacionesUI = impactoInstance.generarAprobaciones(true, params.justificacionAprobacion, usuarioLogueado)

		if (aprobacionesUI.isEmpty()) {
			flash.message "No hay pedidos para aprobar"
			return
		}

		aprobacionesUI.each{ aprobacionUI ->
			// Si algun pedido da error, el impacto esta mal
			try {
				def apruebaOk = pedidoService.validarPedido(params, aprobacionUI)

				if(!apruebaOk){
					// Si algun pedido no pudo ser denegado, el resultado final es false
					aprobacionUI.pedido.errors.each {
						aprobacionUIPadre.pedido.errors.reject it.globalError.code
					}
					result = false
					salida.append("El pedido "+ aprobacionUI?.pedido +" no esta ok (fase actual " + aprobacionUI?.pedido?.faseActual + ")")
				}else{
					salida.append("El pedido "+ aprobacionUI?.pedido +" esta ok (fase actual " + aprobacionUI?.pedido?.faseActual + ")")
				}


			} catch (ValidationException e) {
				log.error "Simular pedido"
				log.error e.message

				salida.append("El pedido " + aprobacionUI?.pedido + " no esta ok: "+e.message)

				result = false
			} catch (Exception e) {
				log.error "Error en simular pedido"
				log.error e.message
				throw e
			}
		}

		mostrarResultados(result, salida.toString(), aprobacionUIPadre, true)
	}

	def grabarFormulario = {
		def aprobacionesUI
		def result = true
		def pedidoATrabajar = getPedidoATrabajar(params)
		def impactoInstance = new Impacto(!Impacto.aplicaAHijo(params.comboImpacto)?params.comboImpacto:Impacto.IMPACTO_HIJO, pedidoATrabajar)
		def usuarioLogueado = umeService.usuarioLogueado
		StringBuffer salida = new StringBuffer()

		def aprobacionUIPadre = generarAprobacionUI(true)

//amp		
		
		if (!params.confirmaCambioDireccion) {
			// null  -> no contempla direcciones /
			// true  -> muestra popup confirmacion
			// false -> muestra error por mal validacion
			Boolean confirma = confirmarCambioDireccion(aprobacionUIPadre.pedido, 'grabarFormulario', true)

			if (confirma!=null){
				if(confirma){
					return
				}else{
					aprobacionUIPadre.pedido.validar(aprobacionUIPadre)
					result = false
				}
			}
		} else {
			aprobacionUIPadre.actualizarDireccion()
			result = validarDireccion(aprobacionUIPadre.pedido)
		}

		// Si las validaciones de direccion dan OK, continua aprobando los hijos
		if(result){
		
		
//amp		
			aprobacionesUI = impactoInstance.generarAprobaciones(true, params.justificacionAprobacion, usuarioLogueado)
	
			aprobacionesUI.each{ aprobacionUI ->
				// Si algun pedido da error, el impacto esta mal
				try {
					pedidoService.guardarPedido(params, aprobacionUI)
				} catch (ValidationException e) {
					log.error "Simular pedido"
					log.error e.message
	
					salida.append("El pedido " + aprobacionUI?.pedido + " no esta ok: "+e.message)
	
					result = false
				} catch (Exception e) {
					result = false
					log.error "Error en simular pedido"
					log.error e.message
					throw e
				}
			}
		}
		
		mostrarResultados(result, "El pedido fue guardado", aprobacionUIPadre, true)
	}

	def mostrarResultados(ok, mensajeOk, aprobacionUI, finaliza) {
		def pedido = aprobacionUI.pedido
		if (ok) {
			flash.message = mensajeOk
		}
		if (ok && finaliza) {
			faseFinalizada()
		}else{
			// Si hay errores, que vuelva al index
			def modelView = [pedidoInstance: pedido.parent, justificacionAprobacion: params.justificacionAprobacion]
			modelView.putAll(getMapaDatosPedido(pedido))
			render(view: "index", model: modelView)
		}
	}

	def faseFinalizada() {
		render(view: "index", model: getDatosPedido())
	}

	def recargaBotonera = {
		def pedido = pedidoService.obtenerPedido(params, params.pedidoId)
		render (template: "botonera", model: getMapaDatosPedido(pedido))
	}

	// este método había que redefinirlo para que tome ok la PAU
	def getPedidoATrabajar(params) {
		def impacto = params.comboImpacto
		if (!impacto) {
			impacto = params.impacto
		}
		def trabajoSobreHijo = Impacto.aplicaAHijo(impacto)
		if (trabajoSobreHijo) {
			return pedidoService.obtenerPedidoHijo(params, impacto)
		} else {
			return pedidoService.obtenerPedido(params, params.pedidoId, true)
		}
	}

	private def revisarImpactoModificado() {
		//		if (params.comboImpacto && Impacto.aplicaAHijo(params.comboImpacto)) {
		//			params.impacto = params.comboImpacto
		//		}
	}
}
