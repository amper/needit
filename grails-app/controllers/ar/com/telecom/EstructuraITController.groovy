package ar.com.telecom

class EstructuraITController {

	static adminMenu = true

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    def index = {
        redirect(action: "list", params: params)
    }

    def list = {
        params.max = Math.min(params.max ? params.int('max') : 10, 100)
        [estructuraITInstanceList: EstructuraIT.list(params), estructuraITInstanceTotal: EstructuraIT.count()]
    }

    def create = {
        def estructuraITInstance = new EstructuraIT()
        estructuraITInstance.properties = params
        return [estructuraITInstance: estructuraITInstance]
    }

    def save = {
        def estructuraITInstance = new EstructuraIT(params)
        if (estructuraITInstance.save(flush: true)) {
            flash.message = "${message(code: 'default.created.message', args: [message(code: 'estructuraIT.label', default: 'EstructuraIT'), estructuraITInstance.id])}"
            redirect(action: "show", id: estructuraITInstance.id)
        }
        else {
            render(view: "create", model: [estructuraITInstance: estructuraITInstance])
        }
    }

    def show = {
        def estructuraITInstance = EstructuraIT.get(params.id)
        if (!estructuraITInstance) {
            flash.message = "${message(code: 'default.not.found.message', args: [message(code: 'estructuraIT.label', default: 'EstructuraIT'), params.id])}"
            redirect(action: "list")
        }
        else {
            [estructuraITInstance: estructuraITInstance]
        }
    }

    def edit = {
        def estructuraITInstance = EstructuraIT.get(params.id)
        if (!estructuraITInstance) {
            flash.message = "${message(code: 'default.not.found.message', args: [message(code: 'estructuraIT.label', default: 'EstructuraIT'), params.id])}"
            redirect(action: "list")
        }
        else {
            return [estructuraITInstance: estructuraITInstance]
        }
    }

    def update = {
        def estructuraITInstance = EstructuraIT.get(params.id)
        if (estructuraITInstance) {
            if (params.version) {
                def version = params.version.toLong()
                if (estructuraITInstance.version > version) {
                    
                    estructuraITInstance.errors.rejectValue("version", "default.optimistic.locking.failure", [message(code: 'estructuraIT.label', default: 'EstructuraIT')] as Object[], "Another user has updated this EstructuraIT while you were editing")
                    render(view: "edit", model: [estructuraITInstance: estructuraITInstance])
                    return
                }
            }
            estructuraITInstance.properties = params
            if (!estructuraITInstance.hasErrors() && estructuraITInstance.save(flush: true)) {
                flash.message = "${message(code: 'default.updated.message', args: [message(code: 'estructuraIT.label', default: 'EstructuraIT'), estructuraITInstance.id])}"
                redirect(action: "show", id: estructuraITInstance.id)
            }
            else {
                render(view: "edit", model: [estructuraITInstance: estructuraITInstance])
            }
        }
        else {
            flash.message = "${message(code: 'default.not.found.message', args: [message(code: 'estructuraIT.label', default: 'EstructuraIT'), params.id])}"
            redirect(action: "list")
        }
    }

    def delete = {
        def estructuraITInstance = EstructuraIT.get(params.id)
        if (estructuraITInstance) {
            try {
                estructuraITInstance.delete(flush: true)
                flash.message = "${message(code: 'default.deleted.message', args: [message(code: 'estructuraIT.label', default: 'EstructuraIT'), params.id])}"
                redirect(action: "list")
            }
            catch (org.springframework.dao.DataIntegrityViolationException e) {
                flash.message = "${message(code: 'default.not.deleted.message', args: [message(code: 'estructuraIT.label', default: 'EstructuraIT'), params.id])}"
                redirect(action: "show", id: params.id)
            }
        }
        else {
            flash.message = "${message(code: 'default.not.found.message', args: [message(code: 'estructuraIT.label', default: 'EstructuraIT'), params.id])}"
            redirect(action: "list")
        }
    }
}
