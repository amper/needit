package ar.com.telecom

import grails.converters.JSON
import ar.com.telecom.pcs.entities.Sistema

class ComboPruebaController extends WorkflowController{
	
    def index = { }
	
	def autocomplete = {
		// 1 - Taglib --> hidden con el nombre posta (hay que ponerle "sistemaSugerido.id") y crear el textbox que 
		// se llame "_sistemaSugerido.id"   "_" + name
		// 2- Permitir que en la definición del comboRapido le pongamos : "Sistema.createCriteria().list(max: 5)"
		//    a) Service
		//    b) campo por el cual filtrás
		//
		//    a) Usar un service 
		
		def result = []
		try {
			result = Sistema.createCriteria().list(max: 5){
				ilike("descripcion", "%" + params.word?.trim() + "%")
			}
		} catch(Exception ex) {
			result = [ 'error' : false ]
		}
		jsonOutput(result)
	}
	
	def jsonOutput(Object json) {
		render json as JSON
	}
}
