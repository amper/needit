package ar.com.telecom

import grails.validation.ValidationException

import org.hibernate.criterion.CriteriaSpecification

import ar.com.telecom.pcs.entities.AbstractPedido
import ar.com.telecom.pcs.entities.PlanificacionEsfuerzo
import ar.com.telecom.pcs.entities.SoftwareFactory
import ar.com.telecom.pedido.especificacionImpacto.Impacto

class RealesIncurridosController extends WorkflowController{

    def index = { 
		if (!params.impacto) {
			params.impacto = Impacto.IMPACTO_CONSOLIDADO
		}
		
		def pedido = pedidoService.obtenerPedido(params, params.pedidoId)
		if (!pedido.esHijo() && pedido.tieneUnicoImpacto()) {
			params.impacto = "" + pedido.impactos.first().id
		}
		
		def result = [pedidoInstance: pedido]
		result.putAll(getMapaDatosPedido())
		result
		
	}
	
	
	
	private def getMapaDatosPedido() {
		def pedidoATrabajar = getPedidoATrabajar(params)
		def grupoResponsable = pedidoATrabajar.getGrupoResponsable()
		def personsAutocompleteAsign = getUmeService().getUsuariosGrupoLDAP(grupoResponsable, null)
		def releases = new ArrayList()
		def usuarioLogueado = umeService.getUsuarioLogueado()
	
		// Determino si el impacto aplica sobre el hijo
		def trabajaSobrePadre = Impacto.aplicaAPadre(params.impacto)
		def trabajaSobreCoord = Impacto.aplicaACoordinacion(params.impacto)
		def trabajoSobreHijo = Impacto.aplicaAHijo(params.impacto)
		def impactoInstance = new Impacto(!Impacto.aplicaAHijo(params.impacto)?params.impacto:Impacto.IMPACTO_HIJO, pedidoATrabajar)

		def gruposList = impactoInstance.getGruposPlanificacion()
		
		def consol = pedidoService.planificacionConsolidadaIncurrida(pedidoATrabajar.parent, impactoInstance, params.grupo)
		 	
		[personsAutocompleteAsign: personsAutocompleteAsign,
					grupoResponsable: grupoResponsable,
					usuarioLogueado: usuarioLogueado,
					pedidoHijo: pedidoATrabajar,
					trabajoSobreHijo: trabajoSobreHijo,
					trabajaSobrePadre: trabajaSobrePadre,
					trabajaSobreCoord: trabajaSobreCoord,
					impacto: params.impacto,
					impactoInstance : impactoInstance,
					consolidacionList : consol,
					gruposList: gruposList]
	}

	
	def seleccionGrupo = {
		def grupo = (params.grupo=='null') ? null : params.grupo
		
		def pedidoATrabajar = getPedidoATrabajar(params)
		def detallesPlanificacionFiltrados = pedidoATrabajar.obtenerDetallesPlanificacion(grupo, params.impacto)
		def result = [detallesPlanificacionFiltrados:detallesPlanificacionFiltrados, grupo:grupo]
		result.putAll(getMapaDatosPedido())
		
		
		render(template:"tablaDetalles", model:result)
	}
	
	def limpiaDetalle = {
		def usuarioLogueado = umeService.getUsuarioLogueado()
		def pedidoATrabajar = pedidoService.obtenerPedidoHijo(params, params.impacto)
		def impactoInstance = new Impacto(Impacto.IMPACTO_HIJO, pedidoATrabajar)

		def detalle = pedidoService.obtenerDetalle(params.detalleId)
		detalle.blanqueaHorasIncurridas(params.grupo)
		pedidoService.actualizarDetalle(detalle)
	
		render (template: "filaDetalle", model:[detalle: detalle, grupo: params.grupo, impactoInstance:impactoInstance, usuarioLogueado:usuarioLogueado])
	}
	
	def confirmaDetalle = {
		def usuarioLogueado = umeService.getUsuarioLogueado()
		def detalle = pedidoService.obtenerDetalle(params.detalleId)
		def pedidoATrabajar = getPedidoATrabajar(params)
		def impactoInstance = new Impacto(!Impacto.aplicaAHijo(params.impacto)?params.impacto:Impacto.IMPACTO_HIJO, pedidoATrabajar)
		
		def cantidadHoras
		def swf = SoftwareFactory.findByGrupoLDAP(params.grupo)

		if(params.horasIncurridas != 'null' && params.horasIncurridas != ""){
			cantidadHoras = params.horasIncurridas.toInteger().intValue()
			detalle.confirmarRealesIncurridos(swf, cantidadHoras, usuarioLogueado, pedidoATrabajar)
			pedidoService.actualizarDetalle(detalle)
			pedidoService.guardar(params, pedidoATrabajar)
		}else{
			cantidadHoras = 0
		}

		render(template:"filaDetalle", model:[detalle: detalle, grupo: params.grupo, impactoInstance: impactoInstance, usuarioLogueado: usuarioLogueado])
		
	}
	
	def grabarFormulario = {
		def result
		def pedidoATrabajar
		try {
			pedidoATrabajar = getPedidoATrabajar(params)
			//result = pedidoService.guardar(pedidoATrabajar)
			
			def grupo = (params.grupo=='null') ? null : params.grupo 
			def detallesPlanificacionFiltrados = pedidoATrabajar.obtenerDetallesPlanificacion(grupo, params.impacto)
			
			if(grupo){
				def swf = SoftwareFactory.findByGrupoLDAP(grupo)
				detallesPlanificacionFiltrados.each { detalle ->
					def cantHoras = params.get("incu_"+detalle.id)
					
					// De no ser un n�mero, va cero
					try{
						new Integer(cantHoras)
					}catch(NumberFormatException e){
						cantHoras = 0
					}
					
					
					detalle.actualizarHorasIncurridas(swf,cantHoras)				
					pedidoService.actualizarDetalle(detalle)
				}
			}
			
			pedidoATrabajar.comentarioRealesIncurridos = params.comentarioRealesIncurridos
			pedidoService.guardar(params, pedidoATrabajar)
			
			
		} catch (ValidationException e) {
			result = false
		}
		mostrarResultados(result, "El pedido fue guardado" , pedidoATrabajar, params.impacto)
	}
	
	def mostrarResultados(ok, mensajeOk, pedido, impacto) {
		if (ok) {
			flash.message = mensajeOk
		}
		
		redirect (action: "index", params: [pedidoId: pedido.getParent().id, impacto:impacto])
	}
	
	def actualizaGrupos = {
		def pedidoATrabajar = getPedidoATrabajar(params)
		def impactoInstance = new Impacto(!Impacto.aplicaAHijo(params.impacto)?params.impacto:Impacto.IMPACTO_HIJO, pedidoATrabajar)
		
		
		render(template:"gruposCargados", model:[pedido: impactoInstance.pedido, impacto: impactoInstance.id.toString()])
	}

	
	def copiaHorasPlanificadasAIncurridas = {
		def usuarioLogueado = umeService.getUsuarioLogueado()
		def pedidoATrabajar = pedidoService.obtenerPedidoHijo(params, params.impacto)
		def impactoInstance = new Impacto(Impacto.IMPACTO_HIJO, pedidoATrabajar)
		def swf = SoftwareFactory.findByGrupoLDAP(params.grupo)

		def detalle = pedidoService.obtenerDetalle(params.detalleId)
		detalle.actualizarHorasIncurridas(swf,detalle.totalHoras(params.grupo))
		
		pedidoService.actualizarDetalle(detalle)
	
		render (template: "filaDetalle", model:[detalle: detalle, grupo: params.grupo, impactoInstance:impactoInstance, usuarioLogueado:usuarioLogueado])
	}
		
}
