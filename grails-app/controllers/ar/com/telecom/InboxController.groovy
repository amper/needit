package ar.com.telecom

import org.codehaus.groovy.grails.commons.ConfigurationHolder

import ar.com.telecom.JavaUtil.htmlToText
import ar.com.telecom.exceptions.BusinessException
import ar.com.telecom.pedido.consulta.Consulta

class InboxController extends AbstractController {

	def pedidoService
	def umeService
	def exportService
	
	def jerarquiaPrueba = {
		def jerarquias = umeService.getJerarquiaUsuario(params.id)
		render jerarquias.nombreApellido
	}

	def index = {
		redirect(action: "list", params: params)
	}

	def list = {
		// Blanqueo el pedido si vengo del wizard que trabaja el workflow
		def usuarioLogueado = umeService.getUsuarioLogueado()
		// TODO: Hay que redirigir a una p\u00E1gina de error
		def pedidoInstanceList = pedidoService.obtenerPedidosInbox(params, usuarioLogueado)
		if (params.totalCount>200){
			flash.message = "Existen m\u00E1s de 200 pedidos, puede refinar la b\u00FAsqueda para reducir la cantidad encontrada"
		}
		
		def tiposConsulta = pedidoService.getTiposConsulta()
		def rolGrupos = pedidoService.getRolesGrupos(params, usuarioLogueado)

		[tipoConsulta: params.tipoConsulta, tiposConsulta: tiposConsulta, pedidoInstanceList: pedidoInstanceList, aprobacionInstanceTotal: params.totalCount, rolGrupos: rolGrupos, usuarioLogueado: usuarioLogueado]
	}

	def inboxCount = {
		def usuarioLogueado = umeService.getUsuarioLogueado()
		def pedidoInstanceList = pedidoService.inboxCount(params, usuarioLogueado)
		render params.totalCount
	}

	def verPendientes = {
		withForm {
			try{
				limpiarParams(params)
				if (params.sort) {
					redirect(action: "list", params: params)
				} else {
					def usuarioLogueado = umeService.getUsuarioLogueado()
					def pedidos = pedidoService.obtenerPedidosInbox(params, usuarioLogueado)
					if (params.totalCount>200){
						flash.message = "Existen m\u00E1s de 200 pedidos, puede refinar la b\u00FAsqueda para reducir la cantidad encontrada"
					}
					
					render (template:"/templates/inbox", model:[tipoConsulta: params.tipoConsulta, pedidoInstanceList: pedidos, aprobacionInstanceTotal: params.totalCount, usuarioLogueado: usuarioLogueado] )
				}
			} catch (Exception e) {
				procesarErrorAjax(e)
			}
		}.invalidToken { throw new BusinessException("Operaci\u00F3n no v\u00E1lida. Por favor vuelva a intentarlo.") }
	}

	// TODO : MANDARLO AL SERVICE
	//	def obtenerPedidosInbox(params, usuarioLogueado){
	//		def listado = pedidoService.consultarAprobacionesInbox(params, usuarioLogueado)
	//		if (!params.tipoConsulta.equals(Consulta.EN_SEGUIMIENTO_PROPIOS)){
	//			listado = ordenarInbox(listado.pedido) //listado en este caso es de aprobaciones
	//		}else{
	//			listado = ordenarInbox(listado) //listado en este caso es de pedidos
	//		}
	//		// TODO: Poner en un par\u00E1metro de sistema
	//		if (params.totalCount>200){
	//			flash.message = "Existen m\u00E1s de 200 pedidos, puede refinar la b\u00FAsqueda para reducir la cantidad encontrada"
	//		}
	//			return listado
	//	}

	void limpiarParams(params){
		params.remove("org")
		params.remove("org.codehaus")
		params.remove("org.codehaus.groovy")
		params.remove("org.codehaus.groovy.grails")
		params.remove("org.codehaus.groovy.grails.SYNCHRONIZER_TOKEN")
	}

	def exportFilePedidosPadre = {
		def usuarioLogueado = umeService.getUsuarioLogueado()
		def pedidos = pedidoService.consultarAprobacionesInbox(params, usuarioLogueado)

		log.debug "Pedidos: " + pedidos

		def listPedidosPadres = []
		def listPedidosHijos = []
		if (!params.tipoConsulta.equals(Consulta.EN_SEGUIMIENTO_PROPIOS)){
			pedidos.pedido.each {  pedido ->
				if (!pedido.esHijo())
					listPedidosPadres.add(pedido)
			}
		} else {
			pedidos.each { pedido ->
				if (!pedido.esHijo())
					listPedidosPadres.add(pedido)
			}
		}

		if (!listPedidosPadres.isEmpty()){
			exportPedidosPadre(listPedidosPadres, usuarioLogueado)
		}
		redirect(action: "list", params: params)
	}


	def exportFilePedidosHijo = {
		def usuarioLogueado = umeService.getUsuarioLogueado()
		def pedidos = pedidoService.consultarAprobacionesInbox(params, usuarioLogueado)
		def listPedidosHijos = []
		if (!params.tipoConsulta.equals(Consulta.EN_SEGUIMIENTO_PROPIOS)){
			pedidos.pedido.each { pedido ->
				pedido.each{ pedidoHijo ->
					if (pedidoHijo.esHijo()){
						listPedidosHijos.add(pedidoHijo)
					}
				}
			}
		}else{
			pedidos.each { 	 pedidoHijo ->
				if (pedidoHijo.esHijo()){
					listPedidosHijos.add(pedidoHijo)
				}
			}
		}
		if (!listPedidosHijos.isEmpty()){
			exportPedidosHijos(listPedidosHijos, usuarioLogueado)
		}else{
			flash.message = "No hay pedidos hijos para exportar"
		}
		redirect(action: "list", params: params)
	}

	def exportPedidosPadre(listPedidos, usuarioLogueado){

		if(params?.format && params.format != "html"){
			Date fechaActual = new Date()
			def nameArch = "InboxPedidosPadres_Fecha_" + formatDate(date: fechaActual, type: "date", style: "MEDIUM")  //"InboxPedidosPadres_" + fechaActual

			response.contentType = ConfigurationHolder.config.grails.mime.types[params.format]
			response.setHeader("Content-disposition", "attachment; filename=$nameArch.${params.extension}")
			List fields =[
				"id",
				"titulo",
				"faseActual",
				"macroEstado",
				"rol",
				"legajoCoordinadorCambio" ,
				"legajoInterlocutorUsuario",
				"prioridad",
				"grupoGestionDemanda",
				"legajoUsuarioGestionDemanda",
				"alcance",
				"objetivo",
				"fechaCreacion"
			]
			Map labels = ['id':'N\u00FAmero',
						'titulo':'T\u00EDtulo',
						'faseActual':'Fase',
						'macroEstado':'MacroEstado',
						//'rol':'Rol',
						'legajoInterlocutorUsuario':'Interlocutor Usuario',
						'legajoCoordinadorCambio':'Coordinador',
						'prioridad':'Prioridad',
						'grupoGestionDemanda':'Grupo Gesti\u00F3n de la demanda',
						'legajoUsuarioGestionDemanda':'Usuario asignatario gesti\u00F3n de la demanda' ,
						'alcance':'Alcance',
						'objetivo':'Objetivo',
						'fechaCreacion':'Fecha Creaci\u00F3n']

			def alcance = { domain, value ->
				def html = new htmlToText()
				if (domain?.alcance)
					return html.html2text(domain?.alcance)
				else
					return ""
			}
			def objetivo = { domain, value ->
				def html = new htmlToText()
				if (domain?.objetivo)
					return html.html2text(domain?.objetivo)
				else
					return ""
			}
			def faseExport = { domain, value ->
				return domain?.faseActual
			}
			def macroEstadoExport = { domain, value ->
				return domain?.faseActual.macroEstado
			}
			def rolExport = { domain, value ->
				if (domain?.faseActual && usuarioLogueado){
					return domain.getRol(domain?.faseActual, usuarioLogueado)
				}else{
					return ""
				}

			}
			def coordinadorExport = { domain, value ->
				if (domain.legajoCoordinadorCambio){
					return domain?.legajoCoordinadorCambio + " - " + obtenerNombre(domain?.legajoCoordinadorCambio)
				} else{
					return ""
				}
			}
			def interlocutorUsuarioExport = { domain, value ->
				if (domain.legajoInterlocutorUsuario){
					return  domain?.legajoInterlocutorUsuario + " - " + obtenerNombre(domain?.legajoInterlocutorUsuario)
				} else{
					return ""
				}
			}
			def fechaCreacionExport = { domain, value ->
				return formatDate(date: domain.fechaCargaPedido, type: "datetime", style: "MEDIUM")
			}

			Map formatters = [faseActual: faseExport, macroEstado: macroEstadoExport, rol: rolExport, legajoCoordinadorCambio: coordinadorExport, legajoInterlocutorUsuario: interlocutorUsuarioExport, fechaCreacion:fechaCreacionExport, alcance:alcance, objetivo:objetivo]
			exportService.export(params.format, response.outputStream,listPedidos, fields, labels,formatters,[:])
		}
	}

	def obtenerNombre(legajo){
		def usuario = umeService.getUsuario(legajo)
		if (usuario){
			return usuario?.nombreApellido
		}
	}

	def exportPedidosHijos(listPedidos, usuarioLogueado){

		if(params?.format && params.format != "html"){

			Date fechaActual = new Date()
			def nameArch = "InboxPedidosHijos_Fecha_" + formatDate(date: fechaActual, type: "date", style: "MEDIUM") //+ fechaActual

			response.contentType = ConfigurationHolder.config.grails.mime.types[params.format]
			response.setHeader("Content-disposition", "attachment; filename=$nameArch.${params.extension}")

			List fields =[
				"numero",
				"titulo",
				"fase",
				"macroEstado",
				"grupoPendiente",
				"legajoUsuarioResponsable",
				"coordinador" ,
				"interlocutorUsuario",
				"prioridad",
				"tipoImpacto",
				"sistema"
			]
			Map labels = ['numero':'N\u00FAmero Pedido',
						'titulo':'T\u00EDtulo',
						'fase':'Fase',
						'macroEstado':'MacroEstado',
						'grupoPendiente':'Grupo Pendiente',
						'legajoUsuarioResponsable':'Pendiente Por',
						'interlocutorUsuario':'Interlocutor Usuario',
						'coordinador':'Coordinador',
						'tipoImpacto':'Tipo Impacto',
						'sistema':'Sistema / \u00E1rea de soporte',
						'prioridad':'Prioridad']


			def numero = { domain, value ->
				return domain?.getNumero()
			}
			def fase = { domain, value ->
				return domain?.faseActual
			}
			def macroEstado = { domain, value ->
				return domain?.faseActual.macroEstado
			}
			def rol = { domain, value ->
				return domain.getRol(domain?.faseActual, usuarioLogueado)
			}
			def coordinador = { domain, value ->
				if (domain?.pedidoPadre.legajoCoordinadorCambio)
					return domain?.pedidoPadre.legajoCoordinadorCambio + "-" + obtenerNombre(domain?.pedidoPadre.legajoCoordinadorCambio)
				else
					return ""
			}
			def legajoUsuarioResponsable = { domain, value ->
				if (domain?.legajoUsuarioResponsable)
					return domain?.legajoUsuarioResponsable + "-" + obtenerNombre(domain?.legajoUsuarioResponsable)
				else
					return ""
			}
			def interlocutorUsuario = { domain, value ->
				if (domain?.pedidoPadre.legajoInterlocutorUsuario)
					return domain?.pedidoPadre.legajoInterlocutorUsuario + "-" + obtenerNombre(domain?.pedidoPadre.legajoInterlocutorUsuario)
				else
					return ""
			}
			def prioridad = { domain, value ->
				return domain?.pedidoPadre.prioridad
			}
			def sistema = { domain, value ->
				return domain?.getTipoPlanificacion()
			}
			def grupoPendiente = { domain, value ->
				return domain?.grupoResponsable
			}

			Map formatters = [numero: numero, fase: fase, macroEstado: macroEstado, rol: rol, coordinador: coordinador, interlocutorUsuario: interlocutorUsuario, prioridad : prioridad, sistema:sistema,grupoPendiente:grupoPendiente, legajoUsuarioResponsable:legajoUsuarioResponsable]
			exportService.export(params.format, response, nameArch, params?.extension,listPedidos, fields, labels,formatters,[:])
		}
	}

	// TODO: Mandarlo al service
	//	def ordenarInbox(pedidos){
	//		def pedidosPadres = pedidos.collect {
	//			it.parent
	//		}.unique { a, b->
	//			a.id <=> b.id
	//		}
	//		definirPaginacionInbox(params)
	////		log.debug "pedidos padres: " + pedidosPadres.id
	//		def listadoPedidos
	//		if (pedidosPadres.isEmpty()) {
	//			params.totalCount = 0
	//			listadoPedidos = []
	//		} else {
	//			listadoPedidos = obtenerPedidosInbox(pedidosPadres.id, params)
	////			Pedido.createCriteria().list(max: params.max, offset: params.offset) {
	////				inList("id", pedidosPadres.id)
	////				createAlias ('faseActual', 'fasePedido')
	////				order (params.sort, params.order)
	////			}
	//			params.totalCount = listadoPedidos.totalCount
	//		}
	//		listadoPedidos
	//	}


	//	private def definirPaginacionInbox(params) {
	//		params.sort = params.sort?:'id'
	//		params.order = params.order?:'desc'
	//		params.max = Math.min(params.max ? params.int('max') : 15, 15)
	//		params.offset = params.offset? params.offset.toInteger():0
	//	}



}
