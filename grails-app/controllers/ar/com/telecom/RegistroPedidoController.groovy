package ar.com.telecom

import grails.validation.ValidationException

import org.codehaus.groovy.grails.web.servlet.mvc.SynchronizerToken

import ar.com.telecom.exceptions.BusinessException
import ar.com.telecom.pcs.entities.TipoAnexo
import ar.com.telecom.pcs.entities.TipoReferencia
import ar.com.telecom.pedido.AprobarPedidoUI

class RegistroPedidoController extends WorkflowController {

	def webUserAgentService
	
	def index = {
		if (webUserAgentService.isBlackberry()) {
			redirect(controller:"inboxMobile", action: "index")
		} else {
			def usuario = umeService.getUsuarioLogueado()
			params.usuarioLogueado = usuario
			def pedido = llenarDatosPedido(params, true)
	
			if(!pedido.puedeEditarIngreso(usuario)){
				redirect(action: "show", params: params)
				return
			}
	
			def result = [pedidoInstance: pedido]
			result.putAll(getMapaDatosPedido())
			result
		}
	}
	
	def nuevoPedido = {
		redirect(action: "index", params: params)
	}

	def validarFormulario = {
		withForm {
			def pedido
			def result
			try {
				pedido = llenarDatosPedido(params, false)
				def aprobacionUI = llenarAprobacionUI(pedido)
				result = pedidoService.validarPedido(params, aprobacionUI)
			} catch (ValidationException e) {
				result = false
			}
			mostrarResultados(result, "El pedido est\u00E1 ok", pedido, false)
		}.invalidToken { throw new BusinessException("Operaci\u00F3n no v\u00E1lida. Por favor vuelva a intentarlo.") }
	}

	def cancelarPedido = {
		withForm {
			def pedido
			try {
				pedido = llenarDatosPedido(params, false)
				def ok = getPedidoService().cancelarPedido(params, pedido)
				log.info "Cancelar pedido: " + ok
				if (ok) {
					flash.message = "Pedido cancelado"
					redirect(action: "list", controller: "inbox")
				}
			} catch (ValidationException e) {
				mostrarResultados(false, "Pedido cancelado", pedido, false)
			}
		}.invalidToken { throw new BusinessException("Operaci\u00F3n no v\u00E1lida. Por favor vuelva a intentarlo.") }
	}

	def finalizarFormulario = {
		withForm {
			def pedido
			def result
			try {
				pedido = llenarDatosPedido(params, false)
				def aprobacionUI = llenarAprobacionUI(pedido)
				result = pedidoService.avanzarPedido(params, aprobacionUI)
			} catch (ValidationException e) {
				result = false
			}
			mostrarResultados(result, "El pedido pas\u00F3 a la fase " + pedido.faseActual, pedido, true)
		}.invalidToken { throw new BusinessException("Operaci\u00F3n no v\u00E1lida. Por favor vuelva a intentarlo.") }
	}

	def obtenerReferenciasOrigen = {
		withForm{
			try{
				def tipoReferencia = TipoReferencia.get(params.referencia)
				def pedido = llenarDatosPedido(params, false)
				render (template: "/templates/referenciaOrigen", model:[tipoReferencia: tipoReferencia.descripcion, pedidoID: pedido?.id, pedidoInstance: pedido, token:SynchronizerToken.store(session).currentToken])
			} catch (Exception e) {
				procesarErrorAjax(e)
			}
		}.invalidToken{ throw new BusinessException("Operaci\u00F3n no v\u00E1lida. Por favor vuelva a intentarlo.") }
	}

	def guardarFormulario = {
		withForm {
			def pedido
			def result
			try {
				pedido = llenarDatosPedido(params, false)
				result = pedidoService.guardar(params, pedido)
			} catch (ValidationException e) {
				result = false
			}

			mostrarResultados(result, "Pedido guardado" , pedido, false)
		}.invalidToken { throw new BusinessException("Operaci\u00F3n no v\u00E1lida. Por favor vuelva a intentarlo.") }
	}

	def mostrarResultados(ok, mensajeOk, pedido, finaliza) {
		if (ok) {
			flash.message = mensajeOk
		}
		if (ok && finaliza) {
			faseFinalizada(pedido)
		} else {
			// Si hay errores, que vuelva al index
			def modelView = [pedidoInstance: pedido]
			modelView.putAll(getMapaDatosPedido())
			render(view: "index", model: modelView)
		}
	}

	/** Definiciones privadas de metodos */
	private def llenarAprobacionUI(pedido) {
		new AprobarPedidoUI(legajoInterlocutorUsuario: params.legajoInterlocutorUsuario, aprueba: true, pedido: pedido)
	}

	private def getMapaDatosPedido(pedido) {
		//gerente del usuario logeado y lista de gerentes disponibles
		def personUsername  = getUmeService().getUsuarioLogueado()
		def userGte = getUmeService().getGerente(personUsername)

		//interlocutores usuarios de la direccion
		def parametros = Constantes.instance.parametros
		def personsAutocompleteGte = []//getUmeService().getGerentesUsuarios()
		def personsAutocompleteInt = getUmeService().getUsuariosGrupoLDAP(parametros.grupoLDAPInterlocutorUsuario, userGte?.legajo)

		def tiposDeAnexo = TipoAnexo.findAllByFase(FaseMultiton.getFase(FaseMultiton.REGISTRACION_PEDIDO))

		return [personsAutocompleteGte:personsAutocompleteGte,
			personsAutocompleteInt:personsAutocompleteInt,
			userGte:userGte,
			tiposDeAnexo:tiposDeAnexo,
			usuarioLogueado: personUsername,
			gerenteITMsg: parametros.cartelAdvertenciaGerentesIT]
	}

	def popupAnexo = {
		log.info "popupAnexo: ${params}"
		withForm {
			try{
				def pedido = llenarDatosPedido(params, true)
				def tiposDeAnexo = TipoAnexo.findAllByFase(FaseMultiton.getFase(FaseMultiton.REGISTRACION_PEDIDO))
				render (template: "/templates/anexoPorTipo", model:[impacto:params?.impacto, pedidoInstance:pedido, tabSeleccionado:params?.tabSeleccionado, tiposDeAnexo:tiposDeAnexo, anexoFrom: params?.anexoFrom])
			} catch (Exception e) {
				procesarErrorAjax(e)
			}
		}.invalidToken { throw new BusinessException("Operaci\u00F3n no v\u00E1lida. Por favor vuelva a intentarlo.") }
	}

	/*def completarUsuario = {
		withForm {
			try{
				throw new BusinessException("Chatruc hizo un gol!")
			} catch (Exception e) {
				//response.sendError org.codehaus.groovy.grails.commons.ConfigurationHolder.config.errorAjax, e.message
				procesarErrorAjax(e)
			}
		}.invalidToken { throw new BusinessException("Operaci\u00F3n no v\u00E1lida. Por favor vuelva a intentarlo.") }
	}*/
	
}
