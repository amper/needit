package ar.com.telecom

import grails.converters.JSON

class ErrorAppController {

	def index = {
		def systemError = params.type?.toInteger().equals(ErrorTagLib.ERROR_CODE)
		[systemError: systemError]
	}

	def errorRole = {
		//render (view: '/error', model: [exception: request["exception"]])
		//redirect(controller: 'logout')
		session.invalidate()
		render (view: "indexSinRol")
	}

	def errorAjaxError = {
		errorAjax("Error de sistema", ErrorTagLib.ERROR_CODE)
	}

	def errorAjaxWarn = {
		errorAjax("Error de negocio", ErrorTagLib.WARNING_CODE)
	}

	def errorAjaxMsg = {
		errorAjax("Error", ErrorTagLib.MSG_CODE)
	}

	def buildParametros (title, msg, type){
		def parametros = [:]
		parametros.msg = msg
		parametros.title = title
		parametros.type = type
		parametros
	}

	def errorAjax(tipoError, contenido) {
		def mensaje = request.'javax.servlet.error.message'
		log.error "errorAjax: ${params} "
		log.error "Mensaje de error: " + mensaje
		render (buildParametros(tipoError, mensaje, contenido) as JSON)
	}
}
