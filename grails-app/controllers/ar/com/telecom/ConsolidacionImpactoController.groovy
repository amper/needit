package ar.com.telecom

import ar.com.telecom.exceptions.BusinessException
import ar.com.telecom.exceptions.ConcurrentAccessException
import ar.com.telecom.pcs.entities.CargaHora
import ar.com.telecom.pcs.entities.DetalleEstrategiaPrueba
import ar.com.telecom.pcs.entities.DetallePlanificacionAreaSoporte
import ar.com.telecom.pcs.entities.DetallePlanificacionSistema
import ar.com.telecom.pcs.entities.EstrategiaPrueba
import ar.com.telecom.pcs.entities.Justificacion
import ar.com.telecom.pcs.entities.JustificacionDisenioExterno
import ar.com.telecom.pcs.entities.OtroCosto
import ar.com.telecom.pcs.entities.Periodo
import ar.com.telecom.pcs.entities.PlanificacionEsfuerzo
import ar.com.telecom.pcs.entities.Prioridad
import ar.com.telecom.pcs.entities.SoftwareFactory
import ar.com.telecom.pcs.entities.TareaSoporte
import ar.com.telecom.pcs.entities.TipoAnexo
import ar.com.telecom.pcs.entities.TipoCosto
import ar.com.telecom.pcs.entities.TipoImpacto
import ar.com.telecom.pcs.entities.TipoPrueba
import ar.com.telecom.pedido.AprobacionUI
import ar.com.telecom.pedido.especificacionImpacto.Impacto
import ar.com.telecom.util.CheckUtil
import ar.com.telecom.util.DateUtil
import ar.com.telecom.util.NumberUtil

class ConsolidacionImpactoController extends WorkflowController {
	def capacidadService
	def webUserAgentService
	
	def index = {
		if (webUserAgentService.isBlackberry()) {
			redirect(controller:"inboxMobile", action: "index")
		} else {
			if (!params.impacto) {
				params.impacto = Impacto.IMPACTO_CONSOLIDADO
			}
			def pedido = llenarDatosPedido(params, true)
			if (pedido.tieneUnicoImpacto()) {
				params.impacto = "" + pedido.impactos.first().id
			}
	
			def result = [pedidoInstance: pedido, resultOtrosCostos:pedido.generarTablaOtrosCostos(), resultAreaSoportes:pedido.generarTablaAreaSoporte() ]
			result.putAll(getMapaDatosPedido(pedido))
			result
		}
	}
	
	def completaGrupoRSWF = {
		withForm {
			try{
				def pedido = llenarDatosPedido(params, true)
				redirect (action: "index", id: pedido.id, params: [pedidoId: pedido.id, impacto: params.impacto])
			} catch (Exception e) {
				procesarErrorAjax(e)
			}
		}.invalidToken {
			throw new BusinessException("Operaci\u00F3n no v\u00E1lida. Por favor vuelva a intentarlo.")
		}
	}

//	def eliminarAnexo = {
//		withForm {
//			try{
//				def pedidoATrabajar = getPedidoATrabajar(params)
//				def origen
//				
//				if (params.anexoFrom.equalsIgnoreCase("EP")) {
//					origen = pedidoATrabajar.estrategiaPruebaActual
//				} else if (params.anexoFrom.equalsIgnoreCase("DE")) {
//					origen = pedidoATrabajar.getDisenioExternoActual()
//				}
//				anexoService.eliminarAnexo(params, pedidoATrabajar, params.anexoId, origen)
//				render(template: "anexoTablaCI", model:[listAnexos: origen.anexos.sort{it.id}, pedidoId: params.pedidoId, impacto: params.impacto, anexoFrom: params.anexoFrom, usuarioLogueado: umeService.usuarioLogueado] + getImpacto(pedidoATrabajar))
//				
//			} catch (Exception e) {
//				procesarErrorAjax(e)
//			}
//		}.invalidToken {
//			throw new BusinessException("Operaci\u00F3n no v\u00E1lida. Por favor vuelva a intentarlo.")
//		}
//	}
	
	def agregarOtroCosto = {
		withForm { 
			try{
				def pedido = llenarDatosPedido(params, true)
				def pedidoATrabajar = getPedidoATrabajar(params)
				def otroCosto = new OtroCosto()
				bindData(otroCosto, params)
				if (otroCosto.validar(pedidoATrabajar)) {
					pedidoATrabajar.agregarOtroCosto(otroCosto)
					def aprobacionUI = generarAprobacionUI(pedidoATrabajar, true)
					pedidoService.guardarPedido(params, aprobacionUI)
					//pedidoService.guardar(params, pedidoATrabajar)
					flash.message = "Se agreg\u00F3 el costo"
					otroCosto = new OtroCosto()
				} 
				def tiposCosto = TipoCosto.list()
				tiposCosto.removeAll(pedidoATrabajar.planificacionActual.otrosCostos.tipoCosto)
				
				render (template: "otrosCostos", model:[pedido: pedido, pedidoATrabajar: pedidoATrabajar, impacto: params.impacto, tiposCosto: tiposCosto, otroCosto: otroCosto, usuarioLogueado: umeService.usuarioLogueado] + getImpacto(pedidoATrabajar))
			} catch (Exception e) {
				procesarErrorAjax(e)
			}
		}.invalidToken {
			throw new BusinessException("Operaci\u00F3n no v\u00E1lida. Por favor vuelva a intentarlo.")
		}
	}

	def agregarOtraPrueba = {
//		log.info "agregarOtraPrueba: ${params}"
		withForm {
			try{
				def pedidoATrabajar = getPedidoATrabajar(params)
				def pedido = pedidoATrabajar.parent
				def estrategiaPrueba = pedidoATrabajar.estrategiaPruebaActual
				def detallePrueba = new DetalleEstrategiaPrueba()
				detallePrueba.seRealiza = true
				detallePrueba.seConsolida = false
				bindData(detallePrueba, params)
				if (detallePrueba.validar(pedidoATrabajar)) {
					log.info "Detalle: " + detallePrueba.toString()
					estrategiaPrueba.addToDetalles(detallePrueba)
					def aprobacionUI = generarAprobacionUI(pedidoATrabajar, true)
					pedidoService.guardarPedido(params, aprobacionUI)
					//pedidoService.guardar(params, params, pedidoATrabajar)
					flash.message = "Se agreg\u00F3 el tipo de prueba"
					detallePrueba = new DetalleEstrategiaPrueba()
				}
				def tiposPrueba = getOtrasPruebas(pedidoATrabajar)
				render (template: "popupCargaOtrasPruebas", model:[pedidoInstance: pedido, pedidoATrabajar: pedidoATrabajar, impacto: params.impacto, otrasPruebas: tiposPrueba, detallePrueba: detallePrueba, estrategiaPrueba:estrategiaPrueba, usuarioLogueado: umeService.usuarioLogueado] + getImpacto(pedidoATrabajar))
			} catch (Exception e) {
				procesarErrorAjax(e)
			}
		}.invalidToken {
			throw new BusinessException("Operaci\u00F3n no v\u00E1lida. Por favor vuelva a intentarlo.")
		}
	}

	def eliminarOtraPrueba = {
		withForm {
			try{
				def pedidoATrabajar = getPedidoATrabajar(params)
				def detallePrueba = DetalleEstrategiaPrueba.findById(params.detalleId)
				def estrategia = pedidoATrabajar.estrategiaPruebaActual
				if (detallePrueba){
					estrategia.removeFromDetalles(detallePrueba)
					generarAprobacionId(pedidoATrabajar, umeService.usuarioLogueado)
					pedidoService.actualizarEstrategia(params, generarAprobacionUI(pedidoATrabajar, true), estrategia)
				}else{
					throw new ConcurrentAccessException("Al intentar grabar el pedido hubo otro usuario que lo modific\u00F3. Por favor ingrese nuevamente.")
				}
				def tiposPrueba = getOtrasPruebas(pedidoATrabajar)
				render (template: "tablaTiposPrueba", model:[pedidoInstance: pedidoATrabajar.parent, pedidoHijo: pedidoATrabajar, pedidoATrabajar: pedidoATrabajar, estrategiaPrueba: estrategia, otrasPruebas: tiposPrueba, impacto: params.impacto, usuarioLogueado: umeService.usuarioLogueado] + getImpacto(pedidoATrabajar))
			} catch (Exception e) {
				procesarErrorAjax(e)
			}
		}.invalidToken {
			throw new BusinessException("Operaci\u00F3n no v\u00E1lida. Por favor vuelva a intentarlo.")
		}
	}


	def recargarComboCostos = {
		withForm {
			try{
				log.info "recargarComboCostos: $params"
				def tiposCosto = TipoCosto.list()
				def pedidoATrabajar = getPedidoATrabajar(params)
		
				// Si elimina un pedido
				if (params.costoEliminado) {
					// Busco el Costo Eliminado
					def costoEliminado = OtroCosto.findById(new Integer(params.costoEliminado))
					// Loopeo entre los Costos de la Planificacion Actual
					pedidoATrabajar.planificacionActual.otrosCostos.tipoCosto.each { elemento ->
						if (!elemento.equals(costoEliminado.tipoCosto)) {
							tiposCosto.remove(elemento)
						}
					}
				}
				render (template: "comboTiposCostos", model:[tiposCosto: tiposCosto])
			} catch (Exception e) {
				procesarErrorAjax(e)
			}
		}.invalidToken {
			throw new BusinessException("Operaci\u00F3n no v\u00E1lida. Por favor vuelva a intentarlo.")
		}
	}

	def recargaOtrosCostos = {
		withForm {
			try{
				def pedidoATrabajar = getPedidoATrabajar(params)
				render (template: "tablaOtrosCostos", model:[otrosCostos: pedidoATrabajar.planificacionActual.otrosCostos, totalCostos: pedidoATrabajar.planificacionActual.totalMontoOtrosCostos(), pedidoHijo: pedidoATrabajar, impacto:params.impacto, planificaYPuedeEditar:true, usuarioLogueado: umeService.usuarioLogueado] + getImpacto(pedidoATrabajar))
			} catch (Exception e) {
				procesarErrorAjax(e)
			}
		}.invalidToken {
			throw new BusinessException("Operaci\u00F3n no v\u00E1lida. Por favor vuelva a intentarlo.")
		}
	}

	def obtenerHorasDefault = {
		withForm {
			try{
				def grupoSWF = null
				def cargaHora = new CargaHora()
				def valorHora = null
				bindData(cargaHora, params)
				if (params.idSWF) {
					grupoSWF = SoftwareFactory.findById(Integer.parseInt(params.idSWF))
					 
					def periodo = null
					if(!params.idPeriodo?.equals("")){
						periodo = Periodo.findById(Integer.parseInt(params.idPeriodo))
					}
					
					def trabajoSobreHijo = Impacto.aplicaAHijo(params.impacto)
					if (trabajoSobreHijo){
						valorHora = grupoSWF?.valorDefaultHora(periodo)
					}
				} else {
					// no tiene sentido
					// si viene por AS / AF es por popCargaTarea, así que no aplica
				}
				cargaHora.valorHora = valorHora
			render (template: "valorHora", model: [cargaHora: cargaHora])
			} catch (Exception e) {
				procesarErrorAjax(e)
			}
		}.invalidToken {
			throw new BusinessException("Operaci\u00F3n no v\u00E1lida. Por favor vuelva a intentarlo.")
		}
	}


	def eliminaOtroCosto = {
		withForm {
			try {
				def otroCostoId = params.otroCostoId as int
				def pedidoATrabajar = getPedidoATrabajar(params)
				def otroCosto = pedidoATrabajar.planificacionActual.otrosCostos.find { it.id == otroCostoId} 
				def aprobacionUI = generarAprobacionUI(pedidoATrabajar, true)
				if (otroCosto){
					pedidoATrabajar.eliminarOtroCosto(otroCosto)
					pedidoService.eliminarOtroCosto(params, aprobacionUI, otroCosto)
				}else{
					throw new ConcurrentAccessException("Al intentar grabar el pedido hubo otro usuario que lo modific\u00F3. Por favor ingrese nuevamente.")
				}
				render (template: "tablaOtrosCostos", model:[otrosCostos: pedidoATrabajar.planificacionActual.otrosCostos, impacto:params.impacto, pedidoHijo:pedidoATrabajar, totalCostos: pedidoATrabajar.planificacionActual.totalMontoOtrosCostos(), planificaYPuedeEditar: true, usuarioLogueado: umeService.usuarioLogueado] + getImpacto(pedidoATrabajar))
			} catch (Exception e) {
				procesarErrorAjax(e)
			}
		}.invalidToken {
			throw new BusinessException("Operaci\u00F3n no v\u00E1lida. Por favor vuelva a intentarlo.")
		}
	}

	def agregaCargaHorasSistemas = {
		withForm {
			try{
				def pedido = llenarDatosPedido(params, false)
				def pedidoATrabajar = getPedidoATrabajar(params)
				def cargaHora = new CargaHora()
				// Valido cantidad y valor hora.
				try{
					bindData(cargaHora, params)
					
				}catch(NumberFormatException e){
					pedido.errors.reject "Cantidad y valor hora deben ser valores numericos"
				}
				
	
				def detalle = DetallePlanificacionSistema.findById(Integer.parseInt(params.detalleId))
				//def detalle = pedidoATrabajar.planificacionActual.detalles.find { it.id.equals(params.detalleId) }
				if (!detalle) {
					return 
				}
				
				if (detalle.tieneCargaHoraRepetida(cargaHora)){
					pedido.errors.reject "Planificaci\u00F3n y esfuerzo - carga de horas de " + cargaHora + ": Ya existe una carga de hora para ese grupo y per\u00EDodo"
				}
	
				if (cargaHora.validar(pedidoATrabajar) && pedido.errors.errorCount == 0){
					detalle.agregarCargaHora(cargaHora)
					// no grabo, dejo que lo haga cargarHorasSistema()
					flash.message = "Se agreg\u00F3 la carga de hora"
				} else {
					params.cargaHora = cargaHora
				}
				cargarHorasSistema(pedidoATrabajar, detalle, params)
			} catch (Exception e) {
				procesarErrorAjax(e)
			}
		}.invalidToken {
			throw new BusinessException("Operaci\u00F3n no v\u00E1lida. Por favor vuelva a intentarlo.")
		}
	}
	
	private def cargarHorasSistema(pedido, detalle, params) {
		def pedidoATrabajar = getPedidoATrabajar(params)
		boolean actualizaDetalle = true
		if (params.show) {
			actualizaDetalle = !params.show.toBoolean()
		}
		if (actualizaDetalle){
			pedidoService.actualizarDetalle(params, generarAprobacionUI(pedidoATrabajar, true), detalle)
		}
		//pedidoService.guardarPedido(params, generarAprobacionUI(pedidoATrabajar, true))
		
		def softwareFactories = pedido.softwareFactories()
		def periodos = capacidadService.getPeriodos(detalle.fechaDesde, detalle.fechaHasta)
		def planificacionActual = PlanificacionEsfuerzo.findById(Integer.parseInt(params.idPlanificacion))
		def detallesAEditar = planificacionActual.detallesAEditar()
		def indiceDetalleAEditar = detallesAEditar.indexOf(detalle)
		def esElPrimero = (indiceDetalleAEditar==0)
		def esElUltimo = (indiceDetalleAEditar==(detallesAEditar.size()-1))
		
		render (template: "bodyCargaHoras", 
			model:[gruposSWF: softwareFactories, periodos: periodos, detalle : detalle, planificacionActual: planificacionActual, 
			esElPrimero:esElPrimero, esElUltimo:esElUltimo, pedidoATrabajar: params.pedidoATrabajar?:params.pedidoId, cargaHora:params.cargaHora, 
			impacto:params.impacto, pedido:pedido, show: params?.show ?: false, usuarioLogueado: umeService.usuarioLogueado] + getImpacto(pedidoATrabajar))
	}
	
	def popCargaHorasSistema = {
//		withForm {
			def pedidoATrabajar = getPedidoATrabajar(params)
			generarAprobacionId(pedidoATrabajar, umeService.usuarioLogueado)

			def detalle = DetallePlanificacionSistema.findById(Integer.parseInt(params.idDetalle))
			
			// Actualizo fecha desde y hasta del detalle(la validacion esta en el JS pero aca la hago PLD)
			if(params.fechaDesde){
				detalle.fechaDesde = DateUtil.toDate(params.fechaDesde)
			}
	
			if(params.fechaHasta){
				detalle.fechaHasta = DateUtil.toDate(params.fechaHasta)
			}
			
			cargarHorasSistema(pedidoATrabajar, detalle, params)
//		}.invalidToken {
//			throw new BusinessException("Operaci\u00F3n no v\u00E1lida. Por favor vuelva a intentarlo.")
//		}
	}

	def limpiaDetallePlanificacion = {
		withForm {
			try{
				def pedidoATrabajar = getPedidoATrabajar(params)
				def detalle = DetallePlanificacionSistema.findById(Integer.parseInt(params.detalleId))
				detalle.blanquear()
				pedidoService.actualizarDetalle(params, generarAprobacionUI(pedidoATrabajar, true), detalle)
		
				render (template: "filaDetallePlanificacion", model:[detalle: detalle, pedidoHijo:pedidoATrabajar, planificacionActual: pedidoATrabajar.planificacionActual, impacto: params.impacto, usuarioLogueado: umeService.usuarioLogueado] + getImpacto(pedidoATrabajar))
			} catch (Exception e) {
				procesarErrorAjax(e)
			}
		}.invalidToken {
			throw new BusinessException("Operaci\u00F3n no v\u00E1lida. Por favor vuelva a intentarlo.")
		}
	}

	def actualizaFilaDetallePlanificacionSist = {
		withForm { 
			try{
				def pedidoATrabajar = getPedidoATrabajar(params)
				def detalle = DetallePlanificacionSistema.findById(Integer.parseInt(params.detalleId))
				render (template: "filaDetallePlanificacion", model:[detalle: detalle, planificacionActual: PlanificacionEsfuerzo.findById(Integer.parseInt(params.idPlanificacion)), impacto:params.impacto, pedidoHijo: pedidoATrabajar, usuarioLogueado: umeService.usuarioLogueado] + getImpacto(pedidoATrabajar))
			} catch (Exception e) {
				procesarErrorAjax(e)
			}
		}.invalidToken {
			throw new BusinessException("Operaci\u00F3n no v\u00E1lida. Por favor vuelva a intentarlo.")
		}
	}

	def eliminaCargaHoraDetalleSistema = {
		withForm {
			try {
				def pedidoATrabajar = getPedidoATrabajar(params)
				def detalleId = params.detalleId as int
				def cargaHoraId = params.cargaHoraId as int
				def detalle = DetallePlanificacionSistema.findById(detalleId)
				//def cargaHora = detalle.cargaHoras.get(Integer.parseInt(params.pos))
				def cargaHora = detalle.cargaHoras.find { it.id == cargaHoraId }
				if (cargaHora) {
					detalle.removeFromCargaHoras(cargaHora)
					//pedidoService.eliminarCargaHora(params, generarAprobacionUI(pedidoATrabajar, true), cargaHora)
					pedidoService.actualizarDetalle(params, generarAprobacionUI(pedidoATrabajar, true), detalle)
				}else{
					throw new ConcurrentAccessException("Al intentar grabar el pedido hubo otro usuario que lo modific\u00F3. Por favor ingrese nuevamente.")
				}
				
				render (template: "tablaHoras", model:[detalleHoras: detalle.cargaHoras, detalleId:detalle.id, idPlanificacion:params.idPlanificacion, impacto: params.impacto, pedidoATrabajar: params.pedidoATrabajar, show: false, usuarioLogueado: umeService.usuarioLogueado] + getImpacto(pedidoATrabajar))
			} catch (Exception e) {
				procesarErrorAjax(e)
			}
		}.invalidToken {
			throw new BusinessException("Operaci\u00F3n no v\u00E1lida. Por favor vuelva a intentarlo.")
		}
	}
	
	def popOtrosCostos = {
		withForm {
			try{
				def pedido = llenarDatosPedido(params, true)
				def pedidoATrabajar = getPedidoATrabajar(params)
				def tiposCosto = TipoCosto.list()
				tiposCosto.removeAll(pedidoATrabajar.planificacionActual.otrosCostos.tipoCosto)
				render (template: "otrosCostos", model:[pedido: pedido, pedidoATrabajar: pedidoATrabajar, impacto: params.impacto, tiposCosto: tiposCosto])
			} catch (Exception e) {
				procesarErrorAjax(e)
			}
		}.invalidToken {
			throw new BusinessException("Operaci\u00F3n no v\u00E1lida. Por favor vuelva a intentarlo.")
		}
	}
	
	def popCargaOtrasPruebas = {
		withForm {
			try{
				def pedidoATrabajar = getPedidoATrabajar(params)
				def pedido = pedidoATrabajar.parent
				def estrategiaPrueba = pedidoATrabajar.estrategiaPruebaActual
				def tiposPrueba = getOtrasPruebas(pedidoATrabajar)
				render (template: "popupCargaOtrasPruebas", model:[pedidoInstance: pedido, pedidoATrabajar: pedidoATrabajar, impacto: params.impacto, otrasPruebas: tiposPrueba, estrategiaPrueba:estrategiaPrueba, usuarioLogueado: umeService.usuarioLogueado] + getImpacto(pedidoATrabajar))
			} catch (Exception e) {
				procesarErrorAjax(e)
			}
		}.invalidToken {
			throw new BusinessException("Operaci\u00F3n no v\u00E1lida. Por favor vuelva a intentarlo.")
		}
	}
	
	def popCargaTarea = {
		log.info "popCargaTarea: ${params}"
		withForm {
			try{
				def pedidoATrabajar = getPedidoATrabajar(params)
				def tareas = getTareas(pedidoATrabajar)
				def valorHora = Constantes.instance.parametros.valorHoraSWFTecoDefault
				
				def grupo
				if(pedidoATrabajar.tieneAreaSoporte()){
					grupo = pedidoATrabajar?.areaSoporte?.grupoAdministrador
				}else{
					grupo = pedidoATrabajar?.sistema?.grupoAdministrador
				}
				render (template: "popupCargaTareas", model:[impacto: params.impacto, pedido: pedidoATrabajar, tareas: tareas, params:params, valorHora: NumberUtil.seteaComas(valorHora), grupo: grupo, usuarioLogueado: umeService.usuarioLogueado] + getImpacto(pedidoATrabajar))
			} catch (Exception e) {
				procesarErrorAjax(e)
			}
		}.invalidToken {
			throw new BusinessException("Operaci\u00F3n no v\u00E1lida. Por favor vuelva a intentarlo.")
		}
	}

	def recargaBotonera = {
		withForm {
			try{
				def pedidoATrabajar = getPedidoATrabajar(params)
				//reasignarAsignatario(pedidoATrabajar, params.legajoNuevo)
				def impactoInstance = new Impacto(!Impacto.aplicaAHijo(params.impacto)?params.impacto:Impacto.IMPACTO_HIJO, pedidoATrabajar)
				render (template: "botonera", model:[impacto: params.impacto, impactoInstance: impactoInstance, usuarioLogueado: umeService.usuarioLogueado, pedidoHijo: pedidoATrabajar])
			} catch (Exception e) {
				procesarErrorAjax(e)
			}
		}.invalidToken {
			throw new BusinessException("Operaci\u00F3n no v\u00E1lida. Por favor vuelva a intentarlo.")
		}
	}

	def reasignarAsignatario(pedido, legajoNuevo) {
		generarAprobacionId(pedido, legajoNuevo)
		pedido.legajoUsuarioResponsable = legajoNuevo
		def aprobacionUI = new AprobacionUI(aprueba: true, pedido: pedido, asignatario: legajoNuevo, usuario: legajoNuevo)
		pedidoService.guardarPedido(params, aprobacionUI)
	}

	def muevePopCargaHora = {
		withForm {
			try{
				def detalles =  params.detalles?.replace("[","").replace("]","").split(",") as List
				detalles = detalles.collect { det -> det.trim()}
				def min = 0
				def max = detalles.size() - 1
				def esElPrimero = false
				def esElUltimo = false
		
				// Obtengo posicion en listado del detalle actual
				def idx
				detalles.eachWithIndex { detalle,i ->
					if (detalle.equalsIgnoreCase(params.idDetalleActual)){
						idx = i
					}
				}
		
				// Evaluo si adelanto o retrocedo
				if(params.boton.equalsIgnoreCase("atras") && idx>min){
					idx--
					if(idx == min){
						esElPrimero = true
					}
				}
		
				if(params.boton.equalsIgnoreCase("adelante") && idx<max){
					idx++
					if(idx == max){
						esElUltimo = true
					}
				}
				
				def detalleFuturo = DetallePlanificacionSistema.findById(Integer.parseInt(detalles[idx]))
				def periodos = capacidadService.getPeriodos(detalleFuturo.fechaDesde, detalleFuturo.fechaHasta)
				def pedidoATrabajar = getPedidoATrabajar(params)
				def softwareFactories = pedidoATrabajar.softwareFactories()
				def impacto = new Impacto(params.impacto, pedidoATrabajar)
				log.info "softwareFactories: ${softwareFactories}"
				render (template: "bodyCargaHoras", 
					model:[gruposSWF: softwareFactories, periodos: periodos, detalle : detalleFuturo, 
					planificacionActual: PlanificacionEsfuerzo.findById(Integer.parseInt(params.idPlanificacionActual)), 
					esElPrimero:esElPrimero, esElUltimo:esElUltimo, pedidoATrabajar:pedidoATrabajar?.id, impacto:params.impacto, 
					pedido:pedidoATrabajar, show: params?.show ?: false, usuarioLogueado: umeService.usuarioLogueado] + getImpacto(pedidoATrabajar))
			} catch (Exception e) {
				procesarErrorAjax(e)
			}
		}.invalidToken {
			throw new BusinessException("Operaci\u00F3n no v\u00E1lida. Por favor vuelva a intentarlo.")
		}
	}

	def eliminarAnexo = {
		log.warn "eliminarAnexo: ${params}"
		def pedido = getPedidoATrabajar(params)// (params, true)
		def origen = pedido.getOrigen(params.anexoId)
		 
		generarAprobacionId(pedido, umeService.usuarioLogueado)
		anexoService.eliminarAnexo(params, pedido, params.anexoId)
		render (template: "/templates/tablaAnexos", model:[fase:params?.fase, 'editable': true, pedidoInstance:pedido, origen:origen, nombreDiv:params?.nombreDiv, impacto:params?.impacto])
	}
	
	/**
	 * Adapta los datos de un pedido
	 */
	def llenarDatosPedido(params, initial) {
		def usuarioLogueado = umeService.getUsuarioLogueado()
		
		if (initial) {
			params.pedidoVersion = null
			params.pedidoHijoVersion = null
		}
		
		def pedido
		if (params.pedidoId) {
			pedido = pedidoService.obtenerPedido(params, params.pedidoId)
		} else {
			throw new BusinessException("El pedido " + params.pedidoId + " no existe")
		}

		if (initial) {
			return pedido
		}

		def pedidoATrabajar = getPedidoATrabajar(params)
		def impacto = new Impacto(params.impacto, pedidoATrabajar)
		if (Impacto.aplicaAHijo(params.impacto)) {
			bindData(pedidoATrabajar, params)
		}
		
		if (params.fechaVencimientoPlanificacion) {
			pedidoATrabajar.parent.fechaVencimientoPlanificacion = DateUtil.toDate(params.fechaVencimientoPlanificacion) 
		}
		
		if (params.prioridad && params.prioridad?.id != 'null'){
			pedidoATrabajar.parent.prioridad = Prioridad.findById(params.prioridad.id) 
		}else{
			pedidoATrabajar.parent.prioridad = null
		}
		
		if (impacto.puedeEditar(usuarioLogueado)) {
			def planificacion = pedidoATrabajar.planificacionActual
			planificacion?.detalles.each { detalle ->
				def campoDesde = params.get("desde_" + detalle.id)
				if (campoDesde) {
					detalle.fechaDesde = DateUtil.toDate(campoDesde)
				}
				def campoHasta = params.get("hasta_" + detalle.id)
				if (campoHasta) {
					detalle.fechaHasta = DateUtil.toDate(campoHasta)
				}
			}
			bindData(planificacion, params)
			if (params.fechaRelease) {
				planificacion.fechaRelease = DateUtil.toDate(params.fechaRelease)
			}


			if (params.cargaEstrategiaPruebas?.equalsIgnoreCase("S")) {
				def estrategiaPrueba = pedidoATrabajar.estrategiaPruebaActual
				if (estrategiaPrueba) {
					bindData(estrategiaPrueba, params)
	
					estrategiaPrueba?.detalles.each { detalle ->
						Boolean seRealiza = CheckUtil.getValue(params, "chkRealiza_" + detalle.id)
						detalle.seRealiza = seRealiza
						Boolean seConsolida = CheckUtil.getValue(params, "chkConsolida_" + detalle.id)
						detalle.seConsolida = seConsolida
						//println "chkRealiza_" + detalle.id + ": " + seRealiza + "|" + detalle.seRealiza
						//println "chkConsolida_" + detalle.id + ": " + seConsolida + "|" + detalle.seConsolida
						def justificacion = params.get("comboJustificacionNoPrueba_" + detalle?.id)
						if (!justificacion || detalle.seRealiza) {
							detalle.justificacionNoRealizacion = null
						} else {
							detalle.justificacionNoRealizacion = Justificacion.findById(justificacion)
						}
						detalle.ajustarDetallePrueba()
					}
	
				}
			}

			def disenioExterno = pedidoATrabajar.getDisenioExternoActual()
			if (disenioExterno) {
				bindData(disenioExterno, params)
			}
		}

		def anexo
		if (params.anexoArchivo?.size > 0){
			anexo = getAnexo(pedido, usuarioLogueado, params)
			
			def estrategiaPrueba = pedidoATrabajar.estrategiaPruebaActual
			def disenioExterno = pedidoATrabajar.getDisenioExternoActual()
			log.info "Anexo from: ${params.anexoFrom}"
			if (anexo){
				if (params.anexoFrom.equalsIgnoreCase("EP")){
					log.info "entro a agregar anexo a estrategia"
					estrategiaPrueba.addToAnexos(anexo)
				}else if (params.anexoFrom.equalsIgnoreCase("DE")){
					log.info "entro a agregar anexo a disenio: ${disenioExterno}" 
					disenioExterno.addToAnexos(anexo)
				}
			}
			return pedido
		}
		return pedidoATrabajar
	}

	def guardarAnexo = {
		withForm {
			try{
				def pedido = getPedidoATrabajar(params) //pedidoService.obtenerPedido(params, params.pedidoId) 
				if (params.anexoArchivo?.size > 0){
					def usuarioLogueado = umeService.getUsuarioLogueado()
					def anexo = getAnexo(pedido, usuarioLogueado, params)
					def estrategiaPrueba = pedido.estrategiaPruebaActual
					def disenioExterno = pedido.disenioExternoActual
					if (anexo){
						if (params.anexoFrom.equalsIgnoreCase("EP")){
							estrategiaPrueba.addToAnexos(anexo)
						} else {
							if (params.anexoFrom.equalsIgnoreCase("DE")){
								disenioExterno.addToAnexos(anexo)
							}
						}
						def aprobacionUI = generarAprobacionUI(pedido, true)
						pedidoService.guardarPedido(params, aprobacionUI)
						//pedidoService.guardar(params, pedido)
					}
				}
				redirect(action: "index", controller: params.controller, params: [pedidoId: params.pedidoId, tabSeleccionado: params?.tabSeleccionado, impacto: params?.impacto])
			} catch (Exception e) {
				procesarErrorAjax(e)
			}
		}.invalidToken {
			throw new BusinessException("Operaci\u00F3n no v\u00E1lida. Por favor vuelva a intentarlo.")
		}
	}
	
	def popupAnexo = {
		withForm {
			try{
				def pedido = getPedidoATrabajar(params)
				def tiposDeAnexo = TipoAnexo.findAllByFase(pedido.faseActual)
				render (template: "/templates/anexoPorTipo", model:[impacto:params?.impacto, pedidoInstance:pedido.parent, tabSeleccionado:params?.tabSeleccionado, tiposDeAnexo:tiposDeAnexo, anexoFrom: params?.anexoFrom])
			} catch (Exception e) {
				procesarErrorAjax(e)
			}
		}.invalidToken { throw new BusinessException("Operaci\u00F3n no v\u00E1lida. Por favor vuelva a intentarlo.") }
	}
	
	def agregaTarea = {
		withForm { 
			try{
				def pedidoATrabajar = getPedidoATrabajar(params)
				def detalle = new DetallePlanificacionAreaSoporte()
	
				if (params.fechaDesde) {
					params.fechaDesde = DateUtil.toDate(params.fechaDesde)
				}
				if (params.fechaHasta) {
					params.fechaHasta = DateUtil.toDate(params.fechaHasta)
				}
				bindData(detalle, params)
	//					if(params.valorHora){
	//						detalle.valorHora = new BigDecimal(params.valorHora)
	//					}
	
				detalle.fechaDesde = DateUtil.toDate(params?.fechaDesde)
				detalle.fechaHasta = DateUtil.toDate(params?.fechaHasta)
	
				if (detalle.validar(pedidoATrabajar)){
					pedidoATrabajar.planificacionActual.agregarDetalle(detalle)
					def aprobacionUI = generarAprobacionUI(pedidoATrabajar, true)
					pedidoService.guardarPedido(params, aprobacionUI)
					//pedidoService.guardar(params, pedidoATrabajar)
					flash.message = "Se agreg\u00F3 la tarea"
					params.cantidadHoras = 0
					detalle = new DetallePlanificacionAreaSoporte()
				}
	
				def tareas = getTareas(pedidoATrabajar)
				def valorHora = Constantes.instance.parametros.valorHoraSWFTecoDefault
				render (template: "popupCargaTareas", model:[impacto: params.impacto, pedido: pedidoATrabajar, tareas: tareas, detalle: detalle, params:params, valorHora: NumberUtil.seteaComas(valorHora), grupo: params.grupo, usuarioLogueado: umeService.usuarioLogueado] + getImpacto(pedidoATrabajar))
			} catch (Exception e) {
				procesarErrorAjax(e)
			}
		}.invalidToken {
			throw new BusinessException("Operaci\u00F3n no v\u00E1lida. Por favor vuelva a intentarlo.")
		}
	}

	def recargaTablaTareas = {
		withForm { 
			try{
				def pedidoATrabajar = getPedidoATrabajar(params)
				def impacto = new Impacto(params.impacto, pedidoATrabajar)
				render (template: "tablaTareas", model:[detalles: pedidoATrabajar.planificacionActual.detalles.sort{it.id}, impacto: params.impacto, puedeEditar: true, usuarioLogueado: umeService.usuarioLogueado] + getImpacto(pedidoATrabajar))
			} catch (Exception e) {
				procesarErrorAjax(e)
			}
		}.invalidToken {
			throw new BusinessException("Operaci\u00F3n no v\u00E1lida. Por favor vuelva a intentarlo.")
		}
	}

	def eliminarTarea = {
		withForm {
			try{
				def pedidoATrabajar = getPedidoATrabajar(params)
				def planificacionActual = pedidoATrabajar.planificacionActual
				def detalle = planificacionActual.detalles.find { detalle -> detalle.id==params.detalleId.toLong() }
				if (detalle){
					planificacionActual.quitarDetalle(detalle)
					pedidoService.eliminarDetalle(params, generarAprobacionUI(pedidoATrabajar, true), detalle)
				}else{
					throw new ConcurrentAccessException("Al intentar grabar el pedido hubo otro usuario que lo modific\u00F3. Por favor ingrese nuevamente.")
				}
				def detalles = planificacionActual.detalles.sort{it.id}
				def impacto = new Impacto(params.impacto, pedidoATrabajar)
				render (template: "tablaTareas", model:[detalles: detalles, impacto: params.impacto, puedeEditar: true, usuarioLogueado: umeService.usuarioLogueado] + getImpacto(pedidoATrabajar))
			} catch (Exception e) {
				procesarErrorAjax(e)
			}
		}.invalidToken {
			throw new BusinessException("Operaci\u00F3n no v\u00E1lida. Por favor vuelva a intentarlo.")
		}
	}

	private def getMapaDatosPedido(pedido) {
		def pedidoATrabajar = getPedidoATrabajar(params)
		def grupoResponsable = pedidoATrabajar.grupoResponsable
		def usuariosResponsables = getUmeService().getUsuariosGrupoLDAP(grupoResponsable, null)
		def releases = new ArrayList()

		if (pedidoATrabajar.planificaActividadesSistema()) {
			releases = pedidoService.getReleases(pedidoATrabajar.getSistema())
		}
		def planificacionActual = pedidoService.getPlanificacionActual(pedidoATrabajar)
		def estrategiaPrueba = pedidoService.getEstrategiaPruebaActual(pedidoATrabajar)
		def disenioExterno = pedidoService.getDisenioExternoActual(pedidoATrabajar)
		def usuarioLogueado = umeService.getUsuarioLogueado()
		// Determino si el impacto aplica sobre el hijo
		def trabajoSobreHijo = Impacto.aplicaAHijo(params.impacto)
		def trabajaSobrePadre = Impacto.aplicaAPadre(params.impacto)
		def trabajaSobreCoord = Impacto.aplicaACoordinacion(params.impacto)
		def impactoInstance = new Impacto(!Impacto.aplicaAHijo(params.impacto)?params.impacto:Impacto.IMPACTO_HIJO, pedidoATrabajar)
		def todosLosTiposCostos = TipoCosto.list()
		def selectedTiposCostos = planificacionActual.otrosCostos.tipoCosto.toList()
		todosLosTiposCostos.removeAll(selectedTiposCostos)

		boolean reasigna = pedidoATrabajar.esHijo() && !pedidoATrabajar.legajoUsuarioResponsable && umeService.usuarioPerteneceGrupo(grupoResponsable, usuarioLogueado)
		
		if (impactoInstance.inicializaAsignatario(usuarioLogueado) || reasigna){
			reasignarAsignatario(pedidoATrabajar, usuarioLogueado)
		}
		
		def tareas = getTareas(pedidoATrabajar)
		def valorHora = Constantes.instance.parametros.valorHoraSWFTecoDefault

		def tablaOtrosCostos
		def tablaAreaSoporte
		if (!pedido.esHijo()){
			tablaOtrosCostos =  pedido.generarTablaOtrosCostos()
			tablaAreaSoporte = pedido.generarTablaAreaSoporte()
		}
		
		[
			personsAutocompleteAsign: usuariosResponsables,
			grupo: grupoResponsable,
			planificacionActual: planificacionActual,
			estrategiaPrueba: estrategiaPrueba,
			releases: releases,
			tiposCosto: todosLosTiposCostos,
			justificacionesDE: JustificacionDisenioExterno.list(),
			usuarioLogueado: usuarioLogueado,
			pedidoHijo: pedidoATrabajar,
			trabajoSobreHijo: trabajoSobreHijo,
			trabajaSobrePadre: trabajaSobrePadre,
			trabajaSobreCoord: trabajaSobreCoord,
			impacto: params.impacto,
			disenioExterno: disenioExterno,
			impactoInstance : impactoInstance,
			tareas: tareas,
			valorHora: NumberUtil.seteaComas(valorHora),
			resultOtrosCostos: tablaOtrosCostos,
			resultAreaSoportes: tablaAreaSoporte,
			otrasPruebas: getOtrasPruebas(pedidoATrabajar),
			consolidacionList : pedidoService.planificacionConsolidada(pedidoATrabajar.parent, impactoInstance)
			]
	}

	private getTareas(pedidoATrabajar){
		def tareas
		if (pedidoATrabajar.esHijo()) {
			if (pedidoATrabajar.areaSoporte) {
				pedidoATrabajar.tipoImpacto = TipoImpacto.findByDescripcion("Area de soporte")
				tareas = TareaSoporte.findAllByAreaSoporte(pedidoATrabajar.areaSoporte)
			}else{
				tareas = TareaSoporte.findAllWhere(areaSoporte: null)
			}
		}
	}

	def mostrarResultados(ok, mensajeOk, aprobacionUI, finaliza) {
		def pedido = aprobacionUI.pedido
		
		if (ok) {
			flash.message = mensajeOk
		}
		if (ok && finaliza) {
			faseFinalizada(pedido)
		}else{
			// Si hay errores, que vuelva al index
			def modelView = [pedidoInstance: pedido.parent, justificacionAprobacion: params.justificacionAprobacion, tabSeleccionado: params?.tabSeleccionado] 
			modelView.putAll(getMapaDatosPedido(pedido))
			render(view: "index", model: modelView)
		}
	}

	def generarAprobacionUI(aprueba) {
		def pedido = llenarDatosPedido(params, false)
		def asignatario
		if (pedido.esHijo()) {
			asignatario = params.legajoUsuarioResponsable
		} else {
			asignatario = params.legajoCoordinadorCambio
		}
		def result = new AprobacionUI(justificacion: params.justificacionAprobacion, aprueba: aprueba, pedido: pedido, asignatario: asignatario, usuario: umeService.getUsuarioLogueado())
		return result
	}
	
	def generarAprobacionUI(pedido, aprueba) {
		// TODO: Llevar el legajoUsuarioResponsable
		// PREGUNTAR A NICO/GATO
		return new AprobacionUI(justificacion: params.justificacionAprobacion, aprueba: aprueba, pedido: pedido, usuario: umeService.usuarioLogueado, asignatario: pedido?.legajoUsuarioResponsable)
	}
	
	def faseFinalizada(pedido) {
		render(view: "index", model: [pedidoInstance: pedido.parent] + getMapaDatosPedido(pedido))
	}


	def getOtrasPruebas(pedido) {
		def tiposPrueba = TipoPrueba.createCriteria().list {
			//			ne("descripcion", TipoPrueba.DESCRIPCION_PU)
			//			ne("descripcion", TipoPrueba.DESCRIPCION_PI)
			//			ne("descripcion", TipoPrueba.DESCRIPCION_PAU)
		}
		def existentes = pedido?.estrategiaPruebaActual?.detalles?.tipoPrueba
		if (existentes){
			log.info "Tipos de prueba: " + tiposPrueba
			log.info "Existentes: " + existentes
			tiposPrueba?.removeAll(existentes)
			log.info "Resultantes: " + tiposPrueba
		}
		return tiposPrueba
	}

	def recargaTablaOtrasPruebas = {
		withForm {
			try{
				def pedido = llenarDatosPedido(params, true)
				def estrategia = EstrategiaPrueba.findById(Integer.parseInt(params.estrategiaPruebaId))
				def pedidoATrabajar = getPedidoATrabajar(params)
				def impactoInstance = new Impacto(!Impacto.aplicaAHijo(params.impacto)?params.impacto:Impacto.IMPACTO_HIJO, pedidoATrabajar)
				def usuarioLogueado = umeService.getUsuarioLogueado()
		
				render (template: "tablaTiposPrueba", model:[estrategiaPrueba: estrategia, pedidoHijo:pedidoATrabajar, impactoInstance: impactoInstance, pedidoInstance:pedido , usuarioLogueado:usuarioLogueado])
			} catch (Exception e) {
				procesarErrorAjax(e)
			}
		}.invalidToken {
			throw new BusinessException("Operaci\u00F3n no v\u00E1lida. Por favor vuelva a intentarlo.")
		}
	}

	def grabaFechasDetalles = {
		withForm {
			try{
				def pedidoATrabajar = getPedidoATrabajar(params)
				
				String [] detalles = params.detalles.split(",")
				String [] fechasDesde = params.fechasDesde.split(",")
				String [] fechasHasta = params.fechasHasta.split(",")
				
				detalles.eachWithIndex { idDetalle, i ->
					def detalle = DetallePlanificacionSistema.findById(Integer.parseInt(idDetalle))
					
					// Actualizo fecha desde y hasta del detalle(la validacion esta en el JS pero aca la hago PLD)
					if(fechasDesde[i]){
						detalle.fechaDesde = DateUtil.toDate(fechasDesde[i])
					}
				
					if(fechasHasta[i]){
						detalle.fechaHasta = DateUtil.toDate(fechasHasta[i])
					}
					pedidoService.actualizarDetalle(params, generarAprobacionUI(pedidoATrabajar, true), detalle)
				}
				render (template: "/templates/token")
			} catch (Exception e) {
				procesarErrorAjax(e)
			}
		}.invalidToken {
			throw new BusinessException("Operaci\u00F3n no v\u00E1lida. Por favor vuelva a intentarlo.")
		}
	}	
	
	def mostrarPopUpReenviar = {
		log.info "mostrarPopUpReenviar: ${params}"
		withForm {
			try{
				def pedidoATrabajar = getPedidoATrabajar(params)
				def pedido = pedidoATrabajar.parent
				def impactoInstance = new Impacto(!Impacto.aplicaAHijo(params.impacto)?params.impacto:Impacto.IMPACTO_HIJO, pedidoATrabajar)
				
				render (template: "/consolidacionImpacto/reenviar", model:[pedidoHijo:pedidoATrabajar, impactoInstance: impactoInstance, pedido:pedido])
			} catch (Exception e) {
				procesarErrorAjax(e)
			}
		}.invalidToken {
			throw new BusinessException("Operaci\u00F3n no v\u00E1lida. Por favor vuelva a intentarlo.")
		}
	}
	
	def reenviar = {
		withForm {
			def pedidoATrabajar = getPedidoATrabajar(params)
			def pedido = pedidoATrabajar.parent
			
			def impactoInstance = new Impacto(!Impacto.aplicaAHijo(params.impacto)?params.impacto:Impacto.IMPACTO_HIJO, pedidoATrabajar)
			def usuarioLogueado = umeService.getUsuarioLogueado()
			
			if (pedidoATrabajar.estaSuspendido()){
				throw new ConcurrentAccessException("El pedido se encuentra suspendido")
			}
			try{
				pedidoATrabajar.reenviar(usuarioLogueado, params.observaciones)
				def aprobacionUI = generarAprobacionUI(pedidoATrabajar, true)
				pedidoService.guardarPedido(params, aprobacionUI, false, true)
				//pedidoService.guardar(params, pedidoATrabajar)
				
				flash.message = "Observaci\u00F3n agregada correctamente"
				redirect(action: "index", params: [pedidoId: pedido.id, impacto: impactoInstance.id] + getImpacto(pedidoATrabajar))
			} catch (Exception e) {
				procesarErrorAjax(e)
			}
		}.invalidToken {
			throw new BusinessException("Operaci\u00F3n no v\u00E1lida. Por favor vuelva a intentarlo.")
		}
	}

	
	def recargaCostoTotalCambio = {
		def pedidoATrabajar = getPedidoATrabajar(params)
		render (template: "costoTotalCambio", model: [usuarioLogueado: umeService.usuarioLogueado] + getImpacto(pedidoATrabajar))
	} 

}
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                 