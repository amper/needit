package ar.com.telecom

import ar.com.telecom.exceptions.BusinessException
import ar.com.telecom.pedido.AprobarPedidoUI


class ValidarPedidoController extends WorkflowController {
	/**
	 * Métodos del controller que usa la vista
	 */
	def webUserAgentService
	
	def index = {
		if (webUserAgentService.isBlackberry()) {
			redirect(controller:"inboxMobile", action: "index")
		} else {
			def pedido = llenarDatosPedido(params, true)
			def usuarioLogueado = umeService.getUsuarioLogueado()
			
			if(!pedido.puedeEditarAprobacion(usuarioLogueado)){
				redirect(action: "show", params: params)
				return
			}
	
			def parametros = Constantes.instance.parametros
			def personsAutocompleteInt = umeService.getUsuariosGrupoLDAP(parametros.grupoLDAPInterlocutorUsuario, pedido?.legajoGerenteUsuario)
			def userSesion = umeService.getUsuario(pedido.legajoUsuarioCreador)
			def aprobacionUI = new AprobarPedidoUI(justificacion: "", legajoInterlocutorUsuario: pedido.legajoInterlocutorUsuario, pedido: pedido)
			aprobacionUI.legajoInterlocutorUsuario = pedido.legajoInterlocutorUsuario
	
			def modelView = [pedidoInstance: pedido, aprobacionUI: aprobacionUI]
			modelView.putAll(getMapaDatosPedido(pedido))
			return modelView
		}
	}
	
	def completarUsuario = {
		def person

		def pedido = pedidoService.obtenerPedido(params, params?.pedidoId)

		if (params.username) {
			person = umeService.getUsuario(params.username)
		}
		
		def personsAutocomplete
		def parametros = Constantes.instance.parametros
		personsAutocomplete = umeService.getUsuariosGrupoLDAP(parametros.grupoLDAPInterlocutorUsuario, pedido?.legajoGerenteUsuario)

		render (template:"/templates/userBox", model:[person:person?.legajo, box:params.box, personsAutocomplete: personsAutocomplete])
	}

	/** Definiciones privadas de métodos */
	def generarAprobacionUI(aprueba) {
		def pedido = llenarDatosPedido(params, false)
		def result = new AprobarPedidoUI(justificacion: params.justificacionAprobacion, aprueba: aprueba, pedido: pedido)
		pedido.legajoInterlocutorUsuario = params.legajoInterlocutorUsuario
		return result
	}

	private def getMapaDatosPedido(pedido) {
		def parametros = Constantes.instance.parametros
		def personsAutocompleteInt = umeService.getUsuariosGrupoLDAP(parametros.grupoLDAPInterlocutorUsuario, pedido?.legajoGerenteUsuario)
		def usuarioLogueado = umeService.getUsuarioLogueado()
		def aprobacionesList = pedido?.aprobaciones.findAll { aprobacion -> aprobacion.posteriorAFechaYFase(pedido.fechaFinalizacionIngresoPedido, pedido.faseValidacionPedido()) }
		return [personsAutocompleteInt:personsAutocompleteInt, aprobacionesList:aprobacionesList, usuarioLogueado:usuarioLogueado]
	}

}
