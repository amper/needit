package ar.com.telecom


class AprobarPedidoController extends WorkflowController {
	/**
	 * Métodos del controller que usa la vista
	 */
	
	def webUserAgentService
	
	def index = {
		if (webUserAgentService.isBlackberry()) {
			redirect(controller:"inboxMobile", action: "index")
		} else {
			def usuarioLogueado = getUmeService().getUsuarioLogueado()
			def pedido = llenarDatosPedido(params, true)
			
			if(!pedido?.puedeEditarValidacion(usuarioLogueado)){
				redirect(action: "show", params: params)
			}
			
			def msgAdvertencia = Constantes.instance.parametros.mensajeAdvertenciaValidar
			
			def result = [pedidoInstance: pedido, msgAdvertencia: msgAdvertencia]
			result.putAll(getMapaDatosPedido(pedido))
			result
		}
	}

	private def getMapaDatosPedido(pedido) {
		def parametros = Constantes.instance.parametros
		def usuarioLogueado = getUmeService().getUsuarioLogueado()
		def aprobacionesList = pedido?.aprobaciones.findAll { aprobacion -> aprobacion.posteriorAFechaYFase(pedido.fechaAprobacionPedidoGU, pedido.faseAprobacionPedido()) }
		return [usuarioLogueado: usuarioLogueado, msgAdvertencia: parametros.mensajeAdvertenciaValidar, aprobacionesList: aprobacionesList]
	}

}
