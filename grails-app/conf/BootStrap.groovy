import grails.util.Environment

import org.codehaus.groovy.grails.commons.ApplicationHolder

import ar.com.telecom.Constantes
import ar.com.telecom.EstructuraIT
import ar.com.telecom.FaseMultiton
import ar.com.telecom.Person
import ar.com.telecom.PersonRole
import ar.com.telecom.Requestmap
import ar.com.telecom.Role
import ar.com.telecom.pcs.entities.ActividadPlanificacion
import ar.com.telecom.pcs.entities.AnexoListaBlanca
import ar.com.telecom.pcs.entities.AreaSoporte
import ar.com.telecom.pcs.entities.AreaSoporteAprobarFase
import ar.com.telecom.pcs.entities.CodigoCierre
import ar.com.telecom.pcs.entities.CriterioValidacionActividad
import ar.com.telecom.pcs.entities.EstadoActividad
import ar.com.telecom.pcs.entities.Fase
import ar.com.telecom.pcs.entities.GrupoGestionDemanda
import ar.com.telecom.pcs.entities.Justificacion
import ar.com.telecom.pcs.entities.JustificacionDisenioExterno
import ar.com.telecom.pcs.entities.MacroEstado
import ar.com.telecom.pcs.entities.ParametrizacionTipoPrueba
import ar.com.telecom.pcs.entities.ParametrosSistema
import ar.com.telecom.pcs.entities.Prioridad
import ar.com.telecom.pcs.entities.Release
import ar.com.telecom.pcs.entities.RolAplicacion
import ar.com.telecom.pcs.entities.RolReasignacion
import ar.com.telecom.pcs.entities.Sistema
import ar.com.telecom.pcs.entities.SoftwareFactory
import ar.com.telecom.pcs.entities.StockMensualSWF
import ar.com.telecom.pcs.entities.TailoringEstrategiaPrueba
import ar.com.telecom.pcs.entities.TareaSoporte
import ar.com.telecom.pcs.entities.TipoActividad
import ar.com.telecom.pcs.entities.TipoAnexo
import ar.com.telecom.pcs.entities.TipoCosto
import ar.com.telecom.pcs.entities.TipoGestion
import ar.com.telecom.pcs.entities.TipoImpacto
import ar.com.telecom.pcs.entities.TipoPedido
import ar.com.telecom.pcs.entities.TipoPrueba
import ar.com.telecom.pcs.entities.TipoReferencia
import ar.com.telecom.pcs.entities.UmbralAprobacionEconomica
import ar.com.telecom.pcs.entities.UmbralAprobacionEconomicaDireccion
import ar.com.telecom.pcs.entities.ValidacionCruceCamposAnexos
import ar.com.telecom.pcs.entities.Versionador

class BootStrap {
	def ldapConnection
	// TODO: Preguntar a Rodri si va con ROLE
	public static String ROL_SOLO_CONSULTA = "ROLE_ROL_SC"
	public static String ROL_GERENTE_DEFAULT = "ROLE_SOLO_CONSULTA"
	public static String ROL_ADMIN = "ROLE_FAF_NEEDIT"
	public static String GRUPO_INTERLOCUTOR_USUARIO = "ROL_IU"
	

	def init = { servletContext ->

		def estoyEnDesarrollo = Environment.current == Environment.DEVELOPMENT
		def estoyEnProduccion = Environment.current == Environment.PRODUCTION


		// PLD: Para que en futuros pasajes no corra el bootstrap y actualice datos paramétricos (Solo se popula la info de ParametrosSistema
		if(!estoyEnProduccion){

			definirRolesReasignacion()
			
			//		if (Environment.current == Environment.TEST) {
			//			def managerDn = "cn=" + ldapConnection?.username + ",ou=usuariosespeciales,ou=usuarios,o=telecom"
			//			def managerPassword = ldapConnection?.password
			//
			//			log.error "MANAGERRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRR ${ldapConnection.username}"
			//			log.error "MANAGERRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRR PASSSSSSSSSSSSS ${ldapConnection.password}"
			//
			//			org.codehaus.groovy.grails.commons.ConfigurationHolder.config.grails.plugins.springsecurity.ldap.context.managerDn = managerDn
			//			org.codehaus.groovy.grails.commons.ConfigurationHolder.config.grails.plugins.springsecurity.ldap.context.managerPassword = managerPassword
			//		}


			def me_ingresoPedido
			def me_analisisCambio
			def me_determinacionImpacto
			def me_construccion
			def me_cierreCambio
			def meh_desarrolloCambio
			def meh_ejecucionPAU
			def meh_aprobacionImpl
			def meh_implantacionCambio
			def meh_actividades

			def fap_registroPedido
			def fap_validacionPedido
			def fap_aprobacionPedido
			def fap_evaluacionCambio
			def fap_especificacionCambio
			def fap_aprobacionCambio
			def fap_consolidacionImpacto
			def fap_validacionImpacto
			def fap_aprobacionImpacto
			def fap_construccion
			def fap_finalizacion
			/** Fases hijas */
			def fah_especificacionImpacto
			def fah_construccionCambio
			def fah_soportePI
			def fah_ejecucionSoportePAU
			def fah_soportePAU
			def fah_aprobacionPAU
			def fah_aprobacionImplantacion
			def fah_implantacion
			def fah_normalizacion
			def fah_construccionPU
			def fah_administrarActiv

			def as_planeamiento
			def as_micro
			def as_dataCenter
			def as_operacionesFact
			def as_arquitectura
			def as_seginfo
			def as_integridadFact
			def as_testingDTI
			def as_testingDST
			def as_asegCalidadFact
			def as_asegCalidadProv
			def as_fraudes
			def as_procynormas


			/** Macroestado */
			// DONE
			if (MacroEstado.count() == 0) {
				me_ingresoPedido = new MacroEstado(descripcion: "Ingreso del Pedido", aplicaPadre: true).save(failOnError: true)
				me_analisisCambio = new MacroEstado(descripcion: "An\u00E1lisis del Cambio", aplicaPadre: true).save(failOnError: true)
				me_determinacionImpacto = new MacroEstado(descripcion: "Determinaci\u00F3n del Impacto", aplicaPadre: true).save(failOnError: true)
				me_construccion = new MacroEstado(descripcion: "Construcci\u00F3n, PI y PAU", aplicaPadre: true).save(failOnError: true)
				me_cierreCambio = new MacroEstado(descripcion: "Cierre del Cambio", aplicaPadre: true).save(failOnError: true)

				meh_desarrolloCambio = new MacroEstado(descripcion: "Desarrollo del Cambio", aplicaPadre: false).save(failOnError: true)
				meh_ejecucionPAU = new MacroEstado(descripcion: "Ejecuci\u00F3n de la PAU", aplicaPadre: false).save(failOnError: true)
				meh_aprobacionImpl = new MacroEstado(descripcion: "Aprobaci\u00F3n de la Implantaci\u00F3n", aplicaPadre: false).save(failOnError: true)
				meh_implantacionCambio = new MacroEstado(descripcion: "Implantaci\u00F3n del Cambio", aplicaPadre: false).save(failOnError: true)
				meh_actividades = new MacroEstado(descripcion: "Administrar actividades", aplicaPadre: false).save(failOnError: true)

			} else {
				me_ingresoPedido = MacroEstado.findByDescripcion("Ingreso del Pedido")
				me_analisisCambio = MacroEstado.findByDescripcion("An\u00E1lisis del Cambio")
				me_determinacionImpacto = MacroEstado.findByDescripcion("Determinaci\u00F3n del Impacto")
				me_construccion = MacroEstado.findByDescripcion("Construcci\u00F3n, PI y PAU")
				me_cierreCambio = MacroEstado.findByDescripcion("Cierre del Cambio")

				meh_desarrolloCambio = MacroEstado.findByDescripcion("Desarrollo del Cambio")
				meh_ejecucionPAU = MacroEstado.findByDescripcion("Ejecuci\u00F3n de la PAU")
				meh_aprobacionImpl = MacroEstado.findByDescripcion("Aprobaci\u00F3n de la Implantaci\u00F3n")
				meh_implantacionCambio = MacroEstado.findByDescripcion("Implantaci\u00F3n del Cambio")
				meh_actividades = MacroEstado.findByDescripcion("Administrar actividades")
			}

			/** Fases padres */
			// DONE
			if (Fase.count() == 0) {
				fap_registroPedido = new Fase(descripcion: "Registraci\u00F3n Pedido", macroEstado: me_ingresoPedido, aplicaPadre: true, controllerNombre: "registroPedido", codigoFase: FaseMultiton.REGISTRACION_PEDIDO, tooltip: "Aqu\u00ED el Usuario Final debe ingresar los datos del pedido.").save(failOnError: true)
				fap_validacionPedido = new Fase(descripcion: "Validaci\u00F3n Pedido", macroEstado: me_ingresoPedido, aplicaPadre: true, controllerNombre: "validarPedido", codigoFase: FaseMultiton.VALIDACION_PEDIDO, tooltip: "Aqu\u00ED el Gerente Usuario aprueba o deniega el pedido.", faseDenegacion: fap_registroPedido).save(failOnError: true)
				fap_aprobacionPedido = new Fase(descripcion: "Aprobaci\u00F3n Pedido", macroEstado: me_ingresoPedido, aplicaPadre: true, controllerNombre: "aprobarPedido", codigoFase: FaseMultiton.APROBACION_PEDIDO, tooltip: "Aqu\u00ED el Interlocutor Usuario aprueba o deniega el pedido.", faseDenegacion: fap_registroPedido).save(failOnError: true)
				fap_evaluacionCambio = new Fase(descripcion: "Evaluaci\u00F3n Cambio", macroEstado: me_analisisCambio, aplicaPadre: true, controllerNombre: "evaluacionPedido", codigoFase: FaseMultiton.EVALUACION_CAMBIO, tooltip: "Aqu\u00ED eval\u00FAa el pedido el sector Gesti\u00F3n de la Demanda.", faseDenegacion: fap_aprobacionPedido).save(failOnError: true)
				fap_especificacionCambio = new Fase(descripcion: "Especificaci\u00F3n Cambio", macroEstado: me_analisisCambio, aplicaPadre: true, controllerNombre: "especificacionCambio", codigoFase:FaseMultiton.ESPECIFICACION_CAMBIO, faseDenegacion: fap_aprobacionPedido,tooltip: "Aqu\u00ED se especifican los sistemas impactados, las \u00E1reas impactadas y el tipo de gesti\u00F3n que seguir\u00E1 el pedido.").save(failOnError: true)
				fap_aprobacionCambio = new Fase(descripcion: "Aprobaci\u00F3n Cambio", macroEstado: me_analisisCambio, aplicaPadre: true, controllerNombre: "aprobacionCambio", codigoFase: FaseMultiton.APROBACION_CAMBIO, faseDenegacion: fap_especificacionCambio,tooltip: "Aqu\u00ED el Interlocutor Usuario y Referente de \u00E1rea de Soporte (cuando corresponda) aprueba el cambio.").save(failOnError: true)
				fap_consolidacionImpacto = new Fase(descripcion: "Consolidaci\u00F3n Impacto", macroEstado: me_determinacionImpacto, aplicaPadre: true, controllerNombre: "consolidacionImpacto", codigoFase: FaseMultiton.CONSOLIDACION_IMPACTO,tooltip: "Aqu\u00ED el Coordinador valida la coherencia entre padre e hijos.").save(failOnError: true)
				fap_validacionImpacto = new Fase(descripcion: "Validaci\u00F3n Impacto", macroEstado: me_determinacionImpacto, aplicaPadre: true, controllerNombre: "validacionImpacto", codigoFase: FaseMultiton.VALIDACION_IMPACTO, faseDenegacion: fap_consolidacionImpacto,tooltip: "Aqu\u00ED el Interlocutor Usuario valida la informaci\u00F3n de impacto del pedido. Tambi\u00E9n deber\u00E1n aprobar la fase las \u00E1reas de Soporte correspondientes que hubieran sido impactadas en el cambio.").save(failOnError: true)
				fap_aprobacionImpacto = new Fase(descripcion: "Aprobaci\u00F3n Impacto", macroEstado: me_determinacionImpacto, aplicaPadre: true, controllerNombre: "aprobacionImpacto", codigoFase: FaseMultiton.APROBACION_IMPACTO, faseDenegacion: fap_validacionImpacto,tooltip: "Aqu\u00ED el Aprobador de Impacto aprueba o deniega el cambio. No se empieza a construir hasta que no se cuente con esta aprobaci\u00F3n.").save(failOnError: true)
				fap_construccion = new Fase(descripcion: "Construcci\u00F3n, Pruebas, PI y PAU", macroEstado: me_construccion, aplicaPadre: true, controllerNombre: "construccionPadre", codigoFase: FaseMultiton.CONSTRUCCION,tooltip: "Aqu\u00ED el Referente SWF gestiona el An\u00E1lisis, Desarrollo, las Pruebas Unitarias y las Pruebas de Integraci\u00F3n.", descripcionEnBaseAHijos: true).save(failOnError: true)
				fap_finalizacion = new Fase(descripcion: "Finalizaci\u00F3n", macroEstado: me_cierreCambio, aplicaPadre: true, controllerNombre: "finalizacionPedido", codigoFase: FaseMultiton.FINALIZACION,tooltip: "Finalizaci\u00F3n del Pedido.").save(failOnError: true)

				/** Fases hijas */
				fah_especificacionImpacto = new Fase(fasePadre: fap_consolidacionImpacto, descripcion: "Especificaci\u00F3n Impacto", macroEstado: me_determinacionImpacto, aplicaPadre: false, controllerNombre: "consolidacionImpacto", codigoFase: FaseMultiton.ESPECIFICACION_IMPACTO_HIJO, faseDenegacion: fap_consolidacionImpacto ,tooltip: "Aqu\u00ED se detallan esfuerzo, planificaci\u00F3n, estrategia de prueba y diseño externo (si aplica).").save(failOnError: true)
				fah_construccionCambio = new Fase(fasePadre: fap_construccion, descripcion: "Construcci\u00F3n Cambio", macroEstado: meh_desarrolloCambio, aplicaPadre: false, controllerNombre: "construccionPadre", codigoFase: FaseMultiton.CONSTRUCCION_CAMBIO_HIJO,tooltip: "Aqu\u00ED se gestionan y documentan las actividades necesarias para la construcci\u00F3n del cambio en el aplicativo impactado.").save(failOnError: true)
				fah_ejecucionSoportePAU = new Fase(fasePadre: fap_construccion, descripcion: "Ejecuci\u00F3n y Soporte PAU", macroEstado: meh_ejecucionPAU, aplicaPadre: false, controllerNombre: "ejecucionSoportePAUHijo", codigoFase: FaseMultiton.EJECUCION_SOPORTE_PAU_HIJO,tooltip: "Aqu\u00ED se realizan las actividades necesarias para la ejecuci\u00F3n de las Pruebas de Aceptaci\u00F3n de Usuario.").save(failOnError: true)
				fah_aprobacionPAU =
						new Fase(descripcion: "Aprobaci\u00F3n PAU",
						macroEstado: meh_ejecucionPAU,
						fasePadre: fap_construccion,
						aplicaPadre: false,
						controllerNombre: "aprobacionPAUHijo",
						codigoFase: FaseMultiton.APROBACION_PAU_HIJO,
						faseDenegacion: null, // fah_ejecucionSoportePAU
						tooltip: "Aqu\u00ED el Interlocutor Usuario aprueba las Pruebas de Aceptaci\u00F3n de Usuario.").save(failOnError: true)
				fah_aprobacionPAU.faseDenegacion = fah_ejecucionSoportePAU
				fah_aprobacionPAU.save(failOnError: true)
				fah_aprobacionImplantacion = new Fase(fasePadre: fap_construccion, descripcion: "Aprobaci\u00F3n implantaci\u00F3n", macroEstado: meh_aprobacionImpl, aplicaPadre: false, controllerNombre: "aprobacionImplantacionHijo", codigoFase: FaseMultiton.APROBACION_IMPLANTACION_HIJO,tooltip: "Aqu\u00ED se realizan las aprobaciones necesarias para la puesta en producci\u00F3n del cambio aplicativo hijo.").save(failOnError: true)
				fah_implantacion = new Fase(fasePadre: fap_construccion, descripcion: "Implantaci\u00F3n", macroEstado: meh_implantacionCambio, aplicaPadre: false, controllerNombre: "implantacionHijo", codigoFase: FaseMultiton.IMPLANTACION_HIJO,tooltip: "Aqu\u00ED se gestionan y documentan las actividades necesarias para la puesta en producci\u00F3n del cambio.").save(failOnError: true)
				fah_normalizacion = new Fase(fasePadre: fap_construccion, descripcion: "Normalizaci\u00F3n", macroEstado: meh_implantacionCambio, aplicaPadre: false, controllerNombre: "normalizacionHijo", codigoFase: FaseMultiton.NORMALIZACION_HIJO,tooltip: "Aqu\u00ED se gestionan y documentan las actividades necesarias para la implementaci\u00F3n del cambio en el ambiente definitivo.").save(failOnError: true)
				fah_construccionPU = new Fase(fasePadre: fap_construccion, descripcion: "Construcci\u00F3n y PU", macroEstado: meh_desarrolloCambio, aplicaPadre: false, controllerNombre: "construccionPadre", codigoFase: FaseMultiton.CONSTRUCCION_PU_HIJO,tooltip: "Aqu\u00ED el Referente SWF de SAP gestiona el An\u00E1lisis, Desarrollo y las Pruebas Unitarias del cambio.").save(failOnError: true)
				fah_administrarActiv = new Fase(fasePadre: fap_construccion, descripcion: "Administraci\u00F3n de actividades", macroEstado: meh_actividades, aplicaPadre: false, controllerNombre: "construccionPadre", codigoFase: FaseMultiton.ADMINISTRAR_ACTIVIDADES_HIJO,tooltip: "Aqu\u00ED las \u00E1reas de Soporte/Administradores Funcionales gestionan y documentan las actividades que requieran para dar soporte al cambio.").save(failOnError: true)

			} else {
				fap_registroPedido = Fase.findByCodigoFase(FaseMultiton.REGISTRACION_PEDIDO)
				fap_validacionPedido = Fase.findByCodigoFase(FaseMultiton.VALIDACION_PEDIDO)
				fap_aprobacionPedido = Fase.findByCodigoFase(FaseMultiton.APROBACION_PEDIDO)
				fap_evaluacionCambio = Fase.findByCodigoFase(FaseMultiton.EVALUACION_CAMBIO)
				fap_especificacionCambio = Fase.findByCodigoFase(FaseMultiton.ESPECIFICACION_CAMBIO)
				fap_aprobacionCambio = Fase.findByCodigoFase(FaseMultiton.APROBACION_CAMBIO)
				fap_consolidacionImpacto = Fase.findByCodigoFase(FaseMultiton.CONSOLIDACION_IMPACTO)
				fap_validacionImpacto = Fase.findByCodigoFase(FaseMultiton.VALIDACION_IMPACTO)
				fap_aprobacionImpacto = Fase.findByCodigoFase(FaseMultiton.APROBACION_IMPACTO)
				fap_construccion = Fase.findByCodigoFase(FaseMultiton.CONSTRUCCION)
				fap_finalizacion = Fase.findByCodigoFase(FaseMultiton.FINALIZACION)

				/** Fases hijas */
				fah_especificacionImpacto = Fase.findByCodigoFase(FaseMultiton.ESPECIFICACION_IMPACTO_HIJO)
				fah_construccionCambio = Fase.findByCodigoFase(FaseMultiton.CONSTRUCCION_CAMBIO_HIJO)
				fah_ejecucionSoportePAU = Fase.findByCodigoFase(FaseMultiton.EJECUCION_SOPORTE_PAU_HIJO)
				fah_aprobacionPAU = Fase.findByCodigoFase(FaseMultiton.APROBACION_PAU_HIJO)
				fah_aprobacionImplantacion = Fase.findByCodigoFase(FaseMultiton.APROBACION_IMPLANTACION_HIJO)
				fah_implantacion = Fase.findByCodigoFase(FaseMultiton.IMPLANTACION_HIJO)
				fah_normalizacion = Fase.findByCodigoFase(FaseMultiton.NORMALIZACION_HIJO)
				fah_construccionPU = Fase.findByCodigoFase(FaseMultiton.CONSTRUCCION_PU_HIJO)
				fah_administrarActiv = Fase.findByCodigoFase(FaseMultiton.ADMINISTRAR_ACTIVIDADES_HIJO)
			}

			definirCodigosCierre()

			def gestionEstandar
			def gestionEmergencia

			/** Tipos de gesti\u00F3n */
			//DONE
			if (TipoGestion.count() == 0) {
				gestionEstandar = new TipoGestion(descripcion: TipoGestion.GESTION_ESTANDAR, fases: [
					fap_registroPedido,
					fap_validacionPedido ,
					fap_aprobacionPedido ,
					fap_evaluacionCambio ,
					fap_especificacionCambio ,
					fap_aprobacionCambio ,
					fap_consolidacionImpacto ,
					fap_validacionImpacto ,
					fap_aprobacionImpacto ,
					fap_construccion ,
					fap_finalizacion
				]).save(failOnError: true)

				gestionEmergencia = new TipoGestion(descripcion: TipoGestion.GESTION_EMERGENCIA, fases: [
					fap_registroPedido,
					fap_validacionPedido ,
					fap_aprobacionPedido ,
					fap_evaluacionCambio ,
					fap_especificacionCambio ,
					fap_consolidacionImpacto ,
					fap_validacionImpacto ,
					fap_construccion ,
					fap_finalizacion
				]).save(failOnError: true)
			} else {
				gestionEstandar = TipoGestion.findByDescripcion(TipoGestion.GESTION_ESTANDAR)
				gestionEmergencia = TipoGestion.findByDescripcion(TipoGestion.GESTION_EMERGENCIA)
			}

			if (UmbralAprobacionEconomicaDireccion.count() == 0) {
			}

			// DONE
			if (UmbralAprobacionEconomica.count() == 0) {
				new UmbralAprobacionEconomica(tipoGestion: gestionEstandar, rangoDesde: 30000, rangoHasta: 250000).save(failOnError: true)
				new UmbralAprobacionEconomica(tipoGestion: gestionEmergencia, rangoDesde: 0, rangoHasta: 99999999).save(failOnError: true)
			}

			/** Prioridades */
			//DONE
			if (Prioridad.count() == 0) {
				new Prioridad(descripcion: "Radar").save(failOnError: true)
				new Prioridad(descripcion: "Alta").save(failOnError: true)
				new Prioridad(descripcion: "Media").save(failOnError: true)
				new Prioridad(descripcion: "Baja").save(failOnError: true)
			}

			/** Tipos de pedido */
			// DONE
			if (TipoPedido.count() == 0) {
				new TipoPedido(descripcion: "Eficiencia - Mejora a Procesos").save(failOnError: true)
				new TipoPedido(descripcion: "Eficiencia - Mejoras T\u00E9cnicas").save(failOnError: true)
				new TipoPedido(descripcion: "Obsolescencia - Procesos").save(failOnError: true)
				new TipoPedido(descripcion: "Obsolescencia - T\u00E9cnica").save(failOnError: true)
				new TipoPedido(descripcion: "Estructural - Mejora a Procesos").save(failOnError: true)
				new TipoPedido(descripcion: "Estructural - Mejoras T\u00E9cnicas").save(failOnError: true)
				new TipoPedido(descripcion: "Regulatorios").save(failOnError: true)
				new TipoPedido(descripcion: "Nuevos Productos y Servicios").save(failOnError: true)
				new TipoPedido(descripcion: "Impositivos").save(failOnError: true)
				new TipoPedido(descripcion: "Legales").save(failOnError: true)
			}

			/** Tipos de referencia */
			// DONE
			if (TipoReferencia.count() == 0) {
				new TipoReferencia(descripcion: "EPM").save(failOnError: true)
				new TipoReferencia(descripcion: "PCS").save(failOnError: true)
				new TipoReferencia(descripcion: "NeedIT").save(failOnError: true)
				new TipoReferencia(descripcion: "Gesti\u00F3n de Problemas").save(failOnError: true)
			}

			/** Areas de soporte */
			if (AreaSoporte.count() == 0) {

				as_planeamiento = new AreaSoporte(descripcion: "Planeamiento y Evaluaciones Econ\u00F3micas", grupoAdministrador: "AS_PYEE").save(failOnError: true)
				as_micro = new AreaSoporte(descripcion: "Microinform\u00E1tica", grupoAdministrador: "AS_MICROINFORMATICA").save(failOnError: true)
				as_dataCenter = new AreaSoporte(descripcion: "Data Center", grupoAdministrador: "AS_DC").save(failOnError: true)
				as_operacionesFact = new AreaSoporte(descripcion: "Operaciones de Facturaci\u00F3n", grupoAdministrador: "AS_OF").save(failOnError: true)
				as_arquitectura = new AreaSoporte(descripcion: "Arquitectura Aplicativa DTI-UF", grupoAdministrador: "AS_AA").save(failOnError: true)
				as_seginfo = new AreaSoporte(descripcion: "Seguridad inform\u00E1tica", grupoAdministrador: "AS_SI").save(failOnError: true)
				as_integridadFact = new AreaSoporte(descripcion: "Grupo Integridad Facturaci\u00F3n", grupoAdministrador: "AS_GIF").save(failOnError: true)
				as_testingDTI = new AreaSoporte(descripcion: "Testing DTI-UF", grupoAdministrador: "AS_TEST").save(failOnError: true)
				as_fraudes = new AreaSoporte(descripcion: "Grupo de Control y Prevenci\u00F3n de Fraudes", grupoAdministrador: "AS_GCYPF").save(failOnError: true)
				as_procynormas = new AreaSoporte(descripcion: "Procesos y Normas", grupoAdministrador: "AS_PYN").save(failOnError: true)

			} else {
				as_planeamiento = AreaSoporte.findByDescripcion("Planeamiento y Evaluaciones Econ\u00F3micas")
				as_micro = AreaSoporte.findByDescripcion("Microinform\u00E1tica")
				as_dataCenter = AreaSoporte.findByDescripcion("Data Center")
				as_operacionesFact = AreaSoporte.findByDescripcion("Operaciones de Facturaci\u00F3n")
				as_arquitectura = AreaSoporte.findByDescripcion("Arquitectura Aplicativa DTI-UF")
				as_seginfo = AreaSoporte.findByDescripcion("Seguridad inform\u00E1tica")
				as_integridadFact = AreaSoporte.findByDescripcion("Grupo Integridad Facturaci\u00F3n")
				as_testingDTI = AreaSoporte.findByDescripcion("Testing DTI-UF")
				as_fraudes = AreaSoporte.findByDescripcion("Grupo de Control y Prevenci\u00F3n de Fraudes")
				as_procynormas = AreaSoporte.findByDescripcion("Procesos y Normas")
			}

			/** Areas de soporte que deben aprobar una fase */
			// DONE
			if (AreaSoporteAprobarFase.count() == 0) {
				new AreaSoporteAprobarFase(fase: fap_aprobacionCambio, areaSoporte: as_arquitectura).save(failOnError: true)
				new AreaSoporteAprobarFase(fase: fap_aprobacionCambio, areaSoporte: as_planeamiento).save(failOnError: true)
				new AreaSoporteAprobarFase(fase: fap_aprobacionCambio, areaSoporte: as_operacionesFact).save(failOnError: true)
				new AreaSoporteAprobarFase(fase: fap_validacionImpacto, areaSoporte: as_arquitectura).save(failOnError: true)
				new AreaSoporteAprobarFase(fase: fap_validacionImpacto, areaSoporte: as_planeamiento).save(failOnError: true)
				new AreaSoporteAprobarFase(fase: fap_validacionImpacto, areaSoporte: as_operacionesFact).save(failOnError: true)
				new AreaSoporteAprobarFase(fase: fah_aprobacionImplantacion, areaSoporte: as_operacionesFact).save(failOnError: true)
				new AreaSoporteAprobarFase(fase: fah_aprobacionImplantacion, areaSoporte: as_seginfo).save(failOnError: true)
				new AreaSoporteAprobarFase(fase: fah_aprobacionImplantacion, areaSoporte: as_planeamiento).save(failOnError: true)
			}

			/** Areas de soporte impactables */
			/** Pendiente de definici\u00F3n de si tiene sentido esta entidad */

			def impacto_SWDesa
			def impacto_SWDesaSAP
			def impacto_AS
			def impacto_AF
			def impacto_SopPruebas
			def impacto_DatosRef

			/** Tipos de impacto */
			//DONE
			if (TipoImpacto.count() == 0) {

				impacto_SWDesaSAP = new TipoImpacto(participaDescripcionFasesHijos: true, descripcion: "SW Desarrollo SAP", codigoAgrupador: "\u00E1reas IT", codigo: "SAP", asociadoSistemas: true, sistemaImpactado: true, tieneDisenoExterno: false, tienePlanificacionyEsfuerzo: true, tieneEstrategiaPrueba: true, permiteIngresarOtrosCostos: true, ingresaTicketSolman: true, fases: [
					fah_especificacionImpacto
				]).save(failOnError: true)

				impacto_SWDesa = new TipoImpacto(participaDescripcionFasesHijos: true, descripcion: "SW Desarrollo", codigoAgrupador: "\u00E1reas IT", codigo: "SWD", asociadoSistemas: true, sistemaImpactado: true, tieneDisenoExterno: true, tienePlanificacionyEsfuerzo: true, tieneEstrategiaPrueba: true, permiteIngresarOtrosCostos: true, ingresaTicketSolman: false, fases: [
					fah_especificacionImpacto,
					fah_construccionCambio,
					fah_ejecucionSoportePAU,
					fah_aprobacionPAU,
					fah_aprobacionImplantacion,
					fah_implantacion,
					fah_normalizacion
				]).save(failOnError: true)

				impacto_AS = new TipoImpacto(descripcion: "Area de soporte", codigoAgrupador: "\u00E1reas soporte", codigo: "AS", asociadoSistemas: false, sistemaImpactado: false, tieneDisenoExterno: false, tienePlanificacionyEsfuerzo: true, tieneEstrategiaPrueba: false, permiteIngresarOtrosCostos: false, ingresaTicketSolman: false, fases: [
					fah_especificacionImpacto,
					fah_administrarActiv
				]).save(failOnError: true)

				impacto_DatosRef = new TipoImpacto(participaDescripcionFasesHijos: true, descripcion: "Datos referenciales", codigoAgrupador: "\u00E1reas IT", codigo: "DR", asociadoSistemas: true, sistemaImpactado: true, tieneDisenoExterno: false, tienePlanificacionyEsfuerzo: true, tieneEstrategiaPrueba: true, permiteIngresarOtrosCostos: true, ingresaTicketSolman: false, fases: [
					fah_especificacionImpacto,
					fah_construccionCambio,
					fah_ejecucionSoportePAU,
					fah_aprobacionPAU,
					fah_aprobacionImplantacion,
					fah_implantacion,
					fah_normalizacion
				]).save(failOnError: true)

				impacto_AF = new TipoImpacto(descripcion: TipoImpacto.ADMINISTRADOR_FUNCIONAL, codigoAgrupador: "Administrador funcional", codigo: "AF", asociadoSistemas: false, sistemaImpactado: true, tieneDisenoExterno: false, tienePlanificacionyEsfuerzo: true, tieneEstrategiaPrueba: false, permiteIngresarOtrosCostos: false, ingresaTicketSolman: false, fases: [
					fah_especificacionImpacto,
					fah_administrarActiv
				]).save(failOnError: true)

				impacto_SopPruebas = new TipoImpacto(participaDescripcionFasesHijos: true, descripcion: "SW Soporte a pruebas", codigoAgrupador: "\u00E1reas IT", codigo: "SP", asociadoSistemas: true, sistemaImpactado: true, tieneDisenoExterno: false, tienePlanificacionyEsfuerzo: true, tieneEstrategiaPrueba: true, permiteIngresarOtrosCostos: false, ingresaTicketSolman: false, fases: [
					fah_especificacionImpacto,
					fah_construccionCambio,
					fah_ejecucionSoportePAU
				]).save(failOnError: true)

			} else {
				impacto_SWDesa = TipoImpacto.findByDescripcion("SW Desarrollo")
				impacto_SWDesaSAP = TipoImpacto.findByDescripcion("SW Desarrollo SAP")
				impacto_AS = TipoImpacto.findByDescripcion("Area de soporte")
				impacto_DatosRef = TipoImpacto.findByDescripcion("Datos referenciales")
				impacto_AF = TipoImpacto.findByDescripcion(TipoImpacto.ADMINISTRADOR_FUNCIONAL)
				impacto_SopPruebas = TipoImpacto.findByDescripcion("SW Soporte a pruebas")
			}

			if (!Requestmap.findByUrl("/inbox/**")) {
				// Ver con Rodri
				new Requestmap(url: "/inbox/**", configAttribute: ROL_SOLO_CONSULTA + "," + ROL_GERENTE_DEFAULT).save(failOnError: true)
			}
			if (!Requestmap.findByUrl("/inboxMobile/**")) {
				new Requestmap(url: "/inboxMobile/**", configAttribute: ROL_SOLO_CONSULTA + "," + ROL_GERENTE_DEFAULT).save(failOnError: true)
			}
			Fase.list().each { fase ->
				def url = "/" + fase.controllerNombre + "**"
				if (!Requestmap.findByUrl( url )) {
					new Requestmap(url: url, configAttribute: ROL_SOLO_CONSULTA + "," + ROL_GERENTE_DEFAULT).save(failOnError: true)
				}

				url = "/" + fase.controllerNombre + "/**"
				if (!Requestmap.findByUrl( url )) {
					new Requestmap(url: url, configAttribute: ROL_SOLO_CONSULTA + "," + ROL_GERENTE_DEFAULT).save(failOnError: true)
				}
			}

			for (claseDominio in ApplicationHolder.application.domainClasses) {
				String nombreClase = claseDominio.name
				def clase = nombreClase[0].toLowerCase() + nombreClase.substring(1, nombreClase.length())

				def url = "/" + clase + "/**"
				if (!Requestmap.findByUrl( url )) {
					new Requestmap(url: url, configAttribute: ROL_ADMIN).save(failOnError: true)
				}

				url = "/" + clase + "**"
				if (!Requestmap.findByUrl( url )) {
					new Requestmap(url: url, configAttribute: ROL_ADMIN).save(failOnError: true)
				}

			}

			if (!Requestmap.findByUrl("/administracion/**")) {
				new Requestmap(url: "/administracion/**", configAttribute: ROL_ADMIN).save(failOnError: true)
			}

			//DONE
			if (TipoAnexo.count() == 0) {
				new TipoAnexo(descripcion: "Documentaci\u00F3n de Registro", fase: fap_registroPedido).save(failOnError: true)
				new TipoAnexo(descripcion: "Documentaci\u00F3n de Especificaci\u00F3n", fase: fap_especificacionCambio).save(failOnError: true)
				new TipoAnexo(descripcion: "Documentaci\u00F3n de Consolidaci\u00F3n Impacto", fase: fap_consolidacionImpacto ).save(failOnError: true)
				new TipoAnexo(descripcion: "Documentaci\u00F3n de Especificaci\u00F3n", fase: fah_especificacionImpacto ).save(failOnError: true)
				new TipoAnexo(descripcion: "Documentaci\u00F3n de Construcci\u00F3n y Pruebas", fase: fah_construccionCambio ).save(failOnError: true)
				new TipoAnexo(descripcion: "Documentaci\u00F3n de Pruebas PAU", fase: fah_ejecucionSoportePAU ).save(failOnError: true)
				new TipoAnexo(descripcion: "Documentaci\u00F3n de Implantaci\u00F3n", fase: fah_implantacion ).save(failOnError: true)
				new TipoAnexo(descripcion: "Documentaci\u00F3n de Normalizaci\u00F3n", fase: fah_normalizacion ).save(failOnError: true)
				new TipoAnexo(descripcion: "Documentaci\u00F3n de \u00E1rea de Soporte", fase: fah_administrarActiv ).save(failOnError: true)
				new TipoAnexo(descripcion: "Documentaci\u00F3n de Administraci\u00F3n Funcional", fase: fah_administrarActiv ).save(failOnError: true)
				new TipoAnexo(descripcion: "Global", fase: null).save(failOnError: true)
			}

			/**
			 * Se elimina la configuraci\u00F3n de mails, tambi\u00E9n la configuraci\u00F3n de usuarios que se hace v\u00EDa importaci\u00F3n de Access
			 * DATOS DE PRUEBA EXCLUSIVOS DE DESARROLLO
			 **/
			if (Sistema.count() == 0 && estoyEnDesarrollo) {
				crearSistemas()
			}

			if (SoftwareFactory.count() == 0) {
				definirSWF()
			}

			// Solo debe correr una vez sola en Prod, con lo cual se comenta para pr\u00F3ximos pasajes.
			//if (Sistema.count() > 0 && estoyEnProduccion) {
			//	actualizarDatosSistemas()
			//}

			if (AreaSoporte.count() > 0 && Sistema.count() > 0) {
				definirAreaSoporteSistemas()
			}

			if (Release.count() == 0 && Sistema.count() > 0) {
				definirReleases()
			}

			if (SoftwareFactory.count() > 0 && Sistema.count() > 0 && estoyEnProduccion) {
				definirSistemaSWF()
			}

			def rol_uf
			def rol_gu
			def rol_iu
			def rol_fgd
			def rol_cc
			def rol_rswf
			def rol_swf
			def rol_as
			def rol_af
			def rol_ap

			if (RolAplicacion.count() == 0) {
				rol_uf = new RolAplicacion(descripcion: "Usuario Final", seleccionableEnListados: true, codigoRol: RolAplicacion.USUARIO_FINAL, propiedadUsuariosEnvioMails: "usuariosFinales", aplicaPadre: true).save(failOnError: true)
				rol_gu = new RolAplicacion(descripcion: "Gerente Usuario", seleccionableEnListados: true, codigoRol: RolAplicacion.GERENTE_USUARIO, propiedadUsuariosEnvioMails: "gerentesUsuarios", aplicaPadre: true).save(failOnError: true)
				rol_iu = new RolAplicacion(descripcion: "Interlocutor Usuario", seleccionableEnListados: true, codigoRol: RolAplicacion.INTERLOCUTOR_USUARIO, propiedadUsuariosEnvioMails: "interlocutoresUsuarios", aplicaPadre: true).save(failOnError: true)
				rol_fgd = new RolAplicacion(descripcion: "Gesti\u00F3n de la Demanda", seleccionableEnListados: true, codigoRol: RolAplicacion.GESTION_DEMANDA, propiedadUsuariosEnvioMails: "usuariosGestionDemanda", propiedadGrupoEnvioMails: "grupoGestionDemanda", aplicaPadre: true).save(failOnError: true)
				rol_cc = new RolAplicacion(descripcion: "Coordinador del Cambio", seleccionableEnListados: true, codigoRol: RolAplicacion.COORDINADOR_CAMBIO, propiedadUsuariosEnvioMails: "usuariosCoordinadoresCambio", aplicaPadre: true).save(failOnError: true)
				rol_rswf = new RolAplicacion(descripcion: "Responsable Software Factory", seleccionableEnListados: true, codigoRol: RolAplicacion.RESPONSABLE_SWF, propiedadUsuariosEnvioMails: "usuariosResponsablesSWF", propiedadGrupoEnvioMails: "grupoResponsable", aplicaPadre: false).save(failOnError: true)
				rol_swf = new RolAplicacion(descripcion: "Software Factory", seleccionableEnListados: true, codigoRol: RolAplicacion.SOFTWARE_FACTORY, propiedadUsuariosEnvioMails: "usuariosSoftwareFactory", propiedadGrupoEnvioMails: "grupoSWF", aplicaPadre: false).save(failOnError: true)
				rol_as = new RolAplicacion(descripcion: "\u00E1rea de Soporte", seleccionableEnListados: true, codigoRol: RolAplicacion.AREA_SOPORTE, propiedadUsuariosEnvioMails: "usuariosAreasSoporte", propiedadGrupoEnvioMails: "grupoResponsable", aplicaPadre: false).save(failOnError: true)
				rol_af = new RolAplicacion(descripcion: "Administrador Funcional", seleccionableEnListados: true, codigoRol: RolAplicacion.ADMINISTRADOR_FUNCIONAL, propiedadUsuariosEnvioMails: "usuariosAdministradoresFuncionales", propiedadGrupoEnvioMails: "grupoResponsable", aplicaPadre: false).save(failOnError: true)
				rol_ap = new RolAplicacion(descripcion: "Aprobador impacto", seleccionableEnListados: true, codigoRol: RolAplicacion.APROBADOR_IMPACTO, propiedadUsuariosEnvioMails: "usuariosAprobadoresImpacto", aplicaPadre: true).save(failOnError: true)
			} else {
				rol_gu = RolAplicacion.findByDescripcion("Gerente Usuario")
				rol_iu = RolAplicacion.findByDescripcion("Interlocutor Usuario")
				rol_fgd = RolAplicacion.findByDescripcion("Gesti\u00F3n de la Demanda")
				rol_cc = RolAplicacion.findByDescripcion("Coordinador del Cambio")
				rol_rswf = RolAplicacion.findByDescripcion("Responsable Software Factory")
				rol_swf = RolAplicacion.findByDescripcion("Software Factory")
				rol_as = RolAplicacion.findByDescripcion("\u00E1rea de Soporte")
				rol_af = RolAplicacion.findByDescripcion("Administrador Funcional")
				rol_ap = RolAplicacion.findByDescripcion("Aprobador impacto")
			}

			crearRolSiCorresponde("\u00E1reas de soporte impactadas", false, "areasSoporteImpactadas", true, RolAplicacion.AREA_SOPORTE_IMPACTADA)
			crearRolSiCorresponde("\u00E1reas de soporte no impactadas", false, "areasSoporteNoImpactadas", true, RolAplicacion.AREA_SOPORTE_NO_IMPACTADA)
			crearRolSiCorresponde("Administradores funcionales impactados", false, "administradoresFuncionalesImpactados", true, RolAplicacion.ADMINISTRADOR_FUNCIONAL_IMPACTADO)
			crearRolSiCorresponde("Responsable Actividades", false, "responsablesActividades", true, RolAplicacion.RESPONSABLE_ACTIVIDAD)
			crearRolSiCorresponde("Aprobadores de fase actual", false, "aprobadoresFaseActual", true, RolAplicacion.APROBADOR_FASE)
			crearRolSiCorresponde("Gesti\u00F3n operativa", false, "usuariosGestionOperativa", false, "GO")
			crearRolSiCorresponde("ROLE_ARQUITECTURA", true, null, true, "ASAA")

			// DONE
			if (GrupoGestionDemanda.count() == 0) {
				new GrupoGestionDemanda(grupoLDAP: "FGD_ALL").save(failOnError: true)
				new GrupoGestionDemanda(grupoLDAP: "FGD_DTI").save(failOnError: true)
			}

			if (!estoyEnProduccion) {
				def role_todos
				if (Role.count() == 0) {
					role_todos = new Role(authority: ROL_SOLO_CONSULTA).save(failOnError: true)
					new Role(authority: ROL_ADMIN).save(failOnError: true)
					new Role(authority: ROL_GERENTE_DEFAULT).save(failOnError: true)
				} else {
					role_todos = Role.findByAuthority(ROL_SOLO_CONSULTA)
				}

				def personasSinRolIntranet

				if (PersonRole.count() == 0) {
					personasSinRolIntranet = Person.list()
				} else {
					personasSinRolIntranet = Person.list().findAll { persona -> !persona.tieneRol(role_todos) }
				}
				personasSinRolIntranet.each { persona -> PersonRole.create(persona, role_todos, true) }
			}

			if (EstructuraIT.count() == 0 ){
				new EstructuraIT(codigoEstructura: "0000000013").save(failOnError: true) // Desarrollo De Sistemas Crm
				new EstructuraIT(codigoEstructura: "0000000014").save(failOnError: true) // Desarr. De Sist. De Prov. E Integr.
				new EstructuraIT(codigoEstructura: "0000000016").save(failOnError: true) // Desarr. De Sist.  De Fact. Y Cobr.
				new EstructuraIT(codigoEstructura: "0000002068").save(failOnError: true) // Sistemas Corporativos
				new EstructuraIT(codigoEstructura: "0000002145").save(failOnError: true) // Sistemas Tecnicos
				new EstructuraIT(codigoEstructura: "0000002378").save(failOnError: true) // Plan. y Arquitectura Aplicativa
				new EstructuraIT(codigoEstructura: "0000002649").save(failOnError: true) // Desarr. De Sist. De Intelig. Del Neg.
				new EstructuraIT(codigoEstructura: "0000002785").save(failOnError: true) // Desarr. De Serv. Internet Y Multim.
				new EstructuraIT(codigoEstructura: "0000002878").save(failOnError: true) // Demanda Y Planificacion
				new EstructuraIT(codigoEstructura: "0000002894").save(failOnError: true) // Gest. Operativa Y Aseg. De Calidad
				new EstructuraIT(codigoEstructura: "0050211207").save(failOnError: true) // Customer Care Mercados Masivos
				new EstructuraIT(codigoEstructura: "0050211241").save(failOnError: true) // Venta Mercados Masivos
				new EstructuraIT(codigoEstructura: "0050211327").save(failOnError: true) // Provision Mercados No Masivos
				new EstructuraIT(codigoEstructura: "0050212881").save(failOnError: true) // Provision Servicios Internet
				new EstructuraIT(codigoEstructura: "0050212890").save(failOnError: true) // Provision Servicios Basicos
				new EstructuraIT(codigoEstructura: "0050212891").save(failOnError: true) // Integracion
				new EstructuraIT(codigoEstructura: "0050212915").save(failOnError: true) // Mediacion Y Fraude
				new EstructuraIT(codigoEstructura: "0050212916").save(failOnError: true) // Facturacion Mercado Masivo
				new EstructuraIT(codigoEstructura: "0050212917").save(failOnError: true) // Cobranzas Masivos
				new EstructuraIT(codigoEstructura: "0050212918").save(failOnError: true) // Fact. Grandes Clte. Y Wholesale
				new EstructuraIT(codigoEstructura: "0000002821").save(failOnError: true) // Sist. Adm. Fin. Y Asset Management
				new EstructuraIT(codigoEstructura: "0000002822").save(failOnError: true) // Sist. De Abastec. Y Vtas. Varias
				new EstructuraIT(codigoEstructura: "0000002823").save(failOnError: true) // Sist. De Recursos Humanos Y Legales
				new EstructuraIT(codigoEstructura: "0000002824").save(failOnError: true) // Tecnologia E Integracion Aplicativa
				new EstructuraIT(codigoEstructura: "0000002875").save(failOnError: true) // Sist Fault Perfor Y Activacion
				new EstructuraIT(codigoEstructura: "0000002876").save(failOnError: true) // Sist Recl Y Workforce Management
				new EstructuraIT(codigoEstructura: "0000002877").save(failOnError: true) // Sistemas De Inventarios De Redes
				new EstructuraIT(codigoEstructura: "0050372003").save(failOnError: true) // Factory Externa De Desarrollo
				new EstructuraIT(codigoEstructura: "0050372004").save(failOnError: true) // Produccion, Calidad y Testing
				new EstructuraIT(codigoEstructura: "0050212920").save(failOnError: true) // Sistemas De Soporte A La Gestion
				new EstructuraIT(codigoEstructura: "0050212927").save(failOnError: true) // Plataforma Datawarehouse
				new EstructuraIT(codigoEstructura: "0050212928").save(failOnError: true) // Desarrollo De Servicios De Acceso
				new EstructuraIT(codigoEstructura: "0050212937").save(failOnError: true) // Valor Agregado
				new EstructuraIT(codigoEstructura: "0050212938").save(failOnError: true) // Desarrollo De Portales
				new EstructuraIT(codigoEstructura: "0050285038").save(failOnError: true) // Gestion De La Demanda
				new EstructuraIT(codigoEstructura: "0050285109").save(failOnError: true) // Gest. Operat. Vtas. Y Customer Care
				new EstructuraIT(codigoEstructura: "0050285110").save(failOnError: true) // Gest. Operat. Fact., Cobr. E Internet
				new EstructuraIT(codigoEstructura: "0050318633").save(failOnError: true) // Testing

			}

			definirTiposPruebaYJustificaciones()

			if (TipoActividad.count() == 0) {
				definirTiposActividades()
			}

			if (ActividadPlanificacion.count() == 0) {
				definirActividadesPlanificacion()
			}

			definirActividadesAS()

			definirTiposCosto()

			definirVersionador()

			if (AnexoListaBlanca.count() == 0){
				definirAnexosListaBlanca()
			}

		}

		def parametros
		if (ParametrosSistema.count() > 0) {
			parametros = ParametrosSistema.list().first()
		} else {
			def gestionEstandar = TipoGestion.findByDescripcion(TipoGestion.GESTION_ESTANDAR)

			parametros = new ParametrosSistema(grupoLDAPUsuarioFinal: "ROL_UF", grupoLDAPInterlocutorUsuario: GRUPO_INTERLOCUTOR_USUARIO, tipoGestionDefault: gestionEstandar, grupoLDAPFuncionGestionDemandaGral: "FGD_ALL",
					grupoLDAPCoordinadorPedido: "ROL_CC", mailEmisorPCS: "needIT_Administracion@ta.telecom.com.ar", cartelAdvertenciaGerentesIT: "Atenci\u00F3n: El Gerente pertenece a un \u00E1rea de IT",
					grupoAdmin: "FAF_NEEDIT",
					tooltipNotasDesarrollo: "1. Tipo de pruebas necesarias - 2. Aspectos de seguridad relevantes - 3. Capacitaci\u00F3n - 4. Restricciones",
					mensajeAdvertenciaValidar: "Atenci\u00F3n: se generar\u00E1 un cambio nuevo",
					valorHoraSWFTecoDefault: 105.31, mailAdministradorFuncional: "needIT_Administracion@ta.telecom.com.ar",
					enviaMails: false, auditaMails: true, minimoJerarquiaAprobador: 40, maximoJerarquiaAprobador: 70,
					minimoJerarquiaGerente: 40, maximoJerarquiaGerente: 90,
					repeticionHoras:0).save(flush: true, failOnError: true)
		}

		Constantes.instance.parametros = ParametrosSistema.list().first()
		Constantes.instance.parametros.traerDatosRelacionados()
		
		

	}

	private def crearRolSiCorresponde(rolDescripcion, seleccionable, propiedadUsrEnvioMails, aplicaPadre, codigoRol) {
		def rol = RolAplicacion.findByCodigoRol(codigoRol)
		if (!rol) {
			rol = new RolAplicacion(descripcion: rolDescripcion, seleccionableEnListados: seleccionable, propiedadUsuariosEnvioMails: propiedadUsrEnvioMails, aplicaPadre: aplicaPadre, codigoRol: codigoRol).save(failOnError: true)
		}
		return rol
	}

	def destroy = {
	}

	private definirRolesReasignacion(){
		if (RolReasignacion.count() == 0) {
			
			def rol_uf = RolAplicacion.findByDescripcion("Usuario Final")
			def rol_gu = RolAplicacion.findByDescripcion("Gerente Usuario")
			def rol_iu = RolAplicacion.findByDescripcion("Interlocutor Usuario")
			def rol_fgd = RolAplicacion.findByDescripcion("Gesti\u00F3n de la Demanda")
			def rol_cc = RolAplicacion.findByDescripcion("Coordinador del Cambio")
			def rol_ai = RolAplicacion.findByDescripcion("Aprobador impacto")
			
			def rea_uf = new RolReasignacion(rol:rol_uf)
			rea_uf.addToRolesAReasignar(rol_uf)
			rea_uf.save(failOnError: true)
			
			def rea_gu = new RolReasignacion(rol:rol_gu)
			rea_gu.addToRolesAReasignar(rol_uf)
			rea_gu.addToRolesAReasignar(rol_gu)
			rea_gu.addToRolesAReasignar(rol_iu)
			rea_gu.addToRolesAReasignar(rol_ai)
			rea_gu.save(failOnError: true)
			
			def rea_iu = new RolReasignacion(rol:rol_iu)
			rea_iu.addToRolesAReasignar(rol_uf)
			rea_iu.addToRolesAReasignar(rol_gu)
			rea_iu.addToRolesAReasignar(rol_iu)
			rea_iu.addToRolesAReasignar(rol_ai)
			rea_iu.save(failOnError: true)
			
			def rea_cc = new RolReasignacion(rol:rol_cc)
			rea_cc.addToRolesAReasignar(rol_uf)
			rea_cc.addToRolesAReasignar(rol_gu)
			rea_cc.addToRolesAReasignar(rol_iu)
			rea_cc.addToRolesAReasignar(rol_cc)
			rea_cc.save(failOnError: true)
			
			def rea_fgd = new RolReasignacion(rol:rol_fgd)
			rea_fgd.addToRolesAReasignar(rol_uf)
			rea_fgd.addToRolesAReasignar(rol_gu)
			rea_fgd.addToRolesAReasignar(rol_iu)
			rea_fgd.addToRolesAReasignar(rol_cc)
			rea_fgd.save(failOnError: true)
			
			def rea_ai = new RolReasignacion(rol:rol_ai)
			rea_ai.addToRolesAReasignar(rol_gu)
			rea_ai.addToRolesAReasignar(rol_iu)
			rea_ai.addToRolesAReasignar(rol_ai)
			rea_ai.save(failOnError: true)
			
			/*def rea_af = new RolReasignacion(rol:rol_af)
			rea_af.addToRolesAReasignar(rol_uf)
			rea_af.addToRolesAReasignar(rol_gu)
			rea_af.addToRolesAReasignar(rol_iu)
			rea_af.addToRolesAReasignar(rol_cc)
			rea_af.addToRolesAReasignar(rol_ap)
			rea_af.save(failOnError: true)*/
				
		}

	}
	
	private definirTiposCosto() {
		// DONE
		if (TipoCosto.count() == 0) {
			new TipoCosto(descripcion: "Hardware").save(failOnError: true)
			new TipoCosto(descripcion: "Licencias (Software)").save(failOnError: true)
			new TipoCosto(descripcion: "Instalaci\u00F3n").save(failOnError: true)
			new TipoCosto(descripcion: "Capacitaci\u00F3n").save(failOnError: true)
			new TipoCosto(descripcion: "Soporte").save(failOnError: true)
			new TipoCosto(descripcion: "Otros Servicios").save(failOnError: true)
		}
	}

	private definirTiposPruebaYJustificaciones() {
		def pruebaPU
		def pruebaPI
		def pruebaPAU
		def estres
		def volumen
		def performance
		def paralelo
		def conversion

		def fap_especificacionCambio = Fase.findByCodigoFase(FaseMultiton.ESPECIFICACION_CAMBIO)
		def fap_consolidacionImpacto = Fase.findByCodigoFase(FaseMultiton.CONSOLIDACION_IMPACTO)

		// DONE
		if (TipoPrueba.count() == 0) {
			pruebaPU = new TipoPrueba(fases: [fap_consolidacionImpacto], descripcion: "PU", opcional: false).save(failOnError: true)
			pruebaPI = new TipoPrueba(fases: [
				fap_consolidacionImpacto,
				fap_especificacionCambio
			], descripcion: "PI", opcional: false).save(failOnError: true)
			pruebaPAU = new TipoPrueba(fases: [
				fap_consolidacionImpacto,
				fap_especificacionCambio
			], descripcion: "PAU", opcional: false).save(failOnError: true)
			estres = new TipoPrueba(fases: [
				fap_consolidacionImpacto,
				fap_especificacionCambio
			], descripcion: "Estr\u00E9s", opcional: true).save(failOnError: true)
			volumen = new TipoPrueba(fases: [
				fap_consolidacionImpacto,
				fap_especificacionCambio
			], descripcion: "Volumen", opcional: true).save(failOnError: true)
			performance = new TipoPrueba(fases: [
				fap_consolidacionImpacto,
				fap_especificacionCambio
			], descripcion: "Performance", opcional: true).save(failOnError: true)
			paralelo = new TipoPrueba(fases: [
				fap_consolidacionImpacto,
				fap_especificacionCambio
			], descripcion: "Paralelo", opcional: true).save(failOnError: true)
			conversion = new TipoPrueba(fases: [
				fap_consolidacionImpacto,
				fap_especificacionCambio
			], descripcion: "Conversi\u00F3n", opcional: true).save(failOnError: true)
		} else {
			pruebaPU = TipoPrueba.findByDescripcion("PU")
			pruebaPI = TipoPrueba.findByDescripcion(TipoPrueba.DESCRIPCION_PI)
			pruebaPAU = TipoPrueba.findByDescripcion(TipoPrueba.DESCRIPCION_PAU)
		}

		def impacto_SWDesa = TipoImpacto.findByDescripcion("SW Desarrollo")
		def impacto_SWDesaSAP = TipoImpacto.findByDescripcion("SW Desarrollo SAP")
		def impacto_AS = TipoImpacto.findByDescripcion("Area de soporte")
		def impacto_DatosRef = TipoImpacto.findByDescripcion("Datos referenciales")
		def impacto_AF = TipoImpacto.findByDescripcion("Administrador funcional")
		def impacto_SopPruebas = TipoImpacto.findByDescripcion("SW Soporte a pruebas")

		def gestionEstandar = TipoGestion.findByDescripcion(TipoGestion.GESTION_ESTANDAR)
		def gestionEmergencia = TipoGestion.findByDescripcion(TipoGestion.GESTION_EMERGENCIA)

		// DONE
		if (ParametrizacionTipoPrueba.count() == 0) {
			new ParametrizacionTipoPrueba(tipoGestion: gestionEstandar, tipoPrueba: pruebaPU, estrategiaRealiza: ParametrizacionTipoPrueba.NO_APLICA, estrategiaConsolidacion: ParametrizacionTipoPrueba.NO_APLICA, tipoImpacto: null).save(failOnError: true)
			new ParametrizacionTipoPrueba(tipoGestion: gestionEstandar, tipoPrueba: pruebaPI, estrategiaRealiza: ParametrizacionTipoPrueba.SI_EDITABLE, estrategiaConsolidacion: ParametrizacionTipoPrueba.SI_EDITABLE, tipoImpacto: null).save(failOnError: true)
			new ParametrizacionTipoPrueba(tipoGestion: gestionEstandar, tipoPrueba: pruebaPAU, estrategiaRealiza: ParametrizacionTipoPrueba.SI_EDITABLE, estrategiaConsolidacion: ParametrizacionTipoPrueba.SI_EDITABLE, tipoImpacto: null).save(failOnError: true)
			new ParametrizacionTipoPrueba(tipoGestion: gestionEmergencia, tipoPrueba: pruebaPU, estrategiaRealiza: ParametrizacionTipoPrueba.NO_APLICA, estrategiaConsolidacion: ParametrizacionTipoPrueba.NO_APLICA, tipoImpacto: null).save(failOnError: true)
			new ParametrizacionTipoPrueba(tipoGestion: gestionEmergencia, tipoPrueba: pruebaPI, estrategiaRealiza: ParametrizacionTipoPrueba.NO_APLICA, estrategiaConsolidacion: ParametrizacionTipoPrueba.NO_APLICA, tipoImpacto: null).save(failOnError: true)
			new ParametrizacionTipoPrueba(tipoGestion: gestionEmergencia, tipoPrueba: pruebaPAU, estrategiaRealiza: ParametrizacionTipoPrueba.NO_EDITABLE, estrategiaConsolidacion: ParametrizacionTipoPrueba.NO_EDITABLE, tipoImpacto: null).save(failOnError: true)

			new ParametrizacionTipoPrueba(tipoGestion: gestionEstandar, tipoPrueba: pruebaPU, estrategiaRealiza: ParametrizacionTipoPrueba.SI_EDITABLE, estrategiaConsolidacion: ParametrizacionTipoPrueba.NO_GRISADO, tipoImpacto: impacto_SWDesa).save(failOnError: true)
			new ParametrizacionTipoPrueba(tipoGestion: gestionEstandar, tipoPrueba: pruebaPI, estrategiaRealiza: ParametrizacionTipoPrueba.SI_EDITABLE, estrategiaConsolidacion: ParametrizacionTipoPrueba.SI_EDITABLE, tipoImpacto: impacto_SWDesa).save(failOnError: true)
			new ParametrizacionTipoPrueba(tipoGestion: gestionEstandar, tipoPrueba: pruebaPAU, estrategiaRealiza: ParametrizacionTipoPrueba.SI_EDITABLE, estrategiaConsolidacion: ParametrizacionTipoPrueba.SI_EDITABLE, tipoImpacto: impacto_SWDesa).save(failOnError: true)
			new ParametrizacionTipoPrueba(tipoGestion: gestionEmergencia, tipoPrueba: pruebaPU, estrategiaRealiza: ParametrizacionTipoPrueba.SI_EDITABLE, estrategiaConsolidacion: ParametrizacionTipoPrueba.NO_GRISADO, tipoImpacto: impacto_SWDesa).save(failOnError: true)
			new ParametrizacionTipoPrueba(tipoGestion: gestionEmergencia, tipoPrueba: pruebaPI, estrategiaRealiza: ParametrizacionTipoPrueba.NO_GRISADO, estrategiaConsolidacion: ParametrizacionTipoPrueba.NO_GRISADO, tipoImpacto: impacto_SWDesa).save(failOnError: true)
			new ParametrizacionTipoPrueba(tipoGestion: gestionEmergencia, tipoPrueba: pruebaPAU, estrategiaRealiza: ParametrizacionTipoPrueba.NO_GRISADO, estrategiaConsolidacion: ParametrizacionTipoPrueba.NO_GRISADO, tipoImpacto: impacto_SWDesa).save(failOnError: true)

			new ParametrizacionTipoPrueba(tipoGestion: gestionEstandar, tipoPrueba: pruebaPU, estrategiaRealiza: ParametrizacionTipoPrueba.NO_EDITABLE, estrategiaConsolidacion: ParametrizacionTipoPrueba.NO_GRISADO, tipoImpacto: impacto_DatosRef).save(failOnError: true)
			new ParametrizacionTipoPrueba(tipoGestion: gestionEstandar, tipoPrueba: pruebaPI, estrategiaRealiza: ParametrizacionTipoPrueba.NO_GRISADO, estrategiaConsolidacion: ParametrizacionTipoPrueba.NO_GRISADO, tipoImpacto: impacto_DatosRef).save(failOnError: true)
			new ParametrizacionTipoPrueba(tipoGestion: gestionEstandar, tipoPrueba: pruebaPAU, estrategiaRealiza: ParametrizacionTipoPrueba.SI_EDITABLE, estrategiaConsolidacion: ParametrizacionTipoPrueba.NO_EDITABLE, tipoImpacto: impacto_DatosRef).save(failOnError: true)
			new ParametrizacionTipoPrueba(tipoGestion: gestionEmergencia, tipoPrueba: pruebaPU, estrategiaRealiza: ParametrizacionTipoPrueba.NO_EDITABLE, estrategiaConsolidacion: ParametrizacionTipoPrueba.NO_GRISADO, tipoImpacto: impacto_DatosRef).save(failOnError: true)
			new ParametrizacionTipoPrueba(tipoGestion: gestionEmergencia, tipoPrueba: pruebaPI, estrategiaRealiza: ParametrizacionTipoPrueba.NO_GRISADO, estrategiaConsolidacion: ParametrizacionTipoPrueba.NO_GRISADO, tipoImpacto: impacto_DatosRef).save(failOnError: true)
			new ParametrizacionTipoPrueba(tipoGestion: gestionEmergencia, tipoPrueba: pruebaPAU, estrategiaRealiza: ParametrizacionTipoPrueba.SI_EDITABLE, estrategiaConsolidacion: ParametrizacionTipoPrueba.NO_EDITABLE, tipoImpacto: impacto_DatosRef).save(failOnError: true)

			new ParametrizacionTipoPrueba(tipoGestion: gestionEstandar, tipoPrueba: pruebaPU, estrategiaRealiza: ParametrizacionTipoPrueba.NO_APLICA, estrategiaConsolidacion: ParametrizacionTipoPrueba.NO_APLICA, tipoImpacto: impacto_SopPruebas).save(failOnError: true)
			new ParametrizacionTipoPrueba(tipoGestion: gestionEstandar, tipoPrueba: pruebaPI, estrategiaRealiza: ParametrizacionTipoPrueba.SI_EDITABLE, estrategiaConsolidacion: ParametrizacionTipoPrueba.SI_EDITABLE, tipoImpacto: impacto_SopPruebas).save(failOnError: true)
			new ParametrizacionTipoPrueba(tipoGestion: gestionEstandar, tipoPrueba: pruebaPAU, estrategiaRealiza: ParametrizacionTipoPrueba.SI_EDITABLE, estrategiaConsolidacion: ParametrizacionTipoPrueba.SI_EDITABLE, tipoImpacto: impacto_SopPruebas).save(failOnError: true)
			new ParametrizacionTipoPrueba(tipoGestion: gestionEmergencia, tipoPrueba: pruebaPU, estrategiaRealiza: ParametrizacionTipoPrueba.NO_APLICA, estrategiaConsolidacion: ParametrizacionTipoPrueba.NO_APLICA, tipoImpacto: impacto_SopPruebas).save(failOnError: true)
			new ParametrizacionTipoPrueba(tipoGestion: gestionEmergencia, tipoPrueba: pruebaPI, estrategiaRealiza: ParametrizacionTipoPrueba.NO_EDITABLE, estrategiaConsolidacion: ParametrizacionTipoPrueba.NO_EDITABLE, tipoImpacto: impacto_SopPruebas).save(failOnError: true)
			new ParametrizacionTipoPrueba(tipoGestion: gestionEmergencia, tipoPrueba: pruebaPAU, estrategiaRealiza: ParametrizacionTipoPrueba.NO_EDITABLE, estrategiaConsolidacion: ParametrizacionTipoPrueba.NO_EDITABLE, tipoImpacto: impacto_SopPruebas).save(failOnError: true)

			new ParametrizacionTipoPrueba(tipoGestion: gestionEstandar, tipoPrueba: pruebaPU, estrategiaRealiza: ParametrizacionTipoPrueba.SI_EDITABLE, estrategiaConsolidacion: ParametrizacionTipoPrueba.NO_GRISADO, tipoImpacto: impacto_SWDesaSAP).save(failOnError: true)
			new ParametrizacionTipoPrueba(tipoGestion: gestionEstandar, tipoPrueba: pruebaPI, estrategiaRealiza: ParametrizacionTipoPrueba.SI_EDITABLE, estrategiaConsolidacion: ParametrizacionTipoPrueba.SI_EDITABLE, tipoImpacto: impacto_SWDesaSAP).save(failOnError: true)
			new ParametrizacionTipoPrueba(tipoGestion: gestionEstandar, tipoPrueba: pruebaPAU, estrategiaRealiza: ParametrizacionTipoPrueba.SI_EDITABLE, estrategiaConsolidacion: ParametrizacionTipoPrueba.SI_EDITABLE, tipoImpacto: impacto_SWDesaSAP).save(failOnError: true)
			new ParametrizacionTipoPrueba(tipoGestion: gestionEmergencia, tipoPrueba: pruebaPU, estrategiaRealiza: ParametrizacionTipoPrueba.SI_EDITABLE, estrategiaConsolidacion: ParametrizacionTipoPrueba.NO_GRISADO, tipoImpacto: impacto_SWDesaSAP).save(failOnError: true)
			new ParametrizacionTipoPrueba(tipoGestion: gestionEmergencia, tipoPrueba: pruebaPI, estrategiaRealiza: ParametrizacionTipoPrueba.SI_EDITABLE, estrategiaConsolidacion: ParametrizacionTipoPrueba.SI_EDITABLE, tipoImpacto: impacto_SWDesaSAP).save(failOnError: true)
			new ParametrizacionTipoPrueba(tipoGestion: gestionEmergencia, tipoPrueba: pruebaPAU, estrategiaRealiza: ParametrizacionTipoPrueba.SI_EDITABLE, estrategiaConsolidacion: ParametrizacionTipoPrueba.SI_EDITABLE, tipoImpacto: impacto_SWDesaSAP).save(failOnError: true)
		}

		// DONE
		if (Justificacion.count() == 0) {

			new Justificacion(tipoPrueba: pruebaPI, descripcion: "El sistema no tiene impacto en el cambio (solo para Hijos Soporte a Pruebas)").save(failOnError: true)
			new Justificacion(tipoPrueba: pruebaPI, descripcion: "El sistema s\u00F3lo participa en provisi\u00F3n de datos o soporte").save(failOnError: true)
			new Justificacion(tipoPrueba: pruebaPI, descripcion: "La PI se realizar\u00E1 junto a la PAU").save(failOnError: true)
			new Justificacion(tipoPrueba: pruebaPI, descripcion: "El IU define que la prueba se realizar\u00E1 en producci\u00F3n").save(failOnError: true)
			new Justificacion(tipoPrueba: pruebaPI, descripcion: "No se requiere PI para este sistema").save(failOnError: true)

			new Justificacion(tipoPrueba: pruebaPAU, descripcion: "El sistema no tiene impacto en el cambio (solo para Hijos Soporte a Pruebas)").save(failOnError: true)
			new Justificacion(tipoPrueba: pruebaPAU, descripcion: "El sistema s\u00F3lo participa en provisi\u00F3n de datos o soporte").save(failOnError: true)
			new Justificacion(tipoPrueba: pruebaPAU, descripcion: "El IU define que la prueba se realizar\u00E1 en producci\u00F3n").save(failOnError: true)

			new Justificacion(tipoPrueba: pruebaPU, descripcion: "No se requiere PU para este sistema").save(failOnError: true)
			new Justificacion(tipoPrueba: pruebaPU, descripcion: "El sistema s\u00F3lo participa en provisi\u00F3n de datos o soporte").save(failOnError: true)
			new Justificacion(tipoPrueba: pruebaPU, descripcion: "El sistema posee desarrollo externo").save(failOnError: true)

			new Justificacion(tipoPrueba: estres, descripcion: "No se requieren pruebas de estr\u00E9s para este sistema").save(failOnError: true)
			new Justificacion(tipoPrueba: volumen, descripcion: "No se requieren pruebas de volumen para este sistema").save(failOnError: true)
			new Justificacion(tipoPrueba: performance, descripcion: "No se requieren pruebas de performance para este sistema").save(failOnError: true)
			new Justificacion(tipoPrueba: paralelo, descripcion: "No se requieren pruebas de paralelo para este sistema").save(failOnError: true)
			new Justificacion(tipoPrueba: conversion, descripcion: "No se requieren pruebas de conversi\u00F3n paara este sistema").save(failOnError: true)
		}

		if (JustificacionDisenioExterno.count() == 0) {
			new JustificacionDisenioExterno(descripcion: "El cambio no impacta en el diseño de la aplicaci\u00F3n").save(failOnError: true)
		}
	}

	//DONE
	private definirTiposActividades() {
		def pruebaPU = TipoPrueba.findByDescripcion("PU")
		def pruebaPI = TipoPrueba.findByDescripcion(TipoPrueba.DESCRIPCION_PI)
		def pruebaPAU = TipoPrueba.findByDescripcion(TipoPrueba.DESCRIPCION_PAU)
		def pruebaEstres = TipoPrueba.findByDescripcion("Estr\u00E9s")
		def pruebaVolumen = TipoPrueba.findByDescripcion("Volumen")
		def pruebaPerf = TipoPrueba.findByDescripcion("Performance")
		def pruebaParal = TipoPrueba.findByDescripcion("Paralelo")
		def pruebaConv = TipoPrueba.findByDescripcion("Conversi\u00F3n")

		def tailoringPU
		def tailoringPI
		def tailoringPAU
		def tailoringEstres
		def tailoringVolumen
		def tailoringPerf
		def tailoringParal
		def tailoringConv

		if (TailoringEstrategiaPrueba.count() == 0) {
			tailoringPU = new TailoringEstrategiaPrueba(tipoPrueba: pruebaPU).save(failOnError: true)
			tailoringPI = new TailoringEstrategiaPrueba(tipoPrueba: pruebaPI).save(failOnError: true)
			tailoringPAU = new TailoringEstrategiaPrueba(tipoPrueba: pruebaPAU).save(failOnError: true)
			tailoringEstres = new TailoringEstrategiaPrueba(tipoPrueba: pruebaEstres).save(failOnError: true)
			tailoringVolumen = new TailoringEstrategiaPrueba(tipoPrueba: pruebaVolumen).save(failOnError: true)
			tailoringPerf = new TailoringEstrategiaPrueba(tipoPrueba: pruebaPerf).save(failOnError: true)
			tailoringParal = new TailoringEstrategiaPrueba(tipoPrueba: pruebaParal).save(failOnError: true)
			tailoringConv = new TailoringEstrategiaPrueba(tipoPrueba: pruebaConv).save(failOnError: true)
		} else {
			tailoringPU = TailoringEstrategiaPrueba.findByTipoPrueba(pruebaPU)
			tailoringPI = TailoringEstrategiaPrueba.findByTipoPrueba(pruebaPI)
			tailoringPAU = TailoringEstrategiaPrueba.findByTipoPrueba(pruebaPAU)
			tailoringEstres = TailoringEstrategiaPrueba.findByTipoPrueba(pruebaEstres)
			tailoringVolumen = TailoringEstrategiaPrueba.findByTipoPrueba(pruebaVolumen)
			tailoringPerf = TailoringEstrategiaPrueba.findByTipoPrueba(pruebaPerf)
			tailoringParal = TailoringEstrategiaPrueba.findByTipoPrueba(pruebaParal)
			tailoringConv = TailoringEstrategiaPrueba.findByTipoPrueba(pruebaConv)
		}

		def validacionAnexosURL
		def validacionAnexosMercury
		def validacionDefault

		if (ValidacionCruceCamposAnexos.count() == 0) {
			validacionAnexosURL = new ValidacionCruceCamposAnexos(descripcion: "Anexos - URL")
			validacionAnexosURL.agregarCampoAValidar "url"
			validacionAnexosURL.save(failOnError: true)

			validacionAnexosMercury = new ValidacionCruceCamposAnexos(descripcion: "Anexos - Mercury")
			validacionAnexosMercury.agregarCampoAValidar "proyectoMercury"
			validacionAnexosMercury.save(failOnError: true)

			validacionDefault = new CriterioValidacionActividad(descripcion: "Default").save(failOnError: true)

		} else {
			validacionAnexosURL = ValidacionCruceCamposAnexos.findByDescripcion("Anexos - URL")
			validacionAnexosMercury = ValidacionCruceCamposAnexos.findByDescripcion("Anexos - Mercury")
			validacionDefault = CriterioValidacionActividad.findByDescripcion("Default")
		}

		def gestionEstandar = TipoGestion.findByDescripcion(TipoGestion.GESTION_ESTANDAR)
		def gestionEmergencia = TipoGestion.findByDescripcion(TipoGestion.GESTION_EMERGENCIA)

		def fah_ejecucionSoportePAU = Fase.findByCodigoFase(FaseMultiton.EJECUCION_SOPORTE_PAU_HIJO)
		def fah_construccionCambio = Fase.findByCodigoFase(FaseMultiton.CONSTRUCCION_CAMBIO_HIJO)
		def fah_adminActividades = Fase.findByCodigoFase(FaseMultiton.ADMINISTRAR_ACTIVIDADES_HIJO)
		def fah_construccionPU = Fase.findByCodigoFase(FaseMultiton.CONSTRUCCION_PU_HIJO)
		def fah_implantacion = Fase.findByCodigoFase(FaseMultiton.IMPLANTACION_HIJO)
		def fah_normalizacion = Fase.findByCodigoFase(FaseMultiton.NORMALIZACION_HIJO)

		def impacto_SWDesa = TipoImpacto.findByDescripcion("SW Desarrollo")
		def impacto_SWDesaSAP = TipoImpacto.findByDescripcion("SW Desarrollo SAP")
		def impacto_AS = TipoImpacto.findByDescripcion("Area de soporte")
		def impacto_DatosRef = TipoImpacto.findByDescripcion("Datos referenciales")
		def impacto_AF = TipoImpacto.findByDescripcion("Administrador funcional")
		def impacto_SopPruebas = TipoImpacto.findByDescripcion("SW Soporte a pruebas")

		def n_a = TipoActividad.CAMPO_NO_APLICA
		def opc = TipoActividad.CAMPO_OPCIONAL
		def obl = TipoActividad.CAMPO_OBLIGATORIO

		// Construcci\u00F3n Cambio
		new TipoActividad(tipoGestion: gestionEstandar, tipoImpacto: impacto_SWDesa, fase: fah_construccionCambio, descripcion: "Armar Ambiente PI", estrategiaAmbiente: obl, obligatoriedad: TipoActividad.TAILORING_OPCIONAL, criterioTailoring: tailoringPI).save(failOnError: true)
		new TipoActividad(tipoGestion: gestionEstandar, tipoImpacto: impacto_SopPruebas, fase: fah_construccionCambio, descripcion: "Armar Ambiente PI", estrategiaAmbiente: obl, obligatoriedad: TipoActividad.TAILORING_OPCIONAL, criterioTailoring: tailoringPI).save(failOnError: true)
		new TipoActividad(tipoGestion: gestionEmergencia, tipoImpacto: impacto_SWDesa, fase: fah_construccionCambio, descripcion: "Armar Ambiente PI", estrategiaAmbiente: obl, obligatoriedad: TipoActividad.TAILORING_OPCIONAL, criterioTailoring: tailoringPI).save(failOnError: true)
		new TipoActividad(tipoGestion: gestionEstandar, tipoImpacto: impacto_SWDesa, fase: fah_construccionCambio, descripcion: "Armar Ambiente PU", estrategiaAmbiente: obl, obligatoriedad: TipoActividad.TAILORING_OPCIONAL, criterioTailoring: tailoringPU).save(failOnError: true)
		new TipoActividad(tipoGestion: gestionEmergencia, tipoImpacto: impacto_SWDesa, fase: fah_construccionCambio, descripcion: "Armar Ambiente PU", estrategiaAmbiente: obl, obligatoriedad: TipoActividad.TAILORING_OPCIONAL, criterioTailoring: tailoringPU).save(failOnError: true)
		new TipoActividad(tipoGestion: gestionEstandar, tipoImpacto: impacto_SWDesa, fase: fah_construccionCambio, descripcion: "Brindar Soporte PI", estrategiaComentarios: opc, obligatoriedad: TipoActividad.TAILORING_OPCIONAL, criterioTailoring: tailoringPI).save(failOnError: true)
		new TipoActividad(tipoGestion: gestionEstandar, tipoImpacto: impacto_SopPruebas, fase: fah_construccionCambio, descripcion: "Brindar Soporte PI", estrategiaComentarios: opc, obligatoriedad: TipoActividad.TAILORING, criterioTailoring: tailoringPI).save(failOnError: true)
		new TipoActividad(tipoGestion: gestionEstandar, tipoImpacto: impacto_SWDesa, fase: fah_construccionCambio, descripcion: "Capacitar \u00E1reas T\u00E9cnicas", estrategiaComentarios: obl, estrategiaURL: opc, obligatoriedad: TipoActividad.OPCIONAL).save(failOnError: true)
		new TipoActividad(tipoGestion: gestionEmergencia, tipoImpacto: impacto_SWDesa, fase: fah_construccionCambio, descripcion: "Capacitar \u00E1reas T\u00E9cnicas", estrategiaComentarios: obl, estrategiaURL: opc, obligatoriedad: TipoActividad.OPCIONAL).save(failOnError: true)
		new TipoActividad(tipoGestion: gestionEstandar, tipoImpacto: impacto_SWDesa, fase: fah_construccionCambio, descripcion: "Capacitar Negocio", estrategiaComentarios: obl, estrategiaURL: opc, obligatoriedad: TipoActividad.OPCIONAL).save(failOnError: true)
		new TipoActividad(tipoGestion: gestionEmergencia, tipoImpacto: impacto_SWDesa, fase: fah_construccionCambio, descripcion: "Capacitar Negocio", estrategiaComentarios: obl, estrategiaURL: opc, obligatoriedad: TipoActividad.OPCIONAL).save(failOnError: true)
		new TipoActividad(tipoGestion: gestionEstandar, tipoImpacto: impacto_SWDesa, fase: fah_construccionCambio, descripcion: "Construir", estrategiaAmbiente: obl, estrategiaVersionador: opc, obligatoriedad: TipoActividad.OBLIGATORIO).save(failOnError: true)
		new TipoActividad(tipoGestion: gestionEstandar, tipoImpacto: impacto_DatosRef, fase: fah_construccionCambio, descripcion: "Construir", estrategiaAmbiente: obl, obligatoriedad: TipoActividad.OBLIGATORIO).save(failOnError: true)
		new TipoActividad(tipoGestion: gestionEmergencia, tipoImpacto: impacto_SWDesa, fase: fah_construccionCambio, descripcion: "Construir", estrategiaAmbiente: obl, estrategiaVersionador: opc, obligatoriedad: TipoActividad.OBLIGATORIO).save(failOnError: true)
		new TipoActividad(tipoGestion: gestionEmergencia, tipoImpacto: impacto_DatosRef, fase: fah_construccionCambio, descripcion: "Construir", estrategiaAmbiente: obl, obligatoriedad: TipoActividad.OBLIGATORIO).save(failOnError: true)
		new TipoActividad(tipoGestion: gestionEstandar, tipoImpacto: impacto_SWDesa, fase: fah_construccionCambio, descripcion: "Documentar Diseño Interno", estrategiaURL: opc, obligatoriedad: TipoActividad.OBLIGATORIO, criterioValidacionActividad: validacionAnexosURL).save(failOnError: true)
		new TipoActividad(tipoGestion: gestionEmergencia, tipoImpacto: impacto_SWDesa, fase: fah_construccionCambio, descripcion: "Documentar Diseño Interno", estrategiaURL: opc, obligatoriedad: TipoActividad.OBLIGATORIO, criterioValidacionActividad: validacionAnexosURL).save(failOnError: true)
		new TipoActividad(tipoGestion: gestionEstandar, tipoImpacto: impacto_SWDesa, fase: fah_construccionCambio, descripcion: "Ejecutar PI", estrategiaPasajes: opc, estrategiaMercury: opc, obligatoriedad: TipoActividad.TAILORING, criterioTailoring: tailoringPI, criterioValidacionActividad: validacionAnexosMercury).save(failOnError: true)
		new TipoActividad(tipoGestion: gestionEstandar, tipoImpacto: impacto_SWDesa, fase: fah_construccionCambio, descripcion: "Ejecutar PU", estrategiaAnexos: opc, estrategiaMercury: opc, obligatoriedad: TipoActividad.TAILORING, criterioTailoring: tailoringPU, criterioValidacionActividad: validacionAnexosMercury).save(failOnError: true)
		new TipoActividad(tipoGestion: gestionEmergencia, tipoImpacto: impacto_SWDesa, fase: fah_construccionCambio, descripcion: "Ejecutar PU", estrategiaAnexos: opc, estrategiaMercury: opc, obligatoriedad: TipoActividad.TAILORING, criterioTailoring: tailoringPU, criterioValidacionActividad: validacionAnexosMercury).save(failOnError: true)
		new TipoActividad(tipoGestion: gestionEstandar, tipoImpacto: impacto_SWDesa, fase: fah_construccionCambio, descripcion: "Resolver Errores de PI", estrategiaMercury: opc, obligatoriedad: TipoActividad.OPCIONAL, criterioValidacionActividad: validacionAnexosMercury).save(failOnError: true)
		new TipoActividad(tipoGestion: gestionEstandar, tipoImpacto: impacto_SopPruebas, fase: fah_construccionCambio, descripcion: "Resolver Errores de PI", estrategiaMercury: opc, obligatoriedad: TipoActividad.OPCIONAL, criterioValidacionActividad: validacionAnexosMercury).save(failOnError: true)
		new TipoActividad(tipoGestion: gestionEmergencia, tipoImpacto: impacto_SWDesa, fase: fah_construccionCambio, descripcion: "Resolver Errores de PI", estrategiaMercury: opc, obligatoriedad: TipoActividad.OPCIONAL, criterioValidacionActividad: validacionAnexosMercury).save(failOnError: true)
		new TipoActividad(tipoGestion: gestionEmergencia, tipoImpacto: impacto_SopPruebas, fase: fah_construccionCambio, descripcion: "Resolver Errores de PI", estrategiaMercury: opc, obligatoriedad: TipoActividad.OPCIONAL, criterioValidacionActividad: validacionAnexosMercury).save(failOnError: true)
		new TipoActividad(tipoGestion: gestionEstandar, tipoImpacto: impacto_SWDesa, fase: fah_construccionCambio, descripcion: "Solicitar Casos Prueba PAU", estrategiaMercury: opc, obligatoriedad: TipoActividad.TAILORING_OPCIONAL, criterioTailoring: tailoringPAU, criterioValidacionActividad: validacionAnexosMercury).save(failOnError: true)
		new TipoActividad(tipoGestion: gestionEstandar, tipoImpacto: impacto_SopPruebas, fase: fah_construccionCambio, descripcion: "Solicitar Casos Prueba PAU", estrategiaMercury: opc, obligatoriedad: TipoActividad.TAILORING_OPCIONAL, criterioTailoring: tailoringPAU, criterioValidacionActividad: validacionAnexosMercury).save(failOnError: true)
		new TipoActividad(tipoGestion: gestionEmergencia, tipoImpacto: impacto_SWDesa, fase: fah_construccionCambio, descripcion: "Solicitar Casos Prueba PAU", estrategiaMercury: opc, obligatoriedad: TipoActividad.TAILORING_OPCIONAL, criterioTailoring: tailoringPAU, criterioValidacionActividad: validacionAnexosMercury).save(failOnError: true)
		new TipoActividad(tipoGestion: gestionEmergencia, tipoImpacto: impacto_SopPruebas, fase: fah_construccionCambio, descripcion: "Solicitar Casos Prueba PAU", estrategiaMercury: opc, obligatoriedad: TipoActividad.TAILORING_OPCIONAL, criterioTailoring: tailoringPAU, criterioValidacionActividad: validacionAnexosMercury).save(failOnError: true)
		new TipoActividad(tipoGestion: gestionEstandar, tipoImpacto: impacto_DatosRef, fase: fah_construccionCambio, descripcion: "Solicitar Casos Prueba PI", estrategiaMercury: opc, obligatoriedad: TipoActividad.TAILORING_OPCIONAL, criterioTailoring: tailoringPI, criterioValidacionActividad: validacionAnexosMercury).save(failOnError: true)
		new TipoActividad(tipoGestion: gestionEstandar, tipoImpacto: impacto_SopPruebas, fase: fah_construccionCambio, descripcion: "Solicitar Casos Prueba PI", estrategiaMercury: opc, obligatoriedad: TipoActividad.TAILORING_OPCIONAL, criterioTailoring: tailoringPI, criterioValidacionActividad: validacionAnexosMercury).save(failOnError: true)

		// Ejecuci\u00F3n y Soporte PAU
		new TipoActividad(tipoGestion: gestionEstandar, tipoImpacto: impacto_SWDesa, fase: fah_ejecucionSoportePAU, descripcion: "Armar Ambientes Conversi\u00F3n", estrategiaAmbiente: obl, obligatoriedad: TipoActividad.TAILORING_OPCIONAL, criterioTailoring: tailoringConv).save(failOnError: true)
		new TipoActividad(tipoGestion: gestionEstandar, tipoImpacto: impacto_SWDesa, fase: fah_ejecucionSoportePAU, descripcion: "Armar Ambientes Estr\u00E9s", estrategiaAmbiente: obl, obligatoriedad: TipoActividad.TAILORING_OPCIONAL, criterioTailoring: tailoringEstres).save(failOnError: true)
		new TipoActividad(tipoGestion: gestionEstandar, tipoImpacto: impacto_SWDesa, fase: fah_ejecucionSoportePAU, descripcion: "Armar Ambientes Paralelo", estrategiaAmbiente: obl, obligatoriedad: TipoActividad.TAILORING_OPCIONAL, criterioTailoring: tailoringParal).save(failOnError: true)
		new TipoActividad(tipoGestion: gestionEstandar, tipoImpacto: impacto_SWDesa, fase: fah_ejecucionSoportePAU, descripcion: "Armar Ambientes PAU", estrategiaAmbiente: obl, obligatoriedad: TipoActividad.TAILORING_OPCIONAL, criterioTailoring: tailoringPAU).save(failOnError: true)
		new TipoActividad(tipoGestion: gestionEstandar, tipoImpacto: impacto_SopPruebas, fase: fah_ejecucionSoportePAU, descripcion: "Armar Ambientes PAU", estrategiaAmbiente: obl, obligatoriedad: TipoActividad.TAILORING_OPCIONAL, criterioTailoring: tailoringPAU).save(failOnError: true)
		new TipoActividad(tipoGestion: gestionEmergencia, tipoImpacto: impacto_SopPruebas, fase: fah_ejecucionSoportePAU, descripcion: "Armar Ambientes PAU", estrategiaAmbiente: obl, obligatoriedad: TipoActividad.TAILORING_OPCIONAL, criterioTailoring: tailoringPAU).save(failOnError: true)
		new TipoActividad(tipoGestion: gestionEstandar, tipoImpacto: impacto_SWDesa, fase: fah_ejecucionSoportePAU, descripcion: "Armar Ambientes Performance", estrategiaAmbiente: obl, obligatoriedad: TipoActividad.TAILORING_OPCIONAL, criterioTailoring: tailoringPerf).save(failOnError: true)
		new TipoActividad(tipoGestion: gestionEstandar, tipoImpacto: impacto_SWDesa, fase: fah_ejecucionSoportePAU, descripcion: "Armar Ambientes Volumen", estrategiaAmbiente: obl, obligatoriedad: TipoActividad.TAILORING_OPCIONAL, criterioTailoring: tailoringVolumen).save(failOnError: true)
		new TipoActividad(tipoGestion: gestionEstandar, tipoImpacto: impacto_SWDesa, fase: fah_ejecucionSoportePAU, descripcion: "Brindar Soporte Conversi\u00F3n", estrategiaComentarios: opc, obligatoriedad: TipoActividad.TAILORING_OPCIONAL, criterioTailoring: tailoringConv).save(failOnError: true)
		new TipoActividad(tipoGestion: gestionEstandar, tipoImpacto: impacto_SWDesa, fase: fah_ejecucionSoportePAU, descripcion: "Brindar Soporte Estr\u00E9s", estrategiaComentarios: opc, obligatoriedad: TipoActividad.TAILORING_OPCIONAL, criterioTailoring: tailoringEstres).save(failOnError: true)
		new TipoActividad(tipoGestion: gestionEstandar, tipoImpacto: impacto_SWDesa, fase: fah_ejecucionSoportePAU, descripcion: "Brindar Soporte Paralelo", estrategiaComentarios: opc, obligatoriedad: TipoActividad.TAILORING_OPCIONAL, criterioTailoring: tailoringParal).save(failOnError: true)
		new TipoActividad(tipoGestion: gestionEstandar, tipoImpacto: impacto_SWDesa, fase: fah_ejecucionSoportePAU, descripcion: "Brindar Soporte PAU", estrategiaComentarios: opc, obligatoriedad: TipoActividad.TAILORING_OPCIONAL, criterioTailoring: tailoringPAU).save(failOnError: true)
		new TipoActividad(tipoGestion: gestionEstandar, tipoImpacto: impacto_DatosRef, fase: fah_ejecucionSoportePAU, descripcion: "Brindar Soporte PAU", estrategiaComentarios: opc, obligatoriedad: TipoActividad.TAILORING_OPCIONAL, criterioTailoring: tailoringPAU).save(failOnError: true)
		new TipoActividad(tipoGestion: gestionEstandar, tipoImpacto: impacto_SopPruebas, fase: fah_ejecucionSoportePAU, descripcion: "Brindar Soporte PAU", estrategiaComentarios: opc, obligatoriedad: TipoActividad.TAILORING, criterioTailoring: tailoringPAU).save(failOnError: true)
		new TipoActividad(tipoGestion: gestionEmergencia, tipoImpacto: impacto_DatosRef, fase: fah_ejecucionSoportePAU, descripcion: "Brindar Soporte PAU", estrategiaComentarios: opc, obligatoriedad: TipoActividad.TAILORING_OPCIONAL, criterioTailoring: tailoringPAU).save(failOnError: true)
		new TipoActividad(tipoGestion: gestionEmergencia, tipoImpacto: impacto_SopPruebas, fase: fah_ejecucionSoportePAU, descripcion: "Brindar Soporte PAU", estrategiaComentarios: opc, obligatoriedad: TipoActividad.TAILORING, criterioTailoring: tailoringPAU).save(failOnError: true)
		new TipoActividad(tipoGestion: gestionEstandar, tipoImpacto: impacto_SWDesa, fase: fah_ejecucionSoportePAU, descripcion: "Brindar Soporte Performance", estrategiaComentarios: opc, obligatoriedad: TipoActividad.TAILORING_OPCIONAL, criterioTailoring: tailoringPerf).save(failOnError: true)
		new TipoActividad(tipoGestion: gestionEstandar, tipoImpacto: impacto_SWDesa, fase: fah_ejecucionSoportePAU, descripcion: "Brindar Soporte Volumen", estrategiaComentarios: opc, obligatoriedad: TipoActividad.TAILORING_OPCIONAL, criterioTailoring: tailoringVolumen).save(failOnError: true)
		new TipoActividad(tipoGestion: gestionEmergencia, tipoImpacto: impacto_SWDesa, fase: fah_ejecucionSoportePAU, descripcion: "Capacitar \u00E1reas T\u00E9cnicas", estrategiaComentarios: obl, estrategiaURL: opc, obligatoriedad: TipoActividad.OPCIONAL).save(failOnError: true)
		new TipoActividad(tipoGestion: gestionEstandar, tipoImpacto: impacto_SWDesa, fase: fah_ejecucionSoportePAU, descripcion: "Capacitar \u00E1reas T\u00E9cnicas", estrategiaComentarios: obl, estrategiaURL: opc, obligatoriedad: TipoActividad.OPCIONAL).save(failOnError: true)
		new TipoActividad(tipoGestion: gestionEstandar, tipoImpacto: impacto_SWDesa, fase: fah_ejecucionSoportePAU, descripcion: "Capacitar Negocio", estrategiaComentarios: obl, estrategiaURL: opc, obligatoriedad: TipoActividad.OPCIONAL).save(failOnError: true)
		new TipoActividad(tipoGestion: gestionEmergencia, tipoImpacto: impacto_SWDesa, fase: fah_ejecucionSoportePAU, descripcion: "Capacitar Negocio", estrategiaComentarios: obl, estrategiaURL: opc, obligatoriedad: TipoActividad.OPCIONAL).save(failOnError: true)
		new TipoActividad(tipoGestion: gestionEstandar, tipoImpacto: impacto_SWDesa, fase: fah_ejecucionSoportePAU, descripcion: "Ejecutar Prueba Conversi\u00F3n", estrategiaAnexos: opc, estrategiaMercury: opc, obligatoriedad: TipoActividad.TAILORING, criterioTailoring: tailoringConv, criterioValidacionActividad: validacionAnexosMercury).save(failOnError: true)
		new TipoActividad(tipoGestion: gestionEstandar, tipoImpacto: impacto_SWDesa, fase: fah_ejecucionSoportePAU, descripcion: "Ejecutar Prueba Estr\u00E9s", estrategiaAnexos: opc, estrategiaMercury: opc, obligatoriedad: TipoActividad.TAILORING, criterioTailoring: tailoringEstres, criterioValidacionActividad: validacionAnexosMercury).save(failOnError: true)
		new TipoActividad(tipoGestion: gestionEstandar, tipoImpacto: impacto_SWDesa, fase: fah_ejecucionSoportePAU, descripcion: "Ejecutar Prueba Paralelo", estrategiaAnexos: opc, estrategiaMercury: opc, obligatoriedad: TipoActividad.TAILORING, criterioTailoring: tailoringParal, criterioValidacionActividad: validacionAnexosMercury).save(failOnError: true)
		new TipoActividad(tipoGestion: gestionEstandar, tipoImpacto: impacto_SWDesa, fase: fah_ejecucionSoportePAU, descripcion: "Ejecutar Prueba PAU", estrategiaMercury: opc, estrategiaPasajes: opc, obligatoriedad: TipoActividad.TAILORING, criterioTailoring: tailoringPAU, criterioValidacionActividad: validacionAnexosMercury).save(failOnError: true)
		new TipoActividad(tipoGestion: gestionEstandar, tipoImpacto: impacto_DatosRef, fase: fah_ejecucionSoportePAU, descripcion: "Ejecutar Prueba PAU", estrategiaMercury: opc, estrategiaPasajes: opc, obligatoriedad: TipoActividad.TAILORING, criterioTailoring: tailoringPAU, criterioValidacionActividad: validacionAnexosMercury).save(failOnError: true)
		new TipoActividad(tipoGestion: gestionEmergencia, tipoImpacto: impacto_DatosRef, fase: fah_ejecucionSoportePAU, descripcion: "Ejecutar Prueba PAU", estrategiaMercury: opc, estrategiaPasajes: opc, obligatoriedad: TipoActividad.TAILORING, criterioTailoring: tailoringPAU, criterioValidacionActividad: validacionAnexosMercury).save(failOnError: true)
		new TipoActividad(tipoGestion: gestionEstandar, tipoImpacto: impacto_SWDesa, fase: fah_ejecucionSoportePAU, descripcion: "Ejecutar Prueba Performance", estrategiaAnexos: opc, estrategiaMercury: opc, obligatoriedad: TipoActividad.TAILORING, criterioTailoring: tailoringPerf, criterioValidacionActividad: validacionAnexosMercury).save(failOnError: true)
		new TipoActividad(tipoGestion: gestionEstandar, tipoImpacto: impacto_SWDesa, fase: fah_ejecucionSoportePAU, descripcion: "Ejecutar Prueba Volumen", estrategiaAnexos: opc, estrategiaMercury: opc, obligatoriedad: TipoActividad.TAILORING, criterioTailoring: tailoringVolumen, criterioValidacionActividad: validacionAnexosMercury).save(failOnError: true)
		new TipoActividad(tipoGestion: gestionEstandar, tipoImpacto: impacto_SWDesa, fase: fah_ejecucionSoportePAU, descripcion: "Implementar en PAU", estrategiaAnexos: n_a, estrategiaPasajes: obl, estrategiaRelease: opc, obligatoriedad: TipoActividad.TAILORING_OPCIONAL, criterioTailoring: tailoringPAU).save(failOnError: true)
		new TipoActividad(tipoGestion: gestionEstandar, tipoImpacto: impacto_DatosRef, fase: fah_ejecucionSoportePAU, descripcion: "Implementar en PAU", estrategiaAnexos: n_a, estrategiaPasajes: obl, estrategiaRelease: opc, obligatoriedad: TipoActividad.TAILORING_OPCIONAL, criterioTailoring: tailoringPAU).save(failOnError: true)
		new TipoActividad(tipoGestion: gestionEmergencia, tipoImpacto: impacto_DatosRef, fase: fah_ejecucionSoportePAU, descripcion: "Implementar en PAU", estrategiaAnexos: n_a, estrategiaPasajes: obl, estrategiaRelease: opc, obligatoriedad: TipoActividad.TAILORING_OPCIONAL, criterioTailoring: tailoringPAU).save(failOnError: true)
		new TipoActividad(tipoGestion: gestionEstandar, tipoImpacto: impacto_SWDesa, fase: fah_ejecucionSoportePAU, descripcion: "Resolver errores PAU", estrategiaMercury: opc, obligatoriedad: TipoActividad.OPCIONAL, criterioTailoring: tailoringPAU, criterioValidacionActividad: validacionAnexosMercury).save(failOnError: true)
		new TipoActividad(tipoGestion: gestionEstandar, tipoImpacto: impacto_DatosRef, fase: fah_ejecucionSoportePAU, descripcion: "Resolver errores PAU", estrategiaMercury: opc, obligatoriedad: TipoActividad.OPCIONAL, criterioTailoring: tailoringPAU, criterioValidacionActividad: validacionAnexosMercury).save(failOnError: true)
		new TipoActividad(tipoGestion: gestionEstandar, tipoImpacto: impacto_SopPruebas, fase: fah_ejecucionSoportePAU, descripcion: "Resolver errores PAU", estrategiaMercury: opc, obligatoriedad: TipoActividad.TAILORING_OPCIONAL, criterioTailoring: tailoringPAU, criterioValidacionActividad: validacionAnexosMercury).save(failOnError: true)
		new TipoActividad(tipoGestion: gestionEstandar, tipoImpacto: impacto_DatosRef, fase: fah_ejecucionSoportePAU, descripcion: "Resolver errores PAU", estrategiaMercury: opc, obligatoriedad: TipoActividad.OPCIONAL, criterioTailoring: tailoringPAU, criterioValidacionActividad: validacionAnexosMercury).save(failOnError: true)
		new TipoActividad(tipoGestion: gestionEmergencia, tipoImpacto: impacto_SopPruebas, fase: fah_ejecucionSoportePAU, descripcion: "Resolver errores PAU", estrategiaMercury: opc, obligatoriedad: TipoActividad.TAILORING_OPCIONAL, criterioTailoring: tailoringPAU, criterioValidacionActividad: validacionAnexosMercury).save(failOnError: true)

		// Implantaci\u00F3n
		new TipoActividad(tipoGestion: gestionEstandar, tipoImpacto: impacto_SWDesa, fase: fah_implantacion, descripcion: "Implementar en Producci\u00F3n", estrategiaComentarios: opc, estrategiaFechaSugerida: opc, estrategiaAnexos: n_a, estrategiaPasajes: obl, estrategiaAmbiente: n_a, estrategiaVersionador: n_a, estrategiaMercury: n_a, estrategiaRelease: opc, estrategiaURL: n_a, obligatoriedad: TipoActividad.OBLIGATORIO, criterioTailoring: null).save(failOnError: true)
		new TipoActividad(tipoGestion: gestionEstandar, tipoImpacto: impacto_DatosRef, fase: fah_implantacion, descripcion: "Implementar en Producci\u00F3n", estrategiaComentarios: opc, estrategiaFechaSugerida: opc, estrategiaAnexos: n_a, estrategiaPasajes: obl, estrategiaAmbiente: n_a, estrategiaVersionador: n_a, estrategiaMercury: n_a, estrategiaRelease: opc, estrategiaURL: n_a, obligatoriedad: TipoActividad.OBLIGATORIO, criterioTailoring: null).save(failOnError: true)
		new TipoActividad(tipoGestion: gestionEmergencia, tipoImpacto: impacto_SWDesa, fase: fah_implantacion, descripcion: "Implementar en Producci\u00F3n", estrategiaComentarios: opc, estrategiaFechaSugerida: opc, estrategiaAnexos: n_a, estrategiaPasajes: obl, estrategiaAmbiente: n_a, estrategiaVersionador: n_a, estrategiaMercury: n_a, estrategiaRelease: opc, estrategiaURL: n_a, obligatoriedad: TipoActividad.OBLIGATORIO, criterioTailoring: null).save(failOnError: true)
		new TipoActividad(tipoGestion: gestionEmergencia, tipoImpacto: impacto_DatosRef, fase: fah_implantacion, descripcion: "Implementar en Producci\u00F3n", estrategiaComentarios: opc, estrategiaFechaSugerida: opc, estrategiaAnexos: n_a, estrategiaPasajes: obl, estrategiaAmbiente: n_a, estrategiaVersionador: n_a, estrategiaMercury: n_a, estrategiaRelease: opc, estrategiaURL: n_a, obligatoriedad: TipoActividad.OBLIGATORIO, criterioTailoring: null).save(failOnError: true)

		// Normalizaci\u00F3n
		new TipoActividad(tipoGestion: gestionEmergencia, tipoImpacto: impacto_SWDesa, fase: fah_normalizacion, descripcion: "Normalizar", estrategiaComentarios: opc, estrategiaFechaSugerida: opc, estrategiaAnexos: n_a, estrategiaPasajes: obl, estrategiaAmbiente: n_a, estrategiaVersionador: n_a, estrategiaMercury: n_a, estrategiaRelease: obl, estrategiaURL: n_a, obligatoriedad: TipoActividad.OBLIGATORIO, criterioTailoring: null).save(failOnError: true)
		new TipoActividad(tipoGestion: gestionEmergencia, tipoImpacto: impacto_DatosRef, fase: fah_normalizacion, descripcion: "Normalizar", estrategiaComentarios: opc, estrategiaFechaSugerida: opc, estrategiaAnexos: n_a, estrategiaPasajes: obl, estrategiaAmbiente: n_a, estrategiaVersionador: n_a, estrategiaMercury: n_a, estrategiaRelease: obl, estrategiaURL: n_a, obligatoriedad: TipoActividad.OBLIGATORIO, criterioTailoring: null).save(failOnError: true)

		//
		// las de ejecuci\u00F3n y soporte PAU / tipo de gesti\u00F3n est\u00E1ndar
		//

		TipoActividad.list().findAll {  tipoActividad -> !tipoActividad.criterioValidacionActividad  }.each { tipoActividad ->
			tipoActividad.criterioValidacionActividad = validacionDefault
			tipoActividad.save(failOnError: true)
		}
	}

	private def definirActividadesAS() {
		def gestionEstandar = TipoGestion.findByDescripcion(TipoGestion.GESTION_ESTANDAR)
		def gestionEmergencia = TipoGestion.findByDescripcion(TipoGestion.GESTION_EMERGENCIA)

		def fah_construccionCambio = Fase.findByCodigoFase(FaseMultiton.CONSTRUCCION_CAMBIO_HIJO)
		def fah_ejecucionSoportePAU = Fase.findByCodigoFase(FaseMultiton.EJECUCION_SOPORTE_PAU_HIJO)
		def fah_construccionPU = Fase.findByCodigoFase(FaseMultiton.CONSTRUCCION_PU_HIJO)
		def fah_adminActividades = Fase.findByCodigoFase(FaseMultiton.ADMINISTRAR_ACTIVIDADES_HIJO)

		def impacto_SWDesa = TipoImpacto.findByDescripcion("SW Desarrollo")
		def impacto_SWDesaSAP = TipoImpacto.findByDescripcion("SW Desarrollo SAP")
		def impacto_AS = TipoImpacto.findByDescripcion("Area de soporte")
		def impacto_DatosRef = TipoImpacto.findByDescripcion("Datos referenciales")
		def impacto_AF = TipoImpacto.findByDescripcion("Administrador funcional")
		def impacto_SopPruebas = TipoImpacto.findByDescripcion("SW Soporte a pruebas")

		def n_a = TipoActividad.CAMPO_NO_APLICA
		def opc = TipoActividad.CAMPO_OPCIONAL
		def obl = TipoActividad.CAMPO_OBLIGATORIO

		def validacionDefault = CriterioValidacionActividad.findByDescripcion("Default")

		def ta_std_genAS
		def ta_std_genASTest01
		def ta_std_genASTest02
		def ta_std_genAF
		def ta_std_genASArqApli
		def ta_std_genASOF

		// Actividades de Areas de Soporte
		if (TipoActividad.countByFase(fah_adminActividades) == 0) {

			ta_std_genAS = new TipoActividad(tipoGestion: gestionEstandar, tipoImpacto: impacto_AS, fase: fah_adminActividades, descripcion: "Tipo de actividad gen\u00E9rica para \u00E1reas de Soporte", estrategiaComentarios: opc, estrategiaFechaSugerida: opc, estrategiaAnexos: opc, estrategiaPasajes: n_a, estrategiaAmbiente: n_a, estrategiaVersionador: n_a, estrategiaMercury: n_a, estrategiaRelease: n_a, estrategiaURL: n_a, obligatoriedad: TipoActividad.OPCIONAL).save(flush: true, failOnError: true)
			ta_std_genASTest01 = new TipoActividad(tipoGestion: gestionEstandar, tipoImpacto: impacto_AS, fase: fah_adminActividades, descripcion: "Tipo de actividad gen\u00E9rica para AS Testing DTI-UF 01", estrategiaComentarios: opc, estrategiaFechaSugerida: opc, estrategiaAnexos: opc, estrategiaPasajes: n_a, estrategiaAmbiente: n_a, estrategiaVersionador: n_a, estrategiaMercury: obl, estrategiaRelease: n_a, estrategiaURL: n_a, obligatoriedad: TipoActividad.OPCIONAL).save(flush: true, failOnError: true)
			ta_std_genASTest02 = new TipoActividad(tipoGestion: gestionEstandar, tipoImpacto: impacto_AS, fase: fah_adminActividades, descripcion: "Tipo de actividad gen\u00E9rica para AS Testing DTI-UF 02", estrategiaComentarios: opc, estrategiaFechaSugerida: opc, estrategiaAnexos: opc, estrategiaPasajes: n_a, estrategiaAmbiente: n_a, estrategiaVersionador: n_a, estrategiaMercury: n_a, estrategiaRelease: n_a, estrategiaURL: n_a, obligatoriedad: TipoActividad.OPCIONAL).save(flush: true, failOnError: true)
			ta_std_genAF = new TipoActividad(tipoGestion: gestionEstandar, tipoImpacto: impacto_AF, fase: fah_adminActividades, descripcion: "Tipo de actividad gen\u00E9rica para Administradores Funcionales", estrategiaComentarios: opc, estrategiaFechaSugerida: opc, estrategiaAnexos: opc, estrategiaPasajes: n_a, estrategiaAmbiente: n_a, estrategiaVersionador: n_a, estrategiaMercury: n_a, estrategiaRelease: n_a, estrategiaURL: n_a, obligatoriedad: TipoActividad.OPCIONAL).save(flush: true, failOnError: true)
			ta_std_genASArqApli = new TipoActividad(tipoGestion: gestionEstandar, tipoImpacto: impacto_AS, fase: fah_adminActividades, descripcion: "Tipo de actividad gen\u00E9rica para AS Arquitectura Aplicativa DTI-UF", estrategiaURL: opc, obligatoriedad: TipoActividad.OPCIONAL).save(flush: true, failOnError: true)
			ta_std_genASOF = new TipoActividad(tipoGestion: gestionEstandar, tipoImpacto: impacto_AS, fase: fah_adminActividades, descripcion: "Tipo de actividad gen\u00E9rica para AS Operaciones de Facturaci\u00F3n", estrategiaComentarios: opc, estrategiaFechaSugerida: n_a, estrategiaAnexos: opc, estrategiaPasajes: n_a, estrategiaAmbiente: n_a, estrategiaVersionador: n_a, estrategiaMercury: n_a, estrategiaRelease: n_a, estrategiaURL: n_a, obligatoriedad: TipoActividad.OPCIONAL).save(flush: true, failOnError: true)
		} else {
			ta_std_genAS = TipoActividad.findByDescripcion("Tipo de actividad gen\u00E9rica para \u00E1reas de Soporte")
			ta_std_genASTest01 = TipoActividad.findByDescripcion("Tipo de actividad gen\u00E9rica para AS Testing DTI-UF 01")
			ta_std_genASTest02 = TipoActividad.findByDescripcion("Tipo de actividad gen\u00E9rica para AS Testing DTI-UF 02")
			ta_std_genAF = TipoActividad.findByDescripcion("Tipo de actividad gen\u00E9rica para Administradores Funcionales")
			ta_std_genASArqApli = TipoActividad.findByDescripcion("Tipo de actividad gen\u00E9rica para AS Arquitectura Aplicativa DTI-UF")
			ta_std_genASOF = TipoActividad.findByDescripcion("Tipo de actividad gen\u00E9rica para AS Operaciones de Facturaci\u00F3n")
		}

		TipoActividad.list().findAll {  tipoActividad -> !tipoActividad.criterioValidacionActividad  }.each { tipoActividad ->
			tipoActividad.criterioValidacionActividad = validacionDefault
			tipoActividad.save(flush: true, failOnError: true)
		}

		def as_planeamiento = AreaSoporte.findByDescripcion("Planeamiento y Evaluaciones Econ\u00F3micas")
		def as_micro = AreaSoporte.findByDescripcion("Microinform\u00E1tica")
		def as_dataCenter = AreaSoporte.findByDescripcion("Data Center")
		def as_operacionesFact = AreaSoporte.findByDescripcion("Operaciones de Facturaci\u00F3n")
		def as_arquitectura = AreaSoporte.findByDescripcion("Arquitectura Aplicativa DTI-UF")
		def as_seginfo = AreaSoporte.findByDescripcion("Seguridad inform\u00E1tica")
		def as_integridadFact = AreaSoporte.findByDescripcion("Grupo Integridad Facturaci\u00F3n")
		def as_testingDTI = AreaSoporte.findByDescripcion("Testing DTI-UF")
		def as_fraudes = AreaSoporte.findByDescripcion("Grupo de Control y Prevenci\u00F3n de Fraudes")
		def as_procynormas = AreaSoporte.findByDescripcion("Procesos y Normas")

		if (TareaSoporte.count() == 0) {
			new TareaSoporte(descripcion: "Actividad gen\u00E9rica para el Administrador Funcional", areaSoporte: null, tipoActividad: ta_std_genAF).save(flush: true, failOnError: true)
			new TareaSoporte(descripcion: "Actividad gen\u00E9rica para Planeamiento y Evaluaciones Econ\u00F3micas", areaSoporte: as_planeamiento, tipoActividad: ta_std_genAS).save(flush: true, failOnError: true)
			new TareaSoporte(descripcion: "Actividad gen\u00E9rica para Microinform\u00E1tica", areaSoporte: as_micro, tipoActividad: ta_std_genAS).save(flush: true, failOnError: true)
			new TareaSoporte(descripcion: "Actividad gen\u00E9rica para Data Center", areaSoporte: as_dataCenter, tipoActividad: ta_std_genAS).save(flush: true, failOnError: true)
			new TareaSoporte(descripcion: "Actividad gen\u00E9rica para Seguridad inform\u00E1tica", areaSoporte: as_seginfo, tipoActividad: ta_std_genAS).save(flush: true, failOnError: true)
			new TareaSoporte(descripcion: "Actividad gen\u00E9rica para Grupo Integridad Facturaci\u00F3n", areaSoporte: as_integridadFact, tipoActividad: ta_std_genAS).save(flush: true, failOnError: true)
			new TareaSoporte(descripcion: "Actividad gen\u00E9rica para Grupo de Control y Prevenci\u00F3n de Fraudes", areaSoporte: as_fraudes, tipoActividad: ta_std_genAS).save(flush: true, failOnError: true)
			new TareaSoporte(descripcion: "Actividad gen\u00E9rica para Procesos y Normas", areaSoporte: as_procynormas, tipoActividad: ta_std_genAS).save(flush: true, failOnError: true)
			new TareaSoporte(descripcion: "Revisar diseño interno", areaSoporte: as_arquitectura, tipoActividad: ta_std_genASArqApli).save(flush: true, failOnError: true)
			new TareaSoporte(descripcion: "Relevamiento-actualizaci\u00F3n BCA", areaSoporte: as_arquitectura, tipoActividad: ta_std_genASArqApli).save(flush: true, failOnError: true)
			new TareaSoporte(descripcion: "PI-Preparaci\u00F3n casos de prueba", areaSoporte: as_testingDTI, tipoActividad: ta_std_genASTest01).save(flush: true, failOnError: true)
			new TareaSoporte(descripcion: "PI-Preparaci\u00F3n ambientes", areaSoporte: as_testingDTI, tipoActividad: ta_std_genASTest02).save(flush: true, failOnError: true)
			new TareaSoporte(descripcion: "PI-Preparaci\u00F3n datos", areaSoporte: as_testingDTI, tipoActividad: ta_std_genASTest02).save(flush: true, failOnError: true)
			new TareaSoporte(descripcion: "PI-Ejecuci\u00F3n", areaSoporte: as_testingDTI, tipoActividad: ta_std_genASTest01).save(flush: true, failOnError: true)
			new TareaSoporte(descripcion: "PAU-Preparaci\u00F3n casos de prueba", areaSoporte: as_testingDTI, tipoActividad: ta_std_genASTest01).save(flush: true, failOnError: true)
			new TareaSoporte(descripcion: "PAU-Preparaci\u00F3n ambientes", areaSoporte: as_testingDTI, tipoActividad: ta_std_genASTest02).save(flush: true, failOnError: true)
			new TareaSoporte(descripcion: "PAU-Preparaci\u00F3n datos", areaSoporte: as_testingDTI, tipoActividad: ta_std_genASTest02).save(flush: true, failOnError: true)
			new TareaSoporte(descripcion: "PAU-Kick off usuarios - Convalidaci\u00F3n casos de pruebas", areaSoporte: as_testingDTI, tipoActividad: ta_std_genASTest02).save(flush: true, failOnError: true)
			new TareaSoporte(descripcion: "PAU-Ejecuci\u00F3n", areaSoporte: as_testingDTI, tipoActividad: ta_std_genASTest01).save(flush: true, failOnError: true)
			new TareaSoporte(descripcion: "Control Tablas Genesis", areaSoporte: as_operacionesFact, tipoActividad: ta_std_genASOF).save(flush: true, failOnError: true)
			new TareaSoporte(descripcion: "Control Tablas Genesis y Facturas", areaSoporte: as_operacionesFact, tipoActividad: ta_std_genASOF).save(flush: true, failOnError: true)
			new TareaSoporte(descripcion: "Control IPB", areaSoporte: as_operacionesFact, tipoActividad: ta_std_genASOF).save(flush: true, failOnError: true)
			new TareaSoporte(descripcion: "Control de Reportes ", areaSoporte: as_operacionesFact, tipoActividad: ta_std_genASOF).save(flush: true, failOnError: true)
			new TareaSoporte(descripcion: "Control FACTESP", areaSoporte: as_operacionesFact, tipoActividad: ta_std_genASOF).save(flush: true, failOnError: true)

		}
	}

	// DONE
	private def definirActividadesPlanificacion() {
		def fap_evaluacionCambio = Fase.findByCodigoFase(FaseMultiton.EVALUACION_CAMBIO)
		def fap_especificacionCambio = Fase.findByCodigoFase(FaseMultiton.ESPECIFICACION_CAMBIO)
		def fah_especificacionImpacto = Fase.findByCodigoFase(FaseMultiton.ESPECIFICACION_IMPACTO_HIJO)
		def fap_consolidacionImpacto = Fase.findByCodigoFase(FaseMultiton.CONSOLIDACION_IMPACTO)
		def fah_construccionCambio = Fase.findByCodigoFase(FaseMultiton.CONSTRUCCION_CAMBIO_HIJO)
		def fah_ejecucionSoportePAU = Fase.findByCodigoFase(FaseMultiton.EJECUCION_SOPORTE_PAU_HIJO)
		def fah_implantacion = Fase.findByCodigoFase(FaseMultiton.IMPLANTACION_HIJO)
		def fah_normalizacion = Fase.findByCodigoFase(FaseMultiton.NORMALIZACION_HIJO)
		def fap_finalizacion = Fase.findByCodigoFase(FaseMultiton.FINALIZACION)

		new ActividadPlanificacion(descripcion: "Evaluaci\u00F3n Cambio", fase:fap_evaluacionCambio).save(failOnError: true)
		new ActividadPlanificacion(descripcion: "Especificaci\u00F3n Cambio", fase:fap_especificacionCambio).save(failOnError: true)
		new ActividadPlanificacion(descripcion: "Especificaci\u00F3n Impacto", fase:fah_especificacionImpacto).save(failOnError: true)
		new ActividadPlanificacion(descripcion: "Consolidaci\u00F3n Impacto", fase:fap_consolidacionImpacto).save(failOnError: true)
		new ActividadPlanificacion(descripcion: "Construcci\u00F3n Cambio", fase:fah_construccionCambio).save(failOnError: true)
		new ActividadPlanificacion(descripcion: "Ejecuci\u00F3n y Soporte PAU", fase:fah_ejecucionSoportePAU).save(failOnError: true)
		new ActividadPlanificacion(descripcion: "Implantaci\u00F3n", fase:fah_implantacion).save(failOnError: true)
		new ActividadPlanificacion(descripcion: "Normalizaci\u00F3n", fase:fah_normalizacion).save(failOnError: true)
		new ActividadPlanificacion(descripcion: "Finalizaci\u00F3n", fase:fap_finalizacion).save(failOnError: true)
	}

	public void definirCodigosCierre() {
		def cerrada

		// DONE
		/** Estados de una actividad */
		if (EstadoActividad.count() == 0) {
			new EstadoActividad(descripcion: EstadoActividad.ABIERTA, esEstadoFinal: false, aprobado: false).save(failOnError: true)
			cerrada = new EstadoActividad(descripcion: "Cerrada", esEstadoFinal: true, aprobado: true).save(failOnError: true)
			new EstadoActividad(descripcion: "Rechazada", esEstadoFinal: true, aprobado: false).save(failOnError: true)
			new EstadoActividad(descripcion: "Cancelada", esEstadoFinal: true, aprobado: false).save(failOnError: true)
			new EstadoActividad(descripcion: "Suspendida", esEstadoFinal: false, aprobado: false).save(failOnError: true)
		} else {
			cerrada = EstadoActividad.findByDescripcion(EstadoActividad.CERRADA)
		}

		// DONE
		if (CodigoCierre.count() == 0) {
			new CodigoCierre(descripcion: "Exitoso", estadoActividad: cerrada).save(failOnError: true)
			new CodigoCierre(descripcion: "Observaciones", estadoActividad: cerrada).save(failOnError: true)
			new CodigoCierre(descripcion: "Fallida", estadoActividad: cerrada).save(failOnError: true)
			new CodigoCierre(descripcion: "Rechazada", estadoActividad: cerrada).save(failOnError: true)
			new CodigoCierre(descripcion: "Cancelada", estadoActividad: cerrada).save(failOnError: true)
		}
	}

	/***********************************************************************************************************************
	 * TODO: Esta info no debe cargarse en el bootstrap en la salida a producci\u00F3n
	 ************************************************************************************************************************
	 */
	private crearSistemas() {
		new Sistema(descripcion: "112.COM_APL" , descripcionLarga:  "Sistema que permite la autogesti\u00F3n de los procesos de venta, postventa y adminis" , grupoAdministrador:  "AP_CH_112.COM_APL", grupoReferenteSWF: GRUPO_INTERLOCUTOR_USUARIO, gerencia:"DTI_DSCRM_112-PROV", grupoTuID: "").save(failOnError: true)
		new Sistema(descripcion: "ABONADOS_EN_GUIA_APL" , descripcionLarga:  "Repositorio de abonados a utilizar en la publicaci\u00F3n de P\u00E1ginas Blancas" , grupoAdministrador:  "AP_CH_ABONADOS_EN_GUIA_APL", grupoReferenteSWF: GRUPO_INTERLOCUTOR_USUARIO, gerencia:"DTI_DSIN_SISTSOPGEST", grupoTuID: "").save(failOnError: true)
		new Sistema(descripcion: "ABUSE_APL" , descripcionLarga:  "Proceso autom\u00E1tico de mails recibidos en la casilla Abuse@ta.telecom.com.ar para" , grupoAdministrador:  "AP_CH_ABUSE_APL", grupoReferenteSWF: GRUPO_INTERLOCUTOR_USUARIO, gerencia:"DSI_DST_DSFP", grupoTuID: "").save(failOnError: true)
		new Sistema(descripcion: "ACDSERVER_APL" , descripcionLarga:  "Aplicativo que permite realizar consultas de facturas pendientes de pago, estado" , grupoAdministrador:  "AP_CH_ACDSERVER_APL", grupoReferenteSWF: GRUPO_INTERLOCUTOR_USUARIO, gerencia:"DTI_DSPI_PROV-SISTEMAS-BASICOS", grupoTuID: "").save(failOnError: true)
		new Sistema(descripcion: "ACTOR_APL" , descripcionLarga:  "SISTEMA UTILIZADO PARA LA EJECUCI\u00F3N DE PRUEBAS Y MEDICIONES SOBRE LAS L\u00EDNEAS DE" , grupoAdministrador:  "AP_CH_ACTOR_APL", grupoReferenteSWF: GRUPO_INTERLOCUTOR_USUARIO, gerencia:"DSI_DST_DSFP", grupoTuID: "").save(failOnError: true)
		new Sistema(descripcion: "ACTUALIZACION_DIARIA_APL" , descripcionLarga:  "Proceso de centralizaci\u00F3n, traducci\u00F3n de c\u00F3digo, validaci\u00F3n e interfaces con BAS" , grupoAdministrador:  "AP_CH_ACTUALIZACION_DIARIA_APL", grupoReferenteSWF: GRUPO_INTERLOCUTOR_USUARIO, gerencia:"DTI_DSPI", grupoTuID: "").save(failOnError: true)
		new Sistema(descripcion: "ADAS_APL" , descripcionLarga:  "Sistema de activaci\u00F3n y desactivaci\u00F3n Autom\u00E1tica de Servicios.  Realiza la provi" , grupoAdministrador:  "AP_CH_ADAS_APL", grupoReferenteSWF: GRUPO_INTERLOCUTOR_USUARIO, gerencia:"DTI_DSPI", grupoTuID: "").save(failOnError: true)
		new Sistema(descripcion: "ADHESION_FACTURA_DIGITAL_APL" , descripcionLarga:  "M\u00F3dulo Web, accedido por el cliente a trav\u00E9s del Portal Autogesti\u00F3n (SSO) para l" , grupoAdministrador:  "AP_CH_ADHESION_FACTURA_DIGITAL_APL", grupoReferenteSWF: GRUPO_INTERLOCUTOR_USUARIO, gerencia:"DTI_DSCRM_FC-DIG", grupoTuID: "").save(failOnError: true)
		new Sistema(descripcion: "ADMINPORTAL_APL" , descripcionLarga:  "APLICACI\u00F3N QUE PERMITE GESTIONAR ACCESOS Y PERFILES A LOS MODULOS FUNCIONALES." , grupoAdministrador:  "AP_ADMINPORTAL_APL", grupoReferenteSWF: GRUPO_INTERLOCUTOR_USUARIO, gerencia:"DSI_DST_DSPFM", grupoTuID: "").save(failOnError: true)
		new Sistema(descripcion: "ADM_DE_CONTACTOS_APL" , descripcionLarga:  "Aplicaci\u00F3n que permite realizar la gesti\u00F3n y seguimiento de acciones de ventas y" , grupoAdministrador:  "AP_CH_ADM_DE_CONTACTOS_APL", grupoReferenteSWF: GRUPO_INTERLOCUTOR_USUARIO, gerencia:"DTI_DSCRM_AC", grupoTuID: "").save(failOnError: true)
		new Sistema(descripcion: "ADM_DE_CONTACTOS_INTERFACES" , descripcionLarga:  "Aplicaci\u00F3n que permite realizar la gesti\u00F3n y seguimiento de acciones de ventas y" , grupoAdministrador:  "AP_CH_ADM_DE_CONTACTOS_INTERFACES", grupoReferenteSWF: GRUPO_INTERLOCUTOR_USUARIO, gerencia:"DTI_DSCRM_AC", grupoTuID: "").save(failOnError: true)
		new Sistema(descripcion: "ADNG_APL" , descripcionLarga:  "El sistema de Administraci\u00F3n de Numeraci\u00F3n No Geogr\u00E1fica tiene como objetivo ges" , grupoAdministrador:  "AP_CH_ADNG_APL", grupoReferenteSWF: GRUPO_INTERLOCUTOR_USUARIO, gerencia:"DTI_DSPI_PROV-MERCADO-NO-MASIVO", grupoTuID: "").save(failOnError: true)
		new Sistema(descripcion: "ADSL_ADMINISTRACION" , descripcionLarga:  "Instancia de administraci\u00F3n del servicio Venta ADSL" , grupoAdministrador:  "AP_CH_ADSL_ADMINISTRACION", grupoReferenteSWF: GRUPO_INTERLOCUTOR_USUARIO, gerencia:"DTI_DSPI_PROV-SISTEMAS-BASICOS", grupoTuID: "").save(failOnError: true)
		new Sistema(descripcion: "ADSL_CONSUMOS_APL" , descripcionLarga:  "Sistema  web en el cual los clientes consultan su tr\u00E1fico de ADSL. (ADSL x Volum" , grupoAdministrador:  "AP_CH_ADSL_CONSUMOS_APL", grupoReferenteSWF: GRUPO_INTERLOCUTOR_USUARIO, gerencia:"DTI_DSPI_PROV-SISTEMAS-BASICOS", grupoTuID: "").save(failOnError: true)
		new Sistema(descripcion: "ADSL_INTERNOS_WEB" , descripcionLarga:  "Instancia de ventas para Utop\u00EDa y CMS (reemplaza al m\u00F3dulo ADSL_AI)" , grupoAdministrador:  "AP_CH_ADSL_INTERNOS_WEB", grupoReferenteSWF: GRUPO_INTERLOCUTOR_USUARIO, gerencia:"DTI_DSPI_PROV-SISTEMAS-BASICOS", grupoTuID: "").save(failOnError: true)
		new Sistema(descripcion: "ADSL_RECARGABLE" , descripcionLarga:  "Portal de recarga del servicio ADSL recargable" , grupoAdministrador:  "AP_CH_ADSL_RECARGABLE", grupoReferenteSWF: GRUPO_INTERLOCUTOR_USUARIO, gerencia:"DTI_DSPI_PROV-SISTEMAS-BASICOS", grupoTuID: "").save(failOnError: true)
		new Sistema(descripcion: "ADSL_SERVICIOS" , descripcionLarga:  "Instancia que permite chequear la factibilidad de obtener el servicio al usuario" , grupoAdministrador:  "AP_CH_ADSL_SERVICIOS", grupoReferenteSWF: GRUPO_INTERLOCUTOR_USUARIO, gerencia:"DTI_DSPI_PROV-SISTEMAS-BASICOS", grupoTuID: "").save(failOnError: true)
		new Sistema(descripcion: "ADSL_VENTAS_APL" , descripcionLarga:  "Instancia de ventas para todos los ISPs excepto Arnet" , grupoAdministrador:  "AP_CH_ADSL_VENTAS_APL", grupoReferenteSWF: GRUPO_INTERLOCUTOR_USUARIO, gerencia:"DTI_DSPI_PROV-SISTEMAS-BASICOS", grupoTuID: "").save(failOnError: true)
		new Sistema(descripcion: "AGILIT_APL" , descripcionLarga:  "GESTI\u00F3N DE CERTIFICACI\u00F3N DE RECURSOS PROVISIONALES DE CONSULTORAS" , grupoAdministrador:  "AP_CH_AGILIT_APL", grupoReferenteSWF: GRUPO_INTERLOCUTOR_USUARIO, gerencia:"", grupoTuID: "").save(failOnError: true)
		new Sistema(descripcion: "ALMA_LCR_APL" , descripcionLarga:  "Sistema que permite realizar en forma autom\u00E1tica la provisi\u00F3n de las L\u00EDneas de C" , grupoAdministrador:  "AP_CH_ALMA_LCR_APL", grupoReferenteSWF: GRUPO_INTERLOCUTOR_USUARIO, gerencia:"DTI_DSPI_PROV-SISTEMAS-BASICOS", grupoTuID: "").save(failOnError: true)
		new Sistema(descripcion: "ALMA_RPV_APL" , descripcionLarga:  "Sistema  que realiza la gesti\u00F3n comercial de la Red Privada Virtual interactuand" , grupoAdministrador:  "AP_CH_ALMA_RPV_APL", grupoReferenteSWF: GRUPO_INTERLOCUTOR_USUARIO, gerencia:"DTI_DSPI_PROV-SISTEMAS-BASICOS", grupoTuID: "").save(failOnError: true)
		new Sistema(descripcion: "ALMA_STP_APL" , descripcionLarga:  "Sistema que realiza la gesti\u00F3n comercial del servicio de Tarjeta Prepaga de la R" , grupoAdministrador:  "AP_CH_ALMA_STP_APL", grupoReferenteSWF: GRUPO_INTERLOCUTOR_USUARIO, gerencia:"DTI_DSPI_PROV-SISTEMAS-BASICOS", grupoTuID: "").save(failOnError: true)
		new Sistema(descripcion: "ALPINA_APL" , descripcionLarga:  "Gesti\u00F3n para planificaci\u00F3n y obras de inversi\u00F3n." , grupoAdministrador:  "", grupoReferenteSWF: GRUPO_INTERLOCUTOR_USUARIO, gerencia:"DSI_DSC_BAJA", grupoTuID: "").save(failOnError: true)
		new Sistema(descripcion: "AMADEUS_APL" , descripcionLarga:  "Aplicaci\u00F3n para el manejo y aprobaci\u00F3n de documentos electr\u00F3nicos." , grupoAdministrador:  "AP_CH_AMADEUS_APL", grupoReferenteSWF: GRUPO_INTERLOCUTOR_USUARIO, gerencia:"DTI_DSIN_EVA", grupoTuID: "").save(failOnError: true)
		new Sistema(descripcion: "ARCHIBUS_APL" , descripcionLarga:  "Sistema de Planimetria de Real Estate" , grupoAdministrador:  "AP_CH_ARCHIBUS_APL", grupoReferenteSWF: GRUPO_INTERLOCUTOR_USUARIO, gerencia:"DSI_DSC_NO-SIGNO", grupoTuID: "").save(failOnError: true)
		new Sistema(descripcion: "ARIS_APL" , descripcionLarga:  "Modelado de procesos de negocio." , grupoAdministrador:  "AP_CH_ARIS_APL", grupoReferenteSWF: GRUPO_INTERLOCUTOR_USUARIO, gerencia:"DSI_DSC_NO-SIGNO", grupoTuID: "").save(failOnError: true)
		new Sistema(descripcion: "ARNET_MONITOREO_APL" , descripcionLarga:  "Conjunto de p\u00E1ginas Web que dan servicios a clientes y a usuarios registrados." , grupoAdministrador:  "", grupoReferenteSWF: GRUPO_INTERLOCUTOR_USUARIO, gerencia:"DTI_BAJA", grupoTuID: "").save(failOnError: true)
		new Sistema(descripcion: "ARNET_VIDEO_STREAMING_APL" , descripcionLarga:  "ALQUILER DE CONTENIDO ON DEMAND PARA SER VISUALIZADO POR WEB Y SET TOP BOX (STB)" , grupoAdministrador:  "AP_INT_Y_MULTIMEDIA_APL", grupoReferenteSWF: GRUPO_INTERLOCUTOR_USUARIO, gerencia:"DTI_DSIM", grupoTuID: "").save(failOnError: true)
		new Sistema(descripcion: "ASSET_CENTER_APL" , descripcionLarga:  "INVENTARIO DE ACTIVOS DE IT" , grupoAdministrador:  "AP_CH_ASSET_CENTER_APL", grupoReferenteSWF: GRUPO_INTERLOCUTOR_USUARIO, gerencia:"DSI_DSC_G-IT", grupoTuID: "").save(failOnError: true)
		new Sistema(descripcion: "ATLANTIS_APL" , descripcionLarga:  "Aplicativo que Administra y Mantiene los recursos del cable submarino Atlantis 2" , grupoAdministrador:  "AP_CH_ATLANTIS_APL", grupoReferenteSWF: GRUPO_INTERLOCUTOR_USUARIO, gerencia:"DSI_DST_DSIN", grupoTuID: "").save(failOnError: true)
		new Sistema(descripcion: "BASEIP_APL" , descripcionLarga:  "APLICACI\u00F3N QUE PERMITE REALIZAR CONSULTAS SOBRE USO Y ASIGNACION DE INFORMACION IP." , grupoAdministrador:  "AP_BASEIP_APL", grupoReferenteSWF: GRUPO_INTERLOCUTOR_USUARIO, gerencia:"DSI_DST_DSPFM", grupoTuID: "").save(failOnError: true)
		new Sistema(descripcion: "BASE_DE_CORREO_APL" , descripcionLarga:  "SISTEMA DE ADMINISTRACI\u00F3N DE BOLETAS DE IMPOSICI\u00F3N INTERNAS" , grupoAdministrador:  "AP_CH_BASE_DE_CORREO_APL", grupoReferenteSWF: GRUPO_INTERLOCUTOR_USUARIO, gerencia:"DSC", grupoTuID: "").save(failOnError: true)
		new Sistema(descripcion: "BASE_ENTORNO_GEO_APL" , descripcionLarga:  "Inventario de equipos de climatizaci\u00F3n, energ\u00EDa y fuerza." , grupoAdministrador:  "AP_CH_BASE_ENTORNO_GEO_APL", grupoReferenteSWF: GRUPO_INTERLOCUTOR_USUARIO, gerencia:"DSI_DST_DSIN", grupoTuID: "").save(failOnError: true)
		new Sistema(descripcion: "BAS_APL" , descripcionLarga:  "Sistema de gesti\u00F3n de solicitudes de presuscripcion, incomunicaciones y rehabili" , grupoAdministrador:  "AP_CH_BAS_APL", grupoReferenteSWF: GRUPO_INTERLOCUTOR_USUARIO, gerencia:"DTI_DSCRM_BAS", grupoTuID: "").save(failOnError: true)
		new Sistema(descripcion: "BCA_APL" , descripcionLarga:  "INVENTARIO DE APLICACIONES DE ARQUITECTURA APLICATIVA" , grupoAdministrador:  "AP_CH_BCA_APL", grupoReferenteSWF: GRUPO_INTERLOCUTOR_USUARIO, gerencia:"DSI_DSC_BCA", grupoTuID: "").save(failOnError: true)
		new Sistema(descripcion: "BEIJING_APL" , descripcionLarga:  "Conjunto de p\u00E1ginas Web que dan servicios a clientes y a usuarios registrados pa" , grupoAdministrador:  "AP_CH_BEIJING_APL", grupoReferenteSWF: GRUPO_INTERLOCUTOR_USUARIO, gerencia:"DTI_SIM_DDP", grupoTuID: "").save(failOnError: true)
		new Sistema(descripcion: "BELEN_APL" , descripcionLarga:  "Sistema de Gestion de Previsi\u00F3n de la Demanda. Soporta la registraci\u00F3n de releva" , grupoAdministrador:  "AP_CH_BELEN_APL", grupoReferenteSWF: GRUPO_INTERLOCUTOR_USUARIO, gerencia:"DTI_DSIN_SISTSOPGEST", grupoTuID: "").save(failOnError: true)
		new Sistema(descripcion: "BIOS_HAL_APL" , descripcionLarga:  "BIOS - Bus de Integraci\u00F3n Corporativo" , grupoAdministrador:  "AP_CH_BIOS_HAL_APL", grupoReferenteSWF: GRUPO_INTERLOCUTOR_USUARIO, gerencia:"DTI_DSPI_INTEGRACION", grupoTuID: "").save(failOnError: true)
		new Sistema(descripcion: "BIOS_HAL_PROCESO_BATCH_GIRAFE" , descripcionLarga:  "BIOS - Bus de Integraci\u00F3n Corporativo" , grupoAdministrador:  "AP_CH_BIOS_HAL_PROCESO_BATCH_GIRAFE", grupoReferenteSWF: GRUPO_INTERLOCUTOR_USUARIO, gerencia:"DTI_DSPI_INTEGRACION", grupoTuID: "").save(failOnError: true)
		new Sistema(descripcion: "BIOS_HAL_REPORTE_TABLERO" , descripcionLarga:  "BIOS - Bus de Integraci\u00F3n Corporativo" , grupoAdministrador:  "AP_CH_BIOS_HAL_REPORTE_TABLERO", grupoReferenteSWF: GRUPO_INTERLOCUTOR_USUARIO, gerencia:"DTI_DSPI_INTEGRACION", grupoTuID: "").save(failOnError: true)
		new Sistema(descripcion: "BIOS_HAL_REPROCESO" , descripcionLarga:  "BIOS - Bus de Integraci\u00F3n Corporativo" , grupoAdministrador:  "AP_CH_BIOS_HAL_REPROCESO", grupoReferenteSWF: GRUPO_INTERLOCUTOR_USUARIO, gerencia:"DTI_DSPI_INTEGRACION", grupoTuID: "").save(failOnError: true)
		new Sistema(descripcion: "BIOS_SERVICIO_ACTOR" , descripcionLarga:  "BIOS - Bus de Integraci\u00F3n Corporativo" , grupoAdministrador:  "AP_CH_BIOS_SERVICIO_ACTOR", grupoReferenteSWF: GRUPO_INTERLOCUTOR_USUARIO, gerencia:"DTI_DSPI_INTEGRACION", grupoTuID: "").save(failOnError: true)
		new Sistema(descripcion: "BIOS_SERVICIO_AFIP" , descripcionLarga:  "BIOS - Bus de Integraci\u00F3n Corporativo" , grupoAdministrador:  "AP_CH_BIOS_SERVICIO_AFIP", grupoReferenteSWF: GRUPO_INTERLOCUTOR_USUARIO, gerencia:"DTI_DSPI_INTEGRACION", grupoTuID: "").save(failOnError: true)
		new Sistema(descripcion: "BIOS_SERVICIO_AMI" , descripcionLarga:  "BIOS - Bus de Integraci\u00F3n Corporativo" , grupoAdministrador:  "AP_CH_BIOS_SERVICIO_AMI", grupoReferenteSWF: GRUPO_INTERLOCUTOR_USUARIO, gerencia:"DTI_DSPI_INTEGRACION", grupoTuID: "").save(failOnError: true)
		new Sistema(descripcion: "BIOS_SERVICIO_CENTRAL_PARQ" , descripcionLarga:  "BIOS - Bus de Integraci\u00F3n Corporativo" , grupoAdministrador:  "AP_CH_BIOS_SERVICIO_CENTRAL_PARQ", grupoReferenteSWF: GRUPO_INTERLOCUTOR_USUARIO, gerencia:"DTI_DSPI_INTEGRACION", grupoTuID: "").save(failOnError: true)
		new Sistema(descripcion: "BIOS_SERVICIO_GATUC" , descripcionLarga:  "BIOS - Bus de Integraci\u00F3n Corporativo" , grupoAdministrador:  "AP_CH_BIOS_SERVICIO_GATUC", grupoReferenteSWF: GRUPO_INTERLOCUTOR_USUARIO, gerencia:"DTI_DSPI_INTEGRACION", grupoTuID: "").save(failOnError: true)
		new Sistema(descripcion: "BIOS_SERVICIO_GENESIS" , descripcionLarga:  "BIOS - Bus de Integraci\u00F3n Corporativo" , grupoAdministrador:  "AP_CH_BIOS_SERVICIO_GENESIS", grupoReferenteSWF: GRUPO_INTERLOCUTOR_USUARIO, gerencia:"DTI_DSPI_INTEGRACION", grupoTuID: "").save(failOnError: true)
		new Sistema(descripcion: "BIOS_SERVICIO_GIRAFE" , descripcionLarga:  "BIOS - Bus de Integraci\u00F3n Corporativo" , grupoAdministrador:  "AP_CH_BIOS_SERVICIO_GIRAFE", grupoReferenteSWF: GRUPO_INTERLOCUTOR_USUARIO, gerencia:"DTI_DSPI_INTEGRACION", grupoTuID: "").save(failOnError: true)
		new Sistema(descripcion: "BIOS_SERVICIO_MAC" , descripcionLarga:  "BIOS - Bus de Integraci\u00F3n Corporativo" , grupoAdministrador:  "AP_CH_BIOS_SERVICIO_MAC", grupoReferenteSWF: GRUPO_INTERLOCUTOR_USUARIO, gerencia:"DTI_DSPI_INTEGRACION", grupoTuID: "").save(failOnError: true)
		new Sistema(descripcion: "BIOS_SERVICIO_NETWORK_INVENTORY" , descripcionLarga:  "BIOS - Bus de Integraci\u00F3n Corporativo" , grupoAdministrador:  "AP_CH_BIOS_SERVICIO_NETWORK_INVENTORY", grupoReferenteSWF: GRUPO_INTERLOCUTOR_USUARIO, gerencia:"DTI_DSPI_INTEGRACION", grupoTuID: "").save(failOnError: true)
		new Sistema(descripcion: "BIOS_SERVICIO_SEGAT" , descripcionLarga:  "BIOS - Bus de Integraci\u00F3n Corporativo" , grupoAdministrador:  "AP_CH_BIOS_SERVICIO_SEGAT", grupoReferenteSWF: GRUPO_INTERLOCUTOR_USUARIO, gerencia:"DTI_DSPI_INTEGRACION", grupoTuID: "").save(failOnError: true)
		new Sistema(descripcion: "BIOS_SERVICIO_SIEBEL" , descripcionLarga:  "BIOS - Bus de Integraci\u00F3n Corporativo" , grupoAdministrador:  "AP_CH_BIOS_SERVICIO_SIEBEL", grupoReferenteSWF: GRUPO_INTERLOCUTOR_USUARIO, gerencia:"DTI_DSPI_INTEGRACION", grupoTuID: "").save(failOnError: true)
		new Sistema(descripcion: "BIOS_SERVICIO_SUC" , descripcionLarga:  "BIOS - Bus de Integraci\u00F3n Corporativo" , grupoAdministrador:  "AP_CH_BIOS_SERVICIO_SUC", grupoReferenteSWF: GRUPO_INTERLOCUTOR_USUARIO, gerencia:"DTI_DSPI_INTEGRACION", grupoTuID: "").save(failOnError: true)
		new Sistema(descripcion: "BIOS_SERVICIO_SUR" , descripcionLarga:  "BIOS - Bus de Integraci\u00F3n Corporativo" , grupoAdministrador:  "AP_CH_BIOS_SERVICIO_SUR", grupoReferenteSWF: GRUPO_INTERLOCUTOR_USUARIO, gerencia:"DTI_DSPI_INTEGRACION", grupoTuID: "").save(failOnError: true)
		new Sistema(descripcion: "BIOS_SERVICIO_TESTING" , descripcionLarga:  "BIOS - Bus de Integraci\u00F3n Corporativo" , grupoAdministrador:  "AP_CH_BIOS_SERVICIO_TESTING", grupoReferenteSWF: GRUPO_INTERLOCUTOR_USUARIO, gerencia:"DTI_DSPI_INTEGRACION", grupoTuID: "").save(failOnError: true)
		new Sistema(descripcion: "BIOS_SERVICIO_UTOPIA" , descripcionLarga:  "BIOS - Bus de Integraci\u00F3n Corporativo" , grupoAdministrador:  "AP_CH_BIOS_SERVICIO_UTOPIA", grupoReferenteSWF: GRUPO_INTERLOCUTOR_USUARIO, gerencia:"DTI_DSPI_INTEGRACION", grupoTuID: "").save(failOnError: true)
		new Sistema(descripcion: "BIOS_SERVICIO_VERAZ" , descripcionLarga:  "BIOS - Bus de Integraci\u00F3n Corporativo" , grupoAdministrador:  "AP_CH_BIOS_SERVICIO_VERAZ", grupoReferenteSWF: GRUPO_INTERLOCUTOR_USUARIO, gerencia:"DTI_DSPI_INTEGRACION", grupoTuID: "").save(failOnError: true)
		new Sistema(descripcion: "BLOOMBERG_APL" , descripcionLarga:  "COTIZACIONES VARIAS" , grupoAdministrador:  "AP_CH_BLOOMBERG_APL", grupoReferenteSWF: GRUPO_INTERLOCUTOR_USUARIO, gerencia:"DSI_DSC_NO-SIGNO", grupoTuID: "").save(failOnError: true)
		new Sistema(descripcion: "CALIPSO_COBROS" , descripcionLarga:  "Sistema de cobranzas del segmento mayorista." , grupoAdministrador:  "AP_CH_CALIPSO_COBROS", grupoReferenteSWF: GRUPO_INTERLOCUTOR_USUARIO, gerencia:"DTI_DSFC_FACT-GC-WH", grupoTuID: "").save(failOnError: true)
		new Sistema(descripcion: "CALIPSO_FACTURACION" , descripcionLarga:  "Sistema de facturaci\u00F3n del segmento mayorista." , grupoAdministrador:  "AP_CH_CALIPSO_FACTURACION", grupoReferenteSWF: GRUPO_INTERLOCUTOR_USUARIO, gerencia:"DTI_DSFC_FACT-GC-WH", grupoTuID: "").save(failOnError: true)
		new Sistema(descripcion: "CALIPSO_PREFACTURACION" , descripcionLarga:  "Sistema de prefacturaci\u00F3n del segmento mayorista." , grupoAdministrador:  "AP_CH_CALIPSO_PREFACTURACION", grupoReferenteSWF: GRUPO_INTERLOCUTOR_USUARIO, gerencia:"DTI_DSFC_FACT-GC-WH", grupoTuID: "").save(failOnError: true)
		new Sistema(descripcion: "CALLWEB_APL" , descripcionLarga:  "Aplicativo para consulta de llamadas almacenadas en el contestador v\u00EDa Web" , grupoAdministrador:  "", grupoReferenteSWF: GRUPO_INTERLOCUTOR_USUARIO, gerencia:"DTI_DSPI", grupoTuID: "").save(failOnError: true)
		new Sistema(descripcion: "CAMPAIGN_MANAGEMENT_APL" , descripcionLarga:  "SAS CAMPAIGN MANAGEMENT TENDR\u00E1 COMO  FUENTE PRINCIPAL DE DATOS UN CORE DE DATAMART DE CAMPAÑAS QUE CONTENDR\u00E1 TODA LA INFORMACI\u00F3N NECESARIA PARA LOS PROCESOS DE GESTI\u00F3N DE CAMPAÑA (SELECCI\u00F3N DEL P\u00FABLICO OBJETIVO DE LAS CAMPAÑAS Y CO" , grupoAdministrador:  "AP_CH_CAMPAIGN_MANAGEMENT_APL", grupoReferenteSWF: GRUPO_INTERLOCUTOR_USUARIO, gerencia:"DTI", grupoTuID: "").save(failOnError: true)
		new Sistema(descripcion: "CARRIERS_INTERCONECTANTES_APL" , descripcionLarga:  "Sistemas que proceso el Tr\u00E1fico Mayoriasta Internacional para realizar la valori" , grupoAdministrador:  "AP_CH_CARRIERS_INTERCONECTANTES_APL", grupoReferenteSWF: GRUPO_INTERLOCUTOR_USUARIO, gerencia:"DTI_DSFC_FACT-GC-WH", grupoTuID: "").save(failOnError: true)
		new Sistema(descripcion: "CARRIERS_INTERCONECTANTES_DISCOVERER" , descripcionLarga:  "Sistemas que proceso el Tr\u00E1fico Mayoriasta Internacional para realizar la valori" , grupoAdministrador:  "AP_CH_CARRIERS_INTERCONECTANTES_DISCOVERER", grupoReferenteSWF: GRUPO_INTERLOCUTOR_USUARIO, gerencia:"DTI_DSFC_FACT-GC-WH", grupoTuID: "").save(failOnError: true)
		new Sistema(descripcion: "CATALOGO_PYS_APL" , descripcionLarga:  "Brinda las caracter\u00EDsticas comerciales de los Productos y Servicios y las caract" , grupoAdministrador:  "AP_CH_CATALOGO_PYS_APL", grupoReferenteSWF: GRUPO_INTERLOCUTOR_USUARIO, gerencia:"DTI_DSPI_PROV-MERCADO-NO-MASIVO", grupoTuID: "").save(failOnError: true)
		new Sistema(descripcion: "CCTBI_APL" , descripcionLarga:  "Sistema que analiza la informacion de venta del Call Center de Telecom, a trav\u00E9s" , grupoAdministrador:  "AP_CH_CCTBI_APL", grupoReferenteSWF: "RSWF_CCTBI", gerencia:"DTI_DSIN_PLATDW", grupoTuID: "").save(failOnError: true)
		new Sistema(descripcion: "CCTBI_CARGA_Y_PUBLICACION" , descripcionLarga:  "Sistema que analiza la informacion de venta del Call Center de Telecom, a trav\u00E9s" , grupoAdministrador:  "AP_CH_CCTBI_CARGA_Y_PUBLICACION", grupoReferenteSWF: GRUPO_INTERLOCUTOR_USUARIO, gerencia:"DTI_DSIN_PLATDW", grupoTuID: "").save(failOnError: true)
		new Sistema(descripcion: "CCTBI_HERRAMIENTA_MICROSTRATEGY" , descripcionLarga:  "Sistema que analiza la informacion de venta del Call Center de Telecom, a trav\u00E9s" , grupoAdministrador:  "AP_CH_CCTBI_HERRAMIENTA_MICROSTRATEGY", grupoReferenteSWF: GRUPO_INTERLOCUTOR_USUARIO, gerencia:"DTI_DSIN_PLATDW", grupoTuID: "").save(failOnError: true)
		new Sistema(descripcion: "CCTBI_MODELO_DE_DATOS" , descripcionLarga:  "Sistema que analiza la informacion de venta del Call Center de Telecom, a trav\u00E9s" , grupoAdministrador:  "AP_CH_CCTBI_MODELO_DE_DATOS", grupoReferenteSWF: GRUPO_INTERLOCUTOR_USUARIO, gerencia:"DTI_DSIN_PLATDW", grupoTuID: "").save(failOnError: true)
		new Sistema(descripcion: "CENTRAL_PARQ_APL" , descripcionLarga:  "Es la replica del parque de los clientes ( Girafe - Utopia - SIDI)" , grupoAdministrador:  "AP_CH_CENTRAL_PARQ_APL", grupoReferenteSWF: GRUPO_INTERLOCUTOR_USUARIO, gerencia:"DTI_DSPI_PROV-SISTEMAS-BASICOS", grupoTuID: "").save(failOnError: true)
		new Sistema(descripcion: "CENTRO_VALIDADOR_APL" , descripcionLarga:  "Aplicativo que realiza todas las actividades relacionadas con la adquisici\u00F3n, es" , grupoAdministrador:  "AP_CH_CENTRO_VALIDADOR_APL", grupoReferenteSWF: GRUPO_INTERLOCUTOR_USUARIO, gerencia:"DTI_DSFC_MEDIACION-FRAUDE", grupoTuID: "").save(failOnError: true)
		new Sistema(descripcion: "CENTRO_VALIDADOR_WEB" , descripcionLarga:  "Aplicativo que realiza todas las actividades relacionadas con la adquisici\u00F3n, es" , grupoAdministrador:  "AP_CH_CENTRO_VALIDADOR_WEB", grupoReferenteSWF: GRUPO_INTERLOCUTOR_USUARIO, gerencia:"DTI_DSFC_MEDIACION-FRAUDE", grupoTuID: "").save(failOnError: true)
		new Sistema(descripcion: "CERTIFICACIONES_APL" , descripcionLarga:  "Ceritificaciones" , grupoAdministrador:  "AP_CH_CERTIFICACIONES_APL", grupoReferenteSWF: GRUPO_INTERLOCUTOR_USUARIO, gerencia:"DSI_DSC_DENARIUS", grupoTuID: "").save(failOnError: true)
		new Sistema(descripcion: "CGL_APL" , descripcionLarga:  "Control de Gesti\u00F3n de Lotes, intercambios con los Terceros (consumos).Este aplic" , grupoAdministrador:  "AP_CH_CGL_APL", grupoReferenteSWF: GRUPO_INTERLOCUTOR_USUARIO, gerencia:"DTI_DSFC_COBR-MASIVOS", grupoTuID: "").save(failOnError: true)
		new Sistema(descripcion: "CICLOPE_APL" , descripcionLarga:  "Sistema para la Ingenier\u00EDa de Plantel Exterior que permite automatizar y optimiz" , grupoAdministrador:  "AP_CH_CICLOPE_APL", grupoReferenteSWF: GRUPO_INTERLOCUTOR_USUARIO, gerencia:"DSI_DST_DSIN", grupoTuID: "").save(failOnError: true)
		new Sistema(descripcion: "CINDI_APL" , descripcionLarga:  "SISTEMA DE GENERACI\u00F3N DE INDICADORES DE NIVEL DE SERVICIO PARA CONTRATOS DE PROV" , grupoAdministrador:  "AP_CH_CINDI_APL", grupoReferenteSWF: GRUPO_INTERLOCUTOR_USUARIO, gerencia:"DSI_DSC_CINDI", grupoTuID: "").save(failOnError: true)
		new Sistema(descripcion: "CITAS_APL" , descripcionLarga:  "M\u00F3dulo encargado de administrar recursos y reservar agendas para provisi\u00F3n de ki" , grupoAdministrador:  "AP_CH_CITAS_APL", grupoReferenteSWF: GRUPO_INTERLOCUTOR_USUARIO, gerencia:"DTI_DSPI_PROV-SERV-INTERNET", grupoTuID: "").save(failOnError: true)
		new Sistema(descripcion: "CMS_APL" , descripcionLarga:  "Customer Management System. Front end \u00FAnico para la atenci\u00F3n de clientes residen" , grupoAdministrador:  "AP_CH_CMS_APL", grupoReferenteSWF: GRUPO_INTERLOCUTOR_USUARIO, gerencia:"DTI_DSCRM_CMS", grupoTuID: "").save(failOnError: true)
		new Sistema(descripcion: "CNM_APL" , descripcionLarga:  "Sistema de Customer Network Management (CNM) desarrollado para ofrecer informaci" , grupoAdministrador:  "AP_CH_CNM_APL", grupoReferenteSWF: GRUPO_INTERLOCUTOR_USUARIO, gerencia:"DSI_DST_DSRT", grupoTuID: "").save(failOnError: true)
		new Sistema(descripcion: "COBRANZA_ELECTRONICA_BACK_END" , descripcionLarga:  "Modulo Web que permite el pago electr\u00F3nico de las facturas pendientes de pago (b" , grupoAdministrador:  "AP_CH_COBRANZA_ELECTRONICA_BACK_END", grupoReferenteSWF: GRUPO_INTERLOCUTOR_USUARIO, gerencia:"DTI_DSFC_COBR-MASIVOS", grupoTuID: "").save(failOnError: true)
		new Sistema(descripcion: "COBRANZA_ELECTRONICA_FRONT_END" , descripcionLarga:  "Modulo Web que permite el pago electr\u00F3nico de las facturas pendientes de pago (f" , grupoAdministrador:  "AP_CH_COBRANZA_ELECTRONICA_FRONT_END", grupoReferenteSWF: GRUPO_INTERLOCUTOR_USUARIO, gerencia:"DTI_DSFC_COBR-MASIVOS", grupoTuID: "").save(failOnError: true)
		new Sistema(descripcion: "COBRANZA_ELECTRONICA_WONDERSOFT" , descripcionLarga:  "Modulo Web que permite el pago electr\u00F3nico de las facturas pendientes de pago (o" , grupoAdministrador:  "AP_CH_COBRANZA_ELECTRONICA_WONDERSOFT", grupoReferenteSWF: GRUPO_INTERLOCUTOR_USUARIO, gerencia:"DTI_DSFC_COBR-MASIVOS", grupoTuID: "").save(failOnError: true)
		new Sistema(descripcion: "COBRANZA_INTEGRADA_FRONT_END" , descripcionLarga:  "Modulo web que pertine al cliente unificar las facturas pendientes de pago con l" , grupoAdministrador:  "AP_CH_COBRANZA_INTEGRADA_FRONT_END", grupoReferenteSWF: GRUPO_INTERLOCUTOR_USUARIO, gerencia:"DTI_DSFC_COBR-MASIVOS", grupoTuID: "").save(failOnError: true)
		new Sistema(descripcion: "CRIS_APL" , descripcionLarga:  "M\u00F3dulo funcional de CRM URSI cuya finalidad es soportar la  registraci\u00F3n, an\u00E1lis" , grupoAdministrador:  "AP_CH_CRIS_APL", grupoReferenteSWF: GRUPO_INTERLOCUTOR_USUARIO, gerencia:"DTI_DSCRM_CRIS", grupoTuID: "").save(failOnError: true)
		new Sistema(descripcion: "CRM_USE_OYP_APL" , descripcionLarga:  "Sistema de CRM (Customer Relationship Management) para la atenci\u00F3n de clientes U" , grupoAdministrador:  "AP_CH_CRM_USE_OYP_APL", grupoReferenteSWF: GRUPO_INTERLOCUTOR_USUARIO, gerencia:"DTI_DSPI_PROV-MERCADO-NO-MASIVO", grupoTuID: "").save(failOnError: true)
		new Sistema(descripcion: "CUENTA_CORPORATIVA_BACK_END" , descripcionLarga:  "Aplicativo Web el cual permite que Grandes Clientes consulten el detalle de su f" , grupoAdministrador:  "AP_CH_CUENTA_CORPORATIVA_BACK_END", grupoReferenteSWF: GRUPO_INTERLOCUTOR_USUARIO, gerencia:"DTI_DSFC_COBR-MASIVOS", grupoTuID: "").save(failOnError: true)
		new Sistema(descripcion: "CUENTA_CORPORATIVA_FRONT_END" , descripcionLarga:  "Aplicativo Web el cual permite que Grandes Clientes consulten el detalle de su f" , grupoAdministrador:  "AP_CH_CUENTA_CORPORATIVA_FRONT_END", grupoReferenteSWF: GRUPO_INTERLOCUTOR_USUARIO, gerencia:"DTI_DSFC_COBR-MASIVOS", grupoTuID: "").save(failOnError: true)
		new Sistema(descripcion: "CUENTA_ONLINE_BACK_END_APL" , descripcionLarga:  "SISTEMA QUE PERMITE LA ADMINISTRACI\u00F3N PARA LOS USUARIOS INTERNOS." , grupoAdministrador:  "AP_CH_CUENTA_ONLINE_BACK_END_APL", grupoReferenteSWF: GRUPO_INTERLOCUTOR_USUARIO, gerencia:"DTI_DSFC_COBR-MASIVOS", grupoTuID: "").save(failOnError: true)
		new Sistema(descripcion: "CUENTA_ONLINE_FRONT_END_APL" , descripcionLarga:  "SISTEMA QUE PERMITE QUE LOS GRANDES CLIENTES Y WHOLESALE CONSULTEN EL DETALLE DE" , grupoAdministrador:  "AP_CH_CUENTA_ONLINE_FRONT_END_APL", grupoReferenteSWF: GRUPO_INTERLOCUTOR_USUARIO, gerencia:"DTI_DSFC_COBR-MASIVOS", grupoTuID: "").save(failOnError: true)
		new Sistema(descripcion: "DACOTA_APL" , descripcionLarga:  "ELEMENTO DE CONFIGURACI\u00F3N PARA TEMAS ASOCIADOS CON EL APLICATIVO EN GENERAL" , grupoAdministrador:  "AP_CH_DACOTA_APL", grupoReferenteSWF: "RSWF_DACOTA", gerencia:"DTI_DSIN_PLATDW", grupoTuID: "").save(failOnError: true)
		new Sistema(descripcion: "DATA_ACTIVATOR_APL" , descripcionLarga:  "El sistema Data Activator consiste en una plataforma para la activaci\u00F3n de servi" , grupoAdministrador:  "AP_CH_DATA_ACTIVATOR_APL", grupoReferenteSWF: GRUPO_INTERLOCUTOR_USUARIO, gerencia:"DSI_DST", grupoTuID: "").save(failOnError: true)
		new Sistema(descripcion: "DDT_APL" , descripcionLarga:  "DDT ES EL SISTEMA ENCARGADO DE GESTIONAR INFORMACI\u00F3N RELACIONADA AL PROCESO DE FACTURACI\u00F3N." , grupoAdministrador:  "AP_CH_DDT_APL", grupoReferenteSWF: GRUPO_INTERLOCUTOR_USUARIO, gerencia:"", grupoTuID: "").save(failOnError: true)
		new Sistema(descripcion: "DEIMOS_APL" , descripcionLarga:  "Sistema que administra los planes de financiaci?n de las facturas impagas de cli" , grupoAdministrador:  "AP_CH_DEIMOS_APL", grupoReferenteSWF: GRUPO_INTERLOCUTOR_USUARIO, gerencia:"DTI_DSFC_COBR-MASIVOS", grupoTuID: "").save(failOnError: true)
		new Sistema(descripcion: "DELTA_APL" , descripcionLarga:  "APLICACI\u00F3N WEB PARA LA GESTION DE PREVENTA, VENTA Y POSVENTA DE SERVICIOS DE DATOS DE GRANDES CLIENTES Y PYMES." , grupoAdministrador:  "AP_CH_SIDI_APL", grupoReferenteSWF: GRUPO_INTERLOCUTOR_USUARIO, gerencia:"", grupoTuID: "").save(failOnError: true)
		new Sistema(descripcion: "DENARIUS_APL" , descripcionLarga:  "Aplicativo que soporta las funciones  inherentes a la liquidaci\u00F3n de sueldos, va" , grupoAdministrador:  "AP_CH_DENARIUS_APL", grupoReferenteSWF: "RSWF_DENARIUS", gerencia:"DSI_DSC_DENARIUS", grupoTuID: "").save(failOnError: true)
		new Sistema(descripcion: "DENARIUS_FUNCIONAL" , descripcionLarga:  "MODULO FUNCIONAL" , grupoAdministrador:  "AP_CH_DENARIUS_FUNCIONAL", grupoReferenteSWF: GRUPO_INTERLOCUTOR_USUARIO, gerencia:"DSI_DSC_DENARIUS", grupoTuID: "").save(failOnError: true)
		new Sistema(descripcion: "DENARIUS_MASTER" , descripcionLarga:  "Modulo de Conceptos" , grupoAdministrador:  "AP_CH_DENARIUS_MASTER", grupoReferenteSWF: GRUPO_INTERLOCUTOR_USUARIO, gerencia:"DSI_DSC_DENARIUS", grupoTuID: "").save(failOnError: true)
		new Sistema(descripcion: "DIARIA_.NET_APL" , descripcionLarga:  "Proceso de centralizaci\u00F3n, traducci\u00F3n de c\u00F3digo, validaci\u00F3n e interfaces con BAS" , grupoAdministrador:  "AP_CH_DIARIA_.NET_APL", grupoReferenteSWF: GRUPO_INTERLOCUTOR_USUARIO, gerencia:"DTI_DSPI", grupoTuID: "").save(failOnError: true)
		new Sistema(descripcion: "E-MAILING_FRONT_END_APL" , descripcionLarga:  "MAIL AVISO DE FACTURA A CLIENTES SUSCRIPTOS  A LA APLICACION FACTURA SIN PAPEL" , grupoAdministrador:  "AP_CH_E-MAILING_FRONT_END_APL", grupoReferenteSWF: GRUPO_INTERLOCUTOR_USUARIO, gerencia:"DTI_DSFC_FACT-MERCADO-MASIVO", grupoTuID: "").save(failOnError: true)
		new Sistema(descripcion: "EPM_DSI_APL" , descripcionLarga:  "Gesti\u00F3n de proyectos de IT." , grupoAdministrador:  "AP_CH_EPM_DSI_APL", grupoReferenteSWF: GRUPO_INTERLOCUTOR_USUARIO, gerencia:"DSI_PMO-IT", grupoTuID: "").save(failOnError: true)
		new Sistema(descripcion: "ESTADISTICA_0800/0810_BACK_END" , descripcionLarga:  "Sistema Web que permite a clientes del 0800/0810 hacer consultas estadist\u00EDcas so" , grupoAdministrador:  "AP_CH_ESTADISTICA_0800/0810_BACK_END", grupoReferenteSWF: GRUPO_INTERLOCUTOR_USUARIO, gerencia:"DTI_DSFC_COBR-MASIVOS", grupoTuID: "").save(failOnError: true)
		new Sistema(descripcion: "ESTADISTICA_0800/0810_FRONT_END" , descripcionLarga:  "Sistema Web que permite a clientes del 0800/0810 hacer consultas estadist\u00EDcas so" , grupoAdministrador:  "AP_CH_ESTADISTICA_0800/0810_FRONT_END", grupoReferenteSWF: GRUPO_INTERLOCUTOR_USUARIO, gerencia:"DTI_DSFC_COBR-MASIVOS", grupoTuID: "").save(failOnError: true)
		new Sistema(descripcion: "EVA_CUENTAS_CORRIENTES_MASIVO" , descripcionLarga:  "Sistema que procesa informaci\u00F3n de cobranzas para detectar y ejecutar acciones c" , grupoAdministrador:  "AP_CH_EVA_CUENTAS_CORRIENTES_MASIVO", grupoReferenteSWF: GRUPO_INTERLOCUTOR_USUARIO, gerencia:"DTI_DSIN_EVA", grupoTuID: "").save(failOnError: true)
		new Sistema(descripcion: "EVA_MOROSIDAD/COBRANZAS_GC/WS" , descripcionLarga:  "Sistema que procesa informaci\u00F3n de cobranzas para detectar y ejecutar acciones c" , grupoAdministrador:  "AP_CH_EVA_MOROSIDAD/COBRANZAS_GC/WS", grupoReferenteSWF: GRUPO_INTERLOCUTOR_USUARIO, gerencia:"DTI_DSIN_EVA", grupoTuID: "").save(failOnError: true)
		new Sistema(descripcion: "EVA_MOROSIDAD/FINANCIACION_MASIVO" , descripcionLarga:  "Sistema que procesa informaci\u00F3n de cobranzas para detectar y ejecutar acciones c" , grupoAdministrador:  "AP_CH_EVA_MOROSIDAD/FINANCIACION_MASIVO", grupoReferenteSWF: GRUPO_INTERLOCUTOR_USUARIO, gerencia:"DTI_DSIN_EVA", grupoTuID: "").save(failOnError: true)
		new Sistema(descripcion: "EVA_POSTBAJA/COMPUTO_IMPOSITIVO" , descripcionLarga:  "Sistema que procesa informaci\u00F3n de cobranzas para detectar y ejecutar acciones c" , grupoAdministrador:  "AP_CH_EVA_POSTBAJA/COMPUTO_IMPOSITIVO", grupoReferenteSWF: GRUPO_INTERLOCUTOR_USUARIO, gerencia:"DTI_DSIN_EVA", grupoTuID: "").save(failOnError: true)
		new Sistema(descripcion: "EVA_REPORTING_MASIVO" , descripcionLarga:  "Sistema que procesa informaci\u00F3n de cobranzas para detectar y ejecutar acciones c" , grupoAdministrador:  "AP_CH_EVA_REPORTING_MASIVO", grupoReferenteSWF: GRUPO_INTERLOCUTOR_USUARIO, gerencia:"DTI_DSIN_EVA", grupoTuID: "").save(failOnError: true)
		new Sistema(descripcion: "EXPERTO_AFIP_APL" , descripcionLarga:  "Contabiliza los tiempos y penalidades por incumplimiento en la prestaci\u00F3n de Ser" , grupoAdministrador:  "AP_CH_EXPERTO_AFIP_APL", grupoReferenteSWF: GRUPO_INTERLOCUTOR_USUARIO, gerencia:"DSI_DST_DSFP", grupoTuID: "").save(failOnError: true)
		new Sistema(descripcion: "E_LEARNING_APL" , descripcionLarga:  "Sistema de Capacitacion" , grupoAdministrador:  "AP_CH_E_LEARNING_APL", grupoReferenteSWF: GRUPO_INTERLOCUTOR_USUARIO, gerencia:"DSI_DSC_NEOPORTAL", grupoTuID: "").save(failOnError: true)
		new Sistema(descripcion: "E_REPORTING_APL" , descripcionLarga:  "Consulta y emisi\u00F3n de reportes de control de gesti\u00F3n." , grupoAdministrador:  "AP_CH_E_REPORTING_APL", grupoReferenteSWF: GRUPO_INTERLOCUTOR_USUARIO, gerencia:"DSI_DSC_NO-SIGNO", grupoTuID: "").save(failOnError: true)
		new Sistema(descripcion: "E_REPORTING_SOCIOS_APL" , descripcionLarga:  "Consulta y emisi\u00F3n de reportes de control de gesti\u00F3n para socios de TELECOM Ital" , grupoAdministrador:  "AP_CH_E_REPORTING_SOCIOS_APL", grupoReferenteSWF: GRUPO_INTERLOCUTOR_USUARIO, gerencia:"DSI_DSC_NO-SIGNO", grupoTuID: "").save(failOnError: true)
		new Sistema(descripcion: "FACTESP_APL" , descripcionLarga:  "Sistema de Facturaciones Especiales. Se utiliza para procesar la facturaci\u00F3n de" , grupoAdministrador:  "AP_CH_FACTESP_APL", grupoReferenteSWF: GRUPO_INTERLOCUTOR_USUARIO, gerencia:"DTI_DSFC_FACT-MERCADO-MASIVO", grupoTuID: "").save(failOnError: true)
		new Sistema(descripcion: "FAULT_UNIFICADO_APL" , descripcionLarga:  "TIENE COMO FINALIDAD PRESENTAR EN TIEMPO REAL LAS FALLAS DE LA RED" , grupoAdministrador:  "AP_CH_FAULT_UNIFICADO_APL", grupoReferenteSWF: GRUPO_INTERLOCUTOR_USUARIO, gerencia:"DSI_DST_DSFP", grupoTuID: "").save(failOnError: true)
		new Sistema(descripcion: "FAX_SERVER_APL" , descripcionLarga:  "Fax Server es una aplicaci\u00F3n para recepci\u00F3n, administraci\u00F3n y almacenamiento de" , grupoAdministrador:  "AP_CH_FAX_SERVER_APL", grupoReferenteSWF: GRUPO_INTERLOCUTOR_USUARIO, gerencia:"DTI_DSIN", grupoTuID: "").save(failOnError: true)
		new Sistema(descripcion: "FAX_SERVER_SEGAT_APL" , descripcionLarga:  "El ivr soporte de las \u00F3rdenes de trabajo para SEGAT" , grupoAdministrador:  "AP_CH_FAX_SERVER_SEGAT_APL", grupoReferenteSWF: GRUPO_INTERLOCUTOR_USUARIO, gerencia:"DTI_DSIN", grupoTuID: "").save(failOnError: true)
		new Sistema(descripcion: "FOSSE_APL" , descripcionLarga:  "ENV\u00EDO Y RECEPCI\u00F3N DE FACTURAS CON AFIP." , grupoAdministrador:  "AP_CH_FOSSE_FRONT_END_APL", grupoReferenteSWF: GRUPO_INTERLOCUTOR_USUARIO, gerencia:"FACTURACI\u00F3N Y COBRANZAS", grupoTuID: "").save(failOnError: true)
		new Sistema(descripcion: "FRAUD_VIEW_APL" , descripcionLarga:  "Sistema de Control de Fraudes en llamadas internacionales y de Larga Distancia N" , grupoAdministrador:  "AP_CH_FRAUD_VIEW_APL", grupoReferenteSWF: GRUPO_INTERLOCUTOR_USUARIO, gerencia:"DTI_DSFC_MEDIACION-FRAUDE", grupoTuID: "").save(failOnError: true)
		new Sistema(descripcion: "FRONTBI_CCTBI_APL" , descripcionLarga:  "FRONT PARA LA EXPLOTACI\u00F3N DE LOS DATOS DE CCTBI" , grupoAdministrador:  "AP_CH_FRONTBI_CCTBI_APL", grupoReferenteSWF: GRUPO_INTERLOCUTOR_USUARIO, gerencia:"DTI_DSIN_SISTSOPGEST", grupoTuID: "").save(failOnError: true)
		new Sistema(descripcion: "FRONTBI_DACOTA_APL" , descripcionLarga:  "FRONTBI_DACOTA_APL" , grupoAdministrador:  "AP_CH_FRONTBI_DACOTA_APL", grupoReferenteSWF: GRUPO_INTERLOCUTOR_USUARIO, gerencia:"DTI_DSIN_SISTSOPGEST", grupoTuID: "").save(failOnError: true)
		new Sistema(descripcion: "FRONTBI_SMART_APL" , descripcionLarga:  "FRONT PARA LA EXPLOTACI\u00F3N DE LOS DATOS DE SMART" , grupoAdministrador:  "AP_CH_FRONTBI_SMART_APL", grupoReferenteSWF: GRUPO_INTERLOCUTOR_USUARIO, gerencia:"DTI_DSIN_SISTSOPGEST", grupoTuID: "").save(failOnError: true)
		new Sistema(descripcion: "FRONTBI_TABGO_APL" , descripcionLarga:  "FRONT PARA LA EXPLOTACI\u00F3N DE LOS DATOS DE TABLERO DE GESTI\u00F3N OPERATIVA" , grupoAdministrador:  "AP_CH_FRONTBI_TABGO_APL", grupoReferenteSWF: GRUPO_INTERLOCUTOR_USUARIO, gerencia:"DTI_DSIN_SISTSOPGEST", grupoTuID: "").save(failOnError: true)
		new Sistema(descripcion: "FRONTBI_TABRA_APL" , descripcionLarga:  "FRONT PARA TABLERO DE REVENUE ASSURANCE" , grupoAdministrador:  "AP_CH_FRONTBI_TABRA_APL", grupoReferenteSWF: GRUPO_INTERLOCUTOR_USUARIO, gerencia:"DTI_DSIN_SISTSOPGEST", grupoTuID: "").save(failOnError: true)
		new Sistema(descripcion: "FRONTBI_ZOOM_APL" , descripcionLarga:  "FRONT PARA LA EXPLOTACI\u00F3N DE LOS DATOS DE ZOOM" , grupoAdministrador:  "AP_CH_FRONTBI_ZOOM_APL", grupoReferenteSWF: GRUPO_INTERLOCUTOR_USUARIO, gerencia:"DTI_DSIN_SISTSOPGEST", grupoTuID: "").save(failOnError: true)
		new Sistema(descripcion: "FW_DESKTOP_ENDPOINT_APL" , descripcionLarga:  "FIREWALL ENDPOINT PARA WORKSTATIONS" , grupoAdministrador:  "SI_CH_TECNO_MS", grupoReferenteSWF: GRUPO_INTERLOCUTOR_USUARIO, gerencia:"SEGURIDAD INFORMATICA", grupoTuID: "").save(failOnError: true)
		new Sistema(descripcion: "GARDEL_APL" , descripcionLarga:  "Sistema de administraci\u00F3n y gesti\u00F3n de la informaci\u00F3n de los RR.HH. de todas las" , grupoAdministrador:  "AP_CH_GARDEL_APL", grupoReferenteSWF: GRUPO_INTERLOCUTOR_USUARIO, gerencia:"DSI_DSC_NO-SIGNO", grupoTuID: "").save(failOnError: true)
		new Sistema(descripcion: "GENESIS_BILL_CYCLE" , descripcionLarga:  "M\u00F3dulo del Sistema GENESIS cuyas funciones principales corresponden a la Factura" , grupoAdministrador:  "AP_CH_GENESIS_BILL_CYCLE", grupoReferenteSWF: "RSWF_GENESIS_BILL_CYCLE", gerencia:"DTI_DSFC_FACT-MERCADO-MASIVO", grupoTuID: "").save(failOnError: true)
		new Sistema(descripcion: "GENESIS_CUSTOMER" , descripcionLarga:  "M\u00F3dulo del Sistema GENESIS que posee la  base de datos de clientes para la factu" , grupoAdministrador:  "AP_CH_GENESIS_CUSTOMER", grupoReferenteSWF: "RSWF_GENESIS_CUSTOMER", gerencia:"DTI_DSFC_FACT-MERCADO-MASIVO", grupoTuID: "").save(failOnError: true)
		new Sistema(descripcion: "GENESIS_MIC" , descripcionLarga:  "M\u00F3dulo Integrado de Cobranzas,  correspondiente al Sistema Genesis cuyas princip" , grupoAdministrador:  "AP_CH_GENESIS_MIC", grupoReferenteSWF: "RSWF_GENESIS_MIC", gerencia:"DTI_DSFC_COBR-MASIVOS", grupoTuID: "").save(failOnError: true)
		new Sistema(descripcion: "GENESIS_OYP" , descripcionLarga:  "M\u00F3dulo del Sistema GENESIS cuyas funciones principales corresponden a la prepara" , grupoAdministrador:  "AP_CH_GENESIS_OYP", grupoReferenteSWF: "RSWF_GENESIS_OYP", gerencia:"DTI_DSFC_FACT-MERCADO-MASIVO", grupoTuID: "").save(failOnError: true)
		new Sistema(descripcion: "GENESIS_UPS" , descripcionLarga:  "M\u00F3dulo del Sistema GENESIS cuyas funciones principales corresponden a la Pre-Fac" , grupoAdministrador:  "AP_CH_GENESIS_UPS", grupoReferenteSWF: "RSWF_GENESIS_UPS", gerencia:"DTI_DSFC_FACT-MERCADO-MASIVO", grupoTuID: "").save(failOnError: true)
		new Sistema(descripcion: "GEO_APL" , descripcionLarga:  "GESTI\u00F3N DE OBLIGACIONES" , grupoAdministrador:  "AP_CH_GEO", grupoReferenteSWF: GRUPO_INTERLOCUTOR_USUARIO, gerencia:"DTI_DSFC_FACT-GC-WH", grupoTuID: "").save(failOnError: true)
		new Sistema(descripcion: "GESPROD_APL" , descripcionLarga:  "Aplicativo que permite el ingreso de otros conceptos a acreditar o debitar en la" , grupoAdministrador:  "AP_CH_GESPROD_APL", grupoReferenteSWF: GRUPO_INTERLOCUTOR_USUARIO, gerencia:"DTI_DSPI", grupoTuID: "").save(failOnError: true)
		new Sistema(descripcion: "GESTION_DE_EMPRENDIMIENTOS_APL" , descripcionLarga:  "El sistema Gesti\u00F3n de Emprendimientos es una herramienta para la creaci\u00F3n y admi" , grupoAdministrador:  "AP_CH_GESTION_DE_EMPRENDIMIENTOS_APL", grupoReferenteSWF: GRUPO_INTERLOCUTOR_USUARIO, gerencia:"DTI_DSIN_SISTSOPGEST", grupoTuID: "").save(failOnError: true)
		new Sistema(descripcion: "GESTOR_DE_COMISIONES_APL" , descripcionLarga:  "GESTOR DE COMISIONES DE TELECOM ARGENTINA" , grupoAdministrador:  "AP_CH_GESTOR_DE_COMISIONES_APL", grupoReferenteSWF: GRUPO_INTERLOCUTOR_USUARIO, gerencia:"", grupoTuID: "").save(failOnError: true)
		new Sistema(descripcion: "GIRAFE_APL" , descripcionLarga:  "Gesti\u00F3n comercial de clientes, Provisi\u00F3n, cargos recurrentes, Reclamos t\u00E9cnicos" , grupoAdministrador:  "AP_CH_GIRAFE_APL", grupoReferenteSWF: "RSWF_GIRAFE", gerencia:"DTI_DSPI_PROV-SISTEMAS-BASICOS", grupoTuID: "").save(failOnError: true)
		new Sistema(descripcion: "HARVEST_APL" , descripcionLarga:  "AUTOMATIZACI\u00F3N DE AMBIENTES" , grupoAdministrador:  "AP_CH_HARVEST_APL", grupoReferenteSWF: GRUPO_INTERLOCUTOR_USUARIO, gerencia:"DSI_OYT_SO-AUTO", grupoTuID: "").save(failOnError: true)
		new Sistema(descripcion: "INFOPACK_APL" , descripcionLarga:  "Confecci\u00F3n de manuales." , grupoAdministrador:  "AP_CH_INFOPACK_APL", grupoReferenteSWF: GRUPO_INTERLOCUTOR_USUARIO, gerencia:"DSI_DSC_NO-SIGNO", grupoTuID: "").save(failOnError: true)
		new Sistema(descripcion: "INTERCONNECT_APL" , descripcionLarga:  "Sistema encargado de la prefacturaci\u00F3n del tr\u00E1fico de interconexi\u00F3n (Mayorista)." , grupoAdministrador:  "AP_CH_INTERCONNECT_APL", grupoReferenteSWF: GRUPO_INTERLOCUTOR_USUARIO, gerencia:"DTI_DSFC_MEDIACION-FRAUDE", grupoTuID: "").save(failOnError: true)
		new Sistema(descripcion: "INTERMOVIL_APL" , descripcionLarga:  "APLICACI\u00F3N QUE PERMITE EJECUTAR SOLICITUDES DE INTERVENCI\u00F3N DE L\u00EDNEAS DE TELEFON\u00EDA M\u00F3VIL." , grupoAdministrador:  "AP_INTERMOVIL_APL", grupoReferenteSWF: GRUPO_INTERLOCUTOR_USUARIO, gerencia:"DSI_DST_DSPFM", grupoTuID: "").save(failOnError: true)
		new Sistema(descripcion: "INVESCOLD_APL" , descripcionLarga:  "Registraci\u00F3n de libros de IVA de la Compañ\u00EDa." , grupoAdministrador:  "AP_CH_INVESCOLD_APL", grupoReferenteSWF: GRUPO_INTERLOCUTOR_USUARIO, gerencia:"DSI_DSC_NO-SIGNO", grupoTuID: "").save(failOnError: true)
		new Sistema(descripcion: "IPB_APL" , descripcionLarga:  "Sistema de prefacturaci\u00F3n de servicios IP pre y pospagos (WiFi, Dial Up, ADSL y" , grupoAdministrador:  "AP_CH_IPB_APL", grupoReferenteSWF: GRUPO_INTERLOCUTOR_USUARIO, gerencia:"DTI_DSFC_IPB", grupoTuID: "").save(failOnError: true)
		new Sistema(descripcion: "IRIS_APL" , descripcionLarga:  "Sistema Mediador con uno de los Inventarios de la Red: Network Inventory." , grupoAdministrador:  "AP_CH_IRIS_APL", grupoReferenteSWF: GRUPO_INTERLOCUTOR_USUARIO, gerencia:"DSI_DST_DSIN", grupoTuID: "").save(failOnError: true)
		new Sistema(descripcion: "IUM_APL" , descripcionLarga:  "Smart Internet Usage.  Es un sistema de mediaci\u00F3n IP que permite la interpretaci" , grupoAdministrador:  "AP_CH_IUM_APL", grupoReferenteSWF: GRUPO_INTERLOCUTOR_USUARIO, gerencia:"DTI_DSFC_MEDIACION-FRAUDE", grupoTuID: "").save(failOnError: true)
		new Sistema(descripcion: "IVR_SIC2_SEGAT_APL" , descripcionLarga:  "Es el IVR de cierre de reclamos t\u00E9cnicos y solicitudes de provisi\u00F3n que da servi" , grupoAdministrador:  "AP_CH_IVR_SIC2_SEGAT_APL", grupoReferenteSWF: GRUPO_INTERLOCUTOR_USUARIO, gerencia:"DTI_DSIN", grupoTuID: "").save(failOnError: true)
		new Sistema(descripcion: "IVR_SIC_SUR_APL" , descripcionLarga:  "Es el ivr de cierre de reclamos t\u00E9cnicos para SUR y Girafe" , grupoAdministrador:  "AP_CH_IVR_SIC_SUR_APL", grupoReferenteSWF: GRUPO_INTERLOCUTOR_USUARIO, gerencia:"DTI_DSIN", grupoTuID: "").save(failOnError: true)
		new Sistema(descripcion: "JANO_APL" , descripcionLarga:  "ELEMENTO DE CONFIGURACI\u00F3N PARA TEMAS ASOCIADOS CON EL APLICATIVO EN GENERAL" , grupoAdministrador:  "AP_CH_JANO_APL", grupoReferenteSWF: GRUPO_INTERLOCUTOR_USUARIO, gerencia:"DTI_DSIN_PLATDW", grupoTuID: "").save(failOnError: true)
		new Sistema(descripcion: "JANO_PROCESO_ETL" , descripcionLarga:  "ELEMENTO DE CONFIGURACI\u00F3N PARA TEMAS ASOCIADOS CON LOS PROCESOS DE ETL." , grupoAdministrador:  "AP_CH_JANO_PROCESO_ETL", grupoReferenteSWF: GRUPO_INTERLOCUTOR_USUARIO, gerencia:"DTI_DSIN_PLATDW", grupoTuID: "").save(failOnError: true)
		new Sistema(descripcion: "KINGDOM_APL" , descripcionLarga:  "Aplicativo para la carga y validaci\u00F3n de domicilios." , grupoAdministrador:  "AP_CH_KINGDOM_APL", grupoReferenteSWF: GRUPO_INTERLOCUTOR_USUARIO, gerencia:"DTI_DSPI_PROV-SISTEMAS-BASICOS", grupoTuID: "").save(failOnError: true)
		new Sistema(descripcion: "LEGACY_BUSQ_ABONADO_APL" , descripcionLarga:  "Aplicativo que se utilizan como base de consulta ante pedidos legales / judicial" , grupoAdministrador:  "AP_CH_LEGACY_BUSQ_ABONADO_APL", grupoReferenteSWF: GRUPO_INTERLOCUTOR_USUARIO, gerencia:"DTI_DSFC_FACT-MERCADO-MASIVO", grupoTuID: "").save(failOnError: true)
		new Sistema(descripcion: "LEGACY_COBRANZAS_APL" , descripcionLarga:  "Aplicativos que se utiliza como base de consulta ante pedidos legales / judicial" , grupoAdministrador:  "AP_CH_LEGACY_COBRANZAS_APL", grupoReferenteSWF: GRUPO_INTERLOCUTOR_USUARIO, gerencia:"DTI_DSFC_COBR-MASIVOS", grupoTuID: "").save(failOnError: true)
		new Sistema(descripcion: "LEGACY_FACTURACION_APL" , descripcionLarga:  "Aplicativos que se utilizan como base de consulta ante pedidos legales / judicia" , grupoAdministrador:  "AP_CH_LEGACY_FACTURACION_APL", grupoReferenteSWF: GRUPO_INTERLOCUTOR_USUARIO, gerencia:"DTI_DSFC_FACT-MERCADO-MASIVO", grupoTuID: "").save(failOnError: true)
		new Sistema(descripcion: "LITIGIOS_APL" , descripcionLarga:  "Sistema que administra y gestiona los litigios de Telecom: Juicios a nivel grupo" , grupoAdministrador:  "AP_CH_LITIGIOS_APL", grupoReferenteSWF: GRUPO_INTERLOCUTOR_USUARIO, gerencia:"DSI_DSC_NO-SIGNO", grupoTuID: "").save(failOnError: true)
		new Sistema(descripcion: "LOCALMOVIL_APL" , descripcionLarga:  "APLICACI\u00F3N QUE PERMITE OBTENER LA LOCALIZACI\u00F3N DE UNA L\u00EDNEA M\u00F3VIL." , grupoAdministrador:  "AP_LOCALMOVIL_APL", grupoReferenteSWF: GRUPO_INTERLOCUTOR_USUARIO, gerencia:"DSI_DST_DSPFM", grupoTuID: "").save(failOnError: true)
		new Sistema(descripcion: "LOYALISO_APL" , descripcionLarga:  "PORTAL QUE GESTIONA LAS NORMAS CORPORATIVAS DE LA COMPAÑ\u00EDA" , grupoAdministrador:  "AP_CH_LOYALISO_APL", grupoReferenteSWF: GRUPO_INTERLOCUTOR_USUARIO, gerencia:"DSI_DSC_NO-SIGNO", grupoTuID: "").save(failOnError: true)
		new Sistema(descripcion: "MAC_APL" , descripcionLarga:  "M\u00F3dulo de Administraci\u00F3n de Clientes. Sistema que gestiona en forma centralizada" , grupoAdministrador:  "AP_CH_MAC_APL", grupoReferenteSWF: GRUPO_INTERLOCUTOR_USUARIO, gerencia:"DTI_DSPI_PROV-SISTEMAS-BASICOS", grupoTuID: "").save(failOnError: true)
		new Sistema(descripcion: "MAIL_TRACK_APL" , descripcionLarga:  "Conjunto de p\u00E1ginas Web que dan servicios a clientes y a usuarios registrados pa" , grupoAdministrador:  "AP_CH_MAIL_TRACK_APL", grupoReferenteSWF: GRUPO_INTERLOCUTOR_USUARIO, gerencia:"DTI_SIM_DDP", grupoTuID: "").save(failOnError: true)
		new Sistema(descripcion: "MEC_APL" , descripcionLarga:  "M\u00F3dulo encargado de emitir, reimprimir y anular remitos que hacen a la provisi\u00F3n" , grupoAdministrador:  "AP_CH_MEC_APL", grupoReferenteSWF: GRUPO_INTERLOCUTOR_USUARIO, gerencia:"DTI_DSPI_PROV-SERV-INTERNET", grupoTuID: "").save(failOnError: true)
		new Sistema(descripcion: "MEGAVISTAS_APL" , descripcionLarga:  "WEB DE CONSULTA SOBRE INFORMACION DEL DOMINIO TX EN NETWORK INVENTORY" , grupoAdministrador:  "AP_CH_NETWORK_INVENTORY_APL", grupoReferenteSWF: GRUPO_INTERLOCUTOR_USUARIO, gerencia:"", grupoTuID: "").save(failOnError: true)
		new Sistema(descripcion: "MERCURY_APL" , descripcionLarga:  "Herramienta de Gesti\u00F3n de Pruebas. Testing de Aplicaciones y Herramientas de IT" , grupoAdministrador:  "AP_CH_MERCURY_APL", grupoReferenteSWF: GRUPO_INTERLOCUTOR_USUARIO, gerencia:"DSI_DSC_G-IT", grupoTuID: "").save(failOnError: true)
		new Sistema(descripcion: "MES_APL" , descripcionLarga:  "Gesti\u00F3n de Auditor\u00EDa." , grupoAdministrador:  "AP_CH_MES_APL", grupoReferenteSWF: GRUPO_INTERLOCUTOR_USUARIO, gerencia:"DSI_DSC_NO-SIGNO", grupoTuID: "").save(failOnError: true)
		new Sistema(descripcion: "MI_CUENTA_BACK_END" , descripcionLarga:  "Sistema que permite el acceso online del cliente a su informaci\u00F3n de Facturaci\u00F3n" , grupoAdministrador:  "AP_CH_MI_CUENTA_BACK_END", grupoReferenteSWF: GRUPO_INTERLOCUTOR_USUARIO, gerencia:"DTI_DSFC_COBR-MASIVOS", grupoTuID: "").save(failOnError: true)
		new Sistema(descripcion: "MI_CUENTA_FRONT_END" , descripcionLarga:  "Sistema que permite el acceso online del cliente a su informaci\u00F3n de Facturaci\u00F3n" , grupoAdministrador:  "AP_CH_MI_CUENTA_FRONT_END", grupoReferenteSWF: GRUPO_INTERLOCUTOR_USUARIO, gerencia:"DTI_DSFC_COBR-MASIVOS", grupoTuID: "").save(failOnError: true)
		new Sistema(descripcion: "NBA_APL" , descripcionLarga:  "Nueva B\u00FAsqueda de abonado." , grupoAdministrador:  "AP_CH_NBA_APL", grupoReferenteSWF: GRUPO_INTERLOCUTOR_USUARIO, gerencia:"DTI_DSFC_FACT-MERCADO-MASIVO", grupoTuID: "").save(failOnError: true)
		new Sistema(descripcion: "NETWORK_INVENTORY_APL" , descripcionLarga:  "Base referencial del inventario de los recursos l\u00F3gicos y f\u00EDsicos de las redes d" , grupoAdministrador:  "AP_CH_NETWORK_INVENTORY_APL", grupoReferenteSWF: GRUPO_INTERLOCUTOR_USUARIO, gerencia:"DSI_DST_DSIN", grupoTuID: "").save(failOnError: true)
		new Sistema(descripcion: "NRED_APL" , descripcionLarga:  "SISTEMA DE INVENTARIO DE NUMERACI\u00F3N DE RED" , grupoAdministrador:  "AP_CH_NRED_APL", grupoReferenteSWF: GRUPO_INTERLOCUTOR_USUARIO, gerencia:"", grupoTuID: "").save(failOnError: true)
		new Sistema(descripcion: "ODS_APL" , descripcionLarga:  "Operational Data Storage. Herramienta flexible para la generaci\u00F3n de reportes, i" , grupoAdministrador:  "AP_CH_ODS_APL", grupoReferenteSWF: GRUPO_INTERLOCUTOR_USUARIO, gerencia:"DTI_DSIN_PLATDW", grupoTuID: "").save(failOnError: true)
		new Sistema(descripcion: "ODS_BD_ODSF" , descripcionLarga:  "Operational Data Storage. Herramienta flexible para la generaci\u00F3n de reportes, i" , grupoAdministrador:  "AP_CH_ODS_BD_ODSF", grupoReferenteSWF: GRUPO_INTERLOCUTOR_USUARIO, gerencia:"DTI_DSIN_PLATDW", grupoTuID: "").save(failOnError: true)
		new Sistema(descripcion: "ODS_CARGA_Y_PUBLICACION" , descripcionLarga:  "Operational Data Storage.Herramienta flexible para la generaci\u00F3n de reportes, in" , grupoAdministrador:  "AP_CH_ODS_CARGA_Y_PUBLICACION", grupoReferenteSWF: GRUPO_INTERLOCUTOR_USUARIO, gerencia:"DTI_DSIN_PLATDW", grupoTuID: "").save(failOnError: true)
		new Sistema(descripcion: "ODS_CLIENTE_POWER_USER" , descripcionLarga:  "Operational Data Storage.Herramienta flexible para la generaci\u00F3n de reportes, in" , grupoAdministrador:  "AP_CH_ODS_CLIENTE_POWER_USER", grupoReferenteSWF: GRUPO_INTERLOCUTOR_USUARIO, gerencia:"DTI_DSIN_PLATDW", grupoTuID: "").save(failOnError: true)
		new Sistema(descripcion: "ODS_CLIENTE_WEB" , descripcionLarga:  "Operational Data Storage.Herramienta flexible para la generaci\u00F3n de reportes, in" , grupoAdministrador:  "AP_CH_ODS_CLIENTE_WEB", grupoReferenteSWF: GRUPO_INTERLOCUTOR_USUARIO, gerencia:"DTI_DSIN_PLATDW", grupoTuID: "").save(failOnError: true)
		new Sistema(descripcion: "ODS_PROCESO_ESPECIAL" , descripcionLarga:  "Operational Data Storage.Herramienta flexible para la generaci\u00F3n de reportes, in" , grupoAdministrador:  "AP_CH_ODS_PROCESO_ESPECIAL", grupoReferenteSWF: GRUPO_INTERLOCUTOR_USUARIO, gerencia:"DTI_DSIN_PLATDW", grupoTuID: "").save(failOnError: true)
		new Sistema(descripcion: "ORCA_APL" , descripcionLarga:  "Es la base descriptiva de todo el parque de conmutaci\u00F3n de Telecom Argentina. Ge" , grupoAdministrador:  "AP_CH_ORCA_APL", grupoReferenteSWF: GRUPO_INTERLOCUTOR_USUARIO, gerencia:"DSI_DST_DSIN", grupoTuID: "").save(failOnError: true)
		new Sistema(descripcion: "PASEVIP_APL" , descripcionLarga:  "Sistema que permite la automatizaci\u00F3n de los procesos complejos para proveer un" , grupoAdministrador:  "AP_CH_PASEVIP_APL", grupoReferenteSWF: GRUPO_INTERLOCUTOR_USUARIO, gerencia:"GSI", grupoTuID: "").save(failOnError: true)
		new Sistema(descripcion: "PCS_APL" , descripcionLarga:  "Gesti\u00F3n de requerimientos evolutivos." , grupoAdministrador:  "AP_CH_PCS_APL", grupoReferenteSWF: "RSWF_PCS", gerencia:"DSI_DSC_PLAN", grupoTuID: "").save(failOnError: true)
		new Sistema(descripcion: "PERSA_APL" , descripcionLarga:  "Performance Statistics Analyzer. Su funci\u00F3n es consolidar y correlacionar y alma" , grupoAdministrador:  "AP_CH_PERSA_APL", grupoReferenteSWF: GRUPO_INTERLOCUTOR_USUARIO, gerencia:"DSI_DST_DSFP", grupoTuID: "").save(failOnError: true)
		new Sistema(descripcion: "PERSEUS_APL" , descripcionLarga:  "PERSEUS" , grupoAdministrador:  "AP_CH_FACTESP_APL", grupoReferenteSWF: GRUPO_INTERLOCUTOR_USUARIO, gerencia:"DTI_DSFC_FACT-MERCADO-MASIVO", grupoTuID: "").save(failOnError: true)
		new Sistema(descripcion: "PIRYP_APL" , descripcionLarga:  "SE UTILIZA PARA LA TRANSFERENCIA DE ARCHIVOS DEL PROYECTO RECAUDACIONES INTERACTIVAS Y/O PAGO A PROVEEDORES ENTRE TELECOM Y EL BANCO RIO (RIO SANTANDER)" , grupoAdministrador:  "AP_CH_USER_PIRYP_APL", grupoReferenteSWF: GRUPO_INTERLOCUTOR_USUARIO, gerencia:"", grupoTuID: "").save(failOnError: true)
		new Sistema(descripcion: "PLATAFORMA_DE_PRECALIFICACION_APL" , descripcionLarga:  "EL SISTEMA REALIZA UNA MEDICION MENSUAL DE PARAMETROS DE CONFIGURACION Y PERFORMANCE DE TODO EL PARQUE DE PORTS ADSL" , grupoAdministrador:  "AP_CH_PLATAFORMA_DE_PRECALIFICACION_APL", grupoReferenteSWF: GRUPO_INTERLOCUTOR_USUARIO, gerencia:"", grupoTuID: "").save(failOnError: true)
		new Sistema(descripcion: "PLATAFORMA_DE_TESTING_APL" , descripcionLarga:  "Realiza pruebas de linea de tecnologia tradicional, NGN y ADSL. Es la interfaz e" , grupoAdministrador:  "AP_CH_PLATAFORMA_DE_TESTING_APL", grupoReferenteSWF: GRUPO_INTERLOCUTOR_USUARIO, gerencia:"DSI_DST_DSFP", grupoTuID: "").save(failOnError: true)
		new Sistema(descripcion: "PORTAL_AUTOGESTION_BACK_END" , descripcionLarga:  "Portal Web que permite la autenticaci\u00F3n de un cliente de Telecom, previamente re" , grupoAdministrador:  "AP_CH_PORTAL_AUTOGESTION_BACK_END", grupoReferenteSWF: GRUPO_INTERLOCUTOR_USUARIO, gerencia:"DTI_DSCRM_PORTAL-AUTOGESTION", grupoTuID: "").save(failOnError: true)
		new Sistema(descripcion: "PORTAL_AUTOGESTION_FRONT_END" , descripcionLarga:  "Portal Web que permite la autenticaci\u00F3n de un cliente de Telecom, previamente re" , grupoAdministrador:  "AP_CH_PORTAL_AUTOGESTION_FRONT_END", grupoReferenteSWF: GRUPO_INTERLOCUTOR_USUARIO, gerencia:"DTI_DSCRM_PORTAL-AUTOGESTION", grupoTuID: "").save(failOnError: true)
		new Sistema(descripcion: "PORTAL_BI_UTF_APL" , descripcionLarga:  "LOGIN PARA LAS APLICACIONES DATAMART" , grupoAdministrador:  "AP_CH_PORTAL_BI_UTF_APL", grupoReferenteSWF: GRUPO_INTERLOCUTOR_USUARIO, gerencia:"", grupoTuID: "").save(failOnError: true)
		new Sistema(descripcion: "PORTAL_CORPORATIVO_APL" , descripcionLarga:  "Herramienta que soporta la certificaci\u00F3n de ISO9000" , grupoAdministrador:  "AP_CH_PORTAL_CORPORATIVO_APL", grupoReferenteSWF: GRUPO_INTERLOCUTOR_USUARIO, gerencia:"DTI_DSIN_EVA", grupoTuID: "").save(failOnError: true)
		new Sistema(descripcion: "PROCESO_VENTA_ADSL_APL" , descripcionLarga:  "Atiende aplicaciones del proceso de Venta_ADSL." , grupoAdministrador:  "", grupoReferenteSWF: GRUPO_INTERLOCUTOR_USUARIO, gerencia:"DTI_GOT", grupoTuID: "").save(failOnError: true)
		new Sistema(descripcion: "PROD_ARNET_ATA" , descripcionLarga:  "Arnet te ayuda.  Aplicaci\u00F3n de monitoreo en el equipo cliente de Arnet" , grupoAdministrador:  "AP_CH_PROD_ARNET_ATA", grupoReferenteSWF: GRUPO_INTERLOCUTOR_USUARIO, gerencia:"DTI_SIM_VA", grupoTuID: "").save(failOnError: true)
		new Sistema(descripcion: "PROD_ARNET_CCIP" , descripcionLarga:  "El sistema de Capa de Control IP es la evoluci\u00F3n y reemplazo de los tres servici" , grupoAdministrador:  "AP_CH_PROD_ARNET_CCIP", grupoReferenteSWF: GRUPO_INTERLOCUTOR_USUARIO, gerencia:"DTI_SIM_DSA", grupoTuID: "").save(failOnError: true)
		new Sistema(descripcion: "PROD_ARNET_CHAT" , descripcionLarga:  "Herramienta de chat" , grupoAdministrador:  "AP_CH_PROD_ARNET_CHAT", grupoReferenteSWF: GRUPO_INTERLOCUTOR_USUARIO, gerencia:"DTI_SIM_VA", grupoTuID: "").save(failOnError: true)
		new Sistema(descripcion: "PROD_ARNET_COARNET_APL" , descripcionLarga:  "PLATAFORMA COPERRNICO. PANEL DE CONTROL DESDE DONDE ES POSIBLE MANEJAR EL HOSTING DE TELECOM. SERVICIO ORIENTADO A EMPRESAS." , grupoAdministrador:  "AP_CH_PROD_ARNET_COARNET_APL", grupoReferenteSWF: GRUPO_INTERLOCUTOR_USUARIO, gerencia:"DTI_SIM_VA", grupoTuID: "").save(failOnError: true)
		new Sistema(descripcion: "PROD_ARNET_DHCP" , descripcionLarga:  "PROD_ARNET_DHCP" , grupoAdministrador:  "AP_CH_PROD_ARNET_DHCP", grupoReferenteSWF: GRUPO_INTERLOCUTOR_USUARIO, gerencia:"DTI_SIM_DSA", grupoTuID: "").save(failOnError: true)
		new Sistema(descripcion: "PROD_ARNET_DNS" , descripcionLarga:  "Parte de la plataforma de la servicio de arnet. Servicio de Dominio" , grupoAdministrador:  "AP_CH_PROD_ARNET_DNS", grupoReferenteSWF: GRUPO_INTERLOCUTOR_USUARIO, gerencia:"DTI_SIM_DSA", grupoTuID: "").save(failOnError: true)
		new Sistema(descripcion: "PROD_ARNET_GATEWAY_DE_PROVISION" , descripcionLarga:  "Sistema de provisi\u00F3n de servicios de internet (Hosting, Web Disk, Smart Iserver)" , grupoAdministrador:  "AP_CH_PROD_ARNET_GATEWAY_DE_PROVISION", grupoReferenteSWF: GRUPO_INTERLOCUTOR_USUARIO, gerencia:"DTI_SIM_DSA", grupoTuID: "").save(failOnError: true)
		new Sistema(descripcion: "PROD_ARNET_HOSTING_EMPRESAS" , descripcionLarga:  "Servicio de hosting para Empresas" , grupoAdministrador:  "AP_CH_PROD_ARNET_HOSTING_EMPRESAS", grupoReferenteSWF: GRUPO_INTERLOCUTOR_USUARIO, gerencia:"DTI_SIM_VA", grupoTuID: "").save(failOnError: true)
		new Sistema(descripcion: "PROD_ARNET_HOSTING_PERSONAL" , descripcionLarga:  "Servicio de hosting de Telecom Personal" , grupoAdministrador:  "AP_CH_PROD_ARNET_HOSTING_PERSONAL", grupoReferenteSWF: GRUPO_INTERLOCUTOR_USUARIO, gerencia:"DTI_SIM_VA", grupoTuID: "").save(failOnError: true)
		new Sistema(descripcion: "PROD_ARNET_IPASS" , descripcionLarga:  "PROD_ARNET_IPASS" , grupoAdministrador:  "AP_CH_PROD_ARNET_IPASS", grupoReferenteSWF: GRUPO_INTERLOCUTOR_USUARIO, gerencia:"DTI_SIM_DSA", grupoTuID: "").save(failOnError: true)
		new Sistema(descripcion: "PROD_ARNET_MAIL_MASIVO" , descripcionLarga:  "Servicio de mail masivo" , grupoAdministrador:  "AP_CH_PROD_ARNET_MAIL_MASIVO", grupoReferenteSWF: GRUPO_INTERLOCUTOR_USUARIO, gerencia:"DTI_SIM_VA", grupoTuID: "").save(failOnError: true)
		new Sistema(descripcion: "PROD_ARNET_PORTALES_ARNET" , descripcionLarga:  "Conjunto de p\u00E1ginas Web que dan servicios a clientes y a usuarios registrados." , grupoAdministrador:  "AP_CH_PROD_ARNET_PORTALES_ARNET", grupoReferenteSWF: GRUPO_INTERLOCUTOR_USUARIO, gerencia:"DTI_SIM_DDP", grupoTuID: "").save(failOnError: true)
		new Sistema(descripcion: "PROD_ARNET_PORTALES_BACK_OFFICE" , descripcionLarga:  "Conjunto de p\u00E1ginas Web que dan servicios a clientes y a usuarios registrados." , grupoAdministrador:  "AP_CH_PROD_ARNET_PORTALES_BACK_OFFICE", grupoReferenteSWF: GRUPO_INTERLOCUTOR_USUARIO, gerencia:"DTI_SIM_DDP", grupoTuID: "").save(failOnError: true)
		new Sistema(descripcion: "PROD_ARNET_PORTALES_TELECOM" , descripcionLarga:  "Conjunto de p\u00E1ginas Web que dan servicios a clientes y a usuarios registrados." , grupoAdministrador:  "AP_CH_PROD_ARNET_PORTALES_TELECOM", grupoReferenteSWF: GRUPO_INTERLOCUTOR_USUARIO, gerencia:"DTI_SIM_DDP", grupoTuID: "").save(failOnError: true)
		new Sistema(descripcion: "PROD_ARNET_SERVICIO_DE_DIRECTORIO" , descripcionLarga:  "Active directory.  Sistema para la gesti\u00F3n para la autenticaci\u00F3n de usuarios bas" , grupoAdministrador:  "AP_CH_PROD_ARNET_SERVICIO_DE_DIRECTORIO", grupoReferenteSWF: GRUPO_INTERLOCUTOR_USUARIO, gerencia:"DTI_SIM_VA", grupoTuID: "").save(failOnError: true)
		new Sistema(descripcion: "PROD_ARNET_SMART_ISERVER" , descripcionLarga:  "Plataforma para soportar el servicio de SmartIServer. Panel de contro de donde p" , grupoAdministrador:  "AP_CH_PROD_ARNET_SMART_ISERVER", grupoReferenteSWF: GRUPO_INTERLOCUTOR_USUARIO, gerencia:"DTI_SIM_VA", grupoTuID: "").save(failOnError: true)
		new Sistema(descripcion: "PROD_ARNET_STREAMING_FLASH_MEDIA" , descripcionLarga:  "Plataforma de Flash Media Server para de videos desde el portal Arnet" , grupoAdministrador:  "AP_CH_PROD_ARNET_STREAMING_FLASH_MEDIA", grupoReferenteSWF: GRUPO_INTERLOCUTOR_USUARIO, gerencia:"DTI_SIM_VA", grupoTuID: "").save(failOnError: true)
		new Sistema(descripcion: "PROD_ARNET_STREAMING_WINDOWS_MEDIA" , descripcionLarga:  "Plataforma de Windows Media Server para de videos desde el portal Arnet" , grupoAdministrador:  "AP_CH_PROD_ARNET_STREAMING_WINDOWS_MEDIA", grupoReferenteSWF: GRUPO_INTERLOCUTOR_USUARIO, gerencia:"DTI_SIM_VA", grupoTuID: "").save(failOnError: true)
		new Sistema(descripcion: "PYO_APL" , descripcionLarga:  "Gestiona  los consumos correspondientes a llamadas de Larga distancia nacional e" , grupoAdministrador:  "AP_CH_PYO_APL", grupoReferenteSWF: GRUPO_INTERLOCUTOR_USUARIO, gerencia:"DTI_DSFC_COBR-MASIVOS", grupoTuID: "").save(failOnError: true)
		new Sistema(descripcion: "QV_ASSE_APL" , descripcionLarga:  "REPORTES ESTADISTICOS Y OPERATIVOS DE ASSET MANAGER" , grupoAdministrador:  "AP_QV_ASSE_APL", grupoReferenteSWF: GRUPO_INTERLOCUTOR_USUARIO, gerencia:"DSI_GSC", grupoTuID: "").save(failOnError: true)
		new Sistema(descripcion: "QV_COST_APL" , descripcionLarga:  "GRUPO CONTROLLING" , grupoAdministrador:  "AP_CH_QV_COST_APL", grupoReferenteSWF: GRUPO_INTERLOCUTOR_USUARIO, gerencia:"GSC", grupoTuID: "").save(failOnError: true)
		new Sistema(descripcion: "QV_MASIVO_APL" , descripcionLarga:  "AUTOMATIZACION DE AMBIENTES" , grupoAdministrador:  "QV_MASIVO_APL", grupoReferenteSWF: GRUPO_INTERLOCUTOR_USUARIO, gerencia:"DSI_DST_DSRT", grupoTuID: "").save(failOnError: true)
		new Sistema(descripcion: "QV_NOMASIVO_APL" , descripcionLarga:  "AUTOMATIZACION DE AMBIENTES" , grupoAdministrador:  "QV_NOMASIVO_APL", grupoReferenteSWF: GRUPO_INTERLOCUTOR_USUARIO, gerencia:"DSI_DST_DSRT", grupoTuID: "").save(failOnError: true)
		new Sistema(descripcion: "QV_PCS_APL" , descripcionLarga:  "AUTOMATIZACION DE AMBIENTES" , grupoAdministrador:  "QV_PCS_APL", grupoReferenteSWF: GRUPO_INTERLOCUTOR_USUARIO, gerencia:"", grupoTuID: "").save(failOnError: true)
		new Sistema(descripcion: "QV_REMEDY_APL" , descripcionLarga:  "AUTOMATIZACION DE AMBIENTES" , grupoAdministrador:  "AP_CH_QV_REMEDY_APL", grupoReferenteSWF: GRUPO_INTERLOCUTOR_USUARIO, gerencia:"DSI_DST_DSRT", grupoTuID: "").save(failOnError: true)
		new Sistema(descripcion: "QV_RRHH_APL" , descripcionLarga:  "HERRAMIENTA DE BI PARA EXTRACCI\u00F3N DE DATOS DE SAP-RRHH" , grupoAdministrador:  "AP_CH_QV_RRHH_APL", grupoReferenteSWF: GRUPO_INTERLOCUTOR_USUARIO, gerencia:"DSI_DSC_QV", grupoTuID: "").save(failOnError: true)
		new Sistema(descripcion: "QV_SIAG_APL" , descripcionLarga:  "HERRAMIENTA DE BI PARA EXTRACCI\u00F3N DE DATOS DE SIAG" , grupoAdministrador:  "AP_CH_QV_SIAG_APL", grupoReferenteSWF: GRUPO_INTERLOCUTOR_USUARIO, gerencia:"DSI_DSC_QV", grupoTuID: "").save(failOnError: true)
		new Sistema(descripcion: "QV_SIMPLIT_APL" , descripcionLarga:  "AUTOMATIZACION DE AMBIENTES" , grupoAdministrador:  "QV_SIMPLIT_APL", grupoReferenteSWF: GRUPO_INTERLOCUTOR_USUARIO, gerencia:"DSI_DST_DSRT", grupoTuID: "").save(failOnError: true)
		new Sistema(descripcion: "QV_SSGG_APL" , descripcionLarga:  "HERRAMIENTA DE BI PARA EXTRACCI\u00F3N DE DATOS DE SERVICIOS GENERALES" , grupoAdministrador:  "AP_CH_QV_SSGG_APL", grupoReferenteSWF: GRUPO_INTERLOCUTOR_USUARIO, gerencia:"DSI_DSC_QV", grupoTuID: "").save(failOnError: true)
		new Sistema(descripcion: "REGISTRY_APL" , descripcionLarga:  "Gesti\u00F3n de inventarios de servicios (SOA)." , grupoAdministrador:  "AP_CH_REGISTRY_APL", grupoReferenteSWF: GRUPO_INTERLOCUTOR_USUARIO, gerencia:"DSI_PYAA", grupoTuID: "").save(failOnError: true)
		new Sistema(descripcion: "REI_APL" , descripcionLarga:  "INVENTARIO REAL ESTATE" , grupoAdministrador:  "AP_CH_REI_APL", grupoReferenteSWF: GRUPO_INTERLOCUTOR_USUARIO, gerencia:"GST", grupoTuID: "").save(failOnError: true)
		new Sistema(descripcion: "REMEDYRED_APL" , descripcionLarga:  "EL TROUBLE TICKET DE RED TIENE POR FINALIDAD CONCENTRAR LA ATENCI\u00F3N DE RECLAMOS DE INSIDENTES PRODUCTIVOS EN EQUIPOS DE LA RED" , grupoAdministrador:  "AP_CH_REMEDYRED_APL", grupoReferenteSWF: GRUPO_INTERLOCUTOR_USUARIO, gerencia:"DSI_DST", grupoTuID: "").save(failOnError: true)
		new Sistema(descripcion: "REPORTE_ASEGURAMIENTO_APL" , descripcionLarga:  "Sistema de integrado de reporting para la informaci\u00F3n generada por el Sistema SU" , grupoAdministrador:  "AP_CH_REPORTE_ASEGURAMIENTO_APL", grupoReferenteSWF: "RSWF_BIAS", gerencia:"DSI_DST_DSRT", grupoTuID: "").save(failOnError: true)
		new Sistema(descripcion: "RYR_APL" , descripcionLarga:  "Aplicaci\u00F3n cuya finalidad es soportar la  registraci\u00F3n, an\u00E1lisis y seguimiento d" , grupoAdministrador:  "AP_CH_RYR", grupoReferenteSWF: GRUPO_INTERLOCUTOR_USUARIO, gerencia:"DTI_DSPI", grupoTuID: "").save(failOnError: true)
		new Sistema(descripcion: "SAS_APL" , descripcionLarga:  "Sistema que obtiene la afectacion de servicios mediante la deteccion de fallas." , grupoAdministrador:  "AP_CH_SAS_APL", grupoReferenteSWF: GRUPO_INTERLOCUTOR_USUARIO, gerencia:"DSI_DST_DSFP", grupoTuID: "").save(failOnError: true)
		new Sistema(descripcion: "SCARD_APL" , descripcionLarga:  "Sistema Web que permite digitalizar, almacenar y visualizar en su formato origin" , grupoAdministrador:  "AP_CH_SCARD_APL", grupoReferenteSWF: GRUPO_INTERLOCUTOR_USUARIO, gerencia:"DTI_DSFC_COBR-MASIVOS", grupoTuID: "").save(failOnError: true)
		new Sistema(descripcion: "SEGAT_II_APL" , descripcionLarga:  "Sistema de Workforce Management" , grupoAdministrador:  "AP_CH_SEGAT_II_APL", grupoReferenteSWF: GRUPO_INTERLOCUTOR_USUARIO, gerencia:"DSI_DST_DSRT", grupoTuID: "").save(failOnError: true)
		new Sistema(descripcion: "SEI_APL" , descripcionLarga:  "SEI ES EL M\u00F3DULO SERVIDOR INSTALADO EN LAS OFICINAS COMERCILES LA CU\u00E1L SE ENCARGA DE IMPRIMIR LOS DOCUMENTOS RELACIONADOS A UNA VENTA." , grupoAdministrador:  "AP_CH_SEI_APL", grupoReferenteSWF: GRUPO_INTERLOCUTOR_USUARIO, gerencia:"", grupoTuID: "").save(failOnError: true)
		new Sistema(descripcion: "SERVER3_APL" , descripcionLarga:  "Sistema que permite el intercambio de informaci\u00F3n entre Telecom y los distintos" , grupoAdministrador:  "AP_CH_SERVER3_APL", grupoReferenteSWF: GRUPO_INTERLOCUTOR_USUARIO, gerencia:"DTI_DSFC_MEDIACION-FRAUDE", grupoTuID: "").save(failOnError: true)
		new Sistema(descripcion: "SGCT_APL" , descripcionLarga:  "Sistema de Gesti\u00F3n Comercial de Tarjetas TATETI, Corporate y Cobranding." , grupoAdministrador:  "AP_CH_SGCT_APL", grupoReferenteSWF: GRUPO_INTERLOCUTOR_USUARIO, gerencia:"DTI_DSPI", grupoTuID: "").save(failOnError: true)
		new Sistema(descripcion: "SGDATOS_APL" , descripcionLarga:  "Sistema de supervisi\u00F3n que permite concentrar la gesti\u00F3n de alarmas de la red de" , grupoAdministrador:  "AP_CH_SGDATOS_APL", grupoReferenteSWF: GRUPO_INTERLOCUTOR_USUARIO, gerencia:"DSI_DST_DSFP", grupoTuID: "").save(failOnError: true)
		new Sistema(descripcion: "SGDATOS_TP_APL" , descripcionLarga:  "Sistema de supervisi\u00F3n que permite concentrar la gesti\u00F3n de alarmas de la red de" , grupoAdministrador:  "AP_CH_SGDATOS_TP_APL", grupoReferenteSWF: GRUPO_INTERLOCUTOR_USUARIO, gerencia:"DSI_DST_DSFP", grupoTuID: "").save(failOnError: true)
		new Sistema(descripcion: "SIAF_APL" , descripcionLarga:  "Sistema Integrado de Administraci\u00F3n Financiera, cuyo alcance es todo el Grupo Te" , grupoAdministrador:  "AP_CH_SIAF_APL", grupoReferenteSWF: GRUPO_INTERLOCUTOR_USUARIO, gerencia:"DSI_DSC_NO-SIGNO", grupoTuID: "").save(failOnError: true)
		new Sistema(descripcion: "SIAG_APL" , descripcionLarga:  "Sistema de ERP que integra funcionalidades de  Finanzas y Controlling, Proyectos" , grupoAdministrador:  "AP_CH_SIAG_APL", grupoReferenteSWF: GRUPO_INTERLOCUTOR_USUARIO, gerencia:"DSI_DSC_NO-SIGNO", grupoTuID: "").save(failOnError: true)
		new Sistema(descripcion: "SIDERAL_APL" , descripcionLarga:  "Sistema de supervisi\u00F3n  de alarmas que permite concentrar la gesti\u00F3n de alarmas" , grupoAdministrador:  "AP_CH_SIDERAL_APL", grupoReferenteSWF: GRUPO_INTERLOCUTOR_USUARIO, gerencia:"DSI_DST_DSFP", grupoTuID: "").save(failOnError: true)
		new Sistema(descripcion: "SIDI_APL" , descripcionLarga:  "Sistema que administra el cat\u00E1logo de productos y servicios de Datos . Permite l" , grupoAdministrador:  "AP_CH_SIDI_APL", grupoReferenteSWF: GRUPO_INTERLOCUTOR_USUARIO, gerencia:"DTI_DSPI_PROV-MERCADO-NO-MASIVO", grupoTuID: "").save(failOnError: true)
		new Sistema(descripcion: "SIDI_REPORTES" , descripcionLarga:  "Sistema que administra el cat\u00E1logo de productos y servicios de Datos . Permite l" , grupoAdministrador:  "AP_CH_SIDI_REPORTES", grupoReferenteSWF: GRUPO_INTERLOCUTOR_USUARIO, gerencia:"DTI_DSPI_PROV-MERCADO-NO-MASIVO", grupoTuID: "").save(failOnError: true)
		new Sistema(descripcion: "SIEBEL_CUSTOMER_CENTRIC_APL" , descripcionLarga:  "Gestor \u00FAnico de clientes." , grupoAdministrador:  "AP_CH_SIEBEL_CUSTOMER_CENTRIC_APL", grupoReferenteSWF: GRUPO_INTERLOCUTOR_USUARIO, gerencia:"DTI_DSCRM_SIEBEL", grupoTuID: "").save(failOnError: true)
		new Sistema(descripcion: "SIGMA_APL" , descripcionLarga:  "Tablero de control de Directores." , grupoAdministrador:  "AP_CH_SIGMA_APL", grupoReferenteSWF: GRUPO_INTERLOCUTOR_USUARIO, gerencia:"DSI_DSC_BAJA", grupoTuID: "").save(failOnError: true)
		new Sistema(descripcion: "SIGNO_ABASTECIMIENTO" , descripcionLarga:  "Modulo de Abastecimiento" , grupoAdministrador:  "AP_CH_SIGNO_ABASTECIMIENTO", grupoReferenteSWF: "RSWF_SIGNO_ABA", gerencia:"DSI_DSC_SIGNO-AYV", grupoTuID: "").save(failOnError: true)
		new Sistema(descripcion: "SIGNO_AF" , descripcionLarga:  "Modulo de Activos Fijos" , grupoAdministrador:  "AP_CH_SIGNO_AF", grupoReferenteSWF: GRUPO_INTERLOCUTOR_USUARIO, gerencia:"DSI_DSC_SIGNO-FIN", grupoTuID: "").save(failOnError: true)
		new Sistema(descripcion: "SIGNO_APL" , descripcionLarga:  "Sistema de ERP que integra funcionalidades de  Finanzas y Controlling, Proyectos" , grupoAdministrador:  "AP_CH_SIGNO_APL", grupoReferenteSWF: GRUPO_INTERLOCUTOR_USUARIO, gerencia:"DSI_DSC_SIGNO", grupoTuID: "").save(failOnError: true)
		new Sistema(descripcion: "SIGNO_BW_FIN" , descripcionLarga:  "Aplicativos para reportes Signo" , grupoAdministrador:  "AP_CH_SIGNO_BW_FIN", grupoReferenteSWF: GRUPO_INTERLOCUTOR_USUARIO, gerencia:"DSI_DSC_BW", grupoTuID: "").save(failOnError: true)
		new Sistema(descripcion: "SIGNO_BW_RED" , descripcionLarga:  "Aplicativos para reportes Signo" , grupoAdministrador:  "AP_CH_SIGNO_BW_RED", grupoReferenteSWF: GRUPO_INTERLOCUTOR_USUARIO, gerencia:"DSI_DSC_BW", grupoTuID: "").save(failOnError: true)
		new Sistema(descripcion: "SIGNO_CAP" , descripcionLarga:  "Modulo de Cobranzas" , grupoAdministrador:  "AP_CH_SIGNO_CAP", grupoReferenteSWF: GRUPO_INTERLOCUTOR_USUARIO, gerencia:"DSI_DSC_SIGNO-FIN", grupoTuID: "").save(failOnError: true)
		new Sistema(descripcion: "SIGNO_COBRANZAS" , descripcionLarga:  "Modulo de Tesoreria" , grupoAdministrador:  "AP_CH_SIGNO_COBRANZAS", grupoReferenteSWF: "RSWF_SIGNO_NETWEAVER", gerencia:"DSI_DSC_SIGNO-FIN", grupoTuID: "").save(failOnError: true)
		new Sistema(descripcion: "SIGNO_COMPRAS" , descripcionLarga:  "Modulo de Compras" , grupoAdministrador:  "AP_CH_SIGNO_COMPRAS", grupoReferenteSWF: GRUPO_INTERLOCUTOR_USUARIO, gerencia:"DSI_DSC_SIGNO-AYV", grupoTuID: "").save(failOnError: true)
		new Sistema(descripcion: "SIGNO_COMPRAS_ESTRATEGIAS" , descripcionLarga:  "SIGNO COMPRAS ESTRATEGIAS" , grupoAdministrador:  "AP_CH_SIGNO_COMPRAS", grupoReferenteSWF: GRUPO_INTERLOCUTOR_USUARIO, gerencia:"DSI_DSC_SIGNO-AYV", grupoTuID: "").save(failOnError: true)
		new Sistema(descripcion: "SIGNO_ESS" , descripcionLarga:  "PORTAL - APLICACIONES - ESS" , grupoAdministrador:  "AP_CH_SAP_GRC", grupoReferenteSWF: GRUPO_INTERLOCUTOR_USUARIO, gerencia:"DSI_DSC_ESS", grupoTuID: "").save(failOnError: true)
		new Sistema(descripcion: "SIGNO_FACTURACION" , descripcionLarga:  "Modulo de Facturacion" , grupoAdministrador:  "AP_CH_SIGNO_FACTURACION", grupoReferenteSWF: GRUPO_INTERLOCUTOR_USUARIO, gerencia:"DSI_DSC_SIGNO-AYV", grupoTuID: "").save(failOnError: true)
		new Sistema(descripcion: "SIGNO_FINANZAS" , descripcionLarga:  "Modulo de Finanzas" , grupoAdministrador:  "AP_CH_SIGNO_FINANZAS", grupoReferenteSWF: "RSWF_SIGNO_NETWEAVER", gerencia:"DSI_DSC_SIGNO-FIN", grupoTuID: "").save(failOnError: true)
		new Sistema(descripcion: "SIGNO_FINANZAS_PY" , descripcionLarga:  "MODULO DE FINANZAS PY" , grupoAdministrador:  "AP_CH_SIGNO_FINANZAS_PY", grupoReferenteSWF: GRUPO_INTERLOCUTOR_USUARIO, gerencia:"DSI_DSC_SIGNO-FIN-PY", grupoTuID: "").save(failOnError: true)
		new Sistema(descripcion: "SIGNO_GUIAINTERNA" , descripcionLarga:  "PORTAL - APLICACIONES - GUIA INTERNA" , grupoAdministrador:  "AP_CH_SIGNO_GUIAINTERNA_APL", grupoReferenteSWF: GRUPO_INTERLOCUTOR_USUARIO, gerencia:"DSC", grupoTuID: "").save(failOnError: true)
		new Sistema(descripcion: "SIGNO_INTRANET_APL" , descripcionLarga:  "INTRANET" , grupoAdministrador:  "AP_CH_SIGNO_INTRANET_APL", grupoReferenteSWF: GRUPO_INTERLOCUTOR_USUARIO, gerencia:"DSI_DSC_INTRANET", grupoTuID: "").save(failOnError: true)
		new Sistema(descripcion: "SIGNO_INTRANET_DESEMPEÑO" , descripcionLarga:  "GESTI\u00F3N DE DESEMPEÑO DEL EMPLEADO" , grupoAdministrador:  "AP_CH_SIGNO_INTRANET_DESEMPEÑO", grupoReferenteSWF: GRUPO_INTERLOCUTOR_USUARIO, gerencia:"DSI_DSC_INTRANET", grupoTuID: "").save(failOnError: true)
		new Sistema(descripcion: "SIGNO_LOGISTICA" , descripcionLarga:  "Modulo de Logistica" , grupoAdministrador:  "AP_CH_SIGNO_LOGISTICA", grupoReferenteSWF: GRUPO_INTERLOCUTOR_USUARIO, gerencia:"DSI_DSC_SIGNO-AYV", grupoTuID: "").save(failOnError: true)
		new Sistema(descripcion: "SIGNO_MANTENIMIENTO" , descripcionLarga:  "SIGNO MANTENIMIENTO" , grupoAdministrador:  "AP_CH_SIGNO_PROYECTOS", grupoReferenteSWF: GRUPO_INTERLOCUTOR_USUARIO, gerencia:"DSI_DSC_SIGNO-PM", grupoTuID: "").save(failOnError: true)
		new Sistema(descripcion: "SIGNO_NEOPORTAL_APL" , descripcionLarga:  "Neoportal" , grupoAdministrador:  "AP_CH_SIGNO_NEOPORTAL_APL", grupoReferenteSWF: GRUPO_INTERLOCUTOR_USUARIO, gerencia:"DSI_DSC_NEOPORTAL", grupoTuID: "").save(failOnError: true)
		new Sistema(descripcion: "SIGNO_NEOPORTAL_RADIO" , descripcionLarga:  "COMUNICACI\u00F3N" , grupoAdministrador:  "AP_CH_SIGNO_NEOPORTAL_RADIO", grupoReferenteSWF: GRUPO_INTERLOCUTOR_USUARIO, gerencia:"DSI_DSC_NEOPORTAL", grupoTuID: "").save(failOnError: true)
		new Sistema(descripcion: "SIGNO_NEO_INTRANET" , descripcionLarga:  "Portal - Aplicaciones Intranet" , grupoAdministrador:  "AP_CH_SIGNO_NEO_INTRANET", grupoReferenteSWF: GRUPO_INTERLOCUTOR_USUARIO, gerencia:"DSI_DSC_INTRANET", grupoTuID: "").save(failOnError: true)
		new Sistema(descripcion: "SIGNO_NEO_NOTES" , descripcionLarga:  "Portal - Aplicaciones Lotus Notes" , grupoAdministrador:  "AP_CH_SIGNO_NEO_NOTES", grupoReferenteSWF: GRUPO_INTERLOCUTOR_USUARIO, gerencia:"DSI_DSC_NEOPORTAL", grupoTuID: "").save(failOnError: true)
		new Sistema(descripcion: "SIGNO_PROYECTOS" , descripcionLarga:  "Modulo de Proyectos" , grupoAdministrador:  "AP_CH_SIGNO_PROYECTOS", grupoReferenteSWF: GRUPO_INTERLOCUTOR_USUARIO, gerencia:"DSI_DSC_SIGNO-PS", grupoTuID: "").save(failOnError: true)
		new Sistema(descripcion: "SIGNO_REAL_ESTATE" , descripcionLarga:  "Modulo de REAL ESTATE" , grupoAdministrador:  "AP_CH_SIGNO_REAL_ESTATE", grupoReferenteSWF: GRUPO_INTERLOCUTOR_USUARIO, gerencia:"DSI_DSC_SIGNO-FIN", grupoTuID: "").save(failOnError: true)
		new Sistema(descripcion: "SIGNO_REGISTRO_FIRMAS" , descripcionLarga:  "PORTAL - APLICACIONES - REGISTRO DE FIRMAS" , grupoAdministrador:  "AP_CH_SIGNO_REGISTRO_FIRMAS", grupoReferenteSWF: GRUPO_INTERLOCUTOR_USUARIO, gerencia:"", grupoTuID: "").save(failOnError: true)
		new Sistema(descripcion: "SIGNO_RRHH" , descripcionLarga:  "Modulo de Recursos Humanos" , grupoAdministrador:  "AP_CH_SIGNO_RRHH", grupoReferenteSWF: GRUPO_INTERLOCUTOR_USUARIO, gerencia:"DSI_DSC_SIGNO-RHU", grupoTuID: "").save(failOnError: true)
		new Sistema(descripcion: "SIGNO_SAP_GRC" , descripcionLarga:  "PORTAL - APLICACIONES - SAP GRC" , grupoAdministrador:  "AP_CH_SAP_GRC_PRO", grupoReferenteSWF: GRUPO_INTERLOCUTOR_USUARIO, gerencia:"DSI_DSC_GRC", grupoTuID: "").save(failOnError: true)
		new Sistema(descripcion: "SIGNO_SH_AGENDABLOG_APL" , descripcionLarga:  "PORTAL - APLICACIONES - SHAREPOINT - AGENDA BLOG" , grupoAdministrador:  "AP_CH_SIGNO_SH_AGENDABLOG_APL", grupoReferenteSWF: GRUPO_INTERLOCUTOR_USUARIO, gerencia:"DSC", grupoTuID: "").save(failOnError: true)
		new Sistema(descripcion: "SIGNO_SH_BLOG_COPAMUNDO_APL" , descripcionLarga:  "PORTAL - APLICACIONES - SHAREPOINT - BLOG COPAMUNDO" , grupoAdministrador:  "AP_CH_SIGNO_SH_BLOG_COPAMUNDO_APL", grupoReferenteSWF: GRUPO_INTERLOCUTOR_USUARIO, gerencia:"DSC", grupoTuID: "").save(failOnError: true)
		new Sistema(descripcion: "SIGNO_SH_BLOG_EQUILIBRIO_APL" , descripcionLarga:  "PORTAL - APLICACIONES - SHAREPOINT -BLOG EQUILIBRIO" , grupoAdministrador:  "AP_CH_SIGNO_SH_BLOG_EQUILIBRIO_APL", grupoReferenteSWF: GRUPO_INTERLOCUTOR_USUARIO, gerencia:"DSC", grupoTuID: "").save(failOnError: true)
		new Sistema(descripcion: "SIGNO_SH_BLOG_TELETRABAJO_APL" , descripcionLarga:  "PORTAL - APLICACIONES - SHAREPOINT - BLOG TELETRABAJO" , grupoAdministrador:  "AP_CH_SIGNO_SH_BLOG_TELETRABAJO_APL", grupoReferenteSWF: GRUPO_INTERLOCUTOR_USUARIO, gerencia:"DSC", grupoTuID: "").save(failOnError: true)
		new Sistema(descripcion: "SIGNO_SH_COLONIA_APL" , descripcionLarga:  "PORTAL - APLICACIONES - SHAREPOINT - COLONIA" , grupoAdministrador:  "AP_CH_SIGNO_SH_COLONIA_APL", grupoReferenteSWF: GRUPO_INTERLOCUTOR_USUARIO, gerencia:"DSC", grupoTuID: "").save(failOnError: true)
		new Sistema(descripcion: "SIGNO_SH_DDJJ_APL" , descripcionLarga:  "PORTAL - APLICACIONES - SHAREPOINT - DDJJ" , grupoAdministrador:  "AP_CH_SIGNO_SH_DDJJ_APL", grupoReferenteSWF: GRUPO_INTERLOCUTOR_USUARIO, gerencia:"DSC", grupoTuID: "").save(failOnError: true)
		new Sistema(descripcion: "SIGNO_SH_IMP_GANANCIAS_APL" , descripcionLarga:  "PORTAL - APLICACIONES - SHAREPOINT - IMP GANANCIAS" , grupoAdministrador:  "AP_CH_SIGNO_SH_IMP_GANANCIAS_APL", grupoReferenteSWF: GRUPO_INTERLOCUTOR_USUARIO, gerencia:"DSC", grupoTuID: "").save(failOnError: true)
		new Sistema(descripcion: "SIGNO_SH_LINEAS_SERVICIO_APL" , descripcionLarga:  "PORTAL - APLICACIONES - SHAREPOINT - LINEAS SERVICIO" , grupoAdministrador:  "AP_CH_SIGNO_SH_LINEAS_SERVICIO_APL", grupoReferenteSWF: GRUPO_INTERLOCUTOR_USUARIO, gerencia:"DSC", grupoTuID: "").save(failOnError: true)
		new Sistema(descripcion: "SIGNO_SH_WIKI_APL" , descripcionLarga:  "PORTAL - APLICACIONES - SHAREPOINT -WIKI" , grupoAdministrador:  "AP_CH_SIGNO_SH_WIKI_APL", grupoReferenteSWF: GRUPO_INTERLOCUTOR_USUARIO, gerencia:"DSC", grupoTuID: "").save(failOnError: true)
		new Sistema(descripcion: "SIGNO_SOLMAN_APL" , descripcionLarga:  "Solution Manager" , grupoAdministrador:  "AP_CH_SIGNO_SOLMAN_APL", grupoReferenteSWF: GRUPO_INTERLOCUTOR_USUARIO, gerencia:"DSI_DSC_SIGNO-SOLMAN", grupoTuID: "").save(failOnError: true)
		new Sistema(descripcion: "SIGNO_SORTEOS" , descripcionLarga:  "PORTAL - APLICACIONES - SORTEOS" , grupoAdministrador:  "AP_CH_SIGNO_SORTEOS_APL", grupoReferenteSWF: GRUPO_INTERLOCUTOR_USUARIO, gerencia:"DSC", grupoTuID: "").save(failOnError: true)
		new Sistema(descripcion: "SIGNO_TESORERIA" , descripcionLarga:  "Modulo de Tesoreria" , grupoAdministrador:  "AP_CH_SIGNO_TESORERIA", grupoReferenteSWF: GRUPO_INTERLOCUTOR_USUARIO, gerencia:"DSI_DSC_SIGNO-FIN", grupoTuID: "").save(failOnError: true)
		new Sistema(descripcion: "SIGNO_VECONP" , descripcionLarga:  "PORTAL - APLICACIONES - VECONP" , grupoAdministrador:  "AP_CH_SIGNO_VECONP", grupoReferenteSWF: GRUPO_INTERLOCUTOR_USUARIO, gerencia:"DSI_DSC_CP", grupoTuID: "").save(failOnError: true)
		new Sistema(descripcion: "SIGNO_VENTAS" , descripcionLarga:  "MODULO DE VENTAS" , grupoAdministrador:  "AP_CH_SIGNO_VENTAS", grupoReferenteSWF: GRUPO_INTERLOCUTOR_USUARIO, gerencia:"DSI_DSC_SIGNO-TERMINALES", grupoTuID: "").save(failOnError: true)
		new Sistema(descripcion: "SIGNO_VIAJES" , descripcionLarga:  "Modulo de VIAJES" , grupoAdministrador:  "AP_CH_SIGNO_VIAJES", grupoReferenteSWF: GRUPO_INTERLOCUTOR_USUARIO, gerencia:"DSI_DSC_SIGNO-FIN", grupoTuID: "").save(failOnError: true)
		new Sistema(descripcion: "SIGNO_VOLUNTARIADO" , descripcionLarga:  "PORTAL - APLICACIONES - RED DE VOLUNTARIOS" , grupoAdministrador:  "AP_CH_SIGNO_VOLUNTARIADO_APL", grupoReferenteSWF: GRUPO_INTERLOCUTOR_USUARIO, gerencia:"DSC", grupoTuID: "").save(failOnError: true)
		new Sistema(descripcion: "SIMPLIT_APL" , descripcionLarga:  "Service Manager" , grupoAdministrador:  "AP_CH_SIMPLIT_APL", grupoReferenteSWF: GRUPO_INTERLOCUTOR_USUARIO, gerencia:"DSI_DSC_G-IT", grupoTuID: "").save(failOnError: true)
		new Sistema(descripcion: "SIR_APL" , descripcionLarga:  "Sistema Integral de Reportes. Obtiene datos de Utopia para la generaci\u00F3n de Repo" , grupoAdministrador:  "AP_CH_SIR_APL", grupoReferenteSWF: GRUPO_INTERLOCUTOR_USUARIO, gerencia:"DTI_DSIN_SISTSOPGEST", grupoTuID: "").save(failOnError: true)
		new Sistema(descripcion: "SMART_APL" , descripcionLarga:  "Datawarehouse de tr\u00E1fico de la compañ\u00EDa." , grupoAdministrador:  "AP_CH_SMART_APL", grupoReferenteSWF: GRUPO_INTERLOCUTOR_USUARIO, gerencia:"DTI_DSIN_PLATDW", grupoTuID: "").save(failOnError: true)
		new Sistema(descripcion: "SMART_CARGA_Y_PUBLICACION" , descripcionLarga:  "Datawarehouse de tr\u00E1fico de la compañ\u00EDa." , grupoAdministrador:  "AP_CH_SMART_CARGA_Y_PUBLICACION", grupoReferenteSWF: GRUPO_INTERLOCUTOR_USUARIO, gerencia:"DTI_DSIN_PLATDW", grupoTuID: "").save(failOnError: true)
		new Sistema(descripcion: "SMART_INTERFACES" , descripcionLarga:  "Datawarehouse de tr\u00E1fico de la compañ\u00EDa." , grupoAdministrador:  "AP_CH_SMART_INTERFACES", grupoReferenteSWF: GRUPO_INTERLOCUTOR_USUARIO, gerencia:"DTI_DSIN_PLATDW", grupoTuID: "").save(failOnError: true)
		new Sistema(descripcion: "SMART_MODELO_DE_DATOS" , descripcionLarga:  "Datawarehouse de tr\u00E1fico de la compañ\u00EDa." , grupoAdministrador:  "AP_CH_SMART_MODELO_DE_DATOS", grupoReferenteSWF: GRUPO_INTERLOCUTOR_USUARIO, gerencia:"DTI_DSIN_PLATDW", grupoTuID: "").save(failOnError: true)
		new Sistema(descripcion: "SP_CALIDAD_REDESYSERVICIOS_APL" , descripcionLarga:  "ADMINISTRACI\u00F3N DE INFORMES DIN\u00E1MICOS" , grupoAdministrador:  "QV_REMEDY_APL", grupoReferenteSWF: GRUPO_INTERLOCUTOR_USUARIO, gerencia:" DSI_DST_DSRT", grupoTuID: "").save(failOnError: true)
		new Sistema(descripcion: "SUC2_APL" , descripcionLarga:  "SUC ES UN SISTEMA QUE MANEJA INFORMACI\u00F3N GEOGR\u00E1FICA, CON EL OBJETIVO DE CREAR, INCORPORAR Y MANTENER INFORMACI\u00F3N CARTOGR\u00E1FICA BASE PARA SU UTILIZACI\u00F3N POR PARTE DE LAS PERSONAS Y LA DE OTROS SISTEMAS. BASADO EN SET AMPLIO DE PRODUCTOS" , grupoAdministrador:  "AP_CH_SUC2_APL", grupoReferenteSWF: GRUPO_INTERLOCUTOR_USUARIO, gerencia:"DTI_DSIN_SISTSOPGEST", grupoTuID: "").save(failOnError: true)
		new Sistema(descripcion: "SUC_GEODATABASE" , descripcionLarga:  "Repositorio de cartograf\u00EDa de Telecom Argentina y fuente de dicha informaci\u00F3n pa" , grupoAdministrador:  "", grupoReferenteSWF: GRUPO_INTERLOCUTOR_USUARIO, gerencia:"DTI_DSIN_SISTSOPGEST", grupoTuID: "").save(failOnError: true)
		new Sistema(descripcion: "SUC_HERRAMIENTAS_DEL_CLIENTE" , descripcionLarga:  "Repositorio de cartograf\u00EDa de Telecom Argentina y fuente de dicha informaci\u00F3n pa" , grupoAdministrador:  "", grupoReferenteSWF: GRUPO_INTERLOCUTOR_USUARIO, gerencia:"DTI_DSIN_SISTSOPGEST", grupoTuID: "").save(failOnError: true)
		new Sistema(descripcion: "SUC_WEB_GEOGRAFICA" , descripcionLarga:  "Repositorio de cartograf\u00EDa de Telecom Argentina y fuente de dicha informaci\u00F3n pa" , grupoAdministrador:  "", grupoReferenteSWF: GRUPO_INTERLOCUTOR_USUARIO, gerencia:"DTI_DSIN_SISTSOPGEST", grupoTuID: "").save(failOnError: true)
		new Sistema(descripcion: "SUPERVISION_VPN_APL" , descripcionLarga:  "“SUPERVISI\u00F3N DE VPNS Y REDES DE DATOS, PERMITE MONITOREAR EL ESTADO Y VISUALIZAR SU TOPOLOG\u00EDA”" , grupoAdministrador:  "AP_CH_SUPERVISION_VPN_APL", grupoReferenteSWF: GRUPO_INTERLOCUTOR_USUARIO, gerencia:"DSI_DST_DSFP", grupoTuID: "").save(failOnError: true)
		new Sistema(descripcion: "SUR_APL" , descripcionLarga:  "Plataforma unificada para la atenci\u00F3n, seguimiento y gesti\u00F3n de trouble tickets" , grupoAdministrador:  "AP_CH_SUR_APL", grupoReferenteSWF: "RSWF_SUR", gerencia:"DSI_DST_DSRT", grupoTuID: "").save(failOnError: true)
		new Sistema(descripcion: "SUS_APL" , descripcionLarga:  "Administrar y supervisar las distintas tecnolog\u00EDas de telefon\u00EDa p\u00FAblica en forma" , grupoAdministrador:  "AP_CH_SUS_APL", grupoReferenteSWF: GRUPO_INTERLOCUTOR_USUARIO, gerencia:"DSI_DST_DSFP", grupoTuID: "").save(failOnError: true)
		new Sistema(descripcion: "TAB_GO_APL" , descripcionLarga:  "TABLERO DE GESTI\u00F3N OPERATIVA: SISTEMA QUE REALIZA AN\u00E1LISIS DE GESTI\u00F3N OPERATIVA" , grupoAdministrador:  "AP_CH_TAB_GO_APL", grupoReferenteSWF: GRUPO_INTERLOCUTOR_USUARIO, gerencia:"DTI_DSIN_PLATDW", grupoTuID: "").save(failOnError: true)
		new Sistema(descripcion: "TAB_RA_APL" , descripcionLarga:  "TABLERO DE REVENUE ASSURANCE: SISTEMA QUE REALIZA AN\u00E1LISIS DE REVENUE ASSURANCE" , grupoAdministrador:  "AP_CH_TAB_RA_APL", grupoReferenteSWF: GRUPO_INTERLOCUTOR_USUARIO, gerencia:"DTI_DSIN_PLATDW", grupoTuID: "").save(failOnError: true)
		new Sistema(descripcion: "TANC_PLANIFICACION_APL" , descripcionLarga:  "Son las ANC tuteladas que sirven para el soporte de Planificaci\u00F3n de Customer Ca" , grupoAdministrador:  "AP_CH_TANC_PLANIFICACION_APL", grupoReferenteSWF: GRUPO_INTERLOCUTOR_USUARIO, gerencia:"DTI_DSIN", grupoTuID: "").save(failOnError: true)
		new Sistema(descripcion: "TANC_POSTVENTAS_APL" , descripcionLarga:  "Son las ANC tuteladas que sirven para el soporte de Post-Ventas de Customer Care" , grupoAdministrador:  "AP_CH_TANC_POSTVENTAS_APL", grupoReferenteSWF: GRUPO_INTERLOCUTOR_USUARIO, gerencia:"DTI_DSIN", grupoTuID: "").save(failOnError: true)
		new Sistema(descripcion: "TANC_VENTAS_APL" , descripcionLarga:  "Son las ANC tuteladas que sirven para el soporte de Ventas de Customer Care" , grupoAdministrador:  "AP_CH_TANC_VENTAS_APL", grupoReferenteSWF: GRUPO_INTERLOCUTOR_USUARIO, gerencia:"DTI_DSIN", grupoTuID: "").save(failOnError: true)
		new Sistema(descripcion: "TANC_WEB_MALICIOSAS_APL" , descripcionLarga:  "Son las ANC tuteladas que sirven para el soporte de las intervenciones Judiciale" , grupoAdministrador:  "AP_CH_TANC_WEB_MALICIOSAS_APL", grupoReferenteSWF: GRUPO_INTERLOCUTOR_USUARIO, gerencia:"DTI_DSIN", grupoTuID: "").save(failOnError: true)
		new Sistema(descripcion: "TARJETA_PRE_POSPAGA_APL" , descripcionLarga:  "Sistema de facturaci\u00F3n de los productos nacionales e internacionales correspondi" , grupoAdministrador:  "AP_CH_TARJETA_PRE_POSPAGA_APL", grupoReferenteSWF: GRUPO_INTERLOCUTOR_USUARIO, gerencia:"DTI_DSFC_MEDIACION-FRAUDE", grupoTuID: "").save(failOnError: true)
		new Sistema(descripcion: "TAS_APL" , descripcionLarga:  "EL TROUBLE TICKET DE ASEGURAMIENTO TIENE POR FINALIDAD CONCENTRAR LA ATENCI\u00F3N DE RECLAMOS DE ASEGURAMIENTO POSVENTA (AP) Y OPERADORES Y PRESTADORES (OYP)." , grupoAdministrador:  "AP_CH_TAS_APL", grupoReferenteSWF: GRUPO_INTERLOCUTOR_USUARIO, gerencia:"DSI_DST_DSRT", grupoTuID: "").save(failOnError: true)
		new Sistema(descripcion: "TEMIS_APL" , descripcionLarga:  "Sistema que administra los oficios judiciales para responder a solicitudes de la" , grupoAdministrador:  "AP_CH_TEMIS_APL", grupoReferenteSWF: GRUPO_INTERLOCUTOR_USUARIO, gerencia:"DTI_DSFC_COBR-MASIVOS", grupoTuID: "").save(failOnError: true)
		new Sistema(descripcion: "TIME_KEEPING_APL" , descripcionLarga:  "SISTEMA DE CARGA DE HORAS" , grupoAdministrador:  "AP_CH_TIME_KEEPING_APL", grupoReferenteSWF: GRUPO_INTERLOCUTOR_USUARIO, gerencia:"DSI_TIME-KEEPING", grupoTuID: "").save(failOnError: true)
		new Sistema(descripcion: "TRANSFORMER_APL" , descripcionLarga:  "APLICACI\u00F3N QUE PERMITE LA TRADUCCI\u00F3N DE LOS PRODUCTOS DE FACTURACI\u00F3N Y PROVISI\u00F3N PARA LA VENTA DE ADSL." , grupoAdministrador:  "AP_CH_ADSL_VENTAS_APL", grupoReferenteSWF: GRUPO_INTERLOCUTOR_USUARIO, gerencia:"DTI_DSPI_PROV-SISTEMAS-BASICOS", grupoTuID: "").save(failOnError: true)
		new Sistema(descripcion: "TROUBLE_TICKET_ARNET_APL" , descripcionLarga:  "Aplicaci\u00F3n donde todos los operadores de atenci\u00F3n comercial registran todos los" , grupoAdministrador:  "", grupoReferenteSWF: GRUPO_INTERLOCUTOR_USUARIO, gerencia:"DSI_DST_BAJA", grupoTuID: "").save(failOnError: true)
		new Sistema(descripcion: "TROUBLE_TICKET_OYP_APL" , descripcionLarga:  "Sistema de trouble ticket para la UN OyP. Permite el seguimiento de tramites com" , grupoAdministrador:  "AP_CH_TROUBLE_TICKET_OYP_APL", grupoReferenteSWF: GRUPO_INTERLOCUTOR_USUARIO, gerencia:"DSI_DST_DSRT", grupoTuID: "").save(failOnError: true)
		new Sistema(descripcion: "TTUSA_PREPAGO_API" , descripcionLarga:  "Sistema que gestiona end to end la provisi\u00F3n de los servicios internacionales" , grupoAdministrador:  "AP_CH_TTUSA_PREPAGO_API", grupoReferenteSWF: GRUPO_INTERLOCUTOR_USUARIO, gerencia:"DTI_DSCRM_TTUSA-PREPAGO", grupoTuID: "").save(failOnError: true)
		new Sistema(descripcion: "TTUSA_PREPAGO_GESTION" , descripcionLarga:  "Sistema que gestiona end to end la provisi\u00F3n de los servicios internacionales" , grupoAdministrador:  "AP_CH_TTUSA_PREPAGO_GESTION", grupoReferenteSWF: GRUPO_INTERLOCUTOR_USUARIO, gerencia:"DTI_DSCRM_TTUSA-PREPAGO", grupoTuID: "").save(failOnError: true)
		new Sistema(descripcion: "TUID_APL" , descripcionLarga:  "SISTEMA DE GESTI\u00F3N DE IDENTIDADES" , grupoAdministrador:  "SI_CH_TUID", grupoReferenteSWF: GRUPO_INTERLOCUTOR_USUARIO, gerencia:"SEGURIDAD INFORMATICA", grupoTuID: "").save(failOnError: true)
		new Sistema(descripcion: "UTOPIA_APL" , descripcionLarga:  "Creaci\u00F3n/Configuraci\u00F3n de productos. Permite la configuraci\u00F3n, la provisi\u00F3n, def" , grupoAdministrador:  "AP_CH_UTOPIA_APL", grupoReferenteSWF: GRUPO_INTERLOCUTOR_USUARIO, gerencia:"DTI_DSPI_PROV-SERV-INTERNET", grupoTuID: "").save(failOnError: true)
		new Sistema(descripcion: "VANTIVE_FIELD_SERVICE_APL" , descripcionLarga:  "Workflow de seguimiento de la provisi\u00F3n de servicios comercializados por la UN U" , grupoAdministrador:  "AP_CH_VANTIVE_FIELD_SERVICE_APL", grupoReferenteSWF: GRUPO_INTERLOCUTOR_USUARIO, gerencia:"DTI_DSPI_PROV-MERCADO-NO-MASIVO", grupoTuID: "").save(failOnError: true)
		new Sistema(descripcion: "VANTIVE_HELP_DESK_APL" , descripcionLarga:  "Sistema que realiza la registraci\u00F3n y seguimiento de los requerimientos de asist" , grupoAdministrador:  "AP_CH_VANTIVE_HELP_DESK_APL", grupoReferenteSWF: GRUPO_INTERLOCUTOR_USUARIO, gerencia:"DTI_DSPI_PROV-MERCADO-NO-MASIVO", grupoTuID: "").save(failOnError: true)
		new Sistema(descripcion: "VANTIVE_INVENTORY_APL" , descripcionLarga:  "Sistema que mantiene el inventario del equipamiento de Microinform\u00E1tica de la em" , grupoAdministrador:  "AP_CH_VANTIVE_INVENTORY_APL", grupoReferenteSWF: GRUPO_INTERLOCUTOR_USUARIO, gerencia:"DTI_DSPI_PROV-MERCADO-NO-MASIVO", grupoTuID: "").save(failOnError: true)
		new Sistema(descripcion: "VANTIVE_SUPPORT_APL" , descripcionLarga:  "Sistema de trouble ticket para los servicios de la ex-UN USE. Permite el seguimi" , grupoAdministrador:  "AP_CH_VANTIVE_SUPPORT_APL", grupoReferenteSWF: GRUPO_INTERLOCUTOR_USUARIO, gerencia:"DSI_DST_DSRT", grupoTuID: "").save(failOnError: true)
		new Sistema(descripcion: "VGA-AFIP_APL" , descripcionLarga:  "LA APLICACI\u00F3N WEB VGA-AFIP, ES CONSUMIDA POR LA AFIP A TRAV\u00E9S DE CNM, EN ELLA SE MUESTRA DE FORMA GRAFICA, Y UBICADA EN EL TERRITORIO NACIONA" , grupoAdministrador:  "ADMF_COR_SUC2", grupoReferenteSWF: GRUPO_INTERLOCUTOR_USUARIO, gerencia:"", grupoTuID: "").save(failOnError: true)
		new Sistema(descripcion: "VIRTEC_APL" , descripcionLarga:  "APLICACI\u00F3N QUE LLAMA A CLIENTES PARA VERIFICAR SI LA LINEA FUNCIONA" , grupoAdministrador:  "AP_CH_VIRTEC_APL", grupoReferenteSWF: GRUPO_INTERLOCUTOR_USUARIO, gerencia:"DTI_DSIN", grupoTuID: "").save(failOnError: true)
		new Sistema(descripcion: "VISP_APL" , descripcionLarga:  "Sistemas que gestiona pools de conexiones Dial Up que son entregados a las Coope" , grupoAdministrador:  "AP_CH_VISP_APL", grupoReferenteSWF: GRUPO_INTERLOCUTOR_USUARIO, gerencia:"DTI_DSPI_PROV-SERV-INTERNET", grupoTuID: "").save(failOnError: true)
		new Sistema(descripcion: "WEBMAIL_APL" , descripcionLarga:  "Conjunto de p\u00E1ginas Web que dan servicios a clientes y a usuarios registrados." , grupoAdministrador:  "AP_CH_WEBMAIL_APL", grupoReferenteSWF: GRUPO_INTERLOCUTOR_USUARIO, gerencia:"DTI_SIM_VA", grupoTuID: "").save(failOnError: true)
		new Sistema(descripcion: "WEB_ADSL_TECNICA_APL" , descripcionLarga:  "INTERFAZ USUARIO WEB PARA REALIZAR CONSULTAS, REPORTES Y ACCIONES SOBRE NETWORK" , grupoAdministrador:  "AP_CH_WEB_ADSL_TECNICA_APL", grupoReferenteSWF: GRUPO_INTERLOCUTOR_USUARIO, gerencia:"DSI_DST_DSIN", grupoTuID: "").save(failOnError: true)
		new Sistema(descripcion: "WEB_DE_DENUNCIAS_APL" , descripcionLarga:  "APLICATIVO DE DENUNCIAS DE EVALUACIONES ESPECIALES" , grupoAdministrador:  "AP_CH_WEB_DENUNCIAS_APL", grupoReferenteSWF: GRUPO_INTERLOCUTOR_USUARIO, gerencia:"DSI_GSC", grupoTuID: "").save(failOnError: true)
		new Sistema(descripcion: "WEB_SNMP_APL" , descripcionLarga:  "Compara informaci\u00F3n de la Redes de Backbone IP, Midas, Voz sobre IP, Legacy Arne" , grupoAdministrador:  "AP_CH_WEB_SNMP_APL", grupoReferenteSWF: GRUPO_INTERLOCUTOR_USUARIO, gerencia:"DSI_DST_DSIN", grupoTuID: "").save(failOnError: true)
		new Sistema(descripcion: "WORKFLOW_TECNICO_APL" , descripcionLarga:  "Workflow de seguimiento de la provisi\u00F3n de productos comercializados por la UN O" , grupoAdministrador:  "AP_CH_WORKFLOW_TECNICO_APL", grupoReferenteSWF: GRUPO_INTERLOCUTOR_USUARIO, gerencia:"DTI_DSPI_PROV-MERCADO-NO-MASIVO", grupoTuID: "").save(failOnError: true)
		new Sistema(descripcion: "YADAS_APL" , descripcionLarga:  "SE UTILIZA EN EL SERVICIO 110 INFORMACION DE GUIA, TRABAJA 7 X 24" , grupoAdministrador:  "AP_CH_CT_YADAS_APL", grupoReferenteSWF: GRUPO_INTERLOCUTOR_USUARIO, gerencia:"", grupoTuID: "").save(failOnError: true)
		new Sistema(descripcion: "ZOOM_APL" , descripcionLarga:  "Repositorio \u00FAnico de informaci\u00F3n, tanto de Telecom como de otras fuentes externa" , grupoAdministrador:  "AP_CH_ZOOM_APL", grupoReferenteSWF: GRUPO_INTERLOCUTOR_USUARIO, gerencia:"DTI_DSIN_PLATDW", grupoTuID: "").save(failOnError: true)
		new Sistema(descripcion: "ZOOM_CARGA_Y_PUBLICACION" , descripcionLarga:  "Repositorio \u00FAnico de informaci\u00F3n, tanto de Telecom como de otras fuentes externa" , grupoAdministrador:  "AP_CH_ZOOM_CARGA_Y_PUBLICACION", grupoReferenteSWF: GRUPO_INTERLOCUTOR_USUARIO, gerencia:"DTI_DSIN_PLATDW", grupoTuID: "").save(failOnError: true)
		new Sistema(descripcion: "ZOOM_INTERFACES" , descripcionLarga:  "Repositorio \u00FAnico de informaci\u00F3n, tanto de Telecom como de otras fuentes externa" , grupoAdministrador:  "AP_CH_ZOOM_INTERFACES", grupoReferenteSWF: GRUPO_INTERLOCUTOR_USUARIO, gerencia:"DTI_DSIN_PLATDW", grupoTuID: "").save(failOnError: true)
		new Sistema(descripcion: "ZOOM_MODELO_DE_DATOS" , descripcionLarga:  "Repositorio \u00FAnico de informaci\u00F3n, tanto de Telecom como de otras fuentes externa" , grupoAdministrador:  "AP_CH_ZOOM_MODELO_DE_DATOS", grupoReferenteSWF: GRUPO_INTERLOCUTOR_USUARIO, gerencia:"DTI_DSIN_PLATDW", grupoTuID: "").save(failOnError: true)
	}

	private def definirSWF() {
		new SoftwareFactory(descripcion: "SWF_DTI_112.COM_PROVEEDOR", gestionaCapacidad: false, responsableCargaCapacidad: null , asociadoSistema: true, grupoLDAP: "SWF_DTI_112.COM_PROVEEDOR", manoObraPropia: false).save(failOnError: true)
		new SoftwareFactory(descripcion: "SWF_DTI_ABONADOS_EN_GUIA_PROVEEDOR", gestionaCapacidad: false, responsableCargaCapacidad: null , asociadoSistema: true, grupoLDAP: "SWF_DTI_ABONADOS_EN_GUIA_PROVEEDOR", manoObraPropia: false).save(failOnError: true)
		new SoftwareFactory(descripcion: "SWF_DTI_ABONADOS_EN_GUIA_TELECOM", gestionaCapacidad: false, responsableCargaCapacidad: null , asociadoSistema: true, grupoLDAP: "SWF_DTI_ABONADOS_EN_GUIA_TELECOM", manoObraPropia: true).save(failOnError: true)
		new SoftwareFactory(descripcion: "SWF_DTI_ACDSERVER_PROVEEDOR", gestionaCapacidad: false, responsableCargaCapacidad: null , asociadoSistema: true, grupoLDAP: "SWF_DTI_ACDSERVER_PROVEEDOR", manoObraPropia: false).save(failOnError: true)
		new SoftwareFactory(descripcion: "SWF_DTI_ACDSERVER_TELECOM", gestionaCapacidad: false, responsableCargaCapacidad: null , asociadoSistema: true, grupoLDAP: "SWF_DTI_ACDSERVER_TELECOM", manoObraPropia: true).save(failOnError: true)
		new SoftwareFactory(descripcion: "SWF_DTI_ADAS_PROVEEDOR", gestionaCapacidad: false, responsableCargaCapacidad: null , asociadoSistema: true, grupoLDAP: "SWF_DTI_ADAS_PROVEEDOR", manoObraPropia: false).save(failOnError: true)
		new SoftwareFactory(descripcion: "SWF_DTI_ADAS_TELECOM", gestionaCapacidad: false, responsableCargaCapacidad: null , asociadoSistema: true, grupoLDAP: "SWF_DTI_ADAS_TELECOM", manoObraPropia: true).save(failOnError: true)
		new SoftwareFactory(descripcion: "SWF_DTI_ADHESION_FACTURA_DIGITAL_PROVEEDOR", gestionaCapacidad: false, responsableCargaCapacidad: null , asociadoSistema: true, grupoLDAP: "SWF_DTI_ADHESION_FACTURA_DIGITAL_PROVEEDOR", manoObraPropia: false).save(failOnError: true)
		new SoftwareFactory(descripcion: "SWF_DTI_ADM_DE_CONTACTOS_PROVEEDOR", gestionaCapacidad: false, responsableCargaCapacidad: null , asociadoSistema: true, grupoLDAP: "SWF_DTI_ADM_DE_CONTACTOS_PROVEEDOR", manoObraPropia: false).save(failOnError: true)
		new SoftwareFactory(descripcion: "SWF_DTI_ADM_DE_CONTACTOS_TELECOM", gestionaCapacidad: false, responsableCargaCapacidad: null , asociadoSistema: true, grupoLDAP: "SWF_DTI_ADM_DE_CONTACTOS_TELECOM", manoObraPropia: true).save(failOnError: true)
		new SoftwareFactory(descripcion: "SWF_DTI_AMADEUS_CENTRAL_PARQ_TELECOM", gestionaCapacidad: false, responsableCargaCapacidad: null , asociadoSistema: true, grupoLDAP: "SWF_DTI_AMADEUS_CENTRAL_PARQ_TELECOM", manoObraPropia: true).save(failOnError: true)
		new SoftwareFactory(descripcion: "SWF_DTI_AMADEUS_PROVEEDOR", gestionaCapacidad: false, responsableCargaCapacidad: null , asociadoSistema: true, grupoLDAP: "SWF_DTI_AMADEUS_PROVEEDOR", manoObraPropia: false).save(failOnError: true)
		new SoftwareFactory(descripcion: "SWF_DTI_AMADEUS_TELECOM", gestionaCapacidad: false, responsableCargaCapacidad: null , asociadoSistema: true, grupoLDAP: "SWF_DTI_AMADEUS_TELECOM", manoObraPropia: true).save(failOnError: true)
		new SoftwareFactory(descripcion: "SWF_DTI_BAS_PROVEEDOR", gestionaCapacidad: false, responsableCargaCapacidad: null , asociadoSistema: true, grupoLDAP: "SWF_DTI_BAS_PROVEEDOR", manoObraPropia: false).save(failOnError: true)
		new SoftwareFactory(descripcion: "SWF_DTI_BAS_TELECOM", gestionaCapacidad: false, responsableCargaCapacidad: null , asociadoSistema: true, grupoLDAP: "SWF_DTI_BAS_TELECOM", manoObraPropia: true).save(failOnError: true)
		new SoftwareFactory(descripcion: "SWF_DTI_BELEN_PROVEEDOR", gestionaCapacidad: false, responsableCargaCapacidad: null , asociadoSistema: true, grupoLDAP: "SWF_DTI_BELEN_PROVEEDOR", manoObraPropia: false).save(failOnError: true)
		new SoftwareFactory(descripcion: "SWF_DTI_BELEN_TELECOM", gestionaCapacidad: false, responsableCargaCapacidad: null , asociadoSistema: true, grupoLDAP: "SWF_DTI_BELEN_TELECOM", manoObraPropia: true).save(failOnError: true)
		new SoftwareFactory(descripcion: "SWF_DTI_BIOS_PROVEEDOR", gestionaCapacidad: false, responsableCargaCapacidad: null , asociadoSistema: true, grupoLDAP: "SWF_DTI_BIOS_PROVEEDOR", manoObraPropia: false).save(failOnError: true)
		new SoftwareFactory(descripcion: "SWF_DTI_BIOS_TELECOM", gestionaCapacidad: false, responsableCargaCapacidad: null , asociadoSistema: true, grupoLDAP: "SWF_DTI_BIOS_TELECOM", manoObraPropia: true).save(failOnError: true)
		new SoftwareFactory(descripcion: "SWF_DTI_CALIPSO_BILLING_TELECOM", gestionaCapacidad: false, responsableCargaCapacidad: null , asociadoSistema: true, grupoLDAP: "SWF_DTI_CALIPSO_BILLING_TELECOM", manoObraPropia: true).save(failOnError: true)
		new SoftwareFactory(descripcion: "SWF_DTI_CALIPSO_COBROS_TELECOM", gestionaCapacidad: false, responsableCargaCapacidad: null , asociadoSistema: true, grupoLDAP: "SWF_DTI_CALIPSO_COBROS_TELECOM", manoObraPropia: true).save(failOnError: true)
		new SoftwareFactory(descripcion: "SWF_DTI_CALIPSO_DATAIQ_PROVEEDOR", gestionaCapacidad: false, responsableCargaCapacidad: null , asociadoSistema: true, grupoLDAP: "SWF_DTI_CALIPSO_DATAIQ_PROVEEDOR", manoObraPropia: false).save(failOnError: true)
		new SoftwareFactory(descripcion: "SWF_DTI_CALIPSO_PREFACTURACION_TELECOM", gestionaCapacidad: false, responsableCargaCapacidad: null , asociadoSistema: true, grupoLDAP: "SWF_DTI_CALIPSO_PREFACTURACION_TELECOM", manoObraPropia: true).save(failOnError: true)
		new SoftwareFactory(descripcion: "SWF_DTI_CALIPSO_PROVEEDOR", gestionaCapacidad: false, responsableCargaCapacidad: null , asociadoSistema: true, grupoLDAP: "SWF_DTI_CALIPSO_PROVEEDOR", manoObraPropia: false).save(failOnError: true)
		new SoftwareFactory(descripcion: "SWF_DTI_CAMPAIGN_MANAGEMENT_PROVEEDOR", gestionaCapacidad: false, responsableCargaCapacidad: null , asociadoSistema: true, grupoLDAP: "SWF_DTI_CAMPAIGN_MANAGEMENT_PROVEEDOR", manoObraPropia: false).save(failOnError: true)
		new SoftwareFactory(descripcion: "SWF_DTI_CAMPAIGN_MANAGEMENT_TELECOM", gestionaCapacidad: false, responsableCargaCapacidad: null , asociadoSistema: true, grupoLDAP: "SWF_DTI_CAMPAIGN_MANAGEMENT_TELECOM", manoObraPropia: true).save(failOnError: true)
		new SoftwareFactory(descripcion: "SWF_DTI_CARRIERS_INTERCONECTANTES_PROVEEDOR", gestionaCapacidad: false, responsableCargaCapacidad: null , asociadoSistema: true, grupoLDAP: "SWF_DTI_CARRIERS_INTERCONECTANTES_PROVEEDOR", manoObraPropia: false).save(failOnError: true)
		new SoftwareFactory(descripcion: "SWF_DTI_CARRIERS_INTERCONECTANTES_TELECOM", gestionaCapacidad: false, responsableCargaCapacidad: null , asociadoSistema: true, grupoLDAP: "SWF_DTI_CARRIERS_INTERCONECTANTES_TELECOM", manoObraPropia: true).save(failOnError: true)
		new SoftwareFactory(descripcion: "SWF_DTI_CATALOGO_PYS_PROVEEDOR", gestionaCapacidad: false, responsableCargaCapacidad: null , asociadoSistema: true, grupoLDAP: "SWF_DTI_CATALOGO_PYS_PROVEEDOR", manoObraPropia: false).save(failOnError: true)
		new SoftwareFactory(descripcion: "SWF_DTI_CATALOGO_PYS_TELECOM", gestionaCapacidad: false, responsableCargaCapacidad: null , asociadoSistema: true, grupoLDAP: "SWF_DTI_CATALOGO_PYS_TELECOM", manoObraPropia: true).save(failOnError: true)
		new SoftwareFactory(descripcion: "SWF_DTI_CCIP_PROVEEDOR", gestionaCapacidad: false, responsableCargaCapacidad: null , asociadoSistema: true, grupoLDAP: "SWF_DTI_CCIP_PROVEEDOR", manoObraPropia: false).save(failOnError: true)
		new SoftwareFactory(descripcion: "SWF_DTI_CCIP_TELECOM", gestionaCapacidad: false, responsableCargaCapacidad: null , asociadoSistema: true, grupoLDAP: "SWF_DTI_CCIP_TELECOM", manoObraPropia: true).save(failOnError: true)
		new SoftwareFactory(descripcion: "SWF_DTI_CCTBI_PROVEEDOR", gestionaCapacidad: false, responsableCargaCapacidad: null , asociadoSistema: true, grupoLDAP: "SWF_DTI_CCTBI_PROVEEDOR", manoObraPropia: false).save(failOnError: true)
		new SoftwareFactory(descripcion: "SWF_DTI_CCTBI_TELECOM", gestionaCapacidad: false, responsableCargaCapacidad: null , asociadoSistema: true, grupoLDAP: "SWF_DTI_CCTBI_TELECOM", manoObraPropia: true).save(failOnError: true)
		new SoftwareFactory(descripcion: "SWF_DTI_CENTRO_VALIDADOR_TELECOM", gestionaCapacidad: false, responsableCargaCapacidad: null , asociadoSistema: true, grupoLDAP: "SWF_DTI_CENTRO_VALIDADOR_TELECOM", manoObraPropia: true).save(failOnError: true)
		new SoftwareFactory(descripcion: "SWF_DTI_CITAS_PROVEEDOR", gestionaCapacidad: false, responsableCargaCapacidad: null , asociadoSistema: true, grupoLDAP: "SWF_DTI_CITAS_PROVEEDOR", manoObraPropia: false).save(failOnError: true)
		new SoftwareFactory(descripcion: "SWF_DTI_CITAS_TELECOM", gestionaCapacidad: false, responsableCargaCapacidad: null , asociadoSistema: true, grupoLDAP: "SWF_DTI_CITAS_TELECOM", manoObraPropia: true).save(failOnError: true)
		new SoftwareFactory(descripcion: "SWF_DTI_CMS_PROVEEDOR", gestionaCapacidad: false, responsableCargaCapacidad: null , asociadoSistema: true, grupoLDAP: "SWF_DTI_CMS_PROVEEDOR", manoObraPropia: false).save(failOnError: true)
		new SoftwareFactory(descripcion: "SWF_DTI_CMS_TELECOM", gestionaCapacidad: false, responsableCargaCapacidad: null , asociadoSistema: true, grupoLDAP: "SWF_DTI_CMS_TELECOM", manoObraPropia: true).save(failOnError: true)
		new SoftwareFactory(descripcion: "SWF_DTI_COBRANZAS_ELECTRONICAS_TELECOM", gestionaCapacidad: false, responsableCargaCapacidad: null , asociadoSistema: true, grupoLDAP: "SWF_DTI_COBRANZAS_ELECTRONICAS_TELECOM", manoObraPropia: true).save(failOnError: true)
		new SoftwareFactory(descripcion: "SWF_DTI_COBRANZAS_INTEGRADAS_TELECOM", gestionaCapacidad: false, responsableCargaCapacidad: null , asociadoSistema: true, grupoLDAP: "SWF_DTI_COBRANZAS_INTEGRADAS_TELECOM", manoObraPropia: true).save(failOnError: true)
		new SoftwareFactory(descripcion: "SWF_DTI_CRIS_PROVEEDOR", gestionaCapacidad: false, responsableCargaCapacidad: null , asociadoSistema: true, grupoLDAP: "SWF_DTI_CRIS_PROVEEDOR", manoObraPropia: false).save(failOnError: true)
		new SoftwareFactory(descripcion: "SWF_DTI_CRIS_TELECOM", gestionaCapacidad: false, responsableCargaCapacidad: null , asociadoSistema: true, grupoLDAP: "SWF_DTI_CRIS_TELECOM", manoObraPropia: true).save(failOnError: true)
		new SoftwareFactory(descripcion: "SWF_DTI_CRM_USE_OYP_PROVEEDOR", gestionaCapacidad: false, responsableCargaCapacidad: null , asociadoSistema: true, grupoLDAP: "SWF_DTI_CRM_USE_OYP_PROVEEDOR", manoObraPropia: false).save(failOnError: true)
		new SoftwareFactory(descripcion: "SWF_DTI_CRM_USE_OYP_TELECOM", gestionaCapacidad: false, responsableCargaCapacidad: null , asociadoSistema: true, grupoLDAP: "SWF_DTI_CRM_USE_OYP_TELECOM", manoObraPropia: true).save(failOnError: true)
		new SoftwareFactory(descripcion: "SWF_DTI_CUENTA_ON_LINE_GC_TELECOM", gestionaCapacidad: false, responsableCargaCapacidad: null , asociadoSistema: true, grupoLDAP: "SWF_DTI_CUENTA_ON_LINE_GC_TELECOM", manoObraPropia: true).save(failOnError: true)
		new SoftwareFactory(descripcion: "SWF_DTI_CUENTA_ON_LINE_PYMES_TELECOM", gestionaCapacidad: false, responsableCargaCapacidad: null , asociadoSistema: true, grupoLDAP: "SWF_DTI_CUENTA_ON_LINE_PYMES_TELECOM", manoObraPropia: true).save(failOnError: true)
		new SoftwareFactory(descripcion: "SWF_DTI_DA_VINCI_GESTOR_DE_COMISIONES_PROVEEDOR", gestionaCapacidad: false, responsableCargaCapacidad: null , asociadoSistema: true, grupoLDAP: "SWF_DTI_DA_VINCI_GESTOR_DE_COMISIONES_PROVEEDOR", manoObraPropia: false).save(failOnError: true)
		new SoftwareFactory(descripcion: "SWF_DTI_DA_VINCI_GESTOR_DE_COMISIONES_TELECOM", gestionaCapacidad: false, responsableCargaCapacidad: null , asociadoSistema: true, grupoLDAP: "SWF_DTI_DA_VINCI_GESTOR_DE_COMISIONES_TELECOM", manoObraPropia: true).save(failOnError: true)
		new SoftwareFactory(descripcion: "SWF_DTI_DACOTA_PROVEEDOR", gestionaCapacidad: false, responsableCargaCapacidad: null , asociadoSistema: true, grupoLDAP: "SWF_DTI_DACOTA_PROVEEDOR", manoObraPropia: false).save(failOnError: true)
		new SoftwareFactory(descripcion: "SWF_DTI_DACOTA_TELECOM", gestionaCapacidad: false, responsableCargaCapacidad: null , asociadoSistema: true, grupoLDAP: "SWF_DTI_DACOTA_TELECOM", manoObraPropia: true).save(failOnError: true)
		new SoftwareFactory(descripcion: "SWF_DTI_DEIMOS_PROVEEDOR", gestionaCapacidad: false, responsableCargaCapacidad: null , asociadoSistema: true, grupoLDAP: "SWF_DTI_DEIMOS_PROVEEDOR", manoObraPropia: false).save(failOnError: true)
		new SoftwareFactory(descripcion: "SWF_DTI_DEIMOS_TELECOM", gestionaCapacidad: false, responsableCargaCapacidad: null , asociadoSistema: true, grupoLDAP: "SWF_DTI_DEIMOS_TELECOM", manoObraPropia: true).save(failOnError: true)
		new SoftwareFactory(descripcion: "SWF_DTI_DELTA_ACCENTUR_PROVEEDOR", gestionaCapacidad: false, responsableCargaCapacidad: null , asociadoSistema: true, grupoLDAP: "SWF_DTI_DELTA_ACCENTUR_PROVEEDOR", manoObraPropia: false).save(failOnError: true)
		new SoftwareFactory(descripcion: "SWF_DTI_DELTA_INDRA_PROVEEDOR", gestionaCapacidad: false, responsableCargaCapacidad: null , asociadoSistema: true, grupoLDAP: "SWF_DTI_DELTA_INDRA_PROVEEDOR", manoObraPropia: false).save(failOnError: true)
		new SoftwareFactory(descripcion: "SWF_DTI_DELTA_SOFRECOM_PROVEEDOR", gestionaCapacidad: false, responsableCargaCapacidad: null , asociadoSistema: true, grupoLDAP: "SWF_DTI_DELTA_SOFRECOM_PROVEEDOR", manoObraPropia: false).save(failOnError: true)
		new SoftwareFactory(descripcion: "SWF_DTI_DELTA_TELECOM", gestionaCapacidad: false, responsableCargaCapacidad: null , asociadoSistema: true, grupoLDAP: "SWF_DTI_DELTA_TELECOM", manoObraPropia: true).save(failOnError: true)
		new SoftwareFactory(descripcion: "SWF_DTI_DETERMINATES_DE_TASACION_ACCENTURE_PROVEEDOR", gestionaCapacidad: false, responsableCargaCapacidad: null , asociadoSistema: true, grupoLDAP: "SWF_DTI_DETERMINATES_DE_TASACION_ACCENTURE_PROVEEDOR", manoObraPropia: false).save(failOnError: true)
		new SoftwareFactory(descripcion: "SWF_DTI_DETERMINATES_DE_TASACION_SOFRECOM_PROVEEDOR", gestionaCapacidad: false, responsableCargaCapacidad: null , asociadoSistema: true, grupoLDAP: "SWF_DTI_DETERMINATES_DE_TASACION_SOFRECOM_PROVEEDOR", manoObraPropia: false).save(failOnError: true)
		new SoftwareFactory(descripcion: "SWF_DTI_DETERMINATES_DE_TASACION_TELECOM", gestionaCapacidad: false, responsableCargaCapacidad: null , asociadoSistema: true, grupoLDAP: "SWF_DTI_DETERMINATES_DE_TASACION_TELECOM", manoObraPropia: true).save(failOnError: true)
		new SoftwareFactory(descripcion: "SWF_DTI_DIARIA_.NET_PROVEEDOR", gestionaCapacidad: false, responsableCargaCapacidad: null , asociadoSistema: true, grupoLDAP: "SWF_DTI_DIARIA_.NET_PROVEEDOR", manoObraPropia: false).save(failOnError: true)
		new SoftwareFactory(descripcion: "SWF_DTI_DIARIA_.NET_TELECOM", gestionaCapacidad: false, responsableCargaCapacidad: null , asociadoSistema: true, grupoLDAP: "SWF_DTI_DIARIA_.NET_TELECOM", manoObraPropia: true).save(failOnError: true)
		new SoftwareFactory(descripcion: "SWF_DTI_DRAGON_TELECOM", gestionaCapacidad: false, responsableCargaCapacidad: null , asociadoSistema: true, grupoLDAP: "SWF_DTI_DRAGON_TELECOM", manoObraPropia: true).save(failOnError: true)
		new SoftwareFactory(descripcion: "SWF_DTI_E-MAILING_PROVEEDOR", gestionaCapacidad: true, responsableCargaCapacidad: "RSWF_DTI_E-MAILING" , asociadoSistema: true, grupoLDAP: "SWF_DTI_E-MAILING_PROVEEDOR", manoObraPropia: false).save(failOnError: true)
		new SoftwareFactory(descripcion: "SWF_DTI_E-MAILING_TELECOM", gestionaCapacidad: false, responsableCargaCapacidad: null , asociadoSistema: true, grupoLDAP: "SWF_DTI_E-MAILING_TELECOM", manoObraPropia: true).save(failOnError: true)
		new SoftwareFactory(descripcion: "SWF_DTI_ESTADISTICA_0800/0810_TELECOM", gestionaCapacidad: false, responsableCargaCapacidad: null , asociadoSistema: true, grupoLDAP: "SWF_DTI_ESTADISTICA_0800/0810_TELECOM", manoObraPropia: true).save(failOnError: true)
		new SoftwareFactory(descripcion: "SWF_DTI_EVA_CUENTAS_CORRIENTES_MASIVO_TELECOM", gestionaCapacidad: false, responsableCargaCapacidad: null , asociadoSistema: true, grupoLDAP: "SWF_DTI_EVA_CUENTAS_CORRIENTES_MASIVO_TELECOM", manoObraPropia: true).save(failOnError: true)
		new SoftwareFactory(descripcion: "SWF_DTI_EVA_DATAIQ_PROVEEDOR", gestionaCapacidad: false, responsableCargaCapacidad: null , asociadoSistema: true, grupoLDAP: "SWF_DTI_EVA_DATAIQ_PROVEEDOR", manoObraPropia: false).save(failOnError: true)
		new SoftwareFactory(descripcion: "SWF_DTI_EVA_INDRA_PROVEEDOR", gestionaCapacidad: false, responsableCargaCapacidad: null , asociadoSistema: true, grupoLDAP: "SWF_DTI_EVA_INDRA_PROVEEDOR", manoObraPropia: false).save(failOnError: true)
		new SoftwareFactory(descripcion: "SWF_DTI_EVA_LAGASH_PROVEEDOR", gestionaCapacidad: false, responsableCargaCapacidad: null , asociadoSistema: true, grupoLDAP: "SWF_DTI_EVA_LAGASH_PROVEEDOR", manoObraPropia: false).save(failOnError: true)
		new SoftwareFactory(descripcion: "SWF_DTI_EVA_MOROSIDAD/COBRANZAS_GC/WS_TELECOM", gestionaCapacidad: false, responsableCargaCapacidad: null , asociadoSistema: true, grupoLDAP: "SWF_DTI_EVA_MOROSIDAD/COBRANZAS_GC/WS_TELECOM", manoObraPropia: true).save(failOnError: true)
		new SoftwareFactory(descripcion: "SWF_DTI_EVA_MOROSIDAD/FINANCIACION_MASIVO_TELECOM", gestionaCapacidad: false, responsableCargaCapacidad: null , asociadoSistema: true, grupoLDAP: "SWF_DTI_EVA_MOROSIDAD/FINANCIACION_MASIVO_TELECOM", manoObraPropia: true).save(failOnError: true)
		new SoftwareFactory(descripcion: "SWF_DTI_EVA_POSTBAJA/COMPUTO_IMPOSITIVO_TELECOM", gestionaCapacidad: false, responsableCargaCapacidad: null , asociadoSistema: true, grupoLDAP: "SWF_DTI_EVA_POSTBAJA/COMPUTO_IMPOSITIVO_TELECOM", manoObraPropia: true).save(failOnError: true)
		new SoftwareFactory(descripcion: "SWF_DTI_EVA_SOFRECOM_PROVEEDOR", gestionaCapacidad: false, responsableCargaCapacidad: null , asociadoSistema: true, grupoLDAP: "SWF_DTI_EVA_SOFRECOM_PROVEEDOR", manoObraPropia: false).save(failOnError: true)
		new SoftwareFactory(descripcion: "SWF_DTI_FACTESP_PROVEEDOR", gestionaCapacidad: true, responsableCargaCapacidad: "RSWF_DTI_FACTESP" , asociadoSistema: true, grupoLDAP: "SWF_DTI_FACTESP_PROVEEDOR", manoObraPropia: false).save(failOnError: true)
		new SoftwareFactory(descripcion: "SWF_DTI_FACTESP_TELECOM", gestionaCapacidad: false, responsableCargaCapacidad: null , asociadoSistema: true, grupoLDAP: "SWF_DTI_FACTESP_TELECOM", manoObraPropia: true).save(failOnError: true)
		new SoftwareFactory(descripcion: "SWF_DTI_FAX_SERVER_PROVEEDOR", gestionaCapacidad: false, responsableCargaCapacidad: null , asociadoSistema: true, grupoLDAP: "SWF_DTI_FAX_SERVER_PROVEEDOR", manoObraPropia: false).save(failOnError: true)
		new SoftwareFactory(descripcion: "SWF_DTI_FOSSE_PROVEEDOR", gestionaCapacidad: true, responsableCargaCapacidad: "RSWF_DTI_FOSSE" , asociadoSistema: true, grupoLDAP: "SWF_DTI_FOSSE_PROVEEDOR", manoObraPropia: false).save(failOnError: true)
		new SoftwareFactory(descripcion: "SWF_DTI_FOSSE_TELECOM", gestionaCapacidad: false, responsableCargaCapacidad: null , asociadoSistema: true, grupoLDAP: "SWF_DTI_FOSSE_TELECOM", manoObraPropia: true).save(failOnError: true)
		new SoftwareFactory(descripcion: "SWF_DTI_FRONTBI_PROVEEDOR", gestionaCapacidad: false, responsableCargaCapacidad: null , asociadoSistema: true, grupoLDAP: "SWF_DTI_FRONTBI_PROVEEDOR", manoObraPropia: false).save(failOnError: true)
		new SoftwareFactory(descripcion: "SWF_DTI_FRONTBI_TELECOM", gestionaCapacidad: false, responsableCargaCapacidad: null , asociadoSistema: true, grupoLDAP: "SWF_DTI_FRONTBI_TELECOM", manoObraPropia: true).save(failOnError: true)
		new SoftwareFactory(descripcion: "SWF_DTI_GENESIS_BILL_CYCLE_PROVEEDOR", gestionaCapacidad: true, responsableCargaCapacidad: "RSWF_DTI_GENESIS_BILL_CYCLE" , asociadoSistema: true, grupoLDAP: "SWF_DTI_GENESIS_BILL_CYCLE_PROVEEDOR", manoObraPropia: false).save(failOnError: true)
		new SoftwareFactory(descripcion: "SWF_DTI_GENESIS_BILL_CYCLE_TELECOM", gestionaCapacidad: false, responsableCargaCapacidad: null , asociadoSistema: true, grupoLDAP: "SWF_DTI_GENESIS_BILL_CYCLE_TELECOM", manoObraPropia: true).save(failOnError: true)
		new SoftwareFactory(descripcion: "SWF_DTI_GENESIS_CUSTOMER_PROVEEDOR", gestionaCapacidad: true, responsableCargaCapacidad: "RSWF_DTI_GENESIS_CUSTOMER" , asociadoSistema: true, grupoLDAP: "SWF_DTI_GENESIS_CUSTOMER_PROVEEDOR", manoObraPropia: false).save(failOnError: true)
		new SoftwareFactory(descripcion: "SWF_DTI_GENESIS_CUSTOMER_TELECOM", gestionaCapacidad: false, responsableCargaCapacidad: null , asociadoSistema: true, grupoLDAP: "SWF_DTI_GENESIS_CUSTOMER_TELECOM", manoObraPropia: true).save(failOnError: true)
		new SoftwareFactory(descripcion: "SWF_DTI_GENESIS_MIC_PROVEEDOR", gestionaCapacidad: true, responsableCargaCapacidad: "RSWF_DTI_GENESIS_MIC" , asociadoSistema: true, grupoLDAP: "SWF_DTI_GENESIS_MIC_PROVEEDOR", manoObraPropia: false).save(failOnError: true)
		new SoftwareFactory(descripcion: "SWF_DTI_GENESIS_MIC_TELECOM", gestionaCapacidad: false, responsableCargaCapacidad: null , asociadoSistema: true, grupoLDAP: "SWF_DTI_GENESIS_MIC_TELECOM", manoObraPropia: true).save(failOnError: true)
		new SoftwareFactory(descripcion: "SWF_DTI_GENESIS_OYP_PROVEEDOR", gestionaCapacidad: true, responsableCargaCapacidad: "RSWF_DTI_GENESIS_OYP" , asociadoSistema: true, grupoLDAP: "SWF_DTI_GENESIS_OYP_PROVEEDOR", manoObraPropia: false).save(failOnError: true)
		new SoftwareFactory(descripcion: "SWF_DTI_GENESIS_OYP_TELECOM", gestionaCapacidad: false, responsableCargaCapacidad: null , asociadoSistema: true, grupoLDAP: "SWF_DTI_GENESIS_OYP_TELECOM", manoObraPropia: true).save(failOnError: true)
		new SoftwareFactory(descripcion: "SWF_DTI_GENESIS_UPS_PROVEEDOR", gestionaCapacidad: true, responsableCargaCapacidad: "RSWF_DTI_GENESIS_UPS" , asociadoSistema: true, grupoLDAP: "SWF_DTI_GENESIS_UPS_PROVEEDOR", manoObraPropia: false).save(failOnError: true)
		new SoftwareFactory(descripcion: "SWF_DTI_GENESIS_UPS_TELECOM", gestionaCapacidad: false, responsableCargaCapacidad: null , asociadoSistema: true, grupoLDAP: "SWF_DTI_GENESIS_UPS_TELECOM", manoObraPropia: true).save(failOnError: true)
		new SoftwareFactory(descripcion: "SWF_DTI_GESPROD_PROVEEDOR", gestionaCapacidad: false, responsableCargaCapacidad: null , asociadoSistema: true, grupoLDAP: "SWF_DTI_GESPROD_PROVEEDOR", manoObraPropia: false).save(failOnError: true)
		new SoftwareFactory(descripcion: "SWF_DTI_GESPROD_TELECOM", gestionaCapacidad: false, responsableCargaCapacidad: null , asociadoSistema: true, grupoLDAP: "SWF_DTI_GESPROD_TELECOM", manoObraPropia: true).save(failOnError: true)
		new SoftwareFactory(descripcion: "SWF_DTI_GESTION_DE_EMPRENDIMIENTOS_PROVEEDOR", gestionaCapacidad: false, responsableCargaCapacidad: null , asociadoSistema: true, grupoLDAP: "SWF_DTI_GESTION_DE_EMPRENDIMIENTOS_PROVEEDOR", manoObraPropia: false).save(failOnError: true)
		new SoftwareFactory(descripcion: "SWF_DTI_GESTION_DE_EMPRENDIMIENTOS_TELECOM", gestionaCapacidad: false, responsableCargaCapacidad: null , asociadoSistema: true, grupoLDAP: "SWF_DTI_GESTION_DE_EMPRENDIMIENTOS_TELECOM", manoObraPropia: true).save(failOnError: true)
		new SoftwareFactory(descripcion: "SWF_DTI_GIRAFE_PROVEEDOR", gestionaCapacidad: false, responsableCargaCapacidad: null , asociadoSistema: true, grupoLDAP: "SWF_DTI_GIRAFE_PROVEEDOR", manoObraPropia: false).save(failOnError: true)
		new SoftwareFactory(descripcion: "SWF_DTI_GIRAFE_TELECOM", gestionaCapacidad: false, responsableCargaCapacidad: null , asociadoSistema: true, grupoLDAP: "SWF_DTI_GIRAFE_TELECOM", manoObraPropia: true).save(failOnError: true)
		new SoftwareFactory(descripcion: "SWF_DTI_GUC_TELECOM", gestionaCapacidad: false, responsableCargaCapacidad: null , asociadoSistema: true, grupoLDAP: "SWF_DTI_GUC_TELECOM", manoObraPropia: true).save(failOnError: true)
		new SoftwareFactory(descripcion: "SWF_DTI_INTERCONNECT_PROVEEDOR", gestionaCapacidad: true, responsableCargaCapacidad: "RSWF_DTI_INTERCONNECT" , asociadoSistema: true, grupoLDAP: "SWF_DTI_INTERCONNECT_PROVEEDOR", manoObraPropia: false).save(failOnError: true)
		new SoftwareFactory(descripcion: "SWF_DTI_INTERCONNECT_TELECOM", gestionaCapacidad: false, responsableCargaCapacidad: null , asociadoSistema: true, grupoLDAP: "SWF_DTI_INTERCONNECT_TELECOM", manoObraPropia: true).save(failOnError: true)
		new SoftwareFactory(descripcion: "SWF_DTI_IPB_PROVEEDOR", gestionaCapacidad: false, responsableCargaCapacidad: null , asociadoSistema: true, grupoLDAP: "SWF_DTI_IPB_PROVEEDOR", manoObraPropia: false).save(failOnError: true)
		new SoftwareFactory(descripcion: "SWF_DTI_IPB_TELECOM", gestionaCapacidad: false, responsableCargaCapacidad: null , asociadoSistema: true, grupoLDAP: "SWF_DTI_IPB_TELECOM", manoObraPropia: true).save(failOnError: true)
		new SoftwareFactory(descripcion: "SWF_DTI_IUM_TELECOM", gestionaCapacidad: false, responsableCargaCapacidad: null , asociadoSistema: true, grupoLDAP: "SWF_DTI_IUM_TELECOM", manoObraPropia: true).save(failOnError: true)
		new SoftwareFactory(descripcion: "SWF_DTI_JANO_PROVEEDOR", gestionaCapacidad: false, responsableCargaCapacidad: null , asociadoSistema: true, grupoLDAP: "SWF_DTI_JANO_PROVEEDOR", manoObraPropia: false).save(failOnError: true)
		new SoftwareFactory(descripcion: "SWF_DTI_JANO_TELECOM", gestionaCapacidad: false, responsableCargaCapacidad: null , asociadoSistema: true, grupoLDAP: "SWF_DTI_JANO_TELECOM", manoObraPropia: true).save(failOnError: true)
		new SoftwareFactory(descripcion: "SWF_DTI_KINGDOM_TELECOM", gestionaCapacidad: false, responsableCargaCapacidad: null , asociadoSistema: true, grupoLDAP: "SWF_DTI_KINGDOM_TELECOM", manoObraPropia: true).save(failOnError: true)
		new SoftwareFactory(descripcion: "SWF_DTI_MAC_PROVEEDOR", gestionaCapacidad: false, responsableCargaCapacidad: null , asociadoSistema: true, grupoLDAP: "SWF_DTI_MAC_PROVEEDOR", manoObraPropia: false).save(failOnError: true)
		new SoftwareFactory(descripcion: "SWF_DTI_MAC_TELECOM", gestionaCapacidad: false, responsableCargaCapacidad: null , asociadoSistema: true, grupoLDAP: "SWF_DTI_MAC_TELECOM", manoObraPropia: true).save(failOnError: true)
		new SoftwareFactory(descripcion: "SWF_DTI_MICUENTA@_TELECOM", gestionaCapacidad: false, responsableCargaCapacidad: null , asociadoSistema: true, grupoLDAP: "SWF_DTI_MICUENTA@_TELECOM", manoObraPropia: true).save(failOnError: true)
		new SoftwareFactory(descripcion: "SWF_DTI_NBA_PROVEEDOR", gestionaCapacidad: true, responsableCargaCapacidad: "RSWF_DTI_NBA" , asociadoSistema: true, grupoLDAP: "SWF_DTI_NBA_PROVEEDOR", manoObraPropia: false).save(failOnError: true)
		new SoftwareFactory(descripcion: "SWF_DTI_NBA_TELECOM", gestionaCapacidad: false, responsableCargaCapacidad: null , asociadoSistema: true, grupoLDAP: "SWF_DTI_NBA_TELECOM", manoObraPropia: true).save(failOnError: true)
		new SoftwareFactory(descripcion: "SWF_DTI_ODS_PROVEEDOR", gestionaCapacidad: false, responsableCargaCapacidad: null , asociadoSistema: true, grupoLDAP: "SWF_DTI_ODS_PROVEEDOR", manoObraPropia: false).save(failOnError: true)
		new SoftwareFactory(descripcion: "SWF_DTI_ODS_TELECOM", gestionaCapacidad: false, responsableCargaCapacidad: null , asociadoSistema: true, grupoLDAP: "SWF_DTI_ODS_TELECOM", manoObraPropia: true).save(failOnError: true)
		new SoftwareFactory(descripcion: "SWF_DTI_PORTAL BI UTF_PROVEEDOR", gestionaCapacidad: false, responsableCargaCapacidad: null , asociadoSistema: true, grupoLDAP: "SWF_DTI_PORTAL BI UTF_PROVEEDOR", manoObraPropia: false).save(failOnError: true)
		new SoftwareFactory(descripcion: "SWF_DTI_PORTAL BI UTF_TELECOM", gestionaCapacidad: false, responsableCargaCapacidad: null , asociadoSistema: true, grupoLDAP: "SWF_DTI_PORTAL BI UTF_TELECOM", manoObraPropia: true).save(failOnError: true)
		new SoftwareFactory(descripcion: "SWF_DTI_PYO_PROVEEDOR", gestionaCapacidad: true, responsableCargaCapacidad: "RSWF_DTI_PYO" , asociadoSistema: true, grupoLDAP: "SWF_DTI_PYO_PROVEEDOR", manoObraPropia: false).save(failOnError: true)
		new SoftwareFactory(descripcion: "SWF_DTI_PYO_TELECOM", gestionaCapacidad: false, responsableCargaCapacidad: null , asociadoSistema: true, grupoLDAP: "SWF_DTI_PYO_TELECOM", manoObraPropia: true).save(failOnError: true)
		new SoftwareFactory(descripcion: "SWF_DTI_SCARD_COLD_PROVEEDOR", gestionaCapacidad: false, responsableCargaCapacidad: null , asociadoSistema: true, grupoLDAP: "SWF_DTI_SCARD_COLD_PROVEEDOR", manoObraPropia: false).save(failOnError: true)
		new SoftwareFactory(descripcion: "SWF_DTI_SCARD_COLD_TELECOM", gestionaCapacidad: false, responsableCargaCapacidad: null , asociadoSistema: true, grupoLDAP: "SWF_DTI_SCARD_COLD_TELECOM", manoObraPropia: true).save(failOnError: true)
		new SoftwareFactory(descripcion: "SWF_DTI_SEI_TELECOM", gestionaCapacidad: false, responsableCargaCapacidad: null , asociadoSistema: true, grupoLDAP: "SWF_DTI_SEI_TELECOM", manoObraPropia: true).save(failOnError: true)
		new SoftwareFactory(descripcion: "SWF_DTI_SGCT_PROVEEDOR", gestionaCapacidad: false, responsableCargaCapacidad: null , asociadoSistema: true, grupoLDAP: "SWF_DTI_SGCT_PROVEEDOR", manoObraPropia: false).save(failOnError: true)
		new SoftwareFactory(descripcion: "SWF_DTI_SGCT_TELECOM", gestionaCapacidad: false, responsableCargaCapacidad: null , asociadoSistema: true, grupoLDAP: "SWF_DTI_SGCT_TELECOM", manoObraPropia: true).save(failOnError: true)
		new SoftwareFactory(descripcion: "SWF_DTI_SIEBEL_PROVEEDOR", gestionaCapacidad: false, responsableCargaCapacidad: null , asociadoSistema: true, grupoLDAP: "SWF_DTI_SIEBEL_PROVEEDOR", manoObraPropia: false).save(failOnError: true)
		new SoftwareFactory(descripcion: "SWF_DTI_SIEBEL_TELECOM", gestionaCapacidad: false, responsableCargaCapacidad: null , asociadoSistema: true, grupoLDAP: "SWF_DTI_SIEBEL_TELECOM", manoObraPropia: true).save(failOnError: true)
		new SoftwareFactory(descripcion: "SWF_DTI_SIR_PROVEEDOR", gestionaCapacidad: false, responsableCargaCapacidad: null , asociadoSistema: true, grupoLDAP: "SWF_DTI_SIR_PROVEEDOR", manoObraPropia: false).save(failOnError: true)
		new SoftwareFactory(descripcion: "SWF_DTI_SIR_TELECOM", gestionaCapacidad: false, responsableCargaCapacidad: null , asociadoSistema: true, grupoLDAP: "SWF_DTI_SIR_TELECOM", manoObraPropia: true).save(failOnError: true)
		new SoftwareFactory(descripcion: "SWF_DTI_SMART_PROVEEDOR", gestionaCapacidad: false, responsableCargaCapacidad: null , asociadoSistema: true, grupoLDAP: "SWF_DTI_SMART_PROVEEDOR", manoObraPropia: false).save(failOnError: true)
		new SoftwareFactory(descripcion: "SWF_DTI_SMART_TELECOM", gestionaCapacidad: false, responsableCargaCapacidad: null , asociadoSistema: true, grupoLDAP: "SWF_DTI_SMART_TELECOM", manoObraPropia: true).save(failOnError: true)
		new SoftwareFactory(descripcion: "SWF_DTI_SUC2_PROVEEDOR", gestionaCapacidad: false, responsableCargaCapacidad: null , asociadoSistema: true, grupoLDAP: "SWF_DTI_SUC2_PROVEEDOR", manoObraPropia: false).save(failOnError: true)
		new SoftwareFactory(descripcion: "SWF_DTI_SUC2_TELECOM", gestionaCapacidad: false, responsableCargaCapacidad: null , asociadoSistema: true, grupoLDAP: "SWF_DTI_SUC2_TELECOM", manoObraPropia: true).save(failOnError: true)
		new SoftwareFactory(descripcion: "SWF_DTI_TAB_GO_PROVEEDOR", gestionaCapacidad: false, responsableCargaCapacidad: null , asociadoSistema: true, grupoLDAP: "SWF_DTI_TAB_GO_PROVEEDOR", manoObraPropia: false).save(failOnError: true)
		new SoftwareFactory(descripcion: "SWF_DTI_TAB_GO_TELECOM", gestionaCapacidad: false, responsableCargaCapacidad: null , asociadoSistema: true, grupoLDAP: "SWF_DTI_TAB_GO_TELECOM", manoObraPropia: true).save(failOnError: true)
		new SoftwareFactory(descripcion: "SWF_DTI_TAB_RA_PROVEEDOR", gestionaCapacidad: false, responsableCargaCapacidad: null , asociadoSistema: true, grupoLDAP: "SWF_DTI_TAB_RA_PROVEEDOR", manoObraPropia: false).save(failOnError: true)
		new SoftwareFactory(descripcion: "SWF_DTI_TAB_RA_TELECOM", gestionaCapacidad: false, responsableCargaCapacidad: null , asociadoSistema: true, grupoLDAP: "SWF_DTI_TAB_RA_TELECOM", manoObraPropia: true).save(failOnError: true)
		new SoftwareFactory(descripcion: "SWF_DTI_TECO_USA_PREPAGO_PROVEEDOR", gestionaCapacidad: false, responsableCargaCapacidad: null , asociadoSistema: true, grupoLDAP: "SWF_DTI_TECO_USA_PREPAGO_PROVEEDOR", manoObraPropia: false).save(failOnError: true)
		new SoftwareFactory(descripcion: "SWF_DTI_TECO_USA_PREPAGO_TELECOM", gestionaCapacidad: false, responsableCargaCapacidad: null , asociadoSistema: true, grupoLDAP: "SWF_DTI_TECO_USA_PREPAGO_TELECOM", manoObraPropia: true).save(failOnError: true)
		new SoftwareFactory(descripcion: "SWF_DTI_TEMIS_PROVEEDOR", gestionaCapacidad: true, responsableCargaCapacidad: "RSWF_DTI_TEMIS" , asociadoSistema: true, grupoLDAP: "SWF_DTI_TEMIS_PROVEEDOR", manoObraPropia: false).save(failOnError: true)
		new SoftwareFactory(descripcion: "SWF_DTI_TEMIS_TELECOM", gestionaCapacidad: false, responsableCargaCapacidad: null , asociadoSistema: true, grupoLDAP: "SWF_DTI_TEMIS_TELECOM", manoObraPropia: true).save(failOnError: true)
		new SoftwareFactory(descripcion: "SWF_DTI_TRANSFORMER_TELECOM", gestionaCapacidad: false, responsableCargaCapacidad: null , asociadoSistema: true, grupoLDAP: "SWF_DTI_TRANSFORMER_TELECOM", manoObraPropia: true).save(failOnError: true)
		new SoftwareFactory(descripcion: "SWF_DTI_TRUMAN_TELECOM", gestionaCapacidad: false, responsableCargaCapacidad: null , asociadoSistema: true, grupoLDAP: "SWF_DTI_TRUMAN_TELECOM", manoObraPropia: true).save(failOnError: true)
		new SoftwareFactory(descripcion: "SWF_DTI_UTOPIA_PROVEEDOR", gestionaCapacidad: false, responsableCargaCapacidad: null , asociadoSistema: true, grupoLDAP: "SWF_DTI_UTOPIA_PROVEEDOR", manoObraPropia: false).save(failOnError: true)
		new SoftwareFactory(descripcion: "SWF_DTI_UTOPIA_TELECOM", gestionaCapacidad: false, responsableCargaCapacidad: null , asociadoSistema: true, grupoLDAP: "SWF_DTI_UTOPIA_TELECOM", manoObraPropia: true).save(failOnError: true)
		new SoftwareFactory(descripcion: "SWF_DTI_VANTIVE_FIELD_SERVICE_PROVEEDOR", gestionaCapacidad: false, responsableCargaCapacidad: null , asociadoSistema: true, grupoLDAP: "SWF_DTI_VANTIVE_FIELD_SERVICE_PROVEEDOR", manoObraPropia: false).save(failOnError: true)
		new SoftwareFactory(descripcion: "SWF_DTI_VANTIVE_FIELD_SERVICE_TELECOM", gestionaCapacidad: false, responsableCargaCapacidad: null , asociadoSistema: true, grupoLDAP: "SWF_DTI_VANTIVE_FIELD_SERVICE_TELECOM", manoObraPropia: true).save(failOnError: true)
		new SoftwareFactory(descripcion: "SWF_DTI_VENTA ADSL_PROVEEDOR", gestionaCapacidad: false, responsableCargaCapacidad: null , asociadoSistema: true, grupoLDAP: "SWF_DTI_VENTA ADSL_PROVEEDOR", manoObraPropia: false).save(failOnError: true)
		new SoftwareFactory(descripcion: "SWF_DTI_VENTA ADSL_TELECOM", gestionaCapacidad: false, responsableCargaCapacidad: null , asociadoSistema: true, grupoLDAP: "SWF_DTI_VENTA ADSL_TELECOM", manoObraPropia: true).save(failOnError: true)
		new SoftwareFactory(descripcion: "SWF_DTI_Venta On line_TELECOM", gestionaCapacidad: false, responsableCargaCapacidad: null , asociadoSistema: true, grupoLDAP: "SWF_DTI_Venta On line_TELECOM", manoObraPropia: true).save(failOnError: true)
		new SoftwareFactory(descripcion: "SWF_DTI_WORKFLOW_TECNICO_PROVEEDOR", gestionaCapacidad: false, responsableCargaCapacidad: null , asociadoSistema: true, grupoLDAP: "SWF_DTI_WORKFLOW_TECNICO_PROVEEDOR", manoObraPropia: false).save(failOnError: true)
		new SoftwareFactory(descripcion: "SWF_DTI_WORKFLOW_TECNICO_TELECOM", gestionaCapacidad: false, responsableCargaCapacidad: null , asociadoSistema: true, grupoLDAP: "SWF_DTI_WORKFLOW_TECNICO_TELECOM", manoObraPropia: true).save(failOnError: true)
		new SoftwareFactory(descripcion: "SWF_DTI_ZOOM_PROVEEDOR", gestionaCapacidad: false, responsableCargaCapacidad: null , asociadoSistema: true, grupoLDAP: "SWF_DTI_ZOOM_PROVEEDOR", manoObraPropia: false).save(failOnError: true)
		new SoftwareFactory(descripcion: "SWF_DTI_ZOOM_TELECOM", gestionaCapacidad: false, responsableCargaCapacidad: null , asociadoSistema: true, grupoLDAP: "SWF_DTI_ZOOM_TELECOM", manoObraPropia: true).save(failOnError: true)
		new SoftwareFactory(descripcion: "SWF_PMO_EPM IT_PROVEEDOR", gestionaCapacidad: false, responsableCargaCapacidad: null , asociadoSistema: true, grupoLDAP: "SWF_PMO_EPM IT_PROVEEDOR", manoObraPropia: false).save(failOnError: true)
		new SoftwareFactory(descripcion: "SWF_SC_APLRRHH_TELECOM", gestionaCapacidad: false, responsableCargaCapacidad: null , asociadoSistema: true, grupoLDAP: "SWF_SC_APLRRHH_TELECOM", manoObraPropia: true).save(failOnError: true)
		new SoftwareFactory(descripcion: "SWF_SC_ARIS_PROVEEDOR", gestionaCapacidad: false, responsableCargaCapacidad: null , asociadoSistema: true, grupoLDAP: "SWF_SC_ARIS_PROVEEDOR", manoObraPropia: false).save(failOnError: true)
		new SoftwareFactory(descripcion: "SWF_SC_ASSET_CENTER_PROVEEDOR", gestionaCapacidad: false, responsableCargaCapacidad: null , asociadoSistema: true, grupoLDAP: "SWF_SC_ASSET_CENTER_PROVEEDOR", manoObraPropia: false).save(failOnError: true)
		new SoftwareFactory(descripcion: "SWF_SC_ASSET_CENTER_TELECOM", gestionaCapacidad: false, responsableCargaCapacidad: null , asociadoSistema: true, grupoLDAP: "SWF_SC_ASSET_CENTER_TELECOM", manoObraPropia: true).save(failOnError: true)
		new SoftwareFactory(descripcion: "SWF_SC_DENACERT_PROVEEDOR", gestionaCapacidad: false, responsableCargaCapacidad: null , asociadoSistema: true, grupoLDAP: "SWF_SC_DENACERT_PROVEEDOR", manoObraPropia: false).save(failOnError: true)
		new SoftwareFactory(descripcion: "SWF_SC_F_SIGNO_ PMA_PROVEEDOR", gestionaCapacidad: false, responsableCargaCapacidad: null , asociadoSistema: true, grupoLDAP: "SWF_SC_F_SIGNO_ PMA_PROVEEDOR", manoObraPropia: false).save(failOnError: true)
		new SoftwareFactory(descripcion: "SWF_SC_F_SIGNO_ PMA_TELECOM", gestionaCapacidad: false, responsableCargaCapacidad: null , asociadoSistema: true, grupoLDAP: "SWF_SC_F_SIGNO_ PMA_TELECOM", manoObraPropia: true).save(failOnError: true)
		new SoftwareFactory(descripcion: "SWF_SC_F_SIGNO_ABAP_PROVEEDOR", gestionaCapacidad: false, responsableCargaCapacidad: null , asociadoSistema: true, grupoLDAP: "SWF_SC_F_SIGNO_ABAP_PROVEEDOR", manoObraPropia: false).save(failOnError: true)
		new SoftwareFactory(descripcion: "SWF_SC_F_SIGNO_DESARROLLO_TELECOM", gestionaCapacidad: false, responsableCargaCapacidad: null , asociadoSistema: true, grupoLDAP: "SWF_SC_F_SIGNO_DESARROLLO_TELECOM", manoObraPropia: true).save(failOnError: true)
		new SoftwareFactory(descripcion: "SWF_SC_F_SIGNO_FIN_PROVEEDOR", gestionaCapacidad: false, responsableCargaCapacidad: null , asociadoSistema: true, grupoLDAP: "SWF_SC_F_SIGNO_FIN_PROVEEDOR", manoObraPropia: false).save(failOnError: true)
		new SoftwareFactory(descripcion: "SWF_SC_F_SIGNO_FIN_TELECOM", gestionaCapacidad: false, responsableCargaCapacidad: null , asociadoSistema: true, grupoLDAP: "SWF_SC_F_SIGNO_FIN_TELECOM", manoObraPropia: true).save(failOnError: true)
		new SoftwareFactory(descripcion: "SWF_SC_F_SIGNO_GRAILSJAVA_PROVEEDOR", gestionaCapacidad: false, responsableCargaCapacidad: null , asociadoSistema: true, grupoLDAP: "SWF_SC_F_SIGNO_GRAILSJAVA_PROVEEDOR", manoObraPropia: false).save(failOnError: true)
		new SoftwareFactory(descripcion: "SWF_SC_F_SIGNO_GRAILSJAVA_TELECOM", gestionaCapacidad: false, responsableCargaCapacidad: null , asociadoSistema: true, grupoLDAP: "SWF_SC_F_SIGNO_GRAILSJAVA_TELECOM", manoObraPropia: true).save(failOnError: true)
		new SoftwareFactory(descripcion: "SWF_SC_F_SIGNO_INTRANET_PROVEEDOR", gestionaCapacidad: false, responsableCargaCapacidad: null , asociadoSistema: true, grupoLDAP: "SWF_SC_F_SIGNO_INTRANET_PROVEEDOR", manoObraPropia: false).save(failOnError: true)
		new SoftwareFactory(descripcion: "SWF_SC_F_SIGNO_MM_PROVEEDOR", gestionaCapacidad: false, responsableCargaCapacidad: null , asociadoSistema: true, grupoLDAP: "SWF_SC_F_SIGNO_MM_PROVEEDOR", manoObraPropia: false).save(failOnError: true)
		new SoftwareFactory(descripcion: "SWF_SC_F_SIGNO_MM_TELECOM", gestionaCapacidad: false, responsableCargaCapacidad: null , asociadoSistema: true, grupoLDAP: "SWF_SC_F_SIGNO_MM_TELECOM", manoObraPropia: true).save(failOnError: true)
		new SoftwareFactory(descripcion: "SWF_SC_F_SIGNO_NWJAVA_PROVEEDOR", gestionaCapacidad: false, responsableCargaCapacidad: null , asociadoSistema: true, grupoLDAP: "SWF_SC_F_SIGNO_NWJAVA_PROVEEDOR", manoObraPropia: false).save(failOnError: true)
		new SoftwareFactory(descripcion: "SWF_SC_F_SIGNO_OPENTEXT_PROVEEDOR", gestionaCapacidad: false, responsableCargaCapacidad: null , asociadoSistema: true, grupoLDAP: "SWF_SC_F_SIGNO_OPENTEXT_PROVEEDOR", manoObraPropia: false).save(failOnError: true)
		new SoftwareFactory(descripcion: "SWF_SC_F_SIGNO_SAP_PI_PROVEEDOR", gestionaCapacidad: false, responsableCargaCapacidad: null , asociadoSistema: true, grupoLDAP: "SWF_SC_F_SIGNO_SAP_PI_PROVEEDOR", manoObraPropia: false).save(failOnError: true)
		new SoftwareFactory(descripcion: "SWF_SC_GESTION DE LITIGIOS_PROVEEDOR", gestionaCapacidad: false, responsableCargaCapacidad: null , asociadoSistema: true, grupoLDAP: "SWF_SC_GESTION DE LITIGIOS_PROVEEDOR", manoObraPropia: false).save(failOnError: true)
		new SoftwareFactory(descripcion: "SWF_SC_NEOPORTAL _PROVEEDOR", gestionaCapacidad: false, responsableCargaCapacidad: null , asociadoSistema: true, grupoLDAP: "SWF_SC_NEOPORTAL _PROVEEDOR", manoObraPropia: false).save(failOnError: true)
		new SoftwareFactory(descripcion: "SWF_SC_NEOPORTAL PPPIO_TELECOM", gestionaCapacidad: false, responsableCargaCapacidad: null , asociadoSistema: true, grupoLDAP: "SWF_SC_NEOPORTAL PPPIO_TELECOM", manoObraPropia: true).save(failOnError: true)
		new SoftwareFactory(descripcion: "SWF_SC_QV_CORP_ASAP_COR_PROVEEDOR", gestionaCapacidad: false, responsableCargaCapacidad: null , asociadoSistema: true, grupoLDAP: "SWF_SC_QV_CORP_ASAP_COR_PROVEEDOR", manoObraPropia: false).save(failOnError: true)
		new SoftwareFactory(descripcion: "SWF_SC_QV_CORP_DATA IQ_PROVEEDOR", gestionaCapacidad: false, responsableCargaCapacidad: null , asociadoSistema: true, grupoLDAP: "SWF_SC_QV_CORP_DATA IQ_PROVEEDOR", manoObraPropia: false).save(failOnError: true)
		new SoftwareFactory(descripcion: "SWF_SC_QV_CORP_TELECOM", gestionaCapacidad: false, responsableCargaCapacidad: null , asociadoSistema: true, grupoLDAP: "SWF_SC_QV_CORP_TELECOM", manoObraPropia: true).save(failOnError: true)
		new SoftwareFactory(descripcion: "SWF_SC_SHAREPOINT_PROVEEDOR", gestionaCapacidad: false, responsableCargaCapacidad: null , asociadoSistema: true, grupoLDAP: "SWF_SC_SHAREPOINT_PROVEEDOR", manoObraPropia: false).save(failOnError: true)
		new SoftwareFactory(descripcion: "SWF_SC_SIMPLIT_PROVEEDOR", gestionaCapacidad: false, responsableCargaCapacidad: null , asociadoSistema: true, grupoLDAP: "SWF_SC_SIMPLIT_PROVEEDOR", manoObraPropia: false).save(failOnError: true)
		new SoftwareFactory(descripcion: "SWF_SC_SIMPLIT_TELECOM", gestionaCapacidad: false, responsableCargaCapacidad: null , asociadoSistema: true, grupoLDAP: "SWF_SC_SIMPLIT_TELECOM", manoObraPropia: true).save(failOnError: true)
		new SoftwareFactory(descripcion: "SWF_SC_VECONP_PROVEEDOR", gestionaCapacidad: false, responsableCargaCapacidad: null , asociadoSistema: true, grupoLDAP: "SWF_SC_VECONP_PROVEEDOR", manoObraPropia: false).save(failOnError: true)
		new SoftwareFactory(descripcion: "SWF_ST_ABUSE_SOFRECOM_PROVEEDOR", gestionaCapacidad: false, responsableCargaCapacidad: null , asociadoSistema: true, grupoLDAP: "SWF_ST_ABUSE_SOFRECOM_PROVEEDOR", manoObraPropia: false).save(failOnError: true)
		new SoftwareFactory(descripcion: "SWF_ST_ABUSE_TELECOM", gestionaCapacidad: false, responsableCargaCapacidad: null , asociadoSistema: true, grupoLDAP: "SWF_ST_ABUSE_TELECOM", manoObraPropia: true).save(failOnError: true)
		new SoftwareFactory(descripcion: "SWF_ST_AGILIT_SPC_PROVEEDOR", gestionaCapacidad: false, responsableCargaCapacidad: null , asociadoSistema: true, grupoLDAP: "SWF_ST_AGILIT_SPC_PROVEEDOR", manoObraPropia: false).save(failOnError: true)
		new SoftwareFactory(descripcion: "SWF_ST_AGILIT_TELECOM", gestionaCapacidad: false, responsableCargaCapacidad: null , asociadoSistema: true, grupoLDAP: "SWF_ST_AGILIT_TELECOM", manoObraPropia: true).save(failOnError: true)
		new SoftwareFactory(descripcion: "SWF_ST_BASE_IP_PROVEEDOR", gestionaCapacidad: false, responsableCargaCapacidad: null , asociadoSistema: true, grupoLDAP: "SWF_ST_BASE_IP_PROVEEDOR", manoObraPropia: false).save(failOnError: true)
		new SoftwareFactory(descripcion: "SWF_ST_BASE_IP_TELECOM", gestionaCapacidad: false, responsableCargaCapacidad: null , asociadoSistema: true, grupoLDAP: "SWF_ST_BASE_IP_TELECOM", manoObraPropia: true).save(failOnError: true)
		new SoftwareFactory(descripcion: "SWF_ST_CICLOPE_SPC_PROVEEDOR", gestionaCapacidad: false, responsableCargaCapacidad: null , asociadoSistema: true, grupoLDAP: "SWF_ST_CICLOPE_SPC_PROVEEDOR", manoObraPropia: false).save(failOnError: true)
		new SoftwareFactory(descripcion: "SWF_ST_CICLOPE_TELECOM", gestionaCapacidad: false, responsableCargaCapacidad: null , asociadoSistema: true, grupoLDAP: "SWF_ST_CICLOPE_TELECOM", manoObraPropia: true).save(failOnError: true)
		new SoftwareFactory(descripcion: "SWF_ST_CNM_SPC_PROVEEDOR", gestionaCapacidad: false, responsableCargaCapacidad: null , asociadoSistema: true, grupoLDAP: "SWF_ST_CNM_SPC_PROVEEDOR", manoObraPropia: false).save(failOnError: true)
		new SoftwareFactory(descripcion: "SWF_ST_CNM_TELECOM", gestionaCapacidad: false, responsableCargaCapacidad: null , asociadoSistema: true, grupoLDAP: "SWF_ST_CNM_TELECOM", manoObraPropia: true).save(failOnError: true)
		new SoftwareFactory(descripcion: "SWF_ST_DA_NETCRACKER_PROVEEDOR", gestionaCapacidad: false, responsableCargaCapacidad: null , asociadoSistema: true, grupoLDAP: "SWF_ST_DA_NETCRACKER_PROVEEDOR", manoObraPropia: false).save(failOnError: true)
		new SoftwareFactory(descripcion: "SWF_ST_DA_TELECOM", gestionaCapacidad: false, responsableCargaCapacidad: null , asociadoSistema: true, grupoLDAP: "SWF_ST_DA_TELECOM", manoObraPropia: true).save(failOnError: true)
		new SoftwareFactory(descripcion: "SWF_ST_INVENTARIOS_SPC_PROVEEDOR", gestionaCapacidad: false, responsableCargaCapacidad: null , asociadoSistema: true, grupoLDAP: "SWF_ST_INVENTARIOS_SPC_PROVEEDOR", manoObraPropia: false).save(failOnError: true)
		new SoftwareFactory(descripcion: "SWF_ST_INVENTARIOS_TELECOM", gestionaCapacidad: false, responsableCargaCapacidad: null , asociadoSistema: true, grupoLDAP: "SWF_ST_INVENTARIOS_TELECOM", manoObraPropia: true).save(failOnError: true)
		new SoftwareFactory(descripcion: "SWF_ST_PERSA_PROVEEDOR", gestionaCapacidad: false, responsableCargaCapacidad: null , asociadoSistema: true, grupoLDAP: "SWF_ST_PERSA_PROVEEDOR", manoObraPropia: false).save(failOnError: true)
		new SoftwareFactory(descripcion: "SWF_ST_PERSA_TELECOM", gestionaCapacidad: false, responsableCargaCapacidad: null , asociadoSistema: true, grupoLDAP: "SWF_ST_PERSA_TELECOM", manoObraPropia: true).save(failOnError: true)
		new SoftwareFactory(descripcion: "SWF_ST_PLATAFORMA_PRECALIFICACION_PROVEEDOR", gestionaCapacidad: false, responsableCargaCapacidad: null , asociadoSistema: true, grupoLDAP: "SWF_ST_PLATAFORMA_PRECALIFICACION_PROVEEDOR", manoObraPropia: false).save(failOnError: true)
		new SoftwareFactory(descripcion: "SWF_ST_PLATAFORMA_PRECALIFICACION_TELECOM", gestionaCapacidad: false, responsableCargaCapacidad: null , asociadoSistema: true, grupoLDAP: "SWF_ST_PLATAFORMA_PRECALIFICACION_TELECOM", manoObraPropia: true).save(failOnError: true)
		new SoftwareFactory(descripcion: "SWF_ST_PLATAFORMA_TESTING_PROVEEDOR", gestionaCapacidad: false, responsableCargaCapacidad: null , asociadoSistema: true, grupoLDAP: "SWF_ST_PLATAFORMA_TESTING_PROVEEDOR", manoObraPropia: false).save(failOnError: true)
		new SoftwareFactory(descripcion: "SWF_ST_PLATAFORMA_TESTING_TELECOM", gestionaCapacidad: false, responsableCargaCapacidad: null , asociadoSistema: true, grupoLDAP: "SWF_ST_PLATAFORMA_TESTING_TELECOM", manoObraPropia: true).save(failOnError: true)
		new SoftwareFactory(descripcion: "SWF_ST_PORTAL_CDA_PROVEEDOR", gestionaCapacidad: false, responsableCargaCapacidad: null , asociadoSistema: true, grupoLDAP: "SWF_ST_PORTAL_CDA_PROVEEDOR", manoObraPropia: false).save(failOnError: true)
		new SoftwareFactory(descripcion: "SWF_ST_PORTAL_SOFRECOM_PROVEEDOR", gestionaCapacidad: false, responsableCargaCapacidad: null , asociadoSistema: true, grupoLDAP: "SWF_ST_PORTAL_SOFRECOM_PROVEEDOR", manoObraPropia: false).save(failOnError: true)
		new SoftwareFactory(descripcion: "SWF_ST_PORTAL_TELECOM", gestionaCapacidad: false, responsableCargaCapacidad: null , asociadoSistema: true, grupoLDAP: "SWF_ST_PORTAL_TELECOM", manoObraPropia: true).save(failOnError: true)
		new SoftwareFactory(descripcion: "SWF_ST_QV_SPC_PROVEEDOR", gestionaCapacidad: false, responsableCargaCapacidad: null , asociadoSistema: true, grupoLDAP: "SWF_ST_QV_SPC_PROVEEDOR", manoObraPropia: false).save(failOnError: true)
		new SoftwareFactory(descripcion: "SWF_ST_QV_TELECOM", gestionaCapacidad: false, responsableCargaCapacidad: null , asociadoSistema: true, grupoLDAP: "SWF_ST_QV_TELECOM", manoObraPropia: true).save(failOnError: true)
		new SoftwareFactory(descripcion: "SWF_ST_REMEYRED_SPC_PROVEEDOR", gestionaCapacidad: false, responsableCargaCapacidad: null , asociadoSistema: true, grupoLDAP: "SWF_ST_REMEYRED_SPC_PROVEEDOR", manoObraPropia: false).save(failOnError: true)
		new SoftwareFactory(descripcion: "SWF_ST_REMEYRED_TELECOM", gestionaCapacidad: false, responsableCargaCapacidad: null , asociadoSistema: true, grupoLDAP: "SWF_ST_REMEYRED_TELECOM", manoObraPropia: true).save(failOnError: true)
		new SoftwareFactory(descripcion: "SWF_ST_SEGAT_SPC_PROVEEDOR", gestionaCapacidad: false, responsableCargaCapacidad: null , asociadoSistema: true, grupoLDAP: "SWF_ST_SEGAT_SPC_PROVEEDOR", manoObraPropia: false).save(failOnError: true)
		new SoftwareFactory(descripcion: "SWF_ST_SEGAT_TELECOM", gestionaCapacidad: false, responsableCargaCapacidad: null , asociadoSistema: true, grupoLDAP: "SWF_ST_SEGAT_TELECOM", manoObraPropia: true).save(failOnError: true)
		new SoftwareFactory(descripcion: "SWF_ST_SG_DATOS_PROVEEDOR", gestionaCapacidad: false, responsableCargaCapacidad: null , asociadoSistema: true, grupoLDAP: "SWF_ST_SG_DATOS_PROVEEDOR", manoObraPropia: false).save(failOnError: true)
		new SoftwareFactory(descripcion: "SWF_ST_SG_DATOS_TELECOM", gestionaCapacidad: false, responsableCargaCapacidad: null , asociadoSistema: true, grupoLDAP: "SWF_ST_SG_DATOS_TELECOM", manoObraPropia: true).save(failOnError: true)
		new SoftwareFactory(descripcion: "SWF_ST_SIDERAL_PROVEEDOR", gestionaCapacidad: false, responsableCargaCapacidad: null , asociadoSistema: true, grupoLDAP: "SWF_ST_SIDERAL_PROVEEDOR", manoObraPropia: false).save(failOnError: true)
		new SoftwareFactory(descripcion: "SWF_ST_SIDERAL_TELECOM", gestionaCapacidad: false, responsableCargaCapacidad: null , asociadoSistema: true, grupoLDAP: "SWF_ST_SIDERAL_TELECOM", manoObraPropia: true).save(failOnError: true)
		new SoftwareFactory(descripcion: "SWF_ST_SISTEMA-AFECTACION-SERVICIOS_PROVEEDOR", gestionaCapacidad: false, responsableCargaCapacidad: null , asociadoSistema: true, grupoLDAP: "SWF_ST_SISTEMA-AFECTACION-SERVICIOS_PROVEEDOR", manoObraPropia: false).save(failOnError: true)
		new SoftwareFactory(descripcion: "SWF_ST_SISTEMA-AFECTACION-SERVICIOS_TELECOM", gestionaCapacidad: false, responsableCargaCapacidad: null , asociadoSistema: true, grupoLDAP: "SWF_ST_SISTEMA-AFECTACION-SERVICIOS_TELECOM", manoObraPropia: true).save(failOnError: true)
		new SoftwareFactory(descripcion: "SWF_ST_SUPERVISION-VPNS_PROVEEDOR", gestionaCapacidad: false, responsableCargaCapacidad: null , asociadoSistema: true, grupoLDAP: "SWF_ST_SUPERVISION-VPNS_PROVEEDOR", manoObraPropia: false).save(failOnError: true)
		new SoftwareFactory(descripcion: "SWF_ST_SUPERVISION-VPNS_TELECOM", gestionaCapacidad: false, responsableCargaCapacidad: null , asociadoSistema: true, grupoLDAP: "SWF_ST_SUPERVISION-VPNS_TELECOM", manoObraPropia: true).save(failOnError: true)
		new SoftwareFactory(descripcion: "SWF_ST_SUR_SPC_PROVEEDOR", gestionaCapacidad: false, responsableCargaCapacidad: null , asociadoSistema: true, grupoLDAP: "SWF_ST_SUR_SPC_PROVEEDOR", manoObraPropia: false).save(failOnError: true)
		new SoftwareFactory(descripcion: "SWF_ST_SUR_TELECOM", gestionaCapacidad: false, responsableCargaCapacidad: null , asociadoSistema: true, grupoLDAP: "SWF_ST_SUR_TELECOM", manoObraPropia: true).save(failOnError: true)
		new SoftwareFactory(descripcion: "SWF_ST_SUS_PROVEEDOR", gestionaCapacidad: false, responsableCargaCapacidad: null , asociadoSistema: true, grupoLDAP: "SWF_ST_SUS_PROVEEDOR", manoObraPropia: false).save(failOnError: true)
		new SoftwareFactory(descripcion: "SWF_ST_SUS_SOFRECOM_PROVEEDOR", gestionaCapacidad: false, responsableCargaCapacidad: null , asociadoSistema: true, grupoLDAP: "SWF_ST_SUS_SOFRECOM_PROVEEDOR", manoObraPropia: false).save(failOnError: true)
		new SoftwareFactory(descripcion: "SWF_ST_SUS_TELECOM", gestionaCapacidad: false, responsableCargaCapacidad: null , asociadoSistema: true, grupoLDAP: "SWF_ST_SUS_TELECOM", manoObraPropia: true).save(failOnError: true)
		new SoftwareFactory(descripcion: "SWF_ST_TAS_SPC_PROVEEDOR", gestionaCapacidad: false, responsableCargaCapacidad: null , asociadoSistema: true, grupoLDAP: "SWF_ST_TAS_SPC_PROVEEDOR", manoObraPropia: false).save(failOnError: true)
		new SoftwareFactory(descripcion: "SWF_ST_TAS_TELECOM", gestionaCapacidad: false, responsableCargaCapacidad: null , asociadoSistema: true, grupoLDAP: "SWF_ST_TAS_TELECOM", manoObraPropia: true).save(failOnError: true)

	}

	private def definirHoras(swf, valorHora1, valorHora2) {
		//
		// Capacidad y valor hora de las SWFs
		//
		def swf_dti_genesis_bill_cycle = SoftwareFactory.findByGrupoLDAP("SWF_DTI_GENESIS_BILL_CYCLE_PROVEEDOR")
		swf_dti_genesis_bill_cycle.addToStockMensualHoras(new StockMensualSWF(fechaDesde: new Date().parse("dd/MM/yyyy", "01/04/2012"), fechaHasta: new Date().parse("dd/MM/yyyy","30/03/2014"), stockMensual: 1000, valor: 78.2))
		swf_dti_genesis_bill_cycle.save(failOnError: true)

		def swf_dti_genesis_customer = SoftwareFactory.findByGrupoLDAP("SWF_DTI_GENESIS_CUSTOMER_PROVEEDOR")
		swf_dti_genesis_customer.addToStockMensualHoras(new StockMensualSWF(fechaDesde: new Date().parse("dd/MM/yyyy", "01/04/2012"), fechaHasta: new Date().parse("dd/MM/yyyy","30/03/2014"), stockMensual: 400, valor: 78.2))
		swf_dti_genesis_customer.save(failOnError: true)

		def swf_dti_genesis_ups = SoftwareFactory.findByGrupoLDAP("SWF_DTI_GENESIS_UPS_PROVEEDOR")
		swf_dti_genesis_ups.addToStockMensualHoras(new StockMensualSWF(fechaDesde: new Date().parse("dd/MM/yyyy", "01/04/2012"), fechaHasta: new Date().parse("dd/MM/yyyy","30/03/2014"), stockMensual: 500, valor: 78.2))
		swf_dti_genesis_ups.save(failOnError: true)

		def swf_dti_nba = SoftwareFactory.findByGrupoLDAP("SWF_DTI_NBA_PROVEEDOR")
		swf_dti_nba.addToStockMensualHoras(new StockMensualSWF(fechaDesde: new Date().parse("dd/MM/yyyy", "01/04/2012"), fechaHasta: new Date().parse("dd/MM/yyyy","30/03/2014"), stockMensual: 120, valor: 78.2))
		swf_dti_nba.save(failOnError: true)

		def swf_dti_genesis_mic = SoftwareFactory.findByGrupoLDAP("SWF_DTI_GENESIS_MIC_PROVEEDOR")
		swf_dti_genesis_mic.addToStockMensualHoras(new StockMensualSWF(fechaDesde: new Date().parse("dd/MM/yyyy", "01/04/2012"), fechaHasta: new Date().parse("dd/MM/yyyy","30/03/2014"), stockMensual: 1600, valor: 78.2))
		swf_dti_genesis_mic.save(failOnError: true)

		def swf_dti_genesis_oyp = SoftwareFactory.findByGrupoLDAP("SWF_DTI_GENESIS_OYP_PROVEEDOR")
		swf_dti_genesis_oyp.addToStockMensualHoras(new StockMensualSWF(fechaDesde: new Date().parse("dd/MM/yyyy", "01/04/2012"), fechaHasta: new Date().parse("dd/MM/yyyy","30/03/2014"), stockMensual: 200, valor: 78.2))
		swf_dti_genesis_oyp.save(failOnError: true)

		def swf_dti_pyo = SoftwareFactory.findByGrupoLDAP("SWF_DTI_PYO_PROVEEDOR")
		swf_dti_pyo.addToStockMensualHoras(new StockMensualSWF(fechaDesde: new Date().parse("dd/MM/yyyy", "01/04/2012"), fechaHasta: new Date().parse("dd/MM/yyyy","30/03/2014"), stockMensual: 100, valor: 78.2))
		swf_dti_pyo.save(failOnError: true)

		def swf_dti_interconnect = SoftwareFactory.findByGrupoLDAP("SWF_DTI_INTERCONNECT_PROVEEDOR")
		swf_dti_interconnect.addToStockMensualHoras(new StockMensualSWF(fechaDesde: new Date().parse("dd/MM/yyyy", "01/04/2012"), fechaHasta: new Date().parse("dd/MM/yyyy","30/03/2014"), stockMensual: 100, valor: 78.2))
		swf_dti_interconnect.save(failOnError: true)

		def swf_dti_temis = SoftwareFactory.findByGrupoLDAP("SWF_DTI_TEMIS_PROVEEDOR")
		swf_dti_temis.addToStockMensualHoras(new StockMensualSWF(fechaDesde: new Date().parse("dd/MM/yyyy", "01/04/2012"), fechaHasta: new Date().parse("dd/MM/yyyy","30/03/2014"), stockMensual: 150, valor: 78.2))
		swf_dti_temis.save(failOnError: true)

		def swf_dti_factesp = SoftwareFactory.findByGrupoLDAP("SWF_DTI_FACTESP_PROVEEDOR")
		swf_dti_factesp.addToStockMensualHoras(new StockMensualSWF(fechaDesde: new Date().parse("dd/MM/yyyy", "01/04/2012"), fechaHasta: new Date().parse("dd/MM/yyyy","30/03/2014"), stockMensual: 200, valor: 78.2))
		swf_dti_factesp.save(failOnError: true)

		def swf_dti_fosse = SoftwareFactory.findByGrupoLDAP("SWF_DTI_FOSSE_PROVEEDOR")
		swf_dti_fosse.addToStockMensualHoras(new StockMensualSWF(fechaDesde: new Date().parse("dd/MM/yyyy", "01/04/2012"), fechaHasta: new Date().parse("dd/MM/yyyy","30/03/2014"), stockMensual: 50, valor: 78.2))
		swf_dti_fosse.save(failOnError: true)

		def swf_dti_e_mailing = SoftwareFactory.findByGrupoLDAP("SWF_DTI_E-MAILING_PROVEEDOR")
		swf_dti_e_mailing.addToStockMensualHoras(new StockMensualSWF(fechaDesde: new Date().parse("dd/MM/yyyy", "01/04/2012"), fechaHasta: new Date().parse("dd/MM/yyyy","30/03/2014"), stockMensual: 200, valor: 78.2))
		swf_dti_e_mailing.save(failOnError: true)

		def swf_st_qv_spc = SoftwareFactory.findByGrupoLDAP("SWF_ST_QV_SPC_PROVEEDOR")
		swf_st_qv_spc.addToStockMensualHoras(new StockMensualSWF(fechaDesde: null, fechaHasta: null, stockMensual: null, valor: 75))
		swf_st_qv_spc.save(failOnError: true)

		def swf_st_abuse_sofrecom = SoftwareFactory.findByGrupoLDAP("SWF_ST_ABUSE_SOFRECOM_PROVEEDOR")
		swf_st_abuse_sofrecom.addToStockMensualHoras(new StockMensualSWF(fechaDesde: null, fechaHasta: null, stockMensual: null, valor: 65))
		swf_st_abuse_sofrecom.save(failOnError: true)

		def swf_st_agilit_spc = SoftwareFactory.findByGrupoLDAP("SWF_ST_AGILIT_SPC_PROVEEDOR")
		swf_st_agilit_spc.addToStockMensualHoras(new StockMensualSWF(fechaDesde: null, fechaHasta: null, stockMensual: null, valor: 65))
		swf_st_agilit_spc.save(failOnError: true)

		def swf_st_base_ip = SoftwareFactory.findByGrupoLDAP("SWF_ST_BASE_IP_PROVEEDOR")
		swf_st_base_ip.addToStockMensualHoras(new StockMensualSWF(fechaDesde: null, fechaHasta: null, stockMensual: null, valor: 70))
		swf_st_base_ip.save(failOnError: true)

		def swf_st_ciclope_spc = SoftwareFactory.findByGrupoLDAP("SWF_ST_CICLOPE_SPC_PROVEEDOR")
		swf_st_ciclope_spc.addToStockMensualHoras(new StockMensualSWF(fechaDesde: null, fechaHasta: null, stockMensual: null, valor: 85))
		swf_st_ciclope_spc.save(failOnError: true)

		def swf_st_da_netcracker = SoftwareFactory.findByGrupoLDAP("SWF_ST_DA_NETCRACKER_PROVEEDOR")
		swf_st_da_netcracker.addToStockMensualHoras(new StockMensualSWF(fechaDesde: null, fechaHasta: null, stockMensual: null, valor: 75))
		swf_st_da_netcracker.save(failOnError: true)

		def swf_st_inventarios_spc = SoftwareFactory.findByGrupoLDAP("SWF_ST_INVENTARIOS_SPC_PROVEEDOR")
		swf_st_inventarios_spc.addToStockMensualHoras(new StockMensualSWF(fechaDesde: null, fechaHasta: null, stockMensual: null, valor: 85))
		swf_st_inventarios_spc.save(failOnError: true)

		def swf_st_persa = SoftwareFactory.findByGrupoLDAP("SWF_ST_PERSA_PROVEEDOR")
		swf_st_persa.addToStockMensualHoras(new StockMensualSWF(fechaDesde: null, fechaHasta: null, stockMensual: null, valor: 70))
		swf_st_persa.save(failOnError: true)

		def swf_st_plataforma_precalificacion = SoftwareFactory.findByGrupoLDAP("SWF_ST_PLATAFORMA_PRECALIFICACION_PROVEEDOR")
		swf_st_plataforma_precalificacion.addToStockMensualHoras(new StockMensualSWF(fechaDesde: null, fechaHasta: null, stockMensual: null, valor: 82))
		swf_st_plataforma_precalificacion.save(failOnError: true)

		def swf_st_plataforma_testing = SoftwareFactory.findByGrupoLDAP("SWF_ST_PLATAFORMA_TESTING_PROVEEDOR")
		swf_st_plataforma_testing.addToStockMensualHoras(new StockMensualSWF(fechaDesde: null, fechaHasta: null, stockMensual: null, valor: 82))
		swf_st_plataforma_testing.save(failOnError: true)

		def swf_st_portal_cda = SoftwareFactory.findByGrupoLDAP("SWF_ST_PORTAL_CDA_PROVEEDOR")
		swf_st_portal_cda.addToStockMensualHoras(new StockMensualSWF(fechaDesde: null, fechaHasta: null, stockMensual: null, valor: 70))
		swf_st_portal_cda.save(failOnError: true)

		def swf_st_portal_sofrecom = SoftwareFactory.findByGrupoLDAP("SWF_ST_PORTAL_SOFRECOM_PROVEEDOR")
		swf_st_portal_sofrecom.addToStockMensualHoras(new StockMensualSWF(fechaDesde: null, fechaHasta: null, stockMensual: null, valor: 65))
		swf_st_portal_sofrecom.save(failOnError: true)

		def swf_st_remeyred_spc = SoftwareFactory.findByGrupoLDAP("SWF_ST_REMEYRED_SPC_PROVEEDOR")
		swf_st_remeyred_spc.addToStockMensualHoras(new StockMensualSWF(fechaDesde: null, fechaHasta: null, stockMensual: null, valor: 65))
		swf_st_remeyred_spc.save(failOnError: true)

		def swf_st_segat_spc = SoftwareFactory.findByGrupoLDAP("SWF_ST_SEGAT_SPC_PROVEEDOR")
		swf_st_segat_spc.addToStockMensualHoras(new StockMensualSWF(fechaDesde: null, fechaHasta: null, stockMensual: null, valor: 65))
		swf_st_segat_spc.save(failOnError: true)

		def swf_st_sg_datos = SoftwareFactory.findByGrupoLDAP("SWF_ST_SG_DATOS_PROVEEDOR")
		swf_st_sg_datos.addToStockMensualHoras(new StockMensualSWF(fechaDesde: null, fechaHasta: null, stockMensual: null, valor: 205))
		swf_st_sg_datos.save(failOnError: true)

		def swf_st_sideral = SoftwareFactory.findByGrupoLDAP("SWF_ST_SIDERAL_PROVEEDOR")
		swf_st_sideral.addToStockMensualHoras(new StockMensualSWF(fechaDesde: null, fechaHasta: null, stockMensual: null, valor: 205))
		swf_st_sideral.save(failOnError: true)

		def swf_st_sistema_afectacion_servicios = SoftwareFactory.findByGrupoLDAP("SWF_ST_SISTEMA-AFECTACION-SERVICIOS_PROVEEDOR")
		swf_st_sistema_afectacion_servicios.addToStockMensualHoras(new StockMensualSWF(fechaDesde: null, fechaHasta: null, stockMensual: null, valor: 205))
		swf_st_sistema_afectacion_servicios.save(failOnError: true)

		def swf_st_supervision_vpns = SoftwareFactory.findByGrupoLDAP("SWF_ST_SUPERVISION-VPNS_PROVEEDOR")
		swf_st_supervision_vpns.addToStockMensualHoras(new StockMensualSWF(fechaDesde: null, fechaHasta: null, stockMensual: null, valor: 134))
		swf_st_supervision_vpns.save(failOnError: true)

		def swf_st_sur_spc = SoftwareFactory.findByGrupoLDAP("SWF_ST_SUR_SPC_PROVEEDOR")
		swf_st_sur_spc.addToStockMensualHoras(new StockMensualSWF(fechaDesde: null, fechaHasta: null, stockMensual: null, valor: 65))
		swf_st_sur_spc.save(failOnError: true)

		def swf_st_sus = SoftwareFactory.findByGrupoLDAP("SWF_ST_SUS_PROVEEDOR")
		swf_st_sus.addToStockMensualHoras(new StockMensualSWF(fechaDesde: null, fechaHasta: null, stockMensual: null, valor: 63))
		swf_st_sus.save(failOnError: true)

		def swf_st_sus_sofrecom = SoftwareFactory.findByGrupoLDAP("SWF_ST_SUS_SOFRECOM_PROVEEDOR")
		swf_st_sus_sofrecom.addToStockMensualHoras(new StockMensualSWF(fechaDesde: null, fechaHasta: null, stockMensual: null, valor: 63))
		swf_st_sus_sofrecom.save(failOnError: true)

		def swf_st_tas_spc = SoftwareFactory.findByGrupoLDAP("SWF_ST_TAS_SPC_PROVEEDOR")
		swf_st_tas_spc.addToStockMensualHoras(new StockMensualSWF(fechaDesde: null, fechaHasta: null, stockMensual: null, valor: 65))
		swf_st_tas_spc.save(failOnError: true)

		def swf_dti_adm_de_contactos = SoftwareFactory.findByGrupoLDAP("SWF_DTI_ADM_DE_CONTACTOS_PROVEEDOR")
		swf_dti_adm_de_contactos.addToStockMensualHoras(new StockMensualSWF(fechaDesde: null, fechaHasta: null, stockMensual: null, valor: 69))
		swf_dti_adm_de_contactos.save(failOnError: true)

		def swf_dti_calipso = SoftwareFactory.findByGrupoLDAP("SWF_DTI_CALIPSO_PROVEEDOR")
		swf_dti_calipso.addToStockMensualHoras(new StockMensualSWF(fechaDesde: null, fechaHasta: null, stockMensual: null, valor: 96.48))
		swf_dti_calipso.save(failOnError: true)

		def swf_dti_carriers_interconectantes = SoftwareFactory.findByGrupoLDAP("SWF_DTI_CARRIERS_INTERCONECTANTES_PROVEEDOR")
		swf_dti_carriers_interconectantes.addToStockMensualHoras(new StockMensualSWF(fechaDesde: null, fechaHasta: null, stockMensual: null, valor: 65))
		swf_dti_carriers_interconectantes.save(failOnError: true)

		def swf_dti_catalogo_pys = SoftwareFactory.findByGrupoLDAP("SWF_DTI_CATALOGO_PYS_PROVEEDOR")
		swf_dti_catalogo_pys.addToStockMensualHoras(new StockMensualSWF(fechaDesde: null, fechaHasta: null, stockMensual: null, valor: 70))
		swf_dti_catalogo_pys.save(failOnError: true)

		def swf_dti_crm_use_oyp = SoftwareFactory.findByGrupoLDAP("SWF_DTI_CRM_USE_OYP_PROVEEDOR")
		swf_dti_crm_use_oyp.addToStockMensualHoras(new StockMensualSWF(fechaDesde: null, fechaHasta: null, stockMensual: null, valor: 55))
		swf_dti_crm_use_oyp.save(failOnError: true)

		def swf_dti_da_vinci_gestor_de_comisiones = SoftwareFactory.findByGrupoLDAP("SWF_DTI_DA_VINCI_GESTOR_DE_COMISIONES_PROVEEDOR")
		swf_dti_da_vinci_gestor_de_comisiones.addToStockMensualHoras(new StockMensualSWF(fechaDesde: null, fechaHasta: null, stockMensual: null, valor: 92))
		swf_dti_da_vinci_gestor_de_comisiones.save(failOnError: true)

		def swf_dti_deimos = SoftwareFactory.findByGrupoLDAP("SWF_DTI_DEIMOS_PROVEEDOR")
		swf_dti_deimos.addToStockMensualHoras(new StockMensualSWF(fechaDesde: null, fechaHasta: null, stockMensual: null, valor: 78))
		swf_dti_deimos.save(failOnError: true)

		def swf_dti_delta_accentur = SoftwareFactory.findByGrupoLDAP("SWF_DTI_DELTA_ACCENTUR_PROVEEDOR")
		swf_dti_delta_accentur.addToStockMensualHoras(new StockMensualSWF(fechaDesde: null, fechaHasta: null, stockMensual: null, valor: 70))
		swf_dti_delta_accentur.save(failOnError: true)

		def swf_dti_delta_indra = SoftwareFactory.findByGrupoLDAP("SWF_DTI_DELTA_INDRA_PROVEEDOR")
		swf_dti_delta_indra.addToStockMensualHoras(new StockMensualSWF(fechaDesde: null, fechaHasta: null, stockMensual: null, valor: 70))
		swf_dti_delta_indra.save(failOnError: true)

		def swf_dti_delta_sofrecom = SoftwareFactory.findByGrupoLDAP("SWF_DTI_DELTA_SOFRECOM_PROVEEDOR")
		swf_dti_delta_sofrecom.addToStockMensualHoras(new StockMensualSWF(fechaDesde: null, fechaHasta: null, stockMensual: null, valor: 70))
		swf_dti_delta_sofrecom.save(failOnError: true)

		def swf_dti_determinates_de_tasacion_accenture = SoftwareFactory.findByGrupoLDAP("SWF_DTI_DETERMINATES_DE_TASACION_ACCENTURE_PROVEEDOR")
		swf_dti_determinates_de_tasacion_accenture.addToStockMensualHoras(new StockMensualSWF(fechaDesde: null, fechaHasta: null, stockMensual: null, valor: 75.25))
		swf_dti_determinates_de_tasacion_accenture.save(failOnError: true)

		def swf_dti_determinates_de_tasacion_sofrecom = SoftwareFactory.findByGrupoLDAP("SWF_DTI_DETERMINATES_DE_TASACION_SOFRECOM_PROVEEDOR")
		swf_dti_determinates_de_tasacion_sofrecom.addToStockMensualHoras(new StockMensualSWF(fechaDesde: null, fechaHasta: null, stockMensual: null, valor: 75.25))
		swf_dti_determinates_de_tasacion_sofrecom.save(failOnError: true)

		def swf_dti_ipb = SoftwareFactory.findByGrupoLDAP("SWF_DTI_IPB_PROVEEDOR")
		swf_dti_ipb.addToStockMensualHoras(new StockMensualSWF(fechaDesde: null, fechaHasta: null, stockMensual: null, valor: 114.3))
		swf_dti_ipb.save(failOnError: true)

		def swf_dti_scard_cold = SoftwareFactory.findByGrupoLDAP("SWF_DTI_SCARD_COLD_PROVEEDOR")
		swf_dti_scard_cold.addToStockMensualHoras(new StockMensualSWF(fechaDesde: null, fechaHasta: null, stockMensual: null, valor: 78.2))
		swf_dti_scard_cold.save(failOnError: true)

		def swf_dti_teco_usa_prepago = SoftwareFactory.findByGrupoLDAP("SWF_DTI_TECO_USA_PREPAGO_PROVEEDOR")
		swf_dti_teco_usa_prepago.addToStockMensualHoras(new StockMensualSWF(fechaDesde: null, fechaHasta: null, stockMensual: null, valor: 69))
		swf_dti_teco_usa_prepago.save(failOnError: true)

		def swf_dti_vantive_field_service = SoftwareFactory.findByGrupoLDAP("SWF_DTI_VANTIVE_FIELD_SERVICE_PROVEEDOR")
		swf_dti_vantive_field_service.addToStockMensualHoras(new StockMensualSWF(fechaDesde: null, fechaHasta: null, stockMensual: null, valor: 70))
		swf_dti_vantive_field_service.save(failOnError: true)
	}

	// Sistemas - Completar datos
	private def completarDatosSistemas(Sistema sistema, grupo_rswf, normaliza, grupo_admin, grupo_go) {
		if (sistema) {
			sistema.grupoReferenteSWF = grupo_rswf
			sistema.normaliza = normaliza
			sistema.grupoAdministrador = grupo_admin
			sistema.grupoLDAPGestionOperativa = grupo_go
			sistema.save(failOnError: true)
		}
	}

	private actualizarDatosSistemas() {
		def v112_com = Sistema.findByDescripcion("112.COM_APL")
		def abonados_en_guia = Sistema.findByDescripcion("ABONADOS_EN_GUIA_APL")
		def abuse = Sistema.findByDescripcion("ABUSE_APL")
		def acdserver = Sistema.findByDescripcion("ACDSERVER_APL")
		def adas = Sistema.findByDescripcion("ADAS_APL")
		def adhesion_factura_digital = Sistema.findByDescripcion("ADHESION_FACTURA_DIGITAL_APL")
		def adm_de_contactos = Sistema.findByDescripcion("ADM_DE_CONTACTOS_APL")
		def adm_de_contactos_interfaces = Sistema.findByDescripcion("ADM_DE_CONTACTOS_INTERFACES")
		def adsl_ventas = Sistema.findByDescripcion("ADSL_VENTAS_APL")
		def agilit = Sistema.findByDescripcion("AGILIT_APL")
		def amadeus = Sistema.findByDescripcion("AMADEUS_APL")
		def aris = Sistema.findByDescripcion("ARIS_APL")
		def asset_center = Sistema.findByDescripcion("ASSET_CENTER_APL")
		def atlantis = Sistema.findByDescripcion("ATLANTIS_APL")
		def bas = Sistema.findByDescripcion("BAS_APL")
		def base_entorno_geo = Sistema.findByDescripcion("BASE_ENTORNO_GEO_APL")
		def baseip = Sistema.findByDescripcion("BASEIP_APL")
		def belen = Sistema.findByDescripcion("BELEN_APL")
		def bios_hal = Sistema.findByDescripcion("BIOS_HAL_APL")
		def calipso_cobros = Sistema.findByDescripcion("CALIPSO_COBROS")
		def calipso_facturacion = Sistema.findByDescripcion("CALIPSO_FACTURACION")
		def calipso_prefacturacion = Sistema.findByDescripcion("CALIPSO_PREFACTURACION")
		def campaign_management = Sistema.findByDescripcion("CAMPAIGN_MANAGEMENT_APL")
		def carriers_interconectantes = Sistema.findByDescripcion("CARRIERS_INTERCONECTANTES_APL")
		def catalogo_pys = Sistema.findByDescripcion("CATALOGO_PYS_APL")
		def cctbi = Sistema.findByDescripcion("CCTBI_APL")
		def central_parq = Sistema.findByDescripcion("CENTRAL_PARQ_APL")
		def centro_validador = Sistema.findByDescripcion("CENTRO_VALIDADOR_APL")
		def centro_validador_web = Sistema.findByDescripcion("CENTRO_VALIDADOR_WEB")
		def certificaciones = Sistema.findByDescripcion("CERTIFICACIONES_APL")
		def ciclope = Sistema.findByDescripcion("CICLOPE_APL")
		def citas = Sistema.findByDescripcion("CITAS_APL")
		def cms = Sistema.findByDescripcion("CMS_APL")
		def cnm = Sistema.findByDescripcion("CNM_APL")
		def cobranza_electronica_front_end = Sistema.findByDescripcion("COBRANZA_ELECTRONICA_FRONT_END")
		def cobranza_integrada_front_end = Sistema.findByDescripcion("COBRANZA_INTEGRADA_FRONT_END")
		def cris = Sistema.findByDescripcion("CRIS_APL")
		def crm_use_oyp = Sistema.findByDescripcion("CRM_USE_OYP_APL")
		def cuenta_online_back_end = Sistema.findByDescripcion("CUENTA_ONLINE_BACK_END_APL")
		def cuenta_online_front_end = Sistema.findByDescripcion("CUENTA_ONLINE_FRONT_END_APL")
		def cuenta_online_pyme_front_end = Sistema.findByDescripcion("CUENTA_ONLINE_PYME_FRONT_END_APL")
		def dacota = Sistema.findByDescripcion("DACOTA_APL")
		def data_activator = Sistema.findByDescripcion("DATA_ACTIVATOR_APL")
		def ddt = Sistema.findByDescripcion("DDT_APL")
		def deimos = Sistema.findByDescripcion("DEIMOS_APL")
		def delta = Sistema.findByDescripcion("DELTA_APL")
		def denarius = Sistema.findByDescripcion("DENARIUS_APL")
		def diaria_net = Sistema.findByDescripcion("DIARIA_.NET_APL")
		def e_mailing_front_end = Sistema.findByDescripcion("E-MAILING_FRONT_END_APL")
		def epm_dsi = Sistema.findByDescripcion("EPM_DSI_APL")
		def estadistica_0800_0810_back_end = Sistema.findByDescripcion("ESTADISTICA_0800_0810_BACK_END")
		def estadistica_0800_0810_front_end = Sistema.findByDescripcion("ESTADISTICA_0800_0810_FRONT_END")
		def eva_cuentas_corrientes_masivo = Sistema.findByDescripcion("EVA_CUENTAS_CORRIENTES_MASIVO")
		def eva_morosidad_cobranzas_gc_ws = Sistema.findByDescripcion("EVA_MOROSIDAD_COBRANZAS_GC_WS")
		def eva_morosidad_financiacion_masivo = Sistema.findByDescripcion("EVA_MOROSIDAD_FINANCIACION_MASIVO")
		def eva_postbaja_computo_impositivo = Sistema.findByDescripcion("EVA_POSTBAJA_COMPUTO_IMPOSITIVO")
		def factesp = Sistema.findByDescripcion("FACTESP_APL")
		def fax_server = Sistema.findByDescripcion("FAX_SERVER_APL")
		def fax_server_segat = Sistema.findByDescripcion("FAX_SERVER_SEGAT_APL")
		def fosse = Sistema.findByDescripcion("FOSSE_APL")
		def frontbi_cctbi = Sistema.findByDescripcion("FRONTBI_CCTBI_APL")
		def frontbi_dacota = Sistema.findByDescripcion("FRONTBI_DACOTA_APL")
		def frontbi_smart = Sistema.findByDescripcion("FRONTBI_SMART_APL")
		def frontbi_tabgo = Sistema.findByDescripcion("FRONTBI_TABGO_APL")
		def frontbi_tabra = Sistema.findByDescripcion("FRONTBI_TABRA_APL")
		def frontbi_zoom = Sistema.findByDescripcion("FRONTBI_ZOOM_APL")
		def genesis_bill_cycle = Sistema.findByDescripcion("GENESIS_BILL_CYCLE")
		def genesis_customer = Sistema.findByDescripcion("GENESIS_CUSTOMER")
		def genesis_mic = Sistema.findByDescripcion("GENESIS_MIC")
		def genesis_oyp = Sistema.findByDescripcion("GENESIS_OYP")
		def genesis_ups = Sistema.findByDescripcion("GENESIS_UPS")
		def gesprod = Sistema.findByDescripcion("GESPROD_APL")
		def gestion_de_emprendimientos = Sistema.findByDescripcion("GESTION_DE_EMPRENDIMIENTOS_APL")
		def gestor_de_comisiones = Sistema.findByDescripcion("GESTOR_DE_COMISIONES_APL")
		def girafe = Sistema.findByDescripcion("GIRAFE_APL")
		def guc = Sistema.findByDescripcion("GUC_APL")
		def interconnect = Sistema.findByDescripcion("INTERCONNECT_APL")
		def ipb = Sistema.findByDescripcion("IPB_APL")
		def iris = Sistema.findByDescripcion("IRIS_APL")
		def ium = Sistema.findByDescripcion("IUM_APL")
		def jano = Sistema.findByDescripcion("JANO_APL")
		def jano_proceso_etl = Sistema.findByDescripcion("JANO_PROCESO_ETL")
		def kingdom = Sistema.findByDescripcion("KINGDOM_APL")
		def litigios = Sistema.findByDescripcion("LITIGIOS_APL")
		def mac = Sistema.findByDescripcion("MAC_APL")
		def mi_cuenta_back_end = Sistema.findByDescripcion("MI_CUENTA_BACK_END")
		def mi_cuenta_front_end = Sistema.findByDescripcion("MI_CUENTA_FRONT_END")
		def nba = Sistema.findByDescripcion("NBA_APL")
		def needit = Sistema.findByDescripcion("NEEDIT_APL")
		def network_inventory = Sistema.findByDescripcion("NETWORK_INVENTORY_APL")
		def nred = Sistema.findByDescripcion("NRED_APL")
		def ods = Sistema.findByDescripcion("ODS_APL")
		def persa = Sistema.findByDescripcion("PERSA_APL")
		def plataforma_de_precalificacion = Sistema.findByDescripcion("PLATAFORMA_DE_PRECALIFICACION_APL")
		def plataforma_de_testing = Sistema.findByDescripcion("PLATAFORMA_DE_TESTING_APL")
		def portal_bi_utf = Sistema.findByDescripcion("PORTAL_BI_UTF_APL")
		def prod_arnet_portales_arnet = Sistema.findByDescripcion("PROD_ARNET_PORTALES_ARNET")
		def prod_arnet_portales_back_office = Sistema.findByDescripcion("PROD_ARNET_PORTALES_BACK_OFFICE")
		def prod_arnet_portales_telecom = Sistema.findByDescripcion("PROD_ARNET_PORTALES_TELECOM")
		def pyo = Sistema.findByDescripcion("PYO_APL")
		def qv_asse = Sistema.findByDescripcion("QV_ASSE_APL")
		def qv_clie = Sistema.findByDescripcion("QV_CLIE_APL")
		def qv_masivo = Sistema.findByDescripcion("QV_MASIVO_APL")
		def qv_nomasivo = Sistema.findByDescripcion("QV_NOMASIVO_APL")
		def qv_remedy = Sistema.findByDescripcion("QV_REMEDY_APL")
		def qv_siag = Sistema.findByDescripcion("QV_SIAG_APL")
		def qv_simplit = Sistema.findByDescripcion("QV_SIMPLIT_APL")
		def qv_ssgg = Sistema.findByDescripcion("QV_SSGG_APL")
		def remedyred = Sistema.findByDescripcion("REMEDYRED_APL")
		def reporte_aseguramiento = Sistema.findByDescripcion("REPORTE_ASEGURAMIENTO_APL")
		def sas = Sistema.findByDescripcion("SAS_APL")
		def scard = Sistema.findByDescripcion("SCARD_APL")
		def segat = Sistema.findByDescripcion("SEGAT_APL")
		def sei = Sistema.findByDescripcion("SEI_APL")
		def seta = Sistema.findByDescripcion("SETA_APL")
		def sgct = Sistema.findByDescripcion("SGCT_APL")
		def sgdatos = Sistema.findByDescripcion("SGDATOS_APL")
		def sgdatos_tp = Sistema.findByDescripcion("SGDATOS_TP_APL")
		def sharepoint = Sistema.findByDescripcion("SHAREPOINT_APL")
		def siaf = Sistema.findByDescripcion("SIAF_APL")
		def sideral = Sistema.findByDescripcion("SIDERAL_APL")
		def siebel_customer_centric = Sistema.findByDescripcion("SIEBEL_CUSTOMER_CENTRIC_APL")
		def signo_cap = Sistema.findByDescripcion("SIGNO_CAP")
		def signo_cobranzas = Sistema.findByDescripcion("SIGNO_COBRANZAS")
		def signo_compras = Sistema.findByDescripcion("SIGNO_COMPRAS")
		def signo_compras_estrategias = Sistema.findByDescripcion("SIGNO_COMPRAS_ESTRATEGIAS")
		def signo_ess = Sistema.findByDescripcion("SIGNO_ESS")
		def signo_ess_celulares = Sistema.findByDescripcion("SIGNO_ESS_CELULARES_APL")
		def signo_ess_mi_perfil = Sistema.findByDescripcion("SIGNO_ESS_MI PERFIL_APL")
		def signo_ess_mis_licencias = Sistema.findByDescripcion("SIGNO_ESS_MIS LICENCIAS_APL")
		def signo_ess_plan_empleados = Sistema.findByDescripcion("SIGNO_ESS_PLAN EMPLEADOS_APL")
		def signo_ess_recibo_electronico = Sistema.findByDescripcion("SIGNO_ESS_RECIBO ELECTRONICO_APL")
		def signo_facturas_web = Sistema.findByDescripcion("SIGNO_FACTURAS_WEB")
		def signo_finanzas = Sistema.findByDescripcion("SIGNO_FINANZAS")
		def signo_finanzas_py = Sistema.findByDescripcion("SIGNO_FINANZAS_PY")
		def signo_guiainterna = Sistema.findByDescripcion("SIGNO_GUIAINTERNA")
		def signo_logistica = Sistema.findByDescripcion("SIGNO_LOGISTICA")
		def signo_mantenimiento = Sistema.findByDescripcion("SIGNO_MANTENIMIENTO")
		def signo_neoportal = Sistema.findByDescripcion("SIGNO_NEOPORTAL_APL")
		def signo_proyectos = Sistema.findByDescripcion("SIGNO_PROYECTOS")
		def signo_real_estate = Sistema.findByDescripcion("SIGNO_REAL_ESTATE")
		def signo_registro_firmas = Sistema.findByDescripcion("SIGNO_REGISTRO_FIRMAS")
		def signo_rrhh = Sistema.findByDescripcion("SIGNO_RRHH")
		def signo_sap_grc = Sistema.findByDescripcion("SIGNO_SAP_GRC")
		def signo_sh_agendablog = Sistema.findByDescripcion("SIGNO_SH_AGENDABLOG_APL")
		def signo_sh_blog_copamundo = Sistema.findByDescripcion("SIGNO_SH_BLOG_COPAMUNDO_APL")
		def signo_sh_blog_equilibrio = Sistema.findByDescripcion("SIGNO_SH_BLOG_EQUILIBRIO_APL")
		def signo_sh_blog_teletrabajo = Sistema.findByDescripcion("SIGNO_SH_BLOG_TELETRABAJO_APL")
		def signo_sh_colonia = Sistema.findByDescripcion("SIGNO_SH_COLONIA_APL")
		def signo_sh_ddjj = Sistema.findByDescripcion("SIGNO_SH_DDJJ_APL")
		def signo_sh_imp_ganancias = Sistema.findByDescripcion("SIGNO_SH_IMP_GANANCIAS_APL")
		def signo_sh_lineas_servicio = Sistema.findByDescripcion("SIGNO_SH_LINEAS_SERVICIO_APL")
		def signo_sh_wiki = Sistema.findByDescripcion("SIGNO_SH_WIKI_APL")
		def signo_sorteos = Sistema.findByDescripcion("SIGNO_SORTEOS")
		def signo_tesoreria = Sistema.findByDescripcion("SIGNO_TESORERIA")
		def signo_veconp = Sistema.findByDescripcion("SIGNO_VECONP")
		def signo_voluntariado = Sistema.findByDescripcion("SIGNO_VOLUNTARIADO")
		def simplit = Sistema.findByDescripcion("SIMPLIT_APL")
		def sir = Sistema.findByDescripcion("SIR_APL")
		def smart = Sistema.findByDescripcion("SMART_APL")
		def sp_calidad_redesyservicios = Sistema.findByDescripcion("SP_CALIDAD_REDESYSERVICIOS_APL")
		def suc2 = Sistema.findByDescripcion("SUC2_APL")
		def supervision_vpn = Sistema.findByDescripcion("SUPERVISION_VPN_APL")
		def sur = Sistema.findByDescripcion("SUR_APL")
		def sus = Sistema.findByDescripcion("SUS_APL")
		def tab_go = Sistema.findByDescripcion("TAB_GO_APL")
		def tab_ra = Sistema.findByDescripcion("TAB_RA_APL")
		def tas = Sistema.findByDescripcion("TAS_APL")
		def temis = Sistema.findByDescripcion("TEMIS_APL")
		def transformer = Sistema.findByDescripcion("TRANSFORMER_APL")
		def truman = Sistema.findByDescripcion("TRUMAN_APL")
		def ttusa_prepago_api = Sistema.findByDescripcion("TTUSA_PREPAGO_API")
		def ttusa_prepago_gestion = Sistema.findByDescripcion("TTUSA_PREPAGO_GESTION")
		def utopia = Sistema.findByDescripcion("UTOPIA_APL")
		def vantive_field_service = Sistema.findByDescripcion("VANTIVE_FIELD_SERVICE_APL")
		def web_adsl_tecnica = Sistema.findByDescripcion("WEB_ADSL_TECNICA_APL")
		def workflow_tecnico = Sistema.findByDescripcion("WORKFLOW_TECNICO_APL")
		def yadas = Sistema.findByDescripcion("YADAS_APL")
		def zoom = Sistema.findByDescripcion("ZOOM_APL")

		completarDatosSistemas(v112_com, "RSWF_DTI_112.COM", false, "AF_112.COM", "GO_DTI_VENTAS")
		completarDatosSistemas(abonados_en_guia, "RSWF_DTI_ABONADOS_EN_GUIA", false, "AF_ABONADOS_EN_GUIA", null)
		completarDatosSistemas(abuse, "RSWF_ST_ABUSE", false, "AF_ABUSE", null)
		completarDatosSistemas(acdserver, "RSWF_DTI_ACDSERVER", false, "AF_ACDSERVER", "GO_DTI_VENTAS")
		completarDatosSistemas(adas, "RSWF_DTI_ADAS", false, "AF_ADAS", "GO_DTI_VENTAS")
		completarDatosSistemas(adhesion_factura_digital, "RSWF_DTI_ADHESION_FACTURA_DIGITAL", false, "AF_ADHESION_FACTURA_DIGITAL", "GO_DTI_VENTAS")
		completarDatosSistemas(adm_de_contactos, "RSWF_DTI_ADM_DE_CONTACTOS", false, "AF_ADM_DE_CONTACTOS", "GO_DTI_VENTAS")
		completarDatosSistemas(adm_de_contactos_interfaces, "RSWF_DTI_ADM_DE_CONTACTOS", false, "AF_ADM_DE_CONTACTOS_INTERFACES", "GO_DTI_VENTAS")
		completarDatosSistemas(adsl_ventas, "RSWF_DTI_Venta AdSL", false, "AF_ADSL_VENTAS", "GO_DTI_VENTAS")
		completarDatosSistemas(agilit, "RSWF_ST_AGILIT", false, "AF_AGILIT", "GO_DST_SISTEMASTECNICOS")
		completarDatosSistemas(amadeus, "RSWF_DTI_AMADEUS", false, "AF_AMADEUS", null)
		completarDatosSistemas(aris, "RSWF_SC_APLRRHH", false, "AF_ARIS", "GO_SC_SISTEMAS CORPORATIVOS")
		completarDatosSistemas(asset_center, "RSWF_SC_ASSET_CENTER", false, "AF_ASSET_CENTER", "GO_SC_SIGIT")
		completarDatosSistemas(atlantis, "RSWF_ST_Inventarios", false, "AF_ATLANTIS", "GO_DST_SISTEMASTECNICOS")
		completarDatosSistemas(bas, "RSWF_DTI_BAS", false, "AF_BAS", "GO_DTI_VENTAS")
		completarDatosSistemas(base_entorno_geo, "RSWF_ST_SETA", false, "AF_BASE_ENTORNO_GEO", "GO_DST_SISTEMASTECNICOS")
		completarDatosSistemas(baseip, "RSWF_ST_BASE-IP", false, "AF_BASEIP", null)
		completarDatosSistemas(belen, "RSWF_DTI_BELEN", false, "AF_BELEN", null)
		completarDatosSistemas(bios_hal, "RSWF_DTI_BIOS - Bus de Integraci\u00F3n Corporativo ", false, "AF_BIOS_HAL", "GO_DTI_VENTAS")
		completarDatosSistemas(calipso_cobros, "RSWF_DTI_CALIPSO_COBROS", false, "AF_CALIPSO_COBROS", "GO_DTI_FACTURACION")
		completarDatosSistemas(calipso_facturacion, "RSWF_DTI_CALIPSO_BILLING", false, "AF_CALIPSO_FACTURACION", "GO_DTI_FACTURACION")
		completarDatosSistemas(calipso_prefacturacion, "RSWF_DTI_CALIPSO_PREFACTURACION", false, "AF_CALIPSO_PREFACTURACION", "GO_DTI_FACTURACION")
		completarDatosSistemas(campaign_management, "RSWF_DTI_CAMPAIGN_MANAGEMENT", false, "AF_CAMPAIGN_MANAGEMENT", null)
		completarDatosSistemas(carriers_interconectantes, "RSWF_DTI_CARRIERS_INTERCONECTANTES", false, "AF_CARRIERS_INTERCONECTANTES", null)
		completarDatosSistemas(catalogo_pys, "RSWF_DTI_CATALOGO_PYS", false, "AF_CATALOGO_PYS", null)
		completarDatosSistemas(cctbi, "RSWF_DTI_CCTBI", false, "AF_CCTBI", "GO_DTI_VENTAS")
		completarDatosSistemas(central_parq, "RSWF_DTI_CENTRAL_PARQ", false, "AF_CENTRAL_PARQ", "GO_DTI_VENTAS")
		completarDatosSistemas(centro_validador, "RSWF_DTI_CARRIERS_INTERCONECTANTES", false, "AF_CENTRO_VALIDADOR", null)
		completarDatosSistemas(centro_validador_web, "RSWF_DTI_CARRIERS_INTERCONECTANTES", false, "AF_CENTRO_VALIDADOR_WEB", null)
		completarDatosSistemas(certificaciones, "RSWF_SC_DENACERT", false, "AF_CERTIFICACIONES", "GO_SC_SISTEMAS CORPORATIVOS")
		completarDatosSistemas(ciclope, "RSWF_ST_CICLOPE", false, "AF_CICLOPE", "GO_DST_SISTEMASTECNICOS")
		completarDatosSistemas(citas, "RSWF_DTI_CITAS", false, "AF_CITAS", "GO_DTI_VENTAS")
		completarDatosSistemas(cms, "RSWF_DTI_CMS", false, "AF_CMS", "GO_DTI_VENTAS")
		completarDatosSistemas(cnm, "RSWF_ST_CNM", false, "AF_CNM", "GO_DST_SISTEMASTECNICOS")
		completarDatosSistemas(cobranza_electronica_front_end, "RSWF_DTI_COBRANZA_ELECTRONICA", false, "AF_COBRANZA_ELECTRONICA_FRONT_END", null)
		completarDatosSistemas(cobranza_integrada_front_end, "RSWF_DTI_COBRANZAS_INTEGRADAS", false, "AF_COBRANZA_INTEGRADA_FRONT_END", null)
		completarDatosSistemas(cris, "RSWF_DTI_CRIS", false, "AF_CRIS", "GO_DTI_VENTAS")
		completarDatosSistemas(crm_use_oyp, "RSWF_DTI_CRM_USE_OYP", false, "AF_CRM_USE_OYP", null)
		completarDatosSistemas(cuenta_online_back_end, "RSWF_DTI_CUENTA_ON_LINE_GC", false, "AF_CUENTA_ONLINE_BACK_END", "GO_DTI_FACTURACION")
		completarDatosSistemas(cuenta_online_front_end, "RSWF_DTI_CUENTA_ON_LINE_GC", false, "AF_CUENTA_ONLINE_FRONT_END", null)
		completarDatosSistemas(cuenta_online_pyme_front_end, "RSWF_DTI_CUENTA_ON_LINE_PYMES", false, "AF_CUENTA_ONLINE_PYME_FRONT_END", null)
		completarDatosSistemas(dacota, "RSWF_DTI_DACOTA", false, "AF_DACOTA", "GO_DTI_FACTURACION")
		completarDatosSistemas(data_activator, "RSWF_ST_DATA-ACTIVATOR", false, "AF_DATA_ACTIVATOR", null)
		completarDatosSistemas(ddt, "RSWF_DTI_DETERMINATES_DE_TASACION", false, "AF_DDT", null)
		completarDatosSistemas(deimos, "RSWF_DTI_DEIMOS", false, "AF_DEIMOS", null)
		completarDatosSistemas(delta, "RSWF_DTI_DELTA", false, "AF_DELTA", null)
		completarDatosSistemas(denarius, "RSWF_SC_DENACERT", false, "AF_DENARIUS", "GO_SC_SISTEMAS CORPORATIVOS")
		completarDatosSistemas(diaria_net, "RSWF_DTI_DIARIA_.NET", false, "AF_DIARIA_.NET", "GO_DTI_FACTURACION")
		completarDatosSistemas(e_mailing_front_end, "RSWF_DTI_E-MAILING", false, "AF_E-MAILING_FRONT_END", null)
		completarDatosSistemas(epm_dsi, "RSWF_PMO_IT", false, "AF_EPM_DSI", "GO_ PMO_EPM PMO IT ")
		completarDatosSistemas(estadistica_0800_0810_back_end, "RSWF_DTI_ESTADISTICA_0800/0810", false, "AF_ESTADISTICA_0800/0810_BACK_END", "GO_DTI_FACTURACION")
		completarDatosSistemas(estadistica_0800_0810_front_end, "RSWF_DTI_ESTADISTICA_0800/0810", false, "AF_ESTADISTICA_0800/0810_FRONT_END", null)
		completarDatosSistemas(eva_cuentas_corrientes_masivo, "RSWF_DTI_EVA_CUENTAS_CORRIENTES_MASIVO", false, "AF_EVA_CUENTAS_CORRIENTES_MASIVO", "GO_DTI_FACTURACION")
		completarDatosSistemas(eva_morosidad_cobranzas_gc_ws, "RSWF_DTI_EVA_MOROSIDAD/COBRANZAS_GC/WS", false, "AF_EVA_MOROSIDAD/COBRANZAS_GC/WS", "GO_DTI_FACTURACION")
		completarDatosSistemas(eva_morosidad_financiacion_masivo, "RSWF_DTI_EVA_MOROSIDAD/FINANCIACION_MASIVO", false, "AF_EVA_MOROSIDAD/FINANCIACION_MASIVO", "GO_DTI_FACTURACION")
		completarDatosSistemas(eva_postbaja_computo_impositivo, "RSWF_DTI_EVA_POSTBAJA/COMPUTO_IMPOSITIVO", false, "AF_EVA_POSTBAJA/COMPUTO_IMPOSITIVO", "GO_DTI_FACTURACION")
		completarDatosSistemas(factesp, "RSWF_DTI_FACTESP", false, "AF_FACTESP", "GO_DTI_FACTURACION")
		completarDatosSistemas(fax_server, "RSWF_DTI_FAX_SERVER_APL", false, "AF_FAX_SERVER", "GO_DTI_VENTAS")
		completarDatosSistemas(fax_server_segat, "RSWF_DTI_FAX_SERVER_SEGAT_APL", false, "AF_FAX_SERVER_SEGAT", "GO_DTI_VENTAS")
		completarDatosSistemas(fosse, "RSWF_DTI_FOSSE", false, "AF_FOSSE", null)
		completarDatosSistemas(frontbi_cctbi, "RSWF_DTI_FRONTBI", false, "AF_FRONTBI_CCTBI", "GO_DTI_VENTAS")
		completarDatosSistemas(frontbi_dacota, "RSWF_DTI_FRONTBI", false, "AF_FRONTBI_DACOTA", "GO_DTI_FACTURACION")
		completarDatosSistemas(frontbi_smart, "RSWF_DTI_FRONTBI", false, "AF_FRONTBI_SMART", "GO_DTI_FACTURACION")
		completarDatosSistemas(frontbi_tabgo, "RSWF_DTI_FRONTBI", false, "AF_FRONTBI_TABGO", "GO_DTI_VENTAS")
		completarDatosSistemas(frontbi_tabra, "RSWF_DTI_FRONTBI", false, "AF_FRONTBI_TABRA", "GO_DTI_FACTURACION")
		completarDatosSistemas(frontbi_zoom, "RSWF_DTI_FRONTBI", false, "AF_FRONTBI_ZOOM", "GO_DTI_VENTAS")
		completarDatosSistemas(genesis_bill_cycle, "RSWF_DTI_GENESIS_BILL_CYCLE", true, "AF_GENESIS_BILL_CYCLE", "GO_DTI_FACTURACION")
		completarDatosSistemas(genesis_customer, "RSWF_DTI_GENESIS_CUSTOMER", true, "AF_GENESIS_CUSTOMER", "GO_DTI_FACTURACION")
		completarDatosSistemas(genesis_mic, "RSWF_DTI_GENESIS_MIC", true, "AF_GENESIS_MIC", "GO_DTI_FACTURACION")
		completarDatosSistemas(genesis_oyp, "RSWF_DTI_GENESIS_OYP", true, "AF_GENESIS_OYP", "GO_DTI_FACTURACION")
		completarDatosSistemas(genesis_ups, "RSWF_DTI_GENESIS_UPS", true, "AF_GENESIS_UPS", "GO_DTI_FACTURACION")
		completarDatosSistemas(gesprod, "RSWF_DTI_GESPROD", false, "AF_GESPROD", "GO_DTI_VENTAS")
		completarDatosSistemas(gestion_de_emprendimientos, "RSWF_DTI_GESTION_DE_EMPRENDIMIENTOS", false, "AF_GESTION_DE_EMPRENDIMIENTOS", null)
		completarDatosSistemas(gestor_de_comisiones, "RSWF_DTI_DA_VINCI_GESTOR_DE_COMISIONES", false, "AF_GESTOR_DE_COMISIONES", "GO_DTI_VENTAS")
		completarDatosSistemas(girafe, "RSWF_DTI_GIRAFE", false, "AF_GIRAFE", "GO_DTI_VENTAS")
		completarDatosSistemas(guc, "RSWF_DTI_GUC", false, "AF_GUC", "GO_DTI_FACTURACION")
		completarDatosSistemas(interconnect, "RSWF_DTI_INTERCONNECT", false, "AF_INTERCONNECT", null)
		completarDatosSistemas(ipb, "RSWF_DTI_IPB", false, "AF_IPB", "GO_DTI_FACTURACION")
		completarDatosSistemas(iris, "RSWF_ST_Inventarios", false, "AF_IRIS", "GO_DST_SISTEMASTECNICOS")
		completarDatosSistemas(ium, "RSWF_DTI_IUM", false, "AF_IUM", "GO_DTI_FACTURACION")
		completarDatosSistemas(jano, "RSWF_DTI_JANO", false, "AF_JANO", "GO_DTI_VENTAS")
		completarDatosSistemas(jano_proceso_etl, "RSWF_DTI_JANO", false, "AF_JANO_PROCESO_ETL", "GO_DTI_VENTAS")
		completarDatosSistemas(kingdom, "RSWF_DTI_KINGDOM", false, "AF_KINGDOM", "GO_DTI_VENTAS")
		completarDatosSistemas(litigios, "RSWF_SC_LEGALES", false, "AF_LITIGIOS", "GO_SC_SISTEMAS CORPORATIVOS")
		completarDatosSistemas(mac, "RSWF_DTI_MAC", false, "AF_MAC", "GO_DTI_VENTAS")
		completarDatosSistemas(mi_cuenta_back_end, "RSWF_DTI_MICUENTA@", false, "AF_MI_CUENTA_BACK_END", "GO_DTI_VENTAS")
		completarDatosSistemas(mi_cuenta_front_end, "RSWF_DTI_MICUENTA@", false, "AF_MI_CUENTA_FRONT_END", null)
		completarDatosSistemas(nba, "RSWF_DTI_NBA", false, "AF_NBA", null)
		completarDatosSistemas(needit, "RSWF_SC_NEEDIT", false, "AF_NEEDIT", "GO_SC_SIGIT")
		completarDatosSistemas(network_inventory, "RSWF_ST_Inventarios", false, "AF_NETWORK_INVENTORY", "GO_DST_SISTEMASTECNICOS")
		completarDatosSistemas(nred, "RSWF_ST_Inventarios", false, "AF_NRED", "GO_DST_SISTEMASTECNICOS")
		completarDatosSistemas(ods, "RSWF_DTI_ODS", false, "AF_ODS", "GO_DTI_FACTURACION")
		completarDatosSistemas(persa, "RSWF_ST_PERSA", false, "AF_PERSA", null)
		completarDatosSistemas(plataforma_de_precalificacion, "RSWF_ST_PLATAFORMA-PRECALIFICACION", false, "AF_PLATAFORMA_DE_PRECALIFICACION", null)
		completarDatosSistemas(plataforma_de_testing, "RSWF_ST_PLATAFORMA-TESTING", false, "AF_PLATAFORMA_DE_TESTING", null)
		completarDatosSistemas(portal_bi_utf, "RSWF_DTI_PORTAL BI UTF", false, "AF_PORTAL_BI_UTF", "GO_DTI_VENTAS")
		completarDatosSistemas(prod_arnet_portales_arnet, "RSWF_DTI_Venta On line", false, "AF_PROD_ARNET_PORTALES_ARNET", "GO_DTI_VENTAS")
		completarDatosSistemas(prod_arnet_portales_back_office, "RSWF_DTI_Venta On line", false, "AF_PROD_ARNET_PORTALES_BACK_OFFICE", "GO_DTI_VENTAS")
		completarDatosSistemas(prod_arnet_portales_telecom, "RSWF_DTI_Venta On line", false, "AF_PROD_ARNET_PORTALES_TELECOM", "GO_DTI_VENTAS")
		completarDatosSistemas(pyo, "RSWF_DTI_PYO", false, "AF_PYO", "GO_DTI_FACTURACION")
		completarDatosSistemas(qv_asse, "RSWF_SC_QLIKVIEW_CORPORATIVO", false, "AF_QV_ASSE", "GO_SC_SISTEMAS CORPORATIVOS")
		completarDatosSistemas(qv_clie, "RSWF_SC_QLIKVIEW_CORPORATIVO", false, "AF_QV_CLIE", "GO_SC_SISTEMAS CORPORATIVOS")
		completarDatosSistemas(qv_masivo, "RSWF_ST_QVMASIVO", false, "AF_QV_MASIVO", "GO_DST_SISTEMASTECNICOS")
		completarDatosSistemas(qv_nomasivo, "RSWF_ST_QVMASIVO", false, "AF_QV_NOMASIVO", "GO_DST_SISTEMASTECNICOS")
		completarDatosSistemas(qv_remedy, "RSWF_ST_QVMASIVO", false, "AF_QV_REMEDY", "GO_DST_SISTEMASTECNICOS")
		completarDatosSistemas(qv_siag, "RSWF_SC_QLIKVIEW_CORPORATIVO", false, "AF_QV_SIAG", "GO_SC_SISTEMAS CORPORATIVOS")
		completarDatosSistemas(qv_simplit, "RSWF_ST_QVMASIVO", false, "AF_QV_SIMPLIT", "GO_DST_SISTEMASTECNICOS")
		completarDatosSistemas(qv_ssgg, "RSWF_SC_QLIKVIEW_CORPORATIVO", false, "AF_QV_SSGG", "GO_SC_SISTEMAS CORPORATIVOS")
		completarDatosSistemas(remedyred, "RSWF_ST_REMEDYRED", false, "AF_REMEDYRED", "GO_DST_SISTEMASTECNICOS")
		completarDatosSistemas(reporte_aseguramiento, "RSWF_ST_QVMASIVO", false, "AF_REPORTE_ASEGURAMIENTO", "GO_DST_SISTEMASTECNICOS")
		completarDatosSistemas(sas, "RSWF_ST_SISTEMA-AFECTACION-SERVICIOS", false, "AF_SAS", null)
		completarDatosSistemas(scard, "RSWF_DTI_SCARD_COLD", false, "AF_SCARD", "GO_DTI_FACTURACION")
		completarDatosSistemas(segat, "RSWF_ST_SEGAT", false, "AF_SEGAT", "GO_DST_SISTEMASTECNICOS")
		completarDatosSistemas(sei, "RSWF_DTI_SEI", false, "AF_SEI", "GO_DTI_VENTAS")
		completarDatosSistemas(seta, "RSWF_ST_Inventarios", false, "AF_SETA", "GO_DST_SISTEMASTECNICOS")
		completarDatosSistemas(sgct, "RSWF_DTI_SGCT", false, "AF_SGCT", null)
		completarDatosSistemas(sgdatos, "RSWF_ST_SG-DATOS", false, "AF_SGDATOS", null)
		completarDatosSistemas(sgdatos_tp, "RSWF_ST_SG-DATOS", false, "AF_SGDATOS_TP", null)
		completarDatosSistemas(sharepoint, "RSWF_SC_Sharepoint", false, "AF_SHAREPOINT", "GO_SC_SISTEMAS CORPORATIVOS")
		completarDatosSistemas(siaf, "RSWF_SC_FIN", false, "AF_SIAF", "GO_SC_SISTEMAS CORPORATIVOS")
		completarDatosSistemas(sideral, "RSWF_ST_SIDERAL", false, "AF_SIDERAL", null)
		completarDatosSistemas(siebel_customer_centric, "RSWF_DTI_SIEBEL", false, "AF_SIEBEL_CUSTOMER_CENTRIC", "GO_DTI_VENTAS")
		completarDatosSistemas(signo_cap, "RSWF_SC_FIN", false, "AF_SIGNO_CAP", "GO_SC_SISTEMAS CORPORATIVOS")
		completarDatosSistemas(signo_cobranzas, "RSWF_SC_FIN", false, "AF_SIGNO_COBRANZAS", "GO_SC_SISTEMAS CORPORATIVOS")
		completarDatosSistemas(signo_compras, "RSWF_SC_MM", false, "AF_SIGNO_COMPRAS", "GO_SC_SISTEMAS CORPORATIVOS")
		completarDatosSistemas(signo_compras_estrategias, "RSWF_SC_MM", false, "AF_SIGNO_COMPRAS_ESTRATEGIAS", "GO_SC_SISTEMAS CORPORATIVOS")
		completarDatosSistemas(signo_ess, "RSWF_SC_APLRRHH", false, "AF_SIGNO_ESS", "GO_SC_SISTEMAS CORPORATIVOS")
		completarDatosSistemas(signo_ess_celulares, "RSWF_SC_APLRRHH", false, "AF_SIGNO_ESS_CELULARES", "GO_SC_SISTEMAS CORPORATIVOS")
		completarDatosSistemas(signo_ess_mi_perfil, "RSWF_SC_APLRRHH", false, "AF_SIGNO_ESS_MI PERFIL", "GO_SC_SISTEMAS CORPORATIVOS")
		completarDatosSistemas(signo_ess_mis_licencias, "RSWF_SC_APLRRHH", false, "AF_SIGNO_ESS_MIS LICENCIAS", "GO_SC_SISTEMAS CORPORATIVOS")
		completarDatosSistemas(signo_ess_plan_empleados, "RSWF_SC_APLRRHH", false, "AF_SIGNO_ESS_PLAN EMPLEADOS", "GO_SC_SISTEMAS CORPORATIVOS")
		completarDatosSistemas(signo_ess_recibo_electronico, "RSWF_SC_APLRRHH", false, "AF_SIGNO_ESS_RECIBO ELECTRONICO", "GO_SC_SISTEMAS CORPORATIVOS")
		completarDatosSistemas(signo_facturas_web, "RSWF_SC_FIN", false, "AF_SIGNO_FACTURAS_WEB", "GO_SC_SISTEMAS CORPORATIVOS")
		completarDatosSistemas(signo_finanzas, "RSWF_SC_FIN", false, "AF_SIGNO_FINANZAS", "GO_SC_SISTEMAS CORPORATIVOS")
		completarDatosSistemas(signo_finanzas_py, "RSWF_SC_FIN", false, "AF_SIGNO_FINANZAS_PY", "GO_SC_SISTEMAS CORPORATIVOS")
		completarDatosSistemas(signo_guiainterna, "RSWF_SC_Neoportal", false, "AF_SIGNO_GUIAINTERNA", "GO_SC_SISTEMAS CORPORATIVOS")
		completarDatosSistemas(signo_logistica, "RSWF_SC_MM", false, "AF_SIGNO_LOGISTICA", "GO_SC_SISTEMAS CORPORATIVOS")
		completarDatosSistemas(signo_mantenimiento, "RSWF_SC_PMA", false, "AF_SIGNO_MANTENIMIENTO", "GO_SC_SISTEMAS CORPORATIVOS")
		completarDatosSistemas(signo_neoportal, "RSWF_SC_Neoportal", false, "AF_SIGNO_NEOPORTAL", "GO_SC_SISTEMAS CORPORATIVOS")
		completarDatosSistemas(signo_proyectos, "RSWF_SC_PMA", false, "AF_SIGNO_PROYECTOS", "GO_SC_SISTEMAS CORPORATIVOS")
		completarDatosSistemas(signo_real_estate, "RSWF_SC_PMA", false, "AF_SIGNO_REAL_ESTATE", "GO_SC_SISTEMAS CORPORATIVOS")
		completarDatosSistemas(signo_registro_firmas, "RSWF_SC_Neoportal", false, "AF_SIGNO_REGISTRO_FIRMAS", "GO_SC_SISTEMAS CORPORATIVOS")
		completarDatosSistemas(signo_rrhh, "RSWF_SC_APLRRHH", false, "AF_SIGNO_RRHH", "GO_SC_SISTEMAS CORPORATIVOS")
		completarDatosSistemas(signo_sap_grc, "RSWF_SC_APLRRHH", false, "AF_SIGNO_SAP_GRC", "GO_SC_SISTEMAS CORPORATIVOS")
		completarDatosSistemas(signo_sh_agendablog, "RSWF_SC_Sharepoint", false, "AF_SIGNO_SH_AGENDABLOG", "GO_SC_SISTEMAS CORPORATIVOS")
		completarDatosSistemas(signo_sh_blog_copamundo, "RSWF_SC_Sharepoint", false, "AF_SIGNO_SH_BLOG_COPAMUNDO", "GO_SC_SISTEMAS CORPORATIVOS")
		completarDatosSistemas(signo_sh_blog_equilibrio, "RSWF_SC_Sharepoint", false, "AF_SIGNO_SH_BLOG_EQUILIBRIO", "GO_SC_SISTEMAS CORPORATIVOS")
		completarDatosSistemas(signo_sh_blog_teletrabajo, "RSWF_SC_Sharepoint", false, "AF_SIGNO_SH_BLOG_TELETRABAJO", "GO_SC_SISTEMAS CORPORATIVOS")
		completarDatosSistemas(signo_sh_colonia, "RSWF_SC_Sharepoint", false, "AF_SIGNO_SH_COLONIA", "GO_SC_SISTEMAS CORPORATIVOS")
		completarDatosSistemas(signo_sh_ddjj, "RSWF_SC_Sharepoint", false, "AF_SIGNO_SH_DDJJ", "GO_SC_SISTEMAS CORPORATIVOS")
		completarDatosSistemas(signo_sh_imp_ganancias, "RSWF_SC_Sharepoint", false, "AF_SIGNO_SH_IMP_GANANCIAS", "GO_SC_SISTEMAS CORPORATIVOS")
		completarDatosSistemas(signo_sh_lineas_servicio, "RSWF_SC_Sharepoint", false, "AF_SIGNO_SH_LINEAS_SERVICIO", "GO_SC_SISTEMAS CORPORATIVOS")
		completarDatosSistemas(signo_sh_wiki, "RSWF_SC_Sharepoint", false, "AF_SIGNO_SH_WIKI", "GO_SC_SISTEMAS CORPORATIVOS")
		completarDatosSistemas(signo_sorteos, "RSWF_SC_Neoportal", false, "AF_SIGNO_SORTEOS", "GO_SC_SISTEMAS CORPORATIVOS")
		completarDatosSistemas(signo_tesoreria, "RSWF_SC_FIN", false, "AF_SIGNO_TESORERIA", "GO_SC_SISTEMAS CORPORATIVOS")
		completarDatosSistemas(signo_veconp, "RSWF_SC_LEGALES", false, "AF_SIGNO_VECONP", "GO_SC_SISTEMAS CORPORATIVOS")
		completarDatosSistemas(signo_voluntariado, "RSWF_SC_Neoportal", false, "AF_SIGNO_VOLUNTARIADO", "GO_SC_SISTEMAS CORPORATIVOS")
		completarDatosSistemas(simplit, "RSWF_SC_SIMPLIT", false, "AF_SIMPLIT", "GO_SC_SIGIT")
		completarDatosSistemas(sir, "RSWF_DTI_SIR", false, "AF_SIR", "GO_DTI_VENTAS")
		completarDatosSistemas(smart, "RSWF_DTI_SMART", false, "AF_SMART", "GO_DTI_FACTURACION")
		completarDatosSistemas(sp_calidad_redesyservicios, "RSWF_ST_QVMASIVO", false, "AF_SP_CALIDAD_REDESYSERVICIOS", null)
		completarDatosSistemas(suc2, "RSWF_DTI_SUC2", false, "AF_SUC2", null)
		completarDatosSistemas(supervision_vpn, "RSWF_ST_SUPERVISION-VPNS", false, "AF_SUPERVISION_VPN", null)
		completarDatosSistemas(sur, "RSWF_ST_SUR", false, "AF_SUR", "GO_DST_SISTEMASTECNICOS")
		completarDatosSistemas(sus, "RSWF_ST_SUS", false, "AF_SUS", null)
		completarDatosSistemas(tab_go, "RSWF_DTI_TAB_GO", false, "AF_TAB_GO", "GO_DTI_VENTAS")
		completarDatosSistemas(tab_ra, "RSWF_DTI_TAB_RA", false, "AF_TAB_RA", "GO_DTI_FACTURACION")
		completarDatosSistemas(tas, "RSWF_ST_TAS", false, "AF_TAS", "GO_DST_SISTEMASTECNICOS")
		completarDatosSistemas(temis, "RSWF_DTI_TEMIS", false, "AF_TEMIS", null)
		completarDatosSistemas(transformer, "RSWF_DTI_TRANSFORMER", false, "AF_TRANSFORMER", "GO_DTI_VENTAS")
		completarDatosSistemas(truman, "RSWF_DTI_TRUMAN", false, "AF_TRUMAN", "GO_DTI_VENTAS")
		completarDatosSistemas(ttusa_prepago_api, "RSWF_DTI_TECO_USA_PREPAGO", false, "AF_TTUSA_PREPAGO_API", "GO_DTI_VENTAS")
		completarDatosSistemas(ttusa_prepago_gestion, "RSWF_DTI_TECO_USA_PREPAGO", false, "AF_TTUSA_PREPAGO_GESTION", "GO_DTI_VENTAS")
		completarDatosSistemas(utopia, "RSWF_DTI_UTOPIA", false, "AF_UTOPIA", "GO_DTI_VENTAS")
		completarDatosSistemas(vantive_field_service, "RSWF_DTI_VANTIVE_FIELD_SERVICE", false, "AF_VANTIVE_FIELD_SERVICE", null)
		completarDatosSistemas(web_adsl_tecnica, "RSWF_ST_Inventarios", false, "AF_WEB_ADSL_TECNICA", "GO_DST_SISTEMASTECNICOS")
		completarDatosSistemas(workflow_tecnico, "RSWF_DTI_WORKFLOW_TECNICO", false, "AF_WORKFLOW_TECNICO", null)
		completarDatosSistemas(yadas, "RSWF_DTI_ADAS", false, "AF_YADAS", "GO_DTI_VENTAS")
		completarDatosSistemas(zoom, "RSWF_DTI_ZOOM", false, "AF_ZOOM", "GO_DTI_VENTAS")
	}


	// Sistemas y áreas de Soporte
	private def definirAreaSoporteSistemas() {
		def ipb = Sistema.findByDescripcion("IPB_APL")
		def genesis_bill_cycle = Sistema.findByDescripcion("GENESIS_BILL_CYCLE")
		def genesis_ups = Sistema.findByDescripcion("GENESIS_UPS")
		def calipso_facturacion = Sistema.findByDescripcion("CALIPSO_FACTURACION")
		def genesis_customer = Sistema.findByDescripcion("GENESIS_CUSTOMER")
		def calipso_prefacturacion = Sistema.findByDescripcion("CALIPSO_PREFACTURACION")
		def calipso_cobros = Sistema.findByDescripcion("CALIPSO_COBROS")
		def factesp = Sistema.findByDescripcion("FACTESP_APL")
		def nba = Sistema.findByDescripcion("NBA_APL")
		def utopia = Sistema.findByDescripcion("UTOPIA_APL")

		def as_planeamiento = AreaSoporte.findByDescripcion("Planeamiento y Evaluaciones Econ\u00F3micas")
		def as_operacionesFact = AreaSoporte.findByDescripcion("Operaciones de Facturaci\u00F3n")

		as_planeamiento.addToSistemas(ipb)
		as_planeamiento.addToSistemas(genesis_bill_cycle)
		as_planeamiento.addToSistemas(genesis_ups)
		as_planeamiento.addToSistemas(calipso_facturacion)
		as_operacionesFact.addToSistemas(genesis_bill_cycle)
		as_operacionesFact.addToSistemas(genesis_ups)
		as_operacionesFact.addToSistemas(genesis_customer)
		as_operacionesFact.addToSistemas(calipso_facturacion)
		as_operacionesFact.addToSistemas(calipso_prefacturacion)
		as_operacionesFact.addToSistemas(calipso_cobros)
		as_operacionesFact.addToSistemas(factesp)
		as_operacionesFact.addToSistemas(ipb)
		as_operacionesFact.addToSistemas(nba)
		as_operacionesFact.addToSistemas(utopia)
	}


	// Sistemas y releases
	private def definirReleases() {
		new Release(sistema: Sistema.findByDescripcion("GENESIS_BILL_CYCLE"), descripcion: "BIE1204.05.05").save(failOnError: true)
		new Release(sistema: Sistema.findByDescripcion("GENESIS_BILL_CYCLE"), descripcion: "BIE1204.05.06").save(failOnError: true)
		new Release(sistema: Sistema.findByDescripcion("GENESIS_BILL_CYCLE"), descripcion: "BIE1204.05.07").save(failOnError: true)
		new Release(sistema: Sistema.findByDescripcion("GENESIS_BILL_CYCLE"), descripcion: "BIE1204.05.08").save(failOnError: true)
		new Release(sistema: Sistema.findByDescripcion("GENESIS_BILL_CYCLE"), descripcion: "BIE1204.05.09").save(failOnError: true)
		new Release(sistema: Sistema.findByDescripcion("GENESIS_CUSTOMER"), descripcion: "BIE1204.05.05").save(failOnError: true)
		new Release(sistema: Sistema.findByDescripcion("GENESIS_CUSTOMER"), descripcion: "BIE1204.05.06").save(failOnError: true)
		new Release(sistema: Sistema.findByDescripcion("GENESIS_CUSTOMER"), descripcion: "BIE1204.05.08").save(failOnError: true)
		new Release(sistema: Sistema.findByDescripcion("GENESIS_CUSTOMER"), descripcion: "BIE1204.05.09").save(failOnError: true)
		new Release(sistema: Sistema.findByDescripcion("GENESIS_MIC"), descripcion: "BIE1204.05.05").save(failOnError: true)
		new Release(sistema: Sistema.findByDescripcion("GENESIS_MIC"), descripcion: "BIE1204.05.06").save(failOnError: true)
		new Release(sistema: Sistema.findByDescripcion("GENESIS_MIC"), descripcion: "BIE1204.05.07").save(failOnError: true)
		new Release(sistema: Sistema.findByDescripcion("GENESIS_MIC"), descripcion: "BIE1204.05.08").save(failOnError: true)
		new Release(sistema: Sistema.findByDescripcion("GENESIS_MIC"), descripcion: "BIE1204.05.09").save(failOnError: true)
		new Release(sistema: Sistema.findByDescripcion("GENESIS_OYP"), descripcion: "BIE1204.05.05").save(failOnError: true)
		new Release(sistema: Sistema.findByDescripcion("GENESIS_OYP"), descripcion: "BIE1204.05.06").save(failOnError: true)
		new Release(sistema: Sistema.findByDescripcion("GENESIS_OYP"), descripcion: "BIE1204.05.07").save(failOnError: true)
		new Release(sistema: Sistema.findByDescripcion("GENESIS_OYP"), descripcion: "BIE1204.05.08").save(failOnError: true)
		new Release(sistema: Sistema.findByDescripcion("GENESIS_OYP"), descripcion: "BIE1204.05.09").save(failOnError: true)
		new Release(sistema: Sistema.findByDescripcion("GENESIS_UPS"), descripcion: "BIE1204.05.05").save(failOnError: true)
		new Release(sistema: Sistema.findByDescripcion("GENESIS_UPS"), descripcion: "BIE1204.05.06").save(failOnError: true)
		new Release(sistema: Sistema.findByDescripcion("GENESIS_UPS"), descripcion: "BIE1204.05.07").save(failOnError: true)
		new Release(sistema: Sistema.findByDescripcion("GENESIS_UPS"), descripcion: "BIE1204.05.08").save(failOnError: true)
		new Release(sistema: Sistema.findByDescripcion("GENESIS_UPS"), descripcion: "BIE1204.05.09").save(failOnError: true)
		new Release(sistema: Sistema.findByDescripcion("NBA_APL"), descripcion: "BIE1204.05.05").save(failOnError: true)
		new Release(sistema: Sistema.findByDescripcion("NBA_APL"), descripcion: "BIE1204.05.06").save(failOnError: true)
		new Release(sistema: Sistema.findByDescripcion("NBA_APL"), descripcion: "BIE1204.05.07").save(failOnError: true)
		new Release(sistema: Sistema.findByDescripcion("NBA_APL"), descripcion: "BIE1204.05.08").save(failOnError: true)
		new Release(sistema: Sistema.findByDescripcion("NBA_APL"), descripcion: "BIE1204.05.09").save(failOnError: true)
		new Release(sistema: Sistema.findByDescripcion("PYO_APL"), descripcion: "BIE1204.05.05").save(failOnError: true)
		new Release(sistema: Sistema.findByDescripcion("PYO_APL"), descripcion: "BIE1204.05.06").save(failOnError: true)
		new Release(sistema: Sistema.findByDescripcion("PYO_APL"), descripcion: "BIE1204.05.07").save(failOnError: true)
		new Release(sistema: Sistema.findByDescripcion("PYO_APL"), descripcion: "BIE1204.05.08").save(failOnError: true)
		new Release(sistema: Sistema.findByDescripcion("PYO_APL"), descripcion: "BIE1204.05.09").save(failOnError: true)
	}


	// Sistemas y Software Factories
	private def definirSistemaSWF() {
		def v112_com = Sistema.findByDescripcion("112.COM_APL")
		def abonados_en_guia = Sistema.findByDescripcion("ABONADOS_EN_GUIA_APL")
		def abuse = Sistema.findByDescripcion("ABUSE_APL")
		def acdserver = Sistema.findByDescripcion("ACDSERVER_APL")
		def adas = Sistema.findByDescripcion("ADAS_APL")
		def adhesion_factura_digital = Sistema.findByDescripcion("ADHESION_FACTURA_DIGITAL_APL")
		def adm_de_contactos = Sistema.findByDescripcion("ADM_DE_CONTACTOS_APL")
		def adm_de_contactos_interfaces = Sistema.findByDescripcion("ADM_DE_CONTACTOS_INTERFACES")
		def adsl_ventas = Sistema.findByDescripcion("ADSL_VENTAS_APL")
		def agilit = Sistema.findByDescripcion("AGILIT_APL")
		def amadeus = Sistema.findByDescripcion("AMADEUS_APL")
		def aris = Sistema.findByDescripcion("ARIS_APL")
		def asset_center = Sistema.findByDescripcion("ASSET_CENTER_APL")
		def atlantis = Sistema.findByDescripcion("ATLANTIS_APL")
		def bas = Sistema.findByDescripcion("BAS_APL")
		def base_entorno_geo = Sistema.findByDescripcion("BASE_ENTORNO_GEO_APL")
		def baseip = Sistema.findByDescripcion("BASEIP_APL")
		def belen = Sistema.findByDescripcion("BELEN_APL")
		def bios_hal = Sistema.findByDescripcion("BIOS_HAL_APL")
		def calipso_cobros = Sistema.findByDescripcion("CALIPSO_COBROS")
		def calipso_facturacion = Sistema.findByDescripcion("CALIPSO_FACTURACION")
		def calipso_prefacturacion = Sistema.findByDescripcion("CALIPSO_PREFACTURACION")
		def campaign_management = Sistema.findByDescripcion("CAMPAIGN_MANAGEMENT_APL")
		def carriers_interconectantes = Sistema.findByDescripcion("CARRIERS_INTERCONECTANTES_APL")
		def catalogo_pys = Sistema.findByDescripcion("CATALOGO_PYS_APL")
		def cctbi = Sistema.findByDescripcion("CCTBI_APL")
		def central_parq = Sistema.findByDescripcion("CENTRAL_PARQ_APL")
		def centro_validador = Sistema.findByDescripcion("CENTRO_VALIDADOR_APL")
		def centro_validador_web = Sistema.findByDescripcion("CENTRO_VALIDADOR_WEB")
		def certificaciones = Sistema.findByDescripcion("CERTIFICACIONES_APL")
		def ciclope = Sistema.findByDescripcion("CICLOPE_APL")
		def citas = Sistema.findByDescripcion("CITAS_APL")
		def cms = Sistema.findByDescripcion("CMS_APL")
		def cnm = Sistema.findByDescripcion("CNM_APL")
		def cobranza_electronica_front_end = Sistema.findByDescripcion("COBRANZA_ELECTRONICA_FRONT_END")
		def cobranza_integrada_front_end = Sistema.findByDescripcion("COBRANZA_INTEGRADA_FRONT_END")
		def cris = Sistema.findByDescripcion("CRIS_APL")
		def crm_use_oyp = Sistema.findByDescripcion("CRM_USE_OYP_APL")
		def cuenta_online_back_end = Sistema.findByDescripcion("CUENTA_ONLINE_BACK_END_APL")
		def cuenta_online_front_end = Sistema.findByDescripcion("CUENTA_ONLINE_FRONT_END_APL")
		def cuenta_online_pyme_front_end = Sistema.findByDescripcion("CUENTA_ONLINE_PYME_FRONT_END_APL")
		def dacota = Sistema.findByDescripcion("DACOTA_APL")
		def data_activator = Sistema.findByDescripcion("DATA_ACTIVATOR_APL")
		def ddt = Sistema.findByDescripcion("DDT_APL")
		def deimos = Sistema.findByDescripcion("DEIMOS_APL")
		def delta = Sistema.findByDescripcion("DELTA_APL")
		def denarius = Sistema.findByDescripcion("DENARIUS_APL")
		def diaria_net = Sistema.findByDescripcion("DIARIA_.NET_APL")
		def e_mailing_front_end = Sistema.findByDescripcion("E-MAILING_FRONT_END_APL")
		def epm_dsi = Sistema.findByDescripcion("EPM_DSI_APL")
		def estadistica_0800_0810_back_end = Sistema.findByDescripcion("ESTADISTICA_0800/0810_BACK_END")
		def estadistica_0800_0810_front_end = Sistema.findByDescripcion("ESTADISTICA_0800/0810_FRONT_END")
		def eva_cuentas_corrientes_masivo = Sistema.findByDescripcion("EVA_CUENTAS_CORRIENTES_MASIVO")
		def eva_morosidad_cobranzas_gc_ws = Sistema.findByDescripcion("EVA_MOROSIDAD/COBRANZAS_GC/WS")
		def eva_morosidad_financiacion_masivo = Sistema.findByDescripcion("EVA_MOROSIDAD/FINANCIACION_MASIVO")
		def eva_postbaja_computo_impositivo = Sistema.findByDescripcion("EVA_POSTBAJA/COMPUTO_IMPOSITIVO")
		def factesp = Sistema.findByDescripcion("FACTESP_APL")
		def fax_server = Sistema.findByDescripcion("FAX_SERVER_APL")
		def fax_server_segat = Sistema.findByDescripcion("FAX_SERVER_SEGAT_APL")
		def fosse = Sistema.findByDescripcion("FOSSE_APL")
		def frontbi_cctbi = Sistema.findByDescripcion("FRONTBI_CCTBI_APL")
		def frontbi_dacota = Sistema.findByDescripcion("FRONTBI_DACOTA_APL")
		def frontbi_smart = Sistema.findByDescripcion("FRONTBI_SMART_APL")
		def frontbi_tabgo = Sistema.findByDescripcion("FRONTBI_TABGO_APL")
		def frontbi_tabra = Sistema.findByDescripcion("FRONTBI_TABRA_APL")
		def frontbi_zoom = Sistema.findByDescripcion("FRONTBI_ZOOM_APL")
		def genesis_bill_cycle = Sistema.findByDescripcion("GENESIS_BILL_CYCLE")
		def genesis_customer = Sistema.findByDescripcion("GENESIS_CUSTOMER")
		def genesis_mic = Sistema.findByDescripcion("GENESIS_MIC")
		def genesis_oyp = Sistema.findByDescripcion("GENESIS_OYP")
		def genesis_ups = Sistema.findByDescripcion("GENESIS_UPS")
		def gesprod = Sistema.findByDescripcion("GESPROD_APL")
		def gestion_de_emprendimientos = Sistema.findByDescripcion("GESTION_DE_EMPRENDIMIENTOS_APL")
		def gestor_de_comisiones = Sistema.findByDescripcion("GESTOR_DE_COMISIONES_APL")
		def girafe = Sistema.findByDescripcion("GIRAFE_APL")
		def guc = Sistema.findByDescripcion("GUC_APL")
		def interconnect = Sistema.findByDescripcion("INTERCONNECT_APL")
		def ipb = Sistema.findByDescripcion("IPB_APL")
		def iris = Sistema.findByDescripcion("IRIS_APL")
		def ium = Sistema.findByDescripcion("IUM_APL")
		def jano = Sistema.findByDescripcion("JANO_APL")
		def jano_proceso_etl = Sistema.findByDescripcion("JANO_PROCESO_ETL")
		def kingdom = Sistema.findByDescripcion("KINGDOM_APL")
		def litigios = Sistema.findByDescripcion("LITIGIOS_APL")
		def mac = Sistema.findByDescripcion("MAC_APL")
		def mi_cuenta_back_end = Sistema.findByDescripcion("MI_CUENTA_BACK_END")
		def mi_cuenta_front_end = Sistema.findByDescripcion("MI_CUENTA_FRONT_END")
		def nba = Sistema.findByDescripcion("NBA_APL")
		def needit = Sistema.findByDescripcion("NEEDIT_APL")
		def network_inventory = Sistema.findByDescripcion("NETWORK_INVENTORY_APL")
		def nred = Sistema.findByDescripcion("NRED_APL")
		def ods = Sistema.findByDescripcion("ODS_APL")
		def persa = Sistema.findByDescripcion("PERSA_APL")
		def plataforma_de_precalificacion = Sistema.findByDescripcion("PLATAFORMA_DE_PRECALIFICACION_APL")
		def plataforma_de_testing = Sistema.findByDescripcion("PLATAFORMA_DE_TESTING_APL")
		def portal_bi_utf = Sistema.findByDescripcion("PORTAL_BI_UTF_APL")
		def prod_arnet_portales_arnet = Sistema.findByDescripcion("PROD_ARNET_PORTALES_ARNET")
		def prod_arnet_portales_back_office = Sistema.findByDescripcion("PROD_ARNET_PORTALES_BACK_OFFICE")
		def prod_arnet_portales_telecom = Sistema.findByDescripcion("PROD_ARNET_PORTALES_TELECOM")
		def pyo = Sistema.findByDescripcion("PYO_APL")
		def qv_asse = Sistema.findByDescripcion("QV_ASSE_APL")
		def qv_clie = Sistema.findByDescripcion("QV_CLIE_APL")
		def qv_masivo = Sistema.findByDescripcion("QV_MASIVO_APL")
		def qv_nomasivo = Sistema.findByDescripcion("QV_NOMASIVO_APL")
		def qv_remedy = Sistema.findByDescripcion("QV_REMEDY_APL")
		def qv_siag = Sistema.findByDescripcion("QV_SIAG_APL")
		def qv_simplit = Sistema.findByDescripcion("QV_SIMPLIT_APL")
		def qv_ssgg = Sistema.findByDescripcion("QV_SSGG_APL")
		def remedyred = Sistema.findByDescripcion("REMEDYRED_APL")
		def reporte_aseguramiento = Sistema.findByDescripcion("REPORTE_ASEGURAMIENTO_APL")
		def sas = Sistema.findByDescripcion("SAS_APL")
		def scard = Sistema.findByDescripcion("SCARD_APL")
		def segat = Sistema.findByDescripcion("SEGAT_APL")
		def sei = Sistema.findByDescripcion("SEI_APL")
		def seta = Sistema.findByDescripcion("SETA_APL")
		def sgct = Sistema.findByDescripcion("SGCT_APL")
		def sgdatos = Sistema.findByDescripcion("SGDATOS_APL")
		def sgdatos_tp = Sistema.findByDescripcion("SGDATOS_TP_APL")
		def sharepoint = Sistema.findByDescripcion("SHAREPOINT_APL")
		def siaf = Sistema.findByDescripcion("SIAF_APL")
		def sideral = Sistema.findByDescripcion("SIDERAL_APL")
		def siebel_customer_centric = Sistema.findByDescripcion("SIEBEL_CUSTOMER_CENTRIC_APL")
		def signo_cap = Sistema.findByDescripcion("SIGNO_CAP")
		def signo_cobranzas = Sistema.findByDescripcion("SIGNO_COBRANZAS")
		def signo_compras = Sistema.findByDescripcion("SIGNO_COMPRAS")
		def signo_compras_estrategias = Sistema.findByDescripcion("SIGNO_COMPRAS_ESTRATEGIAS")
		def signo_ess = Sistema.findByDescripcion("SIGNO_ESS")
		def signo_ess_celulares = Sistema.findByDescripcion("SIGNO_ESS_CELULARES_APL")
		def signo_ess_mi_perfil = Sistema.findByDescripcion("SIGNO_ESS_MI PERFIL_APL")
		def signo_ess_mis_licencias = Sistema.findByDescripcion("SIGNO_ESS_MIS LICENCIAS_APL")
		def signo_ess_plan_empleados = Sistema.findByDescripcion("SIGNO_ESS_PLAN EMPLEADOS_APL")
		def signo_ess_recibo_electronico = Sistema.findByDescripcion("SIGNO_ESS_RECIBO ELECTRONICO_APL")
		def signo_facturas_web = Sistema.findByDescripcion("SIGNO_FACTURAS_WEB")
		def signo_finanzas = Sistema.findByDescripcion("SIGNO_FINANZAS")
		def signo_finanzas_py = Sistema.findByDescripcion("SIGNO_FINANZAS_PY")
		def signo_guiainterna = Sistema.findByDescripcion("SIGNO_GUIAINTERNA")
		def signo_logistica = Sistema.findByDescripcion("SIGNO_LOGISTICA")
		def signo_mantenimiento = Sistema.findByDescripcion("SIGNO_MANTENIMIENTO")
		def signo_neoportal = Sistema.findByDescripcion("SIGNO_NEOPORTAL_APL")
		def signo_proyectos = Sistema.findByDescripcion("SIGNO_PROYECTOS")
		def signo_real_estate = Sistema.findByDescripcion("SIGNO_REAL_ESTATE")
		def signo_registro_firmas = Sistema.findByDescripcion("SIGNO_REGISTRO_FIRMAS")
		def signo_rrhh = Sistema.findByDescripcion("SIGNO_RRHH")
		def signo_sap_grc = Sistema.findByDescripcion("SIGNO_SAP_GRC")
		def signo_sh_agendablog = Sistema.findByDescripcion("SIGNO_SH_AGENDABLOG_APL")
		def signo_sh_blog_copamundo = Sistema.findByDescripcion("SIGNO_SH_BLOG_COPAMUNDO_APL")
		def signo_sh_blog_equilibrio = Sistema.findByDescripcion("SIGNO_SH_BLOG_EQUILIBRIO_APL")
		def signo_sh_blog_teletrabajo = Sistema.findByDescripcion("SIGNO_SH_BLOG_TELETRABAJO_APL")
		def signo_sh_colonia = Sistema.findByDescripcion("SIGNO_SH_COLONIA_APL")
		def signo_sh_ddjj = Sistema.findByDescripcion("SIGNO_SH_DDJJ_APL")
		def signo_sh_imp_ganancias = Sistema.findByDescripcion("SIGNO_SH_IMP_GANANCIAS_APL")
		def signo_sh_lineas_servicio = Sistema.findByDescripcion("SIGNO_SH_LINEAS_SERVICIO_APL")
		def signo_sh_wiki = Sistema.findByDescripcion("SIGNO_SH_WIKI_APL")
		def signo_sorteos = Sistema.findByDescripcion("SIGNO_SORTEOS")
		def signo_tesoreria = Sistema.findByDescripcion("SIGNO_TESORERIA")
		def signo_veconp = Sistema.findByDescripcion("SIGNO_VECONP")
		def signo_voluntariado = Sistema.findByDescripcion("SIGNO_VOLUNTARIADO")
		def simplit = Sistema.findByDescripcion("SIMPLIT_APL")
		def sir = Sistema.findByDescripcion("SIR_APL")
		def smart = Sistema.findByDescripcion("SMART_APL")
		def sp_calidad_redesyservicios = Sistema.findByDescripcion("SP_CALIDAD_REDESYSERVICIOS_APL")
		def suc2 = Sistema.findByDescripcion("SUC2_APL")
		def supervision_vpn = Sistema.findByDescripcion("SUPERVISION_VPN_APL")
		def sur = Sistema.findByDescripcion("SUR_APL")
		def sus = Sistema.findByDescripcion("SUS_APL")
		def tab_go = Sistema.findByDescripcion("TAB_GO_APL")
		def tab_ra = Sistema.findByDescripcion("TAB_RA_APL")
		def tas = Sistema.findByDescripcion("TAS_APL")
		def temis = Sistema.findByDescripcion("TEMIS_APL")
		def transformer = Sistema.findByDescripcion("TRANSFORMER_APL")
		def truman = Sistema.findByDescripcion("TRUMAN_APL")
		def ttusa_prepago_api = Sistema.findByDescripcion("TTUSA_PREPAGO_API")
		def ttusa_prepago_gestion = Sistema.findByDescripcion("TTUSA_PREPAGO_GESTION")
		def utopia = Sistema.findByDescripcion("UTOPIA_APL")
		def vantive_field_service = Sistema.findByDescripcion("VANTIVE_FIELD_SERVICE_APL")
		def web_adsl_tecnica = Sistema.findByDescripcion("WEB_ADSL_TECNICA_APL")
		def workflow_tecnico = Sistema.findByDescripcion("WORKFLOW_TECNICO_APL")
		def yadas = Sistema.findByDescripcion("YADAS_APL")
		def zoom = Sistema.findByDescripcion("ZOOM_APL")

		addSWFtoSistema(v112_com, SoftwareFactory.findByGrupoLDAP("SWF_DTI_112.COM_PROVEEDOR"))
		addSWFtoSistema(abonados_en_guia, SoftwareFactory.findByGrupoLDAP("SWF_DTI_ABONADOS_EN_GUIA_PROVEEDOR"))
		addSWFtoSistema(abonados_en_guia, SoftwareFactory.findByGrupoLDAP("SWF_DTI_ABONADOS_EN_GUIA_TELECOM"))
		addSWFtoSistema(abuse, SoftwareFactory.findByGrupoLDAP("SWF_ST_ABUSE_TELECOM"))
		addSWFtoSistema(abuse, SoftwareFactory.findByGrupoLDAP("SWF_ST_ABUSE_SOFRECOM_PROVEEDOR"))
		addSWFtoSistema(acdserver, SoftwareFactory.findByGrupoLDAP("SWF_DTI_ACDSERVER_PROVEEDOR"))
		addSWFtoSistema(acdserver, SoftwareFactory.findByGrupoLDAP("SWF_DTI_ACDSERVER_TELECOM"))
		addSWFtoSistema(adas, SoftwareFactory.findByGrupoLDAP("SWF_DTI_ADAS_PROVEEDOR"))
		addSWFtoSistema(adas, SoftwareFactory.findByGrupoLDAP("SWF_DTI_ADAS_TELECOM"))
		addSWFtoSistema(adhesion_factura_digital, SoftwareFactory.findByGrupoLDAP("SWF_DTI_ADHESION_FACTURA_DIGITAL_PROVEEDOR"))
		addSWFtoSistema(adm_de_contactos, SoftwareFactory.findByGrupoLDAP("SWF_DTI_ADM_DE_CONTACTOS_TELECOM"))
		addSWFtoSistema(adm_de_contactos_interfaces, SoftwareFactory.findByGrupoLDAP("SWF_DTI_ADM_DE_CONTACTOS_TELECOM"))
		addSWFtoSistema(adsl_ventas, SoftwareFactory.findByGrupoLDAP("SWF_DTI_VENTA ADSL_TELECOM"))
		addSWFtoSistema(agilit, SoftwareFactory.findByGrupoLDAP("SWF_ST_AGILIT_TELECOM"))
		addSWFtoSistema(agilit, SoftwareFactory.findByGrupoLDAP("SWF_ST_AGILIT_SPC_PROVEEDOR"))
		addSWFtoSistema(amadeus, SoftwareFactory.findByGrupoLDAP("SWF_DTI_AMADEUS_PROVEEDOR"))
		addSWFtoSistema(amadeus, SoftwareFactory.findByGrupoLDAP("SWF_DTI_AMADEUS_TELECOM"))
		addSWFtoSistema(aris, SoftwareFactory.findByGrupoLDAP("SWF_SC_APLRRHH_TELECOM"))
		addSWFtoSistema(aris, SoftwareFactory.findByGrupoLDAP("SWF_SC_ARIS_PROVEEDOR"))
		addSWFtoSistema(asset_center, SoftwareFactory.findByGrupoLDAP("SWF_SC_ASSET_CENTER_TELECOM"))
		addSWFtoSistema(asset_center, SoftwareFactory.findByGrupoLDAP("SWF_SC_ASSET_CENTER_PROVEEDOR"))
		addSWFtoSistema(atlantis, SoftwareFactory.findByGrupoLDAP("SWF_ST_INVENTARIOS_TELECOM"))
		addSWFtoSistema(atlantis, SoftwareFactory.findByGrupoLDAP("SWF_ST_INVENTARIOS_SPC_PROVEEDOR"))
		addSWFtoSistema(bas, SoftwareFactory.findByGrupoLDAP("SWF_DTI_BAS_PROVEEDOR"))
		addSWFtoSistema(bas, SoftwareFactory.findByGrupoLDAP("SWF_DTI_BAS_TELECOM"))
		addSWFtoSistema(base_entorno_geo, SoftwareFactory.findByGrupoLDAP("SWF_ST_INVENTARIOS_TELECOM"))
		addSWFtoSistema(base_entorno_geo, SoftwareFactory.findByGrupoLDAP("SWF_ST_INVENTARIOS_SPC_PROVEEDOR"))
		addSWFtoSistema(baseip, SoftwareFactory.findByGrupoLDAP("SWF_ST_BASE_IP_TELECOM"))
		addSWFtoSistema(baseip, SoftwareFactory.findByGrupoLDAP("SWF_ST_BASE_IP_PROVEEDOR"))
		addSWFtoSistema(belen, SoftwareFactory.findByGrupoLDAP("SWF_DTI_BELEN_TELECOM"))
		addSWFtoSistema(bios_hal, SoftwareFactory.findByGrupoLDAP("SWF_DTI_BIOS_TELECOM"))
		addSWFtoSistema(bios_hal, SoftwareFactory.findByGrupoLDAP("SWF_DTI_BIOS_PROVEEDOR"))
		addSWFtoSistema(calipso_cobros, SoftwareFactory.findByGrupoLDAP("SWF_DTI_CALIPSO_COBROS_TELECOM"))
		addSWFtoSistema(calipso_facturacion, SoftwareFactory.findByGrupoLDAP("SWF_DTI_CALIPSO_BILLING_TELECOM"))
		addSWFtoSistema(calipso_prefacturacion, SoftwareFactory.findByGrupoLDAP("SWF_DTI_CALIPSO_PREFACTURACION_TELECOM"))
		addSWFtoSistema(campaign_management, SoftwareFactory.findByGrupoLDAP("SWF_DTI_CAMPAIGN_MANAGEMENT_TELECOM"))
		addSWFtoSistema(carriers_interconectantes, SoftwareFactory.findByGrupoLDAP("SWF_DTI_CARRIERS_INTERCONECTANTES_PROVEEDOR"))
		addSWFtoSistema(catalogo_pys, SoftwareFactory.findByGrupoLDAP("SWF_DTI_CATALOGO_PYS_TELECOM"))
		addSWFtoSistema(cctbi, SoftwareFactory.findByGrupoLDAP("SWF_DTI_CCTBI_TELECOM"))
		addSWFtoSistema(central_parq, SoftwareFactory.findByGrupoLDAP("SWF_DTI_AMADEUS_CENTRAL_PARQ_TELECOM"))
		addSWFtoSistema(centro_validador, SoftwareFactory.findByGrupoLDAP("SWF_DTI_CENTRO_VALIDADOR_TELECOM"))
		addSWFtoSistema(centro_validador, SoftwareFactory.findByGrupoLDAP("SWF_DTI_CENTRO_VALIDADOR_TELECOM"))
		addSWFtoSistema(centro_validador_web, SoftwareFactory.findByGrupoLDAP("SWF_DTI_CENTRO_VALIDADOR_TELECOM"))
		addSWFtoSistema(certificaciones, SoftwareFactory.findByGrupoLDAP("SWF_SC_DENACERT_PROVEEDOR"))
		addSWFtoSistema(ciclope, SoftwareFactory.findByGrupoLDAP("SWF_ST_CICLOPE_TELECOM"))
		addSWFtoSistema(ciclope, SoftwareFactory.findByGrupoLDAP("SWF_ST_CICLOPE_SPC_PROVEEDOR"))
		addSWFtoSistema(citas, SoftwareFactory.findByGrupoLDAP("SWF_DTI_CITAS_PROVEEDOR"))
		addSWFtoSistema(citas, SoftwareFactory.findByGrupoLDAP("SWF_DTI_CITAS_TELECOM"))
		addSWFtoSistema(cms, SoftwareFactory.findByGrupoLDAP("SWF_DTI_CMS_PROVEEDOR"))
		addSWFtoSistema(cms, SoftwareFactory.findByGrupoLDAP("SWF_DTI_CMS_TELECOM"))
		addSWFtoSistema(cnm, SoftwareFactory.findByGrupoLDAP("SWF_ST_CNM_TELECOM"))
		addSWFtoSistema(cnm, SoftwareFactory.findByGrupoLDAP("SWF_ST_CNM_SPC_PROVEEDOR"))
		addSWFtoSistema(cobranza_electronica_front_end, SoftwareFactory.findByGrupoLDAP("SWF_DTI_COBRANZAS_ELECTRONICAS_TELECOM"))
		addSWFtoSistema(cobranza_integrada_front_end, SoftwareFactory.findByGrupoLDAP("SWF_DTI_COBRANZAS_INTEGRADAS_TELECOM"))
		addSWFtoSistema(cris, SoftwareFactory.findByGrupoLDAP("SWF_DTI_CRIS_PROVEEDOR"))
		addSWFtoSistema(cris, SoftwareFactory.findByGrupoLDAP("SWF_DTI_CRIS_TELECOM"))
		addSWFtoSistema(crm_use_oyp, SoftwareFactory.findByGrupoLDAP("SWF_DTI_CRM_USE_OYP_TELECOM"))
		addSWFtoSistema(cuenta_online_back_end, SoftwareFactory.findByGrupoLDAP("SWF_DTI_CUENTA_ON_LINE_GC_TELECOM"))
		addSWFtoSistema(cuenta_online_front_end, SoftwareFactory.findByGrupoLDAP("SWF_DTI_CUENTA_ON_LINE_GC_TELECOM"))
		addSWFtoSistema(cuenta_online_pyme_front_end, SoftwareFactory.findByGrupoLDAP("SWF_DTI_CUENTA_ON_LINE_PYMES_TELECOM"))
		addSWFtoSistema(dacota, SoftwareFactory.findByGrupoLDAP("SWF_DTI_DACOTA_PROVEEDOR"))
		addSWFtoSistema(dacota, SoftwareFactory.findByGrupoLDAP("SWF_DTI_DACOTA_TELECOM"))
		addSWFtoSistema(data_activator, SoftwareFactory.findByGrupoLDAP("SWF_ST_DA_NETCRACKER_PROVEEDOR"))
		addSWFtoSistema(data_activator, SoftwareFactory.findByGrupoLDAP("SWF_ST_DA_TELECOM"))
		addSWFtoSistema(ddt, SoftwareFactory.findByGrupoLDAP("SWF_DTI_DETERMINATES_DE_TASACION_TELECOM"))
		addSWFtoSistema(deimos, SoftwareFactory.findByGrupoLDAP("SWF_DTI_DEIMOS_TELECOM"))
		addSWFtoSistema(delta, SoftwareFactory.findByGrupoLDAP("SWF_DTI_DELTA_TELECOM"))
		addSWFtoSistema(denarius, SoftwareFactory.findByGrupoLDAP("SWF_SC_DENACERT_PROVEEDOR"))
		addSWFtoSistema(diaria_net, SoftwareFactory.findByGrupoLDAP("SWF_DTI_DIARIA_.NET_PROVEEDOR"))
		addSWFtoSistema(diaria_net, SoftwareFactory.findByGrupoLDAP("SWF_DTI_DIARIA_.NET_TELECOM"))
		addSWFtoSistema(e_mailing_front_end, SoftwareFactory.findByGrupoLDAP("SWF_DTI_E-MAILING_TELECOM"))
		addSWFtoSistema(epm_dsi, SoftwareFactory.findByGrupoLDAP("SWF_PMO_EPM IT_PROVEEDOR"))
		addSWFtoSistema(estadistica_0800_0810_back_end, SoftwareFactory.findByGrupoLDAP("SWF_DTI_ESTADISTICA_0800/0810_TELECOM"))
		addSWFtoSistema(estadistica_0800_0810_front_end, SoftwareFactory.findByGrupoLDAP("SWF_DTI_ESTADISTICA_0800/0810_TELECOM"))
		addSWFtoSistema(eva_cuentas_corrientes_masivo, SoftwareFactory.findByGrupoLDAP("SWF_DTI_EVA_CUENTAS_CORRIENTES_MASIVO_TELECOM"))
		addSWFtoSistema(eva_morosidad_cobranzas_gc_ws, SoftwareFactory.findByGrupoLDAP("SWF_DTI_EVA_MOROSIDAD/COBRANZAS_GC/WS_TELECOM"))
		addSWFtoSistema(eva_morosidad_financiacion_masivo, SoftwareFactory.findByGrupoLDAP("SWF_DTI_EVA_MOROSIDAD/FINANCIACION_MASIVO_TELECOM"))
		addSWFtoSistema(eva_postbaja_computo_impositivo, SoftwareFactory.findByGrupoLDAP("SWF_DTI_EVA_POSTBAJA/COMPUTO_IMPOSITIVO_TELECOM"))
		addSWFtoSistema(factesp, SoftwareFactory.findByGrupoLDAP("SWF_DTI_FACTESP_TELECOM"))
		addSWFtoSistema(fax_server, SoftwareFactory.findByGrupoLDAP("SWF_DTI_FAX_SERVER_PROVEEDOR"))
		addSWFtoSistema(fax_server_segat, SoftwareFactory.findByGrupoLDAP("SWF_DTI_FAX_SERVER_PROVEEDOR"))
		addSWFtoSistema(fosse, SoftwareFactory.findByGrupoLDAP("SWF_DTI_FOSSE_TELECOM"))
		addSWFtoSistema(frontbi_cctbi, SoftwareFactory.findByGrupoLDAP("SWF_DTI_FRONTBI_TELECOM"))
		addSWFtoSistema(frontbi_cctbi, SoftwareFactory.findByGrupoLDAP("SWF_DTI_FRONTBI_PROVEEDOR"))
		addSWFtoSistema(frontbi_dacota, SoftwareFactory.findByGrupoLDAP("SWF_DTI_FRONTBI_TELECOM"))
		addSWFtoSistema(frontbi_dacota, SoftwareFactory.findByGrupoLDAP("SWF_DTI_FRONTBI_PROVEEDOR"))
		addSWFtoSistema(frontbi_smart, SoftwareFactory.findByGrupoLDAP("SWF_DTI_FRONTBI_TELECOM"))
		addSWFtoSistema(frontbi_smart, SoftwareFactory.findByGrupoLDAP("SWF_DTI_FRONTBI_PROVEEDOR"))
		addSWFtoSistema(frontbi_tabgo, SoftwareFactory.findByGrupoLDAP("SWF_DTI_FRONTBI_TELECOM"))
		addSWFtoSistema(frontbi_tabgo, SoftwareFactory.findByGrupoLDAP("SWF_DTI_FRONTBI_PROVEEDOR"))
		addSWFtoSistema(frontbi_tabra, SoftwareFactory.findByGrupoLDAP("SWF_DTI_FRONTBI_TELECOM"))
		addSWFtoSistema(frontbi_tabra, SoftwareFactory.findByGrupoLDAP("SWF_DTI_FRONTBI_PROVEEDOR"))
		addSWFtoSistema(frontbi_zoom, SoftwareFactory.findByGrupoLDAP("SWF_DTI_FRONTBI_TELECOM"))
		addSWFtoSistema(frontbi_zoom, SoftwareFactory.findByGrupoLDAP("SWF_DTI_FRONTBI_PROVEEDOR"))
		addSWFtoSistema(genesis_bill_cycle, SoftwareFactory.findByGrupoLDAP("SWF_DTI_GENESIS_BILL_CYCLE_TELECOM"))
		addSWFtoSistema(genesis_customer, SoftwareFactory.findByGrupoLDAP("SWF_DTI_GENESIS_CUSTOMER_TELECOM"))
		addSWFtoSistema(genesis_mic, SoftwareFactory.findByGrupoLDAP("SWF_DTI_GENESIS_MIC_TELECOM"))
		addSWFtoSistema(genesis_oyp, SoftwareFactory.findByGrupoLDAP("SWF_DTI_GENESIS_OYP_TELECOM"))
		addSWFtoSistema(genesis_ups, SoftwareFactory.findByGrupoLDAP("SWF_DTI_GENESIS_UPS_TELECOM"))
		addSWFtoSistema(gesprod, SoftwareFactory.findByGrupoLDAP("SWF_DTI_GESPROD_PROVEEDOR"))
		addSWFtoSistema(gesprod, SoftwareFactory.findByGrupoLDAP("SWF_DTI_GESPROD_TELECOM"))
		addSWFtoSistema(gestion_de_emprendimientos, SoftwareFactory.findByGrupoLDAP("SWF_DTI_GESTION_DE_EMPRENDIMIENTOS_PROVEEDOR"))
		addSWFtoSistema(gestion_de_emprendimientos, SoftwareFactory.findByGrupoLDAP("SWF_DTI_GESTION_DE_EMPRENDIMIENTOS_TELECOM"))
		addSWFtoSistema(gestor_de_comisiones, SoftwareFactory.findByGrupoLDAP("SWF_DTI_DA_VINCI_GESTOR_DE_COMISIONES_PROVEEDOR"))
		addSWFtoSistema(gestor_de_comisiones, SoftwareFactory.findByGrupoLDAP("SWF_DTI_DA_VINCI_GESTOR_DE_COMISIONES_TELECOM"))
		addSWFtoSistema(girafe, SoftwareFactory.findByGrupoLDAP("SWF_DTI_GIRAFE_PROVEEDOR"))
		addSWFtoSistema(girafe, SoftwareFactory.findByGrupoLDAP("SWF_DTI_GIRAFE_TELECOM"))
		addSWFtoSistema(guc, SoftwareFactory.findByGrupoLDAP("SWF_DTI_GUC_TELECOM"))
		addSWFtoSistema(interconnect, SoftwareFactory.findByGrupoLDAP("SWF_DTI_INTERCONNECT_TELECOM"))
		addSWFtoSistema(ipb, SoftwareFactory.findByGrupoLDAP("SWF_DTI_IPB_TELECOM"))
		addSWFtoSistema(iris, SoftwareFactory.findByGrupoLDAP("SWF_ST_INVENTARIOS_TELECOM"))
		addSWFtoSistema(iris, SoftwareFactory.findByGrupoLDAP("SWF_ST_INVENTARIOS_SPC_PROVEEDOR"))
		addSWFtoSistema(ium, SoftwareFactory.findByGrupoLDAP("SWF_DTI_IUM_TELECOM"))
		addSWFtoSistema(jano, SoftwareFactory.findByGrupoLDAP("SWF_DTI_JANO_PROVEEDOR"))
		addSWFtoSistema(jano, SoftwareFactory.findByGrupoLDAP("SWF_DTI_JANO_TELECOM"))
		addSWFtoSistema(jano_proceso_etl, SoftwareFactory.findByGrupoLDAP("SWF_DTI_JANO_PROVEEDOR"))
		addSWFtoSistema(jano_proceso_etl, SoftwareFactory.findByGrupoLDAP("SWF_DTI_JANO_TELECOM"))
		addSWFtoSistema(kingdom, SoftwareFactory.findByGrupoLDAP("SWF_DTI_KINGDOM_TELECOM"))
		addSWFtoSistema(litigios, SoftwareFactory.findByGrupoLDAP("SWF_SC_GESTION DE LITIGIOS_PROVEEDOR"))
		addSWFtoSistema(mac, SoftwareFactory.findByGrupoLDAP("SWF_DTI_MAC_PROVEEDOR"))
		addSWFtoSistema(mac, SoftwareFactory.findByGrupoLDAP("SWF_DTI_MAC_TELECOM"))
		addSWFtoSistema(mi_cuenta_back_end, SoftwareFactory.findByGrupoLDAP("SWF_DTI_MICUENTA@_TELECOM"))
		addSWFtoSistema(mi_cuenta_front_end, SoftwareFactory.findByGrupoLDAP("SWF_DTI_MICUENTA@_TELECOM"))
		addSWFtoSistema(nba, SoftwareFactory.findByGrupoLDAP("SWF_DTI_NBA_TELECOM"))
		addSWFtoSistema(needit, SoftwareFactory.findByGrupoLDAP("SWF_SC_F_SIGNO_GRAILSJAVA_TELECOM"))
		addSWFtoSistema(needit, SoftwareFactory.findByGrupoLDAP("SWF_SC_F_SIGNO_GRAILSJAVA_PROVEEDOR"))
		addSWFtoSistema(network_inventory, SoftwareFactory.findByGrupoLDAP("SWF_ST_INVENTARIOS_TELECOM"))
		addSWFtoSistema(network_inventory, SoftwareFactory.findByGrupoLDAP("SWF_ST_INVENTARIOS_SPC_PROVEEDOR"))
		addSWFtoSistema(nred, SoftwareFactory.findByGrupoLDAP("SWF_ST_INVENTARIOS_TELECOM"))
		addSWFtoSistema(nred, SoftwareFactory.findByGrupoLDAP("SWF_ST_INVENTARIOS_SPC_PROVEEDOR"))
		addSWFtoSistema(nred, SoftwareFactory.findByGrupoLDAP("SWF_ST_INVENTARIOS_TELECOM"))
		addSWFtoSistema(nred, SoftwareFactory.findByGrupoLDAP("SWF_ST_INVENTARIOS_SPC_PROVEEDOR"))
		addSWFtoSistema(ods, SoftwareFactory.findByGrupoLDAP("SWF_DTI_ODS_PROVEEDOR"))
		addSWFtoSistema(ods, SoftwareFactory.findByGrupoLDAP("SWF_DTI_ODS_TELECOM"))
		addSWFtoSistema(persa, SoftwareFactory.findByGrupoLDAP("SWF_ST_PERSA_TELECOM"))
		addSWFtoSistema(persa, SoftwareFactory.findByGrupoLDAP("SWF_ST_PERSA_PROVEEDOR"))
		addSWFtoSistema(plataforma_de_precalificacion, SoftwareFactory.findByGrupoLDAP("SWF_ST_PLATAFORMA_PRECALIFICACION_TELECOM"))
		addSWFtoSistema(plataforma_de_precalificacion, SoftwareFactory.findByGrupoLDAP("SWF_ST_PLATAFORMA_PRECALIFICACION_PROVEEDOR"))
		addSWFtoSistema(plataforma_de_testing, SoftwareFactory.findByGrupoLDAP("SWF_ST_PLATAFORMA_TESTING_TELECOM"))
		addSWFtoSistema(plataforma_de_testing, SoftwareFactory.findByGrupoLDAP("SWF_ST_PLATAFORMA_TESTING_PROVEEDOR"))
		addSWFtoSistema(portal_bi_utf, SoftwareFactory.findByGrupoLDAP("SWF_DTI_PORTAL BI UTF_PROVEEDOR"))
		addSWFtoSistema(portal_bi_utf, SoftwareFactory.findByGrupoLDAP("SWF_DTI_PORTAL BI UTF_TELECOM"))
		addSWFtoSistema(prod_arnet_portales_arnet, SoftwareFactory.findByGrupoLDAP("SWF_DTI_Venta On line_TELECOM"))
		addSWFtoSistema(prod_arnet_portales_back_office, SoftwareFactory.findByGrupoLDAP("SWF_DTI_Venta On line_TELECOM"))
		addSWFtoSistema(prod_arnet_portales_telecom, SoftwareFactory.findByGrupoLDAP("SWF_DTI_Venta On line_TELECOM"))
		addSWFtoSistema(pyo, SoftwareFactory.findByGrupoLDAP("SWF_DTI_PYO_TELECOM"))
		addSWFtoSistema(qv_asse, SoftwareFactory.findByGrupoLDAP("SWF_SC_QV_CORP_TELECOM"))
		addSWFtoSistema(qv_asse, SoftwareFactory.findByGrupoLDAP("SWF_SC_QV_CORP_DATA IQ_PROVEEDOR"))
		addSWFtoSistema(qv_asse, SoftwareFactory.findByGrupoLDAP("SWF_SC_QV_CORP_ASAP_COR_PROVEEDOR"))
		addSWFtoSistema(qv_clie, SoftwareFactory.findByGrupoLDAP("SWF_SC_QV_CORP_TELECOM"))
		addSWFtoSistema(qv_clie, SoftwareFactory.findByGrupoLDAP("SWF_SC_QV_CORP_DATA IQ_PROVEEDOR"))
		addSWFtoSistema(qv_clie, SoftwareFactory.findByGrupoLDAP("SWF_SC_QV_CORP_ASAP_COR_PROVEEDOR"))
		addSWFtoSistema(qv_masivo, SoftwareFactory.findByGrupoLDAP("SWF_ST_QV_TELECOM"))
		addSWFtoSistema(qv_masivo, SoftwareFactory.findByGrupoLDAP("SWF_ST_QV_SPC_PROVEEDOR"))
		addSWFtoSistema(qv_nomasivo, SoftwareFactory.findByGrupoLDAP("SWF_ST_QV_TELECOM"))
		addSWFtoSistema(qv_nomasivo, SoftwareFactory.findByGrupoLDAP("SWF_ST_QV_SPC_PROVEEDOR"))
		addSWFtoSistema(qv_remedy, SoftwareFactory.findByGrupoLDAP("SWF_ST_QV_TELECOM"))
		addSWFtoSistema(qv_remedy, SoftwareFactory.findByGrupoLDAP("SWF_ST_QV_SPC_PROVEEDOR"))
		addSWFtoSistema(qv_siag, SoftwareFactory.findByGrupoLDAP("SWF_SC_QV_CORP_TELECOM"))
		addSWFtoSistema(qv_siag, SoftwareFactory.findByGrupoLDAP("SWF_SC_QV_CORP_DATA IQ_PROVEEDOR"))
		addSWFtoSistema(qv_siag, SoftwareFactory.findByGrupoLDAP("SWF_SC_QV_CORP_ASAP_COR_PROVEEDOR"))
		addSWFtoSistema(qv_simplit, SoftwareFactory.findByGrupoLDAP("SWF_ST_QV_TELECOM"))
		addSWFtoSistema(qv_simplit, SoftwareFactory.findByGrupoLDAP("SWF_ST_QV_SPC_PROVEEDOR"))
		addSWFtoSistema(qv_ssgg, SoftwareFactory.findByGrupoLDAP("SWF_SC_QV_CORP_TELECOM"))
		addSWFtoSistema(qv_ssgg, SoftwareFactory.findByGrupoLDAP("SWF_SC_QV_CORP_DATA IQ_PROVEEDOR"))
		addSWFtoSistema(qv_ssgg, SoftwareFactory.findByGrupoLDAP("SWF_SC_QV_CORP_ASAP_COR_PROVEEDOR"))
		addSWFtoSistema(remedyred, SoftwareFactory.findByGrupoLDAP("SWF_ST_REMEYRED_TELECOM"))
		addSWFtoSistema(remedyred, SoftwareFactory.findByGrupoLDAP("SWF_ST_REMEYRED_SPC_PROVEEDOR"))
		addSWFtoSistema(reporte_aseguramiento, SoftwareFactory.findByGrupoLDAP("SWF_ST_QV_TELECOM"))
		addSWFtoSistema(reporte_aseguramiento, SoftwareFactory.findByGrupoLDAP("SWF_ST_QV_SPC_PROVEEDOR"))
		addSWFtoSistema(sas, SoftwareFactory.findByGrupoLDAP("SWF_ST_SISTEMA-AFECTACION-SERVICIOS_TELECOM"))
		addSWFtoSistema(sas, SoftwareFactory.findByGrupoLDAP("SWF_ST_SISTEMA-AFECTACION-SERVICIOS_PROVEEDOR"))
		addSWFtoSistema(scard, SoftwareFactory.findByGrupoLDAP("SWF_DTI_SCARD_COLD_TELECOM"))
		addSWFtoSistema(segat, SoftwareFactory.findByGrupoLDAP("SWF_ST_SEGAT_TELECOM"))
		addSWFtoSistema(segat, SoftwareFactory.findByGrupoLDAP("SWF_ST_SEGAT_SPC_PROVEEDOR"))
		addSWFtoSistema(sei, SoftwareFactory.findByGrupoLDAP("SWF_DTI_SEI_TELECOM"))
		addSWFtoSistema(seta, SoftwareFactory.findByGrupoLDAP("SWF_ST_INVENTARIOS_TELECOM"))
		addSWFtoSistema(seta, SoftwareFactory.findByGrupoLDAP("SWF_ST_INVENTARIOS_SPC_PROVEEDOR"))
		addSWFtoSistema(sgct, SoftwareFactory.findByGrupoLDAP("SWF_DTI_SGCT_PROVEEDOR"))
		addSWFtoSistema(sgct, SoftwareFactory.findByGrupoLDAP("SWF_DTI_SGCT_TELECOM"))
		addSWFtoSistema(sgdatos, SoftwareFactory.findByGrupoLDAP("SWF_ST_SG_DATOS_TELECOM"))
		addSWFtoSistema(sgdatos, SoftwareFactory.findByGrupoLDAP("SWF_ST_SG_DATOS_PROVEEDOR"))
		addSWFtoSistema(sgdatos_tp, SoftwareFactory.findByGrupoLDAP("SWF_ST_SG_DATOS_TELECOM"))
		addSWFtoSistema(sgdatos_tp, SoftwareFactory.findByGrupoLDAP("SWF_ST_SG_DATOS_PROVEEDOR"))
		addSWFtoSistema(sharepoint, SoftwareFactory.findByGrupoLDAP("SWF_SC_SHAREPOINT_PROVEEDOR"))
		addSWFtoSistema(siaf, SoftwareFactory.findByGrupoLDAP("SWF_SC_F_SIGNO_FIN_TELECOM"))
		addSWFtoSistema(siaf, SoftwareFactory.findByGrupoLDAP("SWF_SC_F_SIGNO_FIN_PROVEEDOR"))
		addSWFtoSistema(siaf, SoftwareFactory.findByGrupoLDAP("SWF_SC_F_SIGNO_ABAP_PROVEEDOR"))
		addSWFtoSistema(sideral, SoftwareFactory.findByGrupoLDAP("SWF_ST_SIDERAL_TELECOM"))
		addSWFtoSistema(sideral, SoftwareFactory.findByGrupoLDAP("SWF_ST_SIDERAL_PROVEEDOR"))
		addSWFtoSistema(siebel_customer_centric, SoftwareFactory.findByGrupoLDAP("SWF_DTI_SIEBEL_TELECOM"))
		addSWFtoSistema(siebel_customer_centric, SoftwareFactory.findByGrupoLDAP("SWF_DTI_SIEBEL_PROVEEDOR"))
		addSWFtoSistema(signo_cap, SoftwareFactory.findByGrupoLDAP("SWF_SC_F_SIGNO_FIN_TELECOM"))
		addSWFtoSistema(signo_cap, SoftwareFactory.findByGrupoLDAP("SWF_SC_F_SIGNO_FIN_PROVEEDOR"))
		addSWFtoSistema(signo_cap, SoftwareFactory.findByGrupoLDAP("SWF_SC_F_SIGNO_ABAP_PROVEEDOR"))
		addSWFtoSistema(signo_cobranzas, SoftwareFactory.findByGrupoLDAP("SWF_SC_F_SIGNO_FIN_TELECOM"))
		addSWFtoSistema(signo_cobranzas, SoftwareFactory.findByGrupoLDAP("SWF_SC_F_SIGNO_FIN_PROVEEDOR"))
		addSWFtoSistema(signo_cobranzas, SoftwareFactory.findByGrupoLDAP("SWF_SC_F_SIGNO_ABAP_PROVEEDOR"))
		addSWFtoSistema(signo_compras, SoftwareFactory.findByGrupoLDAP("SWF_SC_F_SIGNO_MM_TELECOM"))
		addSWFtoSistema(signo_compras, SoftwareFactory.findByGrupoLDAP("SWF_SC_F_SIGNO_MM_PROVEEDOR"))
		addSWFtoSistema(signo_compras, SoftwareFactory.findByGrupoLDAP("SWF_SC_F_SIGNO_ABAP_PROVEEDOR"))
		addSWFtoSistema(signo_compras, SoftwareFactory.findByGrupoLDAP("SWF_SC_F_SIGNO_SAP_PI_PROVEEDOR"))
		addSWFtoSistema(signo_compras, SoftwareFactory.findByGrupoLDAP("SWF_SC_F_SIGNO_OPENTEXT_PROVEEDOR"))
		addSWFtoSistema(signo_compras, SoftwareFactory.findByGrupoLDAP("SWF_SC_F_SIGNO_NWJAVA_PROVEEDOR"))
		addSWFtoSistema(signo_compras_estrategias, SoftwareFactory.findByGrupoLDAP("SWF_SC_F_SIGNO_MM_TELECOM"))
		addSWFtoSistema(signo_compras_estrategias, SoftwareFactory.findByGrupoLDAP("SWF_SC_F_SIGNO_MM_PROVEEDOR"))
		addSWFtoSistema(signo_compras_estrategias, SoftwareFactory.findByGrupoLDAP("SWF_SC_F_SIGNO_ABAP_PROVEEDOR"))
		addSWFtoSistema(signo_compras_estrategias, SoftwareFactory.findByGrupoLDAP("SWF_SC_F_SIGNO_NWJAVA_PROVEEDOR"))
		addSWFtoSistema(signo_ess, SoftwareFactory.findByGrupoLDAP("SWF_SC_APLRRHH_TELECOM"))
		addSWFtoSistema(signo_ess, SoftwareFactory.findByGrupoLDAP("SWF_SC_F_SIGNO_DESARROLLO_TELECOM"))
		addSWFtoSistema(signo_ess, SoftwareFactory.findByGrupoLDAP("SWF_SC_F_SIGNO_NWJAVA_PROVEEDOR"))
		addSWFtoSistema(signo_ess, SoftwareFactory.findByGrupoLDAP("SWF_SC_F_SIGNO_ABAP_PROVEEDOR"))
		addSWFtoSistema(signo_ess_celulares, SoftwareFactory.findByGrupoLDAP("SWF_SC_APLRRHH_TELECOM"))
		addSWFtoSistema(signo_ess_celulares, SoftwareFactory.findByGrupoLDAP("SWF_SC_F_SIGNO_DESARROLLO_TELECOM"))
		addSWFtoSistema(signo_ess_celulares, SoftwareFactory.findByGrupoLDAP("SWF_SC_F_SIGNO_NWJAVA_PROVEEDOR"))
		addSWFtoSistema(signo_ess_celulares, SoftwareFactory.findByGrupoLDAP("SWF_SC_F_SIGNO_ABAP_PROVEEDOR"))
		addSWFtoSistema(signo_ess_mi_perfil, SoftwareFactory.findByGrupoLDAP("SWF_SC_APLRRHH_TELECOM"))
		addSWFtoSistema(signo_ess_mi_perfil, SoftwareFactory.findByGrupoLDAP("SWF_SC_F_SIGNO_DESARROLLO_TELECOM"))
		addSWFtoSistema(signo_ess_mi_perfil, SoftwareFactory.findByGrupoLDAP("SWF_SC_F_SIGNO_NWJAVA_PROVEEDOR"))
		addSWFtoSistema(signo_ess_mi_perfil, SoftwareFactory.findByGrupoLDAP("SWF_SC_F_SIGNO_ABAP_PROVEEDOR"))
		addSWFtoSistema(signo_ess_mis_licencias, SoftwareFactory.findByGrupoLDAP("SWF_SC_APLRRHH_TELECOM"))
		addSWFtoSistema(signo_ess_mis_licencias, SoftwareFactory.findByGrupoLDAP("SWF_SC_F_SIGNO_DESARROLLO_TELECOM"))
		addSWFtoSistema(signo_ess_mis_licencias, SoftwareFactory.findByGrupoLDAP("SWF_SC_F_SIGNO_NWJAVA_PROVEEDOR"))
		addSWFtoSistema(signo_ess_mis_licencias, SoftwareFactory.findByGrupoLDAP("SWF_SC_F_SIGNO_ABAP_PROVEEDOR"))
		addSWFtoSistema(signo_ess_plan_empleados, SoftwareFactory.findByGrupoLDAP("SWF_SC_APLRRHH_TELECOM"))
		addSWFtoSistema(signo_ess_plan_empleados, SoftwareFactory.findByGrupoLDAP("SWF_SC_F_SIGNO_DESARROLLO_TELECOM"))
		addSWFtoSistema(signo_ess_plan_empleados, SoftwareFactory.findByGrupoLDAP("SWF_SC_F_SIGNO_NWJAVA_PROVEEDOR"))
		addSWFtoSistema(signo_ess_plan_empleados, SoftwareFactory.findByGrupoLDAP("SWF_SC_F_SIGNO_ABAP_PROVEEDOR"))
		addSWFtoSistema(signo_ess_recibo_electronico, SoftwareFactory.findByGrupoLDAP("SWF_SC_APLRRHH_TELECOM"))
		addSWFtoSistema(signo_ess_recibo_electronico, SoftwareFactory.findByGrupoLDAP("SWF_SC_F_SIGNO_DESARROLLO_TELECOM"))
		addSWFtoSistema(signo_ess_recibo_electronico, SoftwareFactory.findByGrupoLDAP("SWF_SC_F_SIGNO_NWJAVA_PROVEEDOR"))
		addSWFtoSistema(signo_ess_recibo_electronico, SoftwareFactory.findByGrupoLDAP("SWF_SC_F_SIGNO_ABAP_PROVEEDOR"))
		addSWFtoSistema(signo_facturas_web, SoftwareFactory.findByGrupoLDAP("SWF_SC_F_SIGNO_FIN_TELECOM"))
		addSWFtoSistema(signo_facturas_web, SoftwareFactory.findByGrupoLDAP("SWF_SC_F_SIGNO_FIN_PROVEEDOR"))
		addSWFtoSistema(signo_facturas_web, SoftwareFactory.findByGrupoLDAP("SWF_SC_F_SIGNO_ABAP_PROVEEDOR"))
		addSWFtoSistema(signo_finanzas, SoftwareFactory.findByGrupoLDAP("SWF_SC_F_SIGNO_FIN_TELECOM"))
		addSWFtoSistema(signo_finanzas, SoftwareFactory.findByGrupoLDAP("SWF_SC_F_SIGNO_FIN_PROVEEDOR"))
		addSWFtoSistema(signo_finanzas, SoftwareFactory.findByGrupoLDAP("SWF_SC_F_SIGNO_ABAP_PROVEEDOR"))
		addSWFtoSistema(signo_finanzas_py, SoftwareFactory.findByGrupoLDAP("SWF_SC_F_SIGNO_FIN_TELECOM"))
		addSWFtoSistema(signo_finanzas_py, SoftwareFactory.findByGrupoLDAP("SWF_SC_F_SIGNO_FIN_PROVEEDOR"))
		addSWFtoSistema(signo_finanzas_py, SoftwareFactory.findByGrupoLDAP("SWF_SC_F_SIGNO_ABAP_PROVEEDOR"))
		addSWFtoSistema(signo_guiainterna, SoftwareFactory.findByGrupoLDAP("SWF_SC_F_SIGNO_DESARROLLO_TELECOM"))
		addSWFtoSistema(signo_guiainterna, SoftwareFactory.findByGrupoLDAP("SWF_SC_F_SIGNO_NWJAVA_PROVEEDOR"))
		addSWFtoSistema(signo_guiainterna, SoftwareFactory.findByGrupoLDAP("SWF_SC_F_SIGNO_ABAP_PROVEEDOR"))
		addSWFtoSistema(signo_logistica, SoftwareFactory.findByGrupoLDAP("SWF_SC_F_SIGNO_MM_TELECOM"))
		addSWFtoSistema(signo_logistica, SoftwareFactory.findByGrupoLDAP("SWF_SC_F_SIGNO_MM_PROVEEDOR"))
		addSWFtoSistema(signo_logistica, SoftwareFactory.findByGrupoLDAP("SWF_SC_F_SIGNO_ABAP_PROVEEDOR"))
		addSWFtoSistema(signo_logistica, SoftwareFactory.findByGrupoLDAP("SWF_SC_F_SIGNO_SAP_PI_PROVEEDOR"))
		addSWFtoSistema(signo_logistica, SoftwareFactory.findByGrupoLDAP("SWF_SC_F_SIGNO_OPENTEXT_PROVEEDOR"))
		addSWFtoSistema(signo_mantenimiento, SoftwareFactory.findByGrupoLDAP("SWF_SC_F_SIGNO_ PMA_TELECOM"))
		addSWFtoSistema(signo_mantenimiento, SoftwareFactory.findByGrupoLDAP("SWF_SC_F_SIGNO_ PMA_PROVEEDOR"))
		addSWFtoSistema(signo_mantenimiento, SoftwareFactory.findByGrupoLDAP("SWF_SC_F_SIGNO_ABAP_PROVEEDOR"))
		addSWFtoSistema(signo_neoportal, SoftwareFactory.findByGrupoLDAP("SWF_SC_NEOPORTAL _PROVEEDOR"))
		addSWFtoSistema(signo_proyectos, SoftwareFactory.findByGrupoLDAP("SWF_SC_F_SIGNO_ PMA_TELECOM"))
		addSWFtoSistema(signo_proyectos, SoftwareFactory.findByGrupoLDAP("SWF_SC_F_SIGNO_ PMA_PROVEEDOR"))
		addSWFtoSistema(signo_proyectos, SoftwareFactory.findByGrupoLDAP("SWF_SC_F_SIGNO_ABAP_PROVEEDOR"))
		addSWFtoSistema(signo_real_estate, SoftwareFactory.findByGrupoLDAP("SWF_SC_F_SIGNO_ PMA_TELECOM"))
		addSWFtoSistema(signo_real_estate, SoftwareFactory.findByGrupoLDAP("SWF_SC_F_SIGNO_ PMA_PROVEEDOR"))
		addSWFtoSistema(signo_real_estate, SoftwareFactory.findByGrupoLDAP("SWF_SC_F_SIGNO_ABAP_PROVEEDOR"))
		addSWFtoSistema(signo_registro_firmas, SoftwareFactory.findByGrupoLDAP("SWF_SC_F_SIGNO_DESARROLLO_TELECOM"))
		addSWFtoSistema(signo_registro_firmas, SoftwareFactory.findByGrupoLDAP("SWF_SC_F_SIGNO_NWJAVA_PROVEEDOR"))
		addSWFtoSistema(signo_registro_firmas, SoftwareFactory.findByGrupoLDAP("SWF_SC_F_SIGNO_ABAP_PROVEEDOR"))
		addSWFtoSistema(signo_rrhh, SoftwareFactory.findByGrupoLDAP("SWF_SC_APLRRHH_TELECOM"))
		addSWFtoSistema(signo_rrhh, SoftwareFactory.findByGrupoLDAP("SWF_SC_F_SIGNO_DESARROLLO_TELECOM"))
		addSWFtoSistema(signo_rrhh, SoftwareFactory.findByGrupoLDAP("SWF_SC_F_SIGNO_NWJAVA_PROVEEDOR"))
		addSWFtoSistema(signo_rrhh, SoftwareFactory.findByGrupoLDAP("SWF_SC_F_SIGNO_ABAP_PROVEEDOR"))
		addSWFtoSistema(signo_sap_grc, SoftwareFactory.findByGrupoLDAP("SWF_SC_APLRRHH_TELECOM"))
		addSWFtoSistema(signo_sap_grc, SoftwareFactory.findByGrupoLDAP("SWF_SC_F_SIGNO_DESARROLLO_TELECOM"))
		addSWFtoSistema(signo_sap_grc, SoftwareFactory.findByGrupoLDAP("SWF_SC_F_SIGNO_NWJAVA_PROVEEDOR"))
		addSWFtoSistema(signo_sap_grc, SoftwareFactory.findByGrupoLDAP("SWF_SC_F_SIGNO_ABAP_PROVEEDOR"))
		addSWFtoSistema(signo_sap_grc, SoftwareFactory.findByGrupoLDAP("SWF_SC_APLRRHH_TELECOM"))
		addSWFtoSistema(signo_sap_grc, SoftwareFactory.findByGrupoLDAP("SWF_SC_F_SIGNO_DESARROLLO_TELECOM"))
		addSWFtoSistema(signo_sap_grc, SoftwareFactory.findByGrupoLDAP("SWF_SC_F_SIGNO_NWJAVA_PROVEEDOR"))
		addSWFtoSistema(signo_sap_grc, SoftwareFactory.findByGrupoLDAP("SWF_SC_F_SIGNO_ABAP_PROVEEDOR"))
		addSWFtoSistema(signo_sh_agendablog, SoftwareFactory.findByGrupoLDAP("SWF_SC_SHAREPOINT_PROVEEDOR"))
		addSWFtoSistema(signo_sh_blog_copamundo, SoftwareFactory.findByGrupoLDAP("SWF_SC_SHAREPOINT_PROVEEDOR"))
		addSWFtoSistema(signo_sh_blog_equilibrio, SoftwareFactory.findByGrupoLDAP("SWF_SC_SHAREPOINT_PROVEEDOR"))
		addSWFtoSistema(signo_sh_blog_teletrabajo, SoftwareFactory.findByGrupoLDAP("SWF_SC_SHAREPOINT_PROVEEDOR"))
		addSWFtoSistema(signo_sh_colonia, SoftwareFactory.findByGrupoLDAP("SWF_SC_SHAREPOINT_PROVEEDOR"))
		addSWFtoSistema(signo_sh_ddjj, SoftwareFactory.findByGrupoLDAP("SWF_SC_SHAREPOINT_PROVEEDOR"))
		addSWFtoSistema(signo_sh_imp_ganancias, SoftwareFactory.findByGrupoLDAP("SWF_SC_SHAREPOINT_PROVEEDOR"))
		addSWFtoSistema(signo_sh_lineas_servicio, SoftwareFactory.findByGrupoLDAP("SWF_SC_SHAREPOINT_PROVEEDOR"))
		addSWFtoSistema(signo_sh_wiki, SoftwareFactory.findByGrupoLDAP("SWF_SC_SHAREPOINT_PROVEEDOR"))
		addSWFtoSistema(signo_sorteos, SoftwareFactory.findByGrupoLDAP("SWF_SC_F_SIGNO_DESARROLLO_TELECOM"))
		addSWFtoSistema(signo_sorteos, SoftwareFactory.findByGrupoLDAP("SWF_SC_F_SIGNO_NWJAVA_PROVEEDOR"))
		addSWFtoSistema(signo_sorteos, SoftwareFactory.findByGrupoLDAP("SWF_SC_F_SIGNO_ABAP_PROVEEDOR"))
		addSWFtoSistema(signo_tesoreria, SoftwareFactory.findByGrupoLDAP("SWF_SC_F_SIGNO_FIN_TELECOM"))
		addSWFtoSistema(signo_tesoreria, SoftwareFactory.findByGrupoLDAP("SWF_SC_F_SIGNO_FIN_PROVEEDOR"))
		addSWFtoSistema(signo_tesoreria, SoftwareFactory.findByGrupoLDAP("SWF_SC_F_SIGNO_ABAP_PROVEEDOR"))
		addSWFtoSistema(signo_veconp, SoftwareFactory.findByGrupoLDAP("SWF_SC_VECONP_PROVEEDOR"))
		addSWFtoSistema(signo_voluntariado, SoftwareFactory.findByGrupoLDAP("SWF_SC_F_SIGNO_DESARROLLO_TELECOM"))
		addSWFtoSistema(signo_voluntariado, SoftwareFactory.findByGrupoLDAP("SWF_SC_F_SIGNO_NWJAVA_PROVEEDOR"))
		addSWFtoSistema(signo_voluntariado, SoftwareFactory.findByGrupoLDAP("SWF_SC_F_SIGNO_ABAP_PROVEEDOR"))
		addSWFtoSistema(simplit, SoftwareFactory.findByGrupoLDAP("SWF_SC_SIMPLIT_TELECOM"))
		addSWFtoSistema(simplit, SoftwareFactory.findByGrupoLDAP("SWF_SC_SIMPLIT_PROVEEDOR"))
		addSWFtoSistema(sir, SoftwareFactory.findByGrupoLDAP("SWF_DTI_SIR_TELECOM"))
		addSWFtoSistema(smart, SoftwareFactory.findByGrupoLDAP("SWF_DTI_SMART_PROVEEDOR"))
		addSWFtoSistema(smart, SoftwareFactory.findByGrupoLDAP("SWF_DTI_SMART_TELECOM"))
		addSWFtoSistema(sp_calidad_redesyservicios, SoftwareFactory.findByGrupoLDAP("SWF_ST_QV_SPC_PROVEEDOR"))
		addSWFtoSistema(sp_calidad_redesyservicios, SoftwareFactory.findByGrupoLDAP("SWF_ST_QV_SPC_PROVEEDOR"))
		addSWFtoSistema(suc2, SoftwareFactory.findByGrupoLDAP("SWF_DTI_SUC2_PROVEEDOR"))
		addSWFtoSistema(suc2, SoftwareFactory.findByGrupoLDAP("SWF_DTI_SUC2_TELECOM"))
		addSWFtoSistema(supervision_vpn, SoftwareFactory.findByGrupoLDAP("SWF_ST_SUPERVISION-VPNS_TELECOM"))
		addSWFtoSistema(supervision_vpn, SoftwareFactory.findByGrupoLDAP("SWF_ST_SUPERVISION-VPNS_PROVEEDOR"))
		addSWFtoSistema(sur, SoftwareFactory.findByGrupoLDAP("SWF_ST_SUR_TELECOM"))
		addSWFtoSistema(sur, SoftwareFactory.findByGrupoLDAP("SWF_ST_SUR_SPC_PROVEEDOR"))
		addSWFtoSistema(sus, SoftwareFactory.findByGrupoLDAP("SWF_ST_SUS_TELECOM"))
		addSWFtoSistema(sus, SoftwareFactory.findByGrupoLDAP("SWF_ST_SUS_PROVEEDOR"))
		addSWFtoSistema(tab_go, SoftwareFactory.findByGrupoLDAP("SWF_DTI_TAB_GO_TELECOM"))
		addSWFtoSistema(tab_ra, SoftwareFactory.findByGrupoLDAP("SWF_DTI_TAB_RA_TELECOM"))
		addSWFtoSistema(tas, SoftwareFactory.findByGrupoLDAP("SWF_ST_TAS_TELECOM"))
		addSWFtoSistema(tas, SoftwareFactory.findByGrupoLDAP("SWF_ST_TAS_SPC_PROVEEDOR"))
		addSWFtoSistema(temis, SoftwareFactory.findByGrupoLDAP("SWF_DTI_TEMIS_TELECOM"))
		addSWFtoSistema(transformer, SoftwareFactory.findByGrupoLDAP("SWF_DTI_TRANSFORMER_TELECOM"))
		addSWFtoSistema(truman, SoftwareFactory.findByGrupoLDAP("SWF_DTI_TRUMAN_TELECOM"))
		addSWFtoSistema(ttusa_prepago_api, SoftwareFactory.findByGrupoLDAP("SWF_DTI_TECO_USA_PREPAGO_TELECOM"))
		addSWFtoSistema(ttusa_prepago_gestion, SoftwareFactory.findByGrupoLDAP("SWF_DTI_TECO_USA_PREPAGO_TELECOM"))
		addSWFtoSistema(utopia, SoftwareFactory.findByGrupoLDAP("SWF_DTI_UTOPIA_TELECOM"))
		addSWFtoSistema(utopia, SoftwareFactory.findByGrupoLDAP("SWF_DTI_UTOPIA_PROVEEDOR"))
		addSWFtoSistema(vantive_field_service, SoftwareFactory.findByGrupoLDAP("SWF_DTI_VANTIVE_FIELD_SERVICE_TELECOM"))
		addSWFtoSistema(web_adsl_tecnica, SoftwareFactory.findByGrupoLDAP("SWF_ST_INVENTARIOS_TELECOM"))
		addSWFtoSistema(web_adsl_tecnica, SoftwareFactory.findByGrupoLDAP("SWF_ST_INVENTARIOS_SPC_PROVEEDOR"))
		addSWFtoSistema(workflow_tecnico, SoftwareFactory.findByGrupoLDAP("SWF_DTI_WORKFLOW_TECNICO_TELECOM"))
		addSWFtoSistema(yadas, SoftwareFactory.findByGrupoLDAP("SWF_DTI_ADAS_PROVEEDOR"))
		addSWFtoSistema(yadas, SoftwareFactory.findByGrupoLDAP("SWF_DTI_ADAS_TELECOM"))
		addSWFtoSistema(zoom, SoftwareFactory.findByGrupoLDAP("SWF_DTI_ZOOM_PROVEEDOR"))
		addSWFtoSistema(zoom, SoftwareFactory.findByGrupoLDAP("SWF_DTI_ZOOM_TELECOM"))
	}

	private def addSWFtoSistema(sistema, swf) {
		if (sistema && swf && !sistema.tieneSoftwareFactory(swf)) {
			sistema.addToSoftwareFactories(swf)
			sistema.save(flush: true, failOnError: true)
		}
	}

	private def definirVersionador(){

		if (Versionador.count() == 0) {
			new Versionador(descripcion: "Harvest").save(flush: true, failOnError: true)
			new Versionador(descripcion: "Endevor").save(flush: true, failOnError: true)
			new Versionador(descripcion: "Team Connection").save(flush: true, failOnError: true)
			new Versionador(descripcion: "Developer Workbench").save(flush: true, failOnError: true)
		}

	}

	private def definirAnexosListaBlanca(){
		new AnexoListaBlanca(extension: "jpg", descripcion: "Archivo de imagen habilitado").save(flush: true, failOnError: true)
		new AnexoListaBlanca(extension: "txt", descripcion: "Archivo de texto habilitado").save(flush: true, failOnError: true)
		new AnexoListaBlanca(extension: "doc", descripcion: "Archivo de doc habilitado").save(flush: true, failOnError: true)
		new AnexoListaBlanca(extension: "docx", descripcion: "Archivo de docx habilitado").save(flush: true, failOnError: true)
		new AnexoListaBlanca(extension: "xls", descripcion: "Archivo de xls habilitado").save(flush: true, failOnError: true)
		new AnexoListaBlanca(extension: "xlsx", descripcion: "Archivo de xlsx habilitado").save(flush: true, failOnError: true)
		new AnexoListaBlanca(extension: "ppt", descripcion: "Archivo de ppt habilitado").save(flush: true, failOnError: true)
		new AnexoListaBlanca(extension: "pptx", descripcion: "Archivo de pptx habilitado").save(flush: true, failOnError: true)
	}


}"AF_EVA_POSTBAJA/COMPUTO_IMPOSITIVO", "GO_DTI_FACTURACION")
		completarDatosSistemas(factesp, "RSWF_DTI_FACTESP", false, "AF_FACTESP", "GO_DTI_FACTURACION")
		completarDatosSistemas(fax_server, "RSWF_DTI_FAX_SERVER_APL", false, "AF_FAX_SERVER", "GO_DTI_VENTAS")
		completarDatosSistemas(fax_server_segat, "RSWF_DTI_FAX_SERVER_SEGAT_APL", false, "AF_FAX_SERVER_SEGAT", "GO_DTI_VENTAS")
		completarDatosSistemas(fosse, "RSWF_DTI_FOSSE", false, "AF_FOSSE", null)
		completarDatosSistemas(frontbi_cctbi, "RSWF_DTI_FRONTBI", false, "AF_FRONTBI_CCTBI", "GO_DTI_VENTAS")
		completarDatosSistemas(frontbi_dacota, "RSWF_DTI_FRONTBI", false, "AF_FRONTBI_DACOTA", "GO_DTI_FACTURACION")
		completarDatosSistemas(frontbi_smart, "RSWF_DTI_FRONTBI", false, "AF_FRONTBI_SMART", "GO_DTI_FACTURACION")
		completarDatosSistemas(frontbi_tabgo, "RSWF_DTI_FRONTBI", false, "AF_FRONTBI_TABGO", "GO_DTI_VENTAS")
		completarDatosSistemas(frontbi_tabra, "RSWF_DTI_FRONTBI", false, "AF_FRONTBI_TABRA", "GO_DTI_FACTURACION")
		completarDatosSistemas(frontbi_zoom, "RSWF_DTI_FRONTBI", false, "AF_FRONTBI_ZOOM", "GO_DTI_VENTAS")
		completarDatosSistemas(genesis_bill_cycle, "RSWF_DTI_GENESIS_BILL_CYCLE", true, "AF_GENESIS_BILL_CYCLE", "GO_DTI_FACTURACION")
		completarDatosSistemas(genesis_customer, "RSWF_DTI_GENESIS_CUSTOMER", true, "AF_GENESIS_CUSTOMER", "GO_DTI_FACTURACION")
		completarDatosSistemas(genesis_mic, "RSWF_DTI_GENESIS_MIC", true, "AF_GENESIS_MIC", "GO_DTI_FACTURACION")
		completarDatosSistemas(genesis_oyp, "RSWF_DTI_GENESIS_OYP", true, "AF_GENESIS_OYP", "GO_DTI_FACTURACION")
		completarDatosSistemas(genesis_ups, "RSWF_DTI_GENESIS_UPS", true, "AF_GENESIS_UPS", "GO_DTI_FACTURACION")
		completarDatosSistemas(gesprod, "RSWF_DTI_GESPROD", false, "AF_GESPROD", "GO_DTI_VENTAS")
		completarDatosSistemas(gestion_de_emprendimientos, "RSWF_DTI_GESTION_DE_EMPRENDIMIENTOS", false, "AF_GESTION_DE_EMPRENDIMIENTOS", null)
		completarDatosSistemas(gestor_de_comisiones, "RSWF_DTI_DA_VINCI_GESTOR_DE_COMISIONES", false, "AF_GESTOR_DE_COMISIONES", "GO_DTI_VENTAS")
		completarDatosSistemas(girafe, "RSWF_DTI_GIRAFE", false, "AF_GIRAFE", "GO_DTI_VENTAS")
		completarDatosSistemas(guc, "RSWF_DTI_GUC", false, "AF_GUC", "GO_DTI_FACTURACION")
		completarDatosSistemas(interconnect, "RSWF_DTI_INTERCONNECT", false, "AF_INTERCONNECT", null)
		completarDatosSistemas(ipb, "RSWF_DTI_IPB", false, "AF_IPB", "GO_DTI_FACTURACION")
		completarDatosSistemas(iris, "RSWF_ST_Inventarios", false, "AF_IRIS", "GO_DST_SISTEMASTECNICOS")
		completarDatosSistemas(ium, "RSWF_DTI_IUM", false, "AF_IUM", "GO_DTI_FACTURACION")
		completarDatosSistemas(jano, "RSWF_DTI_JANO", false, "AF_JANO", "GO_DTI_VENTAS")
		completarDatosSistemas(jano_proceso_etl, "RSWF_DTI_JANO", false, "AF_JANO_PROCESO_ETL", "GO_DTI_VENTAS")
		completarDatosSistemas(kingdom, "RSWF_DTI_KINGDOM", false, "AF_KINGDOM", "GO_DTI_VENTAS")
		completarDatosSistemas(litigios, "RSWF_SC_LEGALES", false, "AF_LITIGIOS", "GO_SC_SISTEMAS CORPORATIVOS")
		completarDatosSistemas(mac, "RSWF_DTI_MAC", false, "AF_MAC", "GO_DTI_VENTAS")
		completarDatosSistemas(mi_cuenta_back_end, "RSWF_DTI_MICUENTA@", false, "AF_MI_CUENTA_BACK_END", "GO_DTI_VENTAS")
		completarDatosSistemas(mi_cuenta_front_end, "RSWF_DTI_MICUENTA@", false, "AF_MI_CUENTA_FRONT_END", null)
		completarDatosSistemas(nba, "RSWF_DTI_NBA", false, "AF_NBA", null)
		completarDatosSistemas(needit, "RSWF_SC_NEEDIT", false, "AF_NEEDIT", "GO_SC_SIGIT")
		completarDatosSistemas(network_inventory, "RSWF_ST_Inventarios", false, "AF_NETWORK_INVENTORY", "GO_DST_SISTEMASTECNICOS")
		completarDatosSistemas(nred, "RSWF_ST_Inventarios", false, "AF_NRED", "GO_DST_SISTEMASTECNICOS")
		completarDatosSistemas(ods, "RSWF_DTI_ODS", false, "AF_ODS", "GO_DTI_FACTURACION")
		completarDatosSistemas(persa, "RSWF_ST_PERSA", false, "AF_PERSA", null)
		completarDatosSistemas(plataforma_de_precalificacion, "RSWF_ST_PLATAFORMA-PRECALIFICACION", false, "AF_PLATAFORMA_DE_PRECALIFICACION", null)
		completarDatosSistemas(plataforma_de_testing, "RSWF_ST_PLATAFORMA-TESTING", false, "AF_PLATAFORMA_DE_TESTING", null)
		completarDatosSistemas(portal_bi_utf, "RSWF_DTI_PORTAL BI UTF", false, "AF_PORTAL_BI_UTF", "GO_DTI_VENTAS")
		completarDatosSistemas(prod_arnet_portales_arnet, "RSWF_DTI_Venta On line", false, "AF_PROD_ARNET_PORTALES_ARNET", "GO_DTI_VENTAS")
		completarDatosSistemas(prod_arnet_portales_back_office, "RSWF_DTI_Venta On line", false, "AF_PROD_ARNET_PORTALES_BACK_OFFICE", "GO_DTI_VENTAS")
		completarDatosSistemas(prod_arnet_portales_telecom, "RSWF_DTI_Venta On line", false, "AF_PROD_ARNET_PORTALES_TELECOM", "GO_DTI_VENTAS")
		completarDatosSistemas(pyo, "RSWF_DTI_PYO", false, "AF_PYO", "GO_DTI_FACTURACION")
		completarDatosSistemas(qv_asse, "RSWF_SC_QLIKVIEW_CORPORATIVO", false, "AF_QV_ASSE", "GO_SC_SISTEMAS CORPORATIVOS")
		completarDatosSistemas(qv_clie, "RSWF_SC_QLIKVIEW_CORPORATIVO", false, "AF_QV_CLIE", "GO_SC_SISTEMAS CORPORATIVOS")
		completarDatosSistemas(qv_masivo, "RSWF_ST_QVMASIVO", false, "AF_QV_MASIVO", "GO_DST_SISTEMASTECNICOS")
		completarDatosSistemas(qv_nomasivo, "RSWF_ST_QVMASIVO", false, "AF_QV_NOMASIVO", "GO_DST_SISTEMASTECNICOS")
		completarDatosSistemas(qv_remedy, "RSWF_ST_QVMASIVO", false, "AF_QV_REMEDY", "GO_DST_SISTEMASTECNICOS")
		completarDatosSistemas(qv_siag, "RSWF_SC_QLIKVIEW_CORPORATIVO", false, "AF_QV_SIAG", "GO_SC_SISTEMAS CORPORATIVOS")
		completarDatosSistemas(qv_simplit, "RSWF_ST_QVMASIVO", false, "AF_QV_SIMPLIT", "GO_DST_SISTEMASTECNICOS")
		completarDatosSistemas(qv_ssgg, "RSWF_SC_QLIKVIEW_CORPORATIVO", false, "AF_QV_SSGG", "GO_SC_SISTEMAS CORPORATIVOS")
		completarDatosSistemas(remedyred, "RSWF_ST_REMEDYRED", false, "AF_REMEDYRED", "GO_DST_SISTEMASTECNICOS")
		completarDatosSistemas(reporte_aseguramiento, "RSWF_ST_QVMASIVO", false, "AF_REPORTE_ASEGURAMIENTO", "GO_DST_SISTEMASTECNICOS")
		completarDatosSistemas(sas, "RSWF_ST_SISTEMA-AFECTACION-SERVICIOS", false, "AF_SAS", null)
		completarDatosSistemas(scard, "RSWF_DTI_SCARD_COLD", false, "AF_SCARD", "GO_DTI_FACTURACION")
		completarDatosSistemas(segat, "RSWF_ST_SEGAT", false, "AF_SEGAT", "GO_DST_SISTEMASTECNICOS")
		completarDatosSistemas(sei, "RSWF_DTI_SEI", false, "AF_SEI", "GO_DTI_VENTAS")
		completarDatosSistemas(seta, "RSWF_ST_Inventarios", false, "AF_SETA", "GO_DST_SISTEMASTECNICOS")
		completarDatosSistemas(sgct, "RSWF_DTI_SGCT", false, "AF_SGCT", null)
		completarDatosSistemas(sgdatos, "RSWF_ST_SG-DATOS", false, "AF_SGDATOS", null)
		completarDatosSistemas(sgdatos_tp, "RSWF_ST_SG-DATOS", false, "AF_SGDATOS_TP", null)
		completarDatosSistemas(sharepoint, "RSWF_SC_Sharepoint", false, "AF_SHAREPOINT", "GO_SC_SISTEMAS CORPORATIVOS")
		completarDatosSistemas(siaf, "RSWF_SC_FIN", false, "AF_SIAF", "GO_SC_SISTEMAS CORPORATIVOS")
		completarDatosSistemas(sideral, "RSWF_ST_SIDERAL", false, "AF_SIDERAL", null)
		completarDatosSistemas(siebel_customer_centric, "RSWF_DTI_SIEBEL", false, "AF_SIEBEL_CUSTOMER_CENTRIC", "GO_DTI_VENTAS")
		completarDatosSistemas(signo_cap, "RSWF_SC_FIN", false, "AF_SIGNO_CAP", "GO_SC_SISTEMAS CORPORATIVOS")
		completarDatosSistemas(signo_cobranzas, "RSWF_SC_FIN", false, "AF_SIGNO_COBRANZAS", "GO_SC_SISTEMAS CORPORATIVOS")
		completarDatosSistemas(signo_compras, "RSWF_SC_MM", false, "AF_SIGNO_COMPRAS", "GO_SC_SISTEMAS CORPORATIVOS")
		completarDatosSistemas(signo_compras_estrategias, "RSWF_SC_MM", false, "AF_SIGNO_COMPRAS_ESTRATEGIAS", "GO_SC_SISTEMAS CORPORATIVOS")
		completarDatosSistemas(signo_ess, "RSWF_SC_APLRRHH", false, "AF_SIGNO_ESS", "GO_SC_SISTEMAS CORPORATIVOS")
		completarDatosSistemas(signo_ess_celulares, "RSWF_SC_APLRRHH", false, "AF_SIGNO_ESS_CELULARES", "GO_SC_SISTEMAS CORPORATIVOS")
		completarDatosSistemas(signo_ess_mi_perfil, "RSWF_SC_APLRRHH", false, "AF_SIGNO_E