import ar.com.telecom.exceptions.UserWithoutRolesException

class UrlMappings {
	
	static mappings = {
		"/$controller/$action?/$id?"{
			constraints {
				// apply constraints here
			}
		}
		"/"(view:"/index")
		"500"(controller: 'errorApp', action: 'errorRole', exception: UserWithoutRolesException)
		"500"(view: '/errorApp')
		"404"(view:'/error404')
		"419"(controller: 'errorApp', action: 'errorAjaxError')
		"420"(controller: 'errorApp', action: 'errorAjaxError')
		"421"(controller: 'errorApp', action: 'errorAjaxWarn')
		"422"(controller: 'errorApp', action: 'errorAjaxMsg')
	}
}
