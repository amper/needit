package needit

import javax.servlet.http.Cookie

class EncodeHtmlFilters {

	def richInputs = [
		"registroPedido": [
			"objetivo",
			"descripcion",
			"alcance"
		],
		"especificacionCambio": [
			"especificacionDetallada",
			"fueraAlcance",
			"alcanceDetallado",
			"notas",
			"beneficiosEsperados"
		],
		"consolidacionImpacto": [
			"premisasSupuestosRestricciones",
			"descripcionVolumetriaPerformance",
			"descripcionFuncionalSolucion",
			"principalesCambiosProcesos",
			"interfacesInvolucradasSolucion",
			"definicionesPendientesWarnings"
		],
	
		"configuracionEmailTipoEvento": ["texto", "fase"],
		"configuracionEmailCambioEstado": ["texto", "fase"],
		"configuracionEMailReasignacion": ["texto", "fase"]]

	def filters = {
		all(controller:'*', action:'*') {
			before = {
				params.each{ key, value ->
					if(!richInputs."${params.controller}"?.contains(key) && value instanceof String && value.matches(".*(<|>)+.*"))
						params."$key" = value.encodeAsHTML()
				}
			}
			after = {
			}

			afterView = {
			}
		}

	}
}
