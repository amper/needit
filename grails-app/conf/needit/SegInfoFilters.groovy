package needit

/**
 * 
 * @author FED
 *<
 * Esta clase asigna los flags secure y httpOnly a las cookies
 * Está duplicado con el filtro SegInfoFilter
 * A futuro hay que deprecar alguna de las soluciones
 * Momentáneamente se deja para ver cómo implementarlo
 * 
 */
class SegInfoFilters {

	def filters = {
		all(controller:'*', action:'*') {
			before = {
			}

			after = {
				request.cookies.each { cookie ->
					def header = new StringBuffer("")
					header.append "${cookie.name}=${cookie.value}"
					header.append ";Secure"
					header.append ";httpOnly"
					if (cookie.version == 1) {
						header.append ";Version=1"
					}
					if (cookie.comment) {
						header.append ";Comment=\"${cookie.comment}\""
					}
					if (cookie.maxAge > 0) {
						header.append ";Max-Age=${cookie.maxAge}"
					}

					if (cookie.domain) {
						header.append ";Domain=${cookie.domain}"
					}
					if (cookie.path) {
						header.append ";Path=${cookie.path}"
					}
					response.setHeader("Set-Cookie", header.toString())
				}
			}

			afterView = {
			}
		}
	}
}
