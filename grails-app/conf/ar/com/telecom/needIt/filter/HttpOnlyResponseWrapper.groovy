package ar.com.telecom.needIt.filter

import javax.servlet.http.Cookie
import javax.servlet.http.HttpServletResponseWrapper

public class HttpOnlyResponseWrapper extends HttpServletResponseWrapper {

	public HttpOnlyResponseWrapper(res) {
		super(res)
	}

	public void addCookie(Cookie cookie) {
		//println "addCookie: " + cookie.name + "=" + cookie.value
		def header = new StringBuffer("")
		header.append "${cookie.name}=${cookie.value}"
		header.append ";Secure=true"
		header.append ";httpOnly=true"
		if (cookie.version == 1) {
			header.append ";Version=1"
		}
		if (cookie.comment) {
			header.append ";Comment=\"${cookie.comment}\""
		}
		if (cookie.maxAge > 0) {
			header.append ";Max-Age=${cookie.maxAge}"
		}

		if (cookie.domain) {
			header.append ";Domain=${cookie.domain}"
		}
		if (cookie.path) {
			header.append ";Path=${cookie.path}"
		}
		response.setHeader("Set-Cookie", header.toString())
		//println "addCookie - Header: " + header.toString()
		addHeader("Set-Cookie", header.toString());
	}
}
