package ar.com.telecom.needIt.filter

import java.io.IOException

import javax.servlet.Filter
import javax.servlet.FilterChain
import javax.servlet.FilterConfig
import javax.servlet.ServletException
import javax.servlet.ServletRequest
import javax.servlet.ServletResponse
import javax.servlet.http.HttpServletResponse

public class SegInfoFilter implements Filter {

	@Override
	public void destroy() {

	}

	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
	  //HttpOnlyResponseWrapper wrappedResponse = new HttpOnlyResponseWrapper(response);
//		
		request.cookies.each { cookie ->
			def header = new StringBuffer("")
			header.append "${cookie.name}=${cookie.value}"
			header.append ";Secure"
			header.append ";httpOnly"
			if (cookie.version == 1) {
				header.append ";Version=1"
			}
			if (cookie.comment) {
				header.append ";Comment=\"${cookie.comment}\""
			}
			if (cookie.maxAge > 0) {
				header.append ";Max-Age=${cookie.maxAge}"
			}

			if (cookie.domain) {
				header.append ";Domain=${cookie.domain}"
			}
			if (cookie.path) {
				header.append ";Path=${cookie.path}"
			}
			//response.setHeader("Set-Cookie", header.toString())
		}
//
	  chain.doFilter(request, response);
	}

	@Override
	public void init(FilterConfig filterConfig) throws ServletException {

	}

}
