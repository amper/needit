import ar.com.telecom.provider.MyLdapProvider
import ar.com.telecom.ume.MyUserDetailsService

beans = {
	
//	myLdapProvider(MyLdapProvider){
//		umeService = ref("umeService")
//	}
	//userDetailsService(MyUserDetailsService)
	
	switch (grails.util.GrailsUtil.environment) {
		case "test": 
			xmlns jee:"http://www.springframework.org/schema/jee"
			jee.'jndi-lookup'(id:"wsSimplitBean", 'jndi-name':"java:comp/env/ws/simplit")
//			xmlns jee:"http://www.springframework.org/schema/jee"
//			jee.'jndi-lookup'(id:"ldapConnection", 'jndi-name':"java:comp/env/ws/ldap")
			break
		case "production" : 
			xmlns jee:"http://www.springframework.org/schema/jee"
			jee.'jndi-lookup'(id:"wsSimplitBean", 'jndi-name':"java:comp/env/ws/simplit")
			break
		default : 
		    break
	}
}
