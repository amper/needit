import grails.plugins.springsecurity.SecurityConfigType
import grails.util.Environment

import org.apache.log4j.RollingFileAppender

import ar.com.telecom.exceptions.UserWithoutRolesException
import ar.com.telecom.pcs.entities.Auditoria
import ar.com.telecom.ume.UmeService

/* Evento al realizar un Success Login | Genero log de ultimo inicio de sesion */
grails.plugins.springsecurity.useSecurityEventListener = true
grails.plugins.springsecurity.onInteractiveAuthenticationSuccessEvent = { e, appCtx ->
	def principal = appCtx.springSecurityService.principal
	def sLegajo = principal.username

	def usuarioSinRoles = true 
	
	if (Environment.current != Environment.PRODUCTION) {
		usuarioSinRoles = principal.authorities.isEmpty()
	} else {
		usuarioSinRoles = principal.authorities.size() == 1
	}
	if ((usuarioSinRoles && !new UmeService().isGerente(sLegajo))) {
		throw new UserWithoutRolesException("Usted no tiene accesos a la aplicación. Por favor gestione los roles por TuID.")
	}

	if (Environment.current != Environment.DEVELOPMENT) {
		Auditoria.withTransaction {
			def oLogin = Auditoria.findByLegajo(sLegajo)

			if (!oLogin) {
				oLogin = new Auditoria(legajo: sLegajo)
			}
			oLogin.ultimoIngreso = new Date()
			oLogin.save()
		}
	} else {
	}
}

grails.project.groupId = appName // change this to alter the default package name and Maven publishing destination
grails.mime.file.extensions = true // enables the parsing of file extensions from URLs into the request format
grails.mime.use.accept.header = false
grails.mime.types = [ html: [
		'text/html',
		'application/xhtml+xml'
	],
	xml: [
		'text/xml',
		'application/xml'
	],
	text: 'text/plain',
	js: 'text/javascript',
	rss: 'application/rss+xml',
	atom: 'application/atom+xml',
	excel: 'application/vnd.ms-excel',
	pdf: 'application/pdf',
	css: 'text/css',
	csv: 'text/csv',
	all: '*/*',
	json: [
		'application/json',
		'text/json'
	],
	form: 'application/x-www-form-urlencoded',
	multipartForm: 'multipart/form-data'
]

// URL Mapping Cache Max Size, defaults to 5000
//grails.urlmapping.cache.maxsize = 1000

// The default codec used to encode data with ${}
grails.views.default.codec = "none" // none, html, base64 Pusimos html para evitar javascript injection
grails.views.gsp.encoding = "UTF-8"
grails.converters.encoding = "UTF-8"
// enable Sitemesh preprocessing of GSP pages
grails.views.gsp.sitemesh.preprocess = true
// scaffolding templates configuration
grails.scaffolding.templates.domainSuffix = 'Instance'

// Set to false to use the new Grails 1.2 JSONBuilder in the render method
grails.json.legacy.builder = false
// enabled native2ascii conversion of i18n properties files
grails.enable.native2ascii = true
// whether to install the java.util.logging bridge for sl4j. Disable for AppEngine!
grails.logging.jul.usebridge = true
// packages to include in Spring bean scanning
grails.spring.bean.packages = []

// request parameters to mask when logging exceptions
grails.exceptionresolver.params.exclude = ['password']

// log4j configuration
log4j = {
	appenders {
		def catalinaBase = System.properties.getProperty('catalina.base')
		if (!catalinaBase) catalinaBase = '.'   // just in case
		def logDirectory = "${catalinaBase}/logs"

		appender new RollingFileAppender(
				//name: "${appName}",
				name: "needIt",
				maxFileSize: 1024000,
				file: fileAppender,
				layout: pattern(conversionPattern: "%p %c{2} %x %d [%t] %C - %m%n")
		)
	}

	root { warn 'stdout', 'needIt' }

	warn needIt: [
		'org.codehaus.groovy.grails.web.servlet',
		//  controllers
		'org.codehaus.groovy.grails.web.pages',
		//  GSP
		'org.codehaus.groovy.grails.web.sitemesh',
		//  layouts
		'org.codehaus.groovy.grails.web.mapping.filter',
		// URL mapping
		'org.codehaus.groovy.grails.web.mapping',
		// URL mapping
		'org.codehaus.groovy.grails.commons',
		// core / classloading
		'org.codehaus.groovy.grails.plugins',
		// plugins
		'org.codehaus.groovy.grails.orm.hibernate',
		// hibernate integration
		'org.springframework',
		'org.hibernate',
		'net.sf.ehcache.hibernate'
	]

}

// Added by the Spring Security Core plugin:
grails.plugins.springsecurity.errors.login.disabled = "Disculpe, su cuenta esta deshabilitada."
grails.plugins.springsecurity.errors.login.expired = "Disculpe, su cuenta ha expirado."
grails.plugins.springsecurity.errors.login.passwordExpired = "Disculpe, su contraseña ha expirado."
grails.plugins.springsecurity.errors.login.locked = "Disculpe, su cuenta esta bloqueada."
grails.plugins.springsecurity.errors.login.fail = "Disculpe, no pudimos encontrar el usuario con estas credenciales."
grails.plugins.springsecurity.userLookup.userDomainClassName = 'ar.com.telecom.Person'
grails.plugins.springsecurity.userLookup.authorityJoinClassName = 'ar.com.telecom.PersonRole'
grails.plugins.springsecurity.authority.className = 'ar.com.telecom.Role'
grails.plugins.springsecurity.requestMap.className = 'ar.com.telecom.Requestmap'
grails.plugins.springsecurity.securityConfigType = SecurityConfigType.Requestmap
//grails.plugins.springsecurity.providerNames = ['ldapAuthProvider']
//grails.plugins.springsecurity.providerNames = ['myLdapProvider']
// Externalizado en el archivo
grails.plugins.springsecurity.ldap.authorities.defaultRole = 'ROLE_SOLO_CONSULTA'
 
//configuración NeedIt y LDAP externalizada

environments{
	development{
		fileAppender = "D:/grailsLogs/needItDev.log"
		grails.serverURL = "http://10.11.33.22:8080/${appName}"
		grails.config.locations = ["file:D:/grailsConfigs/${appName}-config.groovy", "file:D:/grailsConfigs/ldap-config.groovy"]
		imagen.needit = 'NeedItDESA.png'
//		ume.service.url = "http://10.11.33.22:8080/UMEServices/export"
		ume.service.url = "http://10.11.33.22:8080/UMEServices/export"
		grails.mail.host = "10.4.33.105"
		grails.mail.port = 25
	}
	pi {
		fileAppender = "D:/grailsLogs/needItPI.log"
		grails.serverURL = "http://10.11.33.22:8888/${appName}"
		grails.config.locations = ["file:D:/grailsConfigs/${appName}-config.groovy", "file:D:/grailsConfigs/ldap-config.groovy"]
		imagen.needit = 'NeedItPI.png'
		ume.service.url = "http://10.11.33.22:8080/UMEServices/export"
		grails.mail.host = "10.4.33.105"
		grails.mail.port = 25
		
	}
	fix{
		fileAppender = "D:/grailsLogs/needItFix.log"
		grails.serverURL = "http://10.11.33.90:8090/${appName}"
		grails.config.locations = ["file:D:/grailsConfigsFix/${appName}-config.groovy", "file:D:/grailsConfigsFix/ldap-config.groovy"]
		imagen.needit = 'NeedItFIX.png'
		grails.plugins.springsecurity.portMapper.httpPort='8090'
		grails.plugins.springsecurity.portMapper.httpsPort='8091'
		ume.service.url = "http://10.11.33.90:8090/UMEServices/export"
		grails.plugins.springsecurity.secureChannel.definition = [
			'/login/**':         'REQUIRES_SECURE_CHANNEL',
			'/inbox/**':         'REQUIRES_INSECURE_CHANNEL',
			'/inboxMobile/**':         'REQUIRES_INSECURE_CHANNEL',
			'/inboxAprobaciones/**':         'REQUIRES_INSECURE_CHANNEL',
			'/registroPedido/**':  'REQUIRES_INSECURE_CHANNEL'
		]
		grails.mail.host = "10.4.33.102"
		grails.mail.port = 25
	}
	test{
		fileAppender = "D:/grailsLogs/needIt.log"
		grails.serverURL = "http://10.11.33.90:8080/${appName}"
		grails.config.locations = ["file:D:/grailsConfigs/${appName}-config.groovy", "file:D:/grailsConfigs/ldap-config.groovy"]
		imagen.needit = 'NeedItQA.png'
		grails.plugins.springsecurity.portMapper.httpsPort='8081'
		ume.service.url = "http://10.11.33.22:8080/UMEServices/export"
		grails.plugins.springsecurity.secureChannel.definition = [
			'/login/**':         'REQUIRES_SECURE_CHANNEL',
			'/inbox/**':         'REQUIRES_INSECURE_CHANNEL',
			'/inboxMobile/**':         'REQUIRES_INSECURE_CHANNEL',
			'/inboxAprobaciones/**':         'REQUIRES_INSECURE_CHANNEL',
			'/registroPedido/**':  'REQUIRES_INSECURE_CHANNEL'
		]
		grails.mail.host = "10.4.33.102"
		grails.mail.port = 25
	}
	production{
		fileAppender = "D:/grailsLogs/needIt.log"
		grails.serverURL = "http://needIt"
		grails.config.locations = ["file:D:/grailsConfigs/${appName}-config.groovy", "file:D:/grailsConfigs/ldap-config.groovy"]
		imagen.needit = 'NeedItPROD.png'
		grails.plugins.springsecurity.portMapper.httpsPort='8081'
		ume.service.url = "http://10.77.100.225:8080/UMEServices/export"
		grails.plugins.springsecurity.secureChannel.definition = [
			'/login/**':         'REQUIRES_SECURE_CHANNEL',
			'/inbox/**':         'REQUIRES_INSECURE_CHANNEL',
			'/inboxMobile/**':         'REQUIRES_INSECURE_CHANNEL',
			'/inboxAprobaciones/**':         'REQUIRES_INSECURE_CHANNEL',
			'/registroPedido/**':  'REQUIRES_INSECURE_CHANNEL'
		]
		grails.mail.host = "10.4.33.105"
		grails.mail.port = 25
	}
}


appName = "${appName}"
errorAjax = 419
errorApp = 500
