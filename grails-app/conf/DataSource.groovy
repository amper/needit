dataSource {
}
hibernate {
    cache.use_second_level_cache=true
    cache.use_query_cache=true
    cache.provider_class='org.hibernate.cache.EhCacheProvider'
}
// environment specific settings
environments {
	development {
		dataSource{
			dbCreate="update"
			url="jdbc:sqlserver://10.11.33.22:1433;databaseName=needItPI;"
			driverClassName = "com.microsoft.sqlserver.jdbc.SQLServerDriver"
			username="pcs"
			password="pcs"
			loggingSql = false
		}
	}
	pi {
		dataSource{
			dbCreate="update"
			url="jdbc:sqlserver://10.11.33.22:1433;databaseName=needItPI;"
 			//url="jdbc:sqlserver://10.11.33.90:1433;databaseName=needItPAU;"
			driverClassName = "com.microsoft.sqlserver.jdbc.SQLServerDriver"
			username="pcs"
			password="pcs"
			loggingSql = false
		}
	}	
	test {
		dataSource {
            dbCreate="update"
			jndiName = "java:comp/env/jdbc/needIt"
            loggingSql = false
			pooled = true
		}
	}
	production {
		dataSource {
			dbCreate="update"
			jndiName = "java:comp/env/jdbc/needIt"
			loggingSql = false
			pooled = true
		}
	}
}

