package ar.com.telecom.pcsPlus

import org.springframework.transaction.annotation.Transactional

import ar.com.telecom.pcs.entities.Periodo
import ar.com.telecom.pcs.entities.SoftwareFactory

class CapacidadService {

	static transactional = false

	@Transactional
	def getPeriodos(fechaDesde, fechaHasta) {
		return Periodo.getPeriodos(fechaDesde, fechaHasta)
	}
	
	@Transactional(readOnly = true)
	def getGruposSistema(unSistema) {
		def sistema = SoftwareFactory.findById(unSistema.id)
		sistema.traerDatosRelacionados()
		return sistema
	}
	
}
