package ar.com.telecom.pcsPlus

import org.springframework.transaction.annotation.Transactional

import ar.com.telecom.pcs.entities.Fase
import ar.com.telecom.pcs.entities.LogModificaciones
import ar.com.telecom.pcs.entities.RolAplicacion
import ar.com.telecom.util.DateUtil

class HistorialService {

	static transactional = false

	@Transactional(readOnly = true)
	private def definirPaginacion(params) {
		params.sort = params.sort?:'fechaHasta'
		params.order = params.order?:'desc'
		params.max = Math.min(params.max ? params.int('max') : 10, 100)
		params.offset = params.offset? params.offset.toInteger():0
	}

	@Transactional(readOnly = true)
	def obtenerHistorialPaginado(params, pedido) {
		definirPaginacion(params)
		def logModificaciones = LogModificaciones.createCriteria().list(max: params.max, offset: params.offset) {
			createAlias ('pedido', 'pedido')
			or {
				eq ('pedido.id', params.pedidoId.toInteger())
				inList ('pedido.id', [0] + pedido.pedidosHijos.collect { pedidoHijo -> pedidoHijo.id }) // el 0 es sólo para que no pinche
			}
			createAlias ('fase', 'fase')
			if (params.get("multiselect_comboboxFase")){
				def listadoFase = params?.multiselect_comboboxFase.class.isArray() ? params?.multiselect_comboboxFase as List : [params?.multiselect_comboboxFase]
				'in'  ("fase.id", listadoFase.collect{value -> Long.parseLong(value)})
			}
			if (params.get("multiselect_comboboxMacroestado")){
				def listadoMacroestado = params?.multiselect_comboboxMacroestado.class.isArray() ? params?.multiselect_comboboxMacroestado as List : [params?.multiselect_comboboxMacroestado]
				'in' ("fase.macroEstado.id", listadoMacroestado.collect{value -> Long.parseLong(value)})
			}

			if (params.get("multiselect_comboboxRolAplicacion")){
				List listRoles = []
				params?.multiselect_comboboxRolAplicacion.collect{value ->
					listRoles << RolAplicacion.findById(value.toString()).codigoRol
				}
				'in' ("rol", listRoles)
			}
			
			if (params.fechaDesde && params.fechaHasta){
				params.fechaDesde = DateUtil.toDate(params.fechaDesde)
				params.fechaHasta = DateUtil.toDate(params.fechaHasta)
				Calendar cal = Calendar.getInstance();
				cal.setTime(params.fechaHasta)
				cal.add(Calendar.DAY_OF_YEAR, 1)
				between("fechaDesde", params.fechaDesde, cal.getTime())
			}else{
				params.fechaDesde = null
				params.fechaHasta = null
			}
			if (params.descripcion){
				like("descripcionEvento","%$params.descripcion%")
			}
			if (params.get("multiselect_comboboxTipoEvento")){
				def listSeleccionados = []
				def listEventos = LogModificaciones.getTipoEventos()
				def listadoTipoEventoSeleccionados = params.get("multiselect_comboboxTipoEvento")
				listadoTipoEventoSeleccionados = listadoTipoEventoSeleccionados.class.isArray() ? listadoTipoEventoSeleccionados as List : [listadoTipoEventoSeleccionados]
				listadoTipoEventoSeleccionados.each { tipoEvento ->
					def evento = listEventos.get(tipoEvento)
					if (evento){
						listSeleccionados << evento
					}
				}
				if (!listSeleccionados.isEmpty()){
					'in'("tipoEvento", listSeleccionados)
				}
			}
			if (params.legajoUsuario){
				eq("legajo", params.legajoUsuario)
			}
			if (params.sort != 'descripcionEvento'){
				if (params.sort == 'macroEstado'){
					order ('fase.macroEstado', params.order)
				}else{
					order (params.sort, params.order)
				}
			}
		}
		//Lo guardo en params para despues madnar el total, el total esta sujeto a la forma de ordenar
		params.totalCountLog = logModificaciones.totalCount

		if (params.sort=='descripcionEvento'){
			if (params.order=='asc')
				logModificaciones=logModificaciones.sort { elem -> elem.descripcionEvento}
			else
				logModificaciones=logModificaciones.sort { elem -> elem.descripcionEvento}.reverse()
		}
		
		return logModificaciones
	}
	
	@Transactional(readOnly = true)
	def obtenerFases(macroEstadoID){
		def fases
		def macros 
		
		if (macroEstadoID)
			macros = macroEstadoID.class.isArray() ? macroEstadoID as List : [macroEstadoID]
		
		if (macros){
			fases=Fase.createCriteria().list(){
				'in'("macroEstado.id", macros.collect { it.toLong() })
			}
		}else{
			fases = Fase.getAll()
		}
		return fases
	}
}
