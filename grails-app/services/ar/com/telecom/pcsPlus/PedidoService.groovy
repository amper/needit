package ar.com.telecom.pcsPlus

import grails.util.Environment

import org.hibernate.Criteria
import org.hibernate.StaleObjectStateException
import org.hibernate.criterion.CriteriaSpecification
import org.springframework.transaction.annotation.Transactional

import ar.com.telecom.FaseMultiton
import ar.com.telecom.exceptions.BusinessException
import ar.com.telecom.exceptions.ConcurrentAccessException
import ar.com.telecom.pcs.entities.AbstractPedido
import ar.com.telecom.pcs.entities.Actividad
import ar.com.telecom.pcs.entities.ActividadPlanificacion
import ar.com.telecom.pcs.entities.Aprobacion
import ar.com.telecom.pcs.entities.DetallePlanificacion
import ar.com.telecom.pcs.entities.DetallePlanificacionSistema
import ar.com.telecom.pcs.entities.ECS
import ar.com.telecom.pcs.entities.ECSOtrasPruebas
import ar.com.telecom.pcs.entities.EstadoActividad
import ar.com.telecom.pcs.entities.MailAudit
import ar.com.telecom.pcs.entities.ParametrizacionTipoPrueba
import ar.com.telecom.pcs.entities.ParametrosSistema
import ar.com.telecom.pcs.entities.Pedido
import ar.com.telecom.pcs.entities.PedidoHijo
import ar.com.telecom.pcs.entities.PlanificacionEsfuerzo
import ar.com.telecom.pcs.entities.Release
import ar.com.telecom.pcs.entities.ReporteAprobacionEnviados
import ar.com.telecom.pcs.entities.RolAplicacion
import ar.com.telecom.pcs.entities.Sistema
import ar.com.telecom.pcs.entities.TipoGestion
import ar.com.telecom.pcs.entities.TipoImpacto
import ar.com.telecom.pcs.entities.UmbralAprobacionEconomica
import ar.com.telecom.pcs.entities.UmbralAprobacionEconomicaDireccion
import ar.com.telecom.pedido.AprobacionUI
import ar.com.telecom.pedido.consulta.Consulta
import ar.com.telecom.pedido.consulta.ConsultaInbox
import ar.com.telecom.pedido.consulta.ConsultaPedido
import ar.com.telecom.pedido.consulta.TipoConsulta
import ar.com.telecom.pedido.especificacionImpacto.Impacto
import ar.com.telecom.util.NumberUtil
import ar.com.telecom.util.StringUtil


class PedidoService {
	def umeService
	def anexoService
	def mailService
	def configuracionMailService
	// def dataSource

	static transactional = false

	static MAX_PEDIDOS_ONLINE = 5
	static MAX_INBOX_APROBACIONES = 10


	/**
	 * 
	 * Recupera los datos del pedido en base al id
	 * 
	 * @param id
	 * @return Pedido
	 */
	@Transactional(readOnly = true)
	def obtenerPedido(params, id, boolean actualizaObserversHijos){
		def pedido

		if (id && id != 'null') {
			def idPedido = id as int
			pedido = Pedido.get(idPedido)

			if (pedido) {
				pedido.traerDatosRelacionados()
				// Otra opción es tirar un fetch eager en el get del id
			} else {
				throw new BusinessException("No se encontró el pedido " + id)
			}
		} else {
			pedido = new Pedido(legajoUsuarioCreador: umeService.usuarioLogueado, titulo: "")
		}
		agregarObservers(pedido, actualizaObserversHijos)
		return pedido
	}


	/**
	 * Permite leer el pedido de la base sin actualizar dependencias, sólo para tomar la última información
	 * @param id
	 * @return
	 */
	@Transactional(readOnly = true)
	def obtenerPedidoInicial(id, boolean traerDatosRelacionados) {
		def pedido

		if (id && id != 'null') {
			def idPedido = id as int
			pedido = AbstractPedido.get(idPedido)
			if (traerDatosRelacionados) {
				pedido?.traerDatosRelacionados()
			}
		}
		return pedido
	}

	/**
	 *
	 * Recupera los datos del pedido en base al id
	 *
	 * @param id
	 * @return Pedido
	 */
	@Transactional(readOnly = true)
	def obtenerPedido(params, id) {
		return obtenerPedido(params, id, true)
	}
	
	/**
	*
	* Recupera los datos de la aprobacion en base al id
	*
	* @param id
	* @return Aprobacion
	*/
   @Transactional(readOnly = true)
   def obtenerAprobacion(id) {
	   return Aprobacion.findById(id)
   }

	/**
	 *
	 * Recupera los datos del pedido en base al id
	 *
	 * @param id
	 * @return Pedido
	 */
	@Transactional(readOnly = true)
	def obtenerPedidoHijo(params, id){
		def pedidoHijo

		if (id && id != 'null') {
			def idPedido = id as int
			pedidoHijo = PedidoHijo.get(idPedido)
			if (pedidoHijo) {
				pedidoHijo.traerDatosRelacionados()
			} else {
				throw new BusinessException("No se encontró el pedido hijo " + id)
			}
		} else {
			return null
		}
		agregarObservers(pedidoHijo)

		def pedidoPadre = pedidoHijo.pedidoPadre
		agregarObservers(pedidoPadre, false)

		return pedidoHijo
	}

	@Transactional(readOnly = true)
	def obtenerPedidoGenerico(params, id) {
		obtenerPedidoGenerico(params, id, true)
	}

	@Transactional(readOnly = true)
	def obtenerPedidoGenerico(params, id, agregarObservers){
		def pedido

		if (id && id != 'null') {
			def idPedido = id as int
			pedido = Pedido.get(id)
			if (pedido) {
				pedido.traerDatosRelacionados()
			} else {
				pedido = PedidoHijo.get(id)
				if (pedido) {
					pedido.traerDatosRelacionados()
				}
			}
		}

		if (pedido && agregarObservers) {
			this.agregarObservers(pedido)
		}
		return pedido
	}

	@Transactional(readOnly = true)
	def obtenerListadoPedidosInbox(listadoPadresIds, params){
		return Pedido.createCriteria().list(max: params.max, offset: params.offset) {
			inList("id", listadoPadresIds)
			//createAlias ('faseActual', 'fasePedido')
			order (params.sort, params.order)
		}
	}

	@Transactional(readOnly = true)
	def obtenerPedidosInbox(params, usuarioLogueado){
		def listado = consultarAprobacionesInbox(params, usuarioLogueado)
		if (!params.tipoConsulta.equals(Consulta.EN_SEGUIMIENTO_PROPIOS)){
			listado = ordenarInbox(listado.pedido, params) //listado en este caso es de aprobaciones
		}else{
			listado = ordenarInbox(listado, params) //listado en este caso es de pedidos
		}
		return listado
	}

	@Transactional(readOnly = true)
	def inboxCount(params, usuarioLogueado) {
		ConsultaInbox consultaInbox = setConsultaInbox(params, usuarioLogueado)
		return pedidosInbox(params, consultaInbox)
	}

	@Transactional(readOnly = true)
	def ordenarInbox(pedidos, params){
		def pedidosPadres = pedidos.collect { it.parent }.unique { a, b->
			a.id <=> b.id
		}
		definirPaginacionInbox(params)
		def listadoPedidos
		if (pedidosPadres.isEmpty()) {
			params.totalCount = 0
			listadoPedidos = []
		} else {
			listadoPedidos = obtenerListadoPedidosInbox(pedidosPadres.id, params)
			params.totalCount = listadoPedidos.totalCount
		}
		return listadoPedidos
	}

	/**
	 *
	 * Incorpora observers a un pedido
	 *
	 * @param id
	 * @return
	 */
	@Transactional(readOnly = true)
	def agregarObservers(AbstractPedido pedido) {
		agregarObservers(pedido, true)
	}

	/**
	 *
	 * Incorpora observers a un pedido
	 *
	 * @param id
	 * @return
	 */
	@Transactional(readOnly = true)
	def agregarObservers(AbstractPedido pedido, boolean actualizaObserversHijos) {
		pedido.crearObserversIniciales()
		configuracionMailService.definirObservers(pedido, actualizaObserversHijos)
		log.info "Agregar observers. Observers encontrados para el pedido: " + pedido.observers
		pedido.inicializarObservers()
	}

	/**
	 *	// ANTES
	 * if (!pedido.save(flush: true, failOnError: true)) {
	 * http://jira.grails.org/browse/GRAILS-5698
	 * http://jira.grails.org/browse/GPSEARCHABLE-60
	 * http://www.grails.org/plugin/gorm-labs
	 * http://osdir.com/ml/lang.groovy.grails.user/2006-11/msg00033.html
	 * http://stackoverflow.com/questions/6768242/grails-test-app-lazyinitializationexception
	 * http://burtbeckwith.com/blog/files/169/gorm%20grails%20meetup%20presentation.pdf
	 * 
	 * Método privado / interno para persistir un pedido en la base
	 * 
	 * @param el pedido
	 * @return si se pudo guardar satisfactoriamente
	 */
	@Transactional
	private boolean doGuardar(pedido) {
		pedido.guardar(umeService.getUsuarioLogueado())
		if (!pedido.save(flush: true, failOnError: true)) {
			pedido.errors.each { log.error it }
			pedido.discard()
			return false
		}
		return true
	}

	/**
	 * Método público para persistir en la base
	 * no considera accesos concurrentes
	 * 
	 * @param pedido
	 * @return
	 */
	@Transactional
	boolean guardar(params, pedido) {
		try {
			def result = doGuardar(pedido)
			if (result){
				enviaMailsPendientes(pedido)
			}
			return result
		} catch (Exception e) {
			this.procesarErrorBaseDatos(e)
		}
	}

	/**
	 * Persiste un pedido en la base a partir de una aprobación
	 * considera accesos concurrentes
	 * @param aprobacionUI
	 * @param inicializaObservers true/false
	 * @return si se pudo guardar satisfactoriamente
	 */
	@Transactional
	boolean guardarPedido(params, aprobacionUI, boolean cambiaResponsable) {
		guardarPedido(params, aprobacionUI, cambiaResponsable, false)
	}

	@Transactional
	boolean guardarPedido(params, aprobacionUI, boolean cambiaResponsable, boolean chequeaEstado) {
		try {
			def pedido = aprobacionUI.pedido
			def origenId  = params.actividadId?: params.aprobacionId
			def origen = bloquearAprobacion(origenId, params.actividadId != null, aprobacionUI, cambiaResponsable)
			if (chequeaEstado){
				validarEstadoPedido(origen)
			}
			actualizarAprobaciones(aprobacionUI, origen, cambiaResponsable)
			def result = doGuardar(pedido)
			if (result){
				enviaMailsPendientes(pedido)
			}
			return result
		} catch (Exception e) {
			this.procesarErrorBaseDatos(e)
		}
	}
	
	@Transactional
	boolean guardarActividad(actividad) {
		return guardarActividad(actividad, false)
	}

	@Transactional
	def reasignarActividades(pedido, grupoActividad, legajoActual, legajoNuevo) {
		pedido.getAllActividadesPendientes(grupoActividad, legajoActual).each { actividad -> 
			actividad.legajoUsuarioResponsablePop = legajoNuevo
			this.guardarActividad(actividad, true)  
		}
	}
	
	@Transactional
	boolean guardarActividad(actividad, boolean cambiaResponsable) {
		try {
			def pedido = actividad.pedido
			def origen = bloquearActividad(actividad, cambiaResponsable)
			actualizarAprobaciones(new AprobacionUI(pedido: pedido, asignatario: actividad.legajoUsuarioResponsablePop, grupo: actividad.grupoResponsable), origen, cambiaResponsable)
			def result = doGuardar(pedido)
			if (result){
				enviaMailsPendientes(pedido)
			}
			return result
		} catch (Exception e) {
			this.procesarErrorBaseDatos(e)
		}
	}

	@Transactional
	boolean guardarPedido(params, aprobacionUI) {
		this.guardarPedido(params, aprobacionUI, false)
	}

	@Transactional
	boolean cambiarResponsable(params, aprobacionUI) {
		this.guardarPedido(params, aprobacionUI, true)
	}

	/**
	 * Simula la validación del pedido en el avance (ahora debemos persistir los cambios)
	 * @param aprobacionUI
	 * @return
	 */
	@Transactional
	boolean validarPedido(params, aprobacionUI) {
		def pedido = aprobacionUI.pedido
		def result = pedido.validar(aprobacionUI)
		if (result) {
			def origenId  = params.actividadId?: params.aprobacionId
			def origen = bloquearAprobacion(origenId, params.actividadId != null, aprobacionUI, false)
			actualizarAprobaciones(aprobacionUI, origen, false)
			this.guardar(params, pedido)
		}
		return result
	}

	/**
	 * 
	 * Avanza el pedido a la próxima fase
	 * @param aprobacion
	 * @return
	 */
	@Transactional
	boolean avanzarPedido(params, aprobacionUI) {
		def pedido = aprobacionUI.pedido

		def origenId  = params.actividadId?: params.aprobacionId
		def origen = bloquearAprobacion(origenId, params.actividadId != null, aprobacionUI, false)
		validarEstadoPedido(origen)
		actualizarAprobaciones(aprobacionUI, origen, false)

		if (!pedido.validar(aprobacionUI)) {
			return false
		}

		
		/*if (!pedido.codigoDireccionGerencia) {
			pedido.setearDireccionDe(umeService.getUsuario(pedido.legajoGerenteUsuario))
		}*/

		if (!pedido.id) {
			if (!this.doGuardar(pedido)) {
				return false
			}
		}
		
		def faseAnterior = pedido.faseActual
		internalAvanzar(aprobacionUI)
		while (pedido.tieneAprobacionOmitida() && !pedido.faseActual.equals(faseAnterior)) {
			faseAnterior = pedido.faseActual
			aprobacionUI.justificacion = "Aprobaci&oacute;n omitida"
			internalAvanzar(aprobacionUI)
		}
		
		// Seteo dirección del GU cuando esté vacío
		if (!pedido.codigoDireccionGerencia) {
			pedido.setearDireccionDe(umeService.getUsuario(pedido.legajoGerenteUsuario))
		}
		
		// Grails no tiene todavía do-while!
		this.guardar(params, pedido)
		return true
	}

	@Transactional
	private internalAvanzar(aprobacionUI) {
		AbstractPedido pedido = aprobacionUI.pedido
		this.agregarObservers(pedido)
		pedido.avanzar(aprobacionUI, umeService.getUsuarioLogueado())
		this.enviaMailsPendientes(pedido)
	}

	/**
	 *
	 * Retrocede el pedido a la fase anterior
	 * @param aprobacion
	 * @return
	 */
	@Transactional
	boolean denegarPedido(params, aprobacionUI) {
		def pedido = aprobacionUI.pedido

		def origenId  = params.actividadId?: params.aprobacionId
		def origen = bloquearAprobacion(origenId, params.actividadId != null, aprobacionUI, false)
		validarEstadoPedido(origen)
		actualizarAprobaciones(aprobacionUI, origen, false)
		if (!pedido.validar(aprobacionUI)) {
			return false
		}

		def faseAnterior = pedido.faseActual
		internalDenegar(aprobacionUI)
		while (pedido.tieneDenegacionOmitida() && !pedido.faseActual.equals(faseAnterior)) {
			faseAnterior = pedido.faseActual
			internalDenegar(aprobacionUI)
		}
		this.guardar(params, pedido)

		return true
	}

	@Transactional
	private internalDenegar(aprobacionUI) {
		AbstractPedido pedido = aprobacionUI.pedido
		this.agregarObservers(pedido)
		pedido.retroceder(aprobacionUI, umeService.getUsuarioLogueado())
		this.enviaMailsPendientes(pedido)
	}

	/**
	 *
	 * Cancela el pedido (queda inhabilitado el workflow) 
	 * @param el pedido
	 * @return si se pudo realizar la acción
	 */
	@Transactional
	def boolean cancelarPedido(params, pedido) {
		pedido.cancelar(umeService.getUsuarioLogueado())
		this.guardar(params, pedido)
		return true
	}

	/**
	 *
	 * Suspende el pedido (queda inhabilitado el workflow)
	 * @param el pedido
	 * @return si se pudo realizar la acción
	 */
	@Transactional
	def boolean suspenderPedido(params, pedido) {
		pedido.suspender(umeService.getUsuarioLogueado())
		this.guardar(params, pedido)
		return true
	}

	/**
	 *
	 * Levanta suspensión del pedido (queda habilitado el workflow nuevamente)
	 * @param el pedido
	 * @return si se pudo realizar la acción
	 */
	@Transactional
	def boolean levantaSuspensionPedido(params, pedido) {
		pedido.levantarSuspension(umeService.getUsuarioLogueado())
		this.guardar(params, pedido)
		return true
	}

	/**
	 *
	 */
	@Transactional(readOnly = true)
	def consultarAprobacionesInbox(params, legajo) {
		ConsultaInbox consultaInbox = setConsultaInbox(params, legajo )

		if (consultaInbox?.tipoConsulta.equals(Consulta.EN_SEGUIMIENTO_PROPIOS)){
			consultaInbox.tipoConsulta = Consulta.MIS_PENDIENTES
			def listadoMisPendientes = inboxAprobaciones(params, consultaInbox)
			inboxEnSeguimiento(params, consultaInbox, listadoMisPendientes.pedido)
		}else{
			def listado = inboxAprobaciones(params, consultaInbox)
			return listado
		}
	}

	@Transactional(readOnly = true)
	def setConsultaInbox(params, legajo ){
		def consultaInbox
		if (params.tipoConsulta) {
			consultaInbox = new ConsultaInbox(tipoConsulta: params.tipoConsulta, legajo: legajo)
		} else {
			consultaInbox = new ConsultaInbox(tipoConsulta: Consulta.MIS_PENDIENTES, legajo: legajo)
		}
		if (params.get("rolAplicacion.id") && !params.get("rolAplicacion.id").trim().equals("null")) {
			consultaInbox.rol = params.get("rolAplicacion.id")
		}

		params.tipoConsulta = consultaInbox.tipoConsulta
		return consultaInbox
	}

	@Transactional(readOnly = true)
	def consultarInboxAprobaciones(params, legajo) {
		def consultaInbox = setConsultaInbox(params, legajo )

		def listado = inboxAprobaciones(params, consultaInbox)
		params.totalCount = listado.size()
		return listado
	}


	@Transactional(readOnly = true)
	def busquedaAvanzadaPedidos(params) {
		definirPaginacionInbox(params)
		def pedidosList = Pedido.createCriteria().list(max: params.max, offset: params.offset) {
			inList ("faseActual.macroestado", params.macroestados)
			order (params.sort, params.order)
		}
		params.totalCount = pedidosList.totalCount
		pedidosList
	}

	def pedidosInbox(params, ConsultaInbox consultaInbox) {
		def idsPedidos = Aprobacion.createCriteria().list() {
			projections { // agrupamos por id de pedido
				groupProperty('pedido.id') }

			createAlias ('pedido', 'pedido')
			if ([
				Consulta.MIS_PENDIENTES,
				Consulta.PENDIENTES_MI_GRUPO
			].contains(consultaInbox?.tipoConsulta)) {
				createAlias ('pedido.actividades', 'actividades', Criteria.LEFT_JOIN)
				createAlias ('actividades.estado', 'estado', Criteria.LEFT_JOIN)
			}

			if (consultaInbox?.tipoConsulta.equals(Consulta.MIS_PENDIENTES)) {
				or {
					eq("usuario", consultaInbox?.legajo)
					and {
						eq("actividades.legajoUsuarioResponsable", consultaInbox?.legajo)
						eq("estado.descripcion", EstadoActividad.ABIERTA)
					}
				}
			}
			if (consultaInbox?.tipoConsulta.equals(Consulta.INBOX_APROBACIONES)) {
				eq("usuario", consultaInbox?.legajo)
				and{
					or{
						eq ("fase.id",FaseMultiton.getFase(FaseMultiton.VALIDACION_PEDIDO).id)
						eq ("fase.id",FaseMultiton.getFase(FaseMultiton.APROBACION_PEDIDO).id)
						eq ("fase.id",FaseMultiton.getFase(FaseMultiton.APROBACION_CAMBIO).id)
						eq ("fase.id",FaseMultiton.getFase(FaseMultiton.VALIDACION_IMPACTO).id)
						eq ("fase.id",FaseMultiton.getFase(FaseMultiton.APROBACION_IMPACTO).id)
						eq ("fase.id",FaseMultiton.getFase(FaseMultiton.APROBACION_PAU_HIJO).id)
						eq ("fase.id",FaseMultiton.getFase(FaseMultiton.APROBACION_IMPLANTACION_HIJO).id)
					}
				}
			}
			if (consultaInbox?.tipoConsulta.equals(Consulta.PENDIENTES_MI_GRUPO)) {
				def grupos = umeService.getGrupos(consultaInbox?.legajo)
				grupos.add("")
				inList("grupo", grupos)
			}
			eq("estado", Aprobacion.PENDIENTE)
			//TODO: borrar los "isNull" ya que al tener la aprobación en estado suspendido
			//isNull("pedido.fechaCancelacion")
			//isNull("pedido.fechaSuspension")
			//
			createAlias ('pedido.faseActual', 'fasePedido')
		}

		params.totalCount = idsPedidos.size()

		log.info "Ids pedidos filtrados por pedidosInbox: " + idsPedidos

		return idsPedidos
	}

	/**
	 * Consulta el inbox de aprobaciones
	 */
	@Transactional(readOnly = true)
	private def inboxAprobaciones(params, ConsultaInbox consultaInbox) {
		//		definirPaginacionInbox(params)
		def idsPedidos = pedidosInbox(params, consultaInbox)

		def aprobaciones = []

		if (!idsPedidos.isEmpty()) {
			aprobaciones = Aprobacion.createCriteria().list() {
				createAlias ('pedido', 'pedido')
				if ([
					Consulta.MIS_PENDIENTES,
					Consulta.INBOX_APROBACIONES
				].contains(consultaInbox?.tipoConsulta)) {
					eq("usuario", consultaInbox?.legajo)
				}
				if (consultaInbox?.tipoConsulta.equals(Consulta.PENDIENTES_MI_GRUPO)) {
					def grupos = umeService.getGrupos(consultaInbox?.legajo)
					grupos.add("")
					inList("grupo", grupos)
				}
				eq("estado", Aprobacion.PENDIENTE)
				inList("pedido.id", idsPedidos)
			}
		}

		def pedidosDeActividades = idsPedidos - aprobaciones.collect { aprobacion -> aprobacion.pedido.id }
		def aprobacionesFaltantes = pedidosDeActividades.collect { idPedido ->
			new Aprobacion(usuario: consultaInbox.legajo, rol: "ACT", estado: Aprobacion.PENDIENTE, fecha: new Date(), pedido: this.obtenerPedidoGenerico(params, idPedido, false))
		}

		aprobaciones.addAll(aprobacionesFaltantes)

		if (consultaInbox.ingresoRol()){
			aprobaciones = aprobaciones.findAll { aprobacion ->
				def rol = aprobacion.pedido.getRol(aprobacion.pedido.faseActual, consultaInbox?.legajo)
				rol?.equals(consultaInbox.rol)
			}
		}

		params.totalCount = aprobaciones.size()
		return aprobaciones
	}

	/**
	 * Consulta el inbox de aprobaciones
	 */
	@Transactional(readOnly = true)
	def inboxEnSeguimiento(params, consultaInbox, listadoMisPendientes) {
		log.info  "Legajo: " + consultaInbox.legajo
		def pedidos = AbstractPedido.createCriteria().list() {
			createAlias('aprobaciones', 'aprobacion', Criteria.LEFT_JOIN)
			//createAlias('pedidoPadre', 'pedidoPadre', Criteria.LEFT_JOIN)
			or{
				eq("legajoUsuarioCreador", consultaInbox.legajo)
				eq("legajoInterlocutorUsuario", consultaInbox.legajo)
				eq("legajoGerenteUsuario", consultaInbox.legajo)
				eq("legajoUsuarioGestionDemanda", consultaInbox.legajo)
				eq("legajoCoordinadorCambio", consultaInbox.legajo)
				eq("legajoUsuarioAprobadorEconomico", consultaInbox.legajo)
				eq("legajoUsuarioResponsable", consultaInbox.legajo)
				eq("aprobacion.usuario", consultaInbox.legajo)
			}
			def padresPendientes = listadoMisPendientes.parent.id.collect { it.toInteger() }
			log.info "Listado ID de pendientes: ${padresPendientes}"
			if (padresPendientes){
				and{
					not{ 'in'('id', padresPendientes) }
					or {
						not{ 'in'('pedidoPadre.id', padresPendientes) }
						isNull('pedidoPadre')
					}
				}
			}
			isNull("fechaCierre")
			isNull("fechaCancelacion")
			createAlias ('faseActual', 'fasePedido')
			def orden = params.sort
			if (orden){
				if (orden.startsWith("pedido.")) {
					orden = orden.substring(7)
				}
				if (!orden.equals("rol"))
					order (orden, params.order)
			}
		}
		params.totalCount = pedidos.size()
		if (params.sort=='rol'){
			if (params.order=='asc')
				pedidos=pedidos.sort { elem -> elem.getRol(elem.faseActual, consultaInbox.legajo)}
			else
				pedidos=pedidos.sort { elem -> elem.getRol(elem.faseActual, consultaInbox.legajo)}.reverse()
		}
		log.info "Inbox en Seguimiento. Los pedidos son: ${pedidos.id}"
		return pedidos
	}

	/**
	 * Consulta de pedidos on-line
	 */
	@Transactional(readOnly = true)
	def consultarPedidosOnline(params) {
		params.max = MAX_PEDIDOS_ONLINE
		params.offset = 0
		consultarPedidosInbox(params)
	}

	/**
	 * 
	 */
	@Transactional(readOnly = true)
	def consultarPedidosInbox(params) {
		def consultaPedido

		if (params.id?.isInteger()) {
			consultaPedido = new ConsultaPedido(id: new Integer(params.id.trim()))
		} else {
			consultaPedido = new ConsultaPedido(titulo: params.id)
		}
		consultarPedidos(params, consultaPedido)
	}

	@Transactional(readOnly = true)
	def consultarPedidos(params, consultaPedido) {
		definirPaginacion(params)
		return Pedido.createCriteria().list(max: params.max, offset: params.offset) {
			createAlias ('faseActual', 'fasePedido')

			if (consultaPedido.ingresoId()) {
				eq("id", consultaPedido.id)
			}
			if (consultaPedido.ingresoTitulo()) {
				ilike("titulo", "%" + params.id + "%")
			}
			//ne("cancelado", true)

			if (params.sort == 'macroEstado'){
				order ('fasePedido.macroEstado', params.order)
			}else{
				order (params.sort, params.order)
			}

		}
	}

	@Transactional(readOnly = true)
	private def definirPaginacion(params) {
		params.sort = params.sort?:'id'
		params.order = params.order?:'desc'
		params.max = Math.min(params.max ? params.int('max') : 10, 100)
		params.offset = params.offset? params.offset.toInteger():0
	}

	@Transactional(readOnly = true)
	private def definirPaginacionInbox(params) {
		params.sort = params.sort?:'id'
		params.order = params.order?:'desc'
		params.max = Math.min(params.max ? params.int('max') : 20, 200)
		params.offset = params.offset? params.offset.toInteger():0
	}

	/**
	 * En caso de tener aprobaciones pendientes para grupos, se actualiza grupo y usuario
	 * @param aprobacionUI
	 * @param usuarioLogueado
	 * @return
	 */
	@Transactional
	def actualizarAprobaciones(aprobacionUI, origen, boolean cambiaResponsable) {
		try {
			if (!origen) {
				return
			}
			def pedido = aprobacionUI.pedido
			if (!pedido.id) {
				return
			}
			if (origen.actualizar(aprobacionUI, cambiaResponsable)) {
				origen.save(flush: true, failOnError: true)
			}
		} catch (Exception e) {
			this.procesarErrorBaseDatos(e)
		}
	}

	/**
	 * Llena la lista de tipos de consulta de pedidos posibles
	 */
	@Transactional(readOnly = true)
	def getTiposConsulta() {
		[
			new TipoConsulta(codigo: Consulta.MIS_PENDIENTES, descripcion: "Mis Pendientes"),
			new TipoConsulta(codigo: Consulta.EN_SEGUIMIENTO_PROPIOS, descripcion: "En Seguimiento / propios"),
			new TipoConsulta(codigo: Consulta.PENDIENTES_MI_GRUPO, descripcion: "Pendientes de mis grupos")
		]
	}

	/**
	 * Llena la lista de tipos de consulta de pedidos posibles
	 */
	@Transactional(readOnly = true)
	def getRolesGrupos(params, legajo) {

		List roles = []

		Aprobacion.createCriteria().list() {
			eq("usuario", legajo)
			eq("estado", Aprobacion.PENDIENTE)
		}.findAll { aprobacion ->
			if (aprobacion.pedido?.esHijo()){
				def rol = aprobacion.pedido.getRol(aprobacion.pedido.faseActual, legajo)
				if (rol){
					obtenerRol(roles, rol)
				}
			}else{
				if (aprobacion.rol){
					obtenerRol(roles, aprobacion.rol)
				}
			}
		}
		return roles
	}

	@Transactional(readOnly = true)
	private def obtenerRol(roles, rol){
		def rolAplicacion = RolAplicacion.findByCodigoRol(rol)
		if (rolAplicacion){
			if(!roles.contains(rolAplicacion)){
				roles.add(rolAplicacion)
			}
		}
	}
	/**
	 * Devuelve la lista de usuarios de un pedido
	 */
	@Transactional(readOnly = true)
	def getUsuariosPedido(pedidoId){
		def pedido = Pedido.get(pedidoId)
		if (!pedido){
			throw new BusinessException("No se encontró el pedido " + pedidoId)
		}
		return pedido.usuarios.collect { usuario -> umeService.getUsuario(usuario) }
	}

	@Transactional(readOnly = true)
	def obtenerGrupoReferente(sistemaId, tipoImpactoId){
		def sistema
		def tipoImpacto

		if (sistemaId) {
			sistema = Sistema.findById(Integer.parseInt(sistemaId))
		}else{
			return null
		}

		tipoImpacto = TipoImpacto.findById(Integer.parseInt(tipoImpactoId))
		return (tipoImpacto.asociadoSistemas)?sistema.grupoReferenteSWF:sistema.grupoAdministrador
	}

	/**
	 * Recibo un pedido hijo o padre
	 * Devuelvo la planificación actual de ese pedido
	 * @param unPedido
	 * @return
	 */
	@Transactional
	def getPlanificacionActual(unPedido) {
		if (!unPedido.mostrarSolapaPlanificacion()) {
			return null
		}
		def planificacionActual = unPedido.getPlanificacionActual()
		if (!planificacionActual) {
			planificacionActual = new PlanificacionEsfuerzo(versionPES: 1, tipoPlanificacion: unPedido.getTipoPlanificacion(), comentarioSistema: "")
			if (unPedido.planificaActividadesSistema()) {
				ActividadPlanificacion.list().sort{ it.id }.each { actividad ->
					planificacionActual.addToDetalles(new DetallePlanificacionSistema(actividadPlanificacion: actividad))
				}
			}
			unPedido.addToPlanificaciones(planificacionActual)
			unPedido.save(flush: true, failOnError: true)
		}
		return planificacionActual
	}

	/**
	 * Recibo un pedido hijo o padre
	 * Devuelvo la estrategia actual de ese pedido
	 * @param unPedido
	 * @return
	 */
	@Transactional
	def getEstrategiaPruebaActual(unPedido) {
		if (!unPedido.mostrarSolapaEstrategiaPrueba()) {
			return null
		}
		def estrategiaPrueba = unPedido.getEstrategiaPruebaActual()
		if (!estrategiaPrueba) {
			estrategiaPrueba = unPedido.definirEstrategiaPrueba(generarMapaTiposPruebaParametrizadas(unPedido))
			unPedido.save(flush: true, failOnError: true)
		}
		return estrategiaPrueba
	}

	/**
	 * Recibo un pedido 
	 * Devuelvo la parametrización de los tipos de prueba
	 * @param unPedido
	 * @return
	 */
	@Transactional(readOnly = true)
	def generarMapaTiposPruebaParametrizadas(pedido) {
		def result = [ : ]
		def pedidoPadre = pedido.parent
		ParametrizacionTipoPrueba.createCriteria().list {
			eq("tipoGestion", pedidoPadre.tipoGestion)
			if (pedido.tipoImpacto()) {
				eq("tipoImpacto", pedido.tipoImpacto())
			} else {
				isNull("tipoImpacto")
			}
		}.each { parametroTipoPrueba -> result.put(parametroTipoPrueba.tipoPrueba,  parametroTipoPrueba) }
		log.debug "Mapa tipos prueba parametrizadas: " + result
		result
	}

	@Transactional(readOnly = true)
	def consultarParametrizacion(tipoGestion, tipoPrueba, tipoImpacto) {
		def lista = ParametrizacionTipoPrueba.createCriteria().list {
			eq("tipoGestion", tipoGestion)
			eq("tipoPrueba", tipoPrueba)
			if (tipoImpacto) {
				eq("tipoImpacto", tipoImpacto)
			} else {
				isNull("tipoImpacto")
			}
		}
		if (!lista.isEmpty()) {
			lista.first()
		} else {
			null
		}
	}

	/**
	 * Recibo un pedido hijo o padre
	 * Devuelvo el dise�o externo actual de ese pedido
	 * @param unPedido
	 * @return
	 */
	@Transactional
	def getDisenioExternoActual(unPedido) {
		if (!unPedido.mostrarSolapaDE()) {
			return null
		}
		def disenioExterno = unPedido.getDisenioExternoActual()
		if (!disenioExterno) {
			disenioExterno = unPedido.definirDisenioExterno()
			unPedido.save(flush: true, failOnError: true)
		}
		return disenioExterno
	}

	@Transactional(readOnly = true)
	def getReleases(sistema) {
		if (sistema) {
			return Release.findAllBySistema(sistema)
		} else {
			return []
		}
	}

	/**
	 * Envía mails pendientes de un pedido
	 * 
	 * TODO: Abrir un nuevo thread para que si de error no mate el flujo transaccional
	 * TODO 2: After send mail - revisar 
	 */
	@Transactional
	def enviaMailsPendientes(AbstractPedido unPedido) {
		if (!unPedido.tieneAprobacionOmitida()) {
			def parametro = ParametrosSistema.list().first()
			def mailsPendientes = unPedido.allMailsPendientes

			//log.info "enviaMailsPendientes: mailsPendientes " + mailsPendientes
			log.info "Mails pendientes del pedido: " + mailsPendientes
			mailsPendientes.each { mailPendiente ->
				if (parametro.enviaMails && (Environment.current == Environment.TEST || Environment.current == Environment.PRODUCTION)) {
					// http://grails.org/plugin/mail
					try {
						//log.info "Mail a: " + mailPendiente.to
						mailService.sendMail {
							if (mailPendiente.to.isEmpty()) {
								to parametro.mailAdministradorFuncional
							} else {
								to mailPendiente.to.toArray()  // Por ahora
							}
							from mailPendiente.from
							subject mailPendiente.subject
							html mailPendiente.mail
						}
					} catch (Exception e) {
						log.fatal e
						e.printStackTrace()
					}
				} else {
					log.info "Enviando mail a " + mailPendiente.to + " (" + mailPendiente.subject + ")"
					log.info mailPendiente.mail
					log.info "************************************************************"
				}
				//log.info "Audita mails? " + parametro.auditaMails
				if (parametro.auditaMails) {
					def mailAudit = new MailAudit(asunto: mailPendiente.subject, texto: mailPendiente.mail, mails: StringUtil.hasta(mailPendiente.to.toString(), 249))
					mailAudit.save(flush: true, failOnError: true)
				}
			}
		} else {
			log.info "enviaMailsPendientes: No hay mails para enviar porque hay aprobacion omitida. Fase pedido: " + unPedido.faseActual
		}
		unPedido.eliminarAllMailsPendientes()
		//log.info "Mails pendientes del pedido ahora: " + unPedido.mailsPendientes
	}

	@Transactional(readOnly = true)
	def planificacionConsolidada(Pedido pedidoPadre) {
		def consolidacionList = Pedido.createCriteria().list {
			eq("id", pedidoPadre.id)
		}
		log.debug "Planificación consolidada: " + consolidacionList
	}


	@Transactional(readOnly = true)
	def obtenerDetallesAgrupadosPorTipoDeImpacto(pedido){
		def pedidos = []

		def totalHorasCoordinacion = NumberUtil.orZero(pedido.planificacionActual?.totalHoras())
		if (totalHorasCoordinacion > 0) {
			def totalMontoHorasCoordinacion = NumberUtil.orZero(pedido.planificacionActual?.totalMontoGeneral())
			def detallesPedidoPadre = [descripcion: "Horas de coordinación", totalHoras: totalHorasCoordinacion, totalMontoHoras: totalMontoHorasCoordinacion ]
			pedidos << [codigoAgrupador: TipoImpacto.AREAS_IT , totalHoras: totalHorasCoordinacion, totalMontoHoras: totalMontoHorasCoordinacion, detalles: [detallesPedidoPadre]]
		}

		pedido?.pedidosHijos?.each {

			def pedidosPorTipo = pedidos.find {p -> p.codigoAgrupador ==  it.tipoImpacto.codigoAgrupador}
			if(!pedidosPorTipo){
				pedidosPorTipo = [codigoAgrupador: it.tipoImpacto.codigoAgrupador,totalHoras: 0, totalMontoHoras: 0, detalles: []]
				pedidos << pedidosPorTipo
			}
			def totalHoras = it.planificacionActual?.totalHoras()
			if (!totalHoras) {
				totalHoras = 0
			}
			def totalMontoHoras = it.planificacionActual?.totalMontoGeneral()
			if (!totalMontoHoras) {
				totalMontoHoras = 0
			}
			pedidosPorTipo.totalHoras += totalHoras
			pedidosPorTipo.totalMontoHoras += totalMontoHoras
			pedidosPorTipo.detalles += [descripcion: it.toString(), totalHoras: totalHoras, totalMontoHoras: totalMontoHoras ]
		}

		pedidos
	}

	@Transactional(readOnly = true)
	def planificacionConsolidada(Pedido pedidoPadre, Impacto impacto) {
		def planificaciones = pedidoPadre.planificacionesActualesConsolidadas()
		
		if (planificaciones.isEmpty() || !impacto.esConsolidado()) {
			return []
		} else {
			return getPlanificacionesCumplidas(planificaciones)
		}
	}
	
	@Transactional(readOnly = true)
	def planificacionConsolidadaReplanificacion(Pedido pedidoPadre, Impacto impacto) {
		if (!pedidoPadre){
			return []
		}
		def planificaciones = pedidoPadre.planificacionesActualesConsolidadasEnReplanificacion()
		if (planificaciones.isEmpty() || !impacto.esConsolidado()) {
			return []
		} else {
			return getPlanificacionesCumplidas(planificaciones)
		}
	}
	
	@Transactional(readOnly = true)
	def planificacionConsolidadaIncurrida(Pedido pedidoPadre, Impacto impacto, grupo) {
		def planificaciones = pedidoPadre.planificacionesActualesConsolidadas()

		def consol
		if(grupo == 'null'){
			consol = getPlanificacionesCumplidas(planificaciones)
		}else{
			consol = AbstractPedido.createCriteria().list {
				createAlias ('planificaciones', 'planificaciones')
				createAlias ('planificaciones.detalles', 'detalles')
				createAlias ('planificaciones.detalles.cargaHoras', 'cargaHoras', CriteriaSpecification.LEFT_JOIN)
				createAlias ('planificaciones.detalles.cargaHorasIncurridas', 'cargaHorasIncurridas', CriteriaSpecification.LEFT_JOIN)
				createAlias ('planificaciones.detalles.cargaHorasIncurridas.softwareFactory', 'softwareFactory', CriteriaSpecification.LEFT_JOIN)
				projections {
					// agrupamos por actividad
					groupProperty('detalles.actividadPlanificacion')
					min "detalles.fechaDesde"
					max "detalles.fechaHasta"
					sum "cargaHoras.cantidadHoras"
					// GROSO ARTICULO http://stackoverflow.com/questions/7643046/grails-projection-on-arithmetic-expression-with-executequery
					sum "cargaHoras.totalMontoHoras"
					sum "cargaHorasIncurridas.cantidadHoras"
				}
				inList("planificaciones.id", planificaciones)
				isNotNull("detalles.actividadPlanificacion")
				eq("planificaciones.estado", PlanificacionEsfuerzo.ESTADO_CUMPLIDO)
				eq("softwareFactory.grupoLDAP", grupo)
			}
		}

		return consol

	}

	private def getPlanificacionesCumplidas(planificaciones){
		return AbstractPedido.createCriteria().list {
			createAlias ('planificaciones', 'planificaciones')
			createAlias ('planificaciones.detalles', 'detalles')
			createAlias ('planificaciones.detalles.cargaHoras', 'cargaHoras', CriteriaSpecification.LEFT_JOIN)
			createAlias ('planificaciones.detalles.cargaHorasIncurridas', 'cargaHorasIncurridas', CriteriaSpecification.LEFT_JOIN)
			projections {
				// agrupamos por actividad
				groupProperty('detalles.actividadPlanificacion')
				min "detalles.fechaDesde"
				max "detalles.fechaHasta"
				sum "cargaHoras.cantidadHoras"
				// GROSO ARTICULO http://stackoverflow.com/questions/7643046/grails-projection-on-arithmetic-expression-with-executequery
				sum "cargaHoras.totalMontoHoras"
				sum "cargaHorasIncurridas.cantidadHoras"
			}
			inList("planificaciones.id", planificaciones)
			isNotNull("detalles.actividadPlanificacion")
			eq("planificaciones.estado", PlanificacionEsfuerzo.ESTADO_CUMPLIDO)
		}
	}
	
	@Transactional
	@Deprecated
	def actualizarECS(ECS ecs, ECSOtrasPruebas otraPrueba) {
		otraPrueba.delete(failOnError: true)
	}

	@Transactional
	private def verificarAsignatario(params, aprobacionUI) {
		def pedido = aprobacionUI.pedido
		def origenId  = params.actividadId?: params.aprobacionId
		if (!origenId || origenId.toString().equalsIgnoreCase("null")) {
			throw new ConcurrentAccessException("Al intentar grabar el pedido hubo otro usuario que lo modificó. Por favor ingrese nuevamente.")
		}
		def origen = bloquearAprobacion(origenId, params.actividadId != null, aprobacionUI, false)
		actualizarAprobaciones(aprobacionUI, origen, false)
	}


	/**
	 * Elimina un detalle de planificación
	 * @param detalle
	 * @return
	 */
	@Transactional
	boolean eliminarDetalle(params, aprobacionUI, detalle) {
		verificarAsignatario(params, aprobacionUI)
		detalle.delete(flush: true, failOnError: true)
	}

	/**
	 * Actualiza un detalle de planificación bloqueando aprobacion por concurrencia 
	 * @param detalle
	 * @return
	 */
	@Transactional
	boolean actualizarDetalle(params, aprobacionUI, detalle) {
		verificarAsignatario(params, aprobacionUI)
		detalle.save(flush:true, failOnError: true)
	}

	/**
	 * Actualiza un detalle de planificación
	 * @param detalle
	 * @return
	 */
	@Transactional
	boolean actualizarDetalle(detalle) {
		detalle.save(flush:true, failOnError: true)
	}


	/**
	 * Elimina una estrategia de prueba
	 * @param detalle
	 * @return
	 */

	@Transactional
	boolean eliminarEstrategia(params, aprobacionUI, estrategia) {
		verificarAsignatario(params, aprobacionUI)
		estrategia.delete(flush: true, failOnError: true)
	}

	/**
	 * Actualiza una estrategia de prueba
	 * @param detalle
	 * @return
	 */
	@Transactional
	boolean actualizarEstrategia(params, aprobacionUI, estrategia) {
		verificarAsignatario(params, aprobacionUI)
		estrategia.save(flush:true, failOnError: true)
	}

	/**
	 * Elimina una carga de hora
	 * @param id carga hora
	 * @return
	 */
	@Transactional
	boolean eliminarCargaHora(params, aprobacionUI, cargaHora) {
		verificarAsignatario(params, aprobacionUI)
		cargaHora.delete(flush: true, failOnError: true)
	}

	/**
	 * Elimina otro costo
	 * @param detalle
	 * @return
	 */
	@Transactional
	boolean eliminarOtroCosto(params, aprobacionUI, otroCosto) {
		verificarAsignatario(params, aprobacionUI)
		otroCosto.delete(flush: true, failOnError: true)
	}


	/**
	 * Obtiene un detalle de planificación
	 * @param id de detalle
	 * @return
	 */
	@Transactional(readOnly = true)
	def obtenerDetalle(id) {
		def detalle
		if (id && id != 'null') {
			def idDetalle = id as int
			detalle = DetallePlanificacion.get(id)
			if (detalle) {
				//detalle.traerDatosRelacionados()
				detalle.id
			} else {
				throw new BusinessException("No se encontró el detalle de planificación " + id)
			}

		} else {
			return null
		}
		return detalle
	}

	@Transactional(readOnly = true)
	def allAprobadoresImpacto(pedido) {
		if (!pedido) {
			return
		}
		def indiceDefault = Math.max(1, umeService.indiceAprobadorDefault(pedido))
		def aprobadoresPosibles = umeService.aprobadoresDefault(pedido)
		//UME 1.5
		def usuarioAprobadorDefault = aprobadoresPosibles.get(indiceDefault)
		if (!usuarioAprobadorDefault) {
			return []
		}
		def aprobadores = []
		def aprobadoresImpacto = []
		
		if (usuarioAprobadorDefault.isDirector()) {
			aprobadores = umeService.getGerentesPorDireccion(usuarioAprobadorDefault.direccionCodigo)
			aprobadoresImpacto = umeService.filtrarAprobadoresImpacto(aprobadores, 40, 60)  // TODO: Parametrizar
		} else {
			aprobadores = umeService.getJerarquiaUsuario(usuarioAprobadorDefault.legajo)
			aprobadoresImpacto = umeService.filtrarAprobadoresImpacto(aprobadores)
		}
		
		return aprobadoresImpacto
		
		//UME 1.0
		/*def legajoAprobadorDefault = aprobadoresPosibles.get(indiceDefault)
		if (!legajoAprobadorDefault) {
			return []
		}
		return umeService.filtrarAprobadoresImpacto(umeService.getGerentesDireccion(legajoAprobadorDefault?.legajo))
		*/
	}

	@Transactional(readOnly = true)
	def aprobadorImpactoDefault(pedido) {
		def aprobadoresPosibles = umeService.aprobadoresDefault(pedido)
		if (aprobadoresPosibles.isEmpty()) {
			return null
		}
		
		return aprobadoresPosibles.get(umeService.indiceAprobadorDefault(pedido))
	}


	@Transactional
	boolean eliminarOtraPruebaECS(ecs, otraPrueba) {
		ecs.removeFromOtrasPruebasECS(otraPrueba)
		otraPrueba.delete(flush: true, failOnError: true)
	}

	@Transactional
	boolean eliminarSistemaECS(pedido, ecs, sistema) {
		ecs.eliminarSistemaImpactado(sistema)
		ecs.save(flush: true, failOnError: true)
	}

	@Transactional
	boolean eliminarAreaSoporteECS(ecs, areaSoporte) {
		ecs.eliminarAreaImpactada(areaSoporte)
		ecs.save(flush: true, failOnError: true)
	}

	@Transactional(readOnly = true)
	def inboxAprobaciones(pedidoInstanceList, params) {
		def usuarioLogueado = umeService.getUsuarioLogueado()
		def listadoPedidos
		if (!params.max) {
			params.max = MAX_INBOX_APROBACIONES
		}
		if (pedidoInstanceList.isEmpty()) {
			params.totalCount = 0
			listadoPedidos = []
		} else {
			params.totalCount = pedidoInstanceList.size()
			listadoPedidos = Aprobacion.createCriteria().list(max: params.max, offset: params.offset) {
				inList("id", pedidoInstanceList.id)
				createAlias ('pedido', 'pedido')
				if (!params.sort) {
					params.sort = "pedido.titulo"
				}
				if (!params.order) {
					params.order = "asc"
				}
				order (params.sort, params.order)
			}
		}
		[aprobacionInstanceList: listadoPedidos, aprobacionInstanceTotal: params.totalCount, usuarioLogueado:usuarioLogueado]
	}

	@Transactional(readOnly = true)
	def obtenerPedidos(params, usuarioLogueado) {
		params.tipoConsulta = Consulta.INBOX_APROBACIONES
		return pedidoService.consultarInboxAprobaciones(params, usuarioLogueado)
	}

	@Transactional(readOnly = true)
	def getActividad(pIdActividad) {
		def idActividad = pIdActividad as int
		if (!idActividad) {
			return null
		}
		def actividad = Actividad.findById(idActividad)
		actividad.traerDatosRelacionados()
		agregarObservers(actividad.pedido, false)
		//actividad.discard()
		return actividad
	}

	@Transactional(readOnly = true)
	def procesarErrorBaseDatos(Throwable e) {
		if (e.class.equals(StaleObjectStateException.class) || e.cause?.class.equals(StaleObjectStateException.class)) {
			throw new ConcurrentAccessException("Al intentar grabar el pedido hubo otro usuario que lo modificó. Por favor ingrese nuevamente.")
		} else {
			throw e
		}
	}

	@Transactional(readOnly = true)
	def obtenerParametrizacionTipoPrueba(idTipoGestion, tipoImpacto) {
		def tipoGestion = TipoGestion.findById(idTipoGestion)

		if (tipoImpacto) {
			return ParametrizacionTipoPrueba.findAllByTipoGestionAndTipoImpacto(tipoGestion, tipoImpacto)
		} else {
			return ParametrizacionTipoPrueba.findAllByTipoGestion(tipoGestion)
		}
	}

	@Transactional(readOnly = true)
	@Deprecated
	def chequearVersion(pedido, params) {
		def pedidoVersion
		if (pedido.esHijo()) {
			pedidoVersion = params.pedidoHijoVersion
		} else {
			pedidoVersion = params.pedidoVersion
		}
		if (pedidoVersion && pedidoVersion.toInteger() != pedido.version) {
			throw new BusinessException("En el momento en que ud.ingresó al pedido " +  pedido.id + " otro usuario lo actualizó. <ul><li>Versión actual: " + pedido.version + "</li><li>Versión que usted ten\u00EDa: " + pedidoVersion + "</li></ul><br><br>Por favor reintente. " )
		}
	}

	@Transactional
	def bloquearAprobacion(origenId, boolean origenActividad, aprobacionUI, boolean cambiaResponsable) {
		def origen

		try {
			if (origenActividad) {
				origen = Actividad.findById(origenId, [lock: true])
			} else {
				origen = Aprobacion.findById(origenId, [lock: true])
			}
		} catch (StaleObjectStateException e){
			throw new ConcurrentAccessException("Otro usuario tiene tomada la actividad sobre el pedido")
		}

		if (!origen) {
			return null
		}

		def nombreUsuario = ""
		if (origen.usuario) {
			nombreUsuario = "El usuario " + umeService.getUsuario(origen.usuario).nombreApellido + " ( " + origen.usuario + " )"
		} else {
			nombreUsuario = "El grupo " + origen.grupo
		}
		def usuarioLogueado = umeService.usuarioLogueado
		if (!origen.estaEnCurso()) {
			if (usuarioLogueado.equalsIgnoreCase(origen.usuario)) {
				nombreUsuario = "Otro usuario"
			}
			throw new ConcurrentAccessException(nombreUsuario + " ha finalizado la actividad sobre el pedido")
		}
		if (!cambiaResponsable && origen.usuario && !usuarioLogueado.equalsIgnoreCase(origen.usuario)) {
			throw new ConcurrentAccessException(nombreUsuario +  " tiene tomada la actividad sobre el pedido")
		}
		// TODO: A futuro este chequeo no tiene sentido
		// Así como está esta validación impide que el mismo usuario en dos máquinas diferentes actualice la aprobación o actividad
		// de lo contrario no habría inconveniente en tener una v
		//if (origenVersion && origen.version != origenVersion) {
		//	throw new ConcurrentAccessException("Otro usuario modificó información del pedido")
		//}
		return origen
	}

	@Transactional
	def bloquearActividad(actividad, boolean cambiaResponsable) {
		def origen

		try {
			origen = Actividad.findById(actividad?.id, [lock: true])
		} catch (StaleObjectStateException e){
			throw new ConcurrentAccessException("Otro usuario tiene tomada la actividad sobre el pedido")
		}

		if (!origen) {
			return null
		}

		def nombreUsuario = ""
		if (origen.usuario) {
			nombreUsuario = "El usuario " + umeService.getUsuario(origen.usuario).nombreApellido + " ( " + origen.usuario + " )"
		} else {
			nombreUsuario = "El grupo " + origen.grupo
		}
		def usuarioLogueado = umeService.usuarioLogueado
		if (!origen.estaPendiente()) {
			throw new ConcurrentAccessException(nombreUsuario + " ha finalizado la actividad sobre el pedido")
		}
		if (!cambiaResponsable && origen.usuario && !origen.usuario.equalsIgnoreCase(usuarioLogueado)) {
			throw new ConcurrentAccessException(nombreUsuario +  " tiene tomada la actividad sobre el pedido")
		}
		// TODO: A futuro este chequeo no tiene sentido
		// Así como está esta validación impide que el mismo usuario en dos máquinas diferentes actualice la aprobación o actividad
		// de lo contrario no habría inconveniente en tener una v
		//if (origenVersion && origen.version != origenVersion) {
		//	throw new ConcurrentAccessException("Otro usuario modificó información del pedido")
		//}
		return origen
	}

	/**
	 * Finaliza una actividad 
	 * considera accesos concurrentes a la actividad para poder cerrarla
	 * @param aprobacionUI
	 * @param inicializaObservers true/false
	 * @return si se pudo guardar satisfactoriamente
	 */
	@Transactional
	boolean finalizarActividad(actividad) {
		try {
			def pedidoHijo = actividad.pedido
			def aprobacionUI = new AprobacionUI(pedido: pedidoHijo)
			def origen = bloquearActividad(actividad, false)
			actualizarAprobaciones(aprobacionUI, origen, false)

			def usuarioLogueado = umeService.usuarioLogueado
			def result = actividad.finalizar(usuarioLogueado)
			if (result) {
				result = doGuardar(pedidoHijo)
			}
			if (result){
				enviaMailsPendientes(pedidoHijo)
			}
			return result
		} catch (Exception e) {
			this.procesarErrorBaseDatos(e)
		}
	}
	
	@Transactional(readOnly = true)
	def obtenerPedidosReporteAprobacionImpacto(fecha){
		Calendar fechaMinima = Calendar.instance
		Calendar fechaMaxima = Calendar.instance
		
		fechaMinima.set(fecha.get(Calendar.YEAR), fecha.get(Calendar.MONTH), 1, 0, 0, 0)
		fechaMaxima.set(fecha.get(Calendar.YEAR), fecha.get(Calendar.MONTH), fecha.getActualMaximum(fecha.DATE), 23, 59, 59)

		log.info "fechaMinima.time: ${fechaMinima.time}"
		log.info "fechaMaxima.time: ${fechaMaxima.time}"
		
		return AbstractPedido.createCriteria().list() {
			
			createAlias ("tipoGestion", "tipoGestion", CriteriaSpecification.LEFT_JOIN)
			
			or {
				and {
					eq("tipoGestion.descripcion", TipoGestion.GESTION_ESTANDAR)
					between("fechaAprobacionEconomica", fechaMinima.time, fechaMaxima.time)
					inList("faseActual", fasesReporteAprobacion())
				}
				and {
					eq("tipoGestion.descripcion", TipoGestion.GESTION_EMERGENCIA)
					between("fechaValidacionImpacto",fechaMinima.time, fechaMaxima.time)
					inList("faseActual", fasesReporteAprobacion())
				}
			}
			
			/*and {
				isNull("fechaCancelacion")
				isNull("fechaSuspension")
			}*/
		}
	}

	@Transactional(readOnly = true)
	def fasesReporteAprobacion(){
		return [FaseMultiton.getFase(FaseMultiton.CONSTRUCCION),FaseMultiton.getFase(FaseMultiton.FINALIZACION)]
	}
	
	@Transactional
	def actualizarReporteAprobacionEnviados(direccion){
		def reporteAprobacionEnviado = ReporteAprobacionEnviados.findByCodigoDireccion(direccion)
		if (!reporteAprobacionEnviado){
			reporteAprobacionEnviado = new ReporteAprobacionEnviados(codigoDireccion: direccion)
		}
		reporteAprobacionEnviado.fecha = new Date()
		reporteAprobacionEnviado.save(flush: true, failOnError: true)
	}
	
	@Transactional(readOnly = true)
	def obtenerDireccionSinPedidos(fecha){
		Calendar fechaAprobacion = Calendar.instance
		fechaAprobacion.set(fecha.get(Calendar.YEAR), fecha.get(Calendar.MONTH), 1, 0, 0, 0)
		return ReporteAprobacionEnviados.createCriteria().list() {
			lt("fecha",fechaAprobacion.time)
		}
	}
	
	def validarEstadoPedido(origen) {
		if (origen){
			if (origen.estaSuspendido()) {
				throw new ConcurrentAccessException("El pedido se encuentra suspendido")
			}
			if (origen.estaCancelado()) {
				throw new ConcurrentAccessException("El pedido se encuentra cancelado")
			}
		}
	}
	
		@Transactional(readOnly = true)
	def getUsuariosGrupoLDAP(String grupoLDAP){
		return getUsuariosGrupoLDAP(grupoLDAP, null)
	}

}
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                 