package ar.com.telecom.pcsPlus

import org.hibernate.criterion.CriteriaSpecification
import org.springframework.transaction.annotation.Transactional

import ar.com.telecom.pcs.entities.AnexoPorTipo
import ar.com.telecom.pcs.entities.RolAplicacion
import ar.com.telecom.pedido.AprobacionUI
import ar.com.telecom.util.DateUtil

class AnexoService {
	def pedidoService
	def umeService

	static transactional = false

//	@Transactional(readOnly=true)
//	@Deprecated
//	/**
//	 * @deprecated Use verAnexo instead
//	 */
//	def viewFile(fileName, extension, pedido, response, controller) {
//		AnexoPorTipo anexo = pedido.anexos.find { anexo -> anexo.nombreArchivo.equalsIgnoreCase(fileName) && anexo.extension.equals(extension) }
//		// TODO: Encodearlo
//		if (anexo) {
//			response.setHeader("Content-Disposition","inline; filename=" + anexo.nombreArchivo + anexo.extension)
//			response.setContentType(anexo.fileContentType)
//			response.setContentLength(anexo.archivo?.size())
//			OutputStream out = response.getOutputStream()
//			out.write(anexo.archivo)
//			out.close();
//		}
//	}

	@Transactional(readOnly=true)
	def verAnexo(anexoId) {
		return AnexoPorTipo.findById(anexoId)
	}

//	@Transactional
//	def eliminarAnexo(params, pedido, anexoId, entidadAnexo) {
//	
//		def aprobacionUI = new AprobacionUI(pedido: pedido, usuario: umeService.usuarioLogueado, asignatario: umeService.usuarioLogueado, aprueba: true)
//		def origenId  = params.actividadId?: params.aprobacionId
//		def origen = pedidoService.bloquearAprobacion(origenId, params.actividadId != null, aprobacionUI, false)
//		pedidoService.actualizarAprobaciones(aprobacionUI, origen, false)
//	
//		def anexo = AnexoPorTipo.findById(anexoId)
//		if (anexo) {
//			entidadAnexo.eliminarAnexo(anexo, umeService.usuarioLogueado)
//			entidadAnexo.save(flush: true, failOnError: true)
//		}
//	}

	@Transactional
	def eliminarAnexo(params, pedido, anexoId) {
	
		def entidadAnexo = pedido.getOrigen(anexoId)
		 
		def aprobacionUI = new AprobacionUI(pedido: pedido, usuario: umeService.usuarioLogueado, asignatario: umeService.usuarioLogueado, aprueba: true)
		def origenId  = params.actividadId?:params.aprobacionId
		def origen = pedidoService.bloquearAprobacion(origenId, params.actividadId != null, aprobacionUI, false)
		pedidoService.actualizarAprobaciones(aprobacionUI, origen, false)
	
		def anexo = AnexoPorTipo.findById(anexoId)
		if (anexo && entidadAnexo) {
			entidadAnexo.eliminarAnexo(anexo, umeService.usuarioLogueado)
			entidadAnexo.save(flush: true, failOnError: true)
		}
	}
	
	@Transactional(readOnly = true)
	private def definirPaginacion(params) {
		params.sort = params.sort?:'fecha'
		params.order = params.order?:'asc'
		params.max = Math.min(params.max ? params.int('max') : 10, 100)
		params.offset = params.offset? params.offset.toInteger():0
	}

	@Transactional(readOnly = true)
	def obtenerAnexosPaginado(params, pedido){
		definirPaginacion(params)
		def listAnexosID = obtenerAnexosID(pedido)
		
		if (!listAnexosID.isEmpty()){
			def listaAnexos = AnexoPorTipo.createCriteria().list(max: params.max, offset: params.offset) {
				// Alias tipo de anexo
				createAlias ('tipoAnexo', 'tipoAnexo')
				// Alias fase
				createAlias ('tipoAnexo.fase', 'fase', CriteriaSpecification.LEFT_JOIN)
				
				'in'("id", listAnexosID)
				// Filtro tipo de anexo
				if (params.get("multiselect_comboboxTiposAnexos")) {
					'in' ("tipoAnexo.id",  params?.multiselect_comboboxTiposAnexos.collect{value -> Long.parseLong(value)} )
				}
				// Filtro por fase
				if (params.get("multiselect_comboboxFase")){
					'in'  ("tipoAnexo.fase.id", params?.multiselect_comboboxFase.collect{value -> Long.parseLong(value)})
				}
	
				if (params.descripcion){
					like("descripcion","%$params.descripcion%")
				}
	
				if (params.fechaDesde && params.fechaHasta){
					params.fechaDesde = DateUtil.toDate(params.fechaDesde)
					params.fechaHasta = DateUtil.toDate(params.fechaHasta)
					Calendar cal = Calendar.getInstance();
					cal.setTime(params.fechaHasta)
					cal.add(Calendar.DAY_OF_YEAR, 1)
					between("fecha", params.fechaDesde, cal.getTime())
				}else{
					params.fechaDesde = null
					params.fechaHasta = null
				}
				if (params.legajoUsuario){
					eq("legajo", params.legajoUsuario)
				}
				if (params.get("multiselect_comboboxRolAplicacion")){
					List listLegajos = []
					params?.multiselect_comboboxRolAplicacion.collect{value ->
						def rol = RolAplicacion.findById(value.toString())
						if (rol){
							def listado = getLegajos(pedido, listAnexosID, rol.codigoRol)
							listado.collect{ listLegajos << it	}
						}
					}
					
					if (!listLegajos.isEmpty()) {
						'in'("legajo", listLegajos)
					} else {
						'in'("legajo", "")
					}
				}
				
				if (params.sort != 'rol' &&  params.sort != 'descripcion' &&  params.sort != 'sistema'){
					order (params.sort, params.order)
				}
			}
			
			if (params.sort=='rol'){
				listaAnexos.sort { elem -> pedido.getRoles(elem?.legajo) }
				if (params.order=='asc')
					listaAnexos = listaAnexos.reverse()
	
			}else if (params.sort=='descripcion'){
				if (params.order=='asc')
					listaAnexos = listaAnexos.sort { elem -> elem.descripcion}
				else
					listaAnexos = listaAnexos.sort { elem -> elem.descripcion}.reverse()
			}
			return listaAnexos
		}else{
			params.fechaDesde = null
			params.fechaHasta = null
			return null
		}
	}

	def getLegajos(pedido, listAnexosID, rol){
		def listLegajos = []
		def anexos = []
		
		anexos = AnexoPorTipo.createCriteria().list() {	'in'("id", listAnexosID) }
		
		anexos?.legajo.each { elem ->
			pedido.getRoles(elem).each { elem2 ->
				if (elem2.equals(rol) && !listLegajos.contains(elem2)) listLegajos.add(elem)
			}
		}
		
		return listLegajos
	}
	
	def obtenerAnexosID(pedido){
		def listIdAnexos = pedido?.anexos?.id
		pedido?.especificaciones?.each{
			it?.anexosPorTipo.each{
				listIdAnexos.add(it.id)
			}
		}
		pedido?.disenioExternoActual?.anexos.each{
			listIdAnexos.add(it.id)
		}
		pedido?.estrategiaPruebaActual?.anexos.each{
			listIdAnexos.add(it.id)
		}
		//agrego los del hijo
		def pedidoHijo = pedido?.pedidosHijos
		pedidoHijo?.disenioExternoActual?.anexos.each{
			anexo ->
			anexo.each{
				listIdAnexos.add(it.id)
			}
		}
		pedidoHijo?.estrategiaPruebaActual?.anexos.each{
			anexo ->
			anexo.each{
				listIdAnexos.add(it.id)
			}
		}
		
		pedidoHijo?.actividades.each{
			actividad ->
			actividad?.anexos.each{
				anexo ->
				anexo.each{
					listIdAnexos.add(it.id)
				}
			}
		}
		return listIdAnexos
	}
	
//	@Transactional
//	@Deprecated
//	/**
//	 * @deprecated Use eliminarAnexo instead 
//	 */
//	def deleteAnexo(fileName, extension, entidadAnexo){
//		entidadAnexo.eliminarAnexo(fileName, extension)
//		entidadAnexo.save(flush: true, failOnError: true)
//	}

	@Transactional(readOnly=true)
	@Deprecated
	/**
	 * @deprecated Use verAnexo instead
	 */
	def getFileName(fileName, extension, pedido, controller) {
		def listAnexosPorTipo = getList(fileName, extension, pedido, controller)
		if (!listAnexosPorTipo.isEmpty()) {
			return fileName + "_" + (listAnexosPorTipo.size()+1)
		} else {
			return fileName
		}
	}

	@Transactional(readOnly=true)
	@Deprecated
	/**
	 * @deprecated Use verAnexo instead
	 */
	def getList(fileName, extension, pedido, controller){
		def listAnexosPorTipo
		if (pedido.faseActual.equals(pedido.faseEspecificacionCambio()) || controller.equals(pedido.faseEspecificacionCambio().controllerNombre)) {
			listAnexosPorTipo = pedido?.obtenerUltimaEspecificacion()?.anexosPorTipo.findAll{ anexo -> anexo?.nombreArchivo.contains(fileName) && anexo?.extension.equals(extension)}
		}else{
			listAnexosPorTipo = pedido?.anexos.findAll{ anexo -> anexo?.nombreArchivo.contains(fileName) && anexo?.extension.equals(extension)}
		}
		return listAnexosPorTipo
	}

}
