package ar.com.telecom.pcsPlus

import org.hibernate.criterion.CriteriaSpecification
import org.springframework.transaction.annotation.Transactional

import ar.com.telecom.FaseMultiton
import ar.com.telecom.pcs.entities.AbstractPedido
import ar.com.telecom.pcs.entities.Pedido
import ar.com.telecom.pcs.entities.Sistema
import ar.com.telecom.pcs.entities.Variante

class BusquedaService {

    static transactional = false

	@Transactional(readOnly = true)
	def obtenerSistemas(word, pedidoId){
		def listadoSistemas = Sistema.createCriteria().list(max: 5){
			ilike("descripcion", "%" + word + "%")
			isNull("fechaBajaSimplit")
		}
		return listadoSistemas
	}
	
	@Transactional
	def guardarVariante(Variante variante){
		variante.save(flush: true, failOnError: true)
	}
	
	@Transactional
	def eliminarVariante(varianteId){
		def variante = Variante.findById(varianteId)
		variante.delete(flush: true, failOnError: true)
	}
	
	@Transactional(readOnly = true)
	def obtenerVariantes(legajo){
		return Variante.findAllByLegajoAutorVariante(legajo)
	}

	@Transactional(readOnly = true)
	def obtenerReferenciasOrigen(word,pedidoId){
		def listado =  Pedido.createCriteria().list(max: 5){
			if (word.isNumber()){
				eq ("id", word.toInteger())
			}else{
				ilike("titulo", "%" + word + "%")
			}
			
			if (pedidoId){
				and{
					not {
						eq ('id', pedidoId.toInteger())
					}
				}
			}
		}

		return listado.collect { pedido -> [id: pedido.id, descripcion: pedido.numero + " - " + pedido.titulo] }
	}
		
	@Transactional(readOnly = true)
	def busquedaAvanzadaPedidos(Variante variante, params) {
		definirPaginacionInbox(params)
		
		def pedidosList = AbstractPedido.createCriteria().list() {
			
			createAlias ("pedidosHijos", "pedidoHijo", CriteriaSpecification.LEFT_JOIN)
			//createAlias ("especificaciones.sistemasImpactados.sistema.descripcion", "sistemaImpactado", CriteriaSpecification.LEFT_JOIN)

			log.info "titulo: " + variante.titulo
			// Filtro Titulo
			if (variante.titulo){
				ilike("titulo","%" + variante.titulo + "%")
			}

			// Filtro Tipo Gestion
			if (variante.tipoGestion) {
				eq ("tipoGestion", variante.tipoGestion)
			}

			createAlias ("especificaciones", "ECS", CriteriaSpecification.LEFT_JOIN)
			createAlias ("ECS.sistemasImpactados", "sistemasImpactados", CriteriaSpecification.LEFT_JOIN)
			createAlias ("ECS.sistemasImpactados.sistema", "sistemaImpactado", CriteriaSpecification.LEFT_JOIN)
			if (variante.criterioSistemaImpactado) {
				if (variante.criterioSistemaImpactado.equalsIgnoreCase(Variante.CRITERIO_ES_IGUAL)) {
					eq ("sistemaImpactado.descripcion", variante.valorSistemaImpactado)
				} else if (variante.criterioSistemaImpactado.equalsIgnoreCase(Variante.CRITERIO_CONTIENE)) {
					ilike ("sistemaImpactado.descripcion", "%$variante.valorSistemaImpactado%")
				}
			}
			// Filtro Tipo de Impacto
			if (variante.tiposImpacto) {
				inList ("sistemasImpactados.tipoImpacto", variante.tiposImpacto)
			}

			
			// Filtro Areas de Soporte
			if (variante.areasSoporte) {
				createAlias ("ECS.areasImpactadas", "areasImp", CriteriaSpecification.LEFT_JOIN)
				def listadoAreasSoporte
				if (params.multiselect_comboboxAreaSoporte){
					listadoAreasSoporte = params.multiselect_comboboxAreaSoporte.class.isArray() ? params.multiselect_comboboxAreaSoporte as List : [params.multiselect_comboboxAreaSoporte]
					listadoAreasSoporte = listadoAreasSoporte.collect{ ai -> ai.toLong()}
				}else{
					listadoAreasSoporte = variante.areasSoporte.id
				}
				inList ("areasImp.id", listadoAreasSoporte)
			}

			// Filtro MacroEstado
			createAlias ('faseActual', 'fasePedido', CriteriaSpecification.LEFT_JOIN)
			if (variante.macroestados) {
				inList ("fasePedido.macroEstado", variante.macroestados)
			}

			// Filtro Fase
			if (variante.fases) {
				def fasesPadre = variante.fases.findAll { it.aplicaPadre }
				def fasesHijas = variante.fases.findAll { !it.aplicaPadre }
				
				if (!fasesPadre.isEmpty()){
					inList ("faseActual", fasesPadre)
				}
				if (!fasesHijas.isEmpty()){
					inList ("pedidoHijo.faseActual", fasesHijas)
				}
			}

			if (!variante.esBusquedaEstandar()) {
				def listEstados = convertToList(params.multiselect_comboboxEstado)
				if (listEstados && !listEstados.isEmpty()){
					or {
						
						if (listEstados.contains("Abierto")){
							and{
								isNull("fechaCierre")
								isNull("fechaCancelacion")
								isNull("fechaSuspension")
							}
						}
						if (listEstados.contains("Finalizado")){
							and{
								isNotNull("fechaCierre")
								if (variante.fechaDesde && variante.fechaHasta){
									between("fechaCierre", variante.fechaDesde, variante.fechaHasta)
								}
							}
						}
						if (listEstados.contains("Cancelado")){
							and{
								isNotNull("fechaCancelacion")
								if (variante.fechaDesde && variante.fechaHasta){
									between("fechaCancelacion", variante.fechaDesde, variante.fechaHasta)
								}
							}
						}
						if (listEstados.contains("Suspendido")){
							and{
								isNotNull("fechaSuspension")
								if (variante.fechaDesde && variante.fechaHasta){
									between("fechaSuspension", variante.fechaDesde, variante.fechaHasta)
								}
							}
						}
					}
				}
				
				if (variante.grupos) {
					def listadoGruposSeleccionados
					if (params.multiselect_comboboxGrupo){
						listadoGruposSeleccionados = params.multiselect_comboboxGrupo.class.isArray() ? params.multiselect_comboboxGrupo as List : [params.multiselect_comboboxGrupo]
						//listadoGruposSeleccionados = listadoGruposSeleccionados.collect{ ai -> ai.toLong()}
					}else{
						listadoGruposSeleccionados = variante.grupos
					}
					
					'in' ("pedidoHijo.grupoResponsable", listadoGruposSeleccionados)
				}
				
				
				if (variante.roles && variante.legajoUsuario) {
					createAlias('aprobaciones', 'aprobacion', CriteriaSpecification.LEFT_JOIN)
					//createAlias('logModificaciones', 'logModificacion', CriteriaSpecification.LEFT_JOIN)
					
					def listadoRolesSeleccionados
					if (params.multiselect_comboboxRol){
						listadoRolesSeleccionados = params.multiselect_comboboxRol.class.isArray() ? params.multiselect_comboboxRol as List : [params.multiselect_comboboxRol]
					}else{
						listadoRolesSeleccionados = variante.roles.collect{ rol -> rol.replace("'","")}
					}
					and {
//						eq ("logModificacion.legajo", variante.legajoUsuario)
//						inList ("logModificacion.rol", listadoRolesSeleccionados)
						eq ("aprobacion.usuario", variante.legajoUsuario)
						inList ("aprobacion.rol", listadoRolesSeleccionados)
					}
				}
				
				if (variante.tipoReferencia) {
					eq ("tipoReferencia", variante.tipoReferencia)
					if (variante.criterioTipoReferencia.equalsIgnoreCase(Variante.CRITERIO_ES_IGUAL)) {
						eq ("referenciaOrigen", variante.valorTipoReferencia)
					} else if (variante.criterioTipoReferencia.equalsIgnoreCase(Variante.CRITERIO_CONTIENE)) {
						ilike ("referenciaOrigen", "%$variante.valorTipoReferencia%")
					}
				}
			
				if (variante.valorPrioridad){
					createAlias('prioridad','pri', CriteriaSpecification.LEFT_JOIN)
					if (variante.criterioPrioridad.equalsIgnoreCase(Variante.CRITERIO_ES_IGUAL)) {
						eq ("pri.descripcion", variante.valorPrioridad)
					} else if (variante.criterioPrioridad.equalsIgnoreCase(Variante.CRITERIO_CONTIENE)) {
						ilike ("pri.descripcion", "%$variante.valorPrioridad%")
					}
				}
			
				if (variante.valorSistemaSugerido){
					createAlias('sistemaSugerido','sistemaSug', CriteriaSpecification.LEFT_JOIN)
					if (variante.criterioSistemaSugerido.equalsIgnoreCase(Variante.CRITERIO_ES_IGUAL)) {
						eq ("sistemaSug.descripcion", variante.valorSistemaSugerido)
					} else if (variante.criterioSistemaSugerido.equalsIgnoreCase(Variante.CRITERIO_CONTIENE)) {
						ilike ("sistemaSug.descripcion", "%" + variante.valorSistemaSugerido + "%")
					}
				}
				
				// Filtro Solman
				createAlias('planificaciones','planificaciones', CriteriaSpecification.LEFT_JOIN)
				if (variante.valorSolman) {
					if (variante.criterioSolman.equalsIgnoreCase(Variante.CRITERIO_ES_IGUAL)) {
						eq ("planificaciones.numeroSOLMAN", variante.valorSolman)
					} else if (variante.criterioSolman.equalsIgnoreCase(Variante.CRITERIO_CONTIENE)) {
						ilike ("planificaciones.numeroSOLMAN", "%" + variante.valorSolman + "%")
					}
				}
				
				if (variante.valorRelease) {
					createAlias('planificaciones.numeroRelease','release', CriteriaSpecification.LEFT_JOIN)
					if (variante.criterioRelease.equalsIgnoreCase(Variante.CRITERIO_ES_IGUAL)) {
						eq ("release.descripcion", variante.valorRelease)
					} else if (variante.criterioRelease.equalsIgnoreCase(Variante.CRITERIO_CONTIENE)) {
						ilike ("release.descripcion", "%${variante.valorRelease}%")
					}
				}
				
				// TODO: PONER UN CONTIENE
				createAlias('pedidoHijo.actividades','actividad', CriteriaSpecification.LEFT_JOIN)
				if (variante.valorSimplitPaP) {
					and {
						eq ("actividad.fase", FaseMultiton.getFase(FaseMultiton.IMPLANTACION_HIJO))
						if (variante.criterioSimplitPaP.equalsIgnoreCase(Variante.CRITERIO_ES_IGUAL)) {
							eq ("actividad.numeroTicket", variante.valorSimplitPaP)
						} else if (variante.criterioSimplitPaP.equalsIgnoreCase(Variante.CRITERIO_CONTIENE)) {
							ilike ("actividad.numeroTicket", "%${variante.valorSimplitPaP}%")
						}
					}
				}
				
				if (variante.valorSimplitPAU) {
					and {
						eq ("actividad.fase", FaseMultiton.getFase(FaseMultiton.APROBACION_PAU_HIJO))
						if (variante.criterioSimplitPAU.equalsIgnoreCase(Variante.CRITERIO_ES_IGUAL)) {
							eq ("actividad.numeroTicket", variante.valorSimplitPAU)
						} else if (variante.criterioSimplitPAU.equalsIgnoreCase(Variante.CRITERIO_CONTIENE)) {
							ilike ("actividad.numeroTicket", "%${variante.valorSimplitPAU}%")
						}
					}
				}
			}
			order ("id", "asc")
		}
		params.totalCount = pedidosList.size()
		
		pedidosList
	}
	
	@Transactional(readOnly = true)
	def convertToList(combo){
		if (combo){
			return combo.class.isArray() ? combo as List : [combo]
		}
		return null
	}
	
	@Transactional(readOnly = true)
	private def definirPaginacionInbox(params) {
		params.sort = params.sort?:'id'
		params.order = params.order?:'desc'
		params.max = Math.min(params.max ? params.int('max') : 20, 100000)
		params.offset = params.offset? params.offset.toInteger():0
	}

}
