package ar.com.telecom.pcsPlus

import org.springframework.transaction.annotation.Transactional

import ar.com.telecom.pcs.entities.AbstractPedido
import ar.com.telecom.pcs.entities.ConfiguracionEMailReasignacion
import ar.com.telecom.pcs.entities.ConfiguracionEmailCambioEstado
import ar.com.telecom.pcs.entities.ConfiguracionEmailTipoEvento
import ar.com.telecom.pcs.entities.RolAplicacion
import ar.com.telecom.pedido.observers.PedidoCambioEstadoObserver
import ar.com.telecom.pedido.observers.PedidoHijoReasignacionObserver
import ar.com.telecom.pedido.observers.PedidoReasignacionMailMultipleObserver
import ar.com.telecom.pedido.observers.PedidoReasignacionMailObserver
import ar.com.telecom.pedido.observers.PedidoReasignacionObserver

class ConfiguracionMailService {

	static transactional = false

	@Transactional(readOnly = true)
	def definirObservers(AbstractPedido pedido) {
		definirObservers(pedido, false)	
	}
	
	@Transactional(readOnly = true)
	def definirObservers(AbstractPedido pedido, boolean actualizaObserversHijos) {
//		if (pedido.tieneObservers()) {
//			return
//		}

		if (pedido.esHijo()) {
			pedido.agregarObserver(new PedidoHijoReasignacionObserver())
			log.info "Agregamos observer que loguea reasignación por hijo"
		} else {
			pedido.agregarObserver(new PedidoReasignacionObserver())
			log.info "Agregamos observer que loguea reasignación por padre"
		}
		// TODO: Ver si es necesario que agreguemos estos mismos observers para los hijos del pedido

		agregarObserverCambioEstado(pedido)
		if (actualizaObserversHijos) {
			pedido.pedidosHijos.each { pedidoHijo -> agregarObserverCambioEstado(pedidoHijo) }
		}

		agregarObserverReasignacion(pedido)
		//		Por ahora no lo agregamos
		//		pedido.pedidosHijos.each {
		//			pedidoHijo -> agregarObserverReasignacion(pedidoHijo)
		//		}

		agregarObserverTipoEvento(pedido)
		if (actualizaObserversHijos) {
			pedido.pedidosHijos.each { pedidoHijo -> agregarObserverTipoEvento(pedidoHijo) }
		}

		pedido.unificarObservers()
		log.info "Observers: " + pedido.observers
	}

	def agregarObserverCambioEstado(AbstractPedido pedido) {
		def configuraciones = ConfiguracionEmailCambioEstado.findAllByFaseActual(pedido.fase) 
		
		configuraciones.each { configuracion ->
			pedido.agregarObserver(new PedidoCambioEstadoObserver(configuracionMail: configuracion))
		}
	}

	def agregarObserverTipoEvento(AbstractPedido pedido) {
		def configuraciones = ConfiguracionEmailTipoEvento.findAllByFase(pedido.fase) +
			ConfiguracionEmailTipoEvento.withCriteria { isNull('fase') }

		configuraciones.each { configuracion ->
			configuracion.observers.each { observer ->
				observer.configuracionMail = configuracion
				pedido.agregarObserver(observer)
			}
		}
	}

	def agregarObserverReasignacion(AbstractPedido pedido) {
		def configuraciones = ConfiguracionEMailReasignacion.findAllByFase(pedido.fase) +
			ConfiguracionEMailReasignacion.withCriteria { isNull('fase') }
		
		configuraciones.each { configuracion ->
			// FED - Por ahora Hardcoded
			if ([
				RolAplicacion.RESPONSABLE_ACTIVIDAD,
				RolAplicacion.APROBADOR_FASE
			].contains(configuracion.rol.codigoRol)) {
				pedido.agregarObserver(new PedidoReasignacionMailMultipleObserver(configuracionMail: configuracion))
			} else {
				pedido.agregarObserver(new PedidoReasignacionMailObserver(configuracionMail: configuracion))
			}
			log.info "Se agrega observer por reasignación. Configuración: " + configuracion
		}
	}
	
}
