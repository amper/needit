package ar.com.telecom.mobile

import org.springframework.web.context.request.RequestContextHolder as RCH

class WebInstanceService {

	//static transactional = true
	boolean transactional = false

	static scope = "prototype"

	def getRequest() {
		return RCH.currentRequestAttributes().currentRequest
	}

	def getSession() {
		return RCH.currentRequestAttributes().session
	}
}
