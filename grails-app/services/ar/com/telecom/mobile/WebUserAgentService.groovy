package ar.com.telecom.mobile

class WebUserAgentService extends WebInstanceService {

	public final static String CHROME = "chrome"
	public final static String FIREFOX = "firefox"
	public final static String SAFARI = "safari"
	public final static String OTHER = "other"
	public final static String MSIE = "msie"
	public final static String UNKNOWN = "unknown"
	public final static String BLACKBERRY = "blackberry"
	public final static String SEAMONKEY = "seamonkey"

	public final static int CLIENT_CHROME = 0
	public final static int CLIENT_FIREFOX = 1
	public final static int CLIENT_SAFARI = 2
	public final static int CLIENT_OTHER = 3
	public final static int CLIENT_MSIE = 4
	public final static int CLIENT_UNKNOWN = 5
	public final static int CLIENT_BLACKBERRY = 6
	public final static int CLIENT_SEAMONKEY = 7

	//static transactional = true
	boolean transactional = false

	def getUserAgentTag() {
		getRequest().getHeader("user-agent")
	}

	def getUserAgentInfo() {

		def userAgent = getUserAgentTag()

		def agentInfo = getRequest().getSession().getAttribute("myapp.service.UserAgentIdentService.agentInfo")
		if (agentInfo != null && agentInfo.agentString == userAgent) {
			return agentInfo
		} else if (agentInfo != null && agentInfo.agentString != userAgent) {
			log.warn "User agent string has changed in a single session!"
			log.warn "Previous User Agent: ${agentInfo.agentString}"
			log.warn "New User Agent: ${userAgent}"
			log.warn "Discarding existing agent info and creating new..."
		} else {
			log.debug "User agent info does not exist in session scope, creating..."
		}

		agentInfo = [:]

		def browserVersion
		def browserType
		def operatingSystem
		def platform
		def security = "unknown"
		def language = "en-US"

		if (userAgent == null) {
			agentInfo.browserType = WebUserAgentService.CLIENT_UNKNOWN
			return agentInfo
		}

		browserType = WebUserAgentService.CLIENT_OTHER;

		int pos = -1;
		if ((pos = userAgent.indexOf("Firefox")) >= 0) {
			browserType = WebUserAgentService.CLIENT_FIREFOX;
			browserVersion = userAgent.substring(pos + 8).trim();
			if (browserVersion.indexOf(" ") > 0)
				browserVersion = browserVersion.substring(0, browserVersion.indexOf(" "));
			log.debug("Browser type: Firefox " + browserVersion);
		}
		if ((pos = userAgent.indexOf("Chrome")) >= 0) {
			browserType = WebUserAgentService.CLIENT_CHROME;
			browserVersion = userAgent.substring(pos + 7).trim();
			if (browserVersion.indexOf(" ") > 0)
				browserVersion = browserVersion.substring(0, browserVersion.indexOf(" "));
			log.debug("Browser type: Chrome " + browserVersion);

		}
		if ((pos = userAgent.indexOf("Safari")) >= 0 && (userAgent.indexOf("Chrome") == -1)) {
			browserType = WebUserAgentService.CLIENT_SAFARI;
			browserVersion = userAgent.substring(pos + 7).trim();
			if (browserVersion.indexOf(" ") > 0)
				browserVersion = browserVersion.substring(0, browserVersion.indexOf(" "));
			log.debug("Browser type: Safari " + browserVersion);

		}
		if ((pos = userAgent.indexOf("BlackBerry")) >= 0) {
			browserType = WebUserAgentService.CLIENT_BLACKBERRY;
			browserVersion = userAgent.substring(userAgent.indexOf("/")).trim();
			if (browserVersion.indexOf(" ") > 0)
				browserVersion = browserVersion.substring(0, browserVersion.indexOf(" "));
			log.debug("Browser type: BlackBerry " + browserVersion);

		}
		if ((pos = userAgent.indexOf("SeaMonkey")) >= 0) {
			browserType = WebUserAgentService.CLIENT_SEAMONKEY;
			browserVersion = userAgent.substring(userAgent.indexOf("/")).trim();
			if (browserVersion.indexOf(" ") > 0)
				browserVersion = browserVersion.substring(0, browserVersion.indexOf(" "));
			log.debug("Browser type: SeaMonkey " + browserVersion);

		}
		if ((pos = userAgent.indexOf("MSIE")) >= 0) {
			browserType = WebUserAgentService.CLIENT_MSIE;
			browserVersion = userAgent.substring(pos + 5).trim();
			if (browserVersion.indexOf(" ") > 0)
				browserVersion = browserVersion.substring(0, browserVersion.indexOf(" "));
			if (browserVersion.indexOf(";") > 0)
				browserVersion = browserVersion.substring(0, browserVersion.indexOf(";"));
			log.debug("Browser type: MSIE " + browserVersion);

		}

		if (userAgent.indexOf("(") > 0) {
			String osInfo = userAgent.substring(userAgent.indexOf("(") + 1);
			log.debug "userAgent: " + userAgent
			osInfo = osInfo.substring(0, osInfo.indexOf(")"));

			/*
			String[] infoParts = osInfo.split("; ");
			log.warn "osInfo: " + osInfo
			log.warn "infoParts: " + infoParts
			platform = infoParts[0];
			operatingSystem = infoParts[2];

			if (browserType != WebUserAgentService.CLIENT_MSIE) {
				if (infoParts[1].equals("U"))
					security = "strong";
				if (infoParts[1].equals("I"))
					security = "weak";
				if (infoParts[1].equals("N"))
					security = "none";

				language = infoParts[3];
			}
			*/

			platform = osInfo
			operatingSystem = platform

		} else {
			if (browserType == WebUserAgentService.CLIENT_BLACKBERRY) {
				operatingSystem = "BlackBerry " + browserVersion;
			}
		}

		agentInfo.browserVersion = browserVersion
		agentInfo.browserType = browserType
		agentInfo.operatingSystem = operatingSystem
		agentInfo.platform = platform
		agentInfo.security = security
		agentInfo.language = language
		agentInfo.agentString = userAgent

		getRequest().getSession().setAttribute("myapp.service.UserAgentIdentService.agentInfo", agentInfo)
		return agentInfo
	}


	public boolean isChrome() {
		return (getUserAgentInfo().browserType == WebUserAgentService.CLIENT_CHROME);
	}

	public boolean isFirefox() {
		return (getUserAgentInfo().browserType == WebUserAgentService.CLIENT_FIREFOX);
	}

	public boolean isMsie() {
		return (getUserAgentInfo().browserType == WebUserAgentService.CLIENT_MSIE);
	}

	public boolean isOther() {
		return (getUserAgentInfo().browserType == WebUserAgentService.CLIENT_OTHER);
	}

	public boolean isSafari() {
		return (getUserAgentInfo().browserType == WebUserAgentService.CLIENT_SAFARI);
	}

	public boolean isBlackberry() {
		return (getUserAgentInfo().browserType == WebUserAgentService.CLIENT_BLACKBERRY);
	}

	public boolean isSeamonkey() {
		return (getUserAgentInfo().browserType == WebUserAgentService.CLIENT_SEAMONKEY);
	}

	public String getBrowserVersion() {
		return getUserAgentInfo().browserVersion;
	}

	public String getOperatingSystem() {
		return getUserAgentInfo().operatingSystem;
	}

	public String getPlatform() {
		return getUserAgentInfo().platform;
	}

	public String getSecurity() {
		return getUserAgentInfo().security;
	}

	public String getLanguage() {
		return getUserAgentInfo().language;
	}

	public String getBrowserType() {
		switch (getUserAgentInfo().browserType) {
			case CLIENT_FIREFOX:
				return FIREFOX;
			case CLIENT_CHROME:
				return CHROME;
			case CLIENT_SAFARI:
				return SAFARI;
			case CLIENT_SEAMONKEY:
				return SEAMONKEY;
			case CLIENT_MSIE:
				return MSIE;
			case CLIENT_BLACKBERRY:
				return BLACKBERRY;
			case CLIENT_OTHER:
			case CLIENT_UNKNOWN:
			default:
				return OTHER;
		}
	}
}
