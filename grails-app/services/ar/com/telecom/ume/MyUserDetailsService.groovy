package ar.com.telecom.ume

import org.codehaus.groovy.grails.plugins.springsecurity.GrailsUserDetailsService
import org.codehaus.groovy.grails.plugins.springsecurity.SpringSecurityUtils
import org.springframework.security.core.authority.GrantedAuthorityImpl
import org.springframework.security.core.userdetails.UserDetails
import org.springframework.security.core.userdetails.UsernameNotFoundException

import ar.com.telecom.Person
import ar.com.telecom.provider.MyUserDetails

class MyUserDetailsService implements GrailsUserDetailsService {

	def umeService
	/**
	 * Some Spring Security classes (e.g. RoleHierarchyVoter) expect at least
	 * one role, so we give a user with no granted roles this one which gets
	 * past that restriction but doesn't grant anything.
	 */
	static final List NO_ROLES = [
		new GrantedAuthorityImpl(SpringSecurityUtils.NO_ROLE)
	]

	UserDetails loadUserByUsername(String username, boolean loadRoles)
	throws UsernameNotFoundException {
		return loadUserByUsername(username)
	}

	UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {

		Person.withTransaction { status ->

			def user = Person.findByUsername(username)
			if (!user) throw new UsernameNotFoundException(
				'User not found', username)

			
			def authorities = umeService.getGrupos(username).collect {
				new GrantedAuthorityImpl(it.authority)
			}

			return new MyUserDetails(user.username, user.password, user.enabled,
				!user.accountExpired, !user.passwordExpired,
				!user.accountLocked, authorities ?: NO_ROLES, user.id)
		}
	}
}
