package ar.com.telecom.ume

import groovy.sql.Sql

import java.sql.SQLException

import org.codehaus.groovy.grails.commons.ConfigurationHolder as ConfigHolder
import org.springframework.transaction.annotation.Transactional

import ar.com.telecom.Person
import ar.com.telecom.UsuariosDireccion
import ar.com.telecom.entitiesume.Estructura
import ar.com.telecom.entitiesume.Usuario
import ar.com.telecom.exceptions.SessionExpiredException
import ar.com.telecom.pcs.entities.AreaSoporte
import ar.com.telecom.pcs.entities.GrupoGestionDemanda
import ar.com.telecom.pcs.entities.ParametrosSistema
import ar.com.telecom.pcs.entities.Sistema
import ar.com.telecom.pcs.entities.UmbralAprobacionEconomica
import ar.com.telecom.pcs.entities.UmbralAprobacionEconomicaDireccion

class UmeService {
		def springSecurityService
		def dataSource
	
		static transactional = false
	
		public static final String PREFIJO_ROLE = "ROLE_"
	
		/**
		 * Permite saber cuál es el gerente asociado a un usuario
		 *
		 * @param legajo el legajo de Telecom
		 * @return el objeto usuario que es gerente del legajo pasado como parámetro
		 */
		@Transactional(readOnly = true)
		def getGerente(String legajo){
			def urlUME = getUmeURL()
			def uri = urlUME.concat("/gerenteByUsuario/").concat(legajo)
			def gerenteXML = new XmlSlurper().parse(uri)
			def gerente = generarUser(gerenteXML)
			return gerente
		}
	
		/**
		 * Permite saber si un usuario es gerente
		 *
		 * @param legajo el legajo de Telecom
		 * @return boolean si es o no gerente
		 */
		@Transactional(readOnly = true)
		def isGerente(String legajo){
			def parametros = ParametrosSistema.list().first()
			log.info "Minimo jerarquia gerente: " + parametros.minimoJerarquiaGerente
			log.info "Maximo jerarquia gerente: " + parametros.maximoJerarquiaGerente
			def tieneJerarquias = this.tieneJerarquias(legajo, parametros.minimoJerarquiaGerente, parametros.maximoJerarquiaGerente)
			log.info "Tiene jerarquias: " + tieneJerarquias
			return tieneJerarquias
		}
	
		def isAprobadorImpacto(String legajo){
			def parametros = ParametrosSistema.list().first()
			def esAprobadorImpacto = this.tieneJerarquias(legajo, parametros.minimoJerarquiaAprobador, parametros.maximoJerarquiaAprobador)
			return esAprobadorImpacto
		}
	
		@Transactional(readOnly = true)
		def tieneJerarquias(String legajo, int minimaJerarquia, int maximaJerarquia){
			if (!legajo) {
				return false
			}
			Usuario user = getUsuario(legajo)
			log.info "User: " + user
			if (!user || !user.jerarquias) {
				return false
			}
	
			log.info "User jerarquias: " + user.jerarquias
			return user.tieneJerarquias(minimaJerarquia, maximaJerarquia)
		}
	
		@Transactional(readOnly = true)
		def getJerarquiaUsuario(String legajo){
			def uri = getUmeURL().concat("/jerarquiaByUsuario/").concat(legajo)
			def jerarquiaXML = new XmlSlurper().parse(uri)
			return jerarquiaXML.usuario.collect { generarUser(it) }
		}
	
		@Transactional(readOnly = true)
		def getNombreApellido(legajo){
			return this.getUsuario(legajo).nombreApellido
		}
	
		@Transactional(readOnly = true)
		def generarUser(itUser){
			def codigoDireccion = itUser.direccionCodigo
			def descripcionDireccion = itUser.direccion
			def legajo = itUser.legajo.toString()
			if (!codigoDireccion || codigoDireccion.equals("")) {
				def estructura = getDireccion(legajo)
				codigoDireccion = estructura?.codigo
				descripcionDireccion = estructura?.descripcion
			}
			def user = new Usuario(
					nombreApellido : itUser.nombreApellido,
					legajo : legajo,
					email : itUser.email,
					jerarquias: getListaJerarquias(itUser.jerarquias),
					gerenciaCodigo : itUser.gerenciaCodigo,
					gerencia : itUser.gerencia,
					direccionCodigo : codigoDireccion,
					direccion : descripcionDireccion
					)
			return user
		}
	
		@Transactional(readOnly = true)
		def generarEstructura(it){
			def estructura = new Estructura(
					codigo : it.codigo,
					codigoPadre : it.codigoPadre,
					codigoDireccion : it.codigoDireccion,
					descripcion: it.descripcion,
					responsable : it.responsable,
					jerarquia : it.jerarquia,
					esDireccion : it.esDireccion,
					)
			return estructura
		}
	
		@Transactional(readOnly = true)
		private getDireccion(legajo) {
	//		try {
	//			def conn = new URL(ConfigHolder.config.url.needitcron.concat("?usuario=$legajo"))
	//			def connection = conn.openConnection()
	//			connection.setRequestMethod("GET")
	//			connection.connect()
	//
	//			def codigoEstructura = connection.content.text
				if (UsuariosDireccion.instance.direccionesCargadas()) {
					this.actualizarUsuariosDireccion()
				}
				def estructura
				def codigoEstructura = UsuariosDireccion.instance.getDireccion(legajo)
				log.info "Legajo: " + legajo + " - Estructura: " + codigoEstructura
				if (codigoEstructura){
					def uri = getUmeURL().concat("/estructuraByCodigo/").concat(codigoEstructura)
					def estructuraXML = new XmlSlurper().parse(uri)
					estructura = generarEstructura(estructuraXML)
					log.info "Estructura: " + estructura
				}
				return estructura
	//		} catch (ConnectException e) {
	//			throw new BusinessException("La conexión a needIt-cron falló. Revise si el servicio está levantado.")
	//		}
		}
	
		private getListaJerarquias(jerarquias){
			if (jerarquias) {
				jerarquias.children().collect { jerarquia -> jerarquia.toString() }
			} else {
				[]
			}
		}
	
		@Transactional(readOnly = true)
		def getGerentesDireccion(Usuario usuario) {
			def gerentes = this.internalGetGerentes("gerentesByUsuario", usuario?.legajo, null)
			if (!gerentes) {
				gerentes = UsuariosDireccion.instance.getUsuarios(usuario?.direccionCodigo)
			}
			return gerentes
		}
	
		@Transactional(readOnly = true)
		def getGerentesPorDireccion(String direccion){
			println "getGerentesPorDireccion - direccion: " + direccion
			if(!direccion){
				return []
			}
	
			def gerentes = this.internalGetGerentes("gerentesByDireccion", direccion, null)
			println "gerentes: " + gerentes 
			if (!gerentes) {
				gerentes = UsuariosDireccion.instance.getUsuarios(direccion)
			}
			return gerentes
		}
	
	
		@Transactional(readOnly = true)
		def getAprobadoresImpactoPorDireccion(String direccion, boolean esDirector){
			def aprobadoresImpacto = getGerentesPorDireccion(direccion).findAll { gerente -> gerente.isAprobadorImpacto() }
			
			// si es director -> filtro entre 40 y 60
			if(esDirector){
				aprobadoresImpacto = filtrarAprobadoresImpacto(aprobadoresImpacto, 40, 60)  // TODO: Parametrizar
			}

			return aprobadoresImpacto			
		}
	
		@Transactional(readOnly = true)
		def getGestionDemandaPorGrupoYDireccion(String grupo, String direccion){
			return getUsuariosGrupoLDAP(grupo).findAll { fgd -> fgd.esDeDireccion(direccion) }
		}
	
		@Transactional(readOnly = true)
		def getInterlocutoresUsuariosPorGrupoYDireccion(String grupo, String direccion){
			return getUsuariosGrupoLDAP(grupo).findAll { iu -> iu.esDeDireccion(direccion) }
		}
	
		private internalGetGerentes(String serviceMethod, String legajo, String filtro) {
			def urlUME = getUmeURL()
			def uri = urlUME.concat("/" + serviceMethod)
			if (legajo) {
				uri = uri.concat("/" + legajo)
			}
			if (filtro){
				uri = uri.concat("?filtro=${filtro}")
			}
	
			def gerentesXML = new XmlSlurper().parse(uri)
			def gerentesUsuario = gerentesXML.usuario.collect { generarUser(it) }
			return gerentesUsuario.unique().sort({ it.nombreApellido })
		}	
	
		@Transactional(readOnly = true)
		def getGerentesUsuarios(){
			this.internalGetGerentes("gerentes", null, null)
		}
	
		//TODO: renombrar el método getGerentesUsuarios por getUsuarios
		@Transactional(readOnly = true)
		def getGerentesUsuarios(methodName, filtro){
			this.internalGetGerentes(methodName, null, filtro)
		}
	
	
		/**
		 *
		 * Permite obtener el objeto usuario asociado en base a un legajo
		 *
		 * @param legajo el legajo de Telecom
		 * @return el usuario asociado
		 */
		@Transactional(readOnly = true)
		def getUsuario(String legajo) {
			if (!legajo) {
				return null
			}
			def urlUME = getUmeURL()
			def uri = urlUME.concat("/usuarioByLegajo/").concat(legajo)
			def usuarioXML = new XmlSlurper().parse(uri)
			def usuario = generarUser(usuarioXML)
			return usuario
		}
	
		@Transactional(readOnly = true)
		@Deprecated
		def getAllUsuarios() {
			def usuarios = new ArrayList<Usuario>()
	
			def listaPersonas = Person.list()
	
			listaPersonas.each { itUser ->
				def user = generarUser(itUser)
				if (!usuarios.contains(user)) {
					usuarios.add(user)
				}
			}
	
			return usuarios.sort()
		}
	
	
		/**
		 * Permite conocer los usuarios de un grupo LDAP.
		 * Los gerentes no conforman un grupo LDAP, por lo que deben usar {@link getGerentes}
		 *
		 * @param grupoLDAP el grupo que debe existir en el arbol LDAP de seguridad
		 * @param legajo: Si existe es para que me traiga todas las personas de un grupo LDAP que pertenecen a la misma direccion que el usuario del legajo
		 *
		 */
		@Transactional(readOnly = true)
		def getUsuariosGrupoLDAP(String grupoLDAP, String legajo){
			def codigoDireccion = null
			def uri
	
			def urlUME = getUmeURL()
			if (legajo){
				uri = urlUME.concat("/usuarioByLegajo/").concat(legajo)
				def usuarioXML = new XmlSlurper().parse(uri)
				Usuario usuario = generarUser(usuarioXML)
				codigoDireccion = usuario?.direccionCodigo
			}
			uri = urlUME.concat("/usuariosByGrupoLDAP/").concat(grupoLDAP)
			def usuariosGrupoLDAPXML = new XmlSlurper().parse(uri)
			def usuariosGrupoLDAP = new ArrayList<Usuario>()
	
			usuariosGrupoLDAPXML.usuario.each {
				Usuario user = generarUser(it)
				if (!codigoDireccion || user?.direccionCodigo?.equalsIgnoreCase(codigoDireccion)) {
					log.info "     Direcci\u00F3n del usuario " + user?.direccionCodigo + ": " + user?.getNombreApellido()
					usuariosGrupoLDAP.add(user)
				}
			}
			return usuariosGrupoLDAP.sort()
		}
	
		/**
		 * Permite saber si un usuario pertenece a un grupo
		 */
		@Transactional(readOnly = true)
		def usuarioPerteneceGrupo(String grupoLDAP, String legajo){
			if (!grupoLDAP) {
				//throw new SystemException("usuarioPerteneceGrupo: falta ingresar grupo LDAP")
				//FED: no me cierra mucho hacer esto pero quiero ver por qué tira error
				return false
			}
			if (!legajo) {
				//throw new SystemException("usuarioPerteneceGrupo: falta ingresar legajo")
				//FED: no me cierra mucho hacer esto pero quiero ver por qué tira error
				return false
			}
			def urlUME = getUmeURL()
			def uri = urlUME.concat("/gruposLDAPByLegajo/").concat(legajo)
			def usuarioGruposXML = new XmlSlurper().parse(uri)
			return usuarioGruposXML?.grupoldap?.any { it.nombre.equals(grupoLDAP) }
		}
	
	
		/**
		 * Permite conocer el usuario conectado
		 */
		@Transactional(readOnly = true)
		def getUsuarioLogueado() {
			def usuario = springSecurityService?.principal
			log.info "Usuario Logeado:_ " + springSecurityService?.principal
			if (!usuario || usuario.equals("anonymousUser")) {
				throw new SessionExpiredException()
			}
			return usuario.username
		}
	
		@Transactional(readOnly = true)
		def getRolesUsuarioLogeado(){
			def roles = springSecurityService?.principal?.authorities
			return roles
		}
	
		/**
		 * Permite conocer los grupos LDAP de NeedIt.
		 */
		@Transactional(readOnly = true)
		def getGrupos(){
			def result = GrupoGestionDemanda.list().collect { grupoLDAP -> grupoLDAP.descripcion }
			result = result.plus(AreaSoporte.list().collect { areaSoporte -> areaSoporte.grupoAdministrador })
			def gruposSistema = Sistema.list().inject([]) { grupos, sistema -> grupos.plus(sistema.gruposGestion)}
			result = result.plus(gruposSistema)
			return result.sort()
		}
	
		/**
		 * Permite conocer los grupos de un usuario.
		 *
		 * @param legajo: debe existir
		 *
		 */
		@Transactional(readOnly = true)
		def getGrupos(String legajo){
			if (!legajo) {
				return []
			}
			def urlUME = getUmeURL()
			def uri = urlUME.concat("/gruposLDAPByLegajo/").concat(legajo)
			def usuarioGrupoLDAPXML = new XmlSlurper().parse(uri)
			def gruposLDAPUsuario = []
	
			usuarioGrupoLDAPXML?.grupoldap?.each {
				gruposLDAPUsuario.add(it.nombre.toString())
			}
			return gruposLDAPUsuario.sort()
		}
	
		String getUmeURL() {
			return org.codehaus.groovy.grails.commons.ConfigurationHolder.config.ume.service.url
		}
	
		boolean puedeRegistrarPedido(){
			def parametro = ParametrosSistema.list().first()
			def legajo
			try {
				legajo = getUsuarioLogueado()
			} catch (SessionExpiredException e) {
				// No está logueado
				return false
			}
	
			log.info "Authorities:_ " + springSecurityService?.principal
	
			//		if (Environment.current != Environment.PRODUCTION){
			//			return ((usuarioPerteneceGrupo( parametro.grupoLDAPUsuarioFinal, legajo))
			//			|| (usuarioPerteneceGrupo( parametro.grupoLDAPInterlocutorUsuario, legajo))
			//			|| (usuarioPerteneceGrupo( parametro.grupoLDAPCoordinadorPedido, legajo))
			//			|| (GrupoGestionDemanda.list().any { grupoGestionDemanda -> usuarioPerteneceGrupo( grupoGestionDemanda.grupoLDAP, legajo) })
			//			|| (isGerente(legajo)))
			//		} else {
			def roles = []
			agregarRol(roles, parametro.grupoLDAPUsuarioFinal)
			agregarRol(roles, parametro.grupoLDAPInterlocutorUsuario)
			agregarRol(roles, parametro.grupoLDAPCoordinadorPedido)
	
			GrupoGestionDemanda.list().each { grupoGestionDemanda ->
				agregarRol(roles, grupoGestionDemanda.grupoLDAP)
			}
	
			def rolesAsignados = springSecurityService?.principal?.authorities.intersect(roles)
			def tieneRolesAsignados = rolesAsignados.size() > 0
	
			return tieneRolesAsignados || isGerente(legajo)
			//		}
		}
	
		void agregarRol(roles, unRol) {
			if (unRol){
				roles.add(PREFIJO_ROLE + unRol)
			}
		}
	
		@Transactional(readOnly = true)
		def getUsuariosGrupoLDAP(String grupoLDAP){
			return getUsuariosGrupoLDAP(grupoLDAP, null)
		}
	
		/**
		 * **********************************************************************************************
		 * Metodos privados para aprobadores de impacto
		 * **********************************************************************************************
		 */
		@Transactional(readOnly = true)
		def aprobadoresImpacto(String legajo) {
			if (!legajo) {
				return []
			}
			
			return filtrarAprobadoresImpacto(getJerarquiaUsuario(legajo))
		}
	
		@Transactional(readOnly = true)
		@Deprecated
		def aprobadorImpactoSegundaInstancia(pedido) {
			def aprobadoresPosibles = aprobadoresDefault(pedido)
			if (aprobadoresPosibles.isEmpty()) {
				return null
			}
			def indiceDefault = indiceAprobadorDefault(pedido)
			indiceDefault = Math.min(indiceDefault + 1, aprobadoresPosibles.size() - 1)
			return aprobadoresPosibles.get(indiceDefault)
		}

		@Transactional(readOnly = true)
		def filtrarAprobadoresImpacto(aprobadores, minJerarquia, maxJerarquia) {
			def aprobadoresOriginales = aprobadores
			//UME 1.5
			def aprobadoresResult = aprobadoresOriginales.findAll {  usuario ->
			 usuario.tieneJerarquias(minJerarquia, maxJerarquia)
			}.sort { it.jerarquia }
	
			 /*
			// UME 1.0
			def aprobadoresResult = aprobadoresOriginales.findAll {  usuario ->
				def jerarquiaLegajo = usuario.jerarquia as int
				jerarquiaLegajo >= parametros.minimoJerarquiaAprobador && jerarquiaLegajo <= parametros.maximoJerarquiaAprobador
			}.sort { it.jerarquia }
			 */
			return aprobadoresResult
		}

		@Transactional(readOnly = true)
		def filtrarAprobadoresImpacto(aprobadores) {
			def parametros = ParametrosSistema.list().first()
			filtrarAprobadoresImpacto(aprobadores, parametros.minimoJerarquiaAprobador, parametros.maximoJerarquiaAprobador)
		}
	
		@Transactional(readOnly = true)
		def indiceAprobadorDefault(pedido) {
			def costoCambio = pedido.costoTotalCambioConsolidado()
			def umbral = UmbralAprobacionEconomicaDireccion.findByTipoGestionAndDireccion(pedido.tipoGestion, pedido.codigoDireccionGerencia)
			if (!umbral) {
				umbral = UmbralAprobacionEconomica.findByTipoGestion(pedido.tipoGestion)
			}
			if (umbral) {
				if (costoCambio < umbral.rangoDesde) {
					return 2
				} else {
					if (costoCambio > umbral.rangoHasta) {
						return 0
					} else {
						return 1
					}
				}
			} else {
				return 2
			}
		}
	
		@Transactional(readOnly = true)
		def aprobadoresDefault(pedido) {
			def legajo = pedido?.legajoGerenteUsuario
			def aprobadoresPosibles = aprobadoresImpacto(legajo)
			def cantidadAprobadores = aprobadoresPosibles.size()
			if (cantidadAprobadores == 0) {
				return []
			}
			//log.info "Costo del cambio: " + costoCambio
			for (int i = cantidadAprobadores; i < 3; i++) {
				aprobadoresPosibles.add(aprobadoresPosibles.last())
			}
			return aprobadoresPosibles
		}
	
		/**
		 * **********************************************************************************************
		 * Fin Metodos privados para aprobadores de impacto
		 * **********************************************************************************************
		 */
	
	
	
		def actualizarUsuariosDireccion() {
			def parametros = ParametrosSistema.list().first()
			def instanceUsuariosDireccion = UsuariosDireccion.instance
			try{
				synchronized (instanceUsuariosDireccion) {
					def results = new Sql(dataSource).rows("execute asociarDireccionesUsuario ?",[parametros.minimoJerarquiaAprobador])
					results.each { result ->
						instanceUsuariosDireccion.relate(result.getAt("legajo"), result.getAt("direccion"))
					}
					instanceUsuariosDireccion.commit(this)
				}
				return instanceUsuariosDireccion.usuariosDireccion
			}catch(SQLException sqle){
				log.error "Excepcion: " + sqle.message
				sqle.printStackTrace()
				throw sqle
			}
		}
	
		boolean esAdministrador(legajo){
			def parametros = ParametrosSistema.list().first()
			def grupoAdmin = parametros.grupoAdmin
	
			return usuarioPerteneceGrupo(grupoAdmin, legajo)
		}

}
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                           