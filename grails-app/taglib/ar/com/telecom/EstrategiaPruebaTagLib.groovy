package ar.com.telecom

import ar.com.telecom.pcs.entities.Justificacion
import ar.com.telecom.pcs.entities.ParametrizacionTipoPrueba
import ar.com.telecom.pcs.entities.TipoPrueba
import ar.com.telecom.pedido.estrategiaPrueba.EstrategiaAdapter

class EstrategiaPruebaTagLib {

	def estrategiaPrueba = { attrs, body ->
		def parametrizacionEstrategias = attrs.parametrizacionEstrategias
		def ecs = attrs.ecs
		EstrategiaAdapter estrategiaDefinida = ecs.definirEstrategiaAdapter()

		if (parametrizacionEstrategias){
			estrategiaDefinida.tiposPrueba.eachWithIndex { estrategia, index ->
				def parametrizacionEstrategia = parametrizacionEstrategias.find { it.tipoPrueba.descripcion.equalsIgnoreCase(estrategia.tipoPrueba)}
				def campoRealiza = crearCampo(parametrizacionEstrategia, parametrizacionEstrategia?.definirRealiza(), "realiza${estrategia.tipoPrueba}", parametrizacionEstrategia?.puedeEditar(), estrategia.seRealiza)
				def campoConsolida = crearCampo(parametrizacionEstrategia, parametrizacionEstrategia?.definirConsolida(), "${estrategia.tipoPrueba}integrada", parametrizacionEstrategia?.puedeEditarConsolida(), estrategia.seConsolida)
				def pruebas = Justificacion.findAllByTipoPrueba(TipoPrueba.findByDescripcion(estrategia.tipoPrueba))
				def justificacion = estrategia.justificacion
				
				out << render(template: '/especificacionCambio/estrategia', model: ['estrategia': estrategia, 'campoRealiza':campoRealiza, 'campoConsolida': campoConsolida, 'pruebas':pruebas, 'justificacion':justificacion, 'index':(index+1)])
			}
		}else{
			out << ""
		}
	}

	def crearCampo(estrategia, estado, nombreCampo, puedeEditar, valor){
		def campo
		if (estado != null){
//			if (estado) {
				if (puedeEditar){
					campo = g.checkBox (name: "${nombreCampo}", checked: valor)
				}else{
					campo = g.hiddenField (name: "${nombreCampo}", value: valor)
					campo += g.checkBoxStatusImage(estado: estado)
				}
//			}else{
//				if (puedeEditar){
//					campo = g.  checkBox(name: "${nombreCampo}", checked: false)
//				}else{
//					campo = g.hiddenField (name: "${nombreCampo}", value: valor)
//					campo += g.checkBoxStatusImage(estado: estado)
//				}
//			}
		} else {
			campo = ParametrizacionTipoPrueba.NO_APLICA
		}
		return campo
	}
}
