package ar.com.telecom

import org.apache.commons.lang.math.RandomUtils

import ar.com.telecom.exceptions.BusinessException
import ar.com.telecom.exceptions.ConcurrentAccessException
import ar.com.telecom.exceptions.SessionExpiredException
import ar.com.telecom.pedido.especificacionImpacto.Impacto;
import ar.com.telecom.util.StringUtil

class ErrorTagLib {
	def pedidoService
	
	public static String ERROR = "contenidoError"
	public static String WARNING = "contenidoWarning"
	public static String MSG = "contenidoInfo"
	
	public static int ERROR_CODE = 1
	public static int WARNING_CODE = 2
	public static int MSG_CODE = 3

	def errorClass = { attrs, body ->
		String error = ""
		def type = attrs.type?.toInteger()
		if (type > 0){
			error = classError(type)
		}
		out << error
	}

	def classError(type){
		switch (type) {
			case ERROR_CODE:
				return ERROR
			case WARNING_CODE:
				return WARNING
			case MSG_CODE:
				return MSG
		}
	}
	
	def typeError(type){
		switch (type) {
			case ERROR:
				return ERROR_CODE
			case WARNING:
				return WARNING_CODE
			case MSG:
				return MSG_CODE
		}
	}

	def codigoError = { attrs, body ->
		def codigoError = StringUtil.hasta("" + RandomUtils.nextLong(), 25)
		log.error "Codigo de error: " + codigoError
		out << codigoError
	}

	def mostrarError = { attrs, body ->
		def origenError
		def contenido
		def mensajeError
		boolean systemError = false
		def link
		try {
			def exception = attrs.exception
			origenError = exception?.cause ? exception?.cause : exception

			if (origenError.class.equals(SessionExpiredException.class)) {
				mensajeError = "Su sesi\u00F3n ha expirado. Debe volver a loguearse."
				contenido = MSG
			} else {
				if (origenError.class.equals(ConcurrentAccessException.class)) {
					contenido = MSG
					mensajeError = origenError.message
					def pedido = getPedidoATrabajar(params)
					link = pedido?.getLinkURL("Volver al pedido")
				} else {
					if (origenError.class.equals(BusinessException.class)) {
						mensajeError = origenError.message
						contenido = WARNING
					} else {
						mensajeError = "Ha ocurrido un error en la aplicacion. Se genero un registro con la informacion.<br/>"
						systemError = true
						contenido = ERROR
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace()
			log.error e

			origenError = e
			contenido = ERROR
			mensajeError =  "Ha ocurrido un error en la aplicacion. Se genero un registro con la informacion.<br/>"
			systemError = true
		}
		out << render(template: '/errorApp/errorTemplate', model: ['origenError': origenError, 'contenido': contenido, 'mensajeError': mensajeError, 'systemError': systemError, 'link':link])
	}
	
	
	private def getPedidoATrabajar(params) {
		def trabajoSobreHijo = Impacto.aplicaAHijo(params.impacto)
		if (trabajoSobreHijo) {
			return pedidoService.obtenerPedidoInicial(params.impacto, false)
		} else {
			return pedidoService.obtenerPedidoInicial(params.pedidoId, false)
		}
	}
	
	
	
}
