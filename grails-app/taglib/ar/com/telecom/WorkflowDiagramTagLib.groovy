package ar.com.telecom

import javax.swing.text.MaskFormatter.HexCharacter;

import org.apache.catalina.util.HexUtils;
import org.apache.commons.lang.CharUtils;

import ar.com.telecom.pcs.entities.AbstractPedido

class WorkflowDiagramTagLib {

	def workflow = { attrs ->
		def pedido = AbstractPedido.get(attrs.pedidoId)

		def fases
		def faseActual
		def pedidoArbol
		def esHijo = false

		if (pedido) {

			fases = pedido?.fasesActuales()
			if (pedido.faseActual) {
				faseActual = pedido.faseActual
			}

			def mapaFasesHijos = [ : ]

			if (pedido.esHijo()) {
				esHijo = true
				pedido.parent.fasesActuales().each { fasePadre ->
					def fasesHijas = fases.findAll { fase -> fase.fasePadre.equals(fasePadre) && fase.correspondeMostrar(pedido) }
					mapaFasesHijos.put(fasePadre, fasesHijas)
				}
				pedidoArbol = pedido.parent
			} else {
				pedidoArbol = pedido
			}

			//out << render(template:"/templates/workflowDiagram", model:[fases:fases, faseActual:faseActual, id: pedido.id, esHijo: pedido.esHijo(), mapaFasesHijos: mapaFasesHijos])
			out << render(template:"/templates/workflowNavigator", model:[oPedido : pedidoArbol, esHijo: pedido.esHijo(), fases: fases, mapaFasesHijos: mapaFasesHijos, faseActual: faseActual])
		} 
	}
}
