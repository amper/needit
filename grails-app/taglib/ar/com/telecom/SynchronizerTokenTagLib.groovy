package ar.com.telecom

import static org.codehaus.groovy.grails.web.servlet.mvc.SynchronizerToken.KEY

import org.codehaus.groovy.grails.plugins.web.taglib.JavascriptTagLib
import org.codehaus.groovy.grails.web.servlet.mvc.SynchronizerToken

import ar.com.telecom.pcs.entities.Pedido
import ar.com.telecom.pcs.entities.PedidoHijo


class SynchronizerTokenTagLib extends JavascriptTagLib{
	def umeService

	def updateToken = { attrs, body ->
		def token = SynchronizerToken.store(session)
		if (token){
			response.addHeader(KEY, token.currentToken.toString())
		}
		//		def pedido = attrs.pedido
		//		def pedidoHijo = attrs.pedidoHijo
		//		if (pedido) {
		//			response.addHeader("pedidoVersion", "" + pedido?.version)
		//		}
		//		if (pedidoHijo) {
		//			response.addHeader("pedidoHijoVersion", "" + pedidoHijo?.version)
		//		}
		def origen = attrs.pedido ?: attrs.pedidoHijo
		if (origen) {
			def aprobacion = origen.aprobacionPendiente(origen.faseActual, umeService.getUsuarioLogueado())
			response.addHeader("aprobacionId", "" + aprobacion?.id)
//			response.addHeader("aprobacionVersion", "" + aprobacion?.version)
		}
	}

	def submitToRemoteSecure = { attrs, body ->
		// get javascript provider
		def p = getProvider()
		// prepare form settings
		attrs.forSubmitTag = ".form"
		p.prepareAjaxForm(attrs)
		out << "<input type='button'"
		out << secureRemoteFunction(attrs)

		if (attrs.name)
			out << " name=\"${attrs?.name}\" "
		if (attrs.id)
			out << " id=\"${attrs?.id}\" "
		if (attrs.value)
			out << " value=\"${attrs?.value}\" "
		if (attrs.style)
			out << " style=\"${attrs?.style}\" "
		if("${attrs.class}")
			out << " class=\"${attrs?.class}\" "
		out << "></input>"
		cleanButton(attrs)
		attrs.each { k,v ->
			out << ' ' << k << "=\"" << v << "\""
		}
		out << body()
	}

	def secureRemoteFunction = { attrs ->
		// before remote function
		def after = ''
		if (attrs.before) {
			out << "${attrs.remove('before')};"
		}
		if (attrs.after) {
			after = "${attrs.remove('after')};"
		}

		attrs.controller = params.controller

		out << ' onclick='
		out << onclickFunction(attrs)

		if (after) {
			out << after
		}
	}

	def secureRemoteLink = {attrs, body ->
		out << '<a href="'
		def cloned = deepClone(attrs)
		out << createLink(cloned)
		out << '" onclick='
		out << onclickFunction(attrs)
		clean(attrs)
		attrs.each { k,v ->
			out << ' ' << k << "=\"" << v << "\""
		}
		out << ">"
		out << body()
		out << "</a>"
	}

	private void clean(attrs){
		attrs.remove('controller')
		attrs.remove('action')
		attrs.remove('params')
		attrs.remove('update')
		attrs.remove('style')
		attrs.remove('id')
		attrs.remove('name')
	}

	private void cleanButton(attrs){
		clean(attrs)
		attrs.remove('class')
		attrs.remove('onComplete')
		attrs.remove('onSuccess')
		attrs.remove('onLoading')
		attrs.remove('type')
		attrs.remove('value')
	}

	private String onclickFunction(attrs){
		String parameters
		//log.warn "onclickFunction: ${params} - ${attrs} - ${attrs?.params}"
		if (attrs?.params.class){
			parameters = attrs?.params
		}else{
			parameters = "{"
			parameters += attrs.params.collect{k,v-> "$k:'$v'"}.join(",")
			if (attrs.paramsJavascript) {
				parameters += ","
			}
			parameters += attrs.paramsJavascript.collect{k,v-> "$k: $v"}.join(",")
			parameters += ",id:'${attrs.id}'}"
		}
		
		String args = "{container: '${attrs.update}', controller: '${attrs.controller?:params.controller}', action: '${attrs.action}', parameters: $parameters"
		args += attrs.onLoading? ", onLoading: function(response){${attrs.onLoading}}" : ""
		args += attrs.onSuccess? ", onSuccess: function(response){${attrs.onSuccess}}" : ""
		args += attrs.onFailure? ", onFailure: function(response){${attrs.onFailure}}" : ""
		args += attrs.onComplete? ", onComplete: function(response){ ${attrs.onComplete} }}" : "}"
		String returnClick = "\""
		if (attrs.before) {
			returnClick += "${attrs.before}"
			attrs.remove('before')
		}
		returnClick += "ajaxCall($args, '');return false;\""
		return returnClick
	}

	private deepClone(Map map) {
		def cloned = [:]
		map.each { k,v ->
			if (v instanceof Map) {
				cloned[k] = deepClone(v)
			}
			else {
				cloned[k] = v
			}
		}
		return cloned
	}
}

