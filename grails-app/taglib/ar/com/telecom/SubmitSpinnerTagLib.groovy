package ar.com.telecom

class SubmitSpinnerTagLib {

	def submitSpinner = { attrs, body ->

		def controller = attrs.controller
		def action = attrs.action
		def form = attrs.form
		def update = attrs.update
		def caption = attrs.caption
		def cssClass = attrs.cssClass
	
//		def classTXT
//		if (cssClass || !cssClass.trim.equals(""))
//			classTXT = "class=\"" + cssClass + "\""
//		else
//			classTXT = ""
		out << "<button class=\"${cssClass}\" onclick=\"executeSubmitSpinner('${controller}', '${action}', '${form}', '${update}')\">${caption}</button>"
//		out << "<button ${classTxt} onclick=\"executeSubmitSpinner('${controller}', '${action}', '${form}', '${update}')\">${caption}</button>"

	}
	
}
