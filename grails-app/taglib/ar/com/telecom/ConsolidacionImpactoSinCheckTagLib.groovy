package ar.com.telecom

import ar.com.telecom.pcs.entities.Justificacion
import ar.com.telecom.pcs.entities.ParametrizacionTipoPrueba
import ar.com.telecom.pcs.entities.TipoPrueba

class ConsolidacionImpactoSinCheckTagLib {
	def pedidoService
	
	def divsTiposPruebaSinCheck = { attrs, body ->
		def divId = attrs.id
		def detalle = attrs.detalle
		def pedido = attrs.pedido
		def pedidoHijo = attrs.pedidoHijo
		def impactoInstance = attrs.impactoInstance
		def usuarioLogueado = attrs.usuarioLogueado
		def puedeEditar = impactoInstance.puedeEditar(usuarioLogueado)
		def clase = attrs.clase

//		out << "<div id=\"headConsolidacion${divId}\" class=\"TablaConsolidacionImpactoItem\">"
//		out << "<table cellspacing=\"0\" cellpadding=\"0\">"
		out << "<tr class=\"${clase}\">"
		// out << "<td class=\"estrategiaPruebaCol1\"><img src=\"" + resource(dir: 'images/', file: 'RightTriangleIcon.gif') + "\" id=\"indicatorTipoPrueba$divId\" onClick=\"toggleDivs('contentTipoPrueba$divId')\" /></td>"
		out << "<td class=\"estrategiaPruebaCol2\">"
		out << detalle?.tipoPrueba?.descripcion
		out << "</td>"
		out << definirCheckBoxs(pedido, pedidoHijo, detalle, puedeEditar)
		out << "<td>"
		out << "</td>"
		out << "</tr>"
//		out << "</table>"
//		out << "</div>"
	}
	
	private def definirCheckBoxs(pedidoPadre, pedidoHijo, detalle, puedeEditar) {
		StringBuffer result = new StringBuffer("")

		def tipoPrueba = detalle?.tipoPrueba
		def descripcionTipoPrueba = tipoPrueba?.descripcion
		//def estrategiaPadre = pedidoPadre.estrategiaPruebaActual
		def estrategiaPadre = pedidoPadre.obtenerUltimaEspecificacion()
		//def tipoPruebaPadre = estrategiaPadre?.traerTipoPrueba(tipoPrueba)
		def tipoPruebaPadre = pedidoPadre.obtenerUltimaEspecificacion()
		
		log.debug "tipo gestion padre: " + pedidoPadre.tipoGestion
		log.debug "tipo impacto: " + pedidoHijo.tipoImpacto()
		def parametrizacion = pedidoService.consultarParametrizacion(pedidoPadre.tipoGestion, tipoPrueba, pedidoHijo.tipoImpacto())

		log.debug "Parametrizaci\u00F3n: " + parametrizacion

		boolean noAplicaPrueba = !estrategiaPadre || tipoPrueba.descripcion.equalsIgnoreCase(TipoPrueba.DESCRIPCION_PU)
		if (noAplicaPrueba) {
			result.append "<td class=\"estrategiaPruebaColBool\">N/A</td>"
			result.append "<td class=\"estrategiaPruebaColBool\">N/A</td>"
		} else {
			if (descripcionTipoPrueba.equalsIgnoreCase(TipoPrueba.DESCRIPCION_PI)) {
				result.append "<td class=\"estrategiaPruebaColBool\">"
				result.append checkBoxStatusImage(estado: estrategiaPadre.realizaPI)
				result.append "</td>"
				result.append "<td class=\"estrategiaPruebaColBool\">"
				result.append checkBoxStatusImage(estado: estrategiaPadre.PIintegrada)
				result.append "</td>"
			} else {
				if (descripcionTipoPrueba.equalsIgnoreCase(TipoPrueba.DESCRIPCION_PAU)) {
					result.append "<td class=\"estrategiaPruebaColBool\">"
					result.append checkBoxStatusImage(estado: estrategiaPadre.realizaPAU)
					result.append "</td>"
					result.append "<td class=\"estrategiaPruebaColBool\">"
					result.append checkBoxStatusImage(estado: estrategiaPadre.PAUintegrada)
					result.append "</td>"
				} else {
					def otraPrueba = pedidoPadre.obtenerTipoPruebaECS(tipoPrueba)
					
					if (otraPrueba) {
						result.append "<td class=\"estrategiaPruebaColBool\">"
						result.append checkBoxStatusImage(estado: true)
						result.append "</td>"
						result.append "<td class=\"estrategiaPruebaColBool\">"
						result.append checkBoxStatusImage(estado: otraPrueba?.seraConsolidada)
						result.append "</td>"
					} else {
						result.append "<td class=\"estrategiaPruebaColBool\">N/A</td>"
						result.append "<td class=\"estrategiaPruebaColBool\">N/A</td>"
					}
				}
			}
		}

		// Propio del pedido
		// si es consolidado = OR de los hijos
		// si es del hijo = la estrategia que viene en el detalle
		boolean seRealiza = detalle?.seRealiza
		boolean seConsolida = detalle?.seConsolida
		def parametrizacionEditable = parametrizacion?.puedeEditar()
		boolean estrategiaConsolidada = pedidoHijo.id.equals(pedidoPadre.id)
		if (estrategiaConsolidada) {
			log.info "estrategia consolidada"
			seRealiza = pedidoPadre.estrategiaConsolidadaRealiza(tipoPrueba)
			seConsolida = pedidoPadre.estrategiaConsolidadaConsolida(tipoPrueba)
			parametrizacionEditable = false
			def parametro = ParametrizacionTipoPrueba.SI_GRISADO
			if (seRealiza == null) {
				parametro = ParametrizacionTipoPrueba.NO_APLICA
			}
		}
		log.info "se realiza " + tipoPrueba + " : " + seRealiza
		log.info "se consolida " + tipoPrueba + " : " + seConsolida

		result.append armarCheckBox(seRealiza) //checkBoxStatusImage(estado: detalle.seRealiza)
		result.append armarCheckBox(seConsolida) //checkBoxStatusImage(estado: detalle.seConsolida)
		result.append "<td class=\"estrategiaPruebaColJustificacion\" >"
		if (puedeEditar && !estrategiaConsolidada) {
			result.append "<script type=\"text/javascript\">"
			result.append "jQuery(document).ready("
			result.append "function(\$) {"
			result.append "\$(\"#comboJustificacionNoPrueba_${detalle?.id}\").combobox();"
			result.append "});"
			result.append "</script>"
			result.append "<div id=\"divComboJustificacionNoPrueba_${detalle?.id}\" styRle=\"margin-left: 25px; width: 200px; ${!seRealiza?:'display: none;'}\">"
			result.append select( id:'comboJustificacionNoPrueba_' + detalle?.id, name: 'comboJustificacionNoPrueba_' + detalle?.id, from: Justificacion.findAllByTipoPrueba(tipoPrueba), class:'estrategiaPruebaColJustificacion', optionKey: 'id', value: detalle?.justificacionNoRealizacion?.id, style: definirVisibilidad(!detalle.seRealiza))
			result.append "</div>"
		} else {
			if (detalle?.justificacionNoRealizacion) {
				result.append "<p class: 'info'>" + detalle?.justificacionNoRealizacion + "</p>"
				def comboHiddenName = 'comboJustificacionNoPrueba_' + detalle?.id
				result.append g.hiddenField(name: comboHiddenName, id: comboHiddenName, value: detalle?.justificacionNoRealizacion?.id)
			}
//			def seRealizaHiddenName = 'chkRealiza_' + detalle?.id
//			def seConsolidaHiddenName = 'chkConsolida_' + detalle?.id
//			result.append g.hiddenField(name: "_"+seRealizaHiddenName, id: "_"+seRealizaHiddenName, value: detalle?.seRealiza?"on":"")
//			result.append g.hiddenField(name: "_"+seConsolidaHiddenName, id: "_"+seConsolidaHiddenName, value: detalle?.seConsolida?"on":"")
//			if (detalle?.seRealiza){
//				result.append g.hiddenField(name: seRealizaHiddenName, id: seRealizaHiddenName, value: detalle?.seRealiza?"on":"")
//			}
//			if (detalle?.seConsolida){
//				result.append g.hiddenField(name: seConsolidaHiddenName, id: seConsolidaHiddenName, value: detalle?.seConsolida?"on":"")
//			}
		}
//		result.append select( id:'comboJustificacionNoPrueba_' + detalle?.id, name: 'comboJustificacionNoPrueba_' + detalle?.id, from: Justificacion.findAllByTipoPrueba(tipoPrueba), class:'estrategiaPruebaColJustificacion', optionKey: 'id', value: detalle?.justificacionNoRealizacion?.id, style: definirVisibilidad(!detalle.seRealiza)) 
		result.append "</td>"

		result
	}

	private def armarCheckBox(estadoCheck) {
		StringBuffer result = new StringBuffer("")
		result.append "<td class=\"estrategiaPruebaColBool\">"
		result.append checkBoxStatusImage(estado: estadoCheck)
		result
	}
	
	def definirVisibilidad(seRealiza) {
		if (seRealiza) {
			return "display: visible;"	
		} else {
			return "display: none;"
		}
	}
}
