package ar.com.telecom.pedido
import ar.com.telecom.ume.UmeService
class PedidoTagLib {
def springSecurityService
def umeService
	def titulo = { attrs ->

		def pedido = attrs.pedido

//		out << "<div class='tituloPrincipal'>"
//		if (pedido?.id) {
//			out << "<label class='tituloPrincipal'>Pedido " + pedido.numero + " "
//			if (pedido?.isCancelado()) {
//				out << "<a title='" + pedido?.descripcionStatus + "' class='pedido_cancelado' href='#'>&nbsp;</a>"
//			}
//			if (pedido?.isSuspendido()) {
//				out << "<a title='" + pedido?.descripcionStatus + "' class='pedido_suspendido' href='#'>&nbsp;</a>"
//			}
//			out << " - " + pedido?.titulo
//			out << "</label>"
//		} else {
//			out << "Nuevo pedido"
//		}
//		out << "</div>"

		out << render(template: '/templates/tituloPrincipal', model: ['pedido': pedido])
	}

	def cabeceraPedido = { attrs ->
		def pedidoInstance = attrs.pedido
		def generaHiddenPedidoId = attrs.generaHiddenPedidoId

		out << "<p class='tituloSeccion'>Pedido Funcional</p>"
		out << "  <div class='formSeccion'>"
		if (params?.controller.equals(pedidoInstance?.faseRegistracionPedido()?.controllerNombre)) {
			out << "   <div class='pDos'>"
			out << "     <label class='formLabel2'>Usuario Final</label>"
			out << "     <div class='boxUser'>"
			out << render(template: '/templates/userBox', model: ['person': pedidoInstance?.legajoUsuarioCreador, 'box': 'legajoUsuarioCreador'])
			out << "     </div>"
			out << "   </div>"
			out << "<div class='pDos'>"
			if (params?.controller.equals(pedidoInstance?.faseValidacionPedido()?.controllerNombre)){
				out << "<label class='formLabel2'>Gerente Usuario</label>"
				out << "<div class='boxUser'>"
				out << render(template: '/templates/userBox', model: ['person': pedidoInstance?.legajoGerenteUsuario, 'box': 'legajoGerenteUsuarioCabecera'])
			}else{
				out << "<label class='formLabel2'>Interlocutor Usuario</label>"
				out << "<div class='boxUser'>"
				out << render(template: '/templates/userBox', model: ['person': pedidoInstance?.legajoInterlocutorUsuario, 'box': 'legajoInterlocutorUsuarioCabecera'])
			}
			out << "</div>"
			out << "</div>"
		}
		if (!generaHiddenPedidoId || (generaHiddenPedidoId && generaHiddenPedidoId.toBoolean())){
			out << "<input type=\"hidden\" name=\"pedidoId\" value=\"" + pedidoInstance?.id + "\"/>"
		}
		
		out << "<div class='pTres'>"
		out << "<label class='formLabel'>Macroestado</label>"
		def macroEstado = pedidoInstance?.faseActual?.macroEstado
		out << "<p class='info'>" +  macroEstado?.descripcion + "</p>"
		out << "</div>"
		out << "<div class='pDos sinMargenLeft'>"
		out << "<label class='formLabel'>Tipo de Pedido</label>"
		out << "<p class='info'>" + pedidoInstance?.tipoPedido?.descripcion + "</p>"
		out << "</div>"
		out << "<div class='pTres sinMargenLeft'>"
		out << "<label class='formLabel'>Objetivo</label>"
		out << "<div class='info'>" + pedidoInstance?.objetivo + "</div>"
		out << "</div>"
		out << "<div class='pTres'>"
		out << "<label class='formLabel'>Descripci&oacute;n del pedido</label>"
		out << "<div class='info'>" + pedidoInstance?.descripcion + "</div>"
		out << "</div>"
		out << "<div class='pTres'>"
		out << "<label class='formLabel'>Alcance</label>"
		out << "<div class='info'>" + pedidoInstance?.alcance + "</div>"
		out << "</div>"
		out << "<div class='pDos sinMargenLeft'>"
		out << "<label class='formLabel'>Direcci&oacute;n</label>"
		out << "<div class='info'>" + pedidoInstance?.getDescripcionDireccionGerencia() + "</div>"
		out << "</div>"
		out << "</div>"
		
		out << render(template: '/suspension/suspensionInfo', model: ['pedido': pedidoInstance])
	}

//	def indicadorEstadoFasePedido = { attrs ->
//		def oPedido = attrs.pedido
//		def oImpacto = attrs.impacto
//		def oFase = attrs.fase
//		out << render(template: '/templates/indicadorEstadoPedido', model: ['pedido': oPedido, 'fase': oFase, 'impacto': oImpacto])
//	}

	def indicadorEstadoPedido = { attrs ->

		def tengoAlgo = false
		def sImg
		def sText = attrs.pedido.descripcionStatus
		def descripcion
		
		if (attrs.pedido.isSuspendido()){
			sImg = resource(dir: 'images/icons', file: 'warning.png')
			descripcion = "Suspendido"
			tengoAlgo = true
		}
		if (attrs.pedido.isCancelado()){
			sImg = resource(dir: 'images/icons', file: 'exclamation.png')
			descripcion = "Cancelado"
			tengoAlgo = true
		}
		if (attrs.pedido.isEnReplanificacion()){
			sImg = resource(dir: 'images/icons', file: 'date_edit.png')
			descripcion = "En Replanificación"
			tengoAlgo = true
		}
		if (attrs.pedido.isCerrado()){
			sImg = resource(dir: 'images/icons', file: 'accept.png')
			descripcion = "Finalizado"
			tengoAlgo = true
		}

		if (tengoAlgo) {
			out << "- "+ descripcion + "  <img src='${sImg}' height='16' width='16' title='${sText}'/>"
		}
	}
	
	def puedeEditarRegistroPedido = { attrs ->
		
		out <<  umeService.puedeRegistrarPedido() 
	}

}
