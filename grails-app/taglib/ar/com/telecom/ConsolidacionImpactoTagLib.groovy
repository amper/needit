package ar.com.telecom

import ar.com.telecom.pcs.entities.Justificacion
import ar.com.telecom.pcs.entities.ParametrizacionTipoPrueba
import ar.com.telecom.pcs.entities.TipoPrueba

class ConsolidacionImpactoTagLib {
	def pedidoService

	def divsTiposPrueba = { attrs, body ->

		def divId = attrs.id
		def detalle = attrs.detalle
		def pedido = attrs.pedido
		def pedidoHijo = attrs.pedidoHijo
		def impactoInstance = attrs.impactoInstance
		def usuarioLogueado = attrs.usuarioLogueado
		def puedeEditar = impactoInstance.puedeEditar(usuarioLogueado)
		def clase = attrs.clase

//		out << "<div id=\"headConsolidacion${divId}\" class=\"TablaConsolidacionImpactoItem\">"
//		out << "<table cellspacing=\"0\" cellpadding=\"0\">"
		out << "<tr class=\"${clase}\">"
		out << "<td class=\"estrategiaPruebaCol2\">"
		out << detalle?.tipoPrueba?.descripcion
		out << "</td>"
		out << definirCheckBoxs(pedido, pedidoHijo, detalle, puedeEditar)
		out << "<td style=\"width: 5% !important;\" class=\"${clase}\">&nbsp;"

		//boolean padreTieneTipoPrueba = !pedido.tieneTipoPrueba(detalle.tipoPrueba)
		//if (detalle?.id && detalle.tipoPrueba.opcional && puedeEditar && (!padreTieneTipoPrueba || !pedido.esHijo())) {
		
		boolean ecsTieneTipoPrueba = (pedidoHijo.obtenerUltimaEspecificacion().getTipoPrueba(detalle.tipoPrueba)!=null)
		if (detalle?.id && detalle.tipoPrueba.opcional && puedeEditar && pedidoHijo.esHijo() && !ecsTieneTipoPrueba) {
			out << secureRemoteLink (action: "eliminarOtraPrueba", title: "Eliminar", class: "limpiarBox", style: "margin-left: 0px !important;", id: "eliminaPrueba_" + detalle.id, update: "tablaPruebasCompletas", before: "guardarPedido();", params: [pedidoId: pedido.id, pedidoHijoId: pedidoHijo.id, detalleId: detalle.id, impacto: impactoInstance.id, usuarioLogueado: attrs.usuarioLogueado])
		}
		
		out << "</td>"
		out << "</tr>"
//		out << "</table>"
//		out << "</div>"
	}

	private def definirCheckBoxs(pedidoPadre, pedidoHijo, detalle, puedeEditar) {
		log.info "*********** definirCheckBoxs"
		StringBuffer result = new StringBuffer("")

		def tipoPrueba = detalle?.tipoPrueba
		def descripcionTipoPrueba = tipoPrueba?.descripcion
		def estrategiaPadre = pedidoPadre.obtenerUltimaEspecificacion()
		def parametrizacion = pedidoService.consultarParametrizacion(pedidoPadre.tipoGestion, tipoPrueba, pedidoHijo.tipoImpacto())

		log.info "tipo gestion padre: " + pedidoPadre.tipoGestion
		log.info "tipo impacto: " + pedidoHijo.tipoImpacto()
		log.info "Parametrizaci\u00F3n: " + parametrizacion

		// Estrategia del padre
		boolean noAplicaPrueba = !estrategiaPadre || tipoPrueba.descripcion.equalsIgnoreCase(TipoPrueba.DESCRIPCION_PU)
		if (noAplicaPrueba) {
			result.append "<td class=\"estrategiaPruebaColBool\">N/A</td>"
			result.append "<td class=\"estrategiaPruebaColBool\">N/A</td>"
		} else {
			if (descripcionTipoPrueba.equalsIgnoreCase(TipoPrueba.DESCRIPCION_PI)) {
				result.append "<td class=\"estrategiaPruebaColBool\">"
				result.append checkBoxStatusImage(estado: estrategiaPadre.realizaPI)
				result.append "</td>"
				result.append "<td class=\"estrategiaPruebaColBool\">"
				result.append checkBoxStatusImage(estado: estrategiaPadre.PIintegrada)
				result.append "</td>"
			} else {
				if (descripcionTipoPrueba.equalsIgnoreCase(TipoPrueba.DESCRIPCION_PAU)) {
					result.append "<td class=\"estrategiaPruebaColBool\">"
					result.append checkBoxStatusImage(estado: estrategiaPadre.realizaPAU)
					result.append "</td>"
					result.append "<td class=\"estrategiaPruebaColBool\">"
					result.append checkBoxStatusImage(estado: estrategiaPadre.PAUintegrada)
					result.append "</td>"
				} else {
					def otraPrueba = pedidoPadre.obtenerTipoPruebaECS(tipoPrueba)
					
					if (otraPrueba) {
						result.append "<td class=\"estrategiaPruebaColBool\">"
						result.append checkBoxStatusImage(estado: true)
						result.append "</td>"
						result.append "<td class=\"estrategiaPruebaColBool\">"
						result.append checkBoxStatusImage(estado: otraPrueba?.seraConsolidada)
						result.append "</td>"
					} else {
						result.append "<td class=\"estrategiaPruebaColBool\">N/A</td>"
						result.append "<td class=\"estrategiaPruebaColBool\">N/A</td>"
					}
				}
			}
		}
		// Propio del pedido
		// si es consolidado = OR de los hijos
		// si es del hijo = la estrategia que viene en el detalle
		boolean seRealiza = detalle?.seRealiza
		boolean seConsolida = detalle?.seConsolida
		def parametrizacionEditable = true
		if (parametrizacion) {
			parametrizacionEditable = parametrizacion.puedeEditar()
		}
		boolean estrategiaConsolidada = pedidoHijo.id.equals(pedidoPadre.id)
		if (estrategiaConsolidada) {
			log.info "estrategia consolidada"
			seRealiza = pedidoPadre.estrategiaConsolidadaRealiza(tipoPrueba)
			seConsolida = pedidoPadre.estrategiaConsolidadaConsolida(tipoPrueba)
			parametrizacionEditable = false
			def parametro = ParametrizacionTipoPrueba.SI_GRISADO
			if (seRealiza == null) {
				parametro = ParametrizacionTipoPrueba.NO_APLICA
			}
			result.append armarCheckBox(parametro, seRealiza, 'chkRealiza_' + detalle?.id, detalle.id, true, seRealiza)
			result.append armarCheckBox(parametro, seConsolida, 'chkConsolida_' + detalle?.id, detalle.id, false, seRealiza)
		} else {
			result.append armarCheckBox(parametrizacion?.estrategiaRealiza, seRealiza, 'chkRealiza_' + detalle?.id, detalle.id, true, seRealiza)
			result.append armarCheckBox(parametrizacion?.estrategiaConsolidacion, seConsolida, 'chkConsolida_' + detalle?.id, detalle.id, false, seRealiza)
		}
		log.info "se realiza " + tipoPrueba + " : " + seRealiza
		log.info "se consolida " + tipoPrueba + " : " + seConsolida

		// Columna justificación
		result.append "<td class=\"estrategiaPruebaColJustificacion\">"
		if (seRealiza != null && !estrategiaConsolidada) {
//			if (!parametrizacionEditable) {
//				parametrizacionEditable = true
//			}
			def muestraJustificacion = puedeEditar && parametrizacionEditable
			if (muestraJustificacion) {
				result.append "<script type=\"text/javascript\">"
				result.append "jQuery(document).ready("
				result.append "function(\$) {"
				result.append "\$(\"#comboJustificacionNoPrueba_${detalle?.id}\").combobox();"
				result.append "});"
				result.append "</script>"
				result.append "<div id=\"divComboJustificacionNoPrueba_${detalle?.id}\" style=\"width: 100%; ${!seRealiza?:'display: none;'}\">"
				result.append select( id:'comboJustificacionNoPrueba_' + detalle?.id, name: 'comboJustificacionNoPrueba_' + detalle?.id, from: Justificacion.findAllByTipoPrueba(tipoPrueba), class:'estrategiaPruebaColJustificacion', optionKey: 'id', value: detalle?.justificacionNoRealizacion?.id, style: definirVisibilidad(!detalle.seRealiza) + "; width: 100px !important; heigth: 25px !important; font-family: Verdana,Arial,sans-serif; font-size: 1em;")
				result.append "</div>"
			} else {
				if (detalle?.justificacionNoRealizacion) {
					result.append "<p class: 'info'>" + detalle?.justificacionNoRealizacion + "</p>"
					def comboHiddenName = 'comboJustificacionNoPrueba_' + detalle?.id
					result.append g.hiddenField(name: comboHiddenName, id: comboHiddenName, value: detalle?.justificacionNoRealizacion?.id)
				}
//				def seRealizaHiddenName = 'chkRealiza_' + detalle?.id
//				def seConsolidaHiddenName = 'chkConsolida_' + detalle?.id
//				result.append g.hiddenField(name: "_"+seRealizaHiddenName, id: "_"+seRealizaHiddenName, value: detalle?.seRealiza?"on":"")
//				result.append g.hiddenField(name: "_"+seConsolidaHiddenName, id: "_"+seConsolidaHiddenName, value: detalle?.seConsolida?"on":"")
//				if (detalle.seRealiza){
//					result.append g.hiddenField(name: seRealizaHiddenName, id: seRealizaHiddenName, value: detalle?.seRealiza?"on":"")
//				}
//				if (detalle.seRealiza && detalle.seConsolida){
//					result.append g.hiddenField(name: seConsolidaHiddenName, id: seConsolidaHiddenName, value: detalle?.seConsolida?"on":"")
//				}
			}
		}
		result.append "</td>"

		result
	}

	private def armarCheckBox(estrategia, valor, controlId, detalleId, conOnChange, seRealiza) {
		StringBuffer result = new StringBuffer("")
		result.append "<td class=\"estrategiaPruebaColBool\">"
		if (!estrategia) {
			// asumo que son otras pruebas
			estrategia = ParametrizacionTipoPrueba.SI_EDITABLE
		}
		log.info "estrategia parametrizacion: " + estrategia
		if (estrategia.equalsIgnoreCase(ParametrizacionTipoPrueba.NO_APLICA)) {
			result.append "N/A"
		}
		if (estrategia.equalsIgnoreCase(ParametrizacionTipoPrueba.SI_GRISADO) || estrategia.equalsIgnoreCase(ParametrizacionTipoPrueba.NO_GRISADO)) {
			result.append checkBoxStatusImage(estado: valor)
		}
		if (conOnChange) {
			if (estrategia.equalsIgnoreCase(ParametrizacionTipoPrueba.SI_EDITABLE) || estrategia.equalsIgnoreCase(ParametrizacionTipoPrueba.NO_EDITABLE)) {
				result.append g.checkBox(name: controlId, value: valor, onclick: "cambiarJustificacion('" + detalleId + "');")
			}
		} else {
			if (estrategia.equalsIgnoreCase(ParametrizacionTipoPrueba.SI_EDITABLE) || estrategia.equalsIgnoreCase(ParametrizacionTipoPrueba.NO_EDITABLE)) {
				result.append g.checkBox(name: controlId, value: valor, style: definirVisibilidad(seRealiza))
			}
		}
		result
	}

	def definirVisibilidad(seRealiza) {
		if (seRealiza) {
			return "display: visible;"
		} else {
			return "display: none;"
		}
	}
}
