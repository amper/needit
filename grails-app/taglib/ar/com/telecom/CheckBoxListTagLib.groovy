package ar.com.telecom

import java.awt.ItemSelectable;


class CheckBoxListTagLib {

	def checkBoxList = { attrs ->
		def listadoItems = attrs.items
		def nombreCheck = attrs.name
		def seleccionados = attrs.seleccionados
		def optionKey = attrs.optionKey
		def height = attrs.height
		
		//TODO: Hacer el filtro de roles
		
		out << "<div id=\"scroll_checkboxes_container\">"
		if (height)
			out << "<div id=\"scroll_checkboxes\" style=\"height: ${height}px;\">"
		else
			out << "<div id=\"scroll_checkboxes\">"
		listadoItems.eachWithIndex {
			itemCheck, counter  ->
				out << "<label>"

				boolean isChecked = false
				
				//Primero lo convertimos a String para que no de error de Null en isInteger
				if (!seleccionados?.toString()?.isInteger()){
					seleccionados.each { 
					itemSelect ->
						if (itemSelect.trim().isInteger()){
							if (itemCheck.id.toInteger().equals(itemSelect.trim().toInteger())) {
								isChecked = true
							}
						}else{
							if (itemCheck."$optionKey".equals(itemSelect.trim())) {
								isChecked = true
							}
						}
					}
				}else{
					if (itemCheck.id.toInteger().equals(seleccionados.trim().toInteger()))
						isChecked = true
				}
				out << checkBox(id: nombreCheck + "_" + (counter+1), name: nombreCheck, value: itemCheck?."$optionKey", checked: isChecked)
				out << itemCheck?.descripcion
				out << "</label><br />"
		}
		out << "</div>"
		out << "<div id=\"scroll_checkboxesMenu\">"
		out << "<span>"
		out << "Seleccionar: "
		out << "<a href=\"javascript:seleccion('$nombreCheck', ${listadoItems?.size}, true);\">TODOS</a>"
		out << " | " 
		out << "<a href=\"javascript:seleccion('$nombreCheck', ${listadoItems?.size}, false);\">NINGUNO</a>"
		out << "</span>"
		out << "</div>"
		out << "</div>"
	}
	
}
