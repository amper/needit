package ar.com.telecom

class CheckBoxStatusTagLib {

	def checkBoxStatusImage = {
		attrs -> def estado = attrs.estado
		if (estado!=null) {
			if (estado)
				out << "<img src='${resource(dir:'images/icons/',file:'tick.png')}' width='16' height='16' border='0' align='middle'></></img>"
			else
				out << "<img src='${resource(dir:'images/icons/',file:'cross.png')}' width='16' height='16' border='0' align='middle'></img>"
		} else {
			out << "N/A"
		}
	}
	
}
