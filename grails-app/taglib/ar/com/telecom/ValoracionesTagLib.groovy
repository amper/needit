package ar.com.telecom

import ar.com.telecom.util.NumberUtil


class ValoracionesTagLib {

	def divsValoraciones = { attrs, body ->

		def divId = attrs.id
		def detalle = attrs.detalle
		
		out << "<div id=\"headValoracion$divId\" class=\"TablaValidacionImpactoItem\" onClick=\"toggleDivs('contentValoracion$divId')\">"
		out << "<table style=\"border: 1px; width: 100%;\" cellspacing=\"0\" cellpadding=\"0\"><tr>"
		out << "<td width=\"5%\"><div class=\"indicatorValoracion\" id=\"indicatorValoracion$divId\">&nbsp;</div></td>"
		out << "<td width=\"55%\">${detalle.codigoAgrupador}</td>"
		out << "<td width=\"20%\" style=\"text-align: right;\">${detalle.totalHoras}</td>" 
		out << "<td style=\"text-align: right;\" width=\"20%\"> " + NumberUtil.toString(detalle.totalMontoHoras) + "</td>"
		out << "</tr></table>"
		out << "</div>"
		
		out << "<div id=\"contentValoracion$divId\" style=\"width: 100%; display:none\">"
		out << "<table style=\"border: none; width: 100%;\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">"
		
		detalle.detalles.each{
//			def mostrar = true
//			if (it.descripcion.equals("Horas de coordinación") && it.totalHoras == 0 ){
//				mostrar = false
//			}
//			if (mostrar){
				out << "<tr>"
				out << "<td align=\"right\" width=\"60%\">${it.descripcion}</td>"
				out << "<td style=\"text-align: right\" width=\"20%\">${it.totalHoras}</td>"
				out << "<td style=\"text-align: right;\" width=\"20%\"> " + NumberUtil.toString(it.totalMontoHoras) + "</td>"
				out << "</tr>"
//			}
		}
		
		out << "</table>"
		out << "</div>"
		out << "<script type=\"text/javascript\">"
		out << "animatedcollapse.addDiv('contentValoracion$divId', 'fade=1');"
		out << "</script>"
	}
	
	def divsOtrosCostos = { attrs, body ->

		def divId = attrs.id
		def costos = attrs.otroCostos
		
		out << "<div id=\"headValoracion$divId\" class=\"TablaValidacionImpactoItem\" onClick=\"toggleDivs('contentValoracion$divId')\">"
		out << "<table style=\"border: 1px; width: 100%;\" cellspacing=\"0\" cellpadding=\"0\"><tr>"
		out << "<td width=\"5%\"><div class=\"indicatorValoracion\" id=\"indicatorValoracion$divId\">&nbsp;</div></td>"
		out << "<td>${costos.descripcion}</td>"
		out << "<td style=\"text-align: right;\" >\$ ${NumberUtil.toString(costos.total)}</td>"
		out << "</tr></table>"
		out << "</div>"
		
		out << "<div id=\"contentValoracion$divId\" style=\"width: 100%; display:none\">"
		out << "<table style=\"border: none; width: 100%;\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">"
		
		costos.items.each{
			out << "<tr width=\"100%\">"
			out << "<td align=\"right\" width=\"50%\">${it.descripcion}</td>"
			out << "<td align=\"right\" width=\"30%\">${it.detalle}</td>"
			out << "<td style=\"text-align: right;\" width=\"20%\">\$ " + NumberUtil.toString(it.costo) + "</td>"
			out << "</tr>"
		}
		
		out << "</table>"
		out << "</div>"
		out << "<script type=\"text/javascript\">"
		out << "animatedcollapse.addDiv('contentValoracion$divId', 'fade=1');"
		out << "</script>"
	}

	def divsAreaSoporte = { attrs, body ->
		
		def divId = attrs.id
		def areas = attrs.areaSoportes
		def realesIncurridos = attrs.realesIncurridos 
		if(realesIncurridos){
			out << "<div id=\"headValoracion$divId\" class=\"TablaValidacionImpactoItem\" onClick=\"toggleDivs('contentValoracion$divId')\">"
			out << "<table style=\"border: 1px; width: 100%;\" cellspacing=\"0\" cellpadding=\"0\"><tr>"
			out << "<td width=\"3%\"><div class=\"indicatorValoracion\" id=\"indicatorValoracion$divId\">&nbsp;</div></td>"
			out << "<td width=\"25%\">${areas.descripcion}</td>"
			out << "<td width=\"15%\">" + formatDate(date: areas.fechaDesde, type: "date", style: "MEDIUM") + "</td>"
			out << "<td width=\"12%\">" + formatDate(date: areas.fechaHasta, type: "date", style: "MEDIUM") + "</td>"
			out << "<td width=\"16%\" style=\"text-align: right;\">${areas.total}</td>"
			out << "<td width=\"20%\" style=\"text-align: right;\">${areas.totalMontoHoras}</td>"
			out << "<td style=\"text-align: right;\" width=\"10%\">${areas.totalHorasIncurridas}</td>"
		}else{
			out << "<div id=\"headValoracion$divId\" class=\"TablaValidacionImpactoItem\" onClick=\"toggleDivs('contentValoracion$divId')\">"
			out << "<table style=\"border: 1px; width: 100%;\" cellspacing=\"0\" cellpadding=\"0\"><tr>"
			out << "<td width=\"5%\"><div class=\"indicatorValoracion\" id=\"indicatorValoracion$divId\">&nbsp;</div></td>"
			out << "<td width=\"40%\">${areas.descripcion}</td>"
			out << "<td width=\"15%\">" + formatDate(date: areas.fechaDesde, type: "date", style: "MEDIUM") + "</td>"
			out << "<td width=\"12%\">" + formatDate(date: areas.fechaHasta, type: "date", style: "MEDIUM") + "</td>"
			out << "<td width=\"16%\" style=\"text-align: right;\">${areas.total}</td>"
			out << "<td width=\"20%\" style=\"text-align: right;\">${areas.totalMontoHoras}</td>"
		}
		out << "</tr></table>"
		out << "</div>"
		
		out << "<div id=\"contentValoracion$divId\" style=\"width: 100%; display:none\">"
		out << "<table style=\"border: none; width: 100%;\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">"
		
		areas.items.each{
			if(realesIncurridos){
				out << "<tr width=\"100%\">"
				out << "<td align=\"right\" width=\"22%\">${it.tarea}</td>"
				out << "<td align=\"center\" width=\"10%\">" + formatDate(date: it.fechaDesde, type: "date", style: "MEDIUM") + "</td>"
				out << "<td align=\"center\" width=\"10%\">" + formatDate(date: it.fechaHasta, type: "date", style: "MEDIUM") + "</td>"
				out << "<td style=\"text-align: center;\" width=\"7%\">${it.totalHoras}</td>"
				out << "<td style=\"text-align: right;\" width=\"10%\">${it.totalMontoHoras}</td>"
				out << "<td style=\"text-align: right;\" width=\"5%\">${it.totalHorasIncurridas}</td>"
			}else{
				out << "<tr width=\"100%\">"
				out << "<td align=\"right\" width=\"35%\">${it.tarea}</td>"
				out << "<td align=\"center\" width=\"17%\">" + formatDate(date: it.fechaDesde, type: "date", style: "MEDIUM") + "</td>"
				out << "<td align=\"center\" width=\"15%\">" + formatDate(date: it.fechaHasta, type: "date", style: "MEDIUM") + "</td>"
				out << "<td style=\"text-align: right;\" width=\"10%\">${it.totalHoras}</td>"
				out << "<td style=\"text-align: right;\" width=\"10%\">${it.totalMontoHoras}</td>"
			}
			out << "</tr>"
		}
		
		out << "</table>"
		out << "</div>"
		out << "<script type=\"text/javascript\">"
		out << "animatedcollapse.addDiv('contentValoracion$divId', 'fade=1');"
		out << "</script>"
	}
		
	
}
