package ar.com.telecom

import ar.com.telecom.pedido.consulta.Consulta

class InboxTagLib {

	def divsInbox = { attrs, body ->

		def i = attrs.i
		def pedido = attrs?.pedido
		def divId = pedido?.id
		def tipoConsulta = attrs?.tipoConsulta
		def usuarioLogueado = attrs?.usuarioLogueado 

		def pedidoHijosConAprobacionesPendientes
		if ([Consulta.MIS_PENDIENTES, Consulta.PENDIENTES_MI_GRUPO, Consulta.INBOX_APROBACIONES].contains(tipoConsulta)) {
			pedidoHijosConAprobacionesPendientes = new HashSet(pedido?.pedidosHijosConAprobacionesPendientes(usuarioLogueado, tipoConsulta) +
				pedido?.actividadesPendientesPara(usuarioLogueado))
		} else {
			pedidoHijosConAprobacionesPendientes = new HashSet(pedido?.pedidosHijosEnSeguimiento(usuarioLogueado))
		}
		
		def link = g.createLink(id: pedido.id,params: [pedidoId: pedido.id], controller: pedido?.faseActual?.controllerNombre)
		
		out << "<tr class=\"${(i % 2) == 0 ? 'odd' : 'even'}\" style=\"cursor: pointer;\">"
		if (pedido?.getPedidoHijos() && pedidoHijosConAprobacionesPendientes){
			out << "<td onClick=\"expandItem('contentValoracion$divId')\"><div class=\"indicatorValoracion-close\" id=\"indicatorValoracion$divId\">&nbsp;</div>"
		}else{
			out << "<td onClick=\"javascript:window.location = '${link}';\">&nbsp;"
		}
		out << "</td>"
		out << "<td onclick=\"javascript:window.location = '${link}';\">"
		out << "<b>"
		out << g.link(id:pedido.id, params: [pedidoId: pedido.id] , controller: pedido?.faseActual?.controllerNombre, {pedido.id})
		out << "</b>"
		out << "</td>"
		out << "<td onclick=\"javascript:window.location = '${link}';\">${pedido?.titulo}</td>"
		out << "<td onclick=\"javascript:window.location = '${link}';\">${pedido?.faseActual}</td>"
		out << "<td onclick=\"javascript:window.location = '${link}';\">${pedido?.faseActual?.macroEstado}</td>"
		def rolPadre = pedido.getRol(pedido?.faseActual, usuarioLogueado)
		if (rolPadre){ 
			out << "<td onclick=\"javascript:window.location = '${link}';\">${rolPadre}</td>"
		}else{
			out << "<td onclick=\"javascript:window.location = '${link}';\">&nbsp;</td>"
		}
		if (![Consulta.MIS_PENDIENTES, Consulta.EN_SEGUIMIENTO_PROPIOS, Consulta.PENDIENTES_MI_GRUPO, Consulta.INBOX_APROBACIONES].contains(tipoConsulta)) {
			out << "<td>"
			if (pedido?.isCancelado()) {
				out << "<a title='" + pedido?.descripcionStatus + "' class='pedido_cancelado' href='#'>&nbsp;</a>"
			}
			if (pedido?.isSuspendido()) {
				out << "<a title='" + pedido?.descripcionStatus + "' class='pedido_suspendido' href='#'>&nbsp;</a>"
			}
			if (pedido?.isCerrado()) {
				out << "<a title='" + pedido?.descripcionStatus + "' class='pedido_cerrado' href='#'>&nbsp;</a>"
			}
			
			out << "</td>"
		}

		out << "<td onclick=\"javascript:window.location = '${link}';\">&nbsp;</td>"

		out << "</tr>"

		if (pedido?.getPedidoHijos()){
			out << "<tr style=\"cursor: pointer;\"><td style=\"border: none; padding: 0px;\" colspan=\"7\">"
			out << "<div id=\"contentValoracion$divId\" style=\"display: none;\" >"
			out << "<table cellspacing=\"0\" cellpadding=\"0\" style=\"border: none;\">"
			def pedidos = pedidoHijosConAprobacionesPendientes
			pedidos = pedidos.sort{it.getNumero()}
			
			pedidos.eachWithIndex {
				pedidoHijo, iHijo  ->
				
				//FED
				//out << "<tr style=\"background-color: ${(iHijo % 2) == 0 ? '#EEE' : '#DDD'}\">"
				//FED
				//Pone esto en los TD para que genere el link
				//onclick=\"javascript:window.location = '${createLink(id: pedidoHijo.id,params: [pedidoId: pedidoHijo.id], controller: pedidoHijo?.faseActual?.controllerNombre)}';\"
				out << "<tr class=\"${(iHijo % 2) == 0 ? 'fondoPedidoHijo':'fondoCelda2'}\" onclick=\"javascript:window.location = '${createLink(id: pedido.id,params: [pedidoId: pedido.id, impacto: pedidoHijo.id], controller: pedidoHijo?.faseActual?.controllerNombre)}';\">"
				out << "<td class=\"item0\">&nbsp;</td>"
				out << "<td class=\"item1\" >"
				out << pedidoHijo.getNumero()
				out << "</td>"
				out << "<td class=\"item2\">"
				out << pedidoHijo?.titulo
				out << "</td>"
	//			out << "<td class=\"item2\">"
	//			out << pedidoHijo?.actividades
	//			out << "</td>"
				out << "<td class=\"item3\">"
				out << pedidoHijo?.faseActual
				out << "</td>"
				out << "<td class=\"item4\">"
				out << pedidoHijo?.faseActual?.macroEstado
				out <<"</td>"
				out << "<td class=\"item5\" title=\"Sistema - Tipo de impacto\">"
				out << pedidoHijo?.getTipoPlanificacion()
				out << "</td>"
				out << "<td class=\"item6\">"
				def rolHijo = pedidoHijo.getRol(pedidoHijo?.faseActual, usuarioLogueado)
				if (rolHijo){
					out << rolHijo
				}else{
					out << "&nbsp;"
				}
				out << "</td>"
				out << "</tr>"
			
			}
			out << "</table>"
			out << "</div>"
			out << "</tr>"
			
			out << "<script type=\"text/javascript\">"
			out << "animatedcollapse.addDiv('contentValoracion$divId', 'fade=1');"
			out << "</script>"
		}
		
		
	}
	
}
