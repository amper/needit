package ar.com.telecom.mobile

class MobileTagLib {

	static namespace = "web"

	def webUserAgentService

	def isBlackberry = { attrs, body ->
		if (webUserAgentService.isBlackberry()) {
			out << body()
		}
	}

	def isNOTBlackberry = { attrs, body ->
		if (!webUserAgentService.isBlackberry()) {
			out << body()
		}
	}

	def isMsie = { attrs, body ->
		if (webUserAgentService.isMsie()) {
			out << body()
		}
	}
	
	def isNOTMsie = { attrs, body ->
		if (!webUserAgentService.isMsie()) {
			out << body()
		}
	}

	def isFirefox = { attrs, body ->
		if (webUserAgentService.isFirefox()) {
			out << body()
		}
	}

	def isChrome = { attrs, body ->
		if (webUserAgentService.isChrome()) {
			out << body()
		}
	}
}
