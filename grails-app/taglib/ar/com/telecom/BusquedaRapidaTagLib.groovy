package ar.com.telecom

import org.codehaus.groovy.grails.commons.ConfigurationHolder

class BusquedaRapidaTagLib {

	def busquedaRapida = { attrs, body ->
		
		def appName = ConfigurationHolder.config.appName
		def name = attrs.name
		def methodName = attrs.methodName
		def nameField = name + "Field"
		def valorField = ""
		def valorId = ""
		if (attrs.value){
			valorField = attrs.value
			valorId = attrs.value?.id
		} 
		
		def mapName = "mapa_" + name
		def pedidoId = attrs.pedidoId
		
		out << "<script language=\"javascript\">"
		out << "jQuery(document).ready(function() {"
		out << "var t;"
			
		out << "jQuery(\"#${nameField}\").autocomplete({"
		out << "source: new Array(),"
		out << "select: onAutoCompleteSelected,"
		out << "focus: function() { return false; }"
		out << "});"
			
		out << "function serviceCall() {"
		if (pedidoId)
			out << "jQuery.getJSON('/${appName}/registroPedido/busquedaAutocomplete?pedidoId=${pedidoId}&methodName=${methodName}&word='+jQuery(\"#${nameField}\").val(), function(data) {"
		else
			out << "jQuery.getJSON('/${appName}/registroPedido/busquedaAutocomplete?methodName=${methodName}&word='+jQuery(\"#${nameField}\").val(), function(data) {"
		out << "var result = new Array();"
		out << "jQuery.each(data, function(index, item) {"
		out << "result.push({label: item.descripcion, value: item.id});"
		out << "});"
		out << "jQuery(\"#${nameField}\").autocomplete({"
		out << "source: result,"
		out << "select: onAutoCompleteSelected,"
		out << "focus: function() { return false; }"
		out << "});"
		out << "jQuery(\"#${nameField}\").autocomplete(\"search\");"
		out << "});"
		out << "}"
			
		out << "function onAutoCompleteSelected(event, ui) {"
		out << "jQuery(\"#${nameField}\").val(ui.item.label);"
		out << "jQuery(\"#${name}Id\").val(ui.item.value);"
		out << "jQuery(\"#${mapName}\").val('[id:' + ui.item.value + ']');"
		out << "return false;"
		out << "}"

		out << "jQuery(\"#${nameField}\").keyup(function(event) {"
		out << "var isAlphaNumeric = String.fromCharCode(event.keyCode).match(/[A-Za-z0-9]+/g);"
		out << "if(isAlphaNumeric || event.keyCode == 8) {"
		out << "clearTimeout(t);"
		out << "t = setTimeout( serviceCall, 200);"		
		out << "}"
		out << "});"
		out << "});"
		out << "</script>"

		out << "<input type=\"text\" id=\"${nameField}\" name=\"${nameField}\" value=\"${valorField}\" />" //class=\"formInput\" 
		out << "<input type=\"hidden\" id=\"${name}Id\" name=\"${name}.id\" value=\"${valorId}\" />"
		out << "<input type=\"hidden\" id=\"${mapName}\" name=\"${mapName}\" value=\"\" />"
		
		/* HTML
		 
			<script language="javascript">
			jQuery(document).ready(function() {
				var t;
				
				jQuery("#prueba").autocomplete({
					source: new Array(),
					select: onAutoCompleteSelected,
				    focus: function() { return false; }
				});
				
				function serviceCall() {
					jQuery.getJSON('/needIt/workflow/busquedaAutocomplete?methodName=obtenerSistemas&word='+jQuery("#prueba").val(), function(data) {
						var result = new Array();
					    jQuery.each(data, function(index, item) {
					       result.push({label: item.descripcion, value: item.id});
					    });
					    jQuery("#prueba").autocomplete({
						    source: result,
						    select: onAutoCompleteSelected,
						    focus: function() { return false; }
						});
					    jQuery("#prueba").autocomplete("search");
					});
				}
				
				function onAutoCompleteSelected(event, ui) {
					jQuery("#prueba").val(ui.item.label);
					jQuery("#pruebaId").val(ui.item.value);
					return false;
				}
				
				jQuery("#prueba").keyup(function(event) {
					var isAlphaNumeric = String.fromCharCode(event.keyCode).match(/[A-Za-z0-9]+/g);
					if(isAlphaNumeric) {
						clearTimeout(t);
						t = setTimeout( serviceCall, 500);
					}		
				});
			});
			</script>
		
		*/
	}
}
