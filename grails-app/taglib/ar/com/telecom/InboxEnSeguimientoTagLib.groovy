package ar.com.telecom

import ar.com.telecom.pedido.consulta.Consulta

class InboxEnSeguimientoTagLib {

	def divsInboxEnSeguimiento = { attrs, body ->

		def i = attrs.i
		def pedido = attrs?.pedido
		def divId = pedido?.id
		def tipoConsulta = attrs?.tipoConsulta
		def usuarioLogueado = attrs?.usuarioLogueado 

		def pedidoHijosConAprobacionesPendientes 
		
		if ([Consulta.MIS_PENDIENTES, Consulta.PENDIENTES_MI_GRUPO, Consulta.INBOX_APROBACIONES].contains(tipoConsulta)) {
			pedidoHijosConAprobacionesPendientes = new HashSet(pedido?.pedidosHijosConAprobacionesPendientes(usuarioLogueado, tipoConsulta) +
				pedido?.actividadesPendientesPara(usuarioLogueado))
		} else {
			pedidoHijosConAprobacionesPendientes = new HashSet(pedido?.pedidosHijosEnSeguimiento(usuarioLogueado))
		}

		out << "<tr class=\"${(i % 2) == 0 ? 'odd' : 'even'}\" style=\"cursor: pointer;\">"
		if (pedido?.getPedidoHijos() && pedidoHijosConAprobacionesPendientes){
			out << "<td onClick=\"expandItem('contentValoracion$divId')\"><div class=\"indicatorValoracion\" id=\"indicatorValoracion$divId\">&nbsp;"
		}else{
			out << "<td onClick=\"javascript:window.location = '${createLink(id: pedido.id,params: [pedidoId: pedido.id], controller: pedido?.faseActual?.controllerNombre)}';\">&nbsp;"
		}
		out << "</div>"
		out << "</td>"
		out << "<td onclick=\"javascript:window.location = '${createLink(id: pedido.id,params: [pedidoId: pedido.id], controller: pedido?.faseActual?.controllerNombre)}';\">"
		out << "<b>"
		out << g.link(id:pedido.id, params: [pedidoId: pedido.id] , controller: pedido?.faseActual?.controllerNombre, {pedido.id})
		out << "</b>"
		out << "</td>"
		out << "<td onclick=\"javascript:window.location = '${createLink(id: pedido.id,params: [pedidoId: pedido.id], controller: pedido?.faseActual?.controllerNombre)}';\">${pedido?.titulo}</td>"
		out << "<td onclick=\"javascript:window.location = '${createLink(id: pedido.id,params: [pedidoId: pedido.id], controller: pedido?.faseActual?.controllerNombre)}';\">${pedido?.faseActual}</td>"
		out << "<td onclick=\"javascript:window.location = '${createLink(id: pedido.id,params: [pedidoId: pedido.id], controller: pedido?.faseActual?.controllerNombre)}';\">${pedido?.faseActual?.macroEstado}</td>"
//		out << "<td onclick=\"javascript:window.location = '${createLink(id: pedido.id,params: [pedidoId: pedido.id], controller: pedido?.faseActual?.controllerNombre)}';\">N/A</td>"
		def rolPadre = pedido.getRol(pedido?.faseActual, usuarioLogueado)
		if (rolPadre){ 
			out << "<td onclick=\"javascript:window.location = '${createLink(id: pedido.id,params: [pedidoId: pedido.id], controller: pedido?.faseActual?.controllerNombre)}';\">${rolPadre}</td>"
		}else{
			out << "<td onclick=\"javascript:window.location = '${createLink(id: pedido.id,params: [pedidoId: pedido.id], controller: pedido?.faseActual?.controllerNombre)}';\">&nbsp;</td>"
		}

		out << "<td>"
		if (pedido?.isCancelado()) {
			out << "<a title='" + pedido?.descripcionStatus + "' class='pedido_cancelado' href='#'>&nbsp;</a>"
		}
		if (pedido?.isSuspendido()) {
			out << "<a title='" + pedido?.descripcionStatus + "' class='pedido_suspendido' href='#'>&nbsp;</a>"
		}
		if (pedido?.isCerrado()) {
			out << "<a title='" + pedido?.descripcionStatus + "' class='pedido_cerrado' href='#'>&nbsp;</a>"
		}
		
		out << "</td>"
		
		out << "</tr>"

		if (pedido?.getPedidoHijos()){
			out << "<tr style=\"cursor: pointer;\"><td style=\"border: none; padding: 0px;\" colspan=\"7\">"
			out << "<div id=\"contentValoracion$divId\" style=\"display: none;\" >"
			out << "<table cellspacing=\"0\" cellpadding=\"0\" style=\"border: none;\">"
			
			def pedidos = pedidoHijosConAprobacionesPendientes

			pedidos.eachWithIndex {
				pedidoHijo, iHijo  ->
				
				out << "<tr ${(iHijo % 2) == 0 ? "class='fondoCelda1'" : "class='fondoCelda2'"}\"  onclick=\"javascript:window.location = '${createLink(id: pedido.id,params: [pedidoId: pedido.id, impacto: pedidoHijo.id], controller: pedidoHijo?.faseActual?.controllerNombre)}';\">"
				out << "<td class=\"item0\">&nbsp;</td>"
				out << "<td class=\"item1\" >"
				out << pedidoHijo.getNumero()
				out << "</td>"
				out << "<td class=\"item2\">"
				out << pedidoHijo?.getTipoPlanificacion()
				out << "</td>"
				out << "<td class=\"item3\">"
				out << pedidoHijo?.faseActual
				out << "</td>"
				out << "<td class=\"item4\">"
				out << pedidoHijo?.faseActual?.macroEstado
				out <<"</td>"
//				out << "<td class=\"item5\">"
//				out << pedidoHijo?.getTipoPlanificacion()
//				out << "</td>"
				out << "<td class=\"item6\">"
				def rolHijo = pedidoHijo.getRol(pedidoHijo?.faseActual, usuarioLogueado)
				if (rolHijo){
					out << rolHijo
				}else{
					out << "&nbsp;"
				}
				out << "</td>"
				
				out << "<td>"
				if (pedidoHijo?.isCancelado()) {
					out << "<a title='" + pedidoHijo?.descripcionStatus + "' class='pedido_cancelado' href='#'>&nbsp;</a>"
				}
				if (pedidoHijo?.isSuspendido()) {
					out << "<a title='" + pedidoHijo?.descripcionStatus + "' class='pedido_suspendido' href='#'>&nbsp;</a>"
				}
				if (pedidoHijo?.isCerrado()) {
					out << "<a title='" + pedidoHijo?.descripcionStatus + "' class='pedido_cerrado' href='#'>&nbsp;</a>"
				}
				
				out << "</td>"
				
				out << "</tr>"
			
			}
			out << "</table>"
			out << "</div>"
			out << "</tr>"
			
			out << "<script type=\"text/javascript\">"
			out << "animatedcollapse.addDiv('contentValoracion$divId', 'fade=1');"
			out << "</script>"
		}
		
		
	}
	
}
