package ar.com.telecom

import ar.com.telecom.pcs.entities.LogModificaciones;

class TipoEventoTagLib {

	def tipoEvento = { attrs ->
		def tipoEvento = attrs.tipoEvento
		out << LogModificaciones.getTipoEvento(tipoEvento)
	}
}
