package ar.com.telecom

import java.awt.ItemSelectable;


class CheckBoxListMapTagLib {

	def checkBoxListMap = { attrs ->
		def listadoItems = attrs.items
		def nombreCheck = attrs.name
		def seleccionados = attrs.seleccionados

		//TODO: Hacer el filtro de roles
		out << "<div id=\"scroll_checkboxes_container\">"
		out << "<div id=\"scroll_checkboxes\">"
		listadoItems.eachWithIndex {
			itemCheck, counter  ->
			
				out << "<label>"
				boolean isChecked = false
				seleccionados.each { 
					itemSelect ->
						if (itemCheck.value.equals(itemSelect.trim())) {
							isChecked = true
						}
				}
				out << checkBox(id: nombreCheck + "_" + (counter+1), name: nombreCheck, value: itemCheck?.value, checked: isChecked)
				out << itemCheck?.key
				out << "</label><br />"
		}
		
		out << "</div>"
		out << "<div id=\"scroll_checkboxesMenu\">"
		out << "<span>"
		out << "Seleccionar: "
		out << "<a href=\"javascript:seleccion('$nombreCheck', ${listadoItems.size()}, true);\">TODOS</a>"
		out << " | " 
		out << "<a href=\"javascript:seleccion('$nombreCheck', ${listadoItems.size()}, false);\">NINGUNO</a>"
		out << "</span>"
		out << "</div>"
		out << "</div>"
	}
	
}
