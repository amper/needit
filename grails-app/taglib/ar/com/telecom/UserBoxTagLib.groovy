package ar.com.telecom

import org.codehaus.groovy.grails.commons.ConfigurationHolder

import ar.com.telecom.pcs.entities.Fase
import ar.com.telecom.util.DateUtil

class UserBoxTagLib {
	def umeService
	
	def appName = ConfigurationHolder.config.appName
	
	def userBox = { attrs ->
		def user = umeService.getUsuario(attrs.legajo)
		def legajoSinU = user?.legajo?.replace('u', '0')
		
		out << "<img id='foto" + attrs.box + "_" + user.legajo + "' class='img' "
		out << "onerror=javascript:UrlNotValid('foto"
		out << attrs.box + "_"
		out << user.legajo
		out << "') "
		out << "src='http://signomobile.telecom.com.ar/irj/go/km/docs/fotos/" + legajoSinU +".JPG' alt='foto' /> "
		out << "<p class='name'><span>"
		out << getNombreApellido(user)
		out << "</span>"
		out << "</p><span style='margin-right:20px; width:60%;'>"
		out << user.legajo
		out << "</span><span style='margin-right:5px;'><a href='sip:"
		out << user.email
		out << "'><img src='/${appName}/images/communicator.png'/></a></span>"
		out << "<span><a href='mailto:"
		out << user.email
		out << "'><img src='/${appName}/images/mail.png'/></a></span>"
//		out << "<span>"
//		out << user.email 
//		out << "</span>"
		out << "<p>"
		out << user.gerencia 
		out << "</p>"
		out << g.textField(id:'input'+attrs.box, name: attrs.box, value: user.legajo, style:"display:none;")
		
		if (user?.gerenciaCodigo) {
			EstructuraIT estructuraIT = EstructuraIT.findByCodigoEstructura(user.gerenciaCodigo)
			if(estructuraIT){
				out << "<script>mensajeGerenteIT(false);</script>"
			}else{
				out << "<script>mensajeGerenteIT(true);</script>"
			}
		}
		
	}
	
	def userLine = {attrs ->
		def pedido = attrs.pedido
		def controllerNombre = attrs.controller
		def fase = Fase.findByControllerNombre(controllerNombre)
		def legajoResponsable = pedido?.getUsuarioResponsable(fase, attrs.legajo)
		
		
		if(attrs.legajo){
			def user = umeService.getUsuario(attrs.legajo)
			def legajoSinU = user.legajo?.replace('u', '0')
			out << "<div class='itemContenedor'"
			if (legajoResponsable == attrs.legajo){
				out << "style='background-color:#3e8a9a;color:#FFF;'"
			}
			out << ">"
			out << "<img id='foto" + attrs.legajo + "_" + user.legajo + "' class='imgMin' "
			out << "onerror=javascript:UrlNotValid('foto"
			out << attrs.legajo + "_"
			out << user.legajo
			out << "') "
			out << "src='http://signomobile.telecom.com.ar/irj/go/km/docs/fotos/" + legajoSinU +".JPG' alt='foto' />"
			out << "<span class='itemTitulo'>" + attrs.etiqueta +"</span>"
			out << "<p class='itemDescripcion'>"
			out << getNombreApellido(user)
			out << "</p>"
			out << "</div>"
		}
	}

	def userPedidoHijo = { attrs ->
		def user = umeService.getUsuario(attrs.hijo.legajoUsuarioResponsable)
		def legajoSinU = attrs.hijo.legajoUsuarioResponsable?.replace('u', '0')
		
		out << "<div class='itemContenedor'>"

		if(user){
			out << "<img id='fotoPedidoHijo" + user?.legajo + "' class='imgMin' "
			out << "onerror=javascript:UrlNotValid('fotoPedidoHijo"
			out << user?.legajo
			out << "') "
			out << "src='http://signomobile.telecom.com.ar/irj/go/km/docs/fotos/" + legajoSinU +".JPG' alt='foto' />"
		}

		out << "<p class='itemTitulo' style='margin-top:3px;'>" + attrs.hijo.numero.substring(attrs.hijo.numero.indexOf('.') + 1, attrs.hijo.numero.length()) + " - "
		out << g.link(controller:attrs.hijo.faseActual.controllerNombre, params:['pedidoId': attrs.padre.id,'impacto':attrs.hijo.id ], attrs.hijo.sistema ? attrs.hijo.sistema.toString() : attrs.hijo.areaSoporte.toString())
		out << "</p>"

		if(user){
			out << "<p class='itemDescripcion'>"
			out << user?.nombreApellido
			out << "</p>"
		}

		def sEstado = ""
		if (attrs.hijo.descripcionStatus.equalsIgnoreCase('')) {
			sEstado = "(En Curso)"
		} else {
			sEstado = "" + g.indicadorEstadoPedido(pedido: attrs.hijo)
		}
		out << "<span class='itemTitulo' style='clear:both; margin-top:3px;'>"
		out << attrs.hijo.tipoImpacto
		out << " ${sEstado}</span>"

		out << "<p class='itemDescripcion' style='font-style:italic;'>"
		out << "Fase: "
		if (attrs.hijo.faseActual.codigoFase.equalsIgnoreCase(FaseMultiton.ESPECIFICACION_IMPACTO_HIJO)) {
			out << "${attrs.hijo.faseActual} <span title='${attrs.hijo.estiloDescripcion}' id='pedido_${attrs.hijo.estilo}'></span>"
		} else {
			if( attrs.hijo.cumplioFecha(FaseMultiton.getFase(attrs.hijo.faseActual.codigoFase)) ) {
				out << "${attrs.hijo.faseActual} <span title='Cumplido el ${DateUtil.toString(attrs.hijo.getFecha(FaseMultiton.getFase(attrs.hijo.faseActual.codigoFase)), true)}' id='pedido_estado_cumplido'></span>"
			} else {
				out << "${attrs.hijo.faseActual} <span title='Pendiente' id='pedido_estado_pendiente'></span>"
			}
		}
		out << "</p>"
/*
		out << "<p class='itemDescripcion' style='font-style:italic;'>"
		if (attrs.hijo.descripcionStatus.equalsIgnoreCase('')) {
			out << "Estado: En Curso"
		} else {
			out << "Estado: " + g.indicadorEstadoPedido(pedido: attrs.hijo)
		}
		out << "</p>"
*/
		out << "</div>"
	}

	def userDescription = { attrs ->
		
		if (attrs.legajo) {
			def user = umeService.getUsuario(attrs.legajo)
					def legajoSinU = user?.legajo?.replace('u', '0')
					
					out << "<div class='itemContenedor'>"
					out << "<img id='foto" + attrs.pedido + attrs.legajo + "_" + user.legajo + "' class='imgMin' "
					out << "onerror=javascript:UrlNotValid('foto"
					out << attrs.pedido + attrs.legajo + "_"
					out << user.legajo
					out << "') "
					out << "src='http://signomobile.telecom.com.ar/irj/go/km/docs/fotos/" + legajoSinU +".JPG' alt='foto' /> "
					out << "<p class='itemDescripcion'>"
					out << getNombreApellido(user)
					out << "</p>"
					out << "<p>"
					out << user.legajo
					out << "<span><a href='sip:"
					out << user.email
					out << "'><img src='/${appName}/images/communicator.png'/></a></span>"
					out << "<span><a href='mailto:"
					out << user.email
					out << "'><img src='/${appName}/images/mail.png'/></a></span>"
					out << "</p>"
					out << "</div>"
					
			
		}
	}
	
	def userBoxHeader = {attrs ->
		def legajo = umeService.getUsuarioLogueado()
		def user = umeService.getUsuario(legajo)
		
		out << "<img id='foto' class='img' onerror=\"javascript:UrlNotValid('foto')\" src='http://signomobile.telecom.com.ar/irj/go/km/docs/fotos/" +  legajo.replace('u','0') + ".JPG' alt='foto' />"
		out << "<p>"
		out << user?.nombreApellido
		out << "</p>"
	}
	
	def miPerfilModal = {attrs ->
		def legajo = umeService.getUsuarioLogueado()
		def user = umeService.getUsuario(legajo)
		def roles = umeService.getRolesUsuarioLogeado()
		def esGerente = umeService.isGerente(legajo)
		def esAprobadorImpacto = umeService.isAprobadorImpacto(legajo)
		
		out << g.render(template:"/templates/miPerfil", model:['user':user, 'roles': roles, 'esGerente':esGerente, 'esAprobadorImpacto':esAprobadorImpacto])
		
	}

	String getNombreApellido(user){
		return (user.nombreApellido && !user.nombreApellido.toString().equalsIgnoreCase(""))?user.nombreApellido:"Usuario no disponible"
	}
	
}
