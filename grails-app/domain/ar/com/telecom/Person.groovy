package ar.com.telecom

import java.util.Set

import ar.com.telecom.Estructura;


class Person {

	String username
	String password
	Boolean enabled
	Boolean accountExpired = false
	Boolean accountLocked = false
	Boolean passwordExpired = false
	String userRealName
	String email
	Estructura gerencia
	Estructura direccion
	
	static constraints = {
		username blank: false, unique: true		
		//campos agregados
		userRealName(blank: false)
		email(email:true,blank:false)
		gerencia (nullable:true)
		direccion (nullable:true)
	}

	static mapping = {
		password column: '`password`'
	}
	
	String toString() {
		"${username}-${userRealName}"
	}

	Set<Role> getAuthorities() {
		PersonRole.findAllByPerson(this).collect { it.role } as Set
	}
	
	boolean tieneRol(unRol) {
		return getAuthorities().contains(unRol)
	}

}
