package ar.com.telecom

class EstructuraIT {
	String codigoEstructura
	
    static constraints = {
		codigoEstructura(size:1..60)
    }
	
	String toString() {
		codigoEstructura + " (IT)"
	}
}
