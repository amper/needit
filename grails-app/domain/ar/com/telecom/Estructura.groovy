package ar.com.telecom

import ar.com.telecom.Person;


class Estructura {

	Person gerenteAsociado
	String codigoEstructura
	String codigoEstructuraPadre
	String descripcion //nombre de la gerencia/descripcion
	Boolean esDireccion

	int nivelEstructura

	static constraints = {
		gerenteAsociado(nullable:true)
		codigoEstructura(size:1..60)
		codigoEstructuraPadre(size:1..60, nullable:true)
		descripcion(size:1..60)
		esDireccion (nullable:true)
	}
	
	public String toString() {
		return codigoEstructura + " - " + descripcion
	}
	
}
