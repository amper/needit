package ar.com.telecom

class Requestmap {

	String url
	String configAttribute

	static mapping = {
		cache false
	}

	static constraints = {
		url blank: false, unique: true
		configAttribute blank: false
	}
}
