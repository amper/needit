package ar.com.telecom.pcs.entities

import ar.com.telecom.TipoImpactoMultiton
import ar.com.telecom.ume.UmeService

class AreaSoporte{

	String descripcion
	String grupoAdministrador
	
	
    static constraints = {
		descripcion(size:1..80)
		grupoAdministrador(size:1..100)
    }
	
	static hasMany = [ sistemas: Sistema ]
	
	def sistema() {
		return null
	}
	
	def areaSoporte() {
		return this
	}
	
	def tipoImpacto() {
		return TipoImpactoMultiton.areaSoporte()	
	}
	
	String toString() {
		"${descripcion}"
	}
	
	public boolean equals(Object o) {
		try {
			AreaSoporte otro = (AreaSoporte) o
			return otro.id.equals(id)
		} catch (ClassCastException e) {
			return false
		}
	}
	
	public int hashCode() {
		return id.hashCode()
	}
	
	@Deprecated 
	/**
	 * Se reemplaza por getGrupoResponsable()
	 */
	def grupoResponsable() {
		return grupoAdministrador
	}

	@Deprecated
	/**
	 * Se reemplaza por getLegajoUsuarioResponsable()
	 */
	def usuarioResponsable() {
		return null
	}

	def getUsuariosANotificar() {
		return new UmeService().getUsuariosGrupoLDAP(grupoAdministrador, null).collect { it.legajo }
	}
	
	def getGrupoResponsable() {
		return grupoAdministrador
	}
	
	def getLegajoUsuarioResponsable() {
		return null
	}
	
}
