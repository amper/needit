package ar.com.telecom.pcs.entities

class GrupoGestionDemanda {
	String grupoLDAP
	
	static constraints = { grupoLDAP(maxSize:50) }
	
	public String toString(){
		return grupoLDAP;
	}
}
