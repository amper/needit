package ar.com.telecom.pcs.entities

class ActividadPlanificacion {

	public static String FASE_IMPLANTACION = "Implantaci\u00F3n"
	
	String descripcion
	Fase fase
	
    static constraints = {
		descripcion(size:1..70)
		fase(nullable:true)
    }
	
	String toString() {
		"${descripcion}"
	}
	boolean esImplantacion(){
		return descripcion.equalsIgnoreCase(FASE_IMPLANTACION)
	} 
	
}
