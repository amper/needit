package ar.com.telecom.pcs.entities

import grails.orm.HibernateCriteriaBuilder;

class Variante {
	// Tipos de Búsqueda
	public static String BUSQUEDA_ESTANDAR = "S" 
	public static String BUSQUEDA_AVANZADA = "A"
	public static String BUSQUEDA_PERSONALIZADA = "P"
	// Opciones de Criterio
	public static String CRITERIO_ES_IGUAL = "EQ"
	public static String CRITERIO_CONTIENE = "CNT"
	//public static String CRITERIO_PERSONALIZADO = "PERS"
	public static String CRITERIO_ES_MAYOR = "GT"
	public static String CRITERIO_ES_MENOR = "LT"
	
	
	// Encabezado
	String nombreVariante
	String legajoAutorVariante
	String tipoBusqueda
	
	// Busqueda standard
	// *****************
	String titulo
	TipoGestion tipoGestion
	// Estado actual
	//MacroEstado macroestado         // revisar posibilidad de multiselección
	// y las fases
	// Impactos
	String criterioSistemaImpactado
	String valorSistemaImpactado
	// y tipos de impacto
	//String criterioAreaSoporte
	//String criterioTipoImpacto
	//String valorAreaSoporte
	//String criterioAreaSoporteImpactadas
	
	// Busqueda avanzada
	// *****************
	String estructuraRequirente
	String legajoUsuario
	String criterioSistemaSugerido
	String valorSistemaSugerido
	String criterioPrioridad
	String valorPrioridad
	// Trazabilidad
	TipoReferencia tipoReferencia
	String criterioTipoReferencia
	String valorTipoReferencia
	String criterioSolman
	String valorSolman
	String criterioRelease
	String valorRelease
	String criterioSimplitPaP
	String valorSimplitPaP
	String criterioSimplitPAU
	String valorSimplitPAU

	List macroestados
	List fases
	List tiposImpacto
	List areasSoporte
	List roles
	List grupos
	List estados
	
	Date fechaDesde
	Date fechaHasta
	
	static constraints = {
		nombreVariante(size: 1..30)
		legajoAutorVariante(maxSize: 10)
		tipoBusqueda(maxSize: 2, nullable: true)
		titulo(maxSize:200, nullable: true)
		tipoGestion(nullable: true)
		//macroestado(nullable: true)
		criterioSistemaImpactado(maxSize:20, nullable: true)
		valorSistemaImpactado(maxSize:50, nullable: true)
		//criterioAreaSoporte(maxSize:20, nullable: true)
		//valorAreaSoporte(nullable: true)
		areasSoporte(nullable: true)
		//criterioTipoImpacto(maxSize:20, nullable: true)
		//valorTipoImpacto(nullable: true)
		tiposImpacto(nullable: true)
		//rolesABuscar(nullable: true)
		roles(nullable: true)
		estados(nullable: true)
		//criterioAreaSoporteImpactadas(maxSize:20, nullable: true)
		//valorAreaSoporteImpactadas(nullable: true)
		//areasSoporteImpactadas(nullable: true)
		estructuraRequirente(maxSize:20, nullable: true)
		legajoUsuario(maxSize:20, nullable: true)
		criterioSistemaSugerido(maxSize:20, nullable: true)
		valorSistemaSugerido(maxSize:50, nullable: true)
		criterioPrioridad(maxSize:20, nullable: true)
		valorPrioridad(maxSize:50, nullable: true)
		tipoReferencia(nullable: true)
		criterioTipoReferencia(maxSize:20, nullable: true)
		valorTipoReferencia(maxSize:50, nullable: true)
		criterioSolman(maxSize:20, nullable: true)
		valorSolman(maxSize:50, nullable: true)
		criterioRelease(maxSize:20, nullable: true)
		valorRelease(maxSize:50, nullable: true)
		criterioSimplitPaP(maxSize:20, nullable: true)
		valorSimplitPaP(maxSize:50, nullable: true)
		criterioSimplitPAU(maxSize:20, nullable: true)
		valorSimplitPAU(maxSize:50, nullable: true)
		fechaDesde(nullable: true)
		fechaHasta(nullable: true)
	}
	
	static hasMany = [ 
		macroestados: MacroEstado,
		fases: Fase,
		tiposImpacto : TipoImpacto,
		areasSoporte : AreaSoporte,
		roles: RolAplicacion,
		grupos: String,
		estados : String
	]
	
	static mapping = {
		macroestados lazy: false
		fases lazy: false
		tiposImpacto lazy: false
		roles lazy: false
		grupos lazy: false
		estados lazy: false
		tipoGestion fetch: 'join'
		tipoReferencia fetch: 'join'
	}

	String toString() {
		return nombreVariante
	}


	public boolean esBusquedaEstandar() {
		return (!tipoBusqueda || tipoBusqueda.equalsIgnoreCase(BUSQUEDA_ESTANDAR))
	}
	
	/**
	 * Llena el combo de criterios de búsqueda 
	 * @return
	 */
	public List getCriteriosDefault() {
		def criterios = new ArrayList()
        criterios.plus("contiene")
		criterios.plus("igual a")
		criterios
	}
	
	public List getCriteriosNumeros() {
		def criterios = new ArrayList()
		criterios.plus("es igual")
		criterios.plus("es mayor")
		criterios.plus("es menor")
		criterios
	}

	public List getCriteriosIgualPrimero() {
		def criterios = new ArrayList()
		criterios.plus("es igual")
		criterios.plus("contiene")
		criterios
	}
	
//	public static Object getCriterion(Object data, String criterio, Object valor) {
//		HibernateCriteriaBuilder criteria = new HibernateCriteriaBuilder(Variante, null)
//		if (criterio.equals(CRITERIO_ES_IGUAL)) {
//			return criteria.eq(data, valor)
//		}
//		if (criterio.equals(CRITERIO_CONTIENE)) {
//			return criteria.ilike(data, valor)
//		}
//		if (criterio.equals(CRITERIO_ES_MAYOR)) {
//			return criteria.gt(data, valor)
//		}
//		if (criterio.equals(CRITERIO_ES_MENOR)) {
//			return criteria.lt(data, valor)
//		}
//
//	}
//	
	
	public String getComparador(String criterio) {
		if (criterio.equalsIgnoreCase(CRITERIO_ES_IGUAL)) {
			return "eq"
		}
		if (criterio.equalsIgnoreCase("contiene")) {
			return "ilike"
		}
		if (criterio.equalsIgnoreCase("es mayor")) {
			return "ge"
		}
		if (criterio.equalsIgnoreCase("es menor")) {
			return "le"
		}
	}
	
	public String getComparando(String criterio, String valor) {
		if (criterio.equalsIgnoreCase("contiene")) {
			return "%" + valor + "%"
		} else {
			return valor
		}
	}

}
