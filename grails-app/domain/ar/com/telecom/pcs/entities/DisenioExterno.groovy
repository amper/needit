package ar.com.telecom.pcs.entities

import java.util.Date;

class DisenioExterno {

	public static String NO_APLICA = "N/A"
	
	// **********************************************
	// Deprecated
	String comentarioAlcance
	Date fechaRelease
	String numeroRelease
	// **********************************************

	String premisasSupuestosRestricciones
	String descripcionFuncionalSolucion
	String interfacesInvolucradasSolucion
	String descripcionVolumetriaPerformance
	String principalesCambiosProcesos
	String definicionesPendientesWarnings
	JustificacionDisenioExterno justificacionDisenioExterno

	static constraints = {
		fechaRelease(nullable: true)
		numeroRelease(maxSize:20, nullable: true)
		comentarioAlcance(nullable:true)
		premisasSupuestosRestricciones(nullable:true)
		descripcionFuncionalSolucion(nullable:true)
		interfacesInvolucradasSolucion(nullable:true)
		descripcionVolumetriaPerformance(nullable:true)
		principalesCambiosProcesos(nullable:true)
		definicionesPendientesWarnings(nullable:true)
		justificacionDisenioExterno(nullable: true)
	}

	static mapping = {
		comentarioAlcance type: 'text'
		premisasSupuestosRestricciones type: 'text'
		descripcionFuncionalSolucion type: 'text'
		interfacesInvolucradasSolucion type: 'text'
		descripcionVolumetriaPerformance type: 'text'
		principalesCambiosProcesos type: 'text'
		justificacionDisenioExterno fetch: 'join'
	}

	static hasMany = [ anexos: AnexoPorTipo ]

	public DisenioExterno() {
		anexos = []
	}
	
	String toString() {
		return "Diseño Externo " + comentarioAlcance
	}

	def validar(AbstractPedido pedido) {
		if (justificacionDisenioExterno) {
			return
		}

		if (!premisasSupuestosRestricciones && !descripcionFuncionalSolucion && !interfacesInvolucradasSolucion
			&& !descripcionVolumetriaPerformance && !principalesCambiosProcesos && !definicionesPendientesWarnings && anexos.isEmpty()) {
			pedido.errors.reject("Diseño Externo: Debe ingresar justificaci\u00F3n o cargar el diseño externo")
		}
	}
	
	def actualizarEspecificacion() {
		comentarioAlcance = nuevoValor(comentarioAlcance, NO_APLICA)
		premisasSupuestosRestricciones = nuevoValor(premisasSupuestosRestricciones, NO_APLICA)
		descripcionFuncionalSolucion = nuevoValor(descripcionFuncionalSolucion, NO_APLICA)
		interfacesInvolucradasSolucion = nuevoValor(interfacesInvolucradasSolucion, NO_APLICA)
		descripcionVolumetriaPerformance = nuevoValor(descripcionVolumetriaPerformance, NO_APLICA)
		principalesCambiosProcesos = nuevoValor(principalesCambiosProcesos, NO_APLICA)
		definicionesPendientesWarnings = nuevoValor(definicionesPendientesWarnings, NO_APLICA)
	}
	
	def nuevoValor(valorActual, valor) {
		if (valorActual) {
			valorActual
		} else {
			valor
		}
	}
	
	def eliminarAnexo(unAnexo, usuarioLogueado) {
		def anexo = this.anexos.find { anexo -> anexo.id.equals(unAnexo.id) }
		this.removeFromAnexos(anexo)
	}

	def getAnexos(Fase fase) {
		return anexos.findAll { anexo -> anexo.esDeFase(fase) }
	}
	
}
