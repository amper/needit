package ar.com.telecom.pcs.entities

import ar.com.telecom.ume.UmeService

class Sistema {
	
	String descripcion
	String descripcionLarga
	String grupoAdministrador   // administrador funcional
	String gerenciaSimplit      
	boolean impactoSOX
	boolean gestionaEvolutivos
	String grupoReferenteSWF
	boolean normaliza
	Boolean gestionaRelease
	String responsableGestionarReleases
	Date fechaBajaSimplit
	String grupoLDAPGestionOperativa
	
	static hasMany = [ softwareFactories : SoftwareFactory ]

    static constraints = {
		descripcion(size:1..50)
		descripcionLarga(size:1..250)
		grupoAdministrador(size:1..100, nullable:true)
		gerenciaSimplit(size:1..100, nullable:true)
		grupoReferenteSWF(size:1..100, nullable:true)
		responsableGestionarReleases(maxSize:20, nullable: true)
		fechaBajaSimplit(nullable:true)
		gestionaRelease(nullable:true)
		grupoLDAPGestionOperativa(nullable: true, maxSize: 30)
    }
	
	String toString() {
		"${descripcion}"	
	}
	
	def traerDatosRelacionados() {
		softwareFactories?.descripcion
	}
	
	def getGruposGestion() {
		def grupos = softwareFactories.collect { swf -> swf.grupoLDAP }
		if (grupoReferenteSWF) {
			grupos = grupos.plus(grupoReferenteSWF)
		}
		if (grupoAdministrador) {
			grupos = grupos.plus(grupoAdministrador)
		}
		grupos
	}
	
	def getUsuariosANotificar() {
		return new UmeService().getUsuariosGrupoLDAP(grupoAdministrador, null).collect { it.legajo }
	}
	
	boolean tieneSoftwareFactory(swf) {
		return this.softwareFactories?.id?.contains(swf?.id)
	}
}
