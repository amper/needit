package ar.com.telecom.pcs.entities

import java.text.DecimalFormat
import java.util.List;

import org.codehaus.groovy.grails.commons.ConfigurationHolder

import ar.com.telecom.FaseMultiton
import ar.com.telecom.ume.UmeService


class PedidoHijo extends AbstractPedido {

	TipoImpacto tipoImpacto
	//Fase faseActual
	Sistema sistema
	AreaSoporte areaSoporte

	Date fechaCierreEspecificacion
	Date fechaFinActividades   // vale para cualquier hijo
	Date fechaFinEjecucionPAU
	Date fechaAprobacionPAU
	Date fechaAprobacionImplantacion
	Date fechaImplantacion
	Date fechaNormalizacion
	// Sistema XOR Area de soporte

	//Construcci\u00F3n SW Desarrollo
	Boolean impactaBaseArquitectura
	Boolean actualizaCarpetaOperativa
	Boolean requiereManualUsuario

	//Cierre (a definir)
	GradoSatisfaccion gradoSatisfaccion
	String comentarioCierre
	String leccionesAprendidas

	// Usuarios
	String grupoResponsable
	String legajoUsuarioResponsable

	List actividades

	static belongsTo = [ pedidoPadre : Pedido ]

	static hasMany = [ actividades: Actividad ]

	static constraints = {
		sistema(nullable:true)
		areaSoporte(nullable:true)
		impactaBaseArquitectura(nullable: true)
		requiereManualUsuario(nullable:true)
		actualizaCarpetaOperativa(nullable:true)
		gradoSatisfaccion(nullable:true)
		comentarioCierre(nullable: true)
		leccionesAprendidas(nullable:true)
		grupoResponsable(maxSize:50, nullable:true)
		legajoUsuarioResponsable(maxSize: 20, nullable:true)
		fechaCierreEspecificacion(nullable: true)
		fechaFinActividades(nullable: true)
		fechaFinEjecucionPAU(nullable: true)
		fechaAprobacionPAU(nullable: true)
		fechaAprobacionImplantacion(nullable: true)
		fechaImplantacion(nullable: true)
		fechaNormalizacion(nullable: true)
	}

	static mapping = {
		discriminator value: 'H'
		comentarioCierre type: 'text'
		leccionesAprendidas type: 'text'
		tipoImpacto fetch: 'join'
		//faseActual fetch: 'join'
		sistema fetch: 'join'
		areaSoporte fetch: 'join'
		gradoSatisfaccion fetch: 'join'
	}

	public PedidoHijo() {
		super()
		actividades = new ArrayList()
	}

	def getNumero() {
		def indice = pedidoPadre.pedidosHijos.indexOf(this)
		return pedidoPadre.getNumero() + "." + new DecimalFormat('00').format(indice + 1)
	}

	def getTipoPlanificacion() {
		return (sistema ? sistema.toString() : areaSoporte.toString()) + " - " + tipoImpacto.toString()
	}

	String toString() {
		return getTipoPlanificacion()
	}

	def grupoResponsable() {
		return grupoResponsable
	}

	/**
	 * Comportamiento polim\u00F3rfico para la pantalla de Especificaci\u00F3n Impacto
	 */
	def planificaActividadesSistema() {
		return !(planificaTareasSoporte())
	}
	
	boolean requiereCargarDetalles() {
		planificaActividadesSistema()
	}

	def planificaTareasSoporte() {
		return !(tipoImpacto.sistemaImpactado) || (tipoImpacto.descripcion.equalsIgnoreCase(TipoImpacto.ADMINISTRADOR_FUNCIONAL))
	}

	def mostrarSolapaPlanificacion() {
		return tipoImpacto.tienePlanificacionyEsfuerzo
	}

	def mostrarSolapaEstrategiaPrueba() {
		return tipoImpacto.tieneEstrategiaPrueba
	}

	def mostrarSolapaDE() {
		return tipoImpacto.tieneDisenoExterno
	}

	def sistema() {
		return sistema
	}

	public List getGruposPlanificacion() {
		return getPlanificacionActual().getGrupos(origen)
	}

	def obtenerUltimaEspecificacion() {
		return pedidoPadre.obtenerUltimaEspecificacion()
	}

	def permiteIngresarOtrosCostos() {
		return tipoImpacto.permiteIngresarOtrosCostos
	}

	def ingresaTicketSolman() {
		return tipoImpacto.ingresaTicketSolman
	}

	def cargaRelease() {
		return tipoImpacto.asociadoSistemas && (sistema?.gestionaRelease ?: false)
	}

	def getPedidoHijos(){
		return null
	}

	def puedeEditarEspecificacionImpacto(usuarioLogueado) {
		if (isCancelado()){
			return false
		}
		if (this.faseActual?.equals(faseEspecificacionImpacto())) {
			if (fechaCierreEspecificacion) {
				//return usuarioLogueado.equalsIgnoreCase(pedidoPadre.legajoCoordinadorCambio)
				// faltaría meter un flag en pedido padre
				return false
			} else {
				def umeService = new UmeService()
				return usuarioLogueado.equalsIgnoreCase(legajoUsuarioResponsable)
			}
		}
		return false
	}

	def puedeReasignarEspecificacionImpacto(usuarioLogueado) {
		if (isCancelado() || !this.faseActual?.equals(faseEspecificacionImpacto()) || fechaCierreEspecificacion) {
			return false
		}

		def umeService = new UmeService()
		return this.puedeEditarEspecificacionImpacto(usuarioLogueado) || umeService.usuarioPerteneceGrupo(grupoResponsable, usuarioLogueado)
	}

	def puedeEditarConstruccionPadre(usuarioLogueado) {
		if (isCancelado()){
			return false
		}
		if (this.estaEnConstruccion()) {
			def umeService = new UmeService()
			return usuarioLogueado.equalsIgnoreCase(legajoUsuarioResponsable) || umeService.usuarioPerteneceGrupo(grupoResponsable, usuarioLogueado)
		}
		return false
	}

	def puedeEditarAprobacionPAU(usuarioLogueado) {
		if (isCancelado()){
			return false
		}
		if(this.estaEnFase(FaseMultiton.APROBACION_PAU_HIJO)){
			return pedidoPadre.legajoInterlocutorUsuario.equalsIgnoreCase(usuarioLogueado)
		}
		return false
	}

	def puedeEditarAprobacionImplantacion(usuario){
		if (isCancelado()){
			return false
		}
		return estaEnFase(FaseMultiton.APROBACION_IMPLANTACION_HIJO) && (usuarioPerteneceAGrupoDeAprobacion(usuario, FaseMultiton.getFase(FaseMultiton.APROBACION_IMPLANTACION_HIJO)) || usuarioEsUsuarioDeAprobacion(usuario, FaseMultiton.getFase(FaseMultiton.APROBACION_IMPLANTACION_HIJO)))
	}

	boolean usuarioHabilitado(fase, usuarioLogueado) {
		if (isCancelado()) { //|| isSuspendido()
			return false
		}
		def usuarioResponsable = getUsuarioResponsable(fase, usuarioLogueado)
		def umeService = new UmeService()
		if (usuarioResponsable.equalsIgnoreCase(usuarioLogueado) || umeService.usuarioPerteneceGrupo(grupoResponsable, usuarioLogueado)) {
			return true
		}
		if (fase.equals(faseAprobacionPAUHijo())) {
			return pedidoPadre.legajoInterlocutorUsuario.equalsIgnoreCase(usuarioLogueado)
		}
		if (fase.equals(faseAprobacionImplantacionHijo())) {
			return usuarioPerteneceAGrupoDeAprobacion(usuarioLogueado, faseAprobacionImplantacionHijo()) ||
			usuarioEsUsuarioDeAprobacion(usuarioLogueado, faseAprobacionImplantacionHijo())
		}
	}

	def usuarioPerteneceAGrupoDeAprobacion(usuario, fase) {
		def umeService = new UmeService()
		def aprobacionesFiltradas = aprobacionesDeFase(fase).findAll {aprobacion  -> umeService.usuarioPerteneceGrupo(aprobacion.grupo, usuario) }
		return !aprobacionesFiltradas?.isEmpty()
	}

	def usuarioEsUsuarioDeAprobacion(usuario, fase) {
		def aprobacionesFiltradas = aprobacionesDeFase(fase).findAll {aprobacion  -> aprobacion.usuario?.equalsIgnoreCase(usuario)}

		return !aprobacionesFiltradas?.isEmpty()
	}

	def primeraAprobacionDeUsuario(usuario){
		def umeService = new UmeService()
		def aprobacionesPorUsuario = aprobacionesDeFase(fase).findAll {aprobacion  -> aprobacion.usuario?.equalsIgnoreCase(usuario)}?.sort()
		if(!aprobacionesPorUsuario.isEmpty()){
			return aprobacionesPorUsuario?.first()
		}

		def aprobacionesPorGrupo = aprobacionesDeFase(fase).findAll {aprobacion  -> umeService.usuarioPerteneceGrupo(aprobacion.grupo, usuario) }?.sort()
		if(!aprobacionesPorGrupo.isEmpty()){
			return aprobacionesPorGrupo?.first()
		}
		return null
	}

	def getRol(fase, legajo) {
		List roles = rolesAAsignar(fase, legajo, true)
		if (roles.isEmpty()) {
			return null
		} else {
			return roles.first()
			// TODO, devolver la lista de roles entera
		}
	}

	def getRolUsuario(fase, usuarioLogueado) {
		return getRol(fase, usuarioLogueado)
	}

	def getRol(usuarioLogueado) {
		return getRol(faseActual, usuarioLogueado)
	}

	def getRoles(String legajo) {
		return rolesAAsignar(fase, legajo, false)
	}

	def getRolDefault() {
		if (tipoImpacto.sistemaImpactado) {
			if (tipoImpacto.asociadoSistemas || tipoImpacto.tieneEstrategiaPrueba) {
				return RolAplicacion.RESPONSABLE_SWF
			} else {
				return RolAplicacion.ADMINISTRADOR_FUNCIONAL
			}
		} else {
			return RolAplicacion.AREA_SOPORTE
		}
		return null
	}

	def rolesAAsignar(Fase faseAChequear, String legajo, boolean incluyePorGrupo) {
		List result = []
		def rolAAsignar = getRolDefault()

		boolean cumpleRol = false
		if (incluyePorGrupo) {
			cumpleRol = esResponsable(legajo)
		} else {
			cumpleRol = legajo?.equalsIgnoreCase(this.legajoUsuarioResponsable)
		}
		if (cumpleRol) {
			result.add(rolAAsignar)
		}
		if (faseAChequear.equals(faseAprobacionPAUHijo())) {
			if (pedidoPadre.legajoInterlocutorUsuario.equalsIgnoreCase(legajo)) {
				result.add(RolAplicacion.INTERLOCUTOR_USUARIO)
			}
		}
		// las actividades siempre tienen un responsable, no? Por eso no pregunto por el grupo
		boolean responsableDeActividades = actividades.any { actividad -> actividad.legajoUsuarioResponsable?.equalsIgnoreCase(legajo) }
		if (responsableDeActividades) {
			result.add(RolAplicacion.RESPONSABLE_ACTIVIDAD)
		}
		// si el usuario no tiene ningún rol busco el rol del padre (pero sin entrar en los hijos para evitar loop)
		if (result.isEmpty()) {
			result.addAll(pedidoPadre.getRoles(legajo, false))
		}
		return result
	}

	//	def getGrupoActual(grupo) {
	//		//		if (this.faseActual?.equals(faseEspecificacionImpacto())) {
	//		//			return grupoResponsable
	//		//		}
	//		//		en principio no es necesario dado que la aprobaci\u00F3n nace ok y no cambia nunca de grupo
	//		return grupo
	//	}

	boolean esResponsable(usuarioLogueado) {
		if (usuarioLogueado.equalsIgnoreCase(legajoUsuarioResponsable)) {
			return true
		}
		return new UmeService().usuarioPerteneceGrupo(grupoResponsable, usuarioLogueado)
	}

	def esHijo() {
		return true
	}

	def getParent() {
		return pedidoPadre
	}

	def tipoImpacto() {
		return tipoImpacto
	}

	def definirEstrategiaPUPIPAU(estrategiaPrueba, ecs, mapaTiposPrueba) {
		def puDefault = mapaTiposPrueba.get(TipoPrueba.pruebaUnitaria())
		estrategiaPrueba.addToDetalles(new DetalleEstrategiaPrueba(tipoPrueba: TipoPrueba.pruebaUnitaria(), seRealiza: puDefault.definirRealiza(), seConsolida: puDefault.definirConsolida(), justificacionNoRealizacion: null))
		def piDefault = mapaTiposPrueba.get(TipoPrueba.pruebaIntegracion())
		estrategiaPrueba.addToDetalles(new DetalleEstrategiaPrueba(tipoPrueba: TipoPrueba.pruebaIntegracion(), seRealiza: piDefault.definirRealiza(), seConsolida: piDefault.definirConsolida(), justificacionNoRealizacion: ecs.justificacionPI))
		def pauDefault = mapaTiposPrueba.get(TipoPrueba.pruebaPAU())
		estrategiaPrueba.addToDetalles(new DetalleEstrategiaPrueba(tipoPrueba: TipoPrueba.pruebaPAU(), seRealiza: pauDefault.definirRealiza(), seConsolida: pauDefault.definirConsolida(), justificacionNoRealizacion: ecs.justificacionPAU))
	}

	def fasesActuales() {
		return tipoImpacto.fases
	}

	public boolean validar(aprobacionUI) {
		def fase = faseActual
		if (!faseActual) {
			fase = faseEspecificacionImpacto()
		}
		if (!aprobacionUI.aprueba) {
			return aprobacionUI.validar()
		}

		validarAprobacionGeneral(aprobacionUI)
		validarActividades()

		if(this.estaSuspendido()){
			this.errors.reject ("El pedido se encuentra suspendido")
		}
		
		if (fase.equals(faseEspecificacionImpacto())) {
			return this.validarEspecificacionImpacto(aprobacionUI)
		}

		return this.errors.errorCount == 0
	}

	def validarEspecificacionImpacto(aprobacionUI) {
		if (tipoImpacto.tienePlanificacionyEsfuerzo) {
			if (!getPlanificacionActual()) {
				this.errors.reject "Debe completar la planificaci\u00F3n y esfuerzo"
			} else {
				getPlanificacionActual().validar(this)
			}
		}
		if (tipoImpacto.tieneEstrategiaPrueba) {
			if (!getEstrategiaPruebaActual()) {
				this.errors.reject "Debe completar la estrategia de prueba"
			} else {
				getEstrategiaPruebaActual().validar(this)
			}
		}
		if (tipoImpacto.tieneDisenoExterno) {
			if (!getDisenioExternoActual()) {
				this.errors.reject "Debe completar el diseño externo"
			} else {
				getDisenioExternoActual().validar(this)
			}
		}
		return this.errors.errorCount == 0
	}

	def faseFinalizarConstruccionCambio() {
		def detalleEstrategiaPAU = getEstrategiaPruebaActual()?.estrategiaPAU
		if (detalleEstrategiaPAU?.seRealiza) {
			return faseEjecucionPAU()
		} else {
			if (hayImpactoEnImplantacion()) {
				return faseAprobacionImplantacionHijo()
			} else {
				return faseImplantacionHijo()
			}
		}
	}

	def faseFinalizarPAU() {
		if (hayImpactoEnImplantacion()) {
			return faseAprobacionImplantacionHijo()
		} else {
			return faseImplantacionHijo()
		}
	}

	boolean hayImpactoEnImplantacion() {
		def detalleEstrategiaPAU = getEstrategiaPruebaActual()?.estrategiaPAU
		return impactaBaseArquitectura || actualizaCarpetaOperativa || requiereManualUsuario || !areasQueDebenAprobarImplantacion().isEmpty()
	}

	def finalizarEjecucionPAU(){
		this.faseActual = this.faseAprobacionPAUHijo()
		return "El pedido pas\u00F3 a la fase aprobaci\u00F3n PAU"
	}

	boolean completoEspecificacionImpacto() {
		return !faseActual.equals(faseEspecificacionImpacto())
	}

	// TODO: Pull-up a Abstract Pedido
	public boolean avanzar(aprobacionUI, usuarioLogueado, avanzarFase) {
		boolean avanza = true
		def fase = faseActual
		def faseProxima	 = tipoImpacto.faseSiguienteA(faseActual)

		def descripcionEvento
		def usuarioResponsable = getUsuarioResponsable(fase, usuarioLogueado)
		def usuarioAprobador = getUsuarioResponsable(faseProxima, usuarioLogueado)
		def grupoAprobador
		def usuarioHistorial = usuarioLogueado

		def aprobacionesPendientesFaseActual = aprobaciones.findAll { aprobacion  -> aprobacion.pendienteParaFase(fase) }

		def tipoEventoAprobacion = tipoEventoAprobacion()
		def descripcionAdicionalOmitida = ""
		def tieneAprobacionOmitida = tieneAprobacionOmitida()

		grupoAprobador = grupoResponsable

		if (fase.equals(faseEspecificacionImpacto())) {
			avanza = avanzarFase && !denegoEspecificacion()
			if (avanza) {
				descripcionEvento = "Aprobaci\u00F3n de la determinaci\u00F3n del impacto - " + getTipoPlanificacion()
				if (faseProxima) {
					descripcionEvento += " pasa a fase " + faseProxima
				}
				aprobacionUI.justificacion = ""
				usuarioHistorial = null
				tipoEventoAprobacion = LogModificaciones.APROBACION_AUTOMATICA
			} else {
				fechaCierreEspecificacion = new Date()
				descripcionEvento = "Aprobaci\u00F3n Especificaci\u00F3n Impacto " + getTipoPlanificacion()
				getPlanificacionActual().finalizar()
				actualizarEspecificacionImpacto()
			}
		}

		if (fase.equals(faseConstruccionCambioHijo()) || fase.equals(faseConstruccionPU())) {
			descripcionEvento = "Aprobaci\u00F3n Construcci\u00F3n Cambio " + getTipoPlanificacion()
			fechaFinActividades = new Date()
			faseProxima = faseFinalizarConstruccionCambio()
			// FED: Esto se agreg\u00F3 por soporte a pruebas
			if (!tipoImpacto.contiene(faseProxima)) {
				faseProxima = tipoImpacto.faseSiguienteA(faseProxima)
			}
			//
		}

		if (fase.equals(faseAdministrarActividades())) {
			fechaFinActividades = new Date()
			descripcionEvento = "Aprobaci\u00F3n Actividades " + getTipoPlanificacion()
		}

		if (fase.equals(faseEjecucionPAU())) {
			descripcionEvento = "Aprobaci\u00F3n Ejecuci\u00F3n PAU " + getTipoPlanificacion()
			fechaFinEjecucionPAU = new Date()
		}

		if (fase.equals(faseAprobacionPAUHijo())) {
			fechaAprobacionPAU = new Date()
			faseProxima = faseFinalizarPAU()
			descripcionEvento = "Aprobaci\u00F3n Aprobaci\u00F3n PAU  " + getTipoPlanificacion()
		}

		if (fase.equals(faseAprobacionImplantacionHijo())) {
			fechaAprobacionImplantacion = new Date()
			descripcionEvento = "Aprobaci\u00F3n Aprobaci\u00F3n Implantaci\u00F3n  " + getTipoPlanificacion()
			faseProxima = faseImplantacionHijo()  // ¿Hace falta?
		}

		if (fase.equals(faseImplantacionHijo())) {
			fechaImplantacion = new Date()
			descripcionEvento = "Aprobaci\u00F3n Implantaci\u00F3n  " + getTipoPlanificacion()
		}

		if (faseProxima?.equals(faseAprobacionImplantacionHijo())) {
			generarAprobacionesImplantacion(faseProxima)
		}

		if (faseProxima?.equals(faseNormalizacionHijo())) {
			if (!normaliza()) {
				fechaNormalizacion = new Date()
				faseProxima	 = tipoImpacto.faseSiguienteA(faseProxima)
			}
		}

		if (fase.equals(faseNormalizacionHijo())) {
			fechaNormalizacion = new Date()
			descripcionEvento = "Aprobaci\u00F3n Normalizaci\u00F3n  " + getTipoPlanificacion()
		}

		if (aprobacionUI.justificacion) {
			descripcionEvento += " - Justificaci\u00F3n: " + aprobacionUI.justificacion + ". "
		}
		if (tieneAprobacionOmitida) {
			descripcionEvento = descripcionEvento + descripcionAdicionalOmitida
		}

		def rolHistorial = getRol(fase, usuarioLogueado)
		if (!usuarioHistorial) {
			rolHistorial = null
		}
		addToLogModificaciones(new LogModificaciones(legajo: usuarioHistorial, fase: fase, fechaDesde: fechaDesdePara(fase), fechaHasta: new Date(), descripcionEvento: descripcionEvento, tipoEvento: tipoEventoAprobacion, rol: rolHistorial))
		fechaUltimaModificacion = new Date()

		log.info "Fase pr\u00F3xima: " + faseProxima
		log.info "Usuario responsable fase: " + usuarioResponsable
		log.info "Usuario responsable fase siguiente: " + usuarioAprobador
		log.info "aprobaciones pendientes de fase actual " + aprobacionesPendientesFaseActual
		log.info "avanza: " + avanza

		aprobacionesPendientesFaseActual.each { aprobacionIt ->
			if (aprobacionIt.usuario?.equalsIgnoreCase(usuarioLogueado)) {
				aprobacionIt.aprobar()
				aprobacionIt.justificacion = aprobacionUI?.justificacion
			}
		}

		//def aprobacionesPendientesDeOtrosUsuarios = aprobacionesPendientesFaseActual.findAll { aprobacion -> !aprobacion.usuario || !usuarioLogueado?.equalsIgnoreCase(aprobacion.usuario) }
		def aprobacionesPendientesDeOtrosUsuarios = aprobacionesPendientesDeOtrosUsuarios(fase, usuarioLogueado)

		log.info "aprobaciones pendientes de otros usuarios " + aprobacionesPendientesDeOtrosUsuarios

		if (tieneAprobacionOmitida) {
			aprobacionesPendientesDeOtrosUsuarios.each { aprobacionIt ->
				aprobacionIt.aprobar()
				aprobacionIt.justificacion = aprobacionUI?.justificacion
			}
			aprobacionesPendientesDeOtrosUsuarios = []
		}

		if (avanza && aprobacionesPendientesDeOtrosUsuarios.isEmpty()) {
			// Avanzo la fase
			if (faseProxima) {
				faseActual = faseProxima
				if (!faseProxima.equals(faseAprobacionImplantacionHijo())) {
					def estadoAprobacion = Aprobacion.PENDIENTE

					// En AprobacionPAU, no voy a generar el pendiente hasta
					// que todos los hijos no consoliden
					if (faseProxima.equals(faseAprobacionPAUHijo()) && consolidaPAU()) {
						if (pedidoPadre.tienePedidosPendientesPAUConsolidada()) {
							estadoAprobacion = Aprobacion.EN_COLA
						} else {
							pedidoPadre.getAllAprobaciones().findAll { aprobacion  -> aprobacion.encoladaParaFase(faseProxima) }.each { aprobacion ->
								aprobacion.estado = Aprobacion.PENDIENTE
							}
						}
					}

					def aprobacion = new Aprobacion(areaSoporte: areaSoporte, fase: faseProxima, grupo: grupoAprobador, usuario: usuarioAprobador, fecha: new Date(), estado: estadoAprobacion, rol: getRol(faseProxima, usuarioAprobador))
					addToAprobaciones(aprobacion)
				}
				if (planificaActividadesSistema()) {
					this.generarActividadesSistema(faseProxima, usuarioLogueado)
				} else {
					this.generarActividadesSoporte(faseProxima, usuarioLogueado)
				}
				this.notificarCambioEstado()
			} else {
				// OJO, importante que primero le ponga fecha de cierre y luego llame al padre
				fechaCierre = new Date()
				pedidoPadre.pasarACierre(usuarioLogueado)
			}
		} else {
			if (tieneAprobacionOmitida) {
				pedidoPadre.pisarHijoConPadre(this)
				pedidoPadre.avanzar(aprobacionUI, usuarioLogueado)
			}
		}
	}

	// S\u00F3lo normalizo si el tipo de gesti\u00F3n es emergencia y el hijo es de sistemas y dice que normaliza
	boolean normaliza() {
		return pedidoPadre.tipoGestion.descripcion.equalsIgnoreCase(TipoGestion.GESTION_EMERGENCIA) && sistema && sistema?.normaliza
	}

	def areasQueDebenAprobarImplantacion() {
		def areasRelacionadasConSistema = areasRelacionadasConHijo
		def areasDeSoporteQueDebenAprobar = areasSoporteQueDebenAprobar
		return areasRelacionadasConSistema + areasDeSoporteQueDebenAprobar
	}

	def getAreasRelacionadasConHijo() {
		return new HashSet(parent.areasImpactadas().findAll( { area -> area.sistemas.contains(sistema) }))
	}

	def getAreasSoporteQueDebenAprobar() {
		return AreaSoporteAprobarFase.findAllByFase(faseActual).collect { it.areaSoporte }
	}

	def generarAprobacionesImplantacion(fase) {
		if (!this.sistema) {
			return
		}
		// Juramento hipocrático de Seba y Gerar, me dicen que filtre las AS s\u00F3lo si están impactados en la ecs
		// def areasRelacionadasConSistema = new HashSet(AreaSoporte.list().findAll( { area -> area.sistemas.contains(sistema) }))
		def areasRelacionadasConSistema = areasRelacionadasConHijo
		def areasDeSoporteQueDebenAprobar = areasSoporteQueDebenAprobar
		def areasRelacionadasConSistemaFiltradas = areasRelacionadasConSistema - areasDeSoporteQueDebenAprobar

		areasRelacionadasConSistemaFiltradas.each { area ->
			def usuarioResponsable = pedidoPadre.hijoDeAreaSoporte(area)?.legajoUsuarioResponsable
			log.info "Usuario responsable en aprob.implantaci\u00F3n AS: " + usuarioResponsable
			addToAprobaciones(new Aprobacion(fase: fase, areaSoporte: area, grupo: area.grupoAdministrador, usuario: usuarioResponsable, fecha: new Date(), estado: Aprobacion.PENDIENTE, rol: RolAplicacion.AREA_SOPORTE))
		}

		if (tipoImpacto.descripcion.equalsIgnoreCase(TipoImpacto.SOFTWARE_DESARROLLO)) {
			if (requiereManualUsuario) {
				addToAprobaciones(new Aprobacion(fase: fase, grupo: sistema.grupoAdministrador, usuario: null, fecha: new Date(), estado: Aprobacion.PENDIENTE, rol: RolAplicacion.ADMINISTRADOR_FUNCIONAL, descripcionAprobacionSolicitada: "Por requerir manual de usuario"))
			}
			if (impactaBaseArquitectura) {
				addToAprobaciones(new Aprobacion(fase: fase, grupo: sistema.grupoReferenteSWF, usuario: legajoUsuarioResponsable, fecha: new Date(), estado: Aprobacion.PENDIENTE, rol: RolAplicacion.RESPONSABLE_SWF, descripcionAprobacionSolicitada: "Por impacto en BCA"))
			}
			if (actualizaCarpetaOperativa) {
				addToAprobaciones(new Aprobacion(fase: fase, grupo: sistema.grupoReferenteSWF, usuario: legajoUsuarioResponsable, fecha: new Date(), estado: Aprobacion.PENDIENTE, rol: RolAplicacion.RESPONSABLE_SWF, descripcionAprobacionSolicitada: "Por impacto en Carpeta Operativa"))
			}
		}
	}

	boolean aplicaAprobacionImplantacion() {
		return (fasesRealizadas() + faseActual).contains(faseAprobacionImplantacionHijo()) && !denegoEspecificacion()
	}

	public def generarActividadesSistema(Fase fase, String usuarioLogueado) {
		getTiposActividadObligatorios(fase).each { tipoActividad ->
			//addToActividades(new Actividad(tipoActividad: tipoActividad, fase: fase, estado: EstadoActividad.abierta(), grupoResponsable: grupoResponsable, legajoUsuarioResponsable: legajoUsuarioResponsable, pedido: this))
			agregarActividad(tipoActividad, fase, grupoResponsable, legajoUsuarioResponsable, usuarioLogueado, tipoActividad.descripcion)
		}
	}

	def getTiposDeActividadDeFase(fase, tipoGestion) {
		return TipoActividad.createCriteria().list() {
			eq("fase", fase)
			eq("tipoGestion", tipoGestion)
			eq("tipoImpacto", this.tipoImpacto)
		}
	}

	public def generarActividadesSoporte(Fase fase, String usuarioLogueado) {
		//		def tareasSoporte = this.getPlanificacionActual().detalles.collect { detalle -> detalle.tareaSoporte }

		this.getPlanificacionActual().detalles.each { detalle ->
			def tarea = detalle.tareaSoporte
			Actividad nuevaActividad = new Actividad()
			nuevaActividad.tipoActividad = tarea.tipoActividad
			nuevaActividad.fase = fase
			nuevaActividad.fechaSugerida = detalle.fechaHasta
			nuevaActividad.estado = EstadoActividad.abierta()
			nuevaActividad.grupoResponsable = grupoResponsable
			nuevaActividad.legajoUsuarioResponsable = legajoUsuarioResponsable
			nuevaActividad.pedido = this
			nuevaActividad.descripcion = tarea.descripcion
			nuevaActividad.agregarComentario(detalle.comentarioDetalleAreaSoporte, legajoUsuarioResponsable, new UmeService().getUsuario(legajoUsuarioResponsable).nombreApellido)
			this.agregarActividad(nuevaActividad, usuarioLogueado, false)
		}
	}

	public void agregarActividad(TipoActividad unTipoActividad, Fase unaFase, String unGrupoResponsable, String unUsuarioResponsable, String usuarioLogueado, String unaDescripcion) {
		Actividad nuevaActividad = new Actividad(tipoActividad: unTipoActividad, fase: unaFase, estado: EstadoActividad.abierta(), grupoResponsable: unGrupoResponsable, legajoUsuarioResponsable: unUsuarioResponsable, pedido: this, descripcion:unaDescripcion)
		this.agregarActividad(nuevaActividad, usuarioLogueado, false)
	}

	public void agregarActividad(Actividad actividad, String usuarioLogueado, boolean onLine) {
		addToActividades(actividad)
		def unRol = null
		if (onLine) {
			unRol = pedidoPadre.getRol(pedidoPadre.faseActual, usuarioLogueado)
			if (!unRol) {
				unRol = getRol(faseActual, usuarioLogueado)
			}
		}
		def usuarioHistorial = null
		def tipoEvento = LogModificaciones.ACTIVIDAD_GENERADA_AUTOMATICA
		if (onLine) {
			usuarioHistorial = usuarioLogueado
			tipoEvento = LogModificaciones.ACTIVIDAD_GENERADA_MANUAL
		}
		addToLogModificaciones(new LogModificaciones(legajo: usuarioHistorial, fechaDesde: new Date(), rol: unRol, fechaHasta: new Date(), fase: fase, descripcionEvento: "Generaci\u00F3n actividad " + actividad.tipoActividad, tipoEvento: tipoEvento, actividad: actividad))
	}


	public def reenviar(usuarioLogueado, observaciones) {
		fechaCierreEspecificacion = null
		fechaCierre = null
		this.getPlanificacionActual().reenviar()
		addToAprobaciones(new Aprobacion(areaSoporte: areaSoporte, fase: faseActual, grupo: null, usuario: legajoUsuarioResponsable, fecha: new Date(), estado: Aprobacion.PENDIENTE, rol: getRol(legajoUsuarioResponsable)))
		addToLogModificaciones(new LogModificaciones(legajo: usuarioLogueado, fechaDesde: new Date(), rol: pedidoPadre.getRol(faseActual, usuarioLogueado), fechaHasta: new Date(), fase: pedidoPadre.faseActual, tipoEvento: LogModificaciones.REENVIAR, descripcionEvento: this.toString() + ": " +observaciones))
	}

	public def obtenerLogReenviar(){
		def logReenviar = logModificaciones.findAll { logModif ->
			logModif?.tipoEvento.equals(LogModificaciones.REENVIAR)
		}
		if (logReenviar.isEmpty()){
			return null
		}else{
			return logReenviar.last()
		}
	}

	public boolean cerroEspecificacion() {
		return fechaCierreEspecificacion != null
	}

	public boolean denegoEspecificacion() {
		def aprobaciones = aprobacionesDeFase(faseEspecificacionImpacto())
		if (aprobaciones.isEmpty()) {
			return false
		} else {
			return aprobaciones.last().estaDenegado()
		}
	}

	public boolean tieneEspecificacionPendiente() {
		def aprobaciones = aprobacionesDeFase(faseEspecificacionImpacto())
		if (aprobaciones.isEmpty()) {
			return false
		} else {
			return aprobaciones.last().estaPendiente()
		}
	}

	public boolean cumplioFecha() {
		cumplioFecha(faseActual)
	}

	public boolean cumplioFecha(fase) {
		return getFecha(fase) != null
	}

	public Date getFecha(fase) {
		if (fase.equals(faseEspecificacionImpacto())) {
			return fechaCierreEspecificacion
		}

		if (fasesConstruccionPosibles().contains(fase)) {
			return fechaFinActividades
		}

		if (fase.equals(faseEjecucionPAU())) {
			return fechaFinEjecucionPAU
		}

		if (fase.equals(faseAprobacionPAUHijo())) {
			return fechaAprobacionPAU
		}

		if (fase.equals(faseAprobacionImplantacionHijo())) {
			return fechaAprobacionImplantacion
		}

		if (fase.equals(faseImplantacionHijo())) {
			return fechaImplantacion
		}

		if (fase.equals(faseNormalizacionHijo())) {
			return fechaNormalizacion
		}

		return null
	}

	public boolean avanzar(aprobacionUI, usuarioLogueado) {
		this.avanzar(aprobacionUI, usuarioLogueado, false)
	}

	public List getGruposAsignatariosActividad() {
		def gruposTotal = [
			Actividad.USUARIO_FINAL,
			Actividad.INTERLOCUTOR_USUARIO,
			grupoResponsable
		]
		
		if (sistema) {
			def umeService = new UmeService()
			def grupos = sistema.softwareFactories.collect { swf -> swf.grupoLDAP }.grep{swf ->	!umeService.getUsuariosGrupoLDAP(swf, null)?.isEmpty()}

			gruposTotal = gruposTotal.plus(grupos)
		}
		
		return gruposTotal
	}

	public boolean finalizarFase(aprobacionUI, usuarioLogueado) {
		this.avanzar(aprobacionUI, usuarioLogueado, true)
	}

	public boolean retroceder(aprobacionUI, usuarioLogueado) {
		def fase = faseActual
		def usuarioAprobador
		def grupoAprobador = grupoResponsable
		def descripcionEvento = ""
		def descripcionAdicionalOmitida = ""
		def tieneDenegacionOmitida = tieneDenegacionOmitida()
		def generaAprobacion = true
		boolean rechazarTodos = true

		if (!fase) {
			fase = faseEspecificacionImpacto()
		}
		def aprobacionesPendientesFaseActual = aprobaciones.findAll {aprobacion  -> aprobacion.pendienteParaFase(fase) }

		log.info "Retrocediendo desde la fase " + fase

		if (fase.equals(faseEspecificacionImpacto())) {
			descripcionEvento = "Denegaci\u00F3n Especificaci\u00F3n Cambio. "
			descripcionAdicionalOmitida = "Coordinador = RSWF"
			generaAprobacion = false

			//NICO: Le puse la fecha para que cierre el hijo, esto probablemente no quede asi, es temporal.
			fechaCierreEspecificacion = new Date()
			fechaCierre = new Date()
			// no debería generar ninguna aprobaci\u00F3n adicional, el hijo queda trunco por el momento
			// ver más adelante
		}

//		Comentado ya que en Especificación Impacto quiere ir a Consolidación Impacto por error en scaffolding
//		if (fase.faseDenegacion) {
//				faseActual = faseDenegacion
//		}
		
		if (fase.equals(faseAprobacionPAUHijo())) {
			descripcionEvento = "Denegaci\u00F3n Aprobacion PAU. "
			faseActual = faseEjecucionPAU()
			fechaFinEjecucionPAU = null
		}

		if (fase.equals(faseAprobacionImplantacionHijo())) {
			//			Req: el pedido deberá quedar en la Fase a la espera que el RSWF tome una decisi\u00F3n acerca de cuál Proceso de
			//			Soporte activar: Replanificaci\u00F3n, Cambio de Alcance, Cancelaci\u00F3n (ver c\u00F3mo se implementa
			//			if (this.realizaPAU()) {
			//				faseActual = faseAprobacionPAUHijo()
			//			} else {
			//				faseActual = faseConstruccionCambioHijo()
			//			}
			descripcionEvento = "Denegaci\u00F3n Aprobacion Implantaci\u00F3n. "
			fechaAprobacionImplantacion = null
			rechazarTodos = false
			generaAprobacion = false
		}

		usuarioAprobador = getUsuarioResponsable(faseActual, usuarioLogueado)

		if (aprobacionUI.justificacion) {
			descripcionEvento += "Justificaci\u00F3n: " + aprobacionUI.justificacion + ". "
		}

		if (tieneDenegacionOmitida) {
			descripcionEvento = descripcionEvento + descripcionAdicionalOmitida
		}

		if (rechazarTodos) {
			aprobacionesPendientesFaseActual.each { aprobacion ->
				aprobacion.rechazar()
				if (usuarioLogueado.equalsIgnoreCase(aprobacion?.usuario)) {
					aprobacion.justificacion = aprobacionUI?.justificacion
				}
			}
		}
		addToLogModificaciones(new LogModificaciones(legajo: usuarioLogueado, fase: fase, fechaHasta: new Date(), fechaDesde: fechaDesdePara(fase), descripcionEvento: descripcionEvento, tipoEvento: tipoEventoDenegacion(), rol: getRol(faseActual, usuarioLogueado)))

		if (generaAprobacion) {
			def aprobacion = new Aprobacion(areaSoporte: areaSoporte, fase: faseActual, grupo: grupoAprobador, usuario: usuarioAprobador, fecha: new Date(), estado: Aprobacion.PENDIENTE, rol: getRol(faseActual, usuarioAprobador))
			addToAprobaciones(aprobacion)
		}

		fechaUltimaModificacion = new Date()
		this.notificarCambioEstado()
	}

	def doGuardar(usuarioLogueado) {
		// Por ahora no hacemos nada
	}

	public boolean tieneAprobacionOmitida() {
		if (faseActual.equals(faseEspecificacionImpacto()) || faseActual.equals(faseConsolidacionImpacto())) {
			return pedidoPadre.coordinadorEsResponsable()
		}
		return false
	}

	def habilitaAcciones(usuarioLogueado) {
		return legajoUsuarioResponsable?.equalsIgnoreCase(usuarioLogueado)
	}

	def inicializaAsignatario(usuarioLogueado){
		return this.legajoUsuarioResponsable == null
	}

	def softwareFactories() {
		if (sistema) {
			return sistema.softwareFactories
		} else {
			return []
		}
	}

	boolean tieneAreaSoporte() {
		return areaSoporte != null
	}

	boolean tieneSoftwareFactory() {
		return tipoImpacto.asociadoSistemas
	}

	boolean tieneAdministradorFuncional() {
		return !tipoImpacto.asociadoSistemas && tipoImpacto.sistemaImpactado
	}

	def getUsuariosFinales() {
		return pedidoPadre.getUsuariosFinales()
	}

	def getGerentesUsuarios() {
		return pedidoPadre.getGerentesUsuarios()
	}

	def getInterlocutoresUsuarios() {
		return pedidoPadre.getInterlocutoresUsuarios()
	}

	def getUsuariosGestionDemanda() {
		return pedidoPadre.getUsuariosGestionDemanda()
	}

	def getUsuariosCoordinadoresCambio() {
		return pedidoPadre.getUsuariosCoordinadoresCambio()
	}

	def getUsuariosAreasSoporte() {
		return getUsuariosDeHijosQueCumplen { this.tieneAreaSoporte() }
	}

	def getUsuariosResponsablesSWF() {
		return getUsuariosDeHijosQueCumplen { this.tieneSoftwareFactory() }
	}

	def getUsuariosSoftwareFactory() {
		if (tieneSoftwareFactory()) {
			return softwareFactories().inject([], {usuarios, swf -> usuarios + swf.responsableCargaCapacidad })
		} else {
			return []
		}
	}
	
	def getGruposResponsables() {
		return [grupoResponsable]
	}

	def getAreasSoporteImpactadas() {
		return pedidoPadre.areasSoporteImpactadas
	}

	def getAreasSoporteNoImpactadas() {
		return pedidoPadre.areasSoporteNoImpactadas
	}

	def getAdministradoresFuncionalesImpactados() {
		return pedidoPadre.administradoresFuncionalesImpactados
	}

	def getUsuariosAdministradoresFuncionales() {
		return getUsuariosDeHijosQueCumplen { this.tieneAdministradorFuncional() }
	}

	def getUsuariosDeHijosQueCumplen(condicion) {
		if (condicion) {
			return [legajoUsuarioResponsable]
		} else {
			return []
		}
	}

	def getUsuariosAprobadoresImpacto() {
		return pedidoPadre.getUsuariosAprobadoresImpacto()
	}

	def getUsuariosGestionOperativa() {
		// si es de sistema hay que asociarle la gesti\u00F3n operativa...
		// falta enganchar cada sistema con su gesti\u00F3n operativa correspondiente
		if (!sistema || !sistema?.grupoLDAPGestionOperativa) {
			return []
		}
		return new UmeService().getUsuariosGrupoLDAP(sistema.grupoLDAPGestionOperativa, null).collect { it.legajo }
	}

	def getUsuarioResponsable(fase, usuarioLogueado) {
		if (fase.equals(faseAprobacionPAUHijo())) {
			return pedidoPadre.legajoInterlocutorUsuario
		} else {
			return legajoUsuarioResponsable
		}
	}

	public Set getUsuarios() {
		Set usuarios = new HashSet()
		if (legajoUsuarioResponsable) {
			usuarios.add(legajoUsuarioResponsable)
		}
		return usuarios
	}

	def getLegajo(String rol) {
		if (rolDefault && rol.equals(rolDefault)) {
			return legajoUsuarioResponsable
		}
		return null
	}

	boolean consolidaPAU() {
		// opcion 1: las áreas de soporte/administradores funcionales/los de SAP
		// en general, todos los que no tienen estrategia de prueba... no consolidan
		return this.estrategiaPruebaActual?.consolidaPAU() && !denegoEspecificacion()
	}

	boolean realizaPAU() {
		return this.estrategiaPruebaActual?.realizaPAU() && !denegoEspecificacion()
	}

	boolean estaEnConstruccion() {
		return fasesReasignables().contains(this.faseActual)
	}

	def faseConstruccion() {
		fasesConstruccionPosibles().find { fasePosible -> tipoImpacto.fases.contains(fasePosible) }
	}

	def fasesConstruccionPosibles() {
		[
			faseConstruccionCambioHijo(),
			faseConstruccionPU(),
			faseAdministrarActividades()
		]
	}

	def fasesReasignables() {
		[
			faseConstruccionCambioHijo(),
			faseConstruccionPU(),
			faseAdministrarActividades(),
			faseEjecucionPAU(),
			faseImplantacionHijo(),
			faseNormalizacionHijo(),
			faseEspecificacionImpacto()
		]
	}

	def actividadesPendientesPara(usuarioLogueado) {
		return actividades.findAll { actividadPosible -> actividadPosible?.legajoUsuarioResponsable?.equalsIgnoreCase(usuarioLogueado) && actividadPosible?.estado.equals(EstadoActividad.abierta()) }
	}

	boolean aproboAprobacionPAU(){
		def aprobacionesOk  = aprobaciones.findAll { aprobacion ->
			aprobacion.fase.equals(this.faseAprobacionPAUHijo()) &&
					aprobacion.estaAprobado()}
		return !aprobacionesOk.isEmpty()
	}

	boolean aproboAprobacionImplantacion(){
		def aprobacionesOk  = aprobaciones.findAll { aprobacion ->
			aprobacion.fase.equals(this.faseAprobacionImplantacionHijo()) &&
					aprobacion.estaAprobado()}
		return !aprobacionesOk.isEmpty()
	}

	public getJustificacionEspecificacionImpacto() {
		return justificacion(faseEspecificacionImpacto())
	}

	public getJustificacionAP() {
		return justificacion(faseAprobacionPAUHijo())
	}

	public getJustificacionAI(unUsuario) {
		return justificacion(faseAprobacionImplantacionHijo(), unUsuario)
	}

	public mostrarCHPasajes(){
		if (faseActual.equals(faseImplantacionHijo()) || faseActual.equals(faseNormalizacionHijo())) {
			return true
		}
		return false
	}

	def reasignarUsuarioEnAprobacion(legajoAnterior, legajoNuevo, fase){
		def aprobacionesFiltradas = aprobacionesDeFase(fase).findAll {aprobacion  -> aprobacion.usuario?.equalsIgnoreCase(legajoAnterior) }
		aprobacionesFiltradas.each { aprobacion -> aprobacion.usuario = legajoNuevo	}
	}

	def getFase() {
		if (!faseActual) {
			faseActual = FaseMultiton.getFase(FaseMultiton.ESPECIFICACION_IMPACTO_HIJO)
		}
		faseActual
	}

	def validarActividades() {
		def actividadesDeLaFase = getActividadesActuales()
		log.info "Actividades de fase actual: " + actividadesDeLaFase
		def actividadesSinFinalizar = actividadesDeLaFase.any { actividad  -> !actividad.estaFinalizada() }
		log.info "Actividades sin finalizar: " + actividadesSinFinalizar
		if (actividadesSinFinalizar) {
			this.errors.reject "A\u00FAn quedan actividades sin finalizar"
		}

		def tiposActividadObligatorios = this.getTiposActividadObligatorios(this.faseActual)
		log.info "Fase actual del pedido: " + this.faseActual
		log.info "Tipos de actividad obligatorios: " + tiposActividadObligatorios
		def actividadesObligatoriasCerradas = actividadesDeLaFase.findAll { actividad -> actividad.esExitosa() }
		log.info "Actividades Obligatorias cerradas: " + actividadesObligatoriasCerradas
		def tiposActividadesObligatoriasCerradas = actividadesObligatoriasCerradas.collect { actividad -> actividad.tipoActividad }
		log.info "Tipos de Actividad Obligatorias cerradas: " + tiposActividadesObligatoriasCerradas
		def tiposActividadFaltantes = tiposActividadObligatorios - tiposActividadesObligatoriasCerradas
		log.info "Tipos de actividad faltantes: " + tiposActividadFaltantes
		if (!tiposActividadFaltantes.isEmpty()) {
			this.errors.reject "Falta cerrar como EXITOSA actividades del tipo  " + tiposActividadFaltantes
		}

		if (tieneDisenioInterno(faseActual)){
			if (impactaBaseArquitectura==null){
				this.errors.reject "Debe seleccionar una opci\u00F3n en Impacta en BCA"
			}
			if (requiereManualUsuario==null){
				this.errors.reject "Debe seleccionar una opci\u00F3n en Requiere Manual de Usuario"
			}
			if (actualizaCarpetaOperativa==null){
				this.errors.reject "Debe seleccionar una opci\u00F3n en Carpeta Operativa"
			}
		}

		actividadesDeLaFase.each { actividad -> actividad.validar(this)	}

		return this.errors.errorCount == 0
	}

	private def getTiposActividadObligatorios(Fase fase) {
		log.info "Fase: " + fase
		def tiposActividades = getTiposDeActividadDeFase(fase, this.parent.tipoGestion)
		log.info "Tipos de actividades original: " + tiposActividades
		def tiposActividad = tiposActividades.findAll { tipoActividad ->
			log.info "Tipo de actividad: " + tipoActividad
			log.info "Es obligatoria?: " + tipoActividad?.esObligatoria(this)
			tipoActividad.esObligatoria(this) } //&& tipoActividad.tipoGestion.equals(this.parent.tipoGestion) }
		log.info "Tipos de actividades filtradas: " + tiposActividad
		return tiposActividad
	}

	def getActividadesActuales() {
		def actividadesActuales = actividades.findAll { actividad ->
			actividad.fase.equals(this.faseActual)}

		return actividadesActuales
	}

	def getActividadesDeFase(codigoFase) {
		def actividadesActuales = actividades.findAll { actividad ->
			actividad.fase.codigoFase.equals(codigoFase)}

		return actividadesActuales
	}

	def tieneActividadesPendientes(usuarioLogueado) {
		return !actividadesPendientesPara(usuarioLogueado).isEmpty()
	}

	def fasesRealizadas() {
		return tipoImpacto.fasesAnterioresA(faseActual) + pedidoPadre.fasesRealizadas()
	}
	
	def fasesNoRealizadas() {
		return tipoImpacto.fasesPosterioresA(faseActual) + pedidoPadre.fasesNoRealizadas()
	}

	def especificacionDenegada() {
		def aprobacion = ultimaAprobacion(faseEspecificacionImpacto())
		if (aprobacion) {
			return aprobacion.estaDenegado()
		} else {
			return false
		}
	}

	def traerDatosRelacionados() {
		super.traerDatosRelacionados()
		actividades.each { actividad -> actividad.estado }
	}

	boolean tieneDisenioInterno(fase){
		return actividades.any { actividad -> actividad.esDeDisenioInterno(fase) }
	}

	boolean actualizaTildes() {
		return faseActual.equals(faseConstruccionCambioHijo())
	}

	def getLinkURL() {
		return getLinkURL("NeedIT")
	}

	def getLinkURL(descripcion) {
		return "<a href='" + ConfigurationHolder.config.grails.serverURL + "/" + faseActual.controllerNombre + "/index/"+id+"?pedidoId=" + pedidoPadre.id + "&impacto=" + id + "'>"+descripcion+"</a>"
	}

	boolean apruebaPAU() {
		return tipoImpacto.apruebaPAU() && !denegoEspecificacion()
	}

	def getAllMailsPendientes() {
		return mailsPendientes + pedidoPadre.mailsPendientes
	}

	def getTareasSoporte(){
		return getPlanificacionActual()?.tareaSoporte
	}

	def getGrupoGestionDemanda() {
		return parent.grupoGestionDemanda
	}

	boolean asociadoSistemas() {
		return tipoImpacto.asociadoSistemas
	}

	def getLegajoInterlocutorUsuario() {
		return pedidoPadre.legajoInterlocutorUsuario
	}

	def getLegajoGerenteUsuario() {
		return pedidoPadre.legajoGerenteUsuario
	}

	def getLegajoUsuarioGestionDemanda() {
		return pedidoPadre.legajoUsuarioGestionDemanda
	}

	def getLegajoCoordinadorCambio() {
		return pedidoPadre.legajoCoordinadorCambio
	}

	def getLegajoUsuarioAprobadorEconomico() {
		return pedidoPadre.legajoUsuarioAprobadorEconomico
	}

	def getPedidosHijos() {
		return []
	}

	def puedeAgregarOtraPrueba(){
		if(parent.pedidoDeEmergencia() || !realizaPAU() || !parent.obtenerUltimaEspecificacion()?.realizaPAU){
			return false
		}

		return tipoImpacto.aplicaOtrasPruebas()
	}

	def validaPruebaOpcional(tipoPrueba) {
		if (tipoPrueba.opcional && !puedeAgregarOtraPrueba()) {
			this.errors.reject "Estrategia de prueba: No puede agregar prueba opcional"
		}
	}
		
	def getDetallesConsolidados(detalles) {
		return detalles
	}

	def obtenerDetallesPlanificacion(grupo, impacto){
		getPlanificacionActual().obtenerDetallesPlanificacion(grupo, origen)
	}

	def pedidoDeEmergencia(){
		return pedidoPadre.pedidoDeEmergencia()
	}

	def getOrigen() {
		return sistema ?: areaSoporte
	}

	def grupoTieneCargadoRealIncurrido(grupo){
		return getPlanificacionActual().grupoTieneCargadoRealIncurrido(grupo, origen)
	}

	def grupoTieneCerradoRealIncurrido(grupo){
		return getPlanificacionActual().grupoTieneCerradoRealIncurrido(grupo, origen)
	}

	def textoSinGrupoRealIncurrido(){
		if(tieneSoftwareFactory()){
			return "La gesti\u00F3n derivada "+ this.toString() +" fue denegada en especificar impacto."
		}else{
			return "El pedido "+ this.toString() +" no tiene actividades de planificaci\u00F3n generadas en la fase Especificar impacto."
		}
	}

	boolean esSWDesarrolloSAP(){
		return tipoImpacto.esSWDesarrolloSAP()
	}

	def getDescripcion(){
		return ""
	}

	def getAlcance(){
		return ""
	}

	def getObjetivo(){
		return ""
	}

	def getTipoPedido(){
		return null
	}

	def getFechaDeseadaImplementacion() {
		return null
	}

	def getSistemaSugerido(){
		return null
	}

	def getTipoReferencia(){
		return null
	}

	def getReferenciaOrigen(){
		return ""
	}

	def getActividad(Integer idActividad) {
		return actividades.find { actividad -> actividad.id == idActividad }
	}

	boolean puedeReenviar(usuarioLogueado) {
		return cerroEspecificacion() && parent.legajoCoordinadorCambio.equals(usuarioLogueado) && parent.faseActual.equals(faseConsolidacionImpacto()) && !estaSuspendido()
	}

	def getTipoGestion(){
		return pedidoPadre.tipoGestion
	}

	def puedeSuspender(usuarioLogueado){
		return false
	}

	def puedeSuspender(usuarioLogueado, generaErrores){
		return false
	}
	
	def puedeReanudar(usuarioLogueado){
		return false
	}

	def puedeReanudar(usuarioLogueado, generaErrores){
		return false
	}
	
	def enSeguimiento(usuarioLogueado) {
		// Contempla pedidos suspendidos, replanificados o cancelados porque no queda ninguna aprobaci\u00F3n pendiente
		return aprobaciones.any { aprobacion -> usuarioLogueado?.equals(aprobacion?.usuario) && !aprobacion?.estaPendiente() } && this.tieneAprobacionesPendientes()
	}

	def getDescripcionLogSuspension() {
		return "Suspensi\u00F3n de gesti\u00F3n derivada " + numero + " - " + tipoPlanificacion
	}
	
	def getDescripcionLogReanudacion(comentario) {
		return "Reanudaci\u00F3n de gesti\u00F3n derivada " + numero + " - " + tipoPlanificacion
	}

	def getRolLogSuspension() {
		return null
	}

	def getUsuarioLogSuspension(usuarioLogueado) {
		return null
	}
	
	def getCodigoDireccionGerencia(){
		return parent.codigoDireccionGerencia
	}
	
	def getDescripcionDireccionGerencia(){
		return parent.descripcionDireccionGerencia
	}
	
	def getActividadesPendientes(grupoActividad, legajoActual) {
		actividades.findAll { actividad -> actividad.pendientePara(grupoActividad, legajoActual)  }
	}

	def setearDireccionDe(unLegajo) {
		// no hace nada, esto se setea sólo en el padre
	}
	
	def aprobacionesPendientes(String codigoRol, unUsuario) {
		aprobaciones.findAll { aprobacion -> unUsuario?.equalsIgnoreCase(aprobacion.usuario) && aprobacion.estaPendiente() && codigoRol?.equalsIgnoreCase(aprobacion.rol) }
	}
}
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                               