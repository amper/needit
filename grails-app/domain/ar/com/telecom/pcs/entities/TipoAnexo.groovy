package ar.com.telecom.pcs.entities

class TipoAnexo {
	
	Fase fase
	String descripcion
	
	
	public static final String suspension = "Suspensi\u00F3n"
	public static final String reanudacion = "Reanudaci\u00F3n"
	
    static constraints = {
		descripcion(size:1..50)
		fase(nullable: true)
    }
	
	static mapping = {
		fase fetch: 'join'
	}
	
	String toString() {
		descripcion
	}
	
	boolean esDeFase(Fase unaFase) {
		return fase.equals(unaFase)
	}

	boolean esSuspension(){
		return descripcion.equalsIgnoreCase(suspension)
	}	

	boolean esReanudacion(){
		return descripcion.equalsIgnoreCase(reanudacion)
	}
	
}
