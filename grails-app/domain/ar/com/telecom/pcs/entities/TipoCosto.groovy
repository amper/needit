package ar.com.telecom.pcs.entities

class TipoCosto {

	String descripcion
	
    static constraints = {
		descripcion(size:1..50)
    }
	
	String toString() {
		"${descripcion}"
	}
}
