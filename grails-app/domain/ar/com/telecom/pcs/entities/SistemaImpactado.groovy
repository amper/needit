package ar.com.telecom.pcs.entities

import ar.com.telecom.ume.UmeService

class SistemaImpactado {

	Sistema sistema
	TipoImpacto tipoImpacto
	String grupoReferente
	String usuarioResponsable

	static constraints = {
		grupoReferente(size: 1..100)
		usuarioResponsable(maxSize: 20, nullable: true)
	}

	static mapping = {
		cascade: 'all,delete-orphan'
		sistema fetch: 'join'
		tipoImpacto fetch: 'join'
	}

	def areaSoporte() {
		return null
	}

	String toString() {
		"${sistema} - ${tipoImpacto}"
	}

	@Deprecated
	/**
	 * Se reemplaza por getGrupoResponsable()
	 */
	def grupoResponsable() {
		return grupoReferente
	}

	@Deprecated
	/**
	 * Se reemplaza por getLegajoUsuarioResponsable()
	 */
	def usuarioResponsable() {
		return usuarioResponsable
	}

	def tipoImpacto() {
		return tipoImpacto
	}

	def sistema() {
		return sistema
	}

	def validar(AbstractPedido pedido) {
		if (!grupoReferente || grupoReferente.equals("")) {
			pedido.errors.reject("No est\u00E1 definido el grupo referente para el sistema " + this)
		}

		def umeService = new UmeService()

		if (umeService.getUsuariosGrupoLDAP(grupoReferente, null).isEmpty()) {
			pedido.errors.reject("El grupo " + grupoReferente + " para el sistema " + this + " no tiene definido usuarios")
		}

		pedido.obtenerUltimaEspecificacion().validarSistemasImpactadosDuplicados(pedido)

		return pedido.errors.errorCount == 0
	}

	def getGrupoResponsable() {
		return grupoReferente
	}
	
	def getLegajoUsuarioResponsable() {
		return usuarioResponsable
	}
	
	boolean tieneSoftwareFactory() {
		return tipoImpacto.asociadoSistemas
	}

	boolean tieneAdministradorFuncional() {
		return !tipoImpacto.asociadoSistemas && tipoImpacto.sistemaImpactado
	}

	def getUsuariosSoftwareFactory() {
		[usuarioResponsable]
	}
	
}
