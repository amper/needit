package ar.com.telecom.pcs.entities

class StockMensualSWF {
	
	Date fechaDesde
	Date fechaHasta
	int stockMensual
	BigDecimal valor

    static constraints = {
		fechaDesde(nullable: true)
		fechaHasta(nullable: true)
    }
	
	static belongsTo = [ softwareFactory : SoftwareFactory ]
	
	String toString() {
		return fechaDesde.toString() + " - " + fechaHasta.toString() + " (" + stockMensual + " / " + valor + ")"
	}
}
