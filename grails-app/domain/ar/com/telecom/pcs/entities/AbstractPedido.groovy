package ar.com.telecom.pcs.entities

import java.util.Date
import java.util.List

import org.codehaus.groovy.grails.commons.ConfigurationHolder

import ar.com.telecom.Constantes
import ar.com.telecom.FaseMultiton
import ar.com.telecom.pedido.observers.PedidoAprobacionesCambioEstadoObserver
import ar.com.telecom.pedido.observers.PedidoCambioDireccionObserver;
import ar.com.telecom.ume.UmeService
import ar.com.telecom.util.DateUtil

class AbstractPedido {

	Integer id
	String titulo
	String legajoUsuarioCreador
	Date fechaCargaPedido
	Date fechaUltimaModificacion
	Fase faseActual
	Date fechaCancelacion
	String motivoCancelacion
	Date fechaCierre
	Date fechaReplanificacion
	String comentarioRealesIncurridos

	Date fechaSuspension
	MotivoSuspension motivoSuspension
	String comentarioSuspension
	String comentarioReanudacion

	/**
	 * Inbox de aprobaciones por grupo/usuario es común
	 */
	List aprobaciones

	/**
	 * Historial de modificaciones
	 */
	List logModificaciones

	/**
	 * Planificaciones y esfuerzos de sistemas
	 */
	List planificaciones
	List estrategiasPrueba
	List diseniosExternos

	/**
	 * TRANSIENTS
	 */
	def observers
	def mailsPendientes
	def comentarioReasignacion
	def rqSimplitReasignacion
	
	static constraints = {
		titulo(size:1..200)
		legajoUsuarioCreador(nullable:true)
		fechaUltimaModificacion(nullable:true)
		fechaCargaPedido(nullable:true)
		fechaCancelacion(nullable:true)
		fechaSuspension(nullable:true)
		fechaReplanificacion(nullable:true)
		motivoCancelacion(nullable: true)
		motivoSuspension(nullable: true)
		comentarioSuspension(nullable: true)
		comentarioReanudacion(nullable: true)
		comentarioRealesIncurridos(nullable: true)
		fechaCierre(nullable: true)
		comentarioReasignacion(nullable: true)
		rqSimplitReasignacion(nullable: true)
	}

	static hasMany = [ aprobaciones : Aprobacion, logModificaciones : LogModificaciones, anexos: AnexoPorTipo, planificaciones: PlanificacionEsfuerzo, estrategiasPrueba: EstrategiaPrueba, diseniosExternos: DisenioExterno ]

	static mapping = {
		id (generator: 'identity', params: [initial_value: 40000])
		discriminator value: 'O'
		faseActual fetch: 'join'
		version false
		motivoCancelacion(type: 'text')
	}

	String toString() {
		"${getNumero()} - ${titulo}"
	}

	public AbstractPedido() {
		aprobaciones = new ArrayList()
		logModificaciones = new ArrayList()
		fechaCargaPedido = new Date()
		planificaciones = new ArrayList()
		mailsPendientes = new ArrayList()
		anexos = [] 
		crearObserversIniciales()
	}

	def crearObserversIniciales() {
		//WORKAROUND -> Ver dsp porque en pedidoservice.avanzar, reinicia los observers.
			if (observers?.contains(new PedidoCambioDireccionObserver())){
				observers = observers + [ new PedidoAprobacionesCambioEstadoObserver()]
			}else{
				observers = [ new PedidoAprobacionesCambioEstadoObserver(),
				              new PedidoCambioDireccionObserver() ]
			}
	}

	def getDescripcionStatus() {
		if (isCancelado()) {
			return "(cancelado el " + DateUtil.toString(fechaCancelacion) + ")"
		} else {
			if (isSuspendido()) {
				return "(suspendido el " + DateUtil.toString(fechaSuspension) + ")"
			}
			if (isEnReplanificacion()) {
				return "(en replanificación desde" + DateUtil.toString(fechaReplanificacion)  + ")"
			}
			if (isCerrado()) {
				return "(cerrado el " + DateUtil.toString(getFechaDeCierre())  + ")"
			}
			return ""
		}
	}

	def isCancelado() {
		return this.fechaCancelacion != null
	}

	def isSuspendido() {
		return this.fechaSuspension != null
	}
	
	def isEnReplanificacion(){
		return this.fechaReplanificacion != null
	}

	def agregarObserver(observer) {
		observers.add(observer)
	}

	def agregarMailPendiente(envioMail) {
		mailsPendientes.add(envioMail)
	}

	def traerDatosRelacionados() {
		anexos?.nombreArchivo
		aprobaciones.each { aprobacion -> aprobacion.traerDatosRelacionados() }
		logModificaciones?.legajo
		planificaciones.each { planif -> planif.traerDatosRelacionados() }
		estrategiasPrueba?.comentario
		diseniosExternos?.comentarioAlcance
	}

	public boolean cancelar(usuarioLogueado) {
		fechaCancelacion = new Date()
		def fecha = new Date()
		addToLogModificaciones(new LogModificaciones(legajo: usuarioLogueado, fechaDesde: fecha, rol: getRol(faseActual, usuarioLogueado), fechaHasta: fecha, fase: this.fase, tipoEvento: LogModificaciones.CANCELACION, descripcionEvento: "Cancelaci\u00F3n del pedido"))
		aprobacionesPendientes.each { aprobacion -> aprobacion.cancelar() }
		return true
	}

	public boolean suspender(usuarioLogueado) {
		fechaSuspension = new Date()
		motivoSuspension = parent.motivoSuspension
		comentarioSuspension = parent.comentarioSuspension
		
		def fecha = new Date()
		//TODO: Agregar "motivoSuspension" en el log. Buscar las aprobaciones pendientes, y pasarlas a un estado nuevo "S" (Suspendido)-> aprobacion.suspender()
		//addToLogModificaciones(new LogModificaciones(legajo: getUsuarioLogSuspension(usuarioLogueado), fechaDesde: fecha, rol: getRolLogSuspension(usuarioLogueado), fechaHasta: fecha, fase: this.fase, tipoEvento: LogModificaciones.SUSPENSION, descripcionEvento: descripcionLogSuspension))
		addToLogModificaciones(new LogModificaciones(legajo: getUsuarioLogSuspension(usuarioLogueado), fechaDesde: fecha, rol: getRolLogSuspension(), fechaHasta: fecha, fase: this.fase, tipoEvento: LogModificaciones.SUSPENSION, descripcionEvento: descripcionLogSuspension))
		aprobacionesPendientes.each { aprobacion -> aprobacion.suspender() }
		return true
	}
	
	def getAprobacionesPendientes() {
		return aprobaciones.findAll { aprobacion -> aprobacion.estaPendiente() }
	}

	def getDescripcionLogSuspension() {
		def descripcion = "Suspensi\u00F3n del pedido - " + motivoSuspension
		if(comentarioSuspension){
			descripcion = descripcion + " - Comentarios:"+ comentarioSuspension
		}
		return descripcion
	}

	def getDescripcionLogReanudacion(comentario) {
		def descripcion = "Reactivaci\u00F3n del pedido "
		if (comentario){
			descripcion = descripcion + " - " + comentario
			this.comentarioReanudacion = comentario
		}
		return descripcion
	}

	def getRolLogSuspension(usuarioLogueado) {
		return getRol(faseActual, usuarioLogueado)
	}

	def getUsuarioLogSuspension(usuarioLogueado) {
		return usuarioLogueado
	}

	public boolean reanudarSuspension(usuarioLogueado, comentario) {
		def fecha = new Date()
//		addToLogModificaciones(new LogModificaciones(legajo: usuarioLogueado, fechaDesde: fecha, rol: getRol(faseActual, usuarioLogueado), fechaHasta: fecha, fase: this.fase, tipoEvento: LogModificaciones.RETOMAR_SUSPENSION, descripcionEvento: descripcion ))
		addToLogModificaciones(new LogModificaciones(legajo: getUsuarioLogSuspension(usuarioLogueado), fechaDesde: fecha, rol: getRolLogSuspension(), fechaHasta: fecha, fase: this.fase, tipoEvento: LogModificaciones.RETOMAR_SUSPENSION, descripcionEvento: getDescripcionLogReanudacion(comentario) ))
		fechaSuspension = null
		motivoSuspension = null
		comentarioSuspension = null
		//TODO: Buscar las aprobaciones suspendidas, y pasarlas a un estado pendiente -> aprobacion.levantarSuspension()
		aprobaciones.findAll { aprobacion -> aprobacion.estaSuspendido() }.each{ aprobacion -> aprobacion.pasarAPendiente() }
		return true
	}

	def tieneAprobacionesPendientes() {
		return aprobaciones.any { aprobacion -> aprobacion && aprobacion.estaPendiente() }
	}

	def tieneAprobacionesPendientes(usuarioLogueado) {
		return !aprobacionesPendientesPara(usuarioLogueado).isEmpty()
	}

	def tieneAprobacionesPendientes(usuarioLogueado, tipoConsulta) {
		return !aprobacionesPendientesPara(usuarioLogueado, tipoConsulta).isEmpty()
	}


	def aprobacionesPendientesPara(usuarioLogueado) {
		return aprobaciones.findAll { aprobacion -> aprobacion.pendienteParaFase(faseActual) && (aprobacion.esResponsableDeAprobar(usuarioLogueado)) }
	}

	def aprobacionesPendientesPara(usuarioLogueado, tipoConsulta) {
		return aprobaciones.findAll { aprobacion -> aprobacion.pendienteParaFase(faseActual) && (aprobacion.esResponsableDeAprobar(usuarioLogueado, tipoConsulta)) }
	}

	def aprobacionesPendientes(Fase fase, String usuarioLogueado) {
		return aprobaciones.findAll { aprobacion -> aprobacion.pendienteParaFase(fase) && (aprobacion.esResponsableDeAprobar(usuarioLogueado)) }
	}
	
	def aprobacionActual(Fase fase, String usuarioLogueado) {
		def aprobacionesActuales = aprobaciones.findAll { aprobacion -> aprobacion.actualParaFase(fase) && (aprobacion.esResponsableDeAprobar(usuarioLogueado)) }
		if (aprobacionesActuales.isEmpty()) {
			return null
		} else {
			return aprobacionesActuales.first()
		}
	}

	def aprobacionPendiente(Fase fase, String usuarioLogueado) {
		def aprobaciones = aprobacionesPendientes(fase, usuarioLogueado)
		if (aprobaciones.isEmpty()) {
			return null
		} else {
			return aprobaciones.first()
		}
	}

	def inicializarObservers() {
		log.info "Inicializar Observers"
		observers.each { observer -> observer.initialize(this) }
	}

	def reasignar(usuarioLogueado, legajoOriginal, legajoNuevo, descripcion, codigoRol) {
		log.info "Se llama a reasignar dentro de pedido. C\u00F3digo de rol: " + codigoRol + " - Legajo original: " + legajoOriginal + " - Legajo nuevo: " + legajoNuevo + " - Descripci\u00F3n: " + descripcion + " - Fase: " + faseActual + " - Fase 2: " + this.fase
		def umeService = new UmeService()
		def fecha = new Date()
		
		def accion = "asigna"
		def tipoEvento = LogModificaciones.ASIGNACION
		def legajoDe = ""
		if (legajoOriginal) {
			legajoDe = " de " + legajoOriginal + " (" + umeService.getUsuario(legajoOriginal)?.nombreApellido + ")"
			accion = "reasigna"
			tipoEvento = LogModificaciones.REASIGNACION
		}
		
		String descripcionLog = ("Se " + accion + " " + descripcion + legajoDe + (legajoNuevo ? " a " + legajoNuevo + " (" + umeService.getUsuario(legajoNuevo)?.nombreApellido + ")" : " a sin asignar"))
		
		if(comentarioReasignacion){
			descripcionLog = descripcionLog + " - " + comentarioReasignacion
		}
		
		if(rqSimplitReasignacion){
			descripcionLog = descripcionLog + " - " + rqSimplitReasignacion
		}
		
		addToLogModificaciones(new LogModificaciones(legajo: usuarioLogueado, fechaDesde: fecha, fechaHasta: fecha, fase: this.fase, rol: codigoRol, tipoEvento: tipoEvento, descripcionEvento: descripcionLog))

//		FUTURO		
//		if(comentarioReasignacion){
//			descripcionLog = descripcionLog + " - " + comentarioReasignacion
//		}
		
//		if (rqSimplitReasignacion) {
//			descripcionLog = descripcionLog + " - " + rqSimplitReasignacion
//		}
//		FIN FUTURO
		
	}

	def aprobacionesPendientes(unUsuario) {
		aprobaciones.findAll { aprobacion -> unUsuario?.equalsIgnoreCase(aprobacion.usuario) && aprobacion.estaPendiente() }
	}

	def aprobacionesPendientes(String codigoRol, unUsuario) {
		aprobaciones.findAll { aprobacion -> unUsuario?.equalsIgnoreCase(aprobacion.usuario) && aprobacion.estaPendiente() && codigoRol?.equalsIgnoreCase(aprobacion.rol) }
	}

	def ultimoLog() {
		return logModificaciones.sort { it.fechaHasta }.last()
	}

	def fechaDesdePara(unaFase) {
		def logsAnteriores = logModificaciones.findAll { log -> log.conFechaDesde() }.sort { it.fechaHasta }
		if (logsAnteriores) {
			return logsAnteriores.last().fechaHasta
		} else {
			return fechaCargaPedido
		}
	}

	def getFechaAprobacion(fase, legajo) {
		def aprobacionesLegajo = aprobaciones.findAll { aprobacion -> aprobacion?.fase?.equals(fase) && legajo?.equalsIgnoreCase(aprobacion?.usuario) }.sort { it.fecha }
		if (aprobacionesLegajo.isEmpty()) {
			return null
		} else {
			return aprobacionesLegajo.last()?.fecha
		}
	}

	def getMacroEstado() {
		return faseActual.macroEstado
	}

	def eliminarAnexo(unAnexo, usuarioLogueado) {
		// OJO, el removeFromAnexos no toma el id, por eso hay que hacer esto
		def anexo = this.anexos.find { anexo -> anexo.id.equals(unAnexo.id) }
		if (anexo.esGlobal()){
			addToLogModificaciones(new LogModificaciones(legajo: usuarioLogueado, fechaDesde: new Date(), fechaHasta: new Date(), fase: faseActual, rol: getRol(faseActual, anexo.legajo), tipoEvento: LogModificaciones.ELIMINAR_ANEXO_GLOBAL, descripcionEvento: "Se elimna el anexo ${anexo}"))
		}
		this.removeFromAnexos(anexo)
	}
	
	def getOrigen(anexoId) {
		def idAnexo = anexoId as int
		
		def origen = buscarAnexo(idAnexo, this)
		if (!origen) {
			origen = buscarAnexo(idAnexo, estrategiaPruebaActual)
		}
		if (!origen) {
			origen = buscarAnexo(idAnexo, disenioExternoActual)
		}
		if (!origen){
			origen = buscarAnexo(idAnexo, obtenerUltimaEspecificacion())
		}
		if (!origen){
			pedidosHijos?.each{ hijo ->
				hijo.actividades.each{ actividad -> 
					if (!origen){
						origen = buscarAnexo(idAnexo, actividad)
					}
				}
			}
		}
		return origen
	}
	
	def buscarAnexo(idAnexo, origen) {
		def anexo = origen?.anexos.find { itAnexo -> itAnexo.id == idAnexo }
		if (anexo) {
			return origen
		}
	}

	
	boolean puedeEliminarAnexoGlobal(unAnexo, usuarioLogueado){
		return (unAnexo.legajo.equalsIgnoreCase(usuarioLogueado) ||
				usuarioLogueado.equalsIgnoreCase(this.legajoInterlocutorUsuario) ||
				usuarioLogueado.equalsIgnoreCase(this.legajoCoordinadorCambio)
		)
	}

	def getNumero() {
		return "" + id
	}

	def getAnexosPorRol(rol) {
		def listAnexos = []
		this.anexos.each { elem ->
			this.getRoles(elem?.legajo).each { elem2 ->
				if (elem2.equals(rol)) listAnexos.add(elem)
			}
		}
		return listAnexos
	}

	def getAnexos(Fase fase) {
		return anexos.findAll { anexo -> anexo.esDeFase(fase) }
	}

	
	def getLegajos(rol){
		def listLegajos = []

		this.anexos?.legajo.each { elem ->
			this.getRoles(elem).each { elem2 ->
				if (elem2.equals(rol) && !listLegajos.contains(elem2)) listLegajos.add(elem)
			}
		}
		return listLegajos
	}

	def getPlanificacionActual() {
		return getActualDe(planificaciones, { planificacion -> planificacion.enEspecificacion() })
	}
	
	def getPlanificacionEnReplanificacion() {
		return getActualDe(planificaciones, { planificacion -> planificacion.enReplanificacion() })
	}
	
	def getDetallesParaReplanificacion(){
		def detallesPlanificacionActual = planificacionActual?.detalles.findAll { detalle -> detalle.esDeSistema()}.sort{ it.id }
		def detallesPlanificacionEnReplanificacion = planificacionEnReplanificacion?.detalles.findAll { detalle -> detalle.esDeSistema()}.sort{ it.id }
		def mapaDetalles = null
		if (planificacionEnReplanificacion && planificacionActual){
			mapaDetalles = [:]
			detallesPlanificacionActual.each { detalle ->
				mapaDetalles.put(detalle.actividadPlanificacion.id, [detalle])
			}
			detallesPlanificacionEnReplanificacion.each{ detalle ->
				if (!mapaDetalles.get(detalle.actividadPlanificacion.id)){
					mapaDetalles.put(detalle.actividadPlanificacion.id, [detalle])
				} else{
					mapaDetalles.put(detalle.actividadPlanificacion.id, mapaDetalles.get(detalle.actividadPlanificacion.id) + detalle)
				}
			}
		}
		return mapaDetalles
	}

	def getEstrategiaPruebaActual() {
		return getActualDe(estrategiasPrueba)
	}

	def getDisenioExternoActual() {
		return getActualDe(diseniosExternos)
	}

	def getActualDe(unaColeccion) {
		return getActualDe(unaColeccion, { obj -> true } )	
	}
	
	def getActualDe(unaColeccion, condicion) {
		if (!unaColeccion || unaColeccion.isEmpty()) {
			return null
		} else {
			return unaColeccion.findAll(condicion)?.last()
		}
	}

	def definirEstrategiaPrueba(mapaTiposPrueba) {
		def ecs = obtenerUltimaEspecificacion()
		def detalles = []
		def estrategiaPrueba = new EstrategiaPrueba()

		definirEstrategiaPUPIPAU(estrategiaPrueba, ecs, mapaTiposPrueba)
		ecs.otrasPruebasECS.each { otraPrueba ->
			def tipoPruebaDefault = mapaTiposPrueba.get(otraPrueba.tipoPrueba)
			def realiza = tipoPruebaDefault ? tipoPruebaDefault.definirRealiza() : true
			def consolida = tipoPruebaDefault ? tipoPruebaDefault.definirConsolida() : otraPrueba.seraConsolidada
			estrategiaPrueba.addToDetalles(new DetalleEstrategiaPrueba(tipoPrueba: otraPrueba.tipoPrueba, seRealiza: realiza, seConsolida: consolida, justificacion: null))
		}
		addToEstrategiasPrueba(estrategiaPrueba)

		return estrategiaPrueba
	}

	def definirDisenioExterno() {
		def disenioExterno = new DisenioExterno()
		addToDiseniosExternos(disenioExterno)
		return disenioExterno
	}

	def agregarOtroCosto(otroCosto) {
		getPlanificacionActual().addToOtrosCostos(otroCosto)
	}

	def eliminarOtroCosto(otroCosto) {
		getPlanificacionActual().removeFromOtrosCostos(otroCosto)
	}

	def guardar(usuarioLogueado) {
		fechaUltimaModificacion = new Date()
		this.doGuardar(usuarioLogueado)
		log.info "Guardar pedido " + this.toString()
		log.info "Observers: " + observers
		this.notificarGuardar(usuarioLogueado)
	}

	def costoTotalCambio() {
		if (!getPlanificacionActual()?.totalMontoGeneral()) {
			return 0
		} else {
			getPlanificacionActual()?.totalMontoGeneral()
		}
	}

	def costoTotalCambioConsolidado() {
		if (getPlanificacionActual()?.estaCumplido()) {
			return this.costoTotalCambio()
		} else {
			return 0
		}
	}

	//	def actualizarResponsables(aprobacionUI, origen) {
	//		def huboAprobacionesActualizadas = false
	//		def usuarioAprobador = usuarioLogueado
	//		if (faseActual.equals(faseEvaluacionCambio())) {
	//			usuarioAprobador = legajoUsuarioGestionDemanda
	//		}
	//		aprobacionesPendientesPara(usuarioAprobador).each { aprobacion ->
	//			if (aprobacion.grupo) {
	//				def nuevoGrupo = getGrupoActual(aprobacion.grupo)
	//				log.info "Actualizando aprobacion a Grupo " + nuevoGrupo + " - Usuario " + usuarioAprobador
	//				aprobacion.actualizarResponsable(nuevoGrupo, usuarioAprobador)
	//			}
	//			log.info "Actualizando justificaci\u00F3n a " + aprobacionUI.justificacion
	//			aprobacion.justificacion = aprobacionUI.justificacion
	//			huboAprobacionesActualizadas = true
	//		}
	//		return huboAprobacionesActualizadas
	//	}

	//	def getGrupoActual(grupo) {
	//		if (this.faseActual?.equals(faseEvaluacionCambio())) {
	//			return this.getGrupoGestionDemanda()
	//		}
	//		return grupo
	//	}

	public List getGruposPlanificacion() {
		return getPlanificacionActual().grupos
	}

	def faseRegistracionPedido() {
		return FaseMultiton.getFase(FaseMultiton.REGISTRACION_PEDIDO)
	}

	def faseValidacionPedido() {
		return FaseMultiton.getFase(FaseMultiton.VALIDACION_PEDIDO)
	}

	def faseAprobacionPedido(){
		return FaseMultiton.getFase(FaseMultiton.APROBACION_PEDIDO)
	}

	def faseEvaluacionCambio(){
		return FaseMultiton.getFase(FaseMultiton.EVALUACION_CAMBIO)
	}

	def faseEspecificacionCambio() {
		return FaseMultiton.getFase(FaseMultiton.ESPECIFICACION_CAMBIO)
	}

	def faseAprobacionCambio() {
		return FaseMultiton.getFase(FaseMultiton.APROBACION_CAMBIO)
	}

	def faseConsolidacionImpacto() {
		return FaseMultiton.getFase(FaseMultiton.CONSOLIDACION_IMPACTO)
	}

	def faseEspecificacionImpacto() {
		return FaseMultiton.getFase(FaseMultiton.ESPECIFICACION_IMPACTO_HIJO)
	}

	def faseValidacionImpacto() {
		return FaseMultiton.getFase(FaseMultiton.VALIDACION_IMPACTO)
	}

	def faseAprobacionImpacto() {
		return FaseMultiton.getFase(FaseMultiton.APROBACION_IMPACTO)
	}

	def faseConstruccionCambio() {
		return FaseMultiton.getFase(FaseMultiton.CONSTRUCCION)
	}

	def faseFinalizacion() {
		return FaseMultiton.getFase(FaseMultiton.FINALIZACION)
	}

	def faseConstruccionCambioHijo() {
		return FaseMultiton.getFase(FaseMultiton.CONSTRUCCION_CAMBIO_HIJO)
	}

	def faseConstruccionPU() {
		return FaseMultiton.getFase(FaseMultiton.CONSTRUCCION_PU_HIJO)
	}

	def faseAdministrarActividades() {
		return FaseMultiton.getFase(FaseMultiton.ADMINISTRAR_ACTIVIDADES_HIJO)
	}

	def faseEjecucionPAU() {
		return FaseMultiton.getFase(FaseMultiton.EJECUCION_SOPORTE_PAU_HIJO)
	}

	def faseAprobacionPAUHijo() {
		return FaseMultiton.getFase(FaseMultiton.APROBACION_PAU_HIJO)
	}

	def faseAprobacionImplantacionHijo() {
		return FaseMultiton.getFase(FaseMultiton.APROBACION_IMPLANTACION_HIJO)
	}

	def faseImplantacionHijo() {
		return FaseMultiton.getFase(FaseMultiton.IMPLANTACION_HIJO)
	}

	def faseNormalizacionHijo() {
		return FaseMultiton.getFase(FaseMultiton.NORMALIZACION_HIJO)
	}

	def tipoEventoAprobacion() {
		if (tieneAprobacionOmitida()) {
			return LogModificaciones.APROBACION_OMITIDA
		} else {
			return LogModificaciones.APROBACION
		}
	}

	def tipoEventoDenegacion() {
		if (tieneDenegacionOmitida()) {
			return LogModificaciones.DENEGACION_OMITIDA
		} else {
			return LogModificaciones.DENEGACION
		}
	}

	def comentarioActual() {
		StringBuffer result = new StringBuffer("")
		def comentario = getPlanificacionActual()?.comentarioSistema?.trim()
		if (comentario) {
			result.append("Planificaci\u00F3n y Esfuerzo: ")
			result.append(comentario)
			result.append(". ")
		}
		comentario = getEstrategiaPruebaActual()?.comentario?.trim()
		if (comentario) {
			result.append("Estrategia de prueba: ")
			result.append(comentario)
		}
		return result.toString()
	}

	def validarAprobacionGeneral(aprobacionUI) {
		if (!aprobacionUI?.aprueba && !aprobacionUI?.justificacion) {
			this.errors.reject("Debe ingresar justificaci\u00F3n para denegar")
			return false
		}
		return true
	}

	def actualizarEspecificacionImpacto() {
		if (getDisenioExternoActual()) {
			getDisenioExternoActual().actualizarEspecificacion()
		}
	}

	public boolean tieneDenegacionOmitida() {
		return false
	}

	public boolean tieneTipoPrueba(TipoPrueba tipoPrueba) {
		def estrategiaPrueba = estrategiaPruebaActual
		if (!estrategiaPrueba) {
			return false
		}
		return estrategiaPrueba.tieneTipoPrueba(tipoPrueba)
	}

	public def getTipoPrueba(TipoPrueba tipoPrueba) {
		return estrategiaPruebaActual?.getTipoPrueba(tipoPrueba)?.tipoPrueba
	}

	public boolean realizaTipoPrueba(TipoPrueba tipoPrueba) {
		return estrategiaPruebaActual?.realizaTipoPrueba(tipoPrueba)
	}

	public Date getFechaFinPlanificadaActividad(Actividad actividad) {
		if (!sistema) {
			return null
		}
		return getPlanificacionActual()?.buscarFechaFin(actividad.tipoActividad.descripcion)
	}

	def ultimaDenegacion(unaFase) {
		def fases = Fase.findAllByFaseDenegacion(unaFase)
		if (fases.isEmpty()) {
			return null
		}

		// Preguntamos y se elimino el filtro de pendiente
		//def aprobaciones = aprobaciones.findAll { aprobacion -> fases.contains(aprobacion.fase) && !aprobacion.estaPendiente() }.sort { it?.fecha }
		def aprobaciones = aprobaciones.findAll { aprobacion -> fases.contains(aprobacion.fase) && aprobacion.justificacion != null }.sort { it?.fecha }
		if (!aprobaciones) {
			return null
		}

		def ultimaAprobacion = aprobaciones.last()

		if (ultimaAprobacion.estaDenegado()) {
			return ultimaAprobacion
		}

		return null
	}

	//NICO: Agrego esto para que los hijos tengan justificaciones
	def ultimaAprobacion(unaFase) {
		def aprobacionesJustificaciones = aprobaciones.findAll { aprobacion -> aprobacion.fase.equals(unaFase) }.sort { it?.fecha }
		if (aprobacionesJustificaciones){
			return aprobacionesJustificaciones.last()
		}
	}

	def justificacion(unaFase) {
		return ultimaAprobacion(unaFase)?.justificacion
	}

	def justificacion(unaFase, unUsuario) {
		def aprobacionesJustificacionesUsuario = aprobaciones.findAll { aprobacion -> aprobacion.fase.equals(unaFase) && aprobacion.usuario.equals(unUsuario)}.sort { it?.fecha }
		if (aprobacionesJustificacionesUsuario){
			return aprobacionesJustificacionesUsuario.last()?.justificacion
		}
		return null
	}


	def eliminarMailsPendientes() {
		mailsPendientes.clear()
	}

	def eliminarAllMailsPendientes() {
		mailsPendientes.clear()
		pedidosHijos.each { pedidoHijo -> pedidoHijo.eliminarAllMailsPendientes() }
	}

	/*String getDescripcion(fase){
	 return ""
	 }*/
	def aprobacionesDeFase(unaFase) {
		return aprobaciones.findAll { aprobacion -> aprobacion.fase.equals(unaFase) }
	}

	def aprobacionesDeFase(unaFase, unUsuario) {
		return aprobaciones.findAll { aprobacion -> aprobacion.matchea(unUsuario, unaFase) }
	}

	def aprobacionesPendientesDeFase(unaFase) {
		return aprobaciones.findAll { aprobacion -> aprobacion.pendienteParaFase(unaFase) }
	}

	def getFechaEspecificacionDenegada() {
		return ultimaAprobacion(faseEspecificacionImpacto()).fecha
	}

	/**
	 * TODO: Este es el nuevo metodo puedeEditar que deberian llamar los controllers
	 * Es independiente de fase
	 *
	 * @param fase
	 * @param usuarioLogueado
	 * @return
	 */
	boolean puedeEditar(fase, usuarioLogueado) {
		if (isCancelado()) {
			return false
		}
		return this.faseActual?.equals(fase) && usuarioHabilitado(fase, usuarioLogueado)
	}

	boolean usuarioHabilitado(fase, usuarioLogueado) {
		if (isCancelado()) { //|| isSuspendido()
			return false
		}
		if (fase.equals(faseEspecificacionImpacto())) {
			return usuarioEsResponsableDeAlgunHijo(usuarioLogueado)
		}
		return getUsuariosResponsables(fase, usuarioLogueado).contains(usuarioLogueado)
	}

	boolean puedeModificarEvaluacionPedido(usuarioLogueado) {
		return false
	}

	def getUsuariosResponsables(fase, usuarioLogueado) {
		def usuarios = aprobaciones.findAll { it.estaPendiente() }.inject([], { usuarios, aprobacion ->
			if (aprobacion.usuario) {
				usuarios.plus(aprobacion.usuario)
			} else {
				UmeService umeService = new UmeService()
				def usuariosGrupo = umeService.getUsuariosGrupoLDAP(aprobacion.grupo, null)
				usuarios.plus(usuariosGrupo.collect { it.legajo })
			}
		})
		return new HashSet(usuarios)
	}


	def grupoTieneCargadoRealIncurrido(grupo){
		return getPlanificacionActual().grupoTieneCargadoRealIncurrido(grupo)
	}

	def grupoTieneCerradoRealIncurrido(grupo){
		return getPlanificacionActual().grupoTieneCerradoRealIncurrido(grupo)
	}

	def isCerrado() {
		return getFechaDeCierre() != null
	}

	def getFechaDeCierre() {
		//		def ultimaFase = fasesActuales()?.last()
		//		if (!ultimaFase) {
		//			return null
		//		}
		//		return getFecha(ultimaFase)
		return fechaCierre
	}

	boolean cerroUltimaFase() {
		return getFechaDeCierre() != null
	}

	def getLinkURL() {
		return getLinkURL("NeedIT")
	}

	def getLinkURL(descripcion) {
		return "<a href='" + ConfigurationHolder.config.grails.serverURL + "/" + faseActual.controllerNombre + "/index/"+id+"?pedidoId=" + id + "'>"+descripcion+"</a>"
	}


	def getLinkURLAprobacion(){
		return "<a href='" + ConfigurationHolder.config.grails.serverURL + "/inboxAprobaciones/index'>NeedIT - Inbox Aprobaciones</a>"
	}

	def getUsuariosANotificar(impactados) {
		return impactados.inject([], { usuarios, impactado -> usuarios + impactado.usuariosANotificar })
	}

	def getUsuarioAprobadorAreaSoporte(AreaSoporte areaSoporte) {
		def usuario
		def aprobacionesDeAreaSoporte = allAprobaciones.findAll { aprobacion -> aprobacion.areaSoporte?.equals(areaSoporte) && aprobacion.fueContestado() }
		log.info "aprobaciones areas de soporte: " + aprobacionesDeAreaSoporte
		if (aprobacionesDeAreaSoporte){
			usuario = aprobacionesDeAreaSoporte.sort { it.id }?.last()?.usuario
		}
		//		FED: no puedo buscar en el log porque si hay más de un Area de Soporte se mezclarían los usuarios y no
		//		los puedo distinguir
		//		if (!usuario) {
		//			def logAprobacionesDeAreaSoporte = logModificaciones.findAll { it.rol.equalsIgnoreCase(RolAplicacion.AREA_SOPORTE)}.sort { it.id }
		//			log.info "log aprobaciones areas de soporte: " + logAprobacionesDeAreaSoporte
		//			if (logAprobacionesDeAreaSoporte) {
		//				return logAprobacionesDeAreaSoporte.last()?.legajo
		//			}
		//		}
		return usuario
	}

	def getAllAprobaciones() {
		return aprobaciones
	}

	def notificarCambioEstado() {
		observers.each { observer -> observer.notifyCambioEstado(mapaInfoDefault) }
	}

	def notificarGuardar(usuarioLogueado) {
		observers.each { observer -> observer.notifyGuardar(mapaInfoDefault, usuarioLogueado) }
	}

	def getMapaInfoDefault() {
		Map mapaInfo = new HashMap()
		mapaInfo.put("pedido", this)
		mapaInfo.put("pedidoPadre", this.parent)
		return mapaInfo
	}

	def getEstilo() {
		if (especificacionDenegada()) {
			return "estado_denegado"
		} else {
			if (cerroEspecificacion()) {
				return "estado_cumplido"
			} else {
				return "estado_pendiente"
			}
		}
	}

	def getEstiloDescripcion() {
		if (especificacionDenegada()) {
			return "Denegado el " + DateUtil.toString(fechaEspecificacionDenegada)
		} else {
			if (cerroEspecificacion()) {
				return "Cumplido el " + DateUtil.toString(fechaCierreEspecificacion)
			} else {
				return "Pendiente"
			}
		}
	}

	def tieneObservers() {
		return observers.size() > 1
	}

	def muestraLinkRealesIncurridos(){
		def aprobaciones = aprobaciones.findAll {aprobacion  -> aprobacion.estaAprobado()}.sort { it.fecha }

		if(!aprobaciones?.isEmpty()){
			def ultimaAprobacionAprobada = aprobaciones?.last()

			def fechaPermitida = ultimaAprobacionAprobada?.fecha +  61
			def fechaHoy = new Date()

			def okStandard = !pedidoDeEmergencia() && fasesRealizadas().contains(FaseMultiton.getFase(FaseMultiton.APROBACION_IMPACTO))
			def okEmergencia = pedidoDeEmergencia() && fasesRealizadas().contains(FaseMultiton.getFase(FaseMultiton.VALIDACION_IMPACTO))

			return  (okStandard || okEmergencia) && (fechaHoy < fechaPermitida)
		}
		return false
	}

	boolean puedeReasignarEvaluacionPedido(usuarioLogueado) {
		return false
	}

	boolean puedeReenviar(usuarioLogueado) {
		return false
	}

	def aprobacionesPendientesDeOtrosUsuarios(fase, usuarioLogueado) {
		if (!id) {
			return []
		}

		Pedido.lock(id)   // Pido perd\u00F3n a la humanidad , FED
		def aprobaciones = Aprobacion.createCriteria().list() {
			eq("fase", fase)
			eq("pedido", this)
			or {
				isNull("usuario")
				ne("usuario", usuarioLogueado)
			}
			eq("estado", Aprobacion.PENDIENTE)
		}
		return aprobaciones
	}

	def getLinkURLCreacion() {
		return "<a href='" + ConfigurationHolder.config.grails.serverURL + "/" + faseActual.controllerNombre + "/index/"+id+"?pedidoId=" + pedidoPadre.id + "&impacto=" + pedidoPadre.id + "'>NeedIT</a>"
	}

	def unificarObservers() {
		observers.unique()
	}

	boolean estaSuspendido(){
		return fechaSuspension != null
	}

	/**
	 * 	Suspende el pedido padre, y todos sus hijos
	 */
	def suspenderTotal(usuarioLogueado){
		if (!validaSuspension(usuarioLogueado)) {
			return false
		}

		// Suspendo al padre
		def padre = this.parent
		padre.suspender(usuarioLogueado)

		// Suspendo los hijos
		padre.pedidosHijos.each { hijo -> hijo.suspender(usuarioLogueado) }

		return true
	}

	boolean validaSuspension(usuarioLogueado){
		if(estaSuspendido()){
			this.errors.reject("El pedido ya fue suspendido previamente")
			return false
		}
		if(!this.motivoSuspension){
			this.errors.reject("Debe ingresar un motivo para realizar la suspensi\u00F3n")
			return false
		}
		return this.puedeSuspender(usuarioLogueado, true)
	}

	def reanudarTotal(usuarioLogueado, comentarioReanudacion){
		if (!this.puedeReanudar(usuarioLogueado, true)) {
			return false
		}
		
		// Suspendo al padre
		def padre = this.parent
		padre.reanudarSuspension(usuarioLogueado, comentarioReanudacion)

		// Suspendo los hijos
		padre.pedidosHijos.each { hijo -> hijo.reanudarSuspension(usuarioLogueado, comentarioReanudacion) }

		return true
	}

	def getTipoAnexoPopSuspension(){
		return (this.estaSuspendido() ? TipoAnexo.findAllByDescripcion(TipoAnexo.reanudacion) : TipoAnexo.findAllByDescripcion(TipoAnexo.suspension))
	}

	boolean muestraAnexosPopSuspension(){
		return anexos.any { anexo -> anexo.esSuspension() || anexo.esReanudacion() }
	}

	def getAnexosPopSuspension(){
		return anexos.findAll { anexo -> anexo.esSuspension() || anexo.esReanudacion()}
	}
	
	String getComentarioReanudacion(){
		return (comentarioReanudacion) ? comentarioReanudacion : "" 
	}
	
	boolean puedeSoloGuardar(fase, usuarioLogueado){
		return puedeEditar(fase, usuarioLogueado) && estaSuspendido()
	}
	
	boolean esPadre() {
		return !this.esHijo()
	}
	
	boolean estaEnCurso(){
		return !estaSuspendido() && !isCancelado() && !isCerrado()
	}
	
	boolean puedeReasignar(usuarioLogueado){
		UmeService umeService = new UmeService()
		def esAdmin = umeService.esAdministrador(usuarioLogueado)

		if(esAdmin){
			return true
		}
		
		boolean cerradoConRICargados = isCerrado() && grupoTieneCerradoRealIncurrido(SoftwareFactory.grupoLDAPHorasCoordinacion)
		def estadoOk = ( estaSuspendido() || estaEnCurso() ) && !cerradoConRICargados
		
		def codigoRolesDelUsuario = new HashSet (getRoles(usuarioLogueado).flatten())
		boolean realizo = codigoRolesDelUsuario.any { codigoRol -> faseDelRolNoFutura(codigoRol) }
		
		return realizo && estadoOk
	}
	
	def getRolesAReasignar(usuarioLogueado, esAdmin){
		def rolesReasignacion = RolReasignacion.list()
		def codigoRolesReasignacion = rolesReasignacion.rol.codigoRol

		def codigoRolesDelUsuarioLogueado = new HashSet (getRoles(usuarioLogueado).flatten())
		
		def codigoRolesQuePuedeReasignarUsuarioLogueado
		if(esAdmin){
			codigoRolesQuePuedeReasignarUsuarioLogueado = codigoRolesReasignacion
		}else{
			codigoRolesQuePuedeReasignarUsuarioLogueado = codigoRolesReasignacion.intersect(codigoRolesDelUsuarioLogueado)
		}
		
		def rolesQueReasigna = new HashSet()
		
		// Recorro Roles del usuario parametrizados para reasignar
		codigoRolesQuePuedeReasignarUsuarioLogueado.each { codigoRolQuePuedeReasignarUsuarioLogueado ->
			def rolUsuarioLogueado = rolesReasignacion.find{rolReasignacion->rolReasignacion.reasignaRol(codigoRolQuePuedeReasignarUsuarioLogueado)}
			def rolesAReasignarParaUsuarioLogueado = rolUsuarioLogueado.rolesAReasignar.codigoRol
			// Recorro los roles a reasignar
			rolesAReasignarParaUsuarioLogueado.each { codigoRolAReasignar ->
				// Valido que el rol exista y se haya realizado
				if(faseDelRolNoFutura(codigoRolAReasignar)){
					rolesQueReasigna.add(codigoRolAReasignar)
				}
			}
		}
		
		// Excepciones
		if(faseActual.equals(faseValidacionPedido()) && getLegajo(RolAplicacion.INTERLOCUTOR_USUARIO)){
			rolesQueReasigna.add(RolAplicacion.INTERLOCUTOR_USUARIO)
		}
		
		if(faseActual.equals(faseEvaluacionCambio()) && getLegajo(RolAplicacion.COORDINADOR_CAMBIO)){
			rolesQueReasigna.add(RolAplicacion.COORDINADOR_CAMBIO)
		}

		return rolesQueReasigna
	}

	boolean faseDelRolNoFutura(codigoRol){
		return (fasesRealizadas() + faseActual).contains(getFaseByRolAReasignar(codigoRol))
	}
		
	def getFaseByRolAReasignar(codigoRol){
		if(codigoRol.equalsIgnoreCase(RolAplicacion.USUARIO_FINAL)){
			return FaseMultiton.getFase(FaseMultiton.REGISTRACION_PEDIDO)
		}
		
		if(codigoRol.equalsIgnoreCase(RolAplicacion.GERENTE_USUARIO)){
			return FaseMultiton.getFase(FaseMultiton.VALIDACION_PEDIDO)
		}
		
		if(codigoRol.equalsIgnoreCase(RolAplicacion.INTERLOCUTOR_USUARIO)){
			return FaseMultiton.getFase(FaseMultiton.APROBACION_PEDIDO)
		}
		
		if(codigoRol.equalsIgnoreCase(RolAplicacion.COORDINADOR_CAMBIO)){
			return FaseMultiton.getFase(FaseMultiton.ESPECIFICACION_CAMBIO)
		}
		
		if(codigoRol.equalsIgnoreCase(RolAplicacion.GESTION_DEMANDA)){
			return FaseMultiton.getFase(FaseMultiton.EVALUACION_CAMBIO)
		}
		
		if(codigoRol.equalsIgnoreCase(RolAplicacion.APROBADOR_IMPACTO)){
			return FaseMultiton.getFase(FaseMultiton.APROBACION_IMPACTO)
		}
	}
	
	
	def reasignarUsuarioDeRol(codigoRol, legajoNuevo){
		
	}
	
	def getAllActividadesPendientes(grupoActividad, legajoActual) {
		pedidosHijosActivos.inject([], { acum, pedidoHijo -> acum.plus(pedidoHijo.getActividadesPendientes(grupoActividad, legajoActual)) })
	}
	
	def getPedidosHijosActivos() {
		[]
	}

	def getLegajo(RolAplicacion rol) {
		return getLegajo(rol.codigoRol)
	}	
	
	def getRolesAReasignar(usuarioLogueado){
		return new HashSet (getRoles(usuarioLogueado).flatten())
	}
	
	def planificacionesActualesConsolidadas(){
		def planificaciones = []
		if (planificacionActual) {
			planificaciones.addAll planificacionActual.id
		}
		return planificaciones
	}
	
	def planificacionesActualesConsolidadasEnReplanificacion(){
		def planificaciones = []
		if (planificacionEnReplanificacion) {
			planificaciones.addAll planificacionEnReplanificacion.id
		}
		return planificaciones
	}
	
	def enviarReplanificacion(comentario){
		def nuevaPlanificacion = planificacionActual.crearCopia(PlanificacionEsfuerzo.ESTADO_EN_REPLANIFICACION)
		addToPlanificaciones(nuevaPlanificacion)
		def nuevoLog = new LogModificaciones(legajo: this.legajoCoordinadorCambio, fase: this.fase, fechaDesde: new Date(), fechaHasta: new Date(), descripcionEvento: comentario, tipoEvento: LogModificaciones.EN_REPLANIFICACION, rol: RolAplicacion.COORDINADOR_CAMBIO)
		addToLogModificaciones(nuevoLog)
		fechaReplanificacion = new Date()
	}
	
	boolean estaEnFase(codigoFase){
		return this.faseActual.codigoFase.equals(codigoFase) && !isCerrado() && !isCancelado()// && !isSuspendido()
	}
	
}
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                     