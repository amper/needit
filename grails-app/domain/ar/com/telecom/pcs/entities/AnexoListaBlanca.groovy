package ar.com.telecom.pcs.entities

class AnexoListaBlanca {

	String extension
	String descripcion
	
    static constraints = {
		extension(nullable: false, size: 1..20)
		descripcion(nullable: false, maxSize: 255)
    }
}
