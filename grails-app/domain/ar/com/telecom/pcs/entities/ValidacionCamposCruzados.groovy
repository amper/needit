package ar.com.telecom.pcs.entities

import java.util.List

/**
 * Representa una validación que se hace con muchos campos, en los cuales alguno tiene que estar completo
 * 
 * @author u193151
 *
 */
class ValidacionCamposCruzados extends ValidacionCamposActividad {

	/**
	 * Chequea que alguno de los campos no sea vacío
	 */
	public boolean doValidar(Actividad actividad) {
		// TODO: Revisar si la validación va contra la actividad, contra el pedido
		// o si permitimos que vayan varias propiedades contra distintas entidades
		// no borrar el tests que prueba esto!
		camposAValidar.any { campo ->
			def valor = actividad."${campo}"
			valor != null && !valor.toString().equalsIgnoreCase("")
		}
	}

	/**
	* Chequea que alguno de los campos no sea vacío
	*/
   public boolean validar(Actividad actividad, AbstractPedido pedido) {
	   def validacionCampos = this.doValidar(actividad)
	   if (!validacionCampos) {
		   actividad.errors.reject (actividad.descripcion + ": Debe ingresar alguno de estos campos: " + camposAValidar)
	   }
	   return validacionCampos
   }

   public String toString() {
	   return "Validaci\u00F3n campos cruzados - campos a validar: " + camposAValidar.toString()
   }
   
}
