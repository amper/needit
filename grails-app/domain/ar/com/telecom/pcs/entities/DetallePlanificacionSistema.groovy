package ar.com.telecom.pcs.entities

import java.util.Date

import ar.com.telecom.ume.UmeService

class DetallePlanificacionSistema extends DetallePlanificacion {

	ActividadPlanificacion actividadPlanificacion
	List cargaHoras
	List cargaHorasIncurridas
	static hasMany = [cargaHoras : CargaHora, cargaHorasIncurridas: CargaHoraIncurrida]

	DetallePlanificacionSistema() {
		cargaHoras = new ArrayList()
		cargaHorasIncurridas = new ArrayList()
	}

	static mapping = {
		actividadPlanificacion fetch: 'join'
		cargaHoras cascade: 'all-delete-orphan'
		cargaHorasIncurridas cascade: 'all-delete-orphan'
	}

	public String toString() {
		return actividadPlanificacion
	}

	def totalHoras() {
		return cargaHoras.inject(0) { acum, carga -> acum + carga.totalHoras() }
	}

	def totalHoras(grupo){
		if(!grupo){
			return totalHoras()
		}

		def cargaHorasDeGrupo

		if(grupo.equalsIgnoreCase(SoftwareFactory.grupoLDAPHorasCoordinacion)){
			cargaHorasDeGrupo = cargaHoras.findAll {cargaHora  -> cargaHora.softwareFactory == null }
		}else{
			cargaHorasDeGrupo = cargaHoras.findAll {cargaHora  -> cargaHora.softwareFactory.grupoLDAP.equals(grupo) }
		}
		return cargaHorasDeGrupo.inject(0) { acum, carga -> acum + carga.totalHoras() }
	}

	def totalHorasIncurridas(grupo){
		if(!grupo){
			return cargaHorasIncurridas.inject(0) { acum, carga -> acum + carga.totalHoras() }
		}

		def cargaHorasIncurridasDeGrupo = cargaHoraIncurridaDeGrupo(grupo)
		if(cargaHorasIncurridasDeGrupo == null){
			return null
		}

		return cargaHorasIncurridasDeGrupo.inject(0) { acum, carga -> acum + carga.totalHoras() }
	}

	def cargaHoraIncurridaDeGrupo(grupo){
		def cargaHorasIncurridasDeGrupo

		if(grupo.equalsIgnoreCase(SoftwareFactory.grupoLDAPHorasCoordinacion)){
			cargaHorasIncurridasDeGrupo = cargaHorasIncurridas.find {cargaHoraIncurrida  -> cargaHoraIncurrida.softwareFactory == null }
		}else{
			cargaHorasIncurridasDeGrupo = cargaHorasIncurridas.find {cargaHoraIncurrida  -> cargaHoraIncurrida.softwareFactory.grupoLDAP.equals(grupo) }
		}

		if(!cargaHorasIncurridasDeGrupo){
			return null
		}
		return cargaHorasIncurridasDeGrupo
	}


	def totalMontoHoras() {
		return cargaHoras?.inject(0) { acum, carga -> acum + carga.totalMontoHoras() }
	}

	def agregarCargaHora(cargaHora){
		this.addToCargaHoras(cargaHora)
	}

	def borrarCargaHora(cargaHora){
		this.removeFromCargaHoras(cargaHora)
	}

	def validar(AbstractPedido pedido) {
		super.validar(pedido)

		if ((fechaDesde || fechaHasta) && totalHoras() == 0) {
			pedido.errors.reject "Planificaci\u00F3n y esfuerzo " + this.toString() + ": si indica fechas de planificaci\u00F3n debe cargar horas "
		}

		cargaHoras.each { carga -> carga.validar(pedido) }
	}

	def blanquear(){
		super.blanquear()
		cargaHoras.clear()
	}

	boolean esDeAreaDeSoporte(){
		return false
	}

	boolean esDeSistema(){
		return true
	}

	boolean estaCargado() {
		boolean tieneCargaHoras = !cargaHoras.isEmpty()
		return fechaDesde != null && fechaHasta != null && tieneCargaHoras
		// TODO: Quizás en lugar de preguntar si está vacía la colección de carga de horas
		// podemos delegar la pregunta a que alguna de la carga de horas esté completa
	}

	boolean tieneCargaHoraRepetida(CargaHora cargaHora) {
		return cargaHoras.any { horas -> horas.softwareFactory.equals(cargaHora?.softwareFactory) && cargaHora.periodo && horas.periodo?.equals(cargaHora.periodo)}
	}

	public def getDescripcion() {
		return actividadPlanificacion.descripcion
	}

	def getGrupos() {
		def cargas = cargaHoras.collect { cargaHora -> cargaHora.softwareFactory?.grupoLDAP }
		cargas.removeAll([null])
		return cargas
	}

	def getGrupos(origen) {
		return getGrupos()
	}
	
	def tieneGrupo(grupo){
		return cargaHoras.any { cargaHora -> cargaHora.softwareFactory?.grupoLDAP.equals(grupo)}
	}

	def tieneGrupo(grupo, origen){
		return tieneGrupo(grupo)
	}
	
	def noTieneGrupo(){
		return cargaHoras.any { cargaHora -> cargaHora.softwareFactory == null}
	}

	def getFase(){
		return actividadPlanificacion.fase
	}

	def puedeEditar(pedido, usuarioLogueado, grupo){
		def umeService = new UmeService()

		// Solo chequeo que el usuario pertenezca al grupo, cuando no son horas de coordinacion, ya que las horas de coord no tienen grupo
		def validaCoordinacion = true
		if(grupo && !grupo.equalsIgnoreCase(SoftwareFactory.grupoLDAPHorasCoordinacion)){
			validaCoordinacion = umeService.usuarioPerteneceGrupo(grupo, usuarioLogueado) || pedido.esResponsable(usuarioLogueado)
		}

		//return getFase()?.puedeEditar(pedido, usuarioLogueado) && validaCoordinacion
		return ((pedido.fasesRealizadas().contains(getFase()) || pedido.faseActual.equals(getFase())) && validaCoordinacion) ||  pedido.esSWDesarrolloSAP()
	}

	def confirmarRealesIncurridos(swfactory, cantidadHoras, usuarioLogueado, pedido){
		def umeService = new UmeService()
		def cargaHoraIncurrida = actualizarHorasIncurridas(swfactory, cantidadHoras)

		cargaHoraIncurrida.fechaConfirmacionIncurridos = new Date()
		cargaHoraIncurrida.usuarioConfirmadorIncurridos = usuarioLogueado
		
		pedido.addToLogModificaciones(new LogModificaciones(legajo: usuarioLogueado, fechaDesde: new Date(), fechaHasta: new Date(), fase: pedido.faseActual, tipoEvento: LogModificaciones.CONFIRMAR_INCURRIDO, descripcionEvento: "El usuario "+usuarioLogueado+"( "+ umeService.getUsuario(usuarioLogueado)?.nombreApellido + ") confirmo las horas incurridas del detalle:"+getDescripcion() ))
	}

	def confirmoRealesIncurridos(grupo){
		def cargaHorasDeGrupo

		if(grupo.equalsIgnoreCase(SoftwareFactory.grupoLDAPHorasCoordinacion)){
			cargaHorasDeGrupo = cargaHorasIncurridas.find {cargaHora  -> cargaHora.softwareFactory == null }
		}else{
			cargaHorasDeGrupo = cargaHorasIncurridas.find {cargaHora  -> cargaHora.softwareFactory.grupoLDAP.equals(grupo) }
		}


		if(!cargaHorasDeGrupo){
			return false
		}

		return cargaHorasDeGrupo.confirmoRealesIncurridos()
	}

	def actualizarHorasIncurridas(swfactory, cantidadHoras){

		def grupo

		if(swfactory){
			grupo = swfactory.grupoLDAP
		}else{
			grupo = SoftwareFactory.grupoLDAPHorasCoordinacion
		}

		def cargaHoraIncurrida = cargaHoraIncurridaDeGrupo(grupo)

		if(!cargaHoraIncurrida){
			cargaHoraIncurrida = new CargaHoraIncurrida(softwareFactory: swfactory, cantidadHoras: cantidadHoras, fechaConfirmacionIncurridos:null, usuarioConfirmadorIncurridos:null)
			cargaHorasIncurridas.add(cargaHoraIncurrida)
		}else{
			if(!confirmoRealesIncurridos(grupo)){
				cargaHoraIncurrida.cantidadHoras = new Integer(cantidadHoras).intValue()
			}
		}

		return cargaHoraIncurrida
	}

	def blanqueaHorasIncurridas(grupo){
		def cargaHoraIncurrida = cargaHoraIncurridaDeGrupo(grupo)
		cargaHoraIncurrida?.blanquear()
	}
	
	def tieneFechaImplantacion(){
		return actividadPlanificacion.esImplantacion() && tieneFechasCargadas() 
	}
}
