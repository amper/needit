package ar.com.telecom.pcs.entities

class TipoReferencia {

	public static final String needIt = "NeedIT"
	
	String descripcion

	static constraints = {
		descripcion(size:1..50)
    }
	
	String toString() {
		"${descripcion}"
	}
	
	boolean esDeNeedIt(){
		return descripcion!= null && descripcion.equals(needIt)
	}
}
