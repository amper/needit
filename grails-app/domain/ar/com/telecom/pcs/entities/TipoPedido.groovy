package ar.com.telecom.pcs.entities

class TipoPedido {

	String descripcion
	
    static constraints = {
		descripcion(size:1..50)
    }
	
	String toString() {
		"${descripcion}"
	}
}
