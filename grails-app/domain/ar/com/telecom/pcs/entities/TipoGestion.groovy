package ar.com.telecom.pcs.entities

class TipoGestion {

	public static final String GESTION_ESTANDAR = "Est\u00E1ndar"
	public static final String GESTION_EMERGENCIA = "Emergencia"
	public static final String tooltip = "Se considera que un cambio puede adoptar el tipo de gesti\u00F3n Emergencia cuando cumple las dos siguientes caracter\u00EDsticas:Su gesti\u00F3n no es factible mediante el circuito Est\u00E1ndar debido a que en caso de adoptar esta tipificaci\u00F3n el impacto para la compañía incluiría:· P\u00E9rdidas / Penalidades econ\u00F3micas y/o,· Impacto sobre la imagen y/o,· Compromiso de un activo crítico.Su resoluci\u00F3n no supera los 21 días corridos."	
	String descripcion
	List fases 
	
	static final int CANT_FASES = 5
	
    static constraints = {
		descripcion(size:1..50)
    }
	
	static hasMany = [ fases : Fase ]
	
	static mapping = {
		fases lazy : false
	}
	
	String toString() {
		descripcion
	}
	
	def traerDatosRelacionados() {
	}
	
	def faseSiguienteA(fase) {
		def indiceSiguiente = fases.indexOf(fase) + 1
		if (indiceSiguiente < fases.size()) {
			return fases.get(indiceSiguiente)
		} else {
			return null
		}
	}

	def fasesAnterioresA(unaFase) {
		def desde = fases.indexOf(unaFase)
		if (desde == -1){
			return []
		}else{
			return fases.subList(0, desde)
		}
	}

	def fasesPosterioresA(unaFase) {
		def desde = fases.indexOf(unaFase)
		return fases.subList(desde + 1, fases.size() - 1)
		// TODO: Probar
	}

}
