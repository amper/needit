package ar.com.telecom.pcs.entities

import java.util.List

/**
 * Representa una validación que se hace con muchos campos, en los cuales alguno tiene que estar completo
 * 
 * @author u193151
 *
 */
class ValidacionCamposActividad extends CriterioValidacionActividad {

	List camposAValidar

	static hasMany = [ camposAValidar: String ]

	public ValidacionCamposActividad() {
		camposAValidar = new ArrayList()
	}

	public void agregarCampoAValidar(String campo) {
		camposAValidar.add(campo)	
	}
	
}
