package ar.com.telecom.pcs.entities

/**
 * Justificación de no realización de pruebas
 * 
 * @author u193151
 *
 */
class Justificacion {

	TipoPrueba tipoPrueba
	String descripcion
	
    static constraints = {
		descripcion(size:1..80)
    }
	
	String toString(){
		return descripcion
	}
	
}
