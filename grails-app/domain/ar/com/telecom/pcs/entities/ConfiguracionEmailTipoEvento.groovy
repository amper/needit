package ar.com.telecom.pcs.entities

import ar.com.telecom.pedido.observers.PedidoAnexoFaseDiferenteObserver
import ar.com.telecom.pedido.observers.PedidoEliminarAnexoFaseActualObserver
import ar.com.telecom.pedido.observers.PedidoEventoLogObserver
import ar.com.telecom.pedido.observers.PedidoModificacionAnexoGlobalObserver
import ar.com.telecom.pedido.observers.PedidoModificacionObserver
import ar.com.telecom.pedido.observers.PedidoNullObserver

class ConfiguracionEmailTipoEvento {

	String tipoEvento
	Fase fase
	String asunto
	String texto

	static constraints = { 
		asunto(maxSize:1024)
		fase(nullable: true)
	}

	static hasMany = [ roles : RolAplicacion ]
	
	static mapping = {
		fase fetch: 'join' 
		texto type: 'text'
		roles lazy: false
	}

	def traerDatosRelacionados() {
	}

	public String toString() {
		return "Configuraci\u00F3n por evento: " + tipoEvento + " para fase " + fase + " - roles: " + roles
	}
	
	public def getObservers() {
		def result = []
		
//		if (tipoEvento.equalsIgnoreCase(LogModificaciones.MODIFICACION_DATOS)) {
//			result = result + new PedidoModificacionObserver()
//		} 
		if (tipoEvento.equalsIgnoreCase(LogModificaciones.MODIFICACION_ANEXO_GLOBAL)) {
			result = result + new PedidoModificacionAnexoGlobalObserver()
		}
		if (tipoEvento.equalsIgnoreCase(LogModificaciones.ANEXO_GLOBAL_DIFERENTE_FASE)) {
			result = result + new PedidoAnexoFaseDiferenteObserver()
		}
		if (tipoEvento.equalsIgnoreCase(LogModificaciones.ELIMINAR_ANEXO_FASE_ACTUAL)) {
			result = result + new PedidoEliminarAnexoFaseActualObserver()
		}
		//if ([LogModificaciones.APROBACION, LogModificaciones.CANCELACION, LogModificaciones.CREACION].contains(tipoEvento)) {
		//	return new PedidoEventoLogObserver()
		//}
		//return new PedidoNullObserver()
		result = result + new PedidoEventoLogObserver()
		
		return result
	}
	
}
