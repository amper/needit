package ar.com.telecom.pcs.entities

class MotivoSuspension {

	String descripcion
    static constraints = {
		descripcion(maxSize:50)
    }
	
	String toString() {
		return descripcion
	}

}
