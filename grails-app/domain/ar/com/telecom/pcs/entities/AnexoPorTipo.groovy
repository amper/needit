package ar.com.telecom.pcs.entities

import java.util.Date

class AnexoPorTipo {

	TipoAnexo tipoAnexo
	String descripcion
	boolean global
	String legajo
	Date fecha
	int versionAnexo
	String nombreArchivo
	byte[] archivo
	String fileContentType
	String extension
	
    static constraints = {
		legajo(size:1..20)
		nombreArchivo(maxSize:200)
		archivo(nullable:true, maxSize: 10485760) //TODO maxSize: deberiamos poner un maxsize para los archivos que subamos?
		extension(nullable:true)
		tipoAnexo(nullable:true)
		descripcion(nullable:true)
    }
	
	static mapping = {
		descripcion type: 'text'
		tipoAnexo fetch: 'join'
	}
	
	public String toString(){
		return nombreArchivo + extension
	}
	 
	boolean esDeFase(Fase unaFase) {
		return tipoAnexo.esDeFase(unaFase)
	}
	
	boolean puedeEliminarse(pedido){
		return ((pedido.estaSuspendido() && esReanudacion()) || (!pedido.estaSuspendido() && esSuspension())) 
	}
	
	
	boolean esSuspension(){
		return tipoAnexo.esSuspension()
	}

	boolean esReanudacion(){
		return tipoAnexo.esReanudacion()
	}
	
	boolean esGlobal(){
		return (tipoAnexo?.fase==null || global)
	}
}
