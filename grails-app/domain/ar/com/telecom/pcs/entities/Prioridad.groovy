package ar.com.telecom.pcs.entities

class Prioridad {
	String descripcion

    static constraints = {
		descripcion(size:1..40)
	}
	
	public String toString(){
		return descripcion
	}
}
