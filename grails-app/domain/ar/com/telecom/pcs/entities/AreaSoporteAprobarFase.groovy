package ar.com.telecom.pcs.entities

class AreaSoporteAprobarFase {

	Fase fase
	AreaSoporte areaSoporte
	
	static constraints = {

	}
	
	String toString() {
		"${fase} - ${areaSoporte}" 
	}
}
