package ar.com.telecom.pcs.entities

import ar.com.telecom.Person


class GrupoLDAP {

	String descripcion
    static constraints = {
		descripcion(maxSize:50, nullable:true)
    }
	
	static hasMany = [ usuarios : Person ]
}
