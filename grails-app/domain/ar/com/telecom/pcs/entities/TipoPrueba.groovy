package ar.com.telecom.pcs.entities

class TipoPrueba {

	public static String DESCRIPCION_PU = "PU"
	public static String DESCRIPCION_PI = "PI"
	public static String DESCRIPCION_PAU = "PAU"

	String descripcion
	boolean opcional
	
    static constraints = {
		descripcion(size:1..50)
    }
	
	static mapping = {
		//fase fetch: 'join'
	}
	
	static hasMany = [ fases: Fase ]
	
	String toString() {
		"${descripcion}"
	}
	
	public boolean equals(Object o) {
		try {
			TipoPrueba otro = (TipoPrueba) o
			return otro.id.equals(id)
		} catch (ClassCastException e) {
			return false
		}
	}
	
	public int hashCode() {
		return id.hashCode()
	}
	
	static def pruebaUnitaria() {
		return TipoPrueba.findByDescripcion(DESCRIPCION_PU)
	}
	
	static def pruebaIntegracion() {
		return TipoPrueba.findByDescripcion(DESCRIPCION_PI)
	}

	static def pruebaPAU() {
		return TipoPrueba.findByDescripcion(DESCRIPCION_PAU)
	}
			
}
