package ar.com.telecom.pcs.entities

class OtroCosto {

	TipoCosto tipoCosto
	String detalle
	BigDecimal costo
	
    static constraints = {
		detalle(maxSize:150, nullable: true)
    }
	
	static mapping = {
		tipoCosto fetch: 'join'
	}
	
	String toString() {
		"${detalle} - ${costo}"
	}
	
	def validar(AbstractPedido pedido) {
		if (!this.costo){
			pedido.errors.reject "Debe ingresar un valor"
		}

		if (!this.tipoCosto){
			pedido.errors.reject "Debe ingresar un tipo de costo"
		}
		
		return pedido.errors.errorCount == 0
	}

}
