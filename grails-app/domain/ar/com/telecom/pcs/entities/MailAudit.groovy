package ar.com.telecom.pcs.entities

class MailAudit {

	String asunto
	String texto
	String mails

    static constraints = {
		asunto(size:1..350)
    }
	
	static mapping = {
		texto type: 'text'
		mails type: 'text'
	}
	
}
