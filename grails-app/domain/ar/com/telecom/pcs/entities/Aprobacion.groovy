package ar.com.telecom.pcs.entities

import java.util.Date

import ar.com.telecom.pedido.consulta.Consulta
import ar.com.telecom.ume.UmeService

class Aprobacion {
	
	public static String PENDIENTE = "P"
	public static String APROBADO = "A"
	public static String DENEGADO = "R"
	// Esto es para consolidado
	public static String EN_COLA = "C"
	
	public static String SUSPENDIDO = "S"
	public static String CANCELADO = "K"
	
	String usuario
	String grupo
	String estado
	Fase fase
	Date fecha
	String justificacion
	// Justificacion Area Aprobadora - es redundante pero permite hacer la asociación más fácil
	AreaSoporte areaSoporte
	// Sólo para Aprobaciones Nominales
	String descripcionAprobacionSolicitada
	String rol

	static belongsTo = [pedido: AbstractPedido]

	
	def traerDatosRelacionados() {

	}
	
	static constraints = {
		fecha(nullable:true)
		grupo(nullable:true)
		usuario(nullable:true)
		justificacion(nullable:true)
		descripcionAprobacionSolicitada(maxSize:60, nullable:true)
		areaSoporte(nullable:true)
		rol(nullable: true)
	}

	static mapping = { 
		justificacion type: 'text'
		fase fetch: 'join'
		areaSoporte fetch: 'join'
	}

	Aprobacion() {
		
	}

	def estadoDescripcion() {
		if (estado.equalsIgnoreCase(PENDIENTE)) {
			return "Pendiente"
		}
		if (estado.equalsIgnoreCase(APROBADO)) {
			return "Aprobado"
		}
		if (estado.equalsIgnoreCase(DENEGADO)) {
			return "Denegado"
		}
		if (estado.equalsIgnoreCase(EN_COLA)) {
			return "En cola"
		}
		if (estado.equalsIgnoreCase(SUSPENDIDO)) {
			return "Suspendido"
		}
		if (estado.equalsIgnoreCase(CANCELADO)) {
			return "Cancelado"
		}

	}

	def getEstadoDescripcion() {
		return estadoDescripcion()
	}
	
	def estaAprobado() {
		return estado.equalsIgnoreCase(APROBADO)
	}

	def estaDenegado() {
		return estado.equalsIgnoreCase(DENEGADO)
	}

	def estaEncolado() {
		return estado.equalsIgnoreCase(EN_COLA)
	}
	
	def estaSuspendido() {
		return estado.equalsIgnoreCase(SUSPENDIDO)
	}

	def estaCancelado() {
		return estado.equalsIgnoreCase(CANCELADO)
	}
	
	def aprobar() {
		fecha = new Date()
		estado = APROBADO	
	}
	
	def pendienteParaFase(unaFase) {
		return estaPendiente() && fase.equals(unaFase) 
	}

	def actualParaFase(unaFase) {
		return (estaSuspendido() || estaPendiente()) && fase.equals(unaFase)
	}

	def encoladaParaFase(unaFase) {
		return estaEncolado() && fase.equals(unaFase)
	}
	
	def estaPendiente() {
		return estado.equalsIgnoreCase(PENDIENTE)
	}
	
	def estaEnCurso() {
		return [PENDIENTE, SUSPENDIDO].contains(estado)
	}
	
	def rechazar() {
		fecha = new Date()
		estado = DENEGADO
	}
	
	public String toString() {
		return "Aprobacion " + ( usuario ? usuario : grupo) + " - " + estadoDescripcion()
	}
	
	def matchea(unUsuario, unaFase) {
		return usuario?.equalsIgnoreCase(unUsuario) && fase.equals(unaFase)
	}
	
	def reasignar(unLegajo) {
		usuario = unLegajo
	}

	boolean actualizar(aprobacionUI, boolean cambiaResponsable) {
		boolean seActualizo = false
		if (aprobacionUI.grupo) {
			grupo = aprobacionUI.grupo
			seActualizo = true
		}	
		if (aprobacionUI.asignatario || cambiaResponsable) {
			usuario = aprobacionUI.asignatario
			seActualizo = true
		}
		if (aprobacionUI.justificacion) {
			justificacion = aprobacionUI.justificacion
			seActualizo = true
		}
		return seActualizo
	}
	
	def actualizarResponsable(grupoNuevo, legajo) {
		if (grupoNuevo) {
			grupo = grupoNuevo
			log.info "Actualizar Grupo de la aprobaci\u00F3n a " + grupoNuevo
		}
		usuario = legajo
		log.info "Actualizar Responsable de la aprobaci\u00F3n a " + legajo
	}

	def esResponsableDeAprobar(unLegajo) {
		return usuario?.equalsIgnoreCase(unLegajo) || new UmeService().getGrupos(unLegajo).contains(grupo)
	}
	
	def esResponsableDeAprobar(unLegajo, tipoConsulta) {
		if (tipoConsulta && tipoConsulta.equalsIgnoreCase(Consulta.MIS_PENDIENTES)) {
			return usuario?.equalsIgnoreCase(unLegajo) 
		} else {
			return usuario?.equalsIgnoreCase(unLegajo) || new UmeService().getGrupos(unLegajo).contains(grupo)
		}
	}

	// Ver fechaCumplimiento
	@Deprecated   
	def getFechaAprobacion(legajo) {
		def aprobacionesLegajo = pedido.logModificaciones.findAll { logModif ->
			logModif.fase.equals(fase) && logModif.legajo.equalsIgnoreCase(legajo)
		}.sort { it.fechaDesde }
		if (aprobacionesLegajo.isEmpty()) {
			return null
		} else {
			return aprobacionesLegajo.last()?.fechaHasta
		}
	}

	def getFechaCumplimiento() {
		if (estaPendiente()) {
			return null	
		} else {
			return fecha
		}
	}
	
	def getLegajoUsuarioResponsable() {
		return usuario
	}
	
	def encolar() {
		fecha = new Date()
		estado = EN_COLA
	}
	
	boolean posteriorAFechaYFase(unaFecha, unaFase) {
		if (!unaFecha || !unaFase) {
			return false
		}
		return fase.equals(unaFase) && fecha >= unaFecha
	}

	boolean fueContestado() {
		return estaAprobado() || estaDenegado()
	}	
	
	def blaquearAsignatario(){
		usuario = null
	}

	def cancelar(){
		estado = CANCELADO
	}

	def suspender(){
		estado = SUSPENDIDO
	}
	
	def pasarAPendiente(){
		estado = PENDIENTE
	}
	
}
