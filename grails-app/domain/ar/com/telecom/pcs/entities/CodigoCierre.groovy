package ar.com.telecom.pcs.entities

import ar.com.telecom.CodigoCierreMultiton;

class CodigoCierre {
	String descripcion
	EstadoActividad estadoActividad
	
	public static String EXITOSO = "Exitoso"
	public static String OBSERVACIONES = "Observaciones"
	public static String FALLIDA = "Fallida"
	public static String RECHAZADA = "Rechazada"
	public static String CANCELADA = "Cancelada"
	
	
    static constraints = {
		descripcion(size:1..40)
	}
	
	static mapping = {
		estadoActividad fetch: 'join'
	}
	
	public String toString() {
		return descripcion
	}
	
	static def exitoso() {
		return CodigoCierreMultiton.getCodigosCierre(EXITOSO)
	}

	static def observaciones() {
		return CodigoCierreMultiton.getCodigosCierre(OBSERVACIONES)
	}

	static def fallida() {
		return CodigoCierreMultiton.getCodigosCierre(FALLIDA)
	}

	static def rechazada() {
		return CodigoCierreMultiton.getCodigosCierre(RECHAZADA)
	}

	static def cancelada() {
		return CodigoCierreMultiton.getCodigosCierre(CANCELADA)
	}

	public boolean ok() {
		return [exitoso(), observaciones()].contains(this)
	}
	
	boolean seCompleto() {
		return [exitoso(), observaciones(), fallida()].contains(this)
	}

	
	public boolean equals(Object otro) {
		try {
			def otraCodigo = (CodigoCierre) otro
			return descripcion?.equalsIgnoreCase(otraCodigo?.descripcion)
		} catch (ClassCastException e) {
			return false
		}
	}
	
	public int hashCode() {
		return descripcion.hashCode();
	}
	
	 
}
