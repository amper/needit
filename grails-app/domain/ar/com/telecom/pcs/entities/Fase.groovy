package ar.com.telecom.pcs.entities

import ar.com.telecom.FaseMultiton

class Fase {

	String descripcion
	String tooltip
	String controllerNombre
	String codigoFase
	@Deprecated
	boolean aplicaPadre
	Fase faseDenegacion
	MacroEstado macroEstado
	// Nuevo, indica cuál es la fase del padre que está asociado (sólo para fases hijas) 
	Fase fasePadre
	// Nuevo, indica si la descripción de la fase se debe tomar en base a los hijos
	boolean descripcionEnBaseAHijos
	
    static constraints = {
		descripcion(size: 1..100)
		tooltip(maxSize: 255, nullable:true)
		controllerNombre(size: 1..50)
		codigoFase(maxSize: 10)
		macroEstado(nullable:true)
		faseDenegacion(nullable: true)
		fasePadre(nullable: true)
    }

	static mapping = {
		macroEstado fetch: 'join'	
	}
	
	public Fase() {
		descripcion = ""
	}
	
	public boolean equals(Object otro) {
		try {
			def otraFase = (Fase) otro
			def otraFaseDescripcion = otraFase?.descripcion
			return descripcion.equalsIgnoreCase(otraFaseDescripcion)
		} catch (ClassCastException e) {
			return false
		}
	}
	
	public int hashCode() {
		return descripcion.hashCode();
	}
	
	String toString(){
		return descripcion
	}
	
	def traerDatosRelacionados() {
		//macroEstado?.descripcion
		// se debería solucionar con el fetch join
	}
	
	def puedeEditar(pedido, usuarioLogueado){
		return pedido.usuarioHabilitado(this, usuarioLogueado)
	}
	
	boolean correspondeMostrar(pedido) {
		if (codigoFase.equalsIgnoreCase(FaseMultiton.NORMALIZACION_HIJO)) {
			return pedido.normaliza()
		}
		return true
	}
	
}
