package ar.com.telecom.pcs.entities

class ConfiguracionEmailCambioEstado {

	Fase faseActual
	Fase faseSiguiente
	String asunto
	String texto

	static constraints = { 
		asunto(maxSize:1024) 
	}

	static hasMany = [ roles : RolAplicacion ]

	static mapping = { 
		faseActual fetch: 'join'
		faseSiguiente fetch: 'join'
		texto type: 'text'
		roles lazy: false
	}

	public String toString() {
		return "Configuracion: " + faseActual + " > " + faseSiguiente + " - roles: " + roles
	}
}
