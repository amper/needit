package ar.com.telecom.pcs.entities

import ar.com.telecom.exceptions.BusinessException

class TipoImpacto {
	public static String ADMINISTRADOR_FUNCIONAL = "Administrador funcional"
	public static String SOFTWARE_DESARROLLO = "SW Desarrollo"
	public static String SOFTWARE_DESARROLLO_SAP = "Sw Desarrollo SAP"
	public static String DATOS_REFERENCIALES = "Datos referenciales"
	public static String SOPORTE_A_PRUEBAS = "SW Soporte a pruebas"
	public static String AREAS_IT = "\u00E1reas IT"

	String descripcion
	boolean asociadoSistemas				// asociado a sistemas (administrador funcional/area de soporte son falsos)
	boolean sistemaImpactado				// si va en la grilla de sistemas
	boolean tieneDisenoExterno
	boolean tieneEstrategiaPrueba
	boolean tienePlanificacionyEsfuerzo
	boolean permiteIngresarOtrosCostos
	boolean ingresaTicketSolman
	// Nuevo, indica si el tipo impacto debe tenerse en cuenta para mostrar la descripción del padre
	boolean participaDescripcionFasesHijos
	String codigoAgrupador
	String codigo

	List fases

	static hasMany = [ fases : Fase ]

	static constraints = {
		descripcion(size:1..100)
		codigoAgrupador(size: 1..40)
		codigo(nullable: true, maxSize: 3)
	}

	String toString() {
		"${descripcion}"
	}

	TipoImpacto() {
		fases = new ArrayList()
	}

	def faseSiguienteA(unaFase) {
		def indiceFaseActual = fases.indexOf(unaFase)
		if (indiceFaseActual == -1) {
			//throw new BusinessException("No se encontró la fase " + unaFase + " en el conjunto de fases asociado con el tipo de impacto " + this)
			return null
		}
		def indiceSiguiente = indiceFaseActual + 1
		if (indiceSiguiente < fases.size()) {
			return fases.get(indiceSiguiente)
		} else {
			return null
		}
	}

	def fasesAnterioresA(unaFase) {
		def desde = fases.indexOf(unaFase)
		return fases.subList(0, desde)
	}
	
	def fasesPosterioresA(unaFase) {
		def desde = fases.indexOf(unaFase)
		return fases.subList(desde + 1, fases.size() - 1)
		// TODO: Probar
	}


	boolean apruebaPAU() {
		return [
			SOFTWARE_DESARROLLO,
			DATOS_REFERENCIALES
		].contains(descripcion)
	}

	boolean estaAsociadoASistemas() {
		return descripcion.equalsIgnoreCase(SOFTWARE_DESARROLLO) || descripcion.equalsIgnoreCase(SOFTWARE_DESARROLLO_SAP) || descripcion.equalsIgnoreCase("Datos referenciales")
	}

	boolean aplicaOtrasPruebas(){
		return descripcion.equalsIgnoreCase(SOFTWARE_DESARROLLO) || descripcion.equalsIgnoreCase(SOFTWARE_DESARROLLO_SAP) || descripcion.equalsIgnoreCase(SOPORTE_A_PRUEBAS)
	}

	boolean esSWDesarrolloSAP(){
		return descripcion.equalsIgnoreCase(SOFTWARE_DESARROLLO_SAP)
	}

	boolean contiene(unaFase) {
		return fases.any { fase ->
			fase.id.equals(unaFase.id) 
		}
	}

}
