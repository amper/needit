package ar.com.telecom.pcs.entities

import groovy.time.TimeCategory

import java.text.DecimalFormat

import ar.com.telecom.exceptions.BusinessException

class Periodo {

	int anio
	int mes
	
    static constraints = {
    }
	
	String toString() {
		return new DecimalFormat("00").format(mes) + "/" + anio
	}
	
	boolean between(Date fechaDesde, Date fechaHasta) {
		def periodos = getPeriodos(fechaDesde, fechaHasta)
		return periodos.contains(this)
	}
	
	static def getPeriodos(Date fechaDesde, Date fechaHasta) {
		if (!fechaDesde || !fechaHasta) {
			throw new BusinessException("Debe ingresar fechas desde / hasta")
		}
		def result = []
		def cal = Calendar.instance
		cal.set(year: fechaDesde.year + 1900, month: fechaDesde.month, date: 1)
		
		cal.set(Calendar.HOUR_OF_DAY, 0); 
		cal.set(Calendar.MINUTE, 0);
		cal.set(Calendar.SECOND, 0);
		cal.set(Calendar.MILLISECOND, 0);
				
		def fechaAux = cal.time
		
		while (fechaAux <= fechaHasta) {
			def mes = fechaAux[Calendar.MONTH] + 1
			def anio = fechaAux[Calendar.YEAR]
			
			def periodo = Periodo.findByMesAndAnio(mes, anio)
			if (!periodo) {
				periodo = new Periodo(mes: mes, anio: anio).save(flush: true, failOnError: true)
			}
			result.add(periodo)
			use (TimeCategory) {
				fechaAux = fechaAux + 1.month
			}
		}
		return result
	}
	
	public int hashCode() {
		return (anio * 100 + mes).hashCode()
	}
	
	public boolean equals(Object other) {
		if (!other) {
			return false
		}
		return this.hashCode().equals(other.hashCode())
	}
	
	public int compareTo(Periodo otroPeriodo){
		if (!otroPeriodo) {
			return 1;
		}
		return this.hashCode().compareTo(otroPeriodo.hashCode())
	}
}
