package ar.com.telecom.pcs.entities

import java.util.Date
import java.util.List

import ar.com.telecom.pedido.estrategiaPrueba.EstrategiaAdapter
import ar.com.telecom.pedido.estrategiaPrueba.TipoPruebaAdapter
import ar.com.telecom.ume.UmeService

/**
 * Representa una Especificaci\u00F3n de Requerimiento de Software
 * que puede aprobarse o denegarse, generando n versiones
 * 
 * @author u546049
 *
 */
class ECS {

	int versionECS
	Date fechaDeCarga
	String especificacionDetallada
	String alcanceDetallado
	String notas
	String fueraAlcance
	String beneficiosEsperados

	List sistemasImpactados
	List areasImpactadas
	List otrasPruebasECS

	// Estrategia de prueba
	Boolean realizaPI
	Justificacion justificacionPI
	Boolean PIintegrada
	Boolean realizaPAU
	Justificacion justificacionPAU
	Boolean PAUintegrada

	static hasMany = [ otrasPruebasECS: ECSOtrasPruebas,
		sistemasImpactados: SistemaImpactado,
		areasImpactadas: AreaSoporte,
		anexosPorTipo: AnexoPorTipo ]

	static constraints = {
		especificacionDetallada(nullable:true)
		alcanceDetallado(nullable:true)
		notas(nullable:true)
		fueraAlcance(nullable:true)
		beneficiosEsperados(nullable:true)
		realizaPI(nullable: true)
		PIintegrada(nullable: true)
		realizaPAU(nullable: true)
		PAUintegrada(nullable: true)
		justificacionPI(nullable:true)
		justificacionPAU(nullable:true)
		sistemasImpactados cascade: 'all-delete-orphan'
		areasImpactadas cascade: 'all'
		otrasPruebasECS cascade: 'all-delete-orphan'
	}

	static mapping = {
		especificacionDetallada type: 'text'
		alcanceDetallado type: 'text'
		notas type: 'text'
		fueraAlcance type: 'text'
		beneficiosEsperados type: 'text'
		justificacionPI fetch: 'join'
		justificacionPAU fetch: 'join'
	}

	public ECS() {
		fechaDeCarga = new Date()
		otrasPruebasECS = new ArrayList()
		sistemasImpactados = new ArrayList()
		areasImpactadas = new ArrayList()
		anexosPorTipo = []
	}

	def agregarOtraPruebaECS(otraPrueba){
		addToOtrasPruebasECS(otraPrueba)
	}

	def initialize(pedido, parametrizacionPI, parametrizacionPAU) {
		if (!this.fueraAlcance) {
			this.fueraAlcance = pedido.fueraAlcance
		}
		if (!this.beneficiosEsperados) {
			this.beneficiosEsperados = pedido.beneficiosEsperados
		}
		realizaPI = parametrizacionPI.valorRealiza()
		PIintegrada = parametrizacionPI.valorConsolida()
		realizaPAU = parametrizacionPAU.valorRealiza()
		PAUintegrada = parametrizacionPAU.valorConsolida()
	}

	def eliminarSistemaImpactado(sistemaAEliminar) {
		def sist = this.sistemasImpactados.find { sistema -> sistema.id == sistemaAEliminar.id }
		removeFromSistemasImpactados(sist)
	}

	def eliminarAreaImpactada(areaAEliminar) {
		removeFromAreasImpactadas(areaAEliminar)
	}

	def eliminarAnexo(unAnexo, usuarioLogueado) {
		// OJO, el removeFromAnexos no toma el id, por eso hay que hacer esto
		def anexo = this.anexosPorTipo.find { anexo -> anexo.id.equals(unAnexo.id) }
		this.removeFromAnexosPorTipo(anexo)
	}

	boolean tieneSistemaImpacto(otroSistemaImpacto){
		def tipoImpacto = this.sistemasImpactados.find { sistemaImpactado -> sistemaImpactado.sistema.descripcion.equalsIgnoreCase(otroSistemaImpacto.sistema.descripcion) && sistemaImpactado.tipoImpacto.descripcion.equalsIgnoreCase(otroSistemaImpacto.tipoImpacto.descripcion) }
		return (tipoImpacto != null)
	}

	def traerDatosRelacionados() {
		sistemasImpactados?.sistema?.descripcion
		sistemasImpactados?.tipoImpacto?.descripcion
		areasImpactadas?.descripcion
		anexosPorTipo?.tipoAnexo?.descripcion
		otrasPruebasECS?.tipoPrueba?.fases?.descripcion
	}

	String toString() {
		return "ECS " + sistemasImpactados
	}

	def actualizarEstrategiaPrueba(pedido) {
		if (pedido.pedidoDeEmergencia()) {
			realizaPI = null
			justificacionPI = null
			PIintegrada = null
		} else {
			if (realizaPI != null) {
				if (realizaPI == true) {
					justificacionPI = null	
				} else {
					PIintegrada = null
				}
			} else {
				justificacionPI = null
				PIintegrada = null
			}
		}
		if (realizaPAU != null) {
			if (realizaPAU == true) {
				justificacionPAU = null
			} else {
				PAUintegrada = null
			}
		} else {
			justificacionPAU = null
			PAUintegrada = null
		}
	}

	def validar(AbstractPedido pedido) {
		if (!especificacionDetallada || especificacionDetallada.equals("")) {
			pedido.errors.reject("Debe ingresar especificaci\u00F3n detallada")
		}

		if (!alcanceDetallado || alcanceDetallado.equals("")) {
			pedido.errors.reject("Debe ingresar alcance detallado");
		}

		if (!pedido.pedidoDeEmergencia()) {
			if (!realizaPI && !justificacionPI) {
				pedido.errors.reject("Si no realiza PI debe ingresar justificaci\u00F3n")
			}
			
			if (!realizaPI && PIintegrada) {
				pedido.errors.reject("Si no realiza PI no tiene sentido que marque la consolidaci\u00F3n")
			}
			
			if(!realizaPAU && otrasPruebasECS){
				pedido.errors.reject("No puede agregar pruebas adicionales si no realiza PAU")
			}
			
		}

		if (!realizaPAU && !justificacionPAU) {
			pedido.errors.reject("Si no realiza PAU debe ingresar justificaci\u00F3n")
		}

		def umeService = new UmeService()

		sistemasImpactados.each { sistemaImpactado ->
			sistemaImpactado.validar(pedido)
		}

		areasImpactadas.each { area ->
			if (umeService.getUsuariosGrupoLDAP(area.grupoAdministrador, null).isEmpty()) {
				pedido.errors.reject("El grupo " + area.grupoAdministrador + " para el \u00E1rea de soporte " + area + " no tiene definido usuarios")
			}

		}

		def hijosAsociadosASistemas = sistemasImpactados.findAll { sistemaImpactado ->
			sistemaImpactado.tipoImpacto?.asociadoSistemas
		}

		if (hijosAsociadosASistemas.isEmpty()) {
			pedido.errors.reject("Debe ingresar al menos un sistema con tipo de impacto asociado a sistemas (SW Desarrollo, SW Desarrollo SAP o Datos Referenciales)")
		}else{
			def sistemasNecesarios = hijosAsociadosASistemas.findAll { hijos -> hijos.tipoImpacto.estaAsociadoASistemas() }
			if (sistemasNecesarios.isEmpty()){
				pedido.errors.reject("Debe ingresar al menos un sistema con tipo de impacto asociado a sistemas (SW Desarrollo, SW Desarrollo SAP o Datos Referenciales)")
			}
		}

		return pedido.errors.errorCount == 0
	}

	def validarSistemasImpactadosDuplicados(AbstractPedido pedido) {
		Set tiposImpacto = []
		sistemasImpactados.each { sistemaImpactado ->
			def keySistema = sistemaImpactado.toString()
			if (tiposImpacto.contains(keySistema)) {
				pedido.errors.reject("El sistema " + sistemaImpactado + " est\u00E1 duplicado, s\u00F3lo se puede asociar un par sistema-tipo de impacto")
			} else {
				tiposImpacto.add(keySistema)
			}
		}
	}
	
	def obtenerTipoPrueba(String unTipoPrueba) {
		return otrasPruebasECS.find { it.tipoPrueba.descripcion.equalsIgnoreCase(unTipoPrueba) }
	}
	
	def getTipoPrueba(TipoPrueba tipoPrueba) {
		return obtenerTipoPrueba(tipoPrueba.descripcion)
	}

	def definirEstrategiaAdapter() {
		EstrategiaAdapter estrategiaAdapter = new EstrategiaAdapter()
		estrategiaAdapter.agregarTipoPrueba(new TipoPruebaAdapter(tipoPrueba: TipoPrueba.DESCRIPCION_PI, seRealiza: realizaPI, seConsolida: PIintegrada, justificacion: justificacionPI))
		estrategiaAdapter.agregarTipoPrueba(new TipoPruebaAdapter(tipoPrueba: TipoPrueba.DESCRIPCION_PAU, seRealiza: realizaPAU, seConsolida: PAUintegrada, justificacion: justificacionPAU))
		return estrategiaAdapter
	}
	
	def getAnexos(Fase fase) {
		return anexos.findAll { anexo -> anexo.esDeFase(fase) }
	}
	
	def getAnexos(){
		return anexosPorTipo
	}
}
