package ar.com.telecom.pcs.entities

import ar.com.telecom.Constantes

class SoftwareFactory {

	String descripcion
	boolean gestionaCapacidad
	String responsableCargaCapacidad
	boolean asociadoSistema //true: asociado a sistemas, false: asociado a áreas de soporte
	String grupoLDAP
	boolean manoObraPropia
	
	List stockMensualHoras
	
	public static final String grupoLDAPHorasCoordinacion = "Horas de Coordinaci\u00F3n"
	
    static constraints = {
		descripcion(size:1..80)
		responsableCargaCapacidad(maxSize: 80, nullable: true)
		grupoLDAP(size:1..80)
    }
	
	static hasMany = [ stockMensualHoras : StockMensualSWF, sistemas: Sistema ]

	static mapping = {
		stockMensualHoras cascade: 'all-delete-orphan'
	}
	
	static belongsTo = Sistema
	
	String toString() {
		descripcion
	}
	
	def valorDefaultHora(Periodo periodo) {
		if (gestionaCapacidad) {
			if (!periodo) {
				return null
			}
			def stockPeriodo = stockMensualHoras.find { stock -> periodo.between(stock.fechaDesde, stock.fechaHasta) }
			return stockPeriodo?.valor
		} else {
			if (manoObraPropia) {
				return Constantes.instance.parametros.valorHoraSWFTecoDefault
			} else {
				return valorUltimoStockMensual() 
			}
		}
	}

	def valorUltimoStockMensual() {
		if (stockMensualHoras.isEmpty()) {
			return null
		}
		return stockMensualHoras.last().valor 	
	}
	
	def getGrupoResponsable() {
		return grupoLDAP
	}
	
	def getLegajoUsuarioResponsable() {
		return responsableCargaCapacidad
	}
	
	public int compareTo(SoftwareFactory otraSWF){
		return this.descripcion.compareTo(otraSWF.descripcion)
	}
	
	public SoftwareFactory() {
		stockMensualHoras = []
	}
}

