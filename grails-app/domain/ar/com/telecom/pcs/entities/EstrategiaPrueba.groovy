package ar.com.telecom.pcs.entities

import java.util.Date;

class EstrategiaPrueba {

	Date fechaCarga
	int versionEP
	List detalles
	String comentario

	static constraints = {
		comentario(nullable:true)
	}
	
	static mapping = {
		comentario type: 'text'
		detalles cascade: 'all-delete-orphan'	
	}
	
	static hasMany = [ detalles : DetalleEstrategiaPrueba, anexos: AnexoPorTipo ]
	
	public String toString() {
		return "Estrategia de prueba - versi\u00F3n " + versionEP
	}
	
	EstrategiaPrueba() {
		versionEP = 0
		fechaCarga = new Date()
		detalles = new ArrayList()
	}
	
	def traerTipoPrueba(tipoPrueba) {
		if (!tipoPrueba) {
			return null
		} else {
			return detalles.find { detalle -> detalle.tipoPrueba.descripcion.equals(tipoPrueba?.descripcion) }
		}
	}
	
	def validar(AbstractPedido pedido) {
		detalles.each { detalle -> detalle.validar(pedido) }
	}

	def tieneTipoPrueba(TipoPrueba tipoPrueba) {
		return detalles.any { detallePrueba -> detallePrueba.tipoPrueba.id.equals(tipoPrueba.id) }
	}
	
	boolean realizaTipoPrueba(TipoPrueba tipoPrueba) {
		def detalle = traerTipoPrueba(tipoPrueba)
		return detalle?.seRealiza != null && detalle?.seRealiza != false
	}
	
	def eliminarAnexo(unAnexo, usuarioLogueado) {
		def anexo = this.anexos.find { anexo -> anexo.id.equals(unAnexo.id) }
		this.removeFromAnexos(anexo)
	}

	boolean consolidaPAU() {
		def detallePAU = getEstrategiaPAU()
		return detallePAU?.seRealiza && detallePAU?.seConsolida
	}

	boolean realizaPAU() {
		def detallePAU = getEstrategiaPAU()
		return detallePAU?.seRealiza
	}

	def getEstrategiaPAU() {
		return detalles.find { detalle -> detalle.tipoPrueba.equals(TipoPrueba.pruebaPAU()) }
	}
	
	def getDetallesConsolidados(pedido) {
		return pedido.getDetallesConsolidados(detalles)
	}
	
	def getAnexos(Fase fase) {
		return anexos.findAll { anexo -> anexo.esDeFase(fase) }
	}
}
