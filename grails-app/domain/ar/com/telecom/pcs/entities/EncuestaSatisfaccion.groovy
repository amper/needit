package ar.com.telecom.pcs.entities

class EncuestaSatisfaccion {

	public static int SINRESPUESTA = 0
	public static int DEFICIENTE = 1
	public static int REGULAR = 2
	public static int BUENA = 3
	public static int MUYBUENA = 4
	public static int EXCELENTE = 5
	
	int calidadSolucion
	int tiempoResolucion
	int adecuacionNecesidades
	int valoracionGeneral
	String comentariosSolucion
	
	int gradoSatisfaccionCCRSWF
	String comentariosGestion
	
	int agilidadNeedIT
	String comentariosHerramienta
	
	int gradoSatisfaccionProcesoDesarrollo
	String comentariosProceso
	
    static constraints = {
		comentariosSolucion(nullable:true)
		comentariosGestion(nullable:true)
		comentariosHerramienta(nullable:true)
		comentariosProceso(nullable:true)
    }
	
	static mapping = {
		comentariosSolucion type: 'text'
		comentariosGestion type: 'text'
		comentariosHerramienta type: 'text'
		comentariosProceso type: 'text'
	}
	
	static belongsTo = [ pedido : Pedido ]
	
	public static getEscalaSatisfaccion(){
		["Deficiente" : DEFICIENTE,
		 "Regular" : REGULAR,
		 "Buena" : BUENA,
		 "Muy Buena" : MUYBUENA,
		 "Excelente" : EXCELENTE]
	}
	
	public static getEscalaSatisfaccion(escala){
		switch (escala){
			case SINRESPUESTA:
				return "Sin Respuesta"
			case DEFICIENTE:
				return "Deficiente"
			case REGULAR:
				return "Regular"
			case BUENA:
				return "Buena"
			case MUYBUENA:
				return "Muy Buena"
			case EXCELENTE:
				return "Excelente"
		}
	}
}
