package ar.com.telecom.pcs.entities

import ar.com.telecom.Constantes
import ar.com.telecom.ume.UmeService

class RolAplicacion {

	static String USUARIO_FINAL = "UF"
	static String GERENTE_USUARIO = "GU"
	static String INTERLOCUTOR_USUARIO = "IU"
	static String GESTION_DEMANDA = "FGD"
	static String COORDINADOR_CAMBIO = "CC"
	static String RESPONSABLE_SWF = "RSWF"
	static String SOFTWARE_FACTORY = "SWF"
	static String ADMINISTRADOR_FUNCIONAL = "AF"
	static String AREA_SOPORTE = "AS"
	static String APROBADOR_IMPACTO = "AI"
	static String AREA_SOPORTE_IMPACTADA = "ASI"
	static String AREA_SOPORTE_NO_IMPACTADA = "ASNI"
	static String ADMINISTRADOR_FUNCIONAL_IMPACTADO = "AFI"
	static String RESPONSABLE_ACTIVIDAD = "RACT"
	static String APROBADOR_FASE = "APFA"
	static String RESPONSABLE_SISTEMA_IMPACTADO = "RSI"
	
	// No es un rol real, sirve para englobar responsable SWF, AF o AS
	static String RESPONSABLE_HIJO = "REHI" 
	
	String descripcion
	String propiedadUsuariosEnvioMails
	String propiedadGrupoEnvioMails
	boolean aplicaPadre
	boolean seleccionableEnListados
	String codigoRol
	
	static constraints = {
		descripcion(size:1..50)
		propiedadUsuariosEnvioMails(maxSize: 100, nullable: true)
		propiedadGrupoEnvioMails(size:1..100, nullable: true)
		codigoRol(size:2..5)
	}
	
	static hasMany = [ sistemas: Sistema ]
	
	String toString() {
		"${descripcion}"
	}

	boolean muestraDirecciones(){
		return [INTERLOCUTOR_USUARIO, GERENTE_USUARIO, APROBADOR_IMPACTO, GESTION_DEMANDA].contains(codigoRol)
	}

	static String getDescripcionRolAplicacion(codigoRol){
		def rol = RolAplicacion.findByCodigoRol(codigoRol)
		return rol?.descripcion
	}

	static def getDirecciones(pedido){
		Map mapaDirecciones = new HashMap()
		
		def legajoGerente = pedido.getLegajo(GERENTE_USUARIO)
		if(legajoGerente){
			addDireccion(legajoGerente, mapaDirecciones)
		}

		def legajoIU = pedido.getLegajo(INTERLOCUTOR_USUARIO)
		if(legajoIU){
			addDireccion(legajoIU, mapaDirecciones)
		}
		
		def legajoAI = pedido.getLegajo(APROBADOR_IMPACTO)
		if(legajoAI){
			addDireccion(legajoAI, mapaDirecciones)
		}
		
		if(pedido.getCodigoDireccionGerencia()){
			mapaDirecciones.put(pedido.getCodigoDireccionGerencia(), pedido.getDescripcionDireccionGerencia())
		}		
		
		return mapaDirecciones
	}

	private static def addDireccion(legajo, Map mapaDirecciones) {
		def umeService = new UmeService()
		def usuario = umeService.getUsuario(legajo)
		if(usuario?.direccionCodigo && usuario?.direccion){
			mapaDirecciones.put(usuario.direccionCodigo, usuario.direccion)
		}
		return umeService
	}
	
	Closure getMensajeUsuariosDeRol(final pedido, final direccion, final boolean esDirector){
		def parametros = Constantes.instance.parametros
		
		if(codigoRol.equalsIgnoreCase(GERENTE_USUARIO)){
			return { service -> service.getGerentesPorDireccion(direccion) }
		}
		
		if(codigoRol.equalsIgnoreCase(APROBADOR_IMPACTO)){
			return { service -> service.getAprobadoresImpactoPorDireccion(direccion, esDirector)}
		}

		if(codigoRol.equalsIgnoreCase(GESTION_DEMANDA)){
			return { service -> service.getGestionDemandaPorGrupoYDireccion(pedido.grupoGestionDemanda, direccion) }
		}

		if(codigoRol.equalsIgnoreCase(INTERLOCUTOR_USUARIO)){
			return { service -> service.getInterlocutoresUsuariosPorGrupoYDireccion(parametros.grupoLDAPInterlocutorUsuario, direccion) }
		}
		
		final def grupo = ""
		if(codigoRol.equalsIgnoreCase(USUARIO_FINAL)){
			grupo = parametros.grupoLDAPUsuarioFinal
		}
		
		if(codigoRol.equalsIgnoreCase(COORDINADOR_CAMBIO)){
			grupo = parametros.grupoLDAPCoordinadorPedido
		}

		return { service -> service.getUsuariosGrupoLDAP(grupo) }
	}

	def getGrupoActividad() {
		if(codigoRol.equalsIgnoreCase(INTERLOCUTOR_USUARIO)){
			return Actividad.INTERLOCUTOR_USUARIO
		}
		if(codigoRol.equalsIgnoreCase(USUARIO_FINAL)){
			return Actividad.USUARIO_FINAL
		}
		return null
	}
}
