package ar.com.telecom.pcs.entities

class UmbralAprobacionEconomica {

	TipoGestion tipoGestion
	BigDecimal rangoDesde
	BigDecimal rangoHasta
	
    static constraints = {
		rangoDesde(nullable: true)
		rangoHasta(nullable: true)
    }
	
	String toString() {
		def result = new StringBuffer()
		result.append(tipoGestion)
		result.append(" (")
		if (rangoDesde) {
			result.append(rangoDesde)
		} else {
			result.append("")
		}
		result.append(" -> ")
		if (rangoHasta) {
			result.append(rangoHasta)
		} else {
			result.append("")
		}
		result.append(")")
		return result.toString()
	}
}
