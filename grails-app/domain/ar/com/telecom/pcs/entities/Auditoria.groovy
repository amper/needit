package ar.com.telecom.pcs.entities

class Auditoria {

	String legajo
	Date ultimoIngreso

    static constraints = {
    }

	static mapping = {
		version false
	}

}
