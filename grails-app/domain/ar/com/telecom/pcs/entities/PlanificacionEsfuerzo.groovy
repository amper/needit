package ar.com.telecom.pcs.entities
import java.math.BigDecimal;
import java.util.Date;

import org.apache.commons.lang.StringUtils
import org.codehaus.groovy.grails.commons.ApplicationHolder

import ar.com.telecom.pcsPlus.PedidoService
import ar.com.telecom.util.DateUtil

class PlanificacionEsfuerzo {

	public static String ESTADO_PENDIENTE = "P"
	public static String ESTADO_CUMPLIDO = "C"
	public static String ESTADO_CANCELADO = "K"
	public static String ESTADO_EN_REPLANIFICACION ="R"

	Date fechaCreacion
	int versionPES
	String estado
	String tipoPlanificacion
	List detalles
	List otrosCostos

	Release numeroRelease
	Date fechaRelease
	String numeroSOLMAN
	String comentarioSistema

	static constraints = {
		tipoPlanificacion(size:1..100)
		numeroRelease(nullable: true)
		fechaRelease(nullable: true)
		numeroSOLMAN(nullable: true)
		comentarioSistema(nullable: true)
	}

	static mapping = {
		numeroRelease fetch: 'join'
		comentarioSistema type: 'text'
	}

	static hasMany = [ detalles : DetallePlanificacion , otrosCostos : OtroCosto ]

	String toString() {
		return tipoPlanificacion + " (" + version + ")"
	}

	PlanificacionEsfuerzo() {
		fechaCreacion = new Date()
		estado = ESTADO_PENDIENTE
		detalles = new ArrayList()
		otrosCostos = new ArrayList()
	}

	def agregarDetalle(detalle) {
		addToDetalles(detalle)
	}

	def quitarDetalle(detalle) {
		removeFromDetalles(detalle)
	}
	
	def agregarOtroCosto(otroCosto) {
		addToOtrosCostos(otroCosto)
	}

	def quitarOtroCosto(otroCosto) {
		removeFromOtrosCostos(otroCosto)
	}

	def estadoDescripcion() {
		if (estado.equalsIgnoreCase(ESTADO_PENDIENTE)) {
			return "Pendiente"
		}
		if (estado.equalsIgnoreCase(ESTADO_CUMPLIDO)) {
			return "Cumplido"
		}
		if (estado.equalsIgnoreCase(ESTADO_CANCELADO)) {
			return "Cancelado"
		}
		if (estado.equalsIgnoreCase(ESTADO_EN_REPLANIFICACION)) {
			return "En Replanificación"
		}
	}

	def finalizar() {
		estado = ESTADO_CUMPLIDO
	}

	def estaCumplido() {
		return estado.equalsIgnoreCase(ESTADO_CUMPLIDO)
	}

	def estaCancelado() {
		return estado.equalsIgnoreCase(ESTADO_CANCELADO)
	}
	
	def estaEnReplanificacion() {
		return estado.equalsIgnoreCase(ESTADO_EN_REPLANIFICACION)
	}

	def totalHoras() {
		return detalles.inject(0) { acum, detalle -> acum + detalle.totalHoras() }
	}

	def totalMontoHoras() {
		return detalles.inject(0) { acum, detalle -> acum + detalle.totalMontoHoras() }
	}

	def totalMontoOtrosCostos(){
		return otrosCostos.inject(0) { acum, otroCosto -> acum + otroCosto.costo }
	}

	def totalMontoGeneral() {
		return totalMontoHoras() + totalMontoOtrosCostos()
	}

	def validar(AbstractPedido pedido) {
		if (estaCancelado()) {
			return
		}

		//		if (pedido.cargaRelease() && !(numeroRelease || fechaRelease)) {
		//			pedido.errors.reject("Planificaci\u00F3n y esfuerzo: Debe ingresar fecha y número de release")
		//		}
		//
		if (!numeroRelease && fechaRelease) {
			pedido.errors.reject("Planificaci\u00F3n y esfuerzo: Ingres\u00F3 una fecha de release pero no indic\u00F3 un n\u00FAmero de release")
		}

		//		if (numeroRelease && !fechaRelease) {
		//			pedido.errors.reject("Planificaci\u00F3n y esfuerzo: Ingres\u00F3 un número de release pero no indic\u00F3 la fecha de release")
		//		}

		if (pedido.ingresaTicketSolman() && !numeroSOLMAN) {
			pedido.errors.reject("Planificaci\u00F3n y esfuerzo: Debe ingresar un ticket SOLMAN")
		}

		def detallesCargados = detalles.any { detalle -> detalle.estaCargado() }

		log.info "Pedido es de sistema: " + pedido.sistema()
		log.info "Alg\u00FAn detalle est\u00E1 cargado: " + detallesCargados

		if (!detallesCargados && requiereCargarDetalles(pedido)) {
			pedido.errors.reject "Planificaci\u00F3n y esfuerzo: Debe ingresar al menos una actividad para los pedidos de sistema"
		}

		detalles.each { detalle -> detalle.validar(pedido) }
	}
	
	boolean requiereCargarDetalles(pedido) {
		pedido.requiereCargarDetalles()
	}

	def detallesAEditar(){
		return detalles.findAll { detalle -> detalle.puedeCargarHoras()}
	}

	public Date buscarFechaFin(String descripcion) {
		return detalles.find { detalle -> detalle.descripcion.equalsIgnoreCase(descripcion) }.fechaFin
	}

	def traerDatosRelacionados() {
		otrosCostos?.tipoCosto?.descripcion
		detalles?.fechaDesde
	}

	def getGrupos() {
		/* ASI ANDA SIN SET
		 * def grupos = detalles.collect { detalle -> detalle.grupos }.flatten()
		 return grupos*/

		def grupos = new HashSet( detalles.collect { detalle -> detalle.grupos }.flatten() )
		return new ArrayList(grupos)
	}

	def getGrupos(origen) {
		/* ASI ANDA SIN SET
		 * def grupos = detalles.collect { detalle -> detalle.grupos }.flatten()
		 return grupos*/

		def grupos = new HashSet( detalles.collect { detalle -> detalle.getGrupos(origen) }.flatten() )
		return new ArrayList(grupos)
	}

	def grupoTieneCargadoRealIncurrido(grupo){
		obtenerDetallesPlanificacion(grupo).any { detalle -> detalle.totalHorasIncurridas(grupo) != null && detalle.totalHorasIncurridas(grupo) != 0 }
	}

	def grupoTieneCerradoRealIncurrido(grupo){
		return !obtenerDetallesPlanificacion(grupo).any { detalle -> !detalle.confirmoRealesIncurridos(grupo) }
	}

	def obtenerDetallesPlanificacion(grupo){
		if(grupo == null){
			return detalles
		}
		if (grupo.equalsIgnoreCase(SoftwareFactory.grupoLDAPHorasCoordinacion)) {
			return detalles.findAll { detalle -> detalle.noTieneGrupo()}
		}
		return detalles.findAll { detalle -> detalle.tieneGrupo(grupo)}
	}

	def reenviar() {
		estado = ESTADO_PENDIENTE
	}

	boolean tieneFechaImplantacion(pedido, impacto){
		def pedidoService = new PedidoService()
		def consolidados =  pedidoService.planificacionConsolidada(pedido, impacto)

		return consolidados.any { detalle ->
			detalle.getAt(0).esImplantacion() && DateUtil.toString(detalle.getAt(1)) && DateUtil.toString(detalle.getAt(2))
		}
	}

	def obtenerDetallesDeAreaDeSoporte(){
		return detalles.findAll { detalle -> detalle.esDeAreaDeSoporte()}
	}

	def getTareasSoporte() {
		return obtenerDetallesDeAreaDeSoporte()?.tareaSoporte
	}

	def grupoTieneCargadoRealIncurrido(grupo, origen){
		obtenerDetallesPlanificacion(grupo, origen).any { detalle -> detalle.totalHorasIncurridas(grupo) != null && detalle.totalHorasIncurridas(grupo) != 0 }
	}

	def grupoTieneCerradoRealIncurrido(grupo, origen){
		return !obtenerDetallesPlanificacion(grupo, origen).any { detalle -> !detalle.confirmoRealesIncurridos(grupo) }
	}

	def obtenerDetallesPlanificacion(grupo, origen){
		if(grupo == null){
			return detalles
		}
		if (grupo.equalsIgnoreCase(SoftwareFactory.grupoLDAPHorasCoordinacion)) {
			return detalles.findAll { detalle -> detalle.noTieneGrupo()}
		}
		return detalles.findAll { detalle -> detalle.tieneGrupo(grupo, origen)}

	}
	
	boolean enEspecificacion() {
		return [ESTADO_PENDIENTE, ESTADO_CUMPLIDO].contains(estado)
	}
	
	boolean enReplanificacion() {
		return [ESTADO_EN_REPLANIFICACION, ESTADO_CUMPLIDO].contains(estado)
	}
	
	def getActividadesQuePuedenReplanificar(){
		//detalles.
	}
	
	def crearCopia(estadoNuevo){
		def datellesNuevos = []
		def otrosCostosNuevos = []
		def nuevaPlanificacion =  new PlanificacionEsfuerzo(
			fechaCreacion: new Date(), 
			versionPES: 1,
			version: 0, 
			tipoPlanificacion: this.tipoPlanificacion,
			numeroRelease: this.numeroRelease,
			fechaRelease: this.fechaRelease,
			numeroSOLMAN: this.numeroSOLMAN,
			comentarioSistema: this.comentarioSistema,
			estado: estadoNuevo
			//detalles: datellesNuevos, 
			//otrosCostos: otrosCostosNuevos
		)
		println "******detalles********"
		this.detalles.each{ detalle -> 
			//nuevaPlanificacion.agregarDetalle(deepClone(detalle))
			nuevaPlanificacion.agregarDetalle(copiarDetalle(detalle))
		}
		println "*******otros costos********"
		this.otrosCostos.each{ otroCosto ->
			nuevaPlanificacion.agregarOtroCosto(new OtroCosto(tipoCosto: otroCosto.tipoCosto, detalle: otroCosto.detalle, costo: otroCosto.costo))
		}
		println "******fin deep clone******"
		return nuevaPlanificacion
	}
	
	def copiarDetalle(detalle){
		def nuevoDetalle
		if (detalle.esDeSistema()){
			nuevoDetalle = new DetallePlanificacionSistema(actividadPlanificacion: detalle.actividadPlanificacion)
			println "****cargaHoras****"
			detalle.cargaHoras.each{ cargaHora ->
				nuevoDetalle.agregarCargaHora(
					new CargaHora(
						softwareFactory: cargaHora.softwareFactory, 
						periodo: cargaHora.periodo,
						cantidadHoras: cargaHora.cantidadHoras,
						valorHora: cargaHora.valorHora,
						totalMontoHoras: cargaHora.totalMontoHoras ))
			}
			println "****cargaHorasIncurridas****"
			detalle.cargaHorasIncurridas.each{ cargaHoraIncurrida ->
				nuevoDetalle.addToCargaHorasIncurridas(
					new CargaHoraIncurrida(
						softwareFactory: cargaHoraIncurrida.softwareFactory,
						cantidadHoras: cargaHoraIncurrida.cantidadHoras,
						fechaConfirmacionIncurridos: cargaHoraIncurrida.fechaConfirmacionIncurridos,
						usuarioConfirmadorIncurridos: cargaHoraIncurrida.usuarioConfirmadorIncurridos ))
			}
		}else{
			println "****DetallePlanificacionAreaSoporte****"
			nuevoDetalle = new DetallePlanificacionAreaSoporte(
				tareaSoporte: detalle.tareaSoporte,
				comentarioDetalleAreaSoporte: detalle.comentarioDetalleAreaSoporte,
				cantidadHoras: detalle.cantidadHoras,
				valorHora: detalle.valorHora,
				cantidadHorasIncurridas: detalle.cantidadHorasIncurridas,
				fechaConfirmacionIncurridos: detalle.fechaConfirmacionIncurridos,
				usuarioConfirmadorIncurridos: detalle.usuarioConfirmadorIncurridos
			)
		}
		return nuevoDetalle
	}
	
//	def deepClone(domainInstanceToClone){
//		def newDomainInstance = domainInstanceToClone.getClass().newInstance() 
//		def domainClass = ApplicationHolder.application.getDomainClass(newDomainInstance.getClass().name)
//		domainClass?.persistentProperties.each{prop -> 
//			if (!prop.name.equalsIgnoreCase("cargaHorasIncurridas")){ //Guarda con esto!!!!! Porque no anda???
//				if(prop.association){
//					if(prop.owningSide){
//						if(prop.oneToOne){
//							def newAssociationInstance = deepClone(domainInstanceToClone."${prop.name}")
//							newDomainInstance."${prop.name}" = newAssociationInstance 
//						}else{
//							domainInstanceToClone."${prop.name}".each{ associationInstance ->
//								def newAssociationInstance = deepClone(associationInstance)
//								newDomainInstance."addTo${StringUtils.capitalize(prop.name)}"(newAssociationInstance)
//							}
//						}
//					}else{ 
//						if(!prop.bidirectional){
//							newDomainInstance."${prop.name}" = domainInstanceToClone."${prop.name}"
//						}
//					}
//				}else{ 
//					newDomainInstance."${prop.name}" = domainInstanceToClone."${prop.name}"
//				}
//			}
//		}
//		return newDomainInstance
//	}
	
	
}