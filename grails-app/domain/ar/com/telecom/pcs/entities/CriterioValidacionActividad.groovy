package ar.com.telecom.pcs.entities

class CriterioValidacionActividad {

	String descripcion
	
	static constraints = {
		descripcion(size:1..30)
	}
	
	public boolean validar(Actividad actividad, AbstractPedido pedido) {
		return true
	}
	
	public String toString() {
		return "Sin validaci\u00F3n adicional"
	}
}
