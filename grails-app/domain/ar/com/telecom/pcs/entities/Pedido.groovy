package ar.com.telecom.pcs.entities

import java.util.Date

import org.apache.taglibs.standard.tag.el.fmt.FormatNumberTag;

import ar.com.telecom.Constantes
import ar.com.telecom.FaseMultiton
import ar.com.telecom.pcsPlus.ConfiguracionMailService
import ar.com.telecom.pcsPlus.PedidoService
import ar.com.telecom.pedido.especificacionImpacto.Impacto
import ar.com.telecom.pedido.observers.PedidoAprobacionesCambioEstadoObserver
import ar.com.telecom.pedido.observers.PedidoCambioDireccionObserver;
import ar.com.telecom.pedido.observers.PedidoModificacionObserver
import ar.com.telecom.ume.UmeService
import ar.com.telecom.util.DateUtil
import ar.com.telecom.util.NumberUtil

class Pedido extends AbstractPedido {

	//Datos del pedido original
	String descripcion
	String alcance
	String objetivo
	TipoPedido tipoPedido
	String fueraAlcance
	String infoAdicional
	Date fechaDeseadaImplementacion
	Sistema sistemaSugerido
	String beneficiosEsperados
	String riesgosObservables
	TipoReferencia tipoReferencia
	String referenciaOrigen
	TipoGestion tipoGestion

	//Segun Evaluaci\u00F3n cambio
	Prioridad prioridad

	//Según Consolidaci\u00F3n impacto
	Date fechaVencimientoPlanificacion
	String comentarios
	BigDecimal valorHora

	//Usuarios
	String legajoInterlocutorUsuario
	String legajoGerenteUsuario
	String codigoGerencia
	String descripcionGerencia
	String codigoDireccionGerencia
	String descripcionDireccionGerencia
	String grupoGestionDemanda
	String legajoUsuarioGestionDemanda
	String legajoCoordinadorCambio
	String legajoUsuarioAprobadorEconomico

	//Fechas del circuito y justificaciones
	Date fechaFinalizacionIngresoPedido
	Date fechaAprobacionPedidoGU
	Date fechaValidacionIU
	Date fechaEvaluacionPedidoFGD
	Date fechaAprobacionEspecificacionPedidoCC
	Date fechaAprobacionEspecificacionImpacto
	Date fechaValidacionImpacto
	Date fechaAprobacionEconomica
	Date fechaAprobacionPAU
	//Date fechaFinConstruccion

	List especificaciones
	List pedidosHijos

	//Finalizaci\u00F3n
	EncuestaSatisfaccion encuestaSatisfaccion

	static constraints = {
		descripcion(nullable:true)
		alcance(nullable:true)
		objetivo(nullable:true)
		tipoPedido(nullable:true)
		fueraAlcance(nullable:true)
		infoAdicional(nullable:true)
		fechaDeseadaImplementacion(nullable:true)
		sistemaSugerido(nullable:true)
		beneficiosEsperados(nullable:true)
		riesgosObservables(nullable:true)
		tipoReferencia(nullable:true)
		referenciaOrigen(maxSize:250, nullable:true)
		tipoGestion(nullable:true)
		prioridad(nullable:true)
		fechaVencimientoPlanificacion(nullable:true)
		comentarios(nullable:true)
		valorHora(nullable:true)
		legajoInterlocutorUsuario(nullable:true)
		legajoGerenteUsuario(nullable:true)
		codigoGerencia(maxSize:60, nullable:true)
		descripcionGerencia(maxSize: 100, nullable:true)
		codigoDireccionGerencia(maxSize:60, nullable:true)
		descripcionDireccionGerencia(maxSize: 100, nullable:true)
		grupoGestionDemanda(maxSize:100, nullable:true)
		legajoUsuarioGestionDemanda(nullable:true)
		legajoCoordinadorCambio(nullable:true)
		legajoUsuarioAprobadorEconomico(nullable:true)
		fechaFinalizacionIngresoPedido(nullable:true)
		fechaAprobacionPedidoGU(nullable:true)
		fechaValidacionIU(nullable:true)
		fechaEvaluacionPedidoFGD(nullable:true)
		fechaAprobacionEspecificacionPedidoCC(nullable:true)
		fechaAprobacionEspecificacionImpacto(nullable:true)
		fechaValidacionImpacto(nullable: true)
		fechaAprobacionEconomica(nullable:true)
		fechaAprobacionPAU(nullable:true)
		encuestaSatisfaccion(nullable: true)
		//fechaFinConstruccion(nullable: true)
	}

	static hasMany = [ especificaciones : ECS,  pedidosHijos : PedidoHijo ]

	static mapping = {
		discriminator value: 'P'
		descripcion type: 'text'
		alcance type: 'text'
		objetivo type: 'text'
		fueraAlcance type: 'text'
		infoAdicional type: 'text'
		beneficiosEsperados type: 'text'
		riesgosObservables type: 'text'
		comentarios type: 'text'
		tipoReferencia fetch: 'join'
		tipoPedido fetch: 'join'
		tipoGestion fetch: 'join'
		prioridad fetch: 'join'
		sistemaSugerido fetch: 'join'
		encuestaSatisfaccion fetch: 'join'
	}

	public Pedido() {
		super()
		especificaciones = new ArrayList()
	}

	public boolean validarIngresoPedido(aprobacionUI){
		if (!this.titulo || this.titulo.equals("")){
			this.errors.rejectValue("titulo", 'default.null.message',['titulo']as Object[], "");
		}

		if (this.titulo.size() > 200) {
			this.errors.reject ("El t\u00EDtulo no debe exceder los 200 caracteres")
		}

		if (!this.descripcion || this.descripcion.equals("")){
			this.errors.rejectValue("descripcion", 'default.null.message',['descripcion']as Object[], "");
		}

		if (!this.objetivo || this.objetivo.equals("")){
			this.errors.rejectValue("objetivo",'default.null.message',['objetivo']as Object[], "");
		}

		if (!this.alcance || this.alcance.equals("")){
			this.errors.rejectValue("alcance",'default.null.message',['alcance']as Object[], "");
		}

		if (!this.tipoPedido || this.tipoPedido.equals("")){
			this.errors.rejectValue("tipoPedido",'default.null.message',['tipoPedido']as Object[], "");
		}

		if (!this.legajoGerenteUsuario || this.legajoGerenteUsuario.equals("")){
			this.errors.rejectValue("legajoGerenteUsuario","default.null.message",["Gerente Usuario"]as Object[], "");
		}

		if (!legajoInterlocutorUsuario || this.legajoInterlocutorUsuario.equals("")) {
			this.errors.rejectValue("legajoInterlocutorUsuario",'default.null.message',['interlocutor usuario']as Object[], "");
		}

		if (this.fechaDeseadaImplementacion && this.fechaDeseadaImplementacion.before(new Date())){
			this.errors.rejectValue("fechaDeseadaImplementacion",'msg.fechaposterior',['deseada de implementaci\u00F3n']as Object[], "");
		}

		if (this.fechaDeseadaImplementacion) {
			if (this.fechaDeseadaImplementacion.year > 8099) {
				this.errors.rejectValue("fechaDeseadaImplementacion",'msg.anioinferior',['deseada de implementaci\u00F3n']as Object[], "");
			}
		}

		if (tipoReferencia != null && referenciaOrigen == null){
			this.errors.reject ("Debe ingresar una referencia origen v\u00E1lida")
		}

		if (referenciaOrigen?.size() > 250) {
			this.errors.reject ("La referencia origen no debe exceder los 250 caracteres")
		}

		// Si es un tipo de referencia de needIt, valido que la referencia de origen sea v?lida
		if(tipoReferencia != null && tipoReferencia.esDeNeedIt()){
			try {
				if (referenciaOrigen && referenciaOrigen.contains("-")) {
					def referenciaArray = referenciaOrigen.split("-")
					int nroReferenciaOrigen = Integer.parseInt(referenciaArray[0].trim()).intValue()
					def pedidoOrigen = Pedido.findById(nroReferenciaOrigen)

					if (!pedidoOrigen){
						this.errors.reject ("La referencia origen no es un pedido v\u00E1lido")
					}
				} else {
					this.errors.reject ("Debe ingresar referencia origen")
				}
			} catch(NumberFormatException e){
				this.errors.reject ("La referencia origen no es un nro de pedido v\u00E1lido")
			}
		}

		// Valido interlocutor y gerente en igual direccion
		validaDireccionGerenteInterlocutor()

		return this.errors.errorCount == 0
	}


	public boolean validarValidacionPedido(aprobacionUI) {
		return validarAprobacionGeneral(aprobacionUI) && validaDireccionGerenteInterlocutor()
	}

	public boolean validarAprobacionPedido(aprobacionUI) {
		def resultado = validarAprobacionGeneral(aprobacionUI)
		if (resultado && aprobacionUI?.aprueba && !legajoInterlocutorUsuario) {
			this.errors.rejectValue("legajoInterlocutorUsuario",'default.null.message',['interlocutor usuario']as Object[], "");
			resultado = false
		}
		return resultado
	}

	public boolean validarEvaluacionCambio(aprobacionUI) {
		if (!this.grupoGestionDemanda || this.grupoGestionDemanda.equals("")){
			this.errors.rejectValue("grupoGestionDemanda", 'default.null.message',[
				'grupo gesti\u00F3n demanda'
			]as Object[], "");
		}

		if (!this.legajoUsuarioGestionDemanda || this.legajoUsuarioGestionDemanda.equals("")){
			this.errors.rejectValue("legajoUsuarioGestionDemanda", 'default.null.message',[
				'asignatario gesti\u00F3n demanda'
			]as Object[], "");
		}

		if (aprobacionUI?.aprueba && (!this.legajoCoordinadorCambio || this.legajoCoordinadorCambio.equals(""))){
			this.errors.rejectValue("legajoCoordinadorCambio", 'default.null.message',['coordinador cambio']as Object[], "");
		}

		return validarAprobacionGeneral(aprobacionUI) && (this.errors.errorCount == 0)
	}

	public boolean validarEspecificacionCambio(aprobacionUI) {
		def resultado = validarAprobacionGeneral(aprobacionUI)

		if (!tipoGestion) {
			this.errors.reject("Debe ingresar tipo de gesti\u00F3n")
			resultado = false
		}

		def ecs = obtenerUltimaEspecificacion()

		return resultado && ecs.validar(this)
	}

	public boolean validarConsolidacionImpacto(aprobacionUI) {
		if (!getPlanificacionActual()) {
			this.errors.reject "Debe completar la planificaci\u00F3n y esfuerzo"
		} else {
			getPlanificacionActual().validar(this)

			if(!getPlanificacionActual().tieneFechaImplantacion(this, new Impacto(Impacto.IMPACTO_CONSOLIDADO, this))){
				this.errors.reject "Las fechas de la fase implantaci\u00F3n son obligatorias, solicite al RSWF que corresponda reenviando la especificaci\u00F3n de impacto"
			}
		}
		if (!getEstrategiaPruebaActual()) {
			this.errors.reject "Debe completar la estrategia de prueba"
		} else {
			getEstrategiaPruebaActual().validar(this)
		}
		if (mostrarSolapaDE()) {
			if (!getDisenioExternoActual()) {
				this.errors.reject "Debe completar el diseño externo"
			} else {
				getDisenioExternoActual().validar(this)
			}
		}

		if (!this.fechaVencimientoPlanificacion || this.fechaVencimientoPlanificacion.equals("")){
			this.errors.reject "Debe completar la fecha vto. planificaci\u00F3n"
		}

		pedidosHijos.each { pedidoHijo ->
			if (!pedidoHijo.cerroEspecificacion()) {
				this.errors.reject pedidoHijo.toString() + " tiene pendiente la especificaci\u00F3n"
			}
		}

		//		pedidosHijos.each { pedidoHijo ->
		//			if (pedidoHijo.denegoEspecificacion()) {
		//				this.errors.reject pedidoHijo.toString() + " deneg\u00F3 la especificaci\u00F3n"
		//			}
		//		}

		def algunoAprobo = pedidosHijos.any { pedidoHijo -> pedidoHijo.tipoImpacto.estaAsociadoASistemas() && !pedidoHijo.denegoEspecificacion() }

		if (!algunoAprobo) {
			this.errors.reject "Todos los hijos asociados a sistemas denegaron la especificaci\u00F3n: no podr\u00E1 avanzar el pedido"
		}

		return this.errors.errorCount == 0
	}

	public boolean validarValidacionImpacto(aprobacionUI) {
		if (!this.legajoUsuarioAprobadorEconomico && tipoGestion.descripcion?.equals(TipoGestion.GESTION_ESTANDAR)) {
			this.errors.reject "Debe ingresar aprobador de impacto"
		}
		aprobacionUI.validar()
		return this.errors.errorCount == 0
	}

	public boolean validarAprobacionImpacto(aprobacionUI) {
		aprobacionUI.validar()
		return this.errors.errorCount == 0
	}

	def getFase() {
		if (!faseActual) {
			faseActual = FaseMultiton.getFase(FaseMultiton.REGISTRACION_PEDIDO)
		}
		faseActual
	}

	public boolean validar(aprobacionUI) {
		def fase = getFase()

		log.info "Aprobacion aprueba: " + aprobacionUI.aprueba

		if (!aprobacionUI.aprueba) {
			return aprobacionUI.validar()
		}

		if(this.estaSuspendido()){
			this.errors.reject ("El pedido se encuentra suspendido")
		}

		if (fase.equals(faseRegistracionPedido())) {
			return this.validarIngresoPedido(aprobacionUI)
		}
		if (fase.equals(faseValidacionPedido())) {
			return this.validarIngresoPedido(aprobacionUI) && this.validarAprobacionPedido(aprobacionUI)
		}
		if (fase.equals(faseAprobacionPedido())) {
			return this.validarIngresoPedido(aprobacionUI) && this.validarValidacionPedido(aprobacionUI)
		}
		if (fase.equals(faseEvaluacionCambio())) {
			return this.validarEvaluacionCambio(aprobacionUI)
		}
		if (fase.equals(faseEspecificacionCambio())) {
			return this.validarEspecificacionCambio(aprobacionUI)
		}
		if (fase.equals(faseAprobacionCambio())) {
			return this.validarAprobacionGeneral(aprobacionUI)
		}
		if (fase.equals(faseConsolidacionImpacto())) {
			return this.validarConsolidacionImpacto(aprobacionUI)
		}
		if (fase.equals(faseValidacionImpacto())) {
			return this.validarValidacionImpacto(aprobacionUI)
		}
		if (fase.equals(faseAprobacionImpacto())) {
			return this.validarAprobacionImpacto(aprobacionUI)
		}
	}

	def doGuardar(usuarioLogueado) {
		def fase = faseActual
		if (!faseActual) {
			fase = FaseMultiton.getFase(FaseMultiton.REGISTRACION_PEDIDO)
		}
		if (fase.equals(faseRegistracionPedido())) {
			if (this.fechaDeseadaImplementacion) {
				if (this.fechaDeseadaImplementacion.year > 8099){
					this.fechaDeseadaImplementacion.year = 8099;
				}
			}

			def aprobacionesPendientesDeLaMismaFase = aprobaciones.findAll { aprobacion -> aprobacion.pendienteParaFase(faseActual) }
			if (aprobacionesPendientesDeLaMismaFase.isEmpty() && this.estaEnCurso()) {
				addToAprobaciones(new Aprobacion(fase: faseRegistracionPedido(), usuario: legajoUsuarioCreador, fecha: new Date(), estado: Aprobacion.PENDIENTE, rol: getRol(faseActual, usuarioLogueado)))
			}
		}
	}

	def fasesActuales() {
		if (!tipoGestion) {
			return Constantes.instance.parametros.tipoGestionDefault.fases[0..TipoGestion.CANT_FASES - 1]
		} else {
			return tipoGestion.fases
		}
	}

	def tipoGestion() {
		if (!tipoGestion) {
			return Constantes.instance.parametros.tipoGestionDefault
		} else {
			return tipoGestion
		}
	}

	def traerDatosRelacionados() {
		super.traerDatosRelacionados()

		especificaciones?.each { especificacion -> especificacion.traerDatosRelacionados() }
		pedidosHijos.each { pedido -> pedido.traerDatosRelacionados() }
	}

	public boolean avanzar(aprobacionUI, usuarioLogueado) {
		def fase = faseActual
		def grupoAprobador
		def descripcionEvento
		def tieneAprobacionOmitida = tieneAprobacionOmitida()

		// usuarioResponsable = getUsuarioResponsable(fase, usuarioLogueado)
		// FED - vuelvo a dejar el usuario logueado
		def usuarioResponsable = usuarioLogueado
		if (tieneAprobacionOmitida) {
			usuarioResponsable = getUsuarioResponsable(fase, usuarioLogueado)
		}

		if (!fase) {
			fase = faseRegistracionPedido()
		}
		def aprobacionesPendientesFaseActual = aprobaciones.findAll { aprobacion  -> aprobacion.pendienteParaFase(fase) }

		def faseProxima
		if (!tipoGestion) {
			faseProxima = Constantes.instance.parametros.tipoGestionDefault.faseSiguienteA(fase)
		} else {
			faseProxima = tipoGestion.faseSiguienteA(fase)
		}
		def usuarioAprobador = getUsuarioResponsable(faseProxima, usuarioLogueado)

		def tipoEventoAprobacion = tipoEventoAprobacion()
		def descripcionAdicionalOmitida = ""

		if (fase.equals(faseRegistracionPedido())) {
			fechaFinalizacionIngresoPedido = new Date()
			descripcionEvento = "Finalizaci\u00F3n Pedido Usuario Final. "
		}
		if (fase.equals(faseValidacionPedido())) {
			fechaAprobacionPedidoGU = new Date()
			descripcionEvento = "Aprobaci\u00F3n Gerente Usuario. "
			descripcionAdicionalOmitida = "Creador = Gerente"
		}
		if (fase.equals(faseAprobacionPedido())) {
			fechaValidacionIU = new Date()
			grupoAprobador = Constantes.instance.parametros.grupoLDAPFuncionGestionDemandaGral
			grupoGestionDemanda = Constantes.instance.parametros.grupoLDAPFuncionGestionDemandaGral
			descripcionEvento = "Aprobaci\u00F3n Validaci\u00F3n Interlocutor Usuario. "
			if (legajoUsuarioCreador.equalsIgnoreCase(legajoInterlocutorUsuario)) {
				descripcionAdicionalOmitida = "Creador = Interlocutor. "
			}
			if (legajoGerenteUsuario.equalsIgnoreCase(legajoInterlocutorUsuario)) {
				descripcionAdicionalOmitida = "Gerente = Interlocutor. "
			}
		}
		if (fase.equals(faseEvaluacionCambio())) {
			fechaEvaluacionPedidoFGD = new Date()
			descripcionEvento = "Aprobaci\u00F3n Evaluaci\u00F3n Cambio Gesti\u00F3n Demanda. "
		}
		if (faseProxima.equals(faseEspecificacionCambio())) {
			this.obtenerUltimaEspecificacion()
		}
		if (fase.equals(faseEspecificacionCambio())) {
			// Agregado: borro las aprobaciones anteriores
			def aprobacionesDeAprobacionCambio = aprobaciones.findAll { aprobacion ->
				aprobacion.fase.equals(faseAprobacionCambio())
			}
			log.info "Aprobaciones de aprobacion cambio " + aprobacionesDeAprobacionCambio
			aprobacionesDeAprobacionCambio.each { aprobacionAux -> removeFromAprobaciones(aprobacionAux) }
			log.debug "Aprobaciones queda " + aprobaciones
			if (faseProxima.equals(faseAprobacionCambio())) {
				fechaAprobacionEspecificacionPedidoCC = new Date()
				descripcionEvento = "Aprobaci\u00F3n Especificaci\u00F3n. "
			} else {
				usuarioAprobador = legajoCoordinadorCambio
				descripcionEvento = "Aprobaci\u00F3n Cambio. "
			}
		}
		if (fase.equals(faseAprobacionCambio())) {
			descripcionEvento = "Aprobaci\u00F3n Cambio. "
		}

		if (fase.equals(faseConsolidacionImpacto())) {
			descripcionEvento = "Aprobaci\u00F3n Consolidaci\u00F3n Impacto. "
			fechaAprobacionEspecificacionImpacto = new Date()
			actualizarEspecificacionImpacto()
			getPlanificacionActual()?.finalizar()
		}
		if (fase.equals(faseValidacionImpacto())) {
			fechaValidacionImpacto = new Date()
			descripcionEvento = "Aprobaci\u00F3n Validaci\u00F3n Impacto. "
		}
		if (fase.equals(faseAprobacionImpacto())) {
			fechaAprobacionEconomica = new Date()
			descripcionEvento = "Aprobaci\u00F3n Impacto. "
			descripcionAdicionalOmitida = "Interlocutor Usuario = Aprobador Impacto. "
		}
		if (aprobacionUI.justificacion && !tieneAprobacionOmitida) {
			descripcionEvento += "Justificaci\u00F3n: " + aprobacionUI.justificacion + ". "
		}
		if (tieneAprobacionOmitida) {
			descripcionEvento = descripcionEvento + descripcionAdicionalOmitida
		}
		aprobacionesPendientesFaseActual.each { aprobacionIt ->
			if (aprobacionIt.usuario?.equalsIgnoreCase(usuarioLogueado)) {
				aprobacionIt.aprobar()
				// Def. 206 se pide no copiar
				// aprobacionIt.justificacion = aprobacionUI?.justificacion
			}
		}

		addToLogModificaciones(new LogModificaciones(legajo: usuarioResponsable, fase: fase, fechaDesde: fechaDesdePara(fase), fechaHasta: new Date(), descripcionEvento: descripcionEvento, tipoEvento: tipoEventoAprobacion, rol: getRol(fase, usuarioResponsable)))

		fechaUltimaModificacion = new Date()

		def aprobacionesPendientesDeOtrosUsuarios = aprobacionesPendientesDeOtrosUsuarios(fase, usuarioLogueado)

		if (tieneAprobacionOmitida) {
			aprobacionesPendientesDeOtrosUsuarios.each { aprobacionIt ->
				aprobacionIt.aprobar()
				aprobacionIt.justificacion = aprobacionUI?.justificacion
				aprobacionesPendientesDeOtrosUsuarios = []
			}
		}

		log.info "Fase pr\u00F3xima: " + faseProxima
		log.info "aprobaciones pendientes de fase actual " + aprobacionesPendientesFaseActual
		log.info "aprobaciones pendientes de otros usuarios " + aprobacionesPendientesDeOtrosUsuarios

		if (aprobacionesPendientesDeOtrosUsuarios.isEmpty()) {
			faseActual = faseProxima
			if (faseProxima.equals(faseConstruccionCambio())) {
				pedidosHijos.each { pedidoHijo -> pedidoHijo.finalizarFase(aprobacionUI, usuarioLogueado) }
			} else {
				def aprobacion = new Aprobacion(fase: faseProxima, grupo: grupoAprobador, usuario: usuarioAprobador, fecha: new Date(), estado: Aprobacion.PENDIENTE, rol: getRol(faseProxima, usuarioAprobador))
				addToAprobaciones(aprobacion)
			}
			if (faseActual.equals(faseConsolidacionImpacto())) {
				generarHijos(usuarioLogueado)
			}
			this.notificarCambioEstado()
		}
	}

	/**
	 * Método de negocio especial para pasar a finalizado cuando el último de los hijos cierre su fase
	 * @param usuarioLogueado
	 * @return
	 */
	def pasarACierre(usuarioLogueado) {
		log.info "Pasar a cierre"
		if (pedidosHijos.any { pedidoHijo -> !pedidoHijo.cerroUltimaFase() }) {
			log.info "Alguno de los hijos no cerr\u00F3 la \u00FAltima fase"
			return
		}
		// Agrego log
		def usuarioResponsable = getUsuarioResponsable(faseActual, usuarioLogueado)
		addToLogModificaciones(new LogModificaciones(legajo: usuarioResponsable, fase: faseActual, fechaDesde: fechaDesdePara(faseActual), fechaHasta: new Date(), descripcionEvento: "Cierre de todas las gestiones derivadas", tipoEvento: LogModificaciones.APROBACION, rol: getRol(faseActual, usuarioResponsable)))
		// Actualizo pedido
		faseActual = faseFinalizacion()
		log.info "Fase actual del pedido " + this.id + ": " + faseActual
		fechaUltimaModificacion = new Date()
		// Genero aprobacion
		usuarioResponsable = getUsuarioResponsable(faseActual, usuarioLogueado)
		def aprobacion = new Aprobacion(fase: faseActual, grupo: null, usuario: usuarioResponsable, fecha: new Date(), estado: Aprobacion.PENDIENTE, rol: getRol(faseActual, usuarioResponsable))
		addToAprobaciones(aprobacion)
		// Notifico observers
		this.notificarCambioEstado()
	}

	def finalizarPedido(encuesta, usuarioLogueado){
		fechaCierre = new Date()
		encuestaSatisfaccion = encuesta
		addToLogModificaciones(new LogModificaciones(legajo: usuarioLogueado, fechaDesde: new Date(), rol: getRol(faseActual, usuarioLogueado), fechaHasta: new Date(), fase: faseActual, tipoEvento: LogModificaciones.APROBACION, descripcionEvento: "Finalizaci\u00F3n del Pedido"))
		def aprobacionesPendientesFaseActual = aprobaciones.findAll { aprobacion  -> aprobacion.pendienteParaFase(faseActual) }
		aprobacionesPendientesFaseActual.each { aprobacionIt ->
			if (aprobacionIt.usuario?.equalsIgnoreCase(usuarioLogueado)) {
				aprobacionIt.aprobar()
			}
		}
		this.notificarCambioEstado()
	}

	def pedidosHijosPosibles() {
		def ecs = obtenerUltimaEspecificacion()
		def hijosPosibles = ecs?.sistemasImpactados.plus(ecs?.areasImpactadas)
		return hijosPosibles
	}

	def generarHijos(usuarioLogueado) {
		def configuracionMailService = new ConfiguracionMailService()

		pedidosHijosPosibles().each { pedidoHijoPosible ->
			def tipoImpactoHijo = pedidoHijoPosible.tipoImpacto()

			log.info "pedidoHijoPosible.tipoImpacto: " + tipoImpactoHijo
			log.info "pedidoHijoPosible.sistema: " + pedidoHijoPosible.sistema()

			def user = pedidoHijoPosible.usuarioResponsable()
			if (!user && pedidoHijoPosible?.areaSoporte()){
				user = getUsuarioAprobadorAreaSoporte(pedidoHijoPosible?.areaSoporte())
			}

			def pedidoHijo = new PedidoHijo(tipoImpacto: tipoImpactoHijo,
					sistema: pedidoHijoPosible.sistema(), areaSoporte: pedidoHijoPosible.areaSoporte(),
					titulo: this.titulo,
					legajoUsuarioCreador: usuarioLogueado, fechaCargaPedido: new Date(),
					fechaUltimaModificacion: new Date(),
					grupoResponsable: pedidoHijoPosible.grupoResponsable, legajoUsuarioResponsable: user, // antes era pedidoHijoPosible.usuarioResponsable()
					faseActual: faseEspecificacionImpacto())

			addToPedidosHijos(pedidoHijo)
			
			log.info "pedidoHijo: ${pedidoHijo}"

			def faseHijo = tipoImpactoHijo.fases.first()

			def aprobacion = new Aprobacion(areaSoporte: pedidoHijoPosible.areaSoporte(), fase: faseHijo, grupo: pedidoHijoPosible.grupoResponsable(), usuario: user, fecha: new Date(), estado: Aprobacion.PENDIENTE, rol: getRol(faseActual, usuarioLogueado))
			pedidoHijo.addToAprobaciones(aprobacion)

			log.info "aprobacion: ${aprobacion}"

			// Copiado de PedidoService por un tema de arquitectura
			configuracionMailService.definirObservers(pedidoHijo)
			pedidoHijo.inicializarObservers()
			// Fin copiado de pedidoService

			pedidoHijo.addToLogModificaciones(new LogModificaciones(legajo: null, fase: faseHijo, fechaDesde: new Date(), fechaHasta: new Date(), descripcionEvento: "Creaci\u00F3n impacto hijo " + pedidoHijo.tipoPlanificacion, tipoEvento: LogModificaciones.CREACION, rol: null))
			log.info "agrego log"

			// Arreglo NDE - FED
			pedidoHijo.doGuardar(usuarioLogueado)
			// Antes notificaba y al notificar el padre duplicaba novedades
			// pedidoHijo.guardar(usuarioLogueado)
		}
	}

	public getJustificacionGU(){
		return justificacion(faseValidacionPedido())
	}

	public getJustificacionIU(){
		return justificacion(faseAprobacionPedido())
	}

	public getJustificacionFGD(){
		return justificacion(faseEvaluacionCambio())
	}

	public getJustificacionCC(){
		return justificacion(faseEspecificacionCambio())
	}

	public getJustificacionAC(){
		return justificacion(faseAprobacionCambio())
	}

	public ultimaJustificacionRP(){
		def ultimaAprobacionValidacionPedido = ultimaAprobacion(faseValidacionPedido())
		def ultimaAprobacionAprobacionPedido = ultimaAprobacion(faseAprobacionPedido())


		if (ultimaAprobacionValidacionPedido?.fecha > ultimaAprobacionAprobacionPedido?.fecha) {
			return ultimaAprobacionValidacionPedido?.justificacion
		} else {
			return ultimaAprobacionAprobacionPedido?.justificacion
		}
	}

	public ultimaJustificacionAP(){
		def ultimaAprobacionEvaluacionCambio = ultimaAprobacion(faseEvaluacionCambio())
		def ultimaAprobacionEspecificacionCambio = ultimaAprobacion(faseEspecificacionCambio())


		if (ultimaAprobacionEvaluacionCambio?.fecha > ultimaAprobacionEspecificacionCambio?.fecha) {
			return ultimaAprobacionEvaluacionCambio?.justificacion
		} else {
			return ultimaAprobacionEspecificacionCambio?.justificacion
		}
	}

	public boolean retroceder(aprobacionUI, usuarioLogueado) {
		def fase = faseActual
		def usuarioAprobador
		def grupoAprobador
		def descripcionEvento

		if (!fase) {
			fase = faseRegistracionPedido()
		}
		def aprobacionesPendientesFaseActual = aprobaciones.findAll {aprobacion  -> aprobacion.pendienteParaFase(fase) }

		log.info "Retrocediendo desde la fase " + fase

		//def usuarioResponsable = getUsuarioResponsable(fase, usuarioLogueado)
		// FED - vuelvo a dejar el usuario logueado
		def usuarioResponsable = usuarioLogueado

		// Ahora sabemos cu\u00E1l es la fase a la que retrocede al denegar
		faseActual = fase.faseDenegacion

		if (fase.equals(faseValidacionPedido())) {
			fechaAprobacionPedidoGU = null
			descripcionEvento = "Denegaci\u00F3n Gerente Usuario. "
		}
		if (fase.equals(faseAprobacionPedido())) {
			fechaValidacionIU = null
			grupoAprobador = null
			descripcionEvento = "Denegaci\u00F3n Interlocutor Usuario. "
		}
		if (fase.equals(faseEvaluacionCambio())) {
			descripcionEvento = "Denegaci\u00F3n Evaluaci\u00F3n Cambio Gesti\u00F3n Demanda. "
			deshacerEvaluacionCambio()
		}
		if (fase.equals(faseEspecificacionCambio())) {
			fechaAprobacionEspecificacionPedidoCC = null
			fechaEvaluacionPedidoFGD = null
			deshacerEvaluacionCambio()
			descripcionEvento = "Denegaci\u00F3n Evaluaci\u00F3n Cambio Coordinador. "
		}
		if (fase.equals(faseAprobacionCambio())) {
			fechaAprobacionEspecificacionPedidoCC = null
			descripcionEvento = "Denegaci\u00F3n Aprobaci\u00F3n Cambio. "
		}

		if (fase.equals(faseValidacionImpacto())) {
			fechaValidacionImpacto = null
			fechaAprobacionEspecificacionImpacto = null
			legajoUsuarioAprobadorEconomico = null
			descripcionEvento = "Denegaci\u00F3n Validaci\u00F3n Impacto. "
		}

		if (fase.equals(faseAprobacionImpacto())) {
			fechaAprobacionEconomica = null
			// no va !!! fechaAprobacionEspecificacionImpacto = null
			// FED: no vamos a borrarle el aprobador de impacto porque el que deniega
			// es justamente el AI
			// legajoUsuarioAprobadorEconomico = null
			descripcionEvento = "Denegaci\u00F3n Especificaci\u00F3n Pedido. "
		}

		if (aprobacionUI.justificacion) {
			descripcionEvento += "Justificaci\u00F3n: " + aprobacionUI.justificacion + ". "
		}

		usuarioAprobador = getUsuarioResponsable(faseActual, usuarioLogueado)

		aprobacionesPendientesFaseActual.each { aprobacion ->
			aprobacion.rechazar()
			if (usuarioLogueado.equalsIgnoreCase(aprobacion?.usuario)) {
				aprobacion.justificacion = aprobacionUI?.justificacion
			}
		}
		addToLogModificaciones(new LogModificaciones(legajo: usuarioResponsable, fase: fase, fechaHasta: new Date(), fechaDesde: fechaDesdePara(fase), descripcionEvento: descripcionEvento, tipoEvento: tipoEventoDenegacion(), rol: getRol(fase, usuarioResponsable)))

		def aprobacion = new Aprobacion(fase: faseActual, grupo: grupoAprobador, usuario: usuarioAprobador, fecha: new Date(), estado: Aprobacion.PENDIENTE, rol: getRol(faseActual, usuarioAprobador))
		addToAprobaciones(aprobacion)

		fechaUltimaModificacion = new Date()

		this.notificarCambioEstado()
	}

	def getRol(fase, usuarioLogueado) {
		if (fase.equals(faseRegistracionPedido())) {
			return RolAplicacion.USUARIO_FINAL
		}
		if (fase.equals(faseValidacionPedido())) {
			return RolAplicacion.GERENTE_USUARIO
		}
		if (fase.equals(faseAprobacionPedido())) {
			return RolAplicacion.INTERLOCUTOR_USUARIO
		}
		if (fase.equals(faseEvaluacionCambio())) {
			return RolAplicacion.GESTION_DEMANDA
		}
		if (fase.equals(faseEspecificacionCambio())) {
			return RolAplicacion.COORDINADOR_CAMBIO
		}
		if (fase.equals(faseEspecificacionImpacto())) {
			return RolAplicacion.RESPONSABLE_SWF
		}
		if (fase.equals(faseAprobacionCambio())) {
			if (legajoInterlocutorUsuario.equalsIgnoreCase(usuarioLogueado)) {
				return RolAplicacion.INTERLOCUTOR_USUARIO
			} else {
				return RolAplicacion.AREA_SOPORTE
			}
		}
		if (fase.equals(faseConsolidacionImpacto())) {
			return RolAplicacion.COORDINADOR_CAMBIO
		}
		if (fase.equals(faseValidacionImpacto())) {
			return RolAplicacion.INTERLOCUTOR_USUARIO
		}
		if (fase.equals(faseAprobacionImpacto())) {
			return RolAplicacion.APROBADOR_IMPACTO
		}
		if (fase.equals(faseFinalizacion())) {
			return RolAplicacion.INTERLOCUTOR_USUARIO
		}
		return null
	}

	def getRolUsuario(fase, usuarioLogueado) {
		if (fase.equals(faseRegistracionPedido())) {
			if (usuarioLogueado?.equalsIgnoreCase(legajoUsuarioCreador)) {
				return RolAplicacion.USUARIO_FINAL
			}
			if (usuarioLogueado?.equalsIgnoreCase(legajoGerenteUsuario)) {
				return RolAplicacion.GERENTE_USUARIO
			}
			if (usuarioLogueado?.equalsIgnoreCase(legajoInterlocutorUsuario)) {
				return RolAplicacion.INTERLOCUTOR_USUARIO
			}
		}
		if (fase.equals(faseValidacionPedido())) {
			if (usuarioLogueado?.equalsIgnoreCase(legajoGerenteUsuario)) {
				return RolAplicacion.GERENTE_USUARIO
			}
			if (usuarioLogueado?.equalsIgnoreCase(legajoInterlocutorUsuario)) {
				return RolAplicacion.INTERLOCUTOR_USUARIO
			}
		}
		return getRol(fase, usuarioLogueado)
	}

	def getUsuarioResponsable(fase, usuarioLogueado) {
		def usuarioResponsableDefault = null

		if (fase.equals(faseRegistracionPedido())) {
			usuarioResponsableDefault = legajoUsuarioCreador
		}
		if (fase.equals(faseValidacionPedido())) {
			usuarioResponsableDefault = legajoGerenteUsuario
		}
		if (fase.equals(faseAprobacionPedido())) {
			usuarioResponsableDefault = legajoInterlocutorUsuario
		}
		if (fase.equals(faseEvaluacionCambio())) {
			usuarioResponsableDefault = legajoUsuarioGestionDemanda
		}
		if (fase.equals(faseEspecificacionCambio())) {
			usuarioResponsableDefault = legajoCoordinadorCambio
		}
		if (fase.equals(faseAprobacionCambio())) {
			usuarioResponsableDefault = legajoInterlocutorUsuario
		}
		if (fase.equals(faseConsolidacionImpacto())) {
			usuarioResponsableDefault = legajoCoordinadorCambio
		}
		if (fase.equals(faseValidacionImpacto())) {
			usuarioResponsableDefault = legajoInterlocutorUsuario
		}
		if (fase.equals(faseAprobacionImpacto())) {
			usuarioResponsableDefault = legajoUsuarioAprobadorEconomico
		}
		if (fase.equals(faseConstruccionCambio())) {
			usuarioResponsableDefault = legajoCoordinadorCambio
		}
		if (fase.equals(faseFinalizacion())) {
			usuarioResponsableDefault = legajoInterlocutorUsuario
		}
		return usuarioResponsableDefault
	}

	def getLegajo(String codigoRol) {
		if (codigoRol.equals(RolAplicacion.USUARIO_FINAL)) {
			return legajoUsuarioCreador
		}
		if (codigoRol.equals(RolAplicacion.GERENTE_USUARIO)) {
			return legajoGerenteUsuario
		}
		if (codigoRol.equals(RolAplicacion.INTERLOCUTOR_USUARIO)) {
			return legajoInterlocutorUsuario
		}
		if (codigoRol.equals(RolAplicacion.GESTION_DEMANDA)) {
			return legajoUsuarioGestionDemanda
		}
		if (codigoRol.equals(RolAplicacion.COORDINADOR_CAMBIO)) {
			return legajoCoordinadorCambio
		}
		if (codigoRol.equals(RolAplicacion.APROBADOR_IMPACTO)) {
			return legajoUsuarioAprobadorEconomico
		}
		return null
	}

	def boolean huboDenegacionGerenteUsuario() {
		def huboDenegacion = false
		def aprobacion = aprobacionGerenteUsuario()
		huboDenegacion = aprobacion?.estaDenegado()
		return huboDenegacion
	}

	def boolean huboDenegacionInterlocutorUsuario() {
		def huboDenegacion = false
		def aprobacion = aprobacionInterlocutorUsuario()
		huboDenegacion = aprobacion?.estaDenegado()
		return huboDenegacion
	}

	def aprobacionInterlocutorUsuario() {
		def aprobacionesIU = aprobaciones.findAll { aprobacion -> aprobacion.matchea(legajoInterlocutorUsuario, faseValidacionPedido())}.sort { it?.fecha }
		if (aprobacionesIU.isEmpty()) {
			return null
		} else {
			return aprobacionesIU.last()
		}
	}

	def aprobacionGerenteUsuario() {
		def aprobacionesGU = aprobaciones.findAll { aprobacion -> aprobacion.matchea(legajoGerenteUsuario, faseValidacionPedido()) }.sort { it?.fecha }
		if (aprobacionesGU.isEmpty()) {
			return null
		} else {
			return aprobacionesGU.last()
		}
	}

	def getUsuariosFinales() {
		if (legajoUsuarioCreador) {
			return [legajoUsuarioCreador]
		} else {
			return []
		}
	}

	def getGerentesUsuarios() {
		if (legajoGerenteUsuario) {
			return [legajoGerenteUsuario]
		} else {
			return []
		}
	}

	def getInterlocutoresUsuarios() {
		if (legajoInterlocutorUsuario) {
			return [legajoInterlocutorUsuario]
		} else {
			return []
		}
	}

	def getUsuariosGestionDemanda() {
		if (legajoUsuarioGestionDemanda) {
			return [legajoUsuarioGestionDemanda]
		} else {
			return []
		}
	}

	def getUsuariosCoordinadoresCambio() {
		if (legajoCoordinadorCambio) {
			return [legajoCoordinadorCambio]
		} else {
			return []
		}
	}

	def getUsuariosAreasSoporte() {
		def usuarios = getUsuariosQueCumplen(getPedidosHijosActivos(), { pedidoHijo -> pedidoHijo.tieneAreaSoporte() })
		if (usuarios.isEmpty()) {
			usuarios = getUsuariosQueCumplen(obtenerUltimaEspecificacion().areasImpactadas)
		}
		return usuarios
	}

	def getUsuariosResponsablesSWF() {
		def usuarios = getUsuariosQueCumplen(getPedidosHijosActivos(), { pedidoHijo -> pedidoHijo.tieneSoftwareFactory() })
		if (usuarios.isEmpty()) {
			usuarios = getUsuariosQueCumplen(obtenerUltimaEspecificacion().sistemasImpactados, { it.tieneSoftwareFactory() })
		}
		return usuarios
	}

	def getUsuariosSoftwareFactory() {
		def swfs = getPedidosHijosActivos().findAll({ pedidoHijo -> pedidoHijo.tieneSoftwareFactory() }).inject([], { usuarios, pedidoHijo -> usuarios + pedidoHijo.softwareFactory })
		if (swfs.isEmpty()) {
			swfs = obtenerUltimaEspecificacion().sistemasImpactados.findAll({ pedidoHijo -> pedidoHijo.tieneSoftwareFactory() }).inject([], { usuarios, sistemaImpactado -> usuarios + sistemaImpactado.softwareFactory })
		}
		return getUsuariosQueCumplen(swfs)
	}

	def getUsuariosAdministradoresFuncionales() {
		def usuarios = getUsuariosQueCumplen(getPedidosHijosActivos(), { pedidoHijo -> pedidoHijo.tieneAdministradorFuncional() })
		if (usuarios.isEmpty()) {
			usuarios = getUsuariosQueCumplen(obtenerUltimaEspecificacion().sistemasImpactados, { it.tieneAdministradorFuncional() })
		}
		return usuarios
	}

	def getUsuariosGestionOperativa() {
		return getPedidosHijosActivos().inject([], { usuarios, pedidoHijo -> usuarios + pedidoHijo.usuariosGestionOperativa})
	}

	def getUsuariosQueCumplen(impactos) {
		return getUsuariosQueCumplen(impactos, { it -> true })
	}

	def getUsuariosQueCumplen(impactos, condicion) {
		return impactos.findAll(condicion).collect({ impacto ->
			if (impacto.legajoUsuarioResponsable) {
				impacto.legajoUsuarioResponsable
			} else {
				new UmeService().getUsuariosGrupoLDAP(impacto.grupoResponsable, null).collect { it.legajo }
			}
		}).flatten()
	}

	def getAreasSoporteImpactadas() {
		return getUsuariosANotificar(obtenerUltimaEspecificacion()?.areasImpactadas)
	}

	def getAreasSoporteNoImpactadas() {
		def todasLasAreasImpactables = AreaSoporte.list()
		return getUsuariosANotificar(todasLasAreasImpactables - obtenerUltimaEspecificacion()?.areasImpactadas)
	}

	def getAdministradoresFuncionalesImpactados() {
		return getUsuariosANotificar(obtenerUltimaEspecificacion()?.sistemasImpactados.sistema)
	}

	def getUsuariosAprobadoresImpacto() {
		if (legajoUsuarioAprobadorEconomico) {
			return [
				legajoUsuarioAprobadorEconomico
			]
		} else {
			return []
		}
	}

	@Deprecated
	def puedeEditarIngreso(usuarioLogueado){
		if (isCancelado()) {
			return false
		}
		if(this.faseActual?.equals(faseRegistracionPedido()) && this.legajoUsuarioCreador?.equalsIgnoreCase(usuarioLogueado)){
			return true
		}
		if(this.faseActual?.equals(faseValidacionPedido()) && this.legajoGerenteUsuario?.equalsIgnoreCase(usuarioLogueado)){
			return true
		}
		if(this.faseActual?.equals(faseAprobacionPedido()) && this.legajoInterlocutorUsuario?.equalsIgnoreCase(usuarioLogueado)){
			return true
		}
		// TODO: A partir de ahora si quieren cambiar tienen que denegar
		//		if(this.faseActual?.equals(faseEvaluacionCambio()) && this.legajoUsuarioGestionDemanda?.equalsIgnoreCase(usuarioLogueado)){
		//			return false
		//		}
		//		if(this.faseActual?.equals(faseEspecificacionCambio()) && this.legajoCoordinadorCambio?.equalsIgnoreCase(usuarioLogueado)){
		//			return false
		//		}
		return false
	}

	@Deprecated
	def puedeEditarAprobacion(usuarioLogueado){
		if (isCancelado()) {
			return false
		}
		if(this.faseActual?.equals(FaseMultiton.getFase(FaseMultiton.VALIDACION_PEDIDO)) && this.legajoGerenteUsuario?.equalsIgnoreCase(usuarioLogueado)){
			return true
		}
		if(this.faseActual?.equals(FaseMultiton.getFase(FaseMultiton.APROBACION_PEDIDO)) && this.legajoInterlocutorUsuario?.equalsIgnoreCase(usuarioLogueado)){
			return true
		}
		return false
	}

	@Deprecated
	def puedeEditarValidacion(usuarioLogueado){
		if (isCancelado()) {
			return false
		}
		if(this.faseActual?.equals(FaseMultiton.getFase(FaseMultiton.APROBACION_PEDIDO)) && this.legajoInterlocutorUsuario?.equalsIgnoreCase(usuarioLogueado)){
			return true
		}
		return false
	}

	@Deprecated
	def puedeEditarEvaluacion(usuarioLogueado){
		if (isCancelado()) {
			return false
		}
		if (!this.faseActual?.equals(faseEvaluacionCambio())) {
			return false
		}

		return usuarioHabilitado(fase, usuarioLogueado)
	}


	@Deprecated
	def puedeEditarEspecificacionCambio(usuarioLogueado) {
		if (isCancelado()) {
			return false
		}
		if (this.faseActual?.equals(faseEspecificacionCambio()) && this.legajoCoordinadorCambio?.equalsIgnoreCase(usuarioLogueado)) {
			return true
		}
		return false
	}

	def puedeEditarAprobacionCambio(usuarioLogueado) {
		if (isCancelado()) {
			return false
		}
		if (this.faseActual?.equals(faseAprobacionCambio()) && this.tieneAprobacionesPendientes(usuarioLogueado)) {
			return true
		}
		return false
	}

	@Deprecated
	def puedeEditarConsolidacionImpacto(usuarioLogueado) {
		if (isCancelado()) {
			return false
		}
		if (this.faseActual?.equals(faseConsolidacionImpacto())) {
			return this.legajoCoordinadorCambio?.equalsIgnoreCase(usuarioLogueado)
		}
		return false
	}

	/**
	 * No se depreca este método dado que tiene condiciones particulares
	 * @param usuarioLogueado
	 * @return
	 */
	def puedeEditarAprobacionPAU(usuarioLogueado) {
		if (isCancelado()) {
			return false
		}
		if (this.faseActual?.equals(faseConstruccionCambio())) {
			return hijosDisponiblesParaAprobacionPAU() && legajoInterlocutorUsuario.equalsIgnoreCase(usuarioLogueado)
		}
		return false
	}

	@Deprecated
	def puedeEditarAprobacionImplantacion(usuarioLogueado) {
		return false
	}

	@Deprecated
	def usuarioPerteneceAGrupoDeAprobacion(usuarioLogueado, fase) {
		return false
	}

	/**
	 * No se depreca este método dado que tiene condiciones particulares
	 * @param usuarioLogueado
	 * @return
	 */
	def puedeEditarEspecificacionImpacto(usuarioLogueado) {
		if (isCancelado()) {
			return false
		}
		if (this.faseActual?.equals(faseConsolidacionImpacto())) {
			return usuarioEsResponsableDeAlgunHijo(usuarioLogueado)
		}
		return false
	}

	@Deprecated
	def puedeEditarValidacionImpacto(usuarioLogueado) {
		if (isCancelado()) {
			return false
		}
		if (this.faseActual?.equals(faseValidacionImpacto())) {
			return usuarioHabilitado(this.faseActual, usuarioLogueado) && !aprobacionesPendientesPara(usuarioLogueado).isEmpty()
		}
		return false
	}

	@Deprecated
	def puedeEditarAprobacionImpacto(usuarioLogueado) {
		if (isCancelado()) {
			return false
		}
		if (this.faseActual?.equals(faseAprobacionImpacto())) {
			return legajoUsuarioAprobadorEconomico?.equalsIgnoreCase(usuarioLogueado)
		}
		return false
	}

	def usuarioEsUsuarioDeAprobacion(usuarioLogueado, fase) {
		return false
	}

	def hijosCerraronEspecificacion() {
		return !pedidosHijos.any { pedidoHijo -> !pedidoHijo.cerroEspecificacion() }
	}

	def hijosDisponiblesParaAprobacionPAU() {
		return !pedidosHijosQueConsolidanPAU().any { pedidoHijo -> !pedidoHijo.estaEnFase(FaseMultiton.APROBACION_PAU_HIJO) }
	}

	def hijosDisponiblesParaAprobacionImplantacion() {
		return !pedidosHijosQueConsolidanPAU().any { pedidoHijo -> !pedidoHijo.estaEnFase(FaseMultiton.APROBACION_IMPLANTACION_HIJO) }
	}

	def usuarioEsResponsableDeAlgunHijo(usuarioLogueado) {
		def umeService = new UmeService()
		pedidosHijos.each { pedidoHijo ->
			if (pedidoHijo.legajoUsuarioResponsable.equals(usuarioLogueado) && umeService.usuarioPerteneceGrupo(pedidoHijo.grupoResponsable, usuarioLogueado)) {
				return true
			}
		}
		return false
	}

	def agregarEspecificacion(ecs){
		addToEspecificaciones(ecs)
	}

	ECS obtenerUltimaEspecificacion(){
		if (especificaciones.isEmpty()) {
			def ecs = new ECS(versionECS: 1)
			addToEspecificaciones(ecs)
			// Si falla, podemos copiar el default de WorkflowController.llenarDatosPedido
		}
		return especificaciones.last()
	}

	def getRoles(String legajo) {
		return getRoles(legajo, true)
	}

	def getRoles(String legajo, boolean conHijos) {
		def result = []
		if(legajo){
			if (legajo.equalsIgnoreCase(this.legajoUsuarioCreador)) {
				result.add(RolAplicacion.USUARIO_FINAL)
			}
			if (legajo.equalsIgnoreCase(this.legajoGerenteUsuario)) {
				result.add(RolAplicacion.GERENTE_USUARIO)
			}
			if (legajo.equalsIgnoreCase(this.legajoInterlocutorUsuario)) {
				result.add(RolAplicacion.INTERLOCUTOR_USUARIO)
			}
			if (puedeReasignarEvaluacionPedido(legajo, false)){
				result.add(RolAplicacion.GESTION_DEMANDA)
			}
			if (legajo.equalsIgnoreCase(this.legajoCoordinadorCambio)) {
				result.add(RolAplicacion.COORDINADOR_CAMBIO)
			}
		}
		if (conHijos) {
			result.addAll(pedidosHijos.collect { pedidoHijo -> pedidoHijo.getRoles(legajo) })
		}
		return result
	}

	def areasImpactadas() {
		return obtenerUltimaEspecificacion().areasImpactadas
	}

	def getTipoPlanificacion() {
		return "Consolidado"
	}

	def getGrupoResponsable() {
		return Constantes.instance.parametros.grupoLDAPCoordinadorPedido
	}

	/**
	 * Comportamiento polim\u00F3rfico para la pantalla de Especificaci\u00F3n Impacto
	 */
	def planificaActividadesSistema() {
		return true
	}
	
	boolean requiereCargarDetalles() {
		false
	}

	def planificaTareasSoporte() {
		return true
	}

	def mostrarSolapaPlanificacion() {
		return pedidosHijos.any { pedidoHijo -> pedidoHijo.mostrarSolapaPlanificacion() }
	}

	def mostrarSolapaEstrategiaPrueba() {
		return pedidosHijos.any { pedidoHijo -> pedidoHijo.mostrarSolapaEstrategiaPrueba() }
	}

	def mostrarSolapaDE() {
		return pedidosHijos.any { pedidoHijo -> pedidoHijo.mostrarSolapaDE() }
	}

	/**
	 * Devuelvo el conjunto de impactos posibles, esto incluye
	 * - pedidos hijos (de sistema/ área de soporte)
	 * - consolidado
	 * - adicional de coordinaci\u00F3n
	 * @return
	 */
	def getImpactos() {
		def impactos = []
		if (this.tieneUnicoImpacto()) {
			// No hacemos aparecer al padre
		} else {
			impactos.addAll(new Impacto(Impacto.IMPACTO_CONSOLIDADO, this))
			impactos.addAll(new Impacto(Impacto.IMPACTO_COORDINACION, this))
		}
		impactos.addAll(pedidosHijos.collect { pedidoHijo -> new Impacto(Impacto.IMPACTO_HIJO, pedidoHijo) })
		return impactos
	}

	def tieneUnicoImpacto() {
		return coordinadorEsResponsable() && !planificacionActual
	}

	boolean coordinadorEsResponsable(){
		return pedidosHijos.size() == 1 && pedidosHijos.any { pedidoHijo -> this.legajoCoordinadorCambio.equalsIgnoreCase(pedidoHijo.legajoUsuarioResponsable) }
	}
	
	def getSistema() {
		return null
	}

	def tipoImpacto() {
		return null
	}

	def permiteIngresarOtrosCostos() {
		return true
	}

	def ingresaTicketSolman() {
		return false
	}

	def costoTotalCambioConsolidado() {
		def totalPadre = 0
		if (getPlanificacionActual()?.estaCumplido()) {
			totalPadre = this.costoTotalCambio()
		}
		def totalHijos = pedidosHijos.inject (0) { acum, pedidoHijo -> acum + pedidoHijo?.costoTotalCambioConsolidado() }
		return totalPadre + totalHijos
	}

	def cargaRelease() {
		return false
	}

	def getPedidoHijos(){
		return pedidosHijos
	}

	def getPedidosHijosActivos(){
		return pedidosHijos.findAll { pedidoHijo -> !pedidoHijo.denegoEspecificacion() }
	}

	def getPedidosHijosEspecificados(){
		return pedidosHijos.findAll { pedidoHijo -> !pedidoHijo.denegoEspecificacion() && !pedidoHijo.tieneEspecificacionPendiente()}
	}

	def esHijo() {
		return false
	}

	def getParent() {
		return this
	}

	def pedidosHijosConAprobacionesPendientes(usuarioLogueado){
		pedidosHijos.findAll { pedidoHijo -> pedidoHijo?.tieneAprobacionesPendientes(usuarioLogueado) }
	}

	def pedidosHijosConAprobacionesPendientes(usuarioLogueado, tipoConsulta){
		pedidosHijos.findAll { pedidoHijo -> pedidoHijo?.tieneAprobacionesPendientes(usuarioLogueado, tipoConsulta) }
	}

	def pedidosHijosEnSeguimiento(usuarioLogueado) {
		pedidosHijos.findAll { pedidoHijo -> pedidoHijo?.enSeguimiento(usuarioLogueado) }
	}

	def actividadesPendientesPara(usuarioLogueado) {
		pedidosHijos.findAll { pedidoHijo -> pedidoHijo?.tieneActividadesPendientes(usuarioLogueado) }
	}

	def definirEstrategiaPUPIPAU(estrategiaPrueba, ecs, mapaTiposPrueba) {
		estrategiaPrueba.addToDetalles(new DetalleEstrategiaPrueba(tipoPrueba: TipoPrueba.pruebaUnitaria(), seRealiza: null, seConsolida: null, justificacionNoRealizacion: null))
		estrategiaPrueba.addToDetalles(new DetalleEstrategiaPrueba(tipoPrueba: TipoPrueba.pruebaIntegracion(), seRealiza: ecs.realizaPI, seConsolida: ecs.PIintegrada, justificacionNoRealizacion: ecs.justificacionPI))
		estrategiaPrueba.addToDetalles(new DetalleEstrategiaPrueba(tipoPrueba: TipoPrueba.pruebaPAU(), seRealiza: ecs.realizaPAU, seConsolida: ecs.PAUintegrada, justificacionNoRealizacion: ecs.justificacionPAU))
	}

	def sistema() {
		return null
	}

	public boolean tieneAprobacionOmitida() {
		if (faseActual.equals(faseValidacionPedido())) {
			return legajoUsuarioCreador.equalsIgnoreCase(legajoGerenteUsuario)
		}
		if (faseActual.equals(faseAprobacionPedido())) {
			return legajoInterlocutorUsuario?.equalsIgnoreCase(legajoGerenteUsuario) || legajoUsuarioCreador.equalsIgnoreCase(legajoInterlocutorUsuario)
		}
		if (faseActual.equals(faseAprobacionImpacto())) {
			return legajoInterlocutorUsuario?.equalsIgnoreCase(legajoUsuarioAprobadorEconomico)
		}
		return false
	}

	/**
	 *
	 * @return
	 * La sumatoria del monto de horas planificadas del pedido + la de sus hijos
	 */
	def totalMontoHorasPlanificadas(){
		totalPlanificado {it?.totalMontoHoras()}
	}

	/**
	 *
	 * @return
	 * La sumatoria de las horas planificadas del pedido + la de sus hijos
	 */
	def totalHorasPlanificadas(){
		totalPlanificado {it?.totalHoras()}
	}

	def totalGeneral() {
		totalPlanificado {it?.totalMontoGeneral()}
	}

	private def totalPlanificado(def aSumar){
		def total = pedidosHijos?.collect{it.planificacionActual? aSumar(it.planificacionActual): 0}
		def totalHijos = total?.sum() ?: 0
		return NumberUtil.orZero(aSumar(planificacionActual)) + NumberUtil.orZero(totalHijos)
	}

	def habilitaAcciones(usuarioLogueado) {
		return this.legajoCoordinadorCambio?.equalsIgnoreCase(usuarioLogueado)
	}

	def inicializaAsignatario(usuarioLogueado){
		return this.legajoCoordinadorCambio == null
	}

	def pisarHijoConPadre(pedidoHijo) {
		//TODO: No hay que pisar la planificaci\u00F3n del padre, de esa manera se duplicarían las horas
		//		def planificacionHijo = pedidoHijo.getPlanificacionActual()
		//		if (planificacionHijo) {
		//			def ultimaPlanificacion = getPlanificacionActual()
		//			if (ultimaPlanificacion) {
		//				ultimaPlanificacion = planificacionHijo
		//			} else {
		//				addToPlanificaciones(planificacionHijo)
		//			}
		//		}
		def estrategiaHijo = pedidoHijo.getEstrategiaPruebaActual()
		if (estrategiaHijo) {
			def estrategiaPrueba = getEstrategiaPruebaActual()
			if (estrategiaPrueba) {
				estrategiaPrueba = estrategiaHijo
			} else {
				addToEstrategiasPrueba(estrategiaHijo)
			}
		}
		def disenioHijo = pedidoHijo.getDisenioExternoActual()
		if (disenioHijo) {
			def disenio = getDisenioExternoActual()
			if (disenio) {
				disenio = disenioHijo
			} else {
				addToDiseniosExternos(disenioHijo)
			}
		}
	}

	def softwareFactories() {
		return pedidosHijos.inject (new HashSet()) { swfs, pedidoHijo -> swfs + pedidoHijo.softwareFactories() }
	}


	def generarTablaOtrosCostos(){

		def tablaOtrosCostos = []
		def todosLosPedidos = []
		todosLosPedidos += pedidosHijos
		todosLosPedidos << this

		todosLosPedidos.each { pedido ->
			def otrosCostosList = pedido.planificacionActual?.otrosCostos
			if (pedido.cerroEspecificacion() && otrosCostosList){

				otrosCostosList.each{ otroCostoItem ->

					String descripcion = otroCostoItem.tipoCosto.toString()
					def otroCostoDTO = tablaOtrosCostos.find { descripcion ==  it?.descripcion}

					if (!otroCostoDTO){
						otroCostoDTO = [descripcion: descripcion, total: 0, items: []]
						tablaOtrosCostos << otroCostoDTO
					}
					def detalle
					if(otroCostoItem?.detalle){
						detalle = otroCostoItem.detalle
					}else{
						detalle = ""
					}

					otroCostoDTO.total += otroCostoItem.costo
					otroCostoDTO.items << [descripcion: pedido.toString(), detalle: detalle, costo: otroCostoItem.costo]
				}
			}
		}
		tablaOtrosCostos
	}

	def generarTablaAreaSoporte(){

		def tablaAreaSoporte = []
		def todosLosPedidos = []
		todosLosPedidos += pedidosHijos
		todosLosPedidos << this

		todosLosPedidos.each{ pedido ->
			def detallesList = pedido.planificacionActual?.detalles.findAll{ it.esDeAreaDeSoporte() }
			if (detallesList){
				detallesList.each{ detalle ->

					String descripcion = detalle.tareaSoporte.areaSoporte ? detalle.tareaSoporte.areaSoporte.toString() : "Administrador Funcional"
					def detalleDTO = tablaAreaSoporte.find { descripcion == it?.descripcion }

					if (!detalleDTO){
						detalleDTO = [descripcion: descripcion, tarea: detalle.tareaSoporte ? "" : "Sin Actividad", fechaDesde: detalle.fechaDesde, fechaHasta: detalle.fechaHasta ,total: 0, totalMontoHoras:0, items: []]
						tablaAreaSoporte << detalleDTO
					}

					if (detalle.tareaSoporte){
						def allFechas =[]
						if(!detalleDTO.items.isEmpty())
							allFechas += detalleDTO.items
						allFechas << detalle

						detalleDTO.fechaDesde = allFechas.fechaDesde.min()
						detalleDTO.fechaHasta = allFechas.fechaHasta.max()
						detalleDTO.total += detalle.totalHoras()
						detalleDTO.totalMontoHoras += detalle.totalMontoHoras()
						//otroCostoDTO.items << [descripcion: pedido.toString(), detalle: otroCostoItem.detalle, costo: otroCostoItem.costo]
						detalleDTO.items << [areaSoporte: pedido.toString(), tarea: detalle.tareaSoporte, fechaDesde: detalle.fechaDesde, fechaHasta: detalle.fechaHasta, totalHoras: detalle.totalHoras(), totalMontoHoras: detalle.totalMontoHoras()]
					}
				}
			}

		}
		tablaAreaSoporte
	}

	def pedidoDeEmergencia() {
		if (tipoGestion) {
			return tipoGestion.descripcion.equalsIgnoreCase(TipoGestion.GESTION_EMERGENCIA)
		} else {
			return false
		}
	}

	def deshacerEvaluacionCambio() {
		fechaEvaluacionPedidoFGD = null
		grupoGestionDemanda = Constantes.instance.parametros.grupoLDAPFuncionGestionDemandaGral
		legajoUsuarioGestionDemanda = null
		legajoCoordinadorCambio = null
	}

	public Set getUsuarios() {
		Set usuarios = new HashSet()
		if (legajoUsuarioCreador) {
			usuarios.add(legajoUsuarioCreador)
		}
		if (legajoGerenteUsuario) {
			usuarios.add(legajoGerenteUsuario)
		}
		if (legajoInterlocutorUsuario) {
			usuarios.add(legajoInterlocutorUsuario)
		}
		if (legajoUsuarioGestionDemanda) {
			usuarios.add(legajoUsuarioGestionDemanda)
		}
		if (legajoCoordinadorCambio) {
			usuarios.add(legajoCoordinadorCambio)
		}
		if (legajoUsuarioAprobadorEconomico) {
			usuarios.add(legajoUsuarioAprobadorEconomico)
		}
		return usuarios
	}

	boolean consolidaPAU() {
		// opci\u00F3n 1: el padre consolida si alguno de los hijos consolida (??? WTF)
		// return pedidosHijos.any { pedidoHijo -> pedidoHijo.consolidaPAU() }
		// opci\u00F3n 2: el padre consolida si así lo dice su estrategia
		return this.estrategiaPruebaActual?.consolidaPAU()
	}

	def pedidosHijosQueConsolidanPAU() {
		return pedidosHijos.findAll { pedidoHijo -> pedidoHijo.consolidaPAU() && pedidoHijo.apruebaPAU() }
	}

	def tienePedidosPendientesPAUConsolidada() {
		return pedidosHijosQueConsolidanPAU().any { pedidoHijo -> !pedidoHijo.faseActual.equals(faseAprobacionPAUHijo()) }
	}

	def pedidosHijosQueRealizanPAU() {
		return pedidosHijos.findAll { pedidoHijo -> pedidoHijo.realizaPAU() }
	}

	def pedidosHijosQueNoConsolidanPAU() {
		return pedidosHijosQueRealizanPAU() - pedidosHijosQueConsolidanPAU()
	}

	boolean aproboAprobacionPAU(){
		def pedidosQueNoAprobaron = pedidosHijosQueConsolidanPAU().findAll { pedidoHijo -> !pedidoHijo.aproboAprobacionPAU() }
		return pedidosQueNoAprobaron.isEmpty()
	}

	boolean aproboAprobacionImplantacion(){
		def pedidosQueNoAprobaron = pedidosHijosQueConsolidanPAU().findAll { pedidoHijo -> !pedidoHijo.aproboAprobacionImplantacion() }
		return pedidosQueNoAprobaron.isEmpty()
	}

	public boolean cumplioFecha(fase) {
		// Verifico el del primer hijo que consolide PAU, ya que si uno cumple, cumplen todos.
		return getPrimerHijoQueConsolidaPAU()?.cumplioFecha(fase)
	}

	public Date getFecha(fase) {
		// Obtengo la del primer hijo que consolida PAU, ya que si uno consolida, consolidan todos
		return getPrimerHijoQueConsolidaPAU()?.getFecha(fase)
	}

	public getJustificacionAP() {
		// Obtengo la justificacion del primer hijo encontrado
		return getPrimerHijoQueConsolidaPAU()?.getJustificacionAP()
	}

	public getJustificacionAI(unUsuario) {
		// Obtengo la justificacion del primer hijo encontrado
		return getPrimerHijoQueConsolidaPAU()?.getJustificacionAI(unUsuario)
	}

	public getJustificacionValidacionImpacto() {
		return justificacion(faseValidacionImpacto())
	}

	public def getPrimerHijoQueConsolidaPAU(){
		def hijosQueConsolidan = pedidosHijosQueConsolidanPAU()
		return !hijosQueConsolidan.isEmpty()?hijosQueConsolidan.first():null

	}

	public def primeraAprobacionDeUsuario(usuario){
		return null
	}

	def fasesRealizadas() {
		def fasesRealizadas = tipoGestion().fasesAnterioresA(faseActual)
		if(cerroUltimaFase()){
			fasesRealizadas.add(faseFinalizacion())
		}
		return fasesRealizadas
	}

	def fasesNoRealizadas() {
		def fasesNoRealizadas = [faseActual] 
		fasesNoRealizadas.add(tipoGestion().fasesPosterioresA(faseActual))
		if(!cerroUltimaFase()){
			fasesNoRealizadas.add(faseFinalizacion())
		}
		return fasesNoRealizadas
	}

	def especificacionDenegada() {
		def aprobacion = ultimaAprobacion(faseEspecificacionImpacto())
		if (aprobacion) {
			return aprobacion.estaDenegado()
		} else {
			return false
		}
	}

	def cerroEspecificacion() {
		return fechaAprobacionEspecificacionImpacto != null
	}

	def getFechaCierreEspecificacion() {
		return fechaAprobacionEspecificacionImpacto
	}

	def getFechaDeCierre() {
		return fechaCierre
	}

	boolean normaliza() {
		return false
	}

	def hijoDeAreaSoporte(area) {
		return pedidosHijos.find { pedidoHijo -> pedidoHijo.areaSoporte?.descripcion?.equals(area?.descripcion) }
	}

	def pedidosConAprobacionImplantacion() {
		return pedidosHijos.findAll { pedidoHijo -> pedidoHijo.aplicaAprobacionImplantacion() }
	}

	def puedeReasignarAprobadorImpacto(usuarioLogueado){
		if (isCancelado() || isSuspendido()) {
			return false
		}

		return((this.faseActual?.equals(FaseMultiton.getFase(FaseMultiton.VALIDACION_IMPACTO)) || this.faseActual?.equals(FaseMultiton.getFase(FaseMultiton.APROBACION_IMPACTO)))
		&& (this.legajoUsuarioAprobadorEconomico?.equalsIgnoreCase(usuarioLogueado) || this.legajoInterlocutorUsuario?.equalsIgnoreCase(usuarioLogueado)))
	}

	boolean apruebaPAU() {
		return false
	}

	def getAllMailsPendientes() {
		return pedidosHijos.inject(mailsPendientes, { total, pedidoHijo -> total + pedidoHijo.mailsPendientes})
	}

	def obtenerTipoPruebaECS(TipoPrueba tipoPrueba) {
		return obtenerUltimaEspecificacion().getTipoPrueba(tipoPrueba)
	}

	def estrategiaConsolidadaRealiza(TipoPrueba tipoPrueba) {
		return getPedidosHijosEspecificados()?.inject(null, { result, pedidoHijo ->
			DetalleEstrategiaPrueba detalle = pedidoHijo.estrategiaPruebaActual?.traerTipoPrueba(tipoPrueba)
			if (detalle) {
				result = detalle.consolidarRealiza(result)
				result
			} else {
				result
			}
		})
	}

	def estrategiaConsolidadaConsolida(TipoPrueba tipoPrueba) {
		return getPedidosHijosEspecificados()?.inject(null, { result, pedidoHijo ->
			DetalleEstrategiaPrueba detalle = pedidoHijo.estrategiaPruebaActual?.traerTipoPrueba(tipoPrueba)
			if (detalle) {
				detalle.consolidarConsolida(result)
			} else {
				result
			}
		})
	}

	boolean asociadoSistemas() {
		return false
	}

	def getAllAprobaciones() {
		return pedidosHijos.inject (aprobaciones, {total, pedidoHijo -> total + pedidoHijo.aprobaciones})
	}

	def notificarCambioEstado() {
		super.notificarCambioEstado()
		pedidosHijos.each { pedidoHijo ->
			pedidoHijo.notificarCambioEstado()
		}
	}

	def notificarGuardar(usuarioLogueado) {
		super.notificarGuardar(usuarioLogueado)
		pedidosHijos.each { pedidoHijo ->
			pedidoHijo.notificarGuardar(usuarioLogueado)
		}
	}

	def crearObserversIniciales() {
		//WORKAROUND -> Ver dsp porque en pedidoservice.avanzar, reinicia los observers.
			if (observers?.contains(new PedidoCambioDireccionObserver())){
				observers = observers + [
				             new PedidoAprobacionesCambioEstadoObserver(),
				             new PedidoModificacionObserver()
				             ]
			}else{
				observers = [
				             new PedidoAprobacionesCambioEstadoObserver(),
				             new PedidoModificacionObserver(),
				             new PedidoCambioDireccionObserver()
				             ]
			}
	}

	def inicializarObservers() {
		super.inicializarObservers()
		pedidosHijos.each { pedidoHijo ->
			pedidoHijo.inicializarObservers()
		}
	}

	def getRolDefault() {
		return null
	}

	def getDetallesConsolidados(detalles) {
		def result = detalles
		def tiposPruebaPadre = detalles.tipoPrueba
		def tiposPruebaDeHijos = pedidosHijos?.estrategiasPrueba?.detalles?.tipoPrueba.flatten()
		def tiposPruebaHijos = tiposPruebaDeHijos.inject(new HashSet(), {total, tipoPrueba -> total + tipoPrueba})
		def tiposPruebaHijosFiltrados = tiposPruebaHijos.findAll { tipoPruebaHijo -> !tiposPruebaPadre.contains(tipoPruebaHijo) }
		return result.sort{it.id} +  tiposPruebaHijosFiltrados.collect { tipoPrueba -> new DetalleEstrategiaPrueba(tipoPrueba: tipoPrueba)}.sort{it.id}
	}

	def obtenerDetallesPlanificacion(grupo, impacto){
		if(Impacto.aplicaACoordinacion(impacto)){
			return getPlanificacionActual().obtenerDetallesPlanificacion(grupo)
		}else{
			def detallesRet = []
			pedidosHijos.each { pedidoHijo ->
				detallesRet.addAll(pedidoHijo.obtenerDetallesPlanificacion(grupo, impacto))
			}
			return detallesRet
		}
	}

	def generarTablaAreaSoporteIncurridas(){

		def tablaAreaSoporte = []
		def todosLosPedidos = []
		todosLosPedidos += pedidosHijos
		todosLosPedidos << this

		todosLosPedidos.each{ pedido ->
			def detallesList = pedido.planificacionActual?.detalles.findAll{ it.esDeAreaDeSoporte() }
			if (detallesList){
				detallesList.each{ detalle ->

					String descripcion = detalle.tareaSoporte.areaSoporte ? detalle.tareaSoporte.areaSoporte.toString() : "Administrador Funcional"
					def detalleDTO = tablaAreaSoporte.find { descripcion == it?.descripcion }

					if (!detalleDTO){
						detalleDTO = [descripcion: descripcion, tarea: detalle.tareaSoporte ? "" : "Sin Actividad", fechaDesde: detalle.fechaDesde, fechaHasta: detalle.fechaHasta ,total: 0, totalMontoHoras:0,totalHorasIncurridas:0, items: []]
						tablaAreaSoporte << detalleDTO
					}

					if (detalle.tareaSoporte){
						def allFechas =[]
						if(!detalleDTO.items.isEmpty())
							allFechas += detalleDTO.items
						allFechas << detalle

						detalleDTO.fechaDesde = allFechas.fechaDesde.min()
						detalleDTO.fechaHasta = allFechas.fechaHasta.max()
						detalleDTO.total += detalle.totalHoras()
						detalleDTO.totalMontoHoras += detalle.totalMontoHoras()
						detalleDTO.totalHorasIncurridas+= detalle.totalHorasIncurridas(null)


						//otroCostoDTO.items << [descripcion: pedido.toString(), detalle: otroCostoItem.detalle, costo: otroCostoItem.costo]
						detalleDTO.items << [areaSoporte: pedido.toString(), tarea: detalle.tareaSoporte, fechaDesde: detalle.fechaDesde, fechaHasta: detalle.fechaHasta, totalHoras: detalle.totalHoras(), totalMontoHoras: detalle.totalMontoHoras(), totalHorasIncurridas: detalle.totalHorasIncurridas(null)]
					}
				}
			}

		}
		tablaAreaSoporte
	}

	def textoSinGrupoRealIncurrido(){
		return ""
	}

	boolean esSWDesarrolloSAP(){
		return true
	}

	boolean esElCoordinadorCambio(usuarioLogueado){
		return legajoCoordinadorCambio?.equalsIgnoreCase(usuarioLogueado)
	}

	def getSistemaSugeridoDescripcion(){
		return (sistemaSugerido) ? sistemaSugerido : ""
	}

	def getFechaVtoPlanificacionFormateada(){
		return DateUtil.toString(fechaVencimientoPlanificacion)
	}

	boolean puedeReasignarEvaluacionPedido(usuarioLogueado) {
		return puedeReasignarEvaluacionPedido(usuarioLogueado, true)
	}

	boolean puedeReasignarEvaluacionPedido(usuarioLogueado, verificaEstadoWorkflow) {
		if (verificaEstadoWorkflow) {
			if (isCancelado() || isSuspendido()) {
				return false
			}
			if (!this.faseActual?.equals(faseEvaluacionCambio())) {
				return false
			}
		}

		return legajoUsuarioGestionDemanda?.equalsIgnoreCase(usuarioLogueado) || new UmeService().usuarioPerteneceGrupo(grupoGestionDemanda, usuarioLogueado)
	}

	boolean puedeModificarEvaluacionPedido(usuarioLogueado) {
		if (isCancelado()) {
			return false
		}
		if (!this.faseActual?.equals(faseEvaluacionCambio())) {
			return false
		}

		return legajoUsuarioGestionDemanda?.equalsIgnoreCase(usuarioLogueado)
	}

	def getLegajoUsuarioResponsable() {
		return this.legajoCoordinadorCambio
	}

	def tieneObservers() {
		return observers.size() > 2
	}

	def puedeAgregarOtraPrueba(){
		return false
	}

	def getInterlocutorUsuario(){
		return getNombreApellido(legajoInterlocutorUsuario)
	}

	def getCoordinadorCambio(){
		return getNombreApellido(legajoCoordinadorCambio)
	}

	def getNombreApellido(legajo){
		def result
		if (legajo){
			result = new UmeService().getNombreApellido(legajo)
		}else{
			result = "N/A"
		}
		return result
	}

	def unificarObservers() {
		super.unificarObservers()
		pedidosHijos.each { pedidoHijo -> pedidoHijo.unificarObservers() }
	}

	def getFechaFinPlanificada(){
		def impactoInstance = new Impacto(Impacto.IMPACTO_CONSOLIDADO, this)
		def listadoConsolidado = new PedidoService().planificacionConsolidada(this.parent, impactoInstance)
		def consolidadoImplantacion = listadoConsolidado.find{ consolidado -> consolidado[0].toString().trim().equals(ActividadPlanificacion.FASE_IMPLANTACION)}
		return consolidadoImplantacion?.getAt(2)
	}

	def aplicaSuspension(usuarioLogueado, generaErrores){
		def aplicaFase = [faseEspecificacionCambio(), faseAprobacionCambio(), faseConsolidacionImpacto(), faseValidacionImpacto(), faseAprobacionImpacto()].contains(faseActual) || macroEstado.aplicaSuspension()
		
		
		def esCC = legajoCoordinadorCambio?.equalsIgnoreCase(usuarioLogueado)
		if (!generaErrores) {
			return aplicaFase && esCC
		}

		if (!aplicaFase) {
			this.errors.reject "El pedido se encuentra en fase " + faseActual + " y no puede ser suspendido"
		}
		if (!esCC) {
			this.errors.reject "Ud. no es actualmente CC"
		}

		return this.errors.errorCount == 0
	}

	def puedeSuspender(usuarioLogueado, generaErrores){
		return !estaSuspendido() && aplicaSuspension(usuarioLogueado, generaErrores)
	}

	def puedeSuspender(usuarioLogueado){
		return puedeSuspender(usuarioLogueado, false)
	}

	def puedeReanudar(usuarioLogueado){
		return estaSuspendido() && aplicaSuspension(usuarioLogueado, false)
	}

	def puedeReanudar(usuarioLogueado, generaErrores){
		return estaSuspendido() && aplicaSuspension(usuarioLogueado, generaErrores)
	}
	
	def reasignarUsuarioDeRol(codigoRol, legajoNuevo){
		if(codigoRol.equalsIgnoreCase(RolAplicacion.GERENTE_USUARIO)){
			this.legajoGerenteUsuario = legajoNuevo
		}
		if(codigoRol.equalsIgnoreCase(RolAplicacion.APROBADOR_IMPACTO)){
			this.legajoUsuarioAprobadorEconomico = legajoNuevo
		}
		if(codigoRol.equalsIgnoreCase(RolAplicacion.GESTION_DEMANDA)){
			this.legajoUsuarioGestionDemanda = legajoNuevo
		}
		if(codigoRol.equalsIgnoreCase(RolAplicacion.INTERLOCUTOR_USUARIO)){
			this.legajoInterlocutorUsuario = legajoNuevo
		}
		if(codigoRol.equalsIgnoreCase(RolAplicacion.USUARIO_FINAL)){
			this.legajoUsuarioCreador = legajoNuevo
		}
		if(codigoRol.equalsIgnoreCase(RolAplicacion.COORDINADOR_CAMBIO)){
			this.legajoCoordinadorCambio = legajoNuevo
		}
	}

	def planificacionesActualesConsolidadas() {
		def result = super.planificacionesActualesConsolidadas()
		result.addAll pedidosHijos.collect { it.planificacionActual?.id }
		return result 
	}
	
	def planificacionesActualesConsolidadasEnReplanificacion() {
		def result = super.planificacionesActualesConsolidadasEnReplanificacion()
		result.addAll pedidosHijos.collect { it.planificacionEnReplanificacion?.id }
		return result
	}
	
	def puedeReplanificar(usuarioLogueado){
		return esElCoordinadorCambio(usuarioLogueado) && !fasesReplanificacion.disjoint(pedidosHijos?.faseActual)
	}
	
	def getFasesReplanificacion(){
		return [faseConstruccionCambioHijo(), faseEjecucionPAU(), faseAprobacionPAUHijo(), faseAprobacionImplantacionHijo(),
		faseAdministrarActividades(), faseNormalizacionHijo(), faseImplantacionHijo()]
	}
	
	
	def setearDireccionDe(usuario) {
		this.codigoDireccionGerencia = usuario?.direccionCodigo 
		this.descripcionDireccionGerencia = usuario?.direccion 
	}
	

	boolean validaDireccionGerenteInterlocutor(){
		def direccionGerente = new UmeService().getUsuario(legajoGerenteUsuario)?.direccionCodigo
		def direccionInterlocutor = new UmeService().getUsuario(legajoInterlocutorUsuario)?.direccionCodigo
		if (direccionGerente && direccionInterlocutor && !direccionGerente?.equalsIgnoreCase(direccionInterlocutor)){
			this.errors.reject ("El gerente usuario debe pertenecer a la misma direccion que el interlocutor usuario")
			return false
		}
		return true
	}
	
	def aprobacionesPendientes(String codigoRol, unUsuario) {
		def aprobacionesReturn = aprobaciones.findAll { aprobacion -> unUsuario?.equalsIgnoreCase(aprobacion.usuario) && aprobacion.estaPendiente() && codigoRol?.equalsIgnoreCase(aprobacion.rol) }

		pedidosHijos.each { pedidoHijo ->
			aprobacionesReturn += pedidoHijo.aprobacionesPendientes(codigoRol, unUsuario)
		}
		
		return aprobacionesReturn
	}

	def validaPruebaOpcional(tipoPrueba) {
	}

}