
package ar.com.telecom.pcs.entities

import java.util.Date;

import ar.com.telecom.BusquedaController;
import ar.com.telecom.exceptions.BusinessException;
import ar.com.telecom.ume.UmeService

class DetallePlanificacionAreaSoporte extends DetallePlanificacion {

	public static int MAXIMO_TOTAL_HORAS = 500000000
	
	TareaSoporte tareaSoporte
	String comentarioDetalleAreaSoporte
	int cantidadHoras
	BigDecimal valorHora

	int cantidadHorasIncurridas
	Date fechaConfirmacionIncurridos
	String usuarioConfirmadorIncurridos
		
	static constraints  = {
		comentarioDetalleAreaSoporte(maxSize: 255, nullable: true)
		fechaConfirmacionIncurridos(nullable: true)
		usuarioConfirmadorIncurridos(nullable: true)
	}
	
	static mapping = {
		tareaSoporte fetch: 'join'
	}
	
	DetallePlanificacionAreaSoporte() {
		valorHora = 0
		cantidadHoras = 0
		cantidadHorasIncurridas = 0
	}
	
	String toString() {
		return super.toString() + " - " + tareaSoporte
	}

	def totalHoras() {
		return cantidadHoras
	}
	
	def totalHoras(grupo){
		return totalHoras()
	}
	
	def totalMontoHoras() {
		return cantidadHoras * valorHora
	}

	def validar(AbstractPedido pedido) {
		super.validar(pedido)
		
		if (!tareaSoporte){
			pedido.errors.reject("Debe ingresar una Actividad")
		}
		
		if (!valorHora) {
			pedido.errors.reject("Planificaci\u00F3n y esfuerzo " + tareaSoporte?.descripcion + ": el valor hora no puede ser nulo")
		}
		
		if (valorHora == 0) {
			pedido.errors.reject("Planificaci\u00F3n y esfuerzo " + tareaSoporte?.descripcion + ": el valor hora no puede ser cero")
		}

		if (valorHora >= BigDecimal.HALF_LONG_MAX_VALUE) {
			pedido.errors.reject("Planificaci\u00F3n y esfuerzo " + tareaSoporte?.descripcion + ": el valor hora no puede ser superior a " + BigDecimal.HALF_LONG_MAX_VALUE)
		}

		if (totalMontoHoras() >= BigDecimal.HALF_LONG_MAX_VALUE) {
			pedido.errors.reject("Planificaci\u00F3n y esfuerzo " + tareaSoporte?.descripcion + ": el monto total de horas no puede ser superior a " + BigDecimal.HALF_LONG_MAX_VALUE)
		}

		if (comentarioDetalleAreaSoporte?.size() > 255) {
			pedido.errors.reject("Planificaci\u00F3n y esfuerzo " + tareaSoporte?.descripcion + ": el comentario del \u00E1rea de soporte no debe exceder los 255 caracteres")
		}
		
		/*
		if (!cantidadHoras) {
			pedido.errors.reject("Planificaci\u00F3n y esfuerzo " + tareaSoporte?.descripcion + ": la cantidad de horas no pueden ser nulas")
		}
		
		if (cantidadHoras == 0) {
			pedido.errors.reject("Planificaci\u00F3n y esfuerzo " + tareaSoporte?.descripcion + ": la cantidad de horas no pueden ser cero")
		}*/
		
		return pedido.errors.errorCount == 0
	}
	
	boolean esDeAreaDeSoporte(){
		return true
	}

	boolean esDeSistema(){
		return false
	}
	
	boolean estaCargado() {
		return true
	}
	
	def getDescripcion() {
		return tareaSoporte.descripcion
	}
	
	def getGrupos() {
		return tareaSoporte.areaSoporte.grupoAdministrador
	}

	def getGrupos(origen) {
		return origen.grupoAdministrador
	}

	def tieneGrupo(grupo){
		return tareaSoporte.areaSoporte.grupoAdministrador.equals(grupo)
	}

	def tieneGrupo(grupo, origen){
		return origen.grupoAdministrador.equals(grupo)
	}

	def getFase(){
		return tareaSoporte.tipoActividad.fase
	}
	
	def puedeEditar(pedido, usuarioLogueado, grupo){
		def umeService = new UmeService()
		
		//return getFase().puedeEditar(pedido, usuarioLogueado) && pedido.fasesRealizadas().contains(getFase()) &&  (umeService.usuarioPerteneceGrupo(grupo, usuarioLogueado) || pedido.esResponsable(usuarioLogueado))
		return getFase().puedeEditar(pedido, usuarioLogueado) &&  (umeService.usuarioPerteneceGrupo(grupo, usuarioLogueado) || pedido.esResponsable(usuarioLogueado))
	}
	
	def confirmarRealesIncurridos(grupo, horas, usuarioLogueado, pedido){
		def umeService = new UmeService()
		fechaConfirmacionIncurridos = new Date()
		usuarioConfirmadorIncurridos = usuarioLogueado
		//cantidadHoras = horas
		cantidadHorasIncurridas = horas
		pedido.addToLogModificaciones(new LogModificaciones(legajo: usuarioLogueado, fechaHasta: new Date(), fase: pedido.faseActual, tipoEvento: LogModificaciones.CONFIRMAR_INCURRIDO, descripcionEvento: "El usuario "+usuarioLogueado+"( "+ umeService.getUsuario(usuarioLogueado)?.nombreApellido + ") confirmo las horas incurridas del detalle "+getDescripcion() ))
	}
	
	def totalHorasIncurridas(grupo){
		return cantidadHorasIncurridas 
	}
	
	def confirmoRealesIncurridos(grupo){
		return fechaConfirmacionIncurridos != null
	}
	
	def actualizarHorasIncurridas(grupo, cantidadHoras){
		if(!confirmoRealesIncurridos()){
			cantidadHorasIncurridas = new Integer(cantidadHoras).intValue()
		}
	}
	
	def blanqueaHorasIncurridas(grupo){
		if(!confirmoRealesIncurridos()){
			cantidadHorasIncurridas = 0
		}
	}
	
	def tieneFechaImplantacion(){
		return false
	}
}
