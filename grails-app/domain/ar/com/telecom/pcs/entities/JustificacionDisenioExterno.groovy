package ar.com.telecom.pcs.entities

class JustificacionDisenioExterno {

	String descripcion
	
    static constraints = {
		descripcion(size:1..80)
    }
	
	String toString(){
		return descripcion
	}
	
}
