package ar.com.telecom.pcs.entities

import java.util.List

/**
 * Representa una validación que se hace con muchos campos, en los cuales alguno tiene que estar completo
 * 
 * @author u193151
 *
 */
class ValidacionCruceCamposAnexos extends ValidacionCamposActividad {

	// Campo que necesita para no mambearse
	@Deprecated
	String filler
	
	static constraints = {
		filler(maxSize:1, nullable: true)
	}
	
	/**
	 * Chequea que alguno de los campos no sea vacío o hayan ingresado anexos
	 */
	public boolean validar(Actividad actividad, AbstractPedido pedido) {
		boolean ingresoCampos = new ValidacionCamposCruzados(camposAValidar: camposAValidar).doValidar(actividad)
		log.info "ingreso campos: " + ingresoCampos
		boolean ingresoAnexos = !actividad.anexos.isEmpty()
		log.info "ingresoAnexos: " + ingresoAnexos 
		def validacionCampos = ingresoCampos || ingresoAnexos
		if (!validacionCampos) {
			log.info "pedido: " + pedido
			log.info "errors: " + pedido.errors
			log.info "campos a validar: " + camposAValidar
			actividad.errors.reject(actividad.descripcion + ": Debe ingresar alguno de estos campos: " + camposAValidar + " o bien cargar un anexo")
		}
		return validacionCampos
	}

	public String toString() {
		return "Validaci\u00F3n cruce campos anexos - campos a validar: " + camposAValidar.toString()
	}
	
}
