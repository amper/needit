package ar.com.telecom.pcs.entities

/**
 * Define el tailoring contra una estrategia de prueba en base a un tipo de prueba (PU, PI, PAU, etc.)
 * 
 * @author u193151
 *
 */
class TailoringEstrategiaPrueba extends CriterioTailoring {

	TipoPrueba tipoPrueba
	
	static mapping = {
		tipoPrueba fetch: 'join'
	}
	
	String toString() {
		"Tailoring Estrategia de Prueba " + tipoPrueba.toString()
	}
	
	public boolean seRealiza(AbstractPedido pedido) {
		def result = pedido.realizaTipoPrueba(this.tipoPrueba)
		log.info "TailoringEstrategiaPrueba Tipo de prueba: " + this.tipoPrueba
		log.info "Tipo de prueba: " + result
		return result
	}
}
