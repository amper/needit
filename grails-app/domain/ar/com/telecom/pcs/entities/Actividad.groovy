package ar.com.telecom.pcs.entities

import java.util.Date

import ar.com.telecom.ume.UmeService
import ar.com.telecom.util.DateUtil

class Actividad {
	public static final String USUARIO_FINAL = "Usuario final"
	public static final String INTERLOCUTOR_USUARIO = "Interlocutor usuario"


	Fase fase

	// XOR
	// Si la actividad es de sistemas? Siempre debería tener tipo de actividad
	TipoActividad tipoActividad

	String grupoResponsable
	String legajoUsuarioResponsable
	EstadoActividad estado
	Date fechaCreacion
	AbstractPedido pedido

	// Datos del cierre
	Date fechaCierre
	CodigoCierre codigoCierre
	String legajoUsuarioCierre
	String comentariosCierre

	// Opcional - Comentarios
	String comentarios
	// Opcional - Fecha sugerida
	Date fechaSugerida
	// Opcional - Información sobre pasajes
	String numeroTicket
	// Opcional - ambientes
	String nombreServidor
	String ambiente
	// Versionador
	Versionador versionador
	String numeroReferencia
	// Opcional - Mercury (TipoActividad)
	String dominioMercury
	String proyectoMercury
	// Opcional - Release
	Release release
	// Opcional - URL
	String url

	String descripcion
	Date fechaGrabacion
	
	// TRANSIENT
	def legajoUsuarioResponsablePop
	
	static belongsTo = [ pedido : AbstractPedido ]

	static constraints = {
		grupoResponsable(maxSize: 50)
		legajoUsuarioResponsable(maxSize: 20, nullable: true)
		fechaCierre(nullable:true)
		codigoCierre(nullable: true)
		legajoUsuarioCierre(maxSize: 10, nullable: true)
		comentariosCierre(nullable: true)
		comentarios(nullable: true)
		fechaSugerida(nullable: true)
		numeroTicket(maxSize: 20, nullable:true)
		nombreServidor(maxSize: 30, nullable: true)
		ambiente(maxSize: 30, nullable: true)
		numeroReferencia(maxSize: 40, nullable: true)
		dominioMercury(maxSize: 40, nullable: true)
		proyectoMercury(maxSize: 40, nullable: true)
		release(nullable: true)
		versionador(nullable: true)
		url(maxSize: 250, nullable: true)
		descripcion(nullable: true)
		fechaGrabacion(nullable: true)
	}

	static mapping = {
		fase fetch: 'join'
		tipoActividad fetch: 'join'
		estado fetch: 'join'
		codigoCierre fetch: 'join'
		comentariosCierre type: 'text'
		comentarios type: 'text'
		release fetch: 'join'
		versionador fetch: 'join'
	}

	static hasMany = [ anexos: AnexoPorTipo ]
	
	public Actividad() {
		fechaCreacion = new Date()
		fechaGrabacion = null
		anexos = []
	}

	String toString() {
		return descripcion
	}

	def getFechaFinPlanificada() {
		return pedido.fechaFinPlanificadaActividad
	}

	def agregarComentario(String comentario, String usuario, String nombreUsuario) {
		if(comentario){
			def lineaComment = "<b>" + nombreUsuario + " (" + usuario + ")</b> - " + DateUtil.toString(new Date(), true) + "<br>" + comentario + "<hr>"
			if (comentarios){
				comentarios = lineaComment + comentarios
			}else{
				comentarios = lineaComment
			}
		}
	}


	def validar(AbstractPedido pedido) {
		//NDE: Previamente para agregar los errores haciamos: pedido.errors
		//fue modificado a this.errors y algunas veces no funcionaba
		//ahora aparentemente anda
		//Firma tambinen: EAP

		if(pedido.estaSuspendido()){
			this.errors.reject(descripcion + ": No puede cerrarse por estar el pedido en estado suspendido")
		}

		if (numeroTicket?.size() > 20) {
			this.errors.reject(descripcion + ": El n\u00FAmero de ticket no debe exceder los 20 caracteres")
		}

		if (nombreServidor?.size() > 30) {
			this.errors.reject(descripcion + ": El nombre del servidor no debe exceder los 25 caracteres")
		}

		if (ambiente?.size() > 30) {
			this.errors.reject(descripcion + ": El ambiente no debe exceder los 10 caracteres")
		}

		if (numeroReferencia?.size() > 40) {
			this.errors.reject(descripcion + ": El n\u00FAmero de referencia no debe exceder los 40 caracteres")
		}

		if (dominioMercury?.size() > 40) {
			this.errors.reject(descripcion + ": El dominio Mercury no debe exceder los 40 caracteres")
		}

		if (proyectoMercury?.size() > 40) {
			this.errors.reject(descripcion + ": El proyecto Mercury no debe exceder los 40 caracteres")
		}

		if (url?.size() > 250) {
			this.errors.reject(descripcion + ": La URL no debe exceder los 250 caracteres")
		}

		// Estas validaciones son ahora obligatorias seg\u00FAn defectos encontrados 16/06/2012
		if (!tipoActividad) {
			this.errors.reject("Debe ingresar tipo de actividad")
		}
		
		if (!grupoResponsable) {
			this.errors.reject(descripcion + ": Debe ingresar grupo responsable de la actividad")
		}else{
			def usuarios = new UmeService().getUsuariosGrupoLDAP(grupoResponsable, null)
			if(!gruposEspeciales() && usuarios?.isEmpty()){
				this.errors.reject(descripcion + ": Debe ingresar grupo responsable que posea usuarios")
			}
		}

		if (!legajoUsuarioResponsable) {
			this.errors.reject(descripcion + ": Debe ingresar usuario responsable de la actividad")
		}
		// FIN estas validaciones son ahora obligatorias

		if (seCompleto() || !codigoCierre) {
			if (tipoActividad?.esObligatorioComentarios() && !comentarios) {
				this.errors.reject(descripcion + ": Debe ingresar comentarios de la actividad")
			}

			if (tipoActividad?.esObligatorioFechaSugerida() && !fechaSugerida) {
				this.errors.reject(descripcion + ": Debe ingresar fecha sugerida")
			}

			if (tipoActividad?.esObligatorioAnexos() && anexos?.isEmpty()) {
				this.errors.reject(descripcion + ": Debe ingresar anexos")
			}

			if (tipoActividad?.esObligatorioPasajes()) {
				if (!numeroTicket) {
					this.errors.reject(descripcion + ": Debe ingresar el n\u00FAmero de ticket correspondiente al pasaje")
				}
			}

			if (tipoActividad?.esObligatorioVersionador()) {
				if (!versionador) {
					this.errors.reject(descripcion + ": Debe indicar el versionador a utilizar")
				}
				if (!numeroReferencia) {
					this.errors.reject(descripcion + ": Debe ingresar n\u00FAmero de referencia del versionador")
				}
			}

			if (tipoActividad?.esObligatorioAmbiente()) {
				if (!nombreServidor) {
					this.errors.reject(descripcion + ": Debe ingresar el nombre del servidor donde se realizar\u00E1 la actividad")
				}
				if (!ambiente) {
					this.errors.reject(descripcion + ": Debe ingresar el ambiente donde se realizar\u00E1 la actividad")
				}
			}

			if (tipoActividad?.esObligatorioMercury()) {
				if (!dominioMercury) {
					this.errors.reject(descripcion + ": Debe ingresar el dominio Mercury")
				}

				if (!proyectoMercury) {
					this.errors.reject(descripcion + ": Debe ingresar el proyecto Mercury")
				}
			}

			if (tipoActividad?.esObligatorioRelease()) {
				if (!release) {
					this.errors.reject(descripcion + ": Debe ingresar datos del release")
				}
			}

			if (tipoActividad?.esObligatorioURL()) {
				if (!url) {
					this.errors.reject(descripcion + ": Debe ingresar URL")
				}
			}

			tipoActividad?.validar(this, pedido)
			
		}
		
		def result = estado.validar(pedido, this)
		return this.errors.errorCount == 0 && result
	}

	boolean estaPendiente() {
		return !estaFinalizada()
	}

	def estaSuspendido() {
		return false
	}

	def estaCancelado() {
		return false
	}
	
	boolean estaFinalizada(){
		return this.fechaCierre != null
	}

	boolean estaRechazada() {
		return estaFinalizada() && !terminoOk()
	}

	boolean terminoOk() {
		return CodigoCierre.exitoso().equals(codigoCierre) || CodigoCierre.observaciones().equals(codigoCierre)
	}

	boolean seCompleto() {
		return codigoCierre?.seCompleto()
	}

	boolean esExitosa() {
		return CodigoCierre.exitoso().equals(codigoCierre)
	}

	def validarFinalizacion() {
		log.info "Codigo de cierre: " + codigoCierre
		log.info "Comentarios cierre: " + comentariosCierre

		if (!CodigoCierre.cancelada().equals(codigoCierre)&& !CodigoCierre.rechazada().equals(codigoCierre) && !validar(pedido)) {
			pedido.errors.each { this.errors.reject it.toString() }
			
			//this.errors = pedido.errors
			
			//this.errors.reject "Falta completar algunos campos: ingréselos en la ventana de edición"
		}

		if(!esExitosa() && !comentariosCierre){
			this.errors.reject("Debe completar el campo comentarios de cierre si el c\u00F3digo no es exitoso")
		}

		if(!codigoCierre.estadoActividad.cerrada() && !comentariosCierre){
			this.errors.reject("Debe completar el campo comentarios de cierre si el estado no es cerrado")
		}

		return this.errors.errorCount == 0
	}

	boolean finalizar(String usuarioLogueado){
		if (!validarFinalizacion()){
			return false
		}

		this.fechaCierre = new Date()
		this.estado = codigoCierre.estadoActividad

		def tipoEvento
		if (this.codigoCierre.ok()) {
			tipoEvento = LogModificaciones.ACTIVIDAD_FINALIZADA_OK
		} else {
			tipoEvento = LogModificaciones.ACTIVIDAD_FINALIZADA_ERROR
		}
		def justificacion = ""
		if (this.comentariosCierre) {
			justificacion = " - justificaci\u00F3n: " + this.comentariosCierre
		}
		pedido.addToLogModificaciones(new LogModificaciones(legajo: usuarioLogueado, fechaDesde: this.fechaCreacion, rol: pedido.getRol(fase, usuarioLogueado), fechaHasta: this.fechaCierre, fase: fase, descripcionEvento: "Cierre actividad " + tipoActividad + justificacion, tipoEvento: tipoEvento, actividad: this))

		return true
	}

	def eliminarAnexo(unAnexo, usuarioLogueado) {
		// OJO, el removeFromAnexos no toma el id, por eso hay que hacer esto
		def anexo = this.anexos.find { anexo -> anexo.id.equals(unAnexo.id) }
		this.removeFromAnexos(anexo)
	}

	def esElAsignatario(legajo){
		return legajo?.equalsIgnoreCase(this.legajoUsuarioResponsable)
	}

	boolean esDeDisenioInterno(fase){
		return this.fase.equals(fase) && tipoActividad.descripcion.equals("Documentar Diseño Interno")
	}

	def getUsuariosResponsables() {
		if (legajoUsuarioResponsable) {
			return [legajoUsuarioResponsable]
		} else {
			def usuarios = new UmeService().getUsuariosGrupoLDAP(grupoResponsable, null)
			return usuarios.legajo
		}
	}

	def puedeEditarGrupo(usuarioLogueado) {
		return this.puedeEditarActividad(usuarioLogueado) && !gruposEspeciales()
	}

	def gruposEspeciales() {
		return [this.USUARIO_FINAL, this.INTERLOCUTOR_USUARIO].contains(this.grupoResponsable)
	}
	
	def puedeEditarActividad(usuarioLogueado) {
		return !this.estaFinalizada() && (esElAsignatario(usuarioLogueado) || usuarioLogueado.equalsIgnoreCase(this.legajoUsuarioResponsablePop)) 
	}
	
	def nuevaActividad() {
		return !this.id
	}
	
	def puedeReasignarResponsable(usuarioLogueado) {
		if (this.estaFinalizada() || this.gruposEspeciales()) {
			return false
		}
		return (puedeEditarActividad(usuarioLogueado) || this.perteneceAlGrupo(usuarioLogueado)) && !this.pedido?.estaSuspendido()
	}
	
	boolean puedeGrabar(usuarioLogueado) {
		return this.puedeEditarActividad(usuarioLogueado) || (!this.legajoUsuarioResponsable)
	}

	def perteneceAlGrupo(usuarioLogueado) {
		return new UmeService().getUsuariosGrupoLDAP(grupoResponsable, null).collect { it.legajo }.contains(usuarioLogueado)
	}	

	public String getUrl(){
		if(!url){
			return ""
		}
		return url.contains("http://")? url : "http://"+url
	}
	
	boolean actualizar(aprobacionUI, boolean cambiaResponsable) {
		boolean seActualizo = false
		if (aprobacionUI.grupo) {
			grupoResponsable = aprobacionUI.grupo
			seActualizo = true
		}
		if (aprobacionUI.asignatario || cambiaResponsable) {
			if (aprobacionUI.asignatario && !aprobacionUI.asignatario?.equalsIgnoreCase(legajoUsuarioResponsable)) {
				println "Cambio fecha grabacion"
				fechaGrabacion = new Date()
			}
			legajoUsuarioResponsable = aprobacionUI.asignatario
			seActualizo = true
		}
		return seActualizo
	}
	
	def getUsuario() {
		return legajoUsuarioResponsable
	}
	
	def getGrupo() {
		return grupoResponsable
	}

	def traerDatosRelacionados() {
		anexos?.nombreArchivo
		pedido.legajoUsuarioCreador
		legajoUsuarioResponsablePop = legajoUsuarioResponsable
	}

//	def generarCopia() {
//		return new Actividad(
//				fase: this.fase, tipoActividad: this.tipoActividad, grupoResponsable: this.grupoResponsable, 
//				legajoUsuarioResponsable: this.legajoUsuarioResponsable, estado: this.estado, pedido: this.pedido,
//				fechaCierre: this.fechaCierre, codigoCierre: this.codigoCierre, legajoUsuarioCierre: this.legajoUsuarioCierre,
//				comentariosCierre: this.comentariosCierre, comentarios: this.comentarios, fechaSugerida: this.fechaSugerida,
//				numeroTicket: this.numeroTicket, nombreServidor: this.nombreServidor, ambiente: this.ambiente,
//				numeroReferencia: this.numeroReferencia, dominioMercury: this.dominioMercury, proyectoMercury: this.proyectoMercury,
//				release: this.release, url: this.url, descripcion: this.descripcion, anexos: this.anexos
//			)
//	}

	def getAnexos(Fase fase) {
		return anexos.findAll { anexo -> anexo.esDeFase(fase) }
	}
	
	boolean estaEnCurso(){
		return true
	}
	public static boolean esUsuarioFinal(unGrupo){
		return unGrupo?.equalsIgnoreCase(Actividad.USUARIO_FINAL)
	}
	
	public static boolean esInterlocutorUsuario(unGrupo){
		return unGrupo?.equalsIgnoreCase(INTERLOCUTOR_USUARIO)
	}

	def pendientePara(grupoActividad, legajoActual) {
		return estaPendiente() && grupoResponsable.equals(grupoActividad) && this.esElAsignatario(legajoActual)
	}
	
	boolean tieneResponsable() {
		return legajoUsuarioResponsable != null
	}
	
	boolean tieneFechaGrabacion(){
		return fechaGrabacion != null
	}			

}
