package ar.com.telecom.pcs.entities

class ConfiguracionEMailReasignacion {

	Fase fase
	String asunto
	String texto
	// Si es para el usuario que quedó actualmente asignado --> true
	// Si es para el usuario al que reasignaron por otro (el anterior) --> false
	boolean actual
	RolAplicacion rol
	
	static constraints = { 
		asunto(maxSize:1024) 
		fase(nullable: true)
	}

	static mapping = {
		fase fetch: 'join' 
		texto type: 'text'
		rol fetch: 'join'
	}

	def traerDatosRelacionados() {
	}

	public String toString() {
		return "Configuraci\u00F3n por reasignaci\u00F3n para fase " + fase + " - rol: " + rol + (actual ? " actual" : " anterior")
	}
	
}
