package ar.com.telecom.pcs.entities

import ar.com.telecom.EstadoActividadMultiton

class EstadoActividad {

	public static String ABIERTA = "Abierta"
	public static String CERRADA = "Cerrada"
	
	String descripcion
	boolean esEstadoFinal
	boolean aprobado
	
    static constraints = {
		descripcion(size:1..50)
    }
	
	String toString(){
		return descripcion
	}
	
	def validar(AbstractPedido pedido, Actividad actividad) {
		if (esEstadoFinal && !aprobado) {
			if (!actividad.comentariosCierre) {
				actividad.errors.reject("Debe ingresar comentario de cierre")
			}
		}
		
		return actividad.errors.errorCount == 0
	}
	
	static def abierta() {
		return EstadoActividadMultiton.getEstadosActividad(ABIERTA)
	}
	
	static def cerrada() {
		return EstadoActividadMultiton.getEstadosActividad(CERRADA)
	}
	
	public boolean equals(Object o) {
		try {
			EstadoActividad otro = (EstadoActividad) o
			return otro.descripcion.equals(descripcion)
		} catch (ClassCastException e) {
			return false
		}
	}
	
	public int hashCode() {
		return descripcion.hashCode()
	}
	
}
