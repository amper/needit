package ar.com.telecom.pcs.entities

class TipoActividad {

	public static String OBLIGATORIO = "M"
	public static String OPCIONAL = "O"
	public static String TAILORING = "T"
	public static String TAILORING_OPCIONAL = "C"
	
	public static String CAMPO_NO_APLICA = "N/A"
	public static String CAMPO_OBLIGATORIO = "OBL"
	public static String CAMPO_OPCIONAL = "OPC"
	
	Fase fase
	TipoImpacto tipoImpacto
	TipoGestion tipoGestion
	String descripcion
	
	String estrategiaComentarios
	String estrategiaFechaSugerida
	String estrategiaAnexos
	String estrategiaPasajes
	String estrategiaAmbiente
	String estrategiaVersionador
	String estrategiaMercury
	String estrategiaRelease
	String estrategiaURL
	  
	String obligatoriedad
	CriterioTailoring criterioTailoring
	CriterioValidacionActividad criterioValidacionActividad
	
    static constraints = {
		descripcion(size:1..70)
		estrategiaComentarios(maxSize: 5, nullable: true)
		estrategiaFechaSugerida(maxSize: 5, nullable: true)
		estrategiaAnexos(maxSize: 5, nullable: true)
		estrategiaPasajes(maxSize: 5, nullable: true)
		estrategiaAmbiente(maxSize: 5, nullable: true)
		estrategiaVersionador(maxSize: 5, nullable: true)
		estrategiaMercury(maxSize: 5, nullable: true)
		estrategiaRelease(maxSize: 5, nullable: true)
		estrategiaURL(maxSize: 5, nullable: true)
		obligatoriedad(size:1..2)
		criterioTailoring(nullable: true)
		criterioValidacionActividad(nullable: true)
    }
	
	static mapping = {
		fase fetch: 'join'
		tipoImpacto fetch: 'join'
		tipoGestion fetch: 'join'
		criterioTailoring fetch: 'join'
		criterioValidacionActividad fetch: 'join'
	}
	
	public TipoActividad() {
		estrategiaComentarios = CAMPO_OPCIONAL
		estrategiaFechaSugerida = CAMPO_OPCIONAL  
		estrategiaAnexos = CAMPO_OPCIONAL
		estrategiaPasajes = CAMPO_NO_APLICA
		estrategiaAmbiente = CAMPO_NO_APLICA
		estrategiaVersionador = CAMPO_NO_APLICA
		estrategiaMercury = CAMPO_NO_APLICA
		estrategiaRelease = CAMPO_NO_APLICA
		estrategiaURL = CAMPO_NO_APLICA
		criterioValidacionActividad = null
	}
	
	String toString() {
		descripcion
	}
	
	public boolean esObligatoria(AbstractPedido pedido) {
		return obligatoriedad.equalsIgnoreCase(OBLIGATORIO) || (obligatoriedad.equalsIgnoreCase(TAILORING) && aplicaTailoring(pedido))
	}
	
	public boolean esOpcional(AbstractPedido pedido) {
		return obligatoriedad.equalsIgnoreCase(OPCIONAL) || (obligatoriedad.equalsIgnoreCase(TAILORING_OPCIONAL) && aplicaTailoring(pedido))
	}
	
	public boolean aplicaTailoring(AbstractPedido pedido) {
		return criterioTailoring?.seRealiza(pedido)
	}
	
	public boolean muestraComentarios() {
		return seMuestra(estrategiaComentarios)
	}

	public boolean muestraFechaSugerida() {
		return seMuestra(estrategiaFechaSugerida)
	}
	
	public boolean muestraAnexos() {
		return seMuestra(estrategiaAnexos)
	}
	
	public boolean muestraPasajes() {
		return seMuestra(estrategiaPasajes)
	}
	
	public boolean muestraAmbiente() {
		return seMuestra(estrategiaAmbiente)
	}

	public boolean muestraVersionador() {
		return seMuestra(estrategiaVersionador)
	}
	
	public boolean muestraMercury() {
		return seMuestra(estrategiaMercury)
	}

	public boolean muestraRelease() {
		return seMuestra(estrategiaRelease)
	}

	public boolean muestraURL() {
		return seMuestra(estrategiaURL)
	}

	// Preguntar a dodi
	//public boolean seMuestra(String estrategiaCampo, Actividad actividad) {
	//	return [CAMPO_OBLIGATORIO, CAMPO_OPCIONAL].contains(estrategiaCampo) 
	//}
	
	public boolean seMuestra(String estrategiaCampo) {
		return [CAMPO_OBLIGATORIO, CAMPO_OPCIONAL].contains(estrategiaCampo)
	}
	
	public boolean esObligatorioComentarios() {
		estrategiaComentarios.equalsIgnoreCase(CAMPO_OBLIGATORIO)
	}
	
	public boolean esObligatorioFechaSugerida() {
		estrategiaFechaSugerida.equalsIgnoreCase(CAMPO_OBLIGATORIO)
	}
	
	public boolean esObligatorioAnexos() {
		estrategiaAnexos.equalsIgnoreCase(CAMPO_OBLIGATORIO)
	}
	
	public boolean esObligatorioPasajes() {
		estrategiaPasajes.equalsIgnoreCase(CAMPO_OBLIGATORIO)
	}
	
	public boolean esObligatorioVersionador() {
		estrategiaVersionador.equalsIgnoreCase(CAMPO_OBLIGATORIO)
	}
	
	public boolean esObligatorioAmbiente() {
		estrategiaAmbiente.equalsIgnoreCase(CAMPO_OBLIGATORIO)
	}
	
	public boolean esObligatorioMercury() {
		estrategiaMercury.equalsIgnoreCase(CAMPO_OBLIGATORIO)
	}
	
	public boolean esObligatorioRelease() {
		estrategiaRelease.equalsIgnoreCase(CAMPO_OBLIGATORIO)
	}
	
	public boolean esObligatorioURL() {
		estrategiaURL.equalsIgnoreCase(CAMPO_OBLIGATORIO)
	}

	public void validar(Actividad actividad, AbstractPedido pedido) {
		criterioValidacionActividad?.validar(actividad, pedido)
	}	
}
