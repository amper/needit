package ar.com.telecom.pcs.entities

class ParametrosSistema {

	String grupoLDAPUsuarioFinal
	String grupoLDAPInterlocutorUsuario
	String grupoLDAPFuncionGestionDemandaGral
	String grupoLDAPCoordinadorPedido
	String grupoAdmin	 
	String tooltipNotasDesarrollo
	TipoGestion tipoGestionDefault
	String cartelAdvertenciaGerentesIT
	String mensajeAdvertenciaValidar
	String mailEmisorPCS
	String mailAdministradorFuncional
	boolean enviaMails
	boolean auditaMails
	BigDecimal valorHoraSWFTecoDefault
	int minimoJerarquiaAprobador
	int maximoJerarquiaAprobador
	int minimoJerarquiaGerente
	int maximoJerarquiaGerente
	Integer tiempoActualizacionInbox
	Integer tiempoActualizacionInboxAprobaciones
	@Deprecated Integer repeticionHoras
	String notaReporteAprobacionImpacto
	
    static constraints = {
		grupoLDAPUsuarioFinal(maxSize:100, nullable:true)
		grupoLDAPInterlocutorUsuario(size:1..100)
		grupoLDAPFuncionGestionDemandaGral(size:1..100)
		grupoLDAPCoordinadorPedido(size:1..100)
		grupoAdmin(maxSize:100, nullable:true)
		tooltipNotasDesarrollo(maxSize:250, nullable:true)
		cartelAdvertenciaGerentesIT(size:1..255)
		mensajeAdvertenciaValidar(nullable: true)
		mailEmisorPCS(email:true)
		mailAdministradorFuncional(email: true, nullable: true)
		tiempoActualizacionInbox(nullable: true)
		tiempoActualizacionInboxAprobaciones(nullable: true)
		repeticionHoras(nullable: true)
		notaReporteAprobacionImpacto(nullable:true)
    }
	
	static mapping = {
		mensajeAdvertenciaValidar(type: 'text')
		notaReporteAprobacionImpacto(type: 'text')
	}

	def traerDatosRelacionados() {
		tipoGestionDefault.traerDatosRelacionados()
	}
	
	def getRoleAdmin() {
		"ROLE_" + grupoAdmin
	}
}
