package ar.com.telecom.pcs.entities

import java.util.Date;

class CargaHoraIncurrida {
	SoftwareFactory softwareFactory
	int cantidadHoras
	
	Date fechaConfirmacionIncurridos
	String usuarioConfirmadorIncurridos
	
    static constraints = {
		fechaConfirmacionIncurridos(nullable: true)
		usuarioConfirmadorIncurridos(nullable: true)
		softwareFactory(nullable: true)
    }
	static mapping = {
		softwareFactory fetch: 'join'
	}
	
	CargaHoraIncurrida(){
		cantidadHoras = 0 
	}
	
	def totalHoras() {
		return cantidadHoras
	}
	
	public String toString() {
		return "softwareFactory: " + softwareFactory+ " cantidadHoras:"+cantidadHoras
	}
	
	def confirmoRealesIncurridos(){
		return fechaConfirmacionIncurridos != null
	}
	
	def blanquear(){
		if(!confirmoRealesIncurridos()){
			cantidadHoras = 0
		}
	}
}
