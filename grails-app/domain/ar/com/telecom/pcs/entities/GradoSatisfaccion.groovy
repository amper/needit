package ar.com.telecom.pcs.entities

class GradoSatisfaccion {

	String descripcion
	
    static constraints = {
		descripcion(size:1..50)
    }
	
	String toString(){
		return descripcion
	}

}
