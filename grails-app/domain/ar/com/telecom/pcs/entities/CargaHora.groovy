package ar.com.telecom.pcs.entities

import ar.com.telecom.util.NumberUtil

class CargaHora {

	SoftwareFactory softwareFactory
	Periodo periodo
	int cantidadHoras
	BigDecimal valorHora
	BigDecimal totalMontoHoras

	static constraints = {
		softwareFactory(nullable: true)
		periodo(nullable: true)
		valorHora(nullable: true)
	}

	static mapping = {
		softwareFactory fetch: 'join'
		periodo fetch: 'join'
		valorHora scale: 3
		totalMontoHoras formula: "cantidad_horas * valor_hora"
	}

	static belongsTo = [ detalle : DetallePlanificacionSistema ]

	CargaHora() {
		cantidadHoras = 0
		valorHora = NumberUtil.setearDecimalesDefault(new BigDecimal(0))
	}

	String toString() {
		return periodo.toString() + " - " + softwareFactory.toString()
	}

	def totalHoras() {
		return cantidadHoras
	}

	def totalMontoHoras() {
		return cantidadHoras * valorHora
	}

	def validar(AbstractPedido pedido) {
		if (!softwareFactory && pedido.esHijo()) {
			pedido.errors.reject "Planificaci\u00F3n y esfuerzo : debe ingresar un grupo de atenci\u00F3n"
		}
		

		if (softwareFactory?.gestionaCapacidad && !periodo) {
			pedido.errors.reject "Planificaci\u00F3n y esfuerzo " + (!softwareFactory?"":("- carga de horas de "+ softwareFactory)) + ": debe ingresar per\u00EDodo ya que el grupo de software factory gestiona capacidad"
		}
		
		if (cantidadHoras <= 0) {
			pedido.errors.reject "Planificaci\u00F3n y esfuerzo " + (!softwareFactory?"":("- carga de horas de "+ softwareFactory)) + ": debe ingresar cantidad de horas mayor a cero"
		}else{
			if(NumberUtil.cantidadDigitosEnteros(cantidadHoras) > 5){
				pedido.errors.reject "Planificaci\u00F3n y esfuerzo " + (!softwareFactory?"":("- carga de horas de "+ softwareFactory)) + ": la cantidad de horas debe ser un n\u00FAmero con un m\u00E1ximo de 5 caracteres"
			}
		}
		
		if (!valorHora) {
			pedido.errors.reject "Planificaci\u00F3n y esfuerzo " + (!softwareFactory?"":("- carga de horas de "+ softwareFactory)) + ": debe ingresar valor hora"
		}else{
			if(NumberUtil.cantidadDigitosEnteros(valorHora) > 8){
				pedido.errors.reject "Planificaci\u00F3n y esfuerzo " + (!softwareFactory?"":("- carga de horas de "+ softwareFactory)) + ": el valor hora debe ser un numero con un m\u00E1ximo de 8 caracteres"
			}
		}
		
		
		return (!pedido?.errors)||(pedido?.errors?.errorCount == 0)
	}
	
	public int compareTo(CargaHora otraCargaHora){
		if(!otraCargaHora){
			return -1
		}
		if(otraCargaHora.equals(this)){
			return 0
		}
		if(softwareFactory.compareTo(otraCargaHora.softwareFactory) < 0){
			return periodo.compareTo(otraCargaHora.periodo)
		}else{
			return -1
		}
	}
}
