package ar.com.telecom.pcs.entities

import java.util.Date;

import ar.com.telecom.Person;

class LogModificaciones {

	String legajo
	Date fechaDesde
	Date fechaHasta
	Fase fase
	String tipoEvento
	String descripcionEvento
	String rol
	Actividad actividad
	
	static belongsTo = [pedido: AbstractPedido]
	
	public static APROBACION = "A"
	public static APROBACION_AUTOMATICA = "L"
	public static CREACION = "C"
	public static DENEGACION = "D"
	public static FINALIZACION = "F"
	public static DENEGACION_OMITIDA = "N"
	public static APROBACION_OMITIDA = "O"
	public static MODIFICACION_DATOS = "M"
	public static REENVIAR = "V"
	public static REASIGNACION = "H"
	public static ASIGNACION = "AS"
	
	public static CONFIRMAR_INCURRIDO = "I"
	
	public static CANCELACION = "K"
	public static RETOMAR_CANCELACION = "B"
	
	public static SUSPENSION = "S"
	public static RETOMAR_SUSPENSION = "R"

	// Relacionadas con las tareas
	public static ACTIVIDAD_GENERADA_AUTOMATICA = "E"
	public static ACTIVIDAD_FINALIZADA_OK = "G"
	public static ACTIVIDAD_FINALIZADA_ERROR = "J"
	public static ACTIVIDAD_GENERADA_MANUAL = "AM"

	// Esto no necesariamente genera un log de modificaci\u00F3n pero se usa para envío de mail
	public static MODIFICACION_ANEXO_GLOBAL = "W"
	public static ANEXO_GLOBAL_DIFERENTE_FASE = "X"
	public static ELIMINAR_ANEXO_FASE_ACTUAL = "Y"
	public static ELIMINAR_ANEXO_GLOBAL = "EG"
	
	public static ACTUALIZACION_DIRECCION_PEDIDO = "AD"
	
	public static EN_REPLANIFICACION = "ER"
	
	static constraints = {
		legajo(size:1..20, nullable: true)
		fechaDesde(nullable: true)
		fechaHasta(nullable: true)
		tipoEvento(maxSize: 2)
		rol(nullable: true)
		actividad(nullable: true)
	}
	
	static mapping = {
		descripcionEvento type: 'text'
		fase fetch: 'join'
	}
	
	public String toString() {
		return descripcionTipoEvento() + " " + fechaHasta + " (" + legajo + ") " + descripcionEvento + " - fase: " + fase.descripcion
	}
	
	public static getTipoEventos(){
		["Aprobaci\u00F3n" : APROBACION,
		 "Aprobaci\u00F3n autom\u00E1tica" : APROBACION_AUTOMATICA,
		 "Denegaci\u00F3n" : DENEGACION,
		 "Modificaci\u00F3n datos" : MODIFICACION_DATOS, 
		 "Creaci\u00F3n" : CREACION, 
		 "Finalizaci\u00F3n" : FINALIZACION, 
		 "Cancelaci\u00F3n" : CANCELACION,
		 "Retomar Cancelaci\u00F3n" : RETOMAR_CANCELACION,
		 "Reasignaci\u00F3n" : REASIGNACION,
		 "Asignaci\u00F3n" : ASIGNACION, 
		 "Aprobaci\u00F3n omitida" : APROBACION_OMITIDA,
		 "Denegaci\u00F3n omitida" : DENEGACION_OMITIDA, 
		 "Suspensi\u00F3n" : SUSPENSION, 
		 "Retomar Suspensi\u00F3n" : RETOMAR_SUSPENSION,
		 "Modificaci\u00F3n anexo global" : MODIFICACION_ANEXO_GLOBAL, 
		 "Anexo global de diferente fase" : ANEXO_GLOBAL_DIFERENTE_FASE,
		 "Eliminar anexo de fase actual" : ELIMINAR_ANEXO_FASE_ACTUAL,
		 "Eliminar anexo global" : ELIMINAR_ANEXO_GLOBAL,
		 "Reenviar" : REENVIAR,
		 "Confirmaci\u00F3n horas real incurrido" : CONFIRMAR_INCURRIDO,
		 "Actividad generada autom\u00E1tica" : ACTIVIDAD_GENERADA_AUTOMATICA,
		 "Actividad finalizada ok" : ACTIVIDAD_FINALIZADA_OK,
		 "Actividad finalizada con error" : ACTIVIDAD_FINALIZADA_ERROR,
		 "Actividad generada manualmente" : ACTIVIDAD_GENERADA_MANUAL,
		 "Actualizaci\u00F3n direcci\u00F3n pedido" : ACTUALIZACION_DIRECCION_PEDIDO,
		 "En replanificaci\u00F3n" : EN_REPLANIFICACION
		 ]
	}

	def descripcionTipoEvento() {
		return getTipoEvento(this.tipoEvento)
	}
	
	// TODO: ¿Por qué static?	
	public static getTipoEvento(String tipoDeEvento){
		switch (tipoDeEvento){
			case APROBACION: 
				return  "Aprobaci\u00F3n" 
			case APROBACION_AUTOMATICA:
				return  "Aprobaci\u00F3n autom\u00E1tica"
			case DENEGACION:
				return  "Denegaci\u00F3n"
			case MODIFICACION_DATOS:
				return  "Modificaci\u00F3n datos"
			case CREACION:
				return  "Creaci\u00F3n"
			case FINALIZACION:
				return  "Finalizaci\u00F3n"
			case CANCELACION:
				return  "Cancelaci\u00F3n"
			case RETOMAR_CANCELACION:
				return  "Retomar Cancelaci\u00F3n"
			case REASIGNACION:
				return  "Reasignaci\u00F3n"
			case ASIGNACION:
				return  "Asignaci\u00F3n"
			case APROBACION_OMITIDA:
				return  "Aprobaci\u00F3n omitida"
			case DENEGACION_OMITIDA:
				return  "Denegaci\u00F3n omitida"
			case MODIFICACION_ANEXO_GLOBAL:
				return "Modificaci\u00F3n anexo global"
			case ANEXO_GLOBAL_DIFERENTE_FASE:
				return "Anexo global de diferente fase" 
			case ELIMINAR_ANEXO_FASE_ACTUAL:
				return "Eliminar anexo de fase actual"
			case ELIMINAR_ANEXO_GLOBAL:
				return "Eliminar anexo global"
			case SUSPENSION:
				return "Suspensi\u00F3n"
			case RETOMAR_SUSPENSION:
				return "Retomar Suspensi\u00F3n"
			case REENVIAR:
				return "Reenviar"
			case CONFIRMAR_INCURRIDO:
				return "Confirmaci\u00F3n horas real incurrido"
			case ACTIVIDAD_GENERADA_AUTOMATICA:
				return "Actividad generada autom\u00E1tica"
			case ACTIVIDAD_FINALIZADA_OK:
				return "Actividad finalizada ok"
			case ACTIVIDAD_FINALIZADA_ERROR:
				return "Actividad finalizada con error"
			case ACTIVIDAD_GENERADA_MANUAL:
				return "Actividad generada manualmente"
			case ACTUALIZACION_DIRECCION_PEDIDO:
				return "Actualizaci\u00F3n direcci\u00F3n pedido"
			case EN_REPLANIFICACION:
				return "En replanificaci\u00F3n"
		}
	}
	
	def getRoles() {
		return pedido.getRoles(legajo).sort()
	}
	
	def conFechaDesde() {
		return [APROBACION, APROBACION_OMITIDA, DENEGACION, DENEGACION_OMITIDA].contains(tipoEvento)
	}
	
	boolean esAutomatico() {
		return legajo == null
	}
}
