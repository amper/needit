package ar.com.telecom.pcs.entities

import java.util.Date;

import ar.com.telecom.util.DateUtil;

class DetallePlanificacion {

	Date fechaDesde
	Date fechaHasta
	
	static belongsTo = [ planificacion : PlanificacionEsfuerzo ]

	static constraints = {
		fechaDesde(nullable: true)
		fechaHasta(nullable: true)
	}	
	
	public String toString() {
		return "Detalle planificaci\u00F3n " + DateUtil.toString(fechaDesde) + " a " + DateUtil.toString(fechaHasta)
	}
	
	def puedeCargarHoras() {
		return tieneFechasCargadas()
	}
	
	def tieneFechasCargadas(){
		return (fechaDesde) && (fechaHasta)
	}
	
	def validar(AbstractPedido pedido) {
		if (fechaDesde > fechaHasta) {
			pedido.errors.reject "Planificaci\u00F3n y esfuerzo " + this.toString() + ": la fecha desde es mayor que la fecha hasta"
		}
		
		if (fechaDesde?.year > 8099) {
			pedido.errors.reject "Planificaci\u00F3n y esfuerzo " + this.toString() + ": el año de la fecha desde no puede ser mayor que 9999"
		}

		if (fechaHasta?.year > 8099) {
			pedido.errors.reject "Planificaci\u00F3n y esfuerzo " + this.toString() + ": el año de la fecha hasta no puede ser mayor que 9999"
		}

	}
	
	def blanquear(){
		fechaDesde = null
		fechaHasta = null
	}
	
}
