package ar.com.telecom.pcs.entities

class RolReasignacion {

	RolAplicacion rol
	List rolesAReasignar
	
    static hasMany = [ rolesAReasignar: RolAplicacion ]
	
	boolean reasignaRol(unCodigoRol){
		return rol.codigoRol.equalsIgnoreCase(unCodigoRol)
	}

}
