package ar.com.telecom.pcs.entities

class ECSOtrasPruebas {
	
	TipoPrueba tipoPrueba
	boolean seraConsolidada
	
	static belongsTo = [ecs:ECS]
	
    static constraints = {
    }
	
	static mappings = {
		tipoPrueba fetch: 'join'
	}
	
	public boolean equals(Object otraPrueba) {
		if(!otraPrueba){
			return false
		}
		
		if(!otraPrueba.hashCode()){
			return false
		}
		
		return (this.hashCode()).equals(otraPrueba.hashCode()) 
	}
	
	/**
	 * Concatena hashCode del tipo de prueba + 1(si consolida) o 0 (si no consolida)
	 */
	public int hashCode () {
		return tipoPrueba.hashCode()
	}
	
	public String toString(){
		return tipoPrueba.toString() + " " + seraConsolidada
	}
	
}
