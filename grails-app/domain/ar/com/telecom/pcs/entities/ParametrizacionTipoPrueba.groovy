package ar.com.telecom.pcs.entities

class ParametrizacionTipoPrueba {

	public static String NO_APLICA = "N/A"
	public static String SI_EDITABLE = "S/E" 
	public static String NO_EDITABLE = "N/E"
	public static String SI_GRISADO = "S/G"
	public static String NO_GRISADO = "N/G"
	
	TipoGestion tipoGestion
	TipoPrueba tipoPrueba
	TipoImpacto tipoImpacto
	String estrategiaRealiza
	String estrategiaConsolidacion
	
    static constraints = {
		tipoImpacto(nullable: true)
		estrategiaRealiza(maxSize: 3)
		estrategiaConsolidacion(maxSize: 3)
    }
	
	static mapping = {
		tipoGestion fetch: 'join'
		tipoPrueba fetch: 'join'
		tipoImpacto fetch: 'join'
	}

	def traerDatosRelacionados() {
	}
	
	String toString() {
		return tipoGestion.toString() + " - " + tipoPrueba.toString() + " - " + tipoImpacto + ": [" + estrategiaRealiza + ", " + estrategiaConsolidacion + "]"
	}
	
	def definirConsolida() {
		definir(estrategiaConsolidacion)
	}
	
	def definirRealiza() {
		definir(estrategiaRealiza)
	}
	
	private def definir(estrategia) {
		if (estrategia.equalsIgnoreCase(SI_EDITABLE) || estrategia.equalsIgnoreCase(SI_GRISADO)) {
			return true
		}
		if (estrategia.equalsIgnoreCase(NO_EDITABLE) || estrategia.equalsIgnoreCase(NO_GRISADO)) {
			return false
		}
		return null
	}
	
	boolean puedeEditar() {
		return estrategiaRealiza.equalsIgnoreCase(SI_EDITABLE) || estrategiaRealiza.equalsIgnoreCase(NO_EDITABLE)
	}
	
	boolean puedeEditarConsolida(){
		return estrategiaConsolidacion.equalsIgnoreCase(SI_EDITABLE) || estrategiaConsolidacion.equalsIgnoreCase(NO_EDITABLE)
	}
	
	Boolean valorRealiza() {
		return valor(estrategiaRealiza)
	}
	
	Boolean valorConsolida() {
		return valor(estrategiaConsolidacion)
	}
	
	Boolean valor(campo) {
		if ([SI_GRISADO, SI_EDITABLE].contains(campo)) {
			return true
		}
		if ([NO_GRISADO, NO_EDITABLE].contains(campo)) {
			return false
		}
		return null
	}
}
