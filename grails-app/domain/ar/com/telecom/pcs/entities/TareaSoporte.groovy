package ar.com.telecom.pcs.entities

class TareaSoporte {

	String descripcion
	AreaSoporte areaSoporte
	TipoActividad tipoActividad

    static constraints = {
		descripcion(size: 1..80)
		areaSoporte(nullable: true)
    }

	static mapping = {
		tipoActividad fetch: 'join'
	}
	
	String toString(){
		return descripcion
	}

}
