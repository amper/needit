package ar.com.telecom.pcs.entities

class MacroEstado {

	String descripcion
	boolean aplicaPadre
	//String codigo
	
    static constraints = {
		descripcion(size:1..50)
    }
	
	String toString() {
		descripcion
	}
	
	boolean aplicaSuspension() {
		descripcion.equalsIgnoreCase("Construcción, PI y PAU") 
	}
	
}
