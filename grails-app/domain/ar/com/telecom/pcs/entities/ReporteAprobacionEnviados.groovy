package ar.com.telecom.pcs.entities

class ReporteAprobacionEnviados {

	String codigoDireccion
	Date fecha
	
    static constraints = {
		codigoDireccion(nullable: false, unique: true)
		fecha(nullable: false)
    }
	
	String toString() {
		"${codigoDireccion} - ${fecha}"
	}
	
}
