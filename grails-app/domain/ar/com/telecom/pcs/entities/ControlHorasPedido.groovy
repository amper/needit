package ar.com.telecom.pcs.entities

@Deprecated
class ControlHorasPedido {

	int cantidadHoras
	Periodo periodo
	BigDecimal valorPedido
	
    static constraints = {
		valorPedido(nullable: true)
    }
	
	static belongsTo = [pedido: AbstractPedido]
	
}
