package ar.com.telecom.pcs.entities

class UmbralAprobacionEconomicaDireccion {

	TipoGestion tipoGestion
	String direccion
	BigDecimal rangoDesde
	BigDecimal rangoHasta
	
    static constraints = {
		direccion(size:1..50)
		rangoDesde(nullable: true)
		rangoHasta(nullable: true)
    }
	
	String toString() {
		def result = new StringBuffer()
		result.append(tipoGestion)
		result.append(" - ")
		result.append(direccion)
		result.append(" (")
		if (rangoDesde) {
			result.append(rangoDesde)
		} else {
			result.append("")
		}
		result.append(" -> ")
		if (rangoHasta) {
			result.append(rangoHasta)
		} else {
			result.append("")
		}
		result.append(")")
		return result.toString()
	}
}
