package ar.com.telecom.pcs.entities


class DetalleEstrategiaPrueba {

	TipoPrueba tipoPrueba
	Boolean seRealiza
	Justificacion justificacionNoRealizacion
	Boolean seConsolida

	static constraints  = {
		justificacionNoRealizacion(nullable: true)
		seRealiza(nullable: true)
		seConsolida(nullable: true)
    }
	
	static mapping = {
		tipoPrueba fetch: 'join'
		justificacionNoRealizacion fetch: 'join'
	}

	static belongsTo = [ estrategiaPrueba : EstrategiaPrueba ]
	
	public String toString() {
		return tipoPrueba.toString() + " (" + (seRealiza ? "SI" : "NO") + ")"
	}	
	
	def validar(AbstractPedido pedido) {
		if (!tipoPrueba) {
			pedido.errors.reject "Estrategia de prueba: debe ingresar tipo de prueba"
			return false
		}
		
		if (seRealiza != null && !seRealiza && !justificacionNoRealizacion) {
			pedido.errors.reject "Estrategia de prueba " + tipoPrueba.descripcion + ": si no realiza la prueba debe ingresar justificaci\u00F3n"
		}

		if (seRealiza != null && !seRealiza && seConsolida) {
			pedido.errors.reject "Estrategia de prueba " + tipoPrueba.descripcion + ": si no realiza la prueba no tiene sentido que marque la consolidaci\u00F3n"
		}
		
		pedido.validaPruebaOpcional(tipoPrueba)
		
		return pedido.errors.errorCount == 0
	}
	
	def consolidarRealiza(Boolean otroRealiza) {
		return consolidar(seRealiza, otroRealiza)
	}

	def consolidarConsolida(Boolean otroConsolida) {
		return consolidar(seConsolida, otroConsolida)
	}

	def consolidar(Boolean unValor, Boolean otroValor) {
		if (unValor == null) {
			return otroValor
		} 
		if (otroValor == null) {
			return unValor
		}
		return unValor.or(otroValor)
	}
	
	def ajustarDetallePrueba() {
		if (seRealiza != null && seRealiza.toBoolean() != true) {
			seConsolida = false
		}
	}
}
