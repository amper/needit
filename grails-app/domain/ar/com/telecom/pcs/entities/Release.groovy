package ar.com.telecom.pcs.entities

class Release {

	Sistema sistema
	String descripcion
	
    static constraints = {
		descripcion(size:1..50)
    }
	
	static mapping = {
		sistema fetch: 'join'
	}
	
	String toString() {
		"${descripcion}"
	}
	
}
