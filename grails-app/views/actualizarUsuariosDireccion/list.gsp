<g:if test="${!lista.isEmpty()}">
	<table border="1" cellspacing="0" cellpadding="2" >
		<tr>
			<td align="center"><b>Legajo</b></td>
			<td align="center"><b>Direcci&oacute;n</b></td>
		</tr>
		<g:each in="${lista}" var="item">
			<tr>
				<td>${item.key}</td>
				<td>${item.value}</td>
			</tr>
		</g:each>
	</table>
</g:if>