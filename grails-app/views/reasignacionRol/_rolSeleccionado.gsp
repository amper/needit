
<div class="pCompleto conMargenChico">
	<label class="formLabel sinMargenIzquierdo">Usuario a reasignar:</label>
	
	<div id="legajoAReasignar" class="boxUser" style="width:492px;">
		<g:render template="/templates/userBox" model="['person':pedidoInstance.getLegajo(rol), 'box':'legajoAReasignar']"></g:render>
	</div>
</div>

<g:if test="${rol.muestraDirecciones()}">
	<div class="pCompleto conMargenChico">
				<label class="formLabel sinMargenIzquierdo centrado" style="padding-top:10px;">Direcciones:</label>
				<g:if test="${mapaDirecciones.entrySet().size() > 1}">

					<script type="text/javascript">
					// Solo es combo en el caso de que haya mas de una opcion
					jQuery(document).ready(
							function($) {
								$("#direccion").combobox();
								$("#direccion").change(function() {
									seleccionaDireccion('${pedidoInstance.id}', '${rol.codigoRol}', this.value);
								});
							});
					</script>

					<g:select id="direccion" name="direccion" value="${direccion}"
					from="${mapaDirecciones.entrySet()}"
					optionKey="key" optionValue="value" 
					noSelection="['null': '']" />
				</g:if>
				<g:else>
					<script type="text/javascript">
					// Solo es combo en el caso de que haya mas de una opcion
					jQuery(document).ready(
							function($) {
									seleccionaDireccion('${pedidoInstance.id}', '${rol.codigoRol}', '${mapaDirecciones.entrySet().iterator().next().getKey()}');
							});
					</script>

					<g:hiddenField id="direccion" name="direccion" value="${mapaDirecciones.entrySet().iterator().next().getKey()}" />
					<p class="info sinMargenIzquierdo" style="margin-top:8px !important;">${mapaDirecciones.entrySet().iterator().next().getValue()}</p>				
				</g:else>						
		</div>			
</g:if>


<div class="pDos">
	<label class="formLabel sinMargenIzquierdo">Nuevo asignatario:</label>
	
	<div id="nuevoAsignatario" class="boxUser">
		<g:render template="/templates/userBox" model="['box':'nuevoAsignatario','personsAutocomplete':nuevosAsignatarios, 'person':legajoNuevo]"></g:render>
	</div>
</div>		
