<%@page import="ar.com.telecom.pcs.entities.RolAplicacion"%>
<g:updateToken />

<g:javascript library="boxUser" />

<script type="text/javascript">
	jQuery(document).ready(
		function($) {
			$("#rolAReasignar").combobox();
			$("#rolAReasignar").change(function() {
				seleccionaRol(${pedidoInstance.id}, this.value);
			});
		
		});
</script>
 
<div id="mensajes">
	<g:if test="${flash.message}">
		<div class="message">
			${flash.message}
		</div>
	</g:if>
</div>

<g:hasErrors bean="${pedidoInstance}">
	<div class="errors">
		<g:renderErrors bean="${pedidoInstance}" as="list" />
	</div>
</g:hasErrors>


<div style="overflow:auto;">
	<g:uploadForm name="formReasignacion" id="formReasignacion" useToken="true" action="guardarAnexo" controller="reasignacionRol">
			<g:hiddenField name="pedidoId" value="${pedidoInstance.id}" />
			<div class="pDos">
				<label class="formLabel sinMargenIzquierdo">Proceso de soporte:</label>
				<p class="info sinMargenIzquierdo" style="margin-top:8px !important;">Reasignar Rol</p>
			</div>
			
			<div class="pDos">
				<label class="formLabel sinMargenIzquierdo">Solicitante:</label>
				
				<div id="legajoCoordinador" class="boxUser">
					<g:render template="/templates/userBox" model="['person':usuarioLogueado, 'box':'legajoCoordinador']"></g:render>
				</div>
			</div>		
		
			<div class="pCompleto conMargenChico">
				<label class="formLabel sinMargenIzquierdo centrado">N&uacute;mero:</label>
				<p class="info">${pedidoInstance?.numero}</p>
			</div>
		
			<div class="pCompleto conMargenChico">
				<label class="formLabel sinMargenIzquierdo centrado">T&iacute;tulo:</label>
				<p class="info">${pedidoInstance?.titulo}</p>
			</div>
			
			<div class="pCompleto conMargenChico">
				<label class="formLabel sinMargenIzquierdo centrado">Macroestado:</label>
				<p class="info">${pedidoInstance?.macroEstado}</p>
			</div>
			
			<div class="pCompleto conMargenChico">
				<label class="formLabel sinMargenIzquierdo centrado">Rol a reasignar:</label>
				<g:select id="rolAReasignar" name="rolAReasignar" value="${rolAReasignar?.codigoRol}"
						from="${rolesAReasignar}"
						optionValue="${{RolAplicacion.getDescripcionRolAplicacion(it)}}"
						noSelection="['null': '']" />
			</div>			
			
			<div id="rolSeleccionado">
				<g:if test="${recarga && rolAReasignar}">
					<g:render template="/reasignacionRol/rolSeleccionado" model="[pedidoInstance: pedidoInstance, rol: rolAReasignar, mapaDirecciones:mapaDirecciones, direccion: direccion, legajoNuevo: legajoNuevo, nuevosAsignatarios:nuevosAsignatarios]"/>
				</g:if>
			</div>		
			
			<g:if test="${esAdmin}">
				<div class="pCompleto conMargenChico">
					<label class="formLabel sinMargenIzquierdo centrado">RQ Simplit:</label>
					<g:textArea id='rqsimplit' name="rqsimplit" style="width:198px;padding:2px;"></g:textArea>
				</div>
			</g:if>
			
			<div class="pCompleto conMargenChico">
				<label class="formLabel sinMargenIzquierdo centrado">Comentarios:</label>
				<g:textArea id='comentarioReasignacion' name="comentarioReasignacion" style="width:398px;height:40px;padding:2px;">${comentarioReasignacion}</g:textArea>
			</div>

			<div class="separator"></div>

			<div class="formSeccionButton" style="margin-top: 50px;">
				<button value="Cancelar" class="formSeccionSubmitButtonAnexo" name="Cancelar" onClick="cancelarPopReasignar();">Cancelar</button>
				<button id="btnAceptaReanudacion" value="Aceptar" class="formSeccionSubmitButtonAnexo" name="Aceptar" onClick="reasignar(this.form);">Aceptar</button>
			</div>
	</g:uploadForm>
</div>