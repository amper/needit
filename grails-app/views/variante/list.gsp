
<%@ page import="ar.com.telecom.pcs.entities.Variante" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="layout" content="main" />
        <g:set var="entityName" value="${message(code: 'variante.label', default: 'Variante')}" />
        <title><g:message code="default.list.label" args="[entityName]" /></title>
    </head>
    <body>
        <div class="nav">
            <span class="menuButton"><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></span>
            <span class="menuButton"><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></span>
        </div>
        <div class="body">
            <h1><g:message code="default.list.label" args="[entityName]" /></h1>
            <g:if test="${flash.message}">
            <div class="message">${flash.message}</div>
            </g:if>
            <div class="list">
                <table>
                    <thead>
                        <tr>
                        
                            <g:sortableColumn property="id" title="${message(code: 'variante.id.label', default: 'Id')}" />
                        
                            <g:sortableColumn property="nombreVariante" title="${message(code: 'variante.nombreVariante.label', default: 'Nombre Variante')}" />
                        
                            <g:sortableColumn property="legajoAutorVariante" title="${message(code: 'variante.legajoAutorVariante.label', default: 'Legajo Autor Variante')}" />
                        
                            <g:sortableColumn property="tipoBusqueda" title="${message(code: 'variante.tipoBusqueda.label', default: 'Tipo Busqueda')}" />
                        
                            <g:sortableColumn property="titulo" title="${message(code: 'variante.titulo.label', default: 'Titulo')}" />
                        
                            <th><g:message code="variante.tipoGestion.label" default="Tipo Gestion" /></th>
                        
                        </tr>
                    </thead>
                    <tbody>
                    <g:each in="${varianteInstanceList}" status="i" var="varianteInstance">
                        <tr class="${(i % 2) == 0 ? 'odd' : 'even'}">
                        
                            <td><g:link action="show" id="${varianteInstance.id}">${fieldValue(bean: varianteInstance, field: "id")}</g:link></td>
                        
                            <td>${fieldValue(bean: varianteInstance, field: "nombreVariante")}</td>
                        
                            <td>${fieldValue(bean: varianteInstance, field: "legajoAutorVariante")}</td>
                        
                            <td>${fieldValue(bean: varianteInstance, field: "tipoBusqueda")}</td>
                        
                            <td>${fieldValue(bean: varianteInstance, field: "titulo")}</td>
                        
                            <td>${fieldValue(bean: varianteInstance, field: "tipoGestion")}</td>
                        
                        </tr>
                    </g:each>
                    </tbody>
                </table>
            </div>
            <div class="paginateButtons">
                <g:paginate total="${varianteInstanceTotal}" />
            </div>
        </div>
    </body>
</html>
