
<%@ page import="ar.com.telecom.pcs.entities.Variante" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="layout" content="main" />
        <g:set var="entityName" value="${message(code: 'variante.label', default: 'Variante')}" />
        <title><g:message code="default.show.label" args="[entityName]" /></title>
    </head>
    <body>
        <div class="nav">
            <span class="menuButton"><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></span>
            <span class="menuButton"><g:link class="list" action="list"><g:message code="default.list.label" args="[entityName]" /></g:link></span>
            <span class="menuButton"><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></span>
        </div>
        <div class="body">
            <h1><g:message code="default.show.label" args="[entityName]" /></h1>
            <g:if test="${flash.message}">
            <div class="message">${flash.message}</div>
            </g:if>
            <div class="dialog">
                <table>
                    <tbody>
                    
                        <tr class="prop">
                            <td valign="top" class="name"><g:message code="variante.id.label" default="Id" /></td>
                            
                            <td valign="top" class="value">${fieldValue(bean: varianteInstance, field: "id")}</td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name"><g:message code="variante.nombreVariante.label" default="Nombre Variante" /></td>
                            
                            <td valign="top" class="value">${fieldValue(bean: varianteInstance, field: "nombreVariante")}</td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name"><g:message code="variante.legajoAutorVariante.label" default="Legajo Autor Variante" /></td>
                            
                            <td valign="top" class="value">${fieldValue(bean: varianteInstance, field: "legajoAutorVariante")}</td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name"><g:message code="variante.tipoBusqueda.label" default="Tipo Busqueda" /></td>
                            
                            <td valign="top" class="value">${fieldValue(bean: varianteInstance, field: "tipoBusqueda")}</td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name"><g:message code="variante.titulo.label" default="Titulo" /></td>
                            
                            <td valign="top" class="value">${fieldValue(bean: varianteInstance, field: "titulo")}</td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name"><g:message code="variante.tipoGestion.label" default="Tipo Gestion" /></td>
                            
                            <td valign="top" class="value"><g:link controller="tipoGestion" action="show" id="${varianteInstance?.tipoGestion?.id}">${varianteInstance?.tipoGestion?.encodeAsHTML()}</g:link></td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name"><g:message code="variante.macroestado.label" default="Macroestado" /></td>
                            
                            <td valign="top" class="value"><g:link controller="macroEstado" action="show" id="${varianteInstance?.macroestado?.id}">${varianteInstance?.macroestado?.encodeAsHTML()}</g:link></td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name"><g:message code="variante.criterioSistemaImpactado.label" default="Criterio Sistema Impactado" /></td>
                            
                            <td valign="top" class="value">${fieldValue(bean: varianteInstance, field: "criterioSistemaImpactado")}</td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name"><g:message code="variante.valorSistemaImpactado.label" default="Valor Sistema Impactado" /></td>
                            
                            <td valign="top" class="value">${fieldValue(bean: varianteInstance, field: "valorSistemaImpactado")}</td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name"><g:message code="variante.criterioAreaSoporte.label" default="Criterio Area Soporte" /></td>
                            
                            <td valign="top" class="value">${fieldValue(bean: varianteInstance, field: "criterioAreaSoporte")}</td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name"><g:message code="variante.valorAreaSoporte.label" default="Valor Area Soporte" /></td>
                            
                            <td valign="top" class="value">${fieldValue(bean: varianteInstance, field: "valorAreaSoporte")}</td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name"><g:message code="variante.estructuraRequirente.label" default="Estructura Requirente" /></td>
                            
                            <td valign="top" class="value">${fieldValue(bean: varianteInstance, field: "estructuraRequirente")}</td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name"><g:message code="variante.usuario.label" default="Usuario" /></td>
                            
                            <td valign="top" class="value">${fieldValue(bean: varianteInstance, field: "usuario")}</td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name"><g:message code="variante.criterioSistemaSugerido.label" default="Criterio Sistema Sugerido" /></td>
                            
                            <td valign="top" class="value">${fieldValue(bean: varianteInstance, field: "criterioSistemaSugerido")}</td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name"><g:message code="variante.valorSistemaSugerido.label" default="Valor Sistema Sugerido" /></td>
                            
                            <td valign="top" class="value">${fieldValue(bean: varianteInstance, field: "valorSistemaSugerido")}</td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name"><g:message code="variante.criterioPrioridad.label" default="Criterio Prioridad" /></td>
                            
                            <td valign="top" class="value">${fieldValue(bean: varianteInstance, field: "criterioPrioridad")}</td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name"><g:message code="variante.valorPrioridad.label" default="Valor Prioridad" /></td>
                            
                            <td valign="top" class="value">${fieldValue(bean: varianteInstance, field: "valorPrioridad")}</td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name"><g:message code="variante.tipoReferencia.label" default="Tipo Referencia" /></td>
                            
                            <td valign="top" class="value"><g:link controller="tipoReferencia" action="show" id="${varianteInstance?.tipoReferencia?.id}">${varianteInstance?.tipoReferencia?.encodeAsHTML()}</g:link></td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name"><g:message code="variante.criterioTipoReferencia.label" default="Criterio Tipo Referencia" /></td>
                            
                            <td valign="top" class="value">${fieldValue(bean: varianteInstance, field: "criterioTipoReferencia")}</td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name"><g:message code="variante.valorTipoReferencia.label" default="Valor Tipo Referencia" /></td>
                            
                            <td valign="top" class="value">${fieldValue(bean: varianteInstance, field: "valorTipoReferencia")}</td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name"><g:message code="variante.criterioSolman.label" default="Criterio Solman" /></td>
                            
                            <td valign="top" class="value">${fieldValue(bean: varianteInstance, field: "criterioSolman")}</td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name"><g:message code="variante.valorSolman.label" default="Valor Solman" /></td>
                            
                            <td valign="top" class="value">${fieldValue(bean: varianteInstance, field: "valorSolman")}</td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name"><g:message code="variante.criterioRelease.label" default="Criterio Release" /></td>
                            
                            <td valign="top" class="value">${fieldValue(bean: varianteInstance, field: "criterioRelease")}</td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name"><g:message code="variante.valorRelease.label" default="Valor Release" /></td>
                            
                            <td valign="top" class="value">${fieldValue(bean: varianteInstance, field: "valorRelease")}</td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name"><g:message code="variante.criterioSimplitPaP.label" default="Criterio Simplit Pa P" /></td>
                            
                            <td valign="top" class="value">${fieldValue(bean: varianteInstance, field: "criterioSimplitPaP")}</td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name"><g:message code="variante.valorSimplitPaP.label" default="Valor Simplit Pa P" /></td>
                            
                            <td valign="top" class="value">${fieldValue(bean: varianteInstance, field: "valorSimplitPaP")}</td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name"><g:message code="variante.criterioSimplitPAU.label" default="Criterio Simplit PAU" /></td>
                            
                            <td valign="top" class="value">${fieldValue(bean: varianteInstance, field: "criterioSimplitPAU")}</td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name"><g:message code="variante.valorSimplitPAU.label" default="Valor Simplit PAU" /></td>
                            
                            <td valign="top" class="value">${fieldValue(bean: varianteInstance, field: "valorSimplitPAU")}</td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name"><g:message code="variante.fases.label" default="Fases" /></td>
                            
                            <td valign="top" style="text-align: left;" class="value">
                                <ul>
                                <g:each in="${varianteInstance.fases}" var="f">
                                    <li><g:link controller="fase" action="show" id="${f.id}">${f?.encodeAsHTML()}</g:link></li>
                                </g:each>
                                </ul>
                            </td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name"><g:message code="variante.gruposABuscar.label" default="Grupos AB uscar" /></td>
                            
                            <td valign="top" class="value">${fieldValue(bean: varianteInstance, field: "gruposABuscar")}</td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name"><g:message code="variante.rolesABuscar.label" default="Roles AB uscar" /></td>
                            
                            <td valign="top" class="value">${fieldValue(bean: varianteInstance, field: "rolesABuscar")}</td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name"><g:message code="variante.tiposImpacto.label" default="Tipos Impacto" /></td>
                            
                            <td valign="top" style="text-align: left;" class="value">
                                <ul>
                                <g:each in="${varianteInstance.tiposImpacto}" var="t">
                                    <li><g:link controller="tipoImpacto" action="show" id="${t.id}">${t?.encodeAsHTML()}</g:link></li>
                                </g:each>
                                </ul>
                            </td>
                            
                        </tr>
                    
                    </tbody>
                </table>
            </div>
            <div class="buttons">
                <g:form>
                    <g:hiddenField name="id" value="${varianteInstance?.id}" />
                    <span class="button"><g:actionSubmit class="edit" action="edit" value="${message(code: 'default.button.edit.label', default: 'Edit')}" /></span>
                    <span class="button"><g:actionSubmit class="delete" action="delete" value="${message(code: 'default.button.delete.label', default: 'Delete')}" onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');" /></span>
                </g:form>
            </div>
        </div>
    </body>
</html>
