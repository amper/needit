

<%@ page import="ar.com.telecom.pcs.entities.Variante" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="layout" content="main" />
        <g:set var="entityName" value="${message(code: 'variante.label', default: 'Variante')}" />
        <title><g:message code="default.edit.label" args="[entityName]" /></title>
    </head>
    <body>
        <div class="nav">
            <span class="menuButton"><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></span>
            <span class="menuButton"><g:link class="list" action="list"><g:message code="default.list.label" args="[entityName]" /></g:link></span>
            <span class="menuButton"><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></span>
        </div>
        <div class="body">
            <h1><g:message code="default.edit.label" args="[entityName]" /></h1>
            <g:if test="${flash.message}">
            <div class="message">${flash.message}</div>
            </g:if>
            <g:hasErrors bean="${varianteInstance}">
            <div class="errors">
                <g:renderErrors bean="${varianteInstance}" as="list" />
            </div>
            </g:hasErrors>
            <g:form method="post" >
                <g:hiddenField name="id" value="${varianteInstance?.id}" />
                <g:hiddenField name="version" value="${varianteInstance?.version}" />
                <div class="dialog">
                    <table>
                        <tbody>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                  <label for="nombreVariante"><g:message code="variante.nombreVariante.label" default="Nombre Variante" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: varianteInstance, field: 'nombreVariante', 'errors')}">
                                    <g:textField name="nombreVariante" maxlength="30" value="${varianteInstance?.nombreVariante}" />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                  <label for="legajoAutorVariante"><g:message code="variante.legajoAutorVariante.label" default="Legajo Autor Variante" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: varianteInstance, field: 'legajoAutorVariante', 'errors')}">
                                    <g:textField name="legajoAutorVariante" maxlength="10" value="${varianteInstance?.legajoAutorVariante}" />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                  <label for="tipoBusqueda"><g:message code="variante.tipoBusqueda.label" default="Tipo Busqueda" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: varianteInstance, field: 'tipoBusqueda', 'errors')}">
                                    <g:textField name="tipoBusqueda" maxlength="2" value="${varianteInstance?.tipoBusqueda}" />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                  <label for="titulo"><g:message code="variante.titulo.label" default="Titulo" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: varianteInstance, field: 'titulo', 'errors')}">
                                    <g:textField name="titulo" maxlength="200" value="${varianteInstance?.titulo}" />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                  <label for="tipoGestion"><g:message code="variante.tipoGestion.label" default="Tipo Gestion" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: varianteInstance, field: 'tipoGestion', 'errors')}">
                                    <g:select name="tipoGestion.id" from="${ar.com.telecom.pcs.entities.TipoGestion.list()}" optionKey="id" value="${varianteInstance?.tipoGestion?.id}" noSelection="['null': '']" />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                  <label for="macroestado"><g:message code="variante.macroestado.label" default="Macroestado" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: varianteInstance, field: 'macroestado', 'errors')}">
                                    <g:select name="macroestado.id" from="${ar.com.telecom.pcs.entities.MacroEstado.list()}" optionKey="id" value="${varianteInstance?.macroestado?.id}" noSelection="['null': '']" />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                  <label for="criterioSistemaImpactado"><g:message code="variante.criterioSistemaImpactado.label" default="Criterio Sistema Impactado" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: varianteInstance, field: 'criterioSistemaImpactado', 'errors')}">
                                    <g:textField name="criterioSistemaImpactado" maxlength="20" value="${varianteInstance?.criterioSistemaImpactado}" />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                  <label for="valorSistemaImpactado"><g:message code="variante.valorSistemaImpactado.label" default="Valor Sistema Impactado" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: varianteInstance, field: 'valorSistemaImpactado', 'errors')}">
                                    <g:textField name="valorSistemaImpactado" maxlength="50" value="${varianteInstance?.valorSistemaImpactado}" />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                  <label for="criterioAreaSoporte"><g:message code="variante.criterioAreaSoporte.label" default="Criterio Area Soporte" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: varianteInstance, field: 'criterioAreaSoporte', 'errors')}">
                                    <g:textField name="criterioAreaSoporte" maxlength="20" value="${varianteInstance?.criterioAreaSoporte}" />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                  <label for="valorAreaSoporte"><g:message code="variante.valorAreaSoporte.label" default="Valor Area Soporte" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: varianteInstance, field: 'valorAreaSoporte', 'errors')}">
                                    <g:textField name="valorAreaSoporte" maxlength="50" value="${varianteInstance?.valorAreaSoporte}" />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                  <label for="estructuraRequirente"><g:message code="variante.estructuraRequirente.label" default="Estructura Requirente" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: varianteInstance, field: 'estructuraRequirente', 'errors')}">
                                    <g:textField name="estructuraRequirente" maxlength="20" value="${varianteInstance?.estructuraRequirente}" />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                  <label for="usuario"><g:message code="variante.usuario.label" default="Usuario" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: varianteInstance, field: 'usuario', 'errors')}">
                                    <g:textField name="usuario" maxlength="20" value="${varianteInstance?.usuario}" />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                  <label for="criterioSistemaSugerido"><g:message code="variante.criterioSistemaSugerido.label" default="Criterio Sistema Sugerido" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: varianteInstance, field: 'criterioSistemaSugerido', 'errors')}">
                                    <g:textField name="criterioSistemaSugerido" maxlength="20" value="${varianteInstance?.criterioSistemaSugerido}" />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                  <label for="valorSistemaSugerido"><g:message code="variante.valorSistemaSugerido.label" default="Valor Sistema Sugerido" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: varianteInstance, field: 'valorSistemaSugerido', 'errors')}">
                                    <g:textField name="valorSistemaSugerido" maxlength="50" value="${varianteInstance?.valorSistemaSugerido}" />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                  <label for="criterioPrioridad"><g:message code="variante.criterioPrioridad.label" default="Criterio Prioridad" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: varianteInstance, field: 'criterioPrioridad', 'errors')}">
                                    <g:textField name="criterioPrioridad" maxlength="20" value="${varianteInstance?.criterioPrioridad}" />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                  <label for="valorPrioridad"><g:message code="variante.valorPrioridad.label" default="Valor Prioridad" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: varianteInstance, field: 'valorPrioridad', 'errors')}">
                                    <g:textField name="valorPrioridad" maxlength="50" value="${varianteInstance?.valorPrioridad}" />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                  <label for="tipoReferencia"><g:message code="variante.tipoReferencia.label" default="Tipo Referencia" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: varianteInstance, field: 'tipoReferencia', 'errors')}">
                                    <g:select name="tipoReferencia.id" from="${ar.com.telecom.pcs.entities.TipoReferencia.list()}" optionKey="id" value="${varianteInstance?.tipoReferencia?.id}" noSelection="['null': '']" />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                  <label for="criterioTipoReferencia"><g:message code="variante.criterioTipoReferencia.label" default="Criterio Tipo Referencia" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: varianteInstance, field: 'criterioTipoReferencia', 'errors')}">
                                    <g:textField name="criterioTipoReferencia" maxlength="20" value="${varianteInstance?.criterioTipoReferencia}" />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                  <label for="valorTipoReferencia"><g:message code="variante.valorTipoReferencia.label" default="Valor Tipo Referencia" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: varianteInstance, field: 'valorTipoReferencia', 'errors')}">
                                    <g:textField name="valorTipoReferencia" maxlength="50" value="${varianteInstance?.valorTipoReferencia}" />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                  <label for="criterioSolman"><g:message code="variante.criterioSolman.label" default="Criterio Solman" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: varianteInstance, field: 'criterioSolman', 'errors')}">
                                    <g:textField name="criterioSolman" maxlength="20" value="${varianteInstance?.criterioSolman}" />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                  <label for="valorSolman"><g:message code="variante.valorSolman.label" default="Valor Solman" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: varianteInstance, field: 'valorSolman', 'errors')}">
                                    <g:textField name="valorSolman" maxlength="50" value="${varianteInstance?.valorSolman}" />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                  <label for="criterioRelease"><g:message code="variante.criterioRelease.label" default="Criterio Release" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: varianteInstance, field: 'criterioRelease', 'errors')}">
                                    <g:textField name="criterioRelease" maxlength="20" value="${varianteInstance?.criterioRelease}" />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                  <label for="valorRelease"><g:message code="variante.valorRelease.label" default="Valor Release" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: varianteInstance, field: 'valorRelease', 'errors')}">
                                    <g:textField name="valorRelease" maxlength="50" value="${varianteInstance?.valorRelease}" />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                  <label for="criterioSimplitPaP"><g:message code="variante.criterioSimplitPaP.label" default="Criterio Simplit Pa P" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: varianteInstance, field: 'criterioSimplitPaP', 'errors')}">
                                    <g:textField name="criterioSimplitPaP" maxlength="20" value="${varianteInstance?.criterioSimplitPaP}" />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                  <label for="valorSimplitPaP"><g:message code="variante.valorSimplitPaP.label" default="Valor Simplit Pa P" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: varianteInstance, field: 'valorSimplitPaP', 'errors')}">
                                    <g:textField name="valorSimplitPaP" maxlength="50" value="${varianteInstance?.valorSimplitPaP}" />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                  <label for="criterioSimplitPAU"><g:message code="variante.criterioSimplitPAU.label" default="Criterio Simplit PAU" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: varianteInstance, field: 'criterioSimplitPAU', 'errors')}">
                                    <g:textField name="criterioSimplitPAU" maxlength="20" value="${varianteInstance?.criterioSimplitPAU}" />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                  <label for="valorSimplitPAU"><g:message code="variante.valorSimplitPAU.label" default="Valor Simplit PAU" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: varianteInstance, field: 'valorSimplitPAU', 'errors')}">
                                    <g:textField name="valorSimplitPAU" maxlength="50" value="${varianteInstance?.valorSimplitPAU}" />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                  <label for="fases"><g:message code="variante.fases.label" default="Fases" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: varianteInstance, field: 'fases', 'errors')}">
                                    <g:select name="fases" from="${ar.com.telecom.pcs.entities.Fase.list()}" multiple="yes" optionKey="id" size="5" value="${varianteInstance?.fases*.id}" />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                  <label for="gruposABuscar"><g:message code="variante.gruposABuscar.label" default="Grupos AB uscar" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: varianteInstance, field: 'gruposABuscar', 'errors')}">
                                    
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                  <label for="rolesABuscar"><g:message code="variante.rolesABuscar.label" default="Roles AB uscar" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: varianteInstance, field: 'rolesABuscar', 'errors')}">
                                    
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                  <label for="tiposImpacto"><g:message code="variante.tiposImpacto.label" default="Tipos Impacto" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: varianteInstance, field: 'tiposImpacto', 'errors')}">
                                    <g:select name="tiposImpacto" from="${ar.com.telecom.pcs.entities.TipoImpacto.list()}" multiple="yes" optionKey="id" size="5" value="${varianteInstance?.tiposImpacto*.id}" />
                                </td>
                            </tr>
                        
                        </tbody>
                    </table>
                </div>
                <div class="buttons">
                    <span class="button"><g:actionSubmit class="save" action="update" value="${message(code: 'default.button.update.label', default: 'Update')}" /></span>
                    <span class="button"><g:actionSubmit class="delete" action="delete" value="${message(code: 'default.button.delete.label', default: 'Delete')}" onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');" /></span>
                </div>
            </g:form>
        </div>
    </body>
</html>
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                   