

<%@ page import="ar.com.telecom.pcs.entities.MailAudit" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="layout" content="main" />
        <g:set var="entityName" value="${message(code: 'mailAudit.label', default: 'MailAudit')}" />
        <title><g:message code="default.create.label" args="[entityName]" /></title>
    </head>
    <body>
        <div class="nav">
            <span class="menuButton"><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></span>
            <span class="menuButton"><g:link class="list" action="list"><g:message code="default.list.label" args="[entityName]" /></g:link></span>
        </div>
        <div class="body">
            <h1><g:message code="default.create.label" args="[entityName]" /></h1>
            <g:if test="${flash.message}">
            <div class="message">${flash.message}</div>
            </g:if>
            <g:hasErrors bean="${mailAuditInstance}">
            <div class="errors">
                <g:renderErrors bean="${mailAuditInstance}" as="list" />
            </div>
            </g:hasErrors>
            <g:form action="save" >
                <div class="dialog">
                    <table>
                        <tbody>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="asunto"><g:message code="mailAudit.asunto.label" default="Asunto" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: mailAuditInstance, field: 'asunto', 'errors')}">
                                    <g:textField name="asunto" maxlength="250" value="${mailAuditInstance?.asunto}" />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="mails"><g:message code="mailAudit.mails.label" default="Mails" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: mailAuditInstance, field: 'mails', 'errors')}">
                                    <g:textField name="mails" maxlength="250" value="${mailAuditInstance?.mails}" />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="texto"><g:message code="mailAudit.texto.label" default="Texto" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: mailAuditInstance, field: 'texto', 'errors')}">
                                    <g:textField name="texto" value="${mailAuditInstance?.texto}" />
                                </td>
                            </tr>
                        
                        </tbody>
                    </table>
                </div>
                <div class="buttons">
                    <span class="button"><g:submitButton name="create" class="save" value="${message(code: 'default.button.create.label', default: 'Create')}" /></span>
                </div>
            </g:form>
        </div>
    </body>
</html>
