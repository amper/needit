
<%@ page import="ar.com.telecom.pcs.entities.MailAudit" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="layout" content="main" />
        <g:set var="entityName" value="${message(code: 'mailAudit.label', default: 'MailAudit')}" />
        <title><g:message code="default.list.label" args="[entityName]" /></title>
    </head>
    <body>
        <div class="nav">
            <span class="menuButton"><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></span>
            <span class="menuButton"><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></span>
        </div>
        <div class="body">
            <h1><g:message code="default.list.label" args="[entityName]" /></h1>
            <g:if test="${flash.message}">
            <div class="message">${flash.message}</div>
            </g:if>
            <div class="list">
                <table>
                    <thead>
                        <tr>
                        
                            <g:sortableColumn property="id" title="${message(code: 'mailAudit.id.label', default: 'Id')}" />
                        
                            <g:sortableColumn property="asunto" title="${message(code: 'mailAudit.asunto.label', default: 'Asunto')}" />
                        
                            <g:sortableColumn property="mails" title="${message(code: 'mailAudit.mails.label', default: 'Mails')}" />
                        
                            <g:sortableColumn property="texto" title="${message(code: 'mailAudit.texto.label', default: 'Texto')}" />
                        
                        </tr>
                    </thead>
                    <tbody>
                    <g:each in="${mailAuditInstanceList}" status="i" var="mailAuditInstance">
                        <tr class="${(i % 2) == 0 ? 'odd' : 'even'}">
                        
                            <td><g:link action="show" id="${mailAuditInstance.id}">${fieldValue(bean: mailAuditInstance, field: "id")}</g:link></td>
                        
                            <td>${fieldValue(bean: mailAuditInstance, field: "asunto")}</td>
                        
                            <td>${fieldValue(bean: mailAuditInstance, field: "mails")}</td>
                        
                            <td>${fieldValue(bean: mailAuditInstance, field: "texto")}</td>
                        
                        </tr>
                    </g:each>
                    </tbody>
                </table>
            </div>
            <div class="paginateButtons">
                <g:paginate total="${mailAuditInstanceTotal}" />
            </div>
        </div>
    </body>
</html>
