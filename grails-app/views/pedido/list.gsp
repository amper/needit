
<%@ page import="ar.com.telecom.pcs.entities.Pedido" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="layout" content="main" />
        <g:set var="entityName" value="${message(code: 'pedido.label', default: 'Pedido')}" />
        <title><g:message code="default.list.label" args="[entityName]" /></title>
    </head>
    <body>
        <div class="nav">
            <span class="menuButton"><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></span>
            <span class="menuButton"><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></span>
        </div>
        <div class="body">
            <h1><g:message code="default.list.label" args="[entityName]" /></h1>
            <g:if test="${flash.message}">
            <div class="message">${flash.message}</div>
            </g:if>
            <div class="list">
                <table>
                    <thead>
                        <tr>
                        
                            <g:sortableColumn property="id" title="${message(code: 'pedido.id.label', default: 'Id')}" />
                        
                            <g:sortableColumn property="titulo" title="${message(code: 'pedido.titulo.label', default: 'Titulo')}" />
                        
                            <g:sortableColumn property="legajoUsuarioCreador" title="${message(code: 'pedido.legajoUsuarioCreador.label', default: 'Legajo Usuario Creador')}" />
                        
                            <g:sortableColumn property="fechaUltimaModificacion" title="${message(code: 'pedido.fechaUltimaModificacion.label', default: 'Fecha Ultima Modificacion')}" />
                        
                            <g:sortableColumn property="fechaCargaPedido" title="${message(code: 'pedido.fechaCargaPedido.label', default: 'Fecha Carga Pedido')}" />
                        
                            <g:sortableColumn property="fechaCancelacion" title="${message(code: 'pedido.fechaCancelacion.label', default: 'Fecha Cancelacion')}" />
                        
                        </tr>
                    </thead>
                    <tbody>
                    <g:each in="${pedidoInstanceList}" status="i" var="pedidoInstance">
                        <tr class="${(i % 2) == 0 ? 'odd' : 'even'}">
                        
                            <td><g:link action="show" id="${pedidoInstance.id}">${fieldValue(bean: pedidoInstance, field: "id")}</g:link></td>
                        
                            <td>${fieldValue(bean: pedidoInstance, field: "titulo")}</td>
                        
                            <td>${fieldValue(bean: pedidoInstance, field: "legajoUsuarioCreador")}</td>
                        
                            <td><g:formatDate date="${pedidoInstance.fechaUltimaModificacion}" /></td>
                        
                            <td><g:formatDate date="${pedidoInstance.fechaCargaPedido}" /></td>
                        
                            <td><g:formatDate date="${pedidoInstance.fechaCancelacion}" /></td>
                        
                        </tr>
                    </g:each>
                    </tbody>
                </table>
            </div>
            <div class="paginateButtons">
                <g:paginate total="${pedidoInstanceTotal}" />
            </div>
        </div>
    </body>
</html>
