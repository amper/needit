
<%@ page import="ar.com.telecom.pcs.entities.Pedido" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="layout" content="main" />
        <g:set var="entityName" value="${message(code: 'pedido.label', default: 'Pedido')}" />
        <title><g:message code="default.show.label" args="[entityName]" /></title>
    </head>
    <body>
        <div class="nav">
            <span class="menuButton"><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></span>
            <span class="menuButton"><g:link class="list" action="list"><g:message code="default.list.label" args="[entityName]" /></g:link></span>
            <span class="menuButton"><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></span>
        </div>
        <div class="body">
            <h1><g:message code="default.show.label" args="[entityName]" /></h1>
            <g:if test="${flash.message}">
            <div class="message">${flash.message}</div>
            </g:if>
            <div class="dialog">
                <table>
                    <tbody>
                    
                        <tr class="prop">
                            <td valign="top" class="name"><g:message code="pedido.id.label" default="Id" /></td>
                            
                            <td valign="top" class="value">${fieldValue(bean: pedidoInstance, field: "id")}</td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name"><g:message code="pedido.titulo.label" default="Titulo" /></td>
                            
                            <td valign="top" class="value">${fieldValue(bean: pedidoInstance, field: "titulo")}</td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name"><g:message code="pedido.legajoUsuarioCreador.label" default="Legajo Usuario Creador" /></td>
                            
                            <td valign="top" class="value">${fieldValue(bean: pedidoInstance, field: "legajoUsuarioCreador")}</td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name"><g:message code="pedido.fechaUltimaModificacion.label" default="Fecha Ultima Modificacion" /></td>
                            
                            <td valign="top" class="value"><g:formatDate date="${pedidoInstance?.fechaUltimaModificacion}" /></td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name"><g:message code="pedido.fechaCargaPedido.label" default="Fecha Carga Pedido" /></td>
                            
                            <td valign="top" class="value"><g:formatDate date="${pedidoInstance?.fechaCargaPedido}" /></td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name"><g:message code="pedido.fechaCancelacion.label" default="Fecha Cancelacion" /></td>
                            
                            <td valign="top" class="value"><g:formatDate date="${pedidoInstance?.fechaCancelacion}" /></td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name"><g:message code="pedido.fechaSuspension.label" default="Fecha Suspension" /></td>
                            
                            <td valign="top" class="value"><g:formatDate date="${pedidoInstance?.fechaSuspension}" /></td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name"><g:message code="pedido.motivoCancelacion.label" default="Motivo Cancelacion" /></td>
                            
                            <td valign="top" class="value">${fieldValue(bean: pedidoInstance, field: "motivoCancelacion")}</td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name"><g:message code="pedido.motivoSuspension.label" default="Motivo Suspension" /></td>
                            
                            <td valign="top" class="value">${fieldValue(bean: pedidoInstance, field: "motivoSuspension")}</td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name"><g:message code="pedido.descripcion.label" default="Descripcion" /></td>
                            
                            <td valign="top" class="value">${fieldValue(bean: pedidoInstance, field: "descripcion")}</td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name"><g:message code="pedido.alcance.label" default="Alcance" /></td>
                            
                            <td valign="top" class="value">${fieldValue(bean: pedidoInstance, field: "alcance")}</td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name"><g:message code="pedido.objetivo.label" default="Objetivo" /></td>
                            
                            <td valign="top" class="value">${fieldValue(bean: pedidoInstance, field: "objetivo")}</td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name"><g:message code="pedido.tipoPedido.label" default="Tipo Pedido" /></td>
                            
                            <td valign="top" class="value"><g:link controller="tipoPedido" action="show" id="${pedidoInstance?.tipoPedido?.id}">${pedidoInstance?.tipoPedido?.encodeAsHTML()}</g:link></td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name"><g:message code="pedido.fueraAlcance.label" default="Fuera Alcance" /></td>
                            
                            <td valign="top" class="value">${fieldValue(bean: pedidoInstance, field: "fueraAlcance")}</td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name"><g:message code="pedido.infoAdicional.label" default="Info Adicional" /></td>
                            
                            <td valign="top" class="value">${fieldValue(bean: pedidoInstance, field: "infoAdicional")}</td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name"><g:message code="pedido.fechaDeseadaImplementacion.label" default="Fecha Deseada Implementacion" /></td>
                            
                            <td valign="top" class="value"><g:formatDate date="${pedidoInstance?.fechaDeseadaImplementacion}" /></td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name"><g:message code="pedido.sistemaSugerido.label" default="Sistema Sugerido" /></td>
                            
                            <td valign="top" class="value"><g:link controller="sistema" action="show" id="${pedidoInstance?.sistemaSugerido?.id}">${pedidoInstance?.sistemaSugerido?.encodeAsHTML()}</g:link></td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name"><g:message code="pedido.beneficiosEsperados.label" default="Beneficios Esperados" /></td>
                            
                            <td valign="top" class="value">${fieldValue(bean: pedidoInstance, field: "beneficiosEsperados")}</td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name"><g:message code="pedido.riesgosObservables.label" default="Riesgos Observables" /></td>
                            
                            <td valign="top" class="value">${fieldValue(bean: pedidoInstance, field: "riesgosObservables")}</td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name"><g:message code="pedido.tipoReferencia.label" default="Tipo Referencia" /></td>
                            
                            <td valign="top" class="value"><g:link controller="tipoReferencia" action="show" id="${pedidoInstance?.tipoReferencia?.id}">${pedidoInstance?.tipoReferencia?.encodeAsHTML()}</g:link></td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name"><g:message code="pedido.referenciaOrigen.label" default="Referencia Origen" /></td>
                            
                            <td valign="top" class="value">${fieldValue(bean: pedidoInstance, field: "referenciaOrigen")}</td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name"><g:message code="pedido.tipoGestion.label" default="Tipo Gestion" /></td>
                            
                            <td valign="top" class="value"><g:link controller="tipoGestion" action="show" id="${pedidoInstance?.tipoGestion?.id}">${pedidoInstance?.tipoGestion?.encodeAsHTML()}</g:link></td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name"><g:message code="pedido.prioridad.label" default="Prioridad" /></td>
                            
                            <td valign="top" class="value"><g:link controller="prioridad" action="show" id="${pedidoInstance?.prioridad?.id}">${pedidoInstance?.prioridad?.encodeAsHTML()}</g:link></td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name"><g:message code="pedido.fechaVencimientoPlanificacion.label" default="Fecha Vencimiento Planificacion" /></td>
                            
                            <td valign="top" class="value"><g:formatDate date="${pedidoInstance?.fechaVencimientoPlanificacion}" /></td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name"><g:message code="pedido.comentarios.label" default="Comentarios" /></td>
                            
                            <td valign="top" class="value">${fieldValue(bean: pedidoInstance, field: "comentarios")}</td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name"><g:message code="pedido.valorHora.label" default="Valor Hora" /></td>
                            
                            <td valign="top" class="value">${fieldValue(bean: pedidoInstance, field: "valorHora")}</td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name"><g:message code="pedido.legajoInterlocutorUsuario.label" default="Legajo Interlocutor Usuario" /></td>
                            
                            <td valign="top" class="value">${fieldValue(bean: pedidoInstance, field: "legajoInterlocutorUsuario")}</td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name"><g:message code="pedido.legajoGerenteUsuario.label" default="Legajo Gerente Usuario" /></td>
                            
                            <td valign="top" class="value">${fieldValue(bean: pedidoInstance, field: "legajoGerenteUsuario")}</td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name"><g:message code="pedido.codigoDireccionGerencia.label" default="Codigo Direccion Gerencia" /></td>
                            
                            <td valign="top" class="value">${fieldValue(bean: pedidoInstance, field: "codigoDireccionGerencia")}</td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name"><g:message code="pedido.descripcionDireccionGerencia.label" default="Descripcion Direccion Gerencia" /></td>
                            
                            <td valign="top" class="value">${fieldValue(bean: pedidoInstance, field: "descripcionDireccionGerencia")}</td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name"><g:message code="pedido.grupoGestionDemanda.label" default="Grupo Gestion Demanda" /></td>
                            
                            <td valign="top" class="value">${fieldValue(bean: pedidoInstance, field: "grupoGestionDemanda")}</td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name"><g:message code="pedido.legajoUsuarioGestionDemanda.label" default="Legajo Usuario Gestion Demanda" /></td>
                            
                            <td valign="top" class="value">${fieldValue(bean: pedidoInstance, field: "legajoUsuarioGestionDemanda")}</td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name"><g:message code="pedido.legajoCoordinadorCambio.label" default="Legajo Coordinador Cambio" /></td>
                            
                            <td valign="top" class="value">${fieldValue(bean: pedidoInstance, field: "legajoCoordinadorCambio")}</td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name"><g:message code="pedido.legajoUsuarioAprobadorEconomico.label" default="Legajo Usuario Aprobador Economico" /></td>
                            
                            <td valign="top" class="value">${fieldValue(bean: pedidoInstance, field: "legajoUsuarioAprobadorEconomico")}</td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name"><g:message code="pedido.fechaFinalizacionIngresoPedido.label" default="Fecha Finalizacion Ingreso Pedido" /></td>
                            
                            <td valign="top" class="value"><g:formatDate date="${pedidoInstance?.fechaFinalizacionIngresoPedido}" /></td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name"><g:message code="pedido.fechaAprobacionPedidoGU.label" default="Fecha Aprobacion Pedido GU" /></td>
                            
                            <td valign="top" class="value"><g:formatDate date="${pedidoInstance?.fechaAprobacionPedidoGU}" /></td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name"><g:message code="pedido.fechaValidacionIU.label" default="Fecha Validacion IU" /></td>
                            
                            <td valign="top" class="value"><g:formatDate date="${pedidoInstance?.fechaValidacionIU}" /></td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name"><g:message code="pedido.fechaEvaluacionPedidoFGD.label" default="Fecha Evaluacion Pedido FGD" /></td>
                            
                            <td valign="top" class="value"><g:formatDate date="${pedidoInstance?.fechaEvaluacionPedidoFGD}" /></td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name"><g:message code="pedido.fechaAprobacionEspecificacionPedidoCC.label" default="Fecha Aprobacion Especificacion Pedido CC" /></td>
                            
                            <td valign="top" class="value"><g:formatDate date="${pedidoInstance?.fechaAprobacionEspecificacionPedidoCC}" /></td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name"><g:message code="pedido.fechaAprobacionEspecificacionImpacto.label" default="Fecha Aprobacion Especificacion Impacto" /></td>
                            
                            <td valign="top" class="value"><g:formatDate date="${pedidoInstance?.fechaAprobacionEspecificacionImpacto}" /></td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name"><g:message code="pedido.fechaValidacionImpacto.label" default="Fecha Validacion Impacto" /></td>
                            
                            <td valign="top" class="value"><g:formatDate date="${pedidoInstance?.fechaValidacionImpacto}" /></td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name"><g:message code="pedido.fechaAprobacionEconomica.label" default="Fecha Aprobacion Economica" /></td>
                            
                            <td valign="top" class="value"><g:formatDate date="${pedidoInstance?.fechaAprobacionEconomica}" /></td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name"><g:message code="pedido.fechaAprobacionPAU.label" default="Fecha Aprobacion PAU" /></td>
                            
                            <td valign="top" class="value"><g:formatDate date="${pedidoInstance?.fechaAprobacionPAU}" /></td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name"><g:message code="pedido.fechaCierre.label" default="Fecha Cierre" /></td>
                            
                            <td valign="top" class="value"><g:formatDate date="${pedidoInstance?.fechaCierre}" /></td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name"><g:message code="pedido.anexos.label" default="Anexos" /></td>
                            
                            <td valign="top" style="text-align: left;" class="value">
                                <ul>
                                <g:each in="${pedidoInstance.anexos}" var="a">
                                    <li><g:link controller="anexoPorTipo" action="show" id="${a.id}">${a?.encodeAsHTML()}</g:link></li>
                                </g:each>
                                </ul>
                            </td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name"><g:message code="pedido.aprobaciones.label" default="Aprobaciones" /></td>
                            
                            <td valign="top" style="text-align: left;" class="value">
                                <ul>
                                <g:each in="${pedidoInstance.aprobaciones}" var="a">
                                    <li><g:link controller="aprobacion" action="show" id="${a.id}">${a?.encodeAsHTML()}</g:link></li>
                                </g:each>
                                </ul>
                            </td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name"><g:message code="pedido.diseniosExternos.label" default="Disenios Externos" /></td>
                            
                            <td valign="top" style="text-align: left;" class="value">
                                <ul>
                                <g:each in="${pedidoInstance.diseniosExternos}" var="d">
                                    <li><g:link controller="disenioExterno" action="show" id="${d.id}">${d?.encodeAsHTML()}</g:link></li>
                                </g:each>
                                </ul>
                            </td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name"><g:message code="pedido.especificaciones.label" default="Especificaciones" /></td>
                            
                            <td valign="top" style="text-align: left;" class="value">
                                <ul>
                                <g:each in="${pedidoInstance.especificaciones}" var="e">
                                    <li><g:link controller="ECS" action="show" id="${e.id}">${e?.encodeAsHTML()}</g:link></li>
                                </g:each>
                                </ul>
                            </td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name"><g:message code="pedido.estrategiasPrueba.label" default="Estrategias Prueba" /></td>
                            
                            <td valign="top" style="text-align: left;" class="value">
                                <ul>
                                <g:each in="${pedidoInstance.estrategiasPrueba}" var="e">
                                    <li><g:link controller="estrategiaPrueba" action="show" id="${e.id}">${e?.encodeAsHTML()}</g:link></li>
                                </g:each>
                                </ul>
                            </td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name"><g:message code="pedido.faseActual.label" default="Fase Actual" /></td>
                            
                            <td valign="top" class="value"><g:link controller="fase" action="show" id="${pedidoInstance?.faseActual?.id}">${pedidoInstance?.faseActual?.encodeAsHTML()}</g:link></td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name"><g:message code="pedido.logModificaciones.label" default="Log Modificaciones" /></td>
                            
                            <td valign="top" style="text-align: left;" class="value">
                                <ul>
                                <g:each in="${pedidoInstance.logModificaciones}" var="l">
                                    <li><g:link controller="logModificaciones" action="show" id="${l.id}">${l?.encodeAsHTML()}</g:link></li>
                                </g:each>
                                </ul>
                            </td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name"><g:message code="pedido.pedidosHijos.label" default="Pedidos Hijos" /></td>
                            
                            <td valign="top" style="text-align: left;" class="value">
                                <ul>
                                <g:each in="${pedidoInstance.pedidosHijos}" var="p">
                                    <li><g:link controller="pedidoHijo" action="show" id="${p.id}">${p?.encodeAsHTML()}</g:link></li>
                                </g:each>
                                </ul>
                            </td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name"><g:message code="pedido.planificaciones.label" default="Planificaciones" /></td>
                            
                            <td valign="top" style="text-align: left;" class="value">
                                <ul>
                                <g:each in="${pedidoInstance.planificaciones}" var="p">
                                    <li><g:link controller="planificacionEsfuerzo" action="show" id="${p.id}">${p?.encodeAsHTML()}</g:link></li>
                                </g:each>
                                </ul>
                            </td>
                            
                        </tr>
                    
                    </tbody>
                </table>
            </div>
            <div class="buttons">
                <g:form>
                    <g:hiddenField name="id" value="${pedidoInstance?.id}" />
                    <span class="button"><g:actionSubmit class="edit" action="edit" value="${message(code: 'default.button.edit.label', default: 'Edit')}" /></span>
                    <span class="button"><g:actionSubmit class="delete" action="delete" value="${message(code: 'default.button.delete.label', default: 'Delete')}" onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');" /></span>
                </g:form>
            </div>
        </div>
    </body>
</html>
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        