

<%@ page import="ar.com.telecom.pcs.entities.Pedido" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="layout" content="main" />
        <g:set var="entityName" value="${message(code: 'pedido.label', default: 'Pedido')}" />
        <title><g:message code="default.create.label" args="[entityName]" /></title>
    </head>
    <body>
        <div class="nav">
            <span class="menuButton"><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></span>
            <span class="menuButton"><g:link class="list" action="list"><g:message code="default.list.label" args="[entityName]" /></g:link></span>
        </div>
        <div class="body">
            <h1><g:message code="default.create.label" args="[entityName]" /></h1>
            <g:if test="${flash.message}">
            <div class="message">${flash.message}</div>
            </g:if>
            <g:hasErrors bean="${pedidoInstance}">
            <div class="errors">
                <g:renderErrors bean="${pedidoInstance}" as="list" />
            </div>
            </g:hasErrors>
            <g:form action="save" >
                <div class="dialog">
                    <table>
                        <tbody>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="titulo"><g:message code="pedido.titulo.label" default="Titulo" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: pedidoInstance, field: 'titulo', 'errors')}">
                                    <g:textField name="titulo" maxlength="200" value="${pedidoInstance?.titulo}" />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="legajoUsuarioCreador"><g:message code="pedido.legajoUsuarioCreador.label" default="Legajo Usuario Creador" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: pedidoInstance, field: 'legajoUsuarioCreador', 'errors')}">
                                    <g:textField name="legajoUsuarioCreador" value="${pedidoInstance?.legajoUsuarioCreador}" />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="fechaUltimaModificacion"><g:message code="pedido.fechaUltimaModificacion.label" default="Fecha Ultima Modificacion" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: pedidoInstance, field: 'fechaUltimaModificacion', 'errors')}">
                                    <g:datePicker name="fechaUltimaModificacion" precision="day" value="${pedidoInstance?.fechaUltimaModificacion}" default="none" noSelection="['': '']" />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="fechaCargaPedido"><g:message code="pedido.fechaCargaPedido.label" default="Fecha Carga Pedido" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: pedidoInstance, field: 'fechaCargaPedido', 'errors')}">
                                    <g:datePicker name="fechaCargaPedido" precision="day" value="${pedidoInstance?.fechaCargaPedido}" default="none" noSelection="['': '']" />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="fechaCancelacion"><g:message code="pedido.fechaCancelacion.label" default="Fecha Cancelacion" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: pedidoInstance, field: 'fechaCancelacion', 'errors')}">
                                    <g:datePicker name="fechaCancelacion" precision="day" value="${pedidoInstance?.fechaCancelacion}" default="none" noSelection="['': '']" />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="fechaSuspension"><g:message code="pedido.fechaSuspension.label" default="Fecha Suspension" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: pedidoInstance, field: 'fechaSuspension', 'errors')}">
                                    <g:datePicker name="fechaSuspension" precision="day" value="${pedidoInstance?.fechaSuspension}" default="none" noSelection="['': '']" />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="motivoCancelacion"><g:message code="pedido.motivoCancelacion.label" default="Motivo Cancelacion" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: pedidoInstance, field: 'motivoCancelacion', 'errors')}">
                                    <g:textField name="motivoCancelacion" value="${pedidoInstance?.motivoCancelacion}" />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="motivoSuspension"><g:message code="pedido.motivoSuspension.label" default="Motivo Suspension" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: pedidoInstance, field: 'motivoSuspension', 'errors')}">
                                    <g:textField name="motivoSuspension" value="${pedidoInstance?.motivoSuspension}" />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="descripcion"><g:message code="pedido.descripcion.label" default="Descripcion" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: pedidoInstance, field: 'descripcion', 'errors')}">
                                    <g:textField name="descripcion" value="${pedidoInstance?.descripcion}" />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="alcance"><g:message code="pedido.alcance.label" default="Alcance" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: pedidoInstance, field: 'alcance', 'errors')}">
                                    <g:textField name="alcance" value="${pedidoInstance?.alcance}" />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="objetivo"><g:message code="pedido.objetivo.label" default="Objetivo" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: pedidoInstance, field: 'objetivo', 'errors')}">
                                    <g:textField name="objetivo" value="${pedidoInstance?.objetivo}" />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="tipoPedido"><g:message code="pedido.tipoPedido.label" default="Tipo Pedido" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: pedidoInstance, field: 'tipoPedido', 'errors')}">
                                    <g:select name="tipoPedido.id" from="${ar.com.telecom.pcs.entities.TipoPedido.list()}" optionKey="id" value="${pedidoInstance?.tipoPedido?.id}" noSelection="['null': '']" />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="fueraAlcance"><g:message code="pedido.fueraAlcance.label" default="Fuera Alcance" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: pedidoInstance, field: 'fueraAlcance', 'errors')}">
                                    <g:textField name="fueraAlcance" value="${pedidoInstance?.fueraAlcance}" />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="infoAdicional"><g:message code="pedido.infoAdicional.label" default="Info Adicional" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: pedidoInstance, field: 'infoAdicional', 'errors')}">
                                    <g:textField name="infoAdicional" value="${pedidoInstance?.infoAdicional}" />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="fechaDeseadaImplementacion"><g:message code="pedido.fechaDeseadaImplementacion.label" default="Fecha Deseada Implementacion" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: pedidoInstance, field: 'fechaDeseadaImplementacion', 'errors')}">
                                    <g:datePicker name="fechaDeseadaImplementacion" precision="day" value="${pedidoInstance?.fechaDeseadaImplementacion}" default="none" noSelection="['': '']" />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="sistemaSugerido"><g:message code="pedido.sistemaSugerido.label" default="Sistema Sugerido" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: pedidoInstance, field: 'sistemaSugerido', 'errors')}">
                                    <g:select name="sistemaSugerido.id" from="${ar.com.telecom.pcs.entities.Sistema.list()}" optionKey="id" value="${pedidoInstance?.sistemaSugerido?.id}" noSelection="['null': '']" />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="beneficiosEsperados"><g:message code="pedido.beneficiosEsperados.label" default="Beneficios Esperados" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: pedidoInstance, field: 'beneficiosEsperados', 'errors')}">
                                    <g:textField name="beneficiosEsperados" value="${pedidoInstance?.beneficiosEsperados}" />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="riesgosObservables"><g:message code="pedido.riesgosObservables.label" default="Riesgos Observables" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: pedidoInstance, field: 'riesgosObservables', 'errors')}">
                                    <g:textField name="riesgosObservables" value="${pedidoInstance?.riesgosObservables}" />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="tipoReferencia"><g:message code="pedido.tipoReferencia.label" default="Tipo Referencia" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: pedidoInstance, field: 'tipoReferencia', 'errors')}">
                                    <g:select name="tipoReferencia.id" from="${ar.com.telecom.pcs.entities.TipoReferencia.list()}" optionKey="id" value="${pedidoInstance?.tipoReferencia?.id}" noSelection="['null': '']" />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="referenciaOrigen"><g:message code="pedido.referenciaOrigen.label" default="Referencia Origen" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: pedidoInstance, field: 'referenciaOrigen', 'errors')}">
                                    <g:textField name="referenciaOrigen" maxlength="50" value="${pedidoInstance?.referenciaOrigen}" />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="tipoGestion"><g:message code="pedido.tipoGestion.label" default="Tipo Gestion" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: pedidoInstance, field: 'tipoGestion', 'errors')}">
                                    <g:select name="tipoGestion.id" from="${ar.com.telecom.pcs.entities.TipoGestion.list()}" optionKey="id" value="${pedidoInstance?.tipoGestion?.id}" noSelection="['null': '']" />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="prioridad"><g:message code="pedido.prioridad.label" default="Prioridad" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: pedidoInstance, field: 'prioridad', 'errors')}">
                                    <g:select name="prioridad.id" from="${ar.com.telecom.pcs.entities.Prioridad.list()}" optionKey="id" value="${pedidoInstance?.prioridad?.id}" noSelection="['null': '']" />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="fechaVencimientoPlanificacion"><g:message code="pedido.fechaVencimientoPlanificacion.label" default="Fecha Vencimiento Planificacion" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: pedidoInstance, field: 'fechaVencimientoPlanificacion', 'errors')}">
                                    <g:datePicker name="fechaVencimientoPlanificacion" precision="day" value="${pedidoInstance?.fechaVencimientoPlanificacion}" default="none" noSelection="['': '']" />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="comentarios"><g:message code="pedido.comentarios.label" default="Comentarios" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: pedidoInstance, field: 'comentarios', 'errors')}">
                                    <g:textField name="comentarios" value="${pedidoInstance?.comentarios}" />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="valorHora"><g:message code="pedido.valorHora.label" default="Valor Hora" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: pedidoInstance, field: 'valorHora', 'errors')}">
                                    <g:textField name="valorHora" value="${fieldValue(bean: pedidoInstance, field: 'valorHora')}" />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="legajoInterlocutorUsuario"><g:message code="pedido.legajoInterlocutorUsuario.label" default="Legajo Interlocutor Usuario" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: pedidoInstance, field: 'legajoInterlocutorUsuario', 'errors')}">
                                    <g:textField name="legajoInterlocutorUsuario" value="${pedidoInstance?.legajoInterlocutorUsuario}" />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="legajoGerenteUsuario"><g:message code="pedido.legajoGerenteUsuario.label" default="Legajo Gerente Usuario" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: pedidoInstance, field: 'legajoGerenteUsuario', 'errors')}">
                                    <g:textField name="legajoGerenteUsuario" value="${pedidoInstance?.legajoGerenteUsuario}" />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="codigoDireccionGerencia"><g:message code="pedido.codigoDireccionGerencia.label" default="Codigo Direccion Gerencia" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: pedidoInstance, field: 'codigoDireccionGerencia', 'errors')}">
                                    <g:textField name="codigoDireccionGerencia" maxlength="60" value="${pedidoInstance?.codigoDireccionGerencia}" />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="descripcionDireccionGerencia"><g:message code="pedido.descripcionDireccionGerencia.label" default="Descripcion Direccion Gerencia" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: pedidoInstance, field: 'descripcionDireccionGerencia', 'errors')}">
                                    <g:textField name="descripcionDireccionGerencia" maxlength="60" value="${pedidoInstance?.descripcionDireccionGerencia}" />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="grupoGestionDemanda"><g:message code="pedido.grupoGestionDemanda.label" default="Grupo Gestion Demanda" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: pedidoInstance, field: 'grupoGestionDemanda', 'errors')}">
                                    <g:textField name="grupoGestionDemanda" maxlength="100" value="${pedidoInstance?.grupoGestionDemanda}" />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="legajoUsuarioGestionDemanda"><g:message code="pedido.legajoUsuarioGestionDemanda.label" default="Legajo Usuario Gestion Demanda" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: pedidoInstance, field: 'legajoUsuarioGestionDemanda', 'errors')}">
                                    <g:textField name="legajoUsuarioGestionDemanda" value="${pedidoInstance?.legajoUsuarioGestionDemanda}" />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="legajoCoordinadorCambio"><g:message code="pedido.legajoCoordinadorCambio.label" default="Legajo Coordinador Cambio" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: pedidoInstance, field: 'legajoCoordinadorCambio', 'errors')}">
                                    <g:textField name="legajoCoordinadorCambio" value="${pedidoInstance?.legajoCoordinadorCambio}" />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="legajoUsuarioAprobadorEconomico"><g:message code="pedido.legajoUsuarioAprobadorEconomico.label" default="Legajo Usuario Aprobador Economico" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: pedidoInstance, field: 'legajoUsuarioAprobadorEconomico', 'errors')}">
                                    <g:textField name="legajoUsuarioAprobadorEconomico" value="${pedidoInstance?.legajoUsuarioAprobadorEconomico}" />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="fechaFinalizacionIngresoPedido"><g:message code="pedido.fechaFinalizacionIngresoPedido.label" default="Fecha Finalizacion Ingreso Pedido" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: pedidoInstance, field: 'fechaFinalizacionIngresoPedido', 'errors')}">
                                    <g:datePicker name="fechaFinalizacionIngresoPedido" precision="day" value="${pedidoInstance?.fechaFinalizacionIngresoPedido}" default="none" noSelection="['': '']" />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="fechaAprobacionPedidoGU"><g:message code="pedido.fechaAprobacionPedidoGU.label" default="Fecha Aprobacion Pedido GU" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: pedidoInstance, field: 'fechaAprobacionPedidoGU', 'errors')}">
                                    <g:datePicker name="fechaAprobacionPedidoGU" precision="day" value="${pedidoInstance?.fechaAprobacionPedidoGU}" default="none" noSelection="['': '']" />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="fechaValidacionIU"><g:message code="pedido.fechaValidacionIU.label" default="Fecha Validacion IU" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: pedidoInstance, field: 'fechaValidacionIU', 'errors')}">
                                    <g:datePicker name="fechaValidacionIU" precision="day" value="${pedidoInstance?.fechaValidacionIU}" default="none" noSelection="['': '']" />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="fechaEvaluacionPedidoFGD"><g:message code="pedido.fechaEvaluacionPedidoFGD.label" default="Fecha Evaluacion Pedido FGD" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: pedidoInstance, field: 'fechaEvaluacionPedidoFGD', 'errors')}">
                                    <g:datePicker name="fechaEvaluacionPedidoFGD" precision="day" value="${pedidoInstance?.fechaEvaluacionPedidoFGD}" default="none" noSelection="['': '']" />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="fechaAprobacionEspecificacionPedidoCC"><g:message code="pedido.fechaAprobacionEspecificacionPedidoCC.label" default="Fecha Aprobacion Especificacion Pedido CC" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: pedidoInstance, field: 'fechaAprobacionEspecificacionPedidoCC', 'errors')}">
                                    <g:datePicker name="fechaAprobacionEspecificacionPedidoCC" precision="day" value="${pedidoInstance?.fechaAprobacionEspecificacionPedidoCC}" default="none" noSelection="['': '']" />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="fechaAprobacionEspecificacionImpacto"><g:message code="pedido.fechaAprobacionEspecificacionImpacto.label" default="Fecha Aprobacion Especificacion Impacto" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: pedidoInstance, field: 'fechaAprobacionEspecificacionImpacto', 'errors')}">
                                    <g:datePicker name="fechaAprobacionEspecificacionImpacto" precision="day" value="${pedidoInstance?.fechaAprobacionEspecificacionImpacto}" default="none" noSelection="['': '']" />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="fechaValidacionImpacto"><g:message code="pedido.fechaValidacionImpacto.label" default="Fecha Validacion Impacto" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: pedidoInstance, field: 'fechaValidacionImpacto', 'errors')}">
                                    <g:datePicker name="fechaValidacionImpacto" precision="day" value="${pedidoInstance?.fechaValidacionImpacto}" default="none" noSelection="['': '']" />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="fechaAprobacionEconomica"><g:message code="pedido.fechaAprobacionEconomica.label" default="Fecha Aprobacion Economica" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: pedidoInstance, field: 'fechaAprobacionEconomica', 'errors')}">
                                    <g:datePicker name="fechaAprobacionEconomica" precision="day" value="${pedidoInstance?.fechaAprobacionEconomica}" default="none" noSelection="['': '']" />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="fechaAprobacionPAU"><g:message code="pedido.fechaAprobacionPAU.label" default="Fecha Aprobacion PAU" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: pedidoInstance, field: 'fechaAprobacionPAU', 'errors')}">
                                    <g:datePicker name="fechaAprobacionPAU" precision="day" value="${pedidoInstance?.fechaAprobacionPAU}" default="none" noSelection="['': '']" />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="fechaCierre"><g:message code="pedido.fechaCierre.label" default="Fecha Cierre" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: pedidoInstance, field: 'fechaCierre', 'errors')}">
                                    <g:datePicker name="fechaCierre" precision="day" value="${pedidoInstance?.fechaCierre}" default="none" noSelection="['': '']" />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="faseActual"><g:message code="pedido.faseActual.label" default="Fase Actual" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: pedidoInstance, field: 'faseActual', 'errors')}">
                                    <g:select name="faseActual.id" from="${ar.com.telecom.pcs.entities.Fase.list()}" optionKey="id" value="${pedidoInstance?.faseActual?.id}"  />
                                </td>
                            </tr>
                        
                        </tbody>
                    </table>
                </div>
                <div class="buttons">
                    <span class="button"><g:submitButton name="create" class="save" value="${message(code: 'default.button.create.label', default: 'Create')}" /></span>
                </div>
            </g:form>
        </div>
    </body>
</html>
                                                                                                