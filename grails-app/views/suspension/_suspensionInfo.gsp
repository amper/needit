<%@page import="ar.com.telecom.util.DateUtil"%>
<g:if test="${pedido.estaSuspendido()}">
	<div class="seccionHide2" style="background-color: #c1d5d9;">
		<p class="tituloSeccion">Informaci&oacute;n de la suspensi&oacute;n</p>
		
		<div class="formSeccion" style="padding:10px;">
	
			<div class="pTres" style="width:30%;float:left;">
				<label class="formLabel">Fecha de suspensi&oacute;n:</label>
				<p class="info">
					&nbsp; ${DateUtil.toString(pedido.fechaSuspension, false)}
				</p>
			</div>				
	 
			<div class="pTres" style="width:30%;float:left;">
				<label class="formLabel sinMargenIzquierdo">Usuario que suspendi&oacute;:</label>
				<div id="usuarioCoordinador" class="boxUser">
					<g:render template="/templates/userBox"
						model="['person': pedido.legajoCoordinadorCambio, 'box':'usuarioCoordinador']" />
				</div>
			</div>
	
			<div class="pTres">
				<label class="formLabel sinMargenIzquierdo">Motivo:</label>
				<p class="info sinMargenIzquierdo" style="width:256px;">
					${pedido.motivoSuspension}
				</p>
			</div>
			<div class="separator"></div>
			<div class="pCompleto sinMargenIzquierdo" style="width:100%;">
				<label class="formLabel">Comentario:</label>
				<p class="info" style="width:741px;height:60px;">
					&nbsp;${pedido.comentarioSuspension}
				</p>
			</div>
		</div>
	</div>
</g:if>