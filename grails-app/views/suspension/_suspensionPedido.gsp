<%@page import="ar.com.telecom.pcs.entities.MotivoSuspension"%>
<g:updateToken />

<g:javascript library="boxUser" />

<script type="text/javascript">
	jQuery(document).ready(
		function($) {
			jQuery('#motivo').combobox();
		});
</script>
 
<div id="mensajes">
	<g:if test="${flash.message}">
		<div class="message">
			${flash.message}
		</div>
	</g:if>
</div>

<g:hasErrors bean="${pedidoInstance}">
	<div id="divErrors" class="errors">
		<g:renderErrors bean="${pedidoInstance}" as="list" />
	</div>
</g:hasErrors>


<div style="overflow:auto;">
	<g:uploadForm name="formSuspension" id="formSuspension" useToken="true" action="guardarAnexo" controller="suspension">
			<g:hiddenField name="pedidoId" value="${pedidoInstance.id}" />
			<g:hiddenField name="hAnexoSuspension" value="${params.controller}" />
			<div class="pDos">
				<label class="formLabel sinMargenIzquierdo">Proceso de soporte:</label>
				<g:if test="${!pedidoInstance.estaSuspendido()}">
					<p class="info">Suspender Pedido Usuario</p>
				</g:if>
				<g:else>
					<p class="info">Reanudar Pedido Usuario Suspendido</p>
				</g:else>
			</div>
			<div class="pDos">
				<label class="formLabel sinMargenIzquierdo">Coordinador pedido:</label>
				
				<div id="legajoCoordinador" class="boxUser">
					<g:render template="/templates/userBox" model="['person':usuarioLogueado, 'box':'legajoCoordinador']"></g:render>
				</div>
			</div>		
		
			<div class="pCompleto conMargenChico">
				<label class="formLabel sinMargenIzquierdo centrado">N&uacute;mero:</label>
				<p class="info">${pedidoInstance?.numero}</p>
			</div>
		
			<div class="pCompleto conMargenChico">
				<label class="formLabel sinMargenIzquierdo centrado">Titulo:</label>
				<p class="info">${pedidoInstance?.titulo}</p>
			</div>
			
			<div class="pCompleto conMargenChico">
				<label class="formLabel sinMargenIzquierdo centrado">Macroestado:</label>
				<p class="info">${pedidoInstance?.macroEstado}</p>
			</div>
			
			<div class="pCompleto conMargenChico">
				<label class="formLabel sinMargenIzquierdo centrado">Motivo:</label>
				
				<g:if test="${!pedidoInstance.estaSuspendido()}">				
					<g:select id="motivo" name="motivoSuspension.id"
					from="${MotivoSuspension.list()}" value="${pedidoInstance.motivoSuspension?.id}"
					optionKey="id" noSelection="['null': '']" />
				</g:if>
				<g:else>
					<p class="info">${pedidoInstance?.motivoSuspension}</p>
				</g:else>
			</div>			
			
			<div class="pCompleto conMargenChico">
				<label class="formLabel sinMargenIzquierdo centrado">Comentarios:</label>
				<g:if test="${!pedidoInstance.estaSuspendido()}">
					<g:textArea id='comentarioSuspension' name="comentarioSuspension" style="width:398px;height:40px;padding:2px;">${pedidoInstance.comentarioSuspension}</g:textArea>
				</g:if>
				<g:else>
					<p class="info">${pedidoInstance?.comentarioSuspension}</p>
				</g:else>
			</div>

			<g:if test="${pedidoInstance.estaSuspendido()}">
				<div class="pCompleto conMargenChico">
					<label class="formLabel sinMargenIzquierdo centrado">Comentarios reanudaci&oacute;n:</label>
					<g:textArea id='comentarioReanudacion' name="comentarioReanudacion" style="width:398px;height:40px;padding:2px;">${pedidoInstance.comentarioReanudacion}</g:textArea>
				</div>
			</g:if>

			<div class="pDos">
			
			<g:if test="${pedidoInstance?.muestraAnexosPopSuspension()}">
			<label class="formLabel" for='combobox'>Anexos</label>
				<ul>
					<g:each in="${pedidoInstance?.anexosPopSuspension}" var="arch">
						<li><g:link action="verAnexo" target="_blank"
								params="[anexoId: arch.id]">
								${arch}
							</g:link> 
							&nbsp;&nbsp;&nbsp; 
							<g:if test="${arch.puedeEliminarse(pedidoInstance)}">				
								<g:link action="eliminarAnexo"
									params="[pedidoId: pedidoInstance?.id, anexoId: arch.id]">
									<img src="${resource(dir:'images',file:'delete.gif')}"
										width="10" height="10" />
								</g:link>
							</g:if>
						</li>
					</g:each>
					</ul>
			</g:if>
			
			</div>
			<button id="nuevoAnexoGlobal" name="nuevoAnexoGlobal" onclick="openPopUpAnexoSuspension();" class="formSeccionSubmitButtonHistorial ui-button ui-widget ui-state-default ui-corner-all ui-state-hover">Agregar anexo</button>

			<div class="separator"></div>

			<div class="formSeccionButton" style="margin-top: 50px;">
				<button id="btnCancelaPopSuspender" value="Cancelar" class="formSeccionSubmitButtonAnexo" name="Cancelar" onClick="cancelarPopUpSuspension();">Cancelar</button>
				<button id="btnAceptaPopSuspender" value="Aceptar" class="formSeccionSubmitButtonAnexo" name="Aceptar" onClick="validaPopUpConfirmacion(this.form, ${!pedidoInstance.estaSuspendido()});">Aceptar</button>
			</div>
	</g:uploadForm>
</div>

<div id="dialog-anexos" title="Nuevo Anexo" style="display: none;">
	<g:if test="${!pedidoInstance.id}">
		<g:render template="/templates/anexoPorTipo" model="['pedidoInstance': pedidoInstance, 'controllerBack':params.controller]"></g:render>			
	</g:if>
</div>
