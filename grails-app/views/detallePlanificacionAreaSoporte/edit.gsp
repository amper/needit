

<%@ page import="ar.com.telecom.pcs.entities.DetallePlanificacionAreaSoporte" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="layout" content="main" />
        <g:set var="entityName" value="${message(code: 'detallePlanificacionAreaSoporte.label', default: 'DetallePlanificacionAreaSoporte')}" />
        <title><g:message code="default.edit.label" args="[entityName]" /></title>
    </head>
    <body>
        <div class="nav">
            <span class="menuButton"><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></span>
            <span class="menuButton"><g:link class="list" action="list"><g:message code="default.list.label" args="[entityName]" /></g:link></span>
            <span class="menuButton"><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></span>
        </div>
        <div class="body">
            <h1><g:message code="default.edit.label" args="[entityName]" /></h1>
            <g:if test="${flash.message}">
            <div class="message">${flash.message}</div>
            </g:if>
            <g:hasErrors bean="${detallePlanificacionAreaSoporteInstance}">
            <div class="errors">
                <g:renderErrors bean="${detallePlanificacionAreaSoporteInstance}" as="list" />
            </div>
            </g:hasErrors>
            <g:form method="post" >
                <g:hiddenField name="id" value="${detallePlanificacionAreaSoporteInstance?.id}" />
                <g:hiddenField name="version" value="${detallePlanificacionAreaSoporteInstance?.version}" />
                <div class="dialog">
                    <table>
                        <tbody>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                  <label for="fechaDesde"><g:message code="detallePlanificacionAreaSoporte.fechaDesde.label" default="Fecha Desde" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: detallePlanificacionAreaSoporteInstance, field: 'fechaDesde', 'errors')}">
                                    <g:datePicker name="fechaDesde" precision="day" value="${detallePlanificacionAreaSoporteInstance?.fechaDesde}" default="none" noSelection="['': '']" />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                  <label for="fechaHasta"><g:message code="detallePlanificacionAreaSoporte.fechaHasta.label" default="Fecha Hasta" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: detallePlanificacionAreaSoporteInstance, field: 'fechaHasta', 'errors')}">
                                    <g:datePicker name="fechaHasta" precision="day" value="${detallePlanificacionAreaSoporteInstance?.fechaHasta}" default="none" noSelection="['': '']" />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                  <label for="cantidadHoras"><g:message code="detallePlanificacionAreaSoporte.cantidadHoras.label" default="Cantidad Horas" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: detallePlanificacionAreaSoporteInstance, field: 'cantidadHoras', 'errors')}">
                                    <g:textField name="cantidadHoras" value="${fieldValue(bean: detallePlanificacionAreaSoporteInstance, field: 'cantidadHoras')}" />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                  <label for="comentarioDetalleAreaSoporte"><g:message code="detallePlanificacionAreaSoporte.comentarioDetalleAreaSoporte.label" default="Comentario Detalle Area Soporte" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: detallePlanificacionAreaSoporteInstance, field: 'comentarioDetalleAreaSoporte', 'errors')}">
                                    <g:textField name="comentarioDetalleAreaSoporte" value="${detallePlanificacionAreaSoporteInstance?.comentarioDetalleAreaSoporte}" />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                  <label for="planificacion"><g:message code="detallePlanificacionAreaSoporte.planificacion.label" default="Planificacion" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: detallePlanificacionAreaSoporteInstance, field: 'planificacion', 'errors')}">
                                    <g:select name="planificacion.id" from="${ar.com.telecom.pcs.entities.PlanificacionEsfuerzo.list()}" optionKey="id" value="${detallePlanificacionAreaSoporteInstance?.planificacion?.id}"  />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                  <label for="tareaSoporte"><g:message code="detallePlanificacionAreaSoporte.tareaSoporte.label" default="Tarea Soporte" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: detallePlanificacionAreaSoporteInstance, field: 'tareaSoporte', 'errors')}">
                                    <g:select name="tareaSoporte.id" from="${ar.com.telecom.pcs.entities.TareaSoporte.list()}" optionKey="id" value="${detallePlanificacionAreaSoporteInstance?.tareaSoporte?.id}"  />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                  <label for="valorHora"><g:message code="detallePlanificacionAreaSoporte.valorHora.label" default="Valor Hora" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: detallePlanificacionAreaSoporteInstance, field: 'valorHora', 'errors')}">
                                    <g:textField name="valorHora" value="${fieldValue(bean: detallePlanificacionAreaSoporteInstance, field: 'valorHora')}" />
                                </td>
                            </tr>
                        
                        </tbody>
                    </table>
                </div>
                <div class="buttons">
                    <span class="button"><g:actionSubmit class="save" action="update" value="${message(code: 'default.button.update.label', default: 'Update')}" /></span>
                    <span class="button"><g:actionSubmit class="delete" action="delete" value="${message(code: 'default.button.delete.label', default: 'Delete')}" onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');" /></span>
                </div>
            </g:form>
        </div>
    </body>
</html>
