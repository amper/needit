
<%@ page import="ar.com.telecom.pcs.entities.DetallePlanificacionAreaSoporte" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="layout" content="main" />
        <g:set var="entityName" value="${message(code: 'detallePlanificacionAreaSoporte.label', default: 'DetallePlanificacionAreaSoporte')}" />
        <title><g:message code="default.list.label" args="[entityName]" /></title>
    </head>
    <body>
        <div class="nav">
            <span class="menuButton"><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></span>
            <span class="menuButton"><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></span>
        </div>
        <div class="body">
            <h1><g:message code="default.list.label" args="[entityName]" /></h1>
            <g:if test="${flash.message}">
            <div class="message">${flash.message}</div>
            </g:if>
            <div class="list">
                <table>
                    <thead>
                        <tr>
                        
                            <g:sortableColumn property="id" title="${message(code: 'detallePlanificacionAreaSoporte.id.label', default: 'Id')}" />
                        
                            <g:sortableColumn property="fechaDesde" title="${message(code: 'detallePlanificacionAreaSoporte.fechaDesde.label', default: 'Fecha Desde')}" />
                        
                            <g:sortableColumn property="fechaHasta" title="${message(code: 'detallePlanificacionAreaSoporte.fechaHasta.label', default: 'Fecha Hasta')}" />
                        
                            <g:sortableColumn property="cantidadHoras" title="${message(code: 'detallePlanificacionAreaSoporte.cantidadHoras.label', default: 'Cantidad Horas')}" />
                        
                            <g:sortableColumn property="comentarioDetalleAreaSoporte" title="${message(code: 'detallePlanificacionAreaSoporte.comentarioDetalleAreaSoporte.label', default: 'Comentario Detalle Area Soporte')}" />
                        
                            <th><g:message code="detallePlanificacionAreaSoporte.planificacion.label" default="Planificacion" /></th>
                        
                        </tr>
                    </thead>
                    <tbody>
                    <g:each in="${detallePlanificacionAreaSoporteInstanceList}" status="i" var="detallePlanificacionAreaSoporteInstance">
                        <tr class="${(i % 2) == 0 ? 'odd' : 'even'}">
                        
                            <td><g:link action="show" id="${detallePlanificacionAreaSoporteInstance.id}">${fieldValue(bean: detallePlanificacionAreaSoporteInstance, field: "id")}</g:link></td>
                        
                            <td><g:formatDate date="${detallePlanificacionAreaSoporteInstance.fechaDesde}" /></td>
                        
                            <td><g:formatDate date="${detallePlanificacionAreaSoporteInstance.fechaHasta}" /></td>
                        
                            <td>${fieldValue(bean: detallePlanificacionAreaSoporteInstance, field: "cantidadHoras")}</td>
                        
                            <td>${fieldValue(bean: detallePlanificacionAreaSoporteInstance, field: "comentarioDetalleAreaSoporte")}</td>
                        
                            <td>${fieldValue(bean: detallePlanificacionAreaSoporteInstance, field: "planificacion")}</td>
                        
                        </tr>
                    </g:each>
                    </tbody>
                </table>
            </div>
            <div class="paginateButtons">
                <g:paginate total="${detallePlanificacionAreaSoporteInstanceTotal}" />
            </div>
        </div>
    </body>
</html>
