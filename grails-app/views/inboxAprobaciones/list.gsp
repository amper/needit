<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
		<meta name="layout" content="mainApp" />
		<title>NeedIT</title>
		<g:javascript library="boxUser" />
		<g:javascript library="inboxAprobaciones" />
	</head>
	<body>
		<div id="ContenedorGeneral">
			<div id="contenedorIzq">
				<div>
					<p class="tituloPrincipal">Inbox de Aprobaciones &nbsp;&nbsp; <g:secureRemoteLink title="Actualizar" class="botonActualizar" action="actualizar" controller="inboxAprobaciones" params="['action':'actualizar']" update="aprobaciones"></g:secureRemoteLink> </p>
				</div>
			
				<div id="aprobaciones">
					<g:render template="/templates/inboxAprobaciones" model="['aprobacionInstanceList':aprobacionInstanceList, 'aprobacionInstanceTotal':aprobacionInstanceTotal]"></g:render>
				</div>
			</div>
			<g:render template="/templates/menuLateral"/>
				
		</div>
	</body>
</html>
 