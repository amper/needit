
<%@ page import="ar.com.telecom.pcs.entities.ECS" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="layout" content="main" />
        <g:set var="entityName" value="${message(code: 'ECS.label', default: 'ECS')}" />
        <title><g:message code="default.show.label" args="[entityName]" /></title>
    </head>
    <body>
        <div class="nav">
            <span class="menuButton"><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></span>
            <span class="menuButton"><g:link class="list" action="list"><g:message code="default.list.label" args="[entityName]" /></g:link></span>
            <span class="menuButton"><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></span>
        </div>
        <div class="body">
            <h1><g:message code="default.show.label" args="[entityName]" /></h1>
            <g:if test="${flash.message}">
            <div class="message">${flash.message}</div>
            </g:if>
            <div class="dialog">
                <table>
                    <tbody>
                    
                        <tr class="prop">
                            <td valign="top" class="name"><g:message code="ECS.id.label" default="Id" /></td>
                            
                            <td valign="top" class="value">${fieldValue(bean: ECSInstance, field: "id")}</td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name"><g:message code="ECS.especificacionDetallada.label" default="Especificacion Detallada" /></td>
                            
                            <td valign="top" class="value">${fieldValue(bean: ECSInstance, field: "especificacionDetallada")}</td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name"><g:message code="ECS.alcanceDetallado.label" default="Alcance Detallado" /></td>
                            
                            <td valign="top" class="value">${fieldValue(bean: ECSInstance, field: "alcanceDetallado")}</td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name"><g:message code="ECS.notas.label" default="Notas" /></td>
                            
                            <td valign="top" class="value">${fieldValue(bean: ECSInstance, field: "notas")}</td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name"><g:message code="ECS.fueraAlcance.label" default="Fuera Alcance" /></td>
                            
                            <td valign="top" class="value">${fieldValue(bean: ECSInstance, field: "fueraAlcance")}</td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name"><g:message code="ECS.beneficiosEsperados.label" default="Beneficios Esperados" /></td>
                            
                            <td valign="top" class="value">${fieldValue(bean: ECSInstance, field: "beneficiosEsperados")}</td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name"><g:message code="ECS.realizaPI.label" default="Realiza PI" /></td>
                            
                            <td valign="top" class="value"><g:formatBoolean boolean="${ECSInstance?.realizaPI}" /></td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name"><g:message code="ECS.PIintegrada.label" default="PI integrada" /></td>
                            
                            <td valign="top" class="value"><g:formatBoolean boolean="${ECSInstance?.PIintegrada}" /></td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name"><g:message code="ECS.realizaPAU.label" default="Realiza PAU" /></td>
                            
                            <td valign="top" class="value"><g:formatBoolean boolean="${ECSInstance?.realizaPAU}" /></td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name"><g:message code="ECS.PAUintegrada.label" default="PAU integrada" /></td>
                            
                            <td valign="top" class="value"><g:formatBoolean boolean="${ECSInstance?.PAUintegrada}" /></td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name"><g:message code="ECS.justificacionPI.label" default="Justificacion PI" /></td>
                            
                            <td valign="top" class="value"><g:link controller="justificacion" action="show" id="${ECSInstance?.justificacionPI?.id}">${ECSInstance?.justificacionPI?.encodeAsHTML()}</g:link></td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name"><g:message code="ECS.justificacionPAU.label" default="Justificacion PAU" /></td>
                            
                            <td valign="top" class="value"><g:link controller="justificacion" action="show" id="${ECSInstance?.justificacionPAU?.id}">${ECSInstance?.justificacionPAU?.encodeAsHTML()}</g:link></td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name"><g:message code="ECS.sistemasImpactados.label" default="Sistemas Impactados" /></td>
                            
                            <td valign="top" style="text-align: left;" class="value">
                                <ul>
                                <g:each in="${ECSInstance.sistemasImpactados}" var="s">
                                    <li><g:link controller="sistemaImpactado" action="show" id="${s.id}">${s?.encodeAsHTML()}</g:link></li>
                                </g:each>
                                </ul>
                            </td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name"><g:message code="ECS.areasImpactadas.label" default="Areas Impactadas" /></td>
                            
                            <td valign="top" style="text-align: left;" class="value">
                                <ul>
                                <g:each in="${ECSInstance.areasImpactadas}" var="a">
                                    <li><g:link controller="areaSoporte" action="show" id="${a.id}">${a?.encodeAsHTML()}</g:link></li>
                                </g:each>
                                </ul>
                            </td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name"><g:message code="ECS.otrasPruebasECS.label" default="Otras Pruebas ECS" /></td>
                            
                            <td valign="top" style="text-align: left;" class="value">
                                <ul>
                                <g:each in="${ECSInstance.otrasPruebasECS}" var="o">
                                    <li><g:link controller="ECSOtrasPruebas" action="show" id="${o.id}">${o?.encodeAsHTML()}</g:link></li>
                                </g:each>
                                </ul>
                            </td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name"><g:message code="ECS.anexosPorTipo.label" default="Anexos Por Tipo" /></td>
                            
                            <td valign="top" style="text-align: left;" class="value">
                                <ul>
                                <g:each in="${ECSInstance.anexosPorTipo}" var="a">
                                    <li><g:link controller="anexoPorTipo" action="show" id="${a.id}">${a?.encodeAsHTML()}</g:link></li>
                                </g:each>
                                </ul>
                            </td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name"><g:message code="ECS.fechaDeCarga.label" default="Fecha De Carga" /></td>
                            
                            <td valign="top" class="value"><g:formatDate date="${ECSInstance?.fechaDeCarga}" /></td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name"><g:message code="ECS.versionECS.label" default="Version ECS" /></td>
                            
                            <td valign="top" class="value">${fieldValue(bean: ECSInstance, field: "versionECS")}</td>
                            
                        </tr>
                    
                    </tbody>
                </table>
            </div>
            <div class="buttons">
                <g:form>
                    <g:hiddenField name="id" value="${ECSInstance?.id}" />
                    <span class="button"><g:actionSubmit class="edit" action="edit" value="${message(code: 'default.button.edit.label', default: 'Edit')}" /></span>
                    <span class="button"><g:actionSubmit class="delete" action="delete" value="${message(code: 'default.button.delete.label', default: 'Delete')}" onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');" /></span>
                </g:form>
            </div>
        </div>
    </body>
</html>
