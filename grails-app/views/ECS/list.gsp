
<%@ page import="ar.com.telecom.pcs.entities.ECS" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="layout" content="main" />
        <g:set var="entityName" value="${message(code: 'ECS.label', default: 'ECS')}" />
        <title><g:message code="default.list.label" args="[entityName]" /></title>
    </head>
    <body>
        <div class="nav">
            <span class="menuButton"><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></span>
            <span class="menuButton"><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></span>
        </div>
        <div class="body">
            <h1><g:message code="default.list.label" args="[entityName]" /></h1>
            <g:if test="${flash.message}">
            <div class="message">${flash.message}</div>
            </g:if>
            <div class="list">
                <table>
                    <thead>
                        <tr>
                        
                            <g:sortableColumn property="id" title="${message(code: 'ECS.id.label', default: 'Id')}" />
                        
                            <g:sortableColumn property="especificacionDetallada" title="${message(code: 'ECS.especificacionDetallada.label', default: 'Especificacion Detallada')}" />
                        
                            <g:sortableColumn property="alcanceDetallado" title="${message(code: 'ECS.alcanceDetallado.label', default: 'Alcance Detallado')}" />
                        
                            <g:sortableColumn property="notas" title="${message(code: 'ECS.notas.label', default: 'Notas')}" />
                        
                            <g:sortableColumn property="fueraAlcance" title="${message(code: 'ECS.fueraAlcance.label', default: 'Fuera Alcance')}" />
                        
                            <g:sortableColumn property="beneficiosEsperados" title="${message(code: 'ECS.beneficiosEsperados.label', default: 'Beneficios Esperados')}" />
                        
                        </tr>
                    </thead>
                    <tbody>
                    <g:each in="${ECSInstanceList}" status="i" var="ECSInstance">
                        <tr class="${(i % 2) == 0 ? 'odd' : 'even'}">
                        
                            <td><g:link action="show" id="${ECSInstance.id}">${fieldValue(bean: ECSInstance, field: "id")}</g:link></td>
                        
                            <td>${fieldValue(bean: ECSInstance, field: "especificacionDetallada")}</td>
                        
                            <td>${fieldValue(bean: ECSInstance, field: "alcanceDetallado")}</td>
                        
                            <td>${fieldValue(bean: ECSInstance, field: "notas")}</td>
                        
                            <td>${fieldValue(bean: ECSInstance, field: "fueraAlcance")}</td>
                        
                            <td>${fieldValue(bean: ECSInstance, field: "beneficiosEsperados")}</td>
                        
                        </tr>
                    </g:each>
                    </tbody>
                </table>
            </div>
            <div class="paginateButtons">
                <g:paginate total="${ECSInstanceTotal}" />
            </div>
        </div>
    </body>
</html>
