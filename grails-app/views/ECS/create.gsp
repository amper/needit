

<%@ page import="ar.com.telecom.pcs.entities.ECS" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="layout" content="main" />
        <g:set var="entityName" value="${message(code: 'ECS.label', default: 'ECS')}" />
        <title><g:message code="default.create.label" args="[entityName]" /></title>
    </head>
    <body>
        <div class="nav">
            <span class="menuButton"><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></span>
            <span class="menuButton"><g:link class="list" action="list"><g:message code="default.list.label" args="[entityName]" /></g:link></span>
        </div>
        <div class="body">
            <h1><g:message code="default.create.label" args="[entityName]" /></h1>
            <g:if test="${flash.message}">
            <div class="message">${flash.message}</div>
            </g:if>
            <g:hasErrors bean="${ECSInstance}">
            <div class="errors">
                <g:renderErrors bean="${ECSInstance}" as="list" />
            </div>
            </g:hasErrors>
            <g:form action="save" >
                <div class="dialog">
                    <table>
                        <tbody>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="especificacionDetallada"><g:message code="ECS.especificacionDetallada.label" default="Especificacion Detallada" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: ECSInstance, field: 'especificacionDetallada', 'errors')}">
                                    <g:textField name="especificacionDetallada" value="${ECSInstance?.especificacionDetallada}" />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="alcanceDetallado"><g:message code="ECS.alcanceDetallado.label" default="Alcance Detallado" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: ECSInstance, field: 'alcanceDetallado', 'errors')}">
                                    <g:textField name="alcanceDetallado" value="${ECSInstance?.alcanceDetallado}" />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="notas"><g:message code="ECS.notas.label" default="Notas" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: ECSInstance, field: 'notas', 'errors')}">
                                    <g:textField name="notas" value="${ECSInstance?.notas}" />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="fueraAlcance"><g:message code="ECS.fueraAlcance.label" default="Fuera Alcance" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: ECSInstance, field: 'fueraAlcance', 'errors')}">
                                    <g:textField name="fueraAlcance" value="${ECSInstance?.fueraAlcance}" />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="beneficiosEsperados"><g:message code="ECS.beneficiosEsperados.label" default="Beneficios Esperados" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: ECSInstance, field: 'beneficiosEsperados', 'errors')}">
                                    <g:textField name="beneficiosEsperados" value="${ECSInstance?.beneficiosEsperados}" />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="realizaPI"><g:message code="ECS.realizaPI.label" default="Realiza PI" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: ECSInstance, field: 'realizaPI', 'errors')}">
                                    <g:checkBox name="realizaPI" value="${ECSInstance?.realizaPI}" />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="PIintegrada"><g:message code="ECS.PIintegrada.label" default="PI integrada" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: ECSInstance, field: 'PIintegrada', 'errors')}">
                                    <g:checkBox name="PIintegrada" value="${ECSInstance?.PIintegrada}" />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="realizaPAU"><g:message code="ECS.realizaPAU.label" default="Realiza PAU" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: ECSInstance, field: 'realizaPAU', 'errors')}">
                                    <g:checkBox name="realizaPAU" value="${ECSInstance?.realizaPAU}" />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="PAUintegrada"><g:message code="ECS.PAUintegrada.label" default="PAU integrada" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: ECSInstance, field: 'PAUintegrada', 'errors')}">
                                    <g:checkBox name="PAUintegrada" value="${ECSInstance?.PAUintegrada}" />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="justificacionPI"><g:message code="ECS.justificacionPI.label" default="Justificacion PI" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: ECSInstance, field: 'justificacionPI', 'errors')}">
                                    <g:select name="justificacionPI.id" from="${ar.com.telecom.pcs.entities.Justificacion.list()}" optionKey="id" value="${ECSInstance?.justificacionPI?.id}" noSelection="['null': '']" />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="justificacionPAU"><g:message code="ECS.justificacionPAU.label" default="Justificacion PAU" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: ECSInstance, field: 'justificacionPAU', 'errors')}">
                                    <g:select name="justificacionPAU.id" from="${ar.com.telecom.pcs.entities.Justificacion.list()}" optionKey="id" value="${ECSInstance?.justificacionPAU?.id}" noSelection="['null': '']" />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="fechaDeCarga"><g:message code="ECS.fechaDeCarga.label" default="Fecha De Carga" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: ECSInstance, field: 'fechaDeCarga', 'errors')}">
                                    <g:datePicker name="fechaDeCarga" precision="day" value="${ECSInstance?.fechaDeCarga}"  />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="versionECS"><g:message code="ECS.versionECS.label" default="Version ECS" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: ECSInstance, field: 'versionECS', 'errors')}">
                                    <g:textField name="versionECS" value="${fieldValue(bean: ECSInstance, field: 'versionECS')}" />
                                </td>
                            </tr>
                        
                        </tbody>
                    </table>
                </div>
                <div class="buttons">
                    <span class="button"><g:submitButton name="create" class="save" value="${message(code: 'default.button.create.label', default: 'Create')}" /></span>
                </div>
            </g:form>
        </div>
    </body>
</html>
