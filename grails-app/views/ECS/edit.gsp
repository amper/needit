

<%@ page import="ar.com.telecom.pcs.entities.ECS" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="layout" content="main" />
        <g:set var="entityName" value="${message(code: 'ECS.label', default: 'ECS')}" />
        <title><g:message code="default.edit.label" args="[entityName]" /></title>
    </head>
    <body>
        <div class="nav">
            <span class="menuButton"><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></span>
            <span class="menuButton"><g:link class="list" action="list"><g:message code="default.list.label" args="[entityName]" /></g:link></span>
            <span class="menuButton"><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></span>
        </div>
        <div class="body">
            <h1><g:message code="default.edit.label" args="[entityName]" /></h1>
            <g:if test="${flash.message}">
            <div class="message">${flash.message}</div>
            </g:if>
            <g:hasErrors bean="${ECSInstance}">
            <div class="errors">
                <g:renderErrors bean="${ECSInstance}" as="list" />
            </div>
            </g:hasErrors>
            <g:form method="post" >
                <g:hiddenField name="id" value="${ECSInstance?.id}" />
                <g:hiddenField name="version" value="${ECSInstance?.version}" />
                <div class="dialog">
                    <table>
                        <tbody>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                  <label for="especificacionDetallada"><g:message code="ECS.especificacionDetallada.label" default="Especificacion Detallada" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: ECSInstance, field: 'especificacionDetallada', 'errors')}">
                                    <g:textField name="especificacionDetallada" value="${ECSInstance?.especificacionDetallada}" />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                  <label for="alcanceDetallado"><g:message code="ECS.alcanceDetallado.label" default="Alcance Detallado" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: ECSInstance, field: 'alcanceDetallado', 'errors')}">
                                    <g:textField name="alcanceDetallado" value="${ECSInstance?.alcanceDetallado}" />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                  <label for="notas"><g:message code="ECS.notas.label" default="Notas" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: ECSInstance, field: 'notas', 'errors')}">
                                    <g:textField name="notas" value="${ECSInstance?.notas}" />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                  <label for="fueraAlcance"><g:message code="ECS.fueraAlcance.label" default="Fuera Alcance" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: ECSInstance, field: 'fueraAlcance', 'errors')}">
                                    <g:textField name="fueraAlcance" value="${ECSInstance?.fueraAlcance}" />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                  <label for="beneficiosEsperados"><g:message code="ECS.beneficiosEsperados.label" default="Beneficios Esperados" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: ECSInstance, field: 'beneficiosEsperados', 'errors')}">
                                    <g:textField name="beneficiosEsperados" value="${ECSInstance?.beneficiosEsperados}" />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                  <label for="realizaPI"><g:message code="ECS.realizaPI.label" default="Realiza PI" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: ECSInstance, field: 'realizaPI', 'errors')}">
                                    <g:checkBox name="realizaPI" value="${ECSInstance?.realizaPI}" />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                  <label for="PIintegrada"><g:message code="ECS.PIintegrada.label" default="PI integrada" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: ECSInstance, field: 'PIintegrada', 'errors')}">
                                    <g:checkBox name="PIintegrada" value="${ECSInstance?.PIintegrada}" />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                  <label for="realizaPAU"><g:message code="ECS.realizaPAU.label" default="Realiza PAU" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: ECSInstance, field: 'realizaPAU', 'errors')}">
                                    <g:checkBox name="realizaPAU" value="${ECSInstance?.realizaPAU}" />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                  <label for="PAUintegrada"><g:message code="ECS.PAUintegrada.label" default="PAU integrada" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: ECSInstance, field: 'PAUintegrada', 'errors')}">
                                    <g:checkBox name="PAUintegrada" value="${ECSInstance?.PAUintegrada}" />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                  <label for="justificacionPI"><g:message code="ECS.justificacionPI.label" default="Justificacion PI" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: ECSInstance, field: 'justificacionPI', 'errors')}">
                                    <g:select name="justificacionPI.id" from="${ar.com.telecom.pcs.entities.Justificacion.list()}" optionKey="id" value="${ECSInstance?.justificacionPI?.id}" noSelection="['null': '']" />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                  <label for="justificacionPAU"><g:message code="ECS.justificacionPAU.label" default="Justificacion PAU" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: ECSInstance, field: 'justificacionPAU', 'errors')}">
                                    <g:select name="justificacionPAU.id" from="${ar.com.telecom.pcs.entities.Justificacion.list()}" optionKey="id" value="${ECSInstance?.justificacionPAU?.id}" noSelection="['null': '']" />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                  <label for="sistemasImpactados"><g:message code="ECS.sistemasImpactados.label" default="Sistemas Impactados" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: ECSInstance, field: 'sistemasImpactados', 'errors')}">
                                    <g:select name="sistemasImpactados" from="${ar.com.telecom.pcs.entities.SistemaImpactado.list()}" multiple="yes" optionKey="id" size="5" value="${ECSInstance?.sistemasImpactados*.id}" />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                  <label for="areasImpactadas"><g:message code="ECS.areasImpactadas.label" default="Areas Impactadas" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: ECSInstance, field: 'areasImpactadas', 'errors')}">
                                    <g:select name="areasImpactadas" from="${ar.com.telecom.pcs.entities.AreaSoporte.list()}" multiple="yes" optionKey="id" size="5" value="${ECSInstance?.areasImpactadas*.id}" />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                  <label for="otrasPruebasECS"><g:message code="ECS.otrasPruebasECS.label" default="Otras Pruebas ECS" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: ECSInstance, field: 'otrasPruebasECS', 'errors')}">
                                    
<ul>
<g:each in="${ECSInstance?.otrasPruebasECS?}" var="o">
    <li><g:link controller="ECSOtrasPruebas" action="show" id="${o.id}">${o?.encodeAsHTML()}</g:link></li>
</g:each>
</ul>
<g:link controller="ECSOtrasPruebas" action="create" params="['ECS.id': ECSInstance?.id]">${message(code: 'default.add.label', args: [message(code: 'ECSOtrasPruebas.label', default: 'ECSOtrasPruebas')])}</g:link>

                                </td>
                            </tr>
                        
<%--                            <tr class="prop">--%>
<%--                                <td valign="top" class="name">--%>
<%--                                  <label for="anexosPorTipo"><g:message code="ECS.anexosPorTipo.label" default="Anexos Por Tipo" /></label>--%>
<%--                                </td>--%>
<%--                                <td valign="top" class="value ${hasErrors(bean: ECSInstance, field: 'anexosPorTipo', 'errors')}">--%>
<%--                                    <g:select name="anexosPorTipo" from="${ar.com.telecom.pcs.entities.AnexoPorTipo.list()}" multiple="yes" optionKey="id" size="5" value="${ECSInstance?.anexosPorTipo*.id}" />--%>
<%--                                </td>--%>
<%--                            </tr>--%>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                  <label for="fechaDeCarga"><g:message code="ECS.fechaDeCarga.label" default="Fecha De Carga" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: ECSInstance, field: 'fechaDeCarga', 'errors')}">
                                    <g:datePicker name="fechaDeCarga" precision="day" value="${ECSInstance?.fechaDeCarga}"  />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                  <label for="versionECS"><g:message code="ECS.versionECS.label" default="Version ECS" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: ECSInstance, field: 'versionECS', 'errors')}">
                                    <g:textField name="versionECS" value="${fieldValue(bean: ECSInstance, field: 'versionECS')}" />
                                </td>
                            </tr>
                        
                        </tbody>
                    </table>
                </div>
                <div class="buttons">
                    <span class="button"><g:actionSubmit class="save" action="update" value="${message(code: 'default.button.update.label', default: 'Update')}" /></span>
                    <span class="button"><g:actionSubmit class="delete" action="delete" value="${message(code: 'default.button.delete.label', default: 'Delete')}" onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');" /></span>
                </div>
            </g:form>
        </div>
    </body>
</html>
