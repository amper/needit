

<%@ page import="ar.com.telecom.pcs.entities.EstrategiaPrueba" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="layout" content="main" />
        <g:set var="entityName" value="${message(code: 'estrategiaPrueba.label', default: 'EstrategiaPrueba')}" />
        <title><g:message code="default.edit.label" args="[entityName]" /></title>
    </head>
    <body>
        <div class="nav">
            <span class="menuButton"><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></span>
            <span class="menuButton"><g:link class="list" action="list"><g:message code="default.list.label" args="[entityName]" /></g:link></span>
            <span class="menuButton"><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></span>
        </div>
        <div class="body">
            <h1><g:message code="default.edit.label" args="[entityName]" /></h1>
            <g:if test="${flash.message}">
            <div class="message">${flash.message}</div>
            </g:if>
            <g:hasErrors bean="${estrategiaPruebaInstance}">
            <div class="errors">
                <g:renderErrors bean="${estrategiaPruebaInstance}" as="list" />
            </div>
            </g:hasErrors>
            <g:form method="post" >
                <g:hiddenField name="id" value="${estrategiaPruebaInstance?.id}" />
                <g:hiddenField name="version" value="${estrategiaPruebaInstance?.version}" />
                <div class="dialog">
                    <table>
                        <tbody>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                  <label for="comentario"><g:message code="estrategiaPrueba.comentario.label" default="Comentario" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: estrategiaPruebaInstance, field: 'comentario', 'errors')}">
                                    <g:textField name="comentario" value="${estrategiaPruebaInstance?.comentario}" />
                                </td>
                            </tr>
                        
<%--                            <tr class="prop">--%>
<%--                                <td valign="top" class="name">--%>
<%--                                  <label for="anexos"><g:message code="estrategiaPrueba.anexos.label" default="Anexos" /></label>--%>
<%--                                </td>--%>
<%--                                <td valign="top" class="value ${hasErrors(bean: estrategiaPruebaInstance, field: 'anexos', 'errors')}">--%>
<%--                                    <g:select name="anexos" from="${ar.com.telecom.pcs.entities.AnexoPorTipo.list()}" multiple="yes" optionKey="id" size="5" value="${estrategiaPruebaInstance?.anexos*.id}" />--%>
<%--                                </td>--%>
<%--                            </tr>--%>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                  <label for="detalles"><g:message code="estrategiaPrueba.detalles.label" default="Detalles" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: estrategiaPruebaInstance, field: 'detalles', 'errors')}">
                                    
<ul>
<g:each in="${estrategiaPruebaInstance?.detalles?}" var="d">
    <li><g:link controller="detalleEstrategiaPrueba" action="show" id="${d.id}">${d?.encodeAsHTML()}</g:link></li>
</g:each>
</ul>
<g:link controller="detalleEstrategiaPrueba" action="create" params="['estrategiaPrueba.id': estrategiaPruebaInstance?.id]">${message(code: 'default.add.label', args: [message(code: 'detalleEstrategiaPrueba.label', default: 'DetalleEstrategiaPrueba')])}</g:link>

                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                  <label for="fechaCarga"><g:message code="estrategiaPrueba.fechaCarga.label" default="Fecha Carga" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: estrategiaPruebaInstance, field: 'fechaCarga', 'errors')}">
                                    <g:datePicker name="fechaCarga" precision="day" value="${estrategiaPruebaInstance?.fechaCarga}"  />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                  <label for="versionEP"><g:message code="estrategiaPrueba.versionEP.label" default="Version EP" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: estrategiaPruebaInstance, field: 'versionEP', 'errors')}">
                                    <g:textField name="versionEP" value="${fieldValue(bean: estrategiaPruebaInstance, field: 'versionEP')}" />
                                </td>
                            </tr>
                        
                        </tbody>
                    </table>
                </div>
                <div class="buttons">
                    <span class="button"><g:actionSubmit class="save" action="update" value="${message(code: 'default.button.update.label', default: 'Update')}" /></span>
                    <span class="button"><g:actionSubmit class="delete" action="delete" value="${message(code: 'default.button.delete.label', default: 'Delete')}" onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');" /></span>
                </div>
            </g:form>
        </div>
    </body>
</html>
