
<%@ page import="ar.com.telecom.pcs.entities.EstrategiaPrueba" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="layout" content="main" />
        <g:set var="entityName" value="${message(code: 'estrategiaPrueba.label', default: 'EstrategiaPrueba')}" />
        <title><g:message code="default.list.label" args="[entityName]" /></title>
    </head>
    <body>
        <div class="nav">
            <span class="menuButton"><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></span>
            <span class="menuButton"><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></span>
        </div>
        <div class="body">
            <h1><g:message code="default.list.label" args="[entityName]" /></h1>
            <g:if test="${flash.message}">
            <div class="message">${flash.message}</div>
            </g:if>
            <div class="list">
                <table>
                    <thead>
                        <tr>
                        
                            <g:sortableColumn property="id" title="${message(code: 'estrategiaPrueba.id.label', default: 'Id')}" />
                        
                            <g:sortableColumn property="comentario" title="${message(code: 'estrategiaPrueba.comentario.label', default: 'Comentario')}" />
                        
                            <g:sortableColumn property="fechaCarga" title="${message(code: 'estrategiaPrueba.fechaCarga.label', default: 'Fecha Carga')}" />
                        
                            <g:sortableColumn property="versionEP" title="${message(code: 'estrategiaPrueba.versionEP.label', default: 'Version EP')}" />
                        
                        </tr>
                    </thead>
                    <tbody>
                    <g:each in="${estrategiaPruebaInstanceList}" status="i" var="estrategiaPruebaInstance">
                        <tr class="${(i % 2) == 0 ? 'odd' : 'even'}">
                        
                            <td><g:link action="show" id="${estrategiaPruebaInstance.id}">${fieldValue(bean: estrategiaPruebaInstance, field: "id")}</g:link></td>
                        
                            <td>${fieldValue(bean: estrategiaPruebaInstance, field: "comentario")}</td>
                        
                            <td><g:formatDate date="${estrategiaPruebaInstance.fechaCarga}" /></td>
                        
                            <td>${fieldValue(bean: estrategiaPruebaInstance, field: "versionEP")}</td>
                        
                        </tr>
                    </g:each>
                    </tbody>
                </table>
            </div>
            <div class="paginateButtons">
                <g:paginate total="${estrategiaPruebaInstanceTotal}" />
            </div>
        </div>
    </body>
</html>
