<g:updateToken />
 
<label>Grupo referente:</label>
<p>
	<g:if test="${grupo!=null}">
		<b>${grupo}</b> 
	</g:if>
	<g:else>
		<center><b>No existe grupo asociado al Sistema/Tipo de impacto seleccionado</b></center> 
	</g:else>
</p>
<g:hiddenField id="grupoReferenteLDAP" name="grupoReferenteLDAP" value="${grupo}" />

