<g:javascript library="especificacionCambio" />
<script type="text/javascript" >
jQuery(document).ready(
function($) {
	$("#comboJustificacion${index}").combobox();
	$("#realiza${estrategia.tipoPrueba}").change(function() {
		toggleJustificacion${estrategia.tipoPrueba}(this.checked);
	});
});
</script>
<div id="EstPruebas_seccion${index}">
	<div>
		${campoRealiza}
		<label for="realiza${estrategia.tipoPrueba}">Realiza ${estrategia.tipoPrueba}</label>
	</div>
	
	<div id="Justificacion${estrategia.tipoPrueba}">
		<label class="formLabel2 labelCombo" for='comboJustificacion${index}'>Justificaci&oacute;n:</label>
		<g:select id="comboJustificacion${index}"
			name="justificacion${estrategia.tipoPrueba}.id" from="${pruebas}" optionKey="id"
			value="${justificacion?.id}"
			noSelection="['null': '']" />
	</div>
	 
	<div id="${estrategia.tipoPrueba}Consolidadasdiv">
		${campoConsolida}
		<label for="${estrategia.tipoPrueba}Consolidadas">${estrategia.tipoPrueba} Consolidada</label>
	</div>
</div>

<script type="text/javascript" >
	completaEstrategiasPrueba($('comboboxTipoGestion').value, false);
</script>
