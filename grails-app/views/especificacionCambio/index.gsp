<%@page import="ar.com.telecom.Constantes"%>
<%@page import="ar.com.telecom.pcs.entities.TipoGestion"%>
<%@page import="ar.com.telecom.FaseMultiton"%>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta name="layout" content="mainApp" />
<g:javascript library="boxUser" />
<g:javascript library="especificacionCambio" />
<g:javascript library="tiny_mce/tiny_mce" />
<g:javascript library="anexoPorTipo" />
<g:javascript library="busquedaRapida" />
 
<script type="text/javascript">

tinyMCE.init({
    mode : "exact",
    elements : "especificacionDetallada,fueraAlcance,alcanceDetallado,notas,beneficiosEsperados",
    theme : "advanced",
	skin : "o2k7", 
    plugins : "autolink,lists,pagebreak,style,layer,table,save,advhr,advimage,advlink,emotions,iespell,inlinepopups,insertdatetime,preview,media,searchreplace,print,contextmenu,paste,directionality,fullscreen,noneditable,visualchars,nonbreaking,xhtmlxtras,template,wordcount,advlist", 
            
    // Theme options - button# indicated the row# only
    theme_advanced_buttons1 : "newdocument,print,|,bold,italic,underline,|,fontsizeselect,|,cut,copy,paste",
    theme_advanced_buttons2 : "bullist,numlist,|,undo,redo,|,forecolor,backcolor,|,image,|,fullscreen,template",
    theme_advanced_buttons3 : "",      
    theme_advanced_toolbar_location : "top",
    theme_advanced_toolbar_align : "left",
    theme_advanced_statusbar_location : "bottom",
    theme_advanced_resizing : true,
    paste_text_sticky : true,
    setup : function(ed) {
        ed.onInit.add(function(ed) {
          ed.pasteAsPlainText = true;
        });
    },
	template_external_list_url : "lists/template_list.js",

		//template_external_list_url : "lists/template_list.js",
		external_link_list_url : "lists/link_list.js",
		external_image_list_url : "lists/image_list.js",
		media_external_list_url : "lists/media_list.js"
});

</script>

</head>
<body>

	<div class="contenedorTitulo">
		<g:titulo pedido="${pedidoInstance}" />
		<g:workflow pedidoId="${pedidoInstance?.id}" />
	</div>

	<div id="contenedorIzq">
		<g:if test="${flash.message}">
			<div class="message">
				${flash.message}
			</div>
		</g:if>
		<g:hasErrors bean="${pedidoInstance}">
			<div class="errors">
				<g:renderErrors bean="${pedidoInstance}" as="list" />
			</div>
		</g:hasErrors>

		<g:form id="formPedido" name="formPedido" class="formGeneral"  action="grabarFormulario" useToken="true">
			<g:hiddenField name="id" value="${pedidoInstance?.id}" />
			<g:hiddenField name="hVengoPorAnexos" value="${params.controller}" />
			<div class="seccionHide">
			<g:cabeceraPedido pedido="${pedidoInstance}" />
			<g:render template="/templates/workflowHidden"/>
			<g:set scope="page" var="ecs" value="${pedidoInstance?.obtenerUltimaEspecificacion()}"/>
			
			<p class="tituloSeccion">Especificaci&oacute;n Cambio - Versi&oacute;n ${ecs?.versionECS}</p>
			<div class="formSeccion">
				<div class="pTres">
					<label class="formLabel" for='comboboxTipoPedido'>Tipo de
						Pedido</label>
					<g:select id="comboboxTipoPedido"
						class="${hasErrors(bean: pedidoInstance, field: 'tipoPedido?.id', 'errors')}"
						name="tipoPedido.id"
						from="${ar.com.telecom.pcs.entities.TipoPedido.list()}"
						optionKey="id" value="${pedidoInstance?.tipoPedido?.id}"
						noSelection="['null': '']" />
				</div>

				<div class="pTres" title="${TipoGestion.tooltip }">
					<label class="formLabel validate" for='comboboxTipoGestion'>Tipo de
						Gesti&oacute;n</label>
					<g:select id="comboboxTipoGestion"
						class="${hasErrors(bean: pedidoInstance, field: 'tipoGestion?.id', 'errors')}"
						name="tipoGestion.id"
						from="${ar.com.telecom.pcs.entities.TipoGestion.list()}"
						optionKey="id" value="${pedidoInstance?.tipoGestion?.id}"
						noSelection="['null': '']" />
				</div>
				
				<div class="pTres" style="width: 28%;">
					<label class='formLabel'>Prioridad</label>
					<g:select id="comboPrioridad" name="prioridad.id"
						class="${hasErrors(bean: pedidoInstance, field: 'prioridad?.id', 'errors')}"
						from="${ar.com.telecom.pcs.entities.Prioridad.list()}"
						optionKey="id" value="${pedidoInstance?.prioridad?.id}"
						noSelection="['null': '']" />
				</div>
			</div>
		</div>

	<div id="versionECS" style="border: 2px solid #9FB6CD;background-color=#9FB6CD;">
		<div class="seccionHide2">
			<p class="tituloSeccion">Especificaci&oacute;n</p>
			<div class="formSeccion">
				<div class="pDos">
					<label for='especDetallada' class="formLabel validate">Especificaci&oacute;n
						detallada:</label>
					<g:textArea id="especificacionDetallada" name="especificacionDetallada"
						value="${ecs?.especificacionDetallada}"></g:textArea>
				</div>

				<div class="pDos">
					<label for='fueraDetallado' class="formLabel">Fuera de
						alcance detallado:</label>
					<g:textArea id="fueraAlcance" name="fueraAlcance"
						value="${ecs?.fueraAlcance}"></g:textArea>
				</div>

				<div class="pDos">
					<label for='inputBeneficios' class="formLabel validate">Alcance
						detallado:</label>
					<g:textArea id="alcanceDetallado" name="alcanceDetallado"
						value="${ecs?.alcanceDetallado}"></g:textArea>
				</div>

				<div class="pDos">
					<label for="inputRiesgos" class="formLabel">Beneficios
						esperados:</label>
					<g:textArea name="beneficiosEsperados"
						value="${ecs?.beneficiosEsperados}"></g:textArea>
				</div>

				<div class="pDos">
					<label for="notasDev" class="formLabel" title="${Constantes.instance?.parametros?.tooltipNotasDesarrollo}">Notas para el
						desarrollo e implantaci&oacute;n:</label>
					<g:textArea id="notas" name="notas" title="${Constantes.instance?.parametros?.tooltipNotasDesarrollo}"
						value="${ecs?.notas}"></g:textArea>
				</div>

				<div class="pDos">
					<label for="notasDev" class="formLabel">
					</label>
				</div>
			</div>
				
			<p class="tituloSeccion">Estrategia de pruebas</p>
			<div class="formSeccion">

				<div id="estrategiaPruebas">
				</div>

				<div id="EstPruebas_seccion3">
					<label for='nuevaPrueba' class="formLabel">Prueba opcional</label>
					<a href="" id="nuevaPrueba" class="agregarItem">agregar</a>

					<div id="pruebasAdicionales">
						<g:render template="/templates/tablaPruebasAdicionales" model="${['pruebasOpcionalesSeleccionadas': ecs?.otrasPruebasECS, 'edita':true]}"></g:render>
					</div>
				</div>

			</div>
	
			<p class="tituloSeccion">Sistemas y &aacute;reas de soporte impactados</p>
			<div class="formSeccion">

				<div class="separatorGrande"></div>
				
				<div class="pCompleto">
					<label for='nuevoSistema' class="formLabel validate">Sistema impactado</label>
					<a href="javascript:showPopUpSistemas('${pedidoInstance?.id}');" id="nuevoSistema" class="agregarItem">agregar</a>
					<div id="sistemasImpactados" class="tablaSeleccion">
						<g:render template="/templates/tablaSistemasImpactados"
							model="${['sistemasImpactadosSeleccionados': ecs?.sistemasImpactados, 'visible':true]}"></g:render>
					</div>
				</div>

				<div class="pCompleto">
					<label for='nuevaAreaSoporte' class="formLabel">&Aacute;reas de soporte</label>
					<a href="" id="nuevaArea" class="agregarItem">agregar</a>
					<div id="areasSoporte" class="tablaSeleccion">
						<g:render template="/templates/tablaAreasSoporte"
							model="${['areasSoporteSeleccionadas': ecs?.areasImpactadas, 'visible':true]}">
						</g:render>
					</div>
				</div>
			</div>

			<p class="tituloSeccion">Anexos</p>
			<div class="formSeccion">
			<div class="pDos">
			<label for='nuevoAnexo' class="formLabel">Anexos</label>
			<button id="nuevoAnexo" class="formSeccionSubmitButton">Nuevo anexo</button>
			<br>
			<br>
			<g:hiddenField name="hForm" id="hForm" value="formPedido"/>
			<g:render template="/templates/tablaAnexos"
     			model="['fase': FaseMultiton.ESPECIFICACION_CAMBIO, 
     			'editable': true, 'pedidoInstance': pedidoInstance, 'origen': ecs ]">
		    </g:render>
			</div>
				<div class="pDos">
					<label for="justificacionAprobacion" class="formLabel">Justificaci&oacute;n</label>
					<g:textArea name="justificacionAprobacion"
						value="${justificacionAprobacion}"></g:textArea>
				</div>
			</div>
			
<%--			<g:if test="${pedidoInstance?.getJustificacionAC()}">--%>
<%--				<p class="tituloSeccion">Justificaci&oacute;n de desaprobaci&oacute;n</p>--%>
<%--				<div class="formSeccion">--%>
<%--					<p class="info">&nbsp;${pedidoInstance?.getJustificacionAC()}</p>--%>
<%--				</div>--%>
<%--			</g:if>--%>
				<g:render template="/templates/ultimaJustificacionDenegacion"
						     			model="['pedidoInstance': pedidoInstance, 'fase': pedidoInstance.faseEspecificacionCambio()]"></g:render>
			
			</div>
			</div>
		<div>
			<div class="formSeccionButton" style="margin-top: 20px;">
				<g:actionSubmit class="formSeccionSubmitButton" id="Simular"
					action="simularValidacion" value="Simular" />
				<g:actionSubmit class="formSeccionSubmitButton" id="Guardar" action="grabarFormulario" value="Guardar" />
				<g:if test="${!pedidoInstance.estaSuspendido()}">
					<g:actionSubmit class="formSeccionSubmitButton" id="Denegar"
						action="denegarPedido" value="Denegar" />
					<g:actionSubmit class="formSeccionSubmitButton" id="Aprobar"
						action="aprobarPedido" value="Aprobar" />
				</g:if>
			</div>
	</div>
	</g:form>
	</div>

	<!-- Lateral Derecho -->
	<g:render template="/templates/menuLateral" model="['pedidoInstance':pedidoInstance]"/>
	<!-- Fin Lateral Derecho -->

<%--popup Pruebas AGREGAR--%>
	<div id="dialog-NuevaPrueba" title="Ingreso prueba opcional" style="display: none;"></div>
<!--	fin popup-->
  
  
<%--popup Pruebas MODIFICAR--%>
<button id="modificaPrueba" style="display:none;"></button>
<button id="cierraModificaPrueba" style="display:none;"></button>
<button id="cierraSistImpactado" style="display:none;"></button>

	<div id="dialog-ModificaPrueba" title="Modifica prueba opcional" style="display: none;">
		<div style="overflow: auto;">
			<g:form name="formPruebasOpc">
				<g:hiddenField name="pedidoId" value="${pedidoInstance.id}" />
				<div class="pCompleto">
					<label class="formLabel2">Prueba:</label><br>
					<p class="info" id="otraPruebaAModificar"></p>
				</div>
				<div class="separator"></div>
				<div class="separator-sinmargen"></div>
				<div class="pCompleto">
					<label class="formLabel2" for='consolidaAModificar'>Consolidada:</label><br>
					<g:checkBox id="consolidaAModificar" name="consolidaAModificar" class="inputCorto" style="width: 30%; margin-left: 0px;"/>
				</div>
				<br>
				<div class="separator"></div>
				<div align="center">
					<button id="btnCancelarCargaOtrasPruebas" value="Cancelar" class="formSeccionSubmitButtonAnexo" name="Cancelar" onClick="cerrarPopUpCargaOtrasPruebas();">Cerrar</button>
					&nbsp;
					<button id="aceptaModificarPrueba" class="formSeccionSubmitButtonAnexo">Modificar</button>
				</div>
				<input type="hidden" id="posPruebaModif" name="posPruebaModif" />
			</g:form>
		</div>
	</div>
<!--	fin popup-->  
	
<!-- popup Agregar Area Soporte -->	
	<button id="btnCerrarPopAreaSoporte" style="display: none;"></button>
	<div id="dialog-NuevaAreaSoporte" title="Ingreso &aacute;rea soporte" style="display:none;">
<%--		<g:render template="areaSoportePopUp"  model="['pedidoInstance': pedidoInstance]"></g:render>--%>
	</div>
	<%-- fin Agregar Area Soporte--%>

	<%-- popup Agregar Sistemas impactados--%>
	<div id="dialog-NuevoSistemaImpactado" title="Ingreso sistema impactado" style="display:none;">
	</div>
	
	<%-- fin Agregar Sistemas impactados--%>
	
	<div id="dialog-anexos" title="Nuevo Anexo" style="display: none;"></div>

	<script type="text/javascript">
		completaEstrategiasPrueba("${pedidoInstance?.tipoGestion?.id}", true);
	</script>
	
</body>
</html>