<%@page import="ar.com.telecom.pcs.entities.TipoGestion"%>
<%@page import="ar.com.telecom.Constantes"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta name="layout" content="mainApp" />
<g:javascript library="boxUser" />
<g:javascript library="especificacionCambio" />
<g:javascript library="tiny_mce/tiny_mce" />

 
<script type="text/javascript">
tinyMCE.init({
    mode : "exact",
    elements : "notas,beneficiosEsperados,alcanceDetallado,fueraAlcance,especificacionDetallada",
    theme : "advanced",
    readonly : true,
	skin : "o2k7",
    plugins : "autolink,lists,pagebreak,style,layer,table,save,advhr,advimage,advlink,emotions,iespell,inlinepopups,insertdatetime,preview,media,searchreplace,print,contextmenu,paste,directionality,fullscreen,noneditable,visualchars,nonbreaking,xhtmlxtras,template,wordcount,advlist,autosave", 
            
    // Theme options - button# indicated the row# only
    theme_advanced_buttons1 : "newdocument,print,|,bold,italic,underline,|,fontsizeselect,|,cut,copy,paste,|,bullist,numlist,|,undo,redo,|,forecolor,backcolor,|,image,|,fullscreen,template",
    theme_advanced_buttons2 : "",
    theme_advanced_buttons3 : "",      
    theme_advanced_toolbar_location : "top",
    theme_advanced_toolbar_align : "left",
    theme_advanced_statusbar_location : "bottom",
    theme_advanced_resizing : true,
    paste_text_sticky : true,
    setup : function(ed) {
        ed.onInit.add(function(ed) {
          ed.pasteAsPlainText = true;
        });
    },
	template_external_list_url : "lists/template_list.js",

		//template_external_list_url : "lists/template_list.js",
		external_link_list_url : "lists/link_list.js",
		external_image_list_url : "lists/image_list.js",
		media_external_list_url : "lists/media_list.js"
});
</script>



</head>
<body>

	<div class="contenedorTitulo">
		<g:titulo pedido="${pedidoInstance}" />
		<g:workflow pedidoId="${pedidoInstance?.id}" />
	</div>

	<div id="contenedorIzq">
		<g:if test="${flash.message}">
			<div class="message">
				${flash.message}
			</div>
		</g:if>
		<g:hasErrors bean="${pedidoInstance}">
			<div class="errors">
				<g:renderErrors bean="${pedidoInstance}" as="list" />
			</div>
		</g:hasErrors>
		<g:form class="formGeneral" useToken="true">
			<g:hiddenField name="id" value="${pedidoInstance?.id}" />
			<g:render template="/templates/workflowHidden"/>

			<g:set scope="page" var="ecs" value="${pedidoInstance?.obtenerUltimaEspecificacion()}"/>
			<div class="seccionHide">
				<g:cabeceraPedido pedido="${pedidoInstance}" />
				<p class="tituloSeccion">Especificaci&oacute;n Cambio - Versi&oacute;n ${ecs.versionECS}</p>
				<div class="formSeccion">
					<div class="pTres" style="margin-left: 20px;">
						<label class="formLabel" for='comboboxTipoPedido'>Tipo de Pedido</label>
						<p class="info">${pedidoInstance?.tipoPedido}</p>
					</div>
	
					<div class="pTres" style="margin-left: 20px;" title="${TipoGestion.tooltip }">
						<label class="formLabel" for='comboboxTipoGestion'>Tipo de Gesti&oacute;n</label>
						<p class="info">${pedidoInstance?.tipoGestion}</p>
					</div>
					
					<div class="pTres" style="margin-left: 20px; width: 28%;">
						<label class="formLabel" for='comboPrioridad'>Prioridad</label>
						<p class="info">${pedidoInstance?.prioridad}</p>
					</div>
				</div>
			</div>
			
			<div id="versionECS" style="border: 2px solid #9FB6CD;background-color=#9FB6CD;">
				<div class="seccionHide2">
					<p class="tituloSeccion">Especificaci&oacute;n</p>
					<div class="formSeccion">
						<div class="pDos">
							<label class="formLabel">Especificaci&oacute;n detallada:</label>
							<g:textArea id="especificacionDetallada" name="especificacionDetallada"	value="${ecs?.especificacionDetallada}"></g:textArea>
						</div>
						<div class="pDos">
							<label class="formLabel">Fuera de alcance detallado:</label>
							<g:textArea id="fueraAlcance" name="fueraAlcance" value="${ecs?.fueraAlcance}"></g:textArea>
						</div>
		
						<div class="pDos">
							<label class="formLabel">Alcance detallado:</label>
							<g:textArea id="alcanceDetallado" name="alcanceDetallado" value="${ecs?.alcanceDetallado}"></g:textArea>
						</div>
		
						<div class="pDos">
							<label class="formLabel">Beneficios esperados:</label>
							<g:textArea id="beneficiosEsperados" name="beneficiosEsperados" value="${ecs?.beneficiosEsperados}"></g:textArea>
						</div>
		
						<div class="pDos">
							<label class="formLabel" title="${Constantes.instance?.parametros?.tooltipNotasDesarrollo}">Notas para el desarrollo e implantaci&oacute;n:</label>
							<g:textArea id="notas" name="notas" value="${ecs?.notas}" title="${Constantes.instance?.parametros?.tooltipNotasDesarrollo}"></g:textArea>
						</div>
		
						<div class="pDos">	
							<label class="formLabel">
							</label>
						</div>

					</div>
					
					<p class="tituloSeccion">Estrategia de pruebas</p>
					<div class="formSeccion">
						<div class="pTres">
							<div>
								<label for="realizaPI">Realiza PI: </label>
								<g:checkBoxStatusImage estado="${ecs?.realizaPI}" />
							</div>
							<br>
							<g:if test="${ecs.realizaPI != null}">
								<g:if test="${!ecs?.realizaPI}">
									<div id="JustificacionPI">
										<label class="formLabel2" for='comboJustificacion1'>Justificaci&oacute;n:</label>
										<p class="info">${ecs?.justificacionPI}</p>
									</div>
									<br>
								</g:if>				
								<div id="PIConsolidadasdiv">
									<label for="PIConsolidadas">PI Consolidadas: </label>
									<g:checkBoxStatusImage estado="${ecs?.PIintegrada}" />
								</div>
							</g:if>
						</div>
		
						<div class="pTres">
							<div>
								<label for="realizaPAU">Realiza PAU: </label>
								<g:checkBoxStatusImage estado="${ecs?.realizaPAU}" />
							</div>
							<br>
							<g:if test="${ecs.realizaPAU != null}">
								<g:if test="${!ecs?.realizaPAU}">
									<div id="JustificacionPAU">
										<label class="formLabel2" for='comboJustificacion2'>Justificaci&oacute;n:</label>
										<p class="info">${ecs?.justificacionPAU}</p>
									</div>
									<br>
								</g:if>
								<div id="PAUConsolidadasdiv">
									<label for="PAUintegrada">PAU Consolidadas: </label>
									<g:checkBoxStatusImage estado="${ecs?.PAUintegrada}" />
								</div>
							</g:if>
						</div>
						<div class="pTres">
							<div id="pruebasAdicionales">
								<g:render template="/templates/tablaPruebasAdicionales" model="${['pruebasOpcionalesSeleccionadas': ecs?.otrasPruebasECS, 'edita':false]}"></g:render>
							</div>
						</div>
					</div>
		
					<p class="tituloSeccion">Sistemas y &aacute;reas de soporte impactados</p>
					<div class="formSeccion">
		
						<div class="pDos" style="width: 67% !important;">
							<div id="sistemasImpactados">
								<g:render template="/templates/tablaSistemasImpactados"
									model="${['sistemasImpactadosSeleccionados': ecs?.sistemasImpactados, 'visible':false]}"></g:render>
							</div>
						</div>
		
						<div class="pDos" style="width: 30% !important;">
							<div id="areasSoporte">
								<g:render template="/templates/tablaAreasSoporte"
									model="${['areasSoporteSeleccionadas': ecs?.areasImpactadas, 'visible':false]}">
								</g:render>
							</div>
						</div>
					</div>

					<p class="tituloSeccion">Anexos</p>
					<div class="formSeccion">
						<div class="pDos">
						
							<g:if test="${ecs?.anexosPorTipo}">
								 <table>
									     <tr>
									         <g:sortableColumn property="archivo" title="Archivo" />
									         <g:sortableColumn property="tipoAnexo" title="Tipo de anexo" />
									         <g:sortableColumn property="descripcion" title="Descripci&oacute;n" />
									     </tr>
								
										<g:each in="${ecs?.anexosPorTipo}" var="arch">
								 			 <tr>
											      <td><g:link action="verAnexo" target="_blank" params="[anexoId: arch.id]">${arch}</g:link></td>
											      <td>${arch?.tipoAnexo}</td>
											      <td>${arch?.descripcion}</td>
								        	 </tr>
										</g:each>
								</table>								
							</g:if>
							<g:else>
								No se adjuntaron anexos
							</g:else>
						</div>
						
					<g:if test="${pedidoInstance?.getJustificacionCC()}">
						<div class="pDos">
							<label for="justificacionAprobacion" class="formLabel">Justificaci&oacute;n</label>
							<p class="info">${pedidoInstance?.getJustificacionCC()}</p>
						</div>
					</g:if>
				</div>
				
<%--				<g:if test="${pedidoInstance?.getJustificacionAC()}">--%>
<%--					<p class="tituloSeccion">Justificaci&oacute;n de desaprobaci&oacute;n</p>--%>
<%--					<div class="formSeccion">--%>
<%--						<p class="info">&nbsp;${pedidoInstance?.getJustificacionAC()}</p>--%>
<%--					</div>--%>
<%--				</g:if>--%>
				
					<g:render template="/templates/ultimaJustificacionDenegacion"
						     			model="['pedidoInstance': pedidoInstance, 'fase': pedidoInstance.faseEspecificacionCambio()]"></g:render>
						     							
				</div>
				</div>
				
			<div id="message"></div>
		</g:form>
			
	</div>
	<!-- fin contenedor izq -->

	<!-- Lateral Derecho -->
	<g:render template="/templates/menuLateral" model="['pedidoInstance':pedidoInstance]"/>
	<!-- Fin Lateral Derecho -->
	
	<!-- 
	************
	************
	MODALS POPUP
	************
	************
	************
	-->

	<!-- popup ayuda seccion -->
	<div id="dialog-help" title="Ayuda Aprobar Pedido"
		style="display: none;">
		<div style="height: 350px; width: 100%; overflow: auto;">
			<p class="helpTitle">Seccion ayuda</p>
			<p>Hola esto es una seccion de ayuda Hola esto es una seccion de</p>
		</div>
	</div>
	<!-- fin popup ayuda seccion -->
</body>
</html>
