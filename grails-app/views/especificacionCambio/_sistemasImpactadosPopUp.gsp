<g:updateToken pedido="${pedidoInstance}"/>

<div id="mensajes">
	<g:if test="${flash.message}">
		<div class="message">
			${flash.message}
		</div>
	</g:if>
</div>

<g:hasErrors bean="${pedidoInstance}">
	<div class="errors" id="errors">
		<g:renderErrors bean="${pedidoInstance}" as="list" />
	</div>
</g:hasErrors>
 
<div style="overflow:auto;">
	<g:form name="formNuevoSistemaImpactado" useToken="true">
			<g:hiddenField name="pedidoId" value="${pedidoInstance.id}" />
			<div class="pCompletoPopUp">
				<label for='combobox'>Sistema:</label>
				<input id="sistemaImpactoField" data-name="sistemaImpacto" data-url="/registroPedido/busquedaAutocomplete" data-method-name="obtenerSistemas" class="busqueda-rapida formInput" />
				
			</div>
		
			<div class="pCompletoPopUp">
				<label for='combobox'>Tipo de impacto:</label>
				<g:select id="comboTipoImpacto"  name="tipoImpacto.id" from="${tiposImpactoSistemas}" value="${params.tipoImpacto?.id}" optionKey="id" noSelection="['null': '']" />
			</div>

				<div id="refDiv"> 
					<div id="grupoReferenteDiv" class="pCompletoPopUp"></div>
					<div id="usuarioReferente" class="pCompletoPopUp">
						<label>Referente<a href="javascript:deleteUserBox();" class="limpiarBox"></a></label>
							<div id="legajoUsuarioReferente" class="boxUser" style="margin:0px!important">
								<g:render template="/templates/userBox" model="['box':'legajoUsuarioReferente', 'personsAutocomplete': personsAutocomplete, 'person':params.legajoUsuarioReferente]"></g:render>
							</div>
					</div>
				
<%--					<div class="pCompletoPopUp" style="padding-top:10px;">--%>
						<center>
							<button id="btnCerrarPopSist" onClick="cerrarPopUpNuevoSistImpactado();" class="formSeccionSubmitButtonAnexo">Cerrar</button>
						
							<g:submitToRemoteSecure
								class="formSeccionSubmitButtonAnexo"
								action="agregarNuevoSistemaImpactado"
								update="dialog-NuevoSistemaImpactado" onComplete="recargaSistemasImpactados()" 
								value="Agregar"></g:submitToRemoteSecure>
						</center>
<%--					</div>--%>
				</div>
	</g:form>
</div>