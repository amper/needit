<g:updateToken pedido="${pedidoInstance}"/>

<script type="text/javascript">
	jQuery(document).ready(
		function($) {
			$("#comboAreasSoporte").combobox();
		});
</script>

<div id="mensajes">
	<g:if test="${flash.message}">
		<div class="message">
			${flash.message}
		</div>
	</g:if>
</div>

<g:hasErrors bean="${pedidoInstance}">
	<div class="errors">
		<g:renderErrors bean="${pedidoInstance}" as="list" />
	</div>
</g:hasErrors>
 

<div style="overflow: auto;">
	<g:form name="formNuevaAreaSoporte" useToken="true">
		<g:hiddenField name="pedidoId" value="${pedidoInstance?.id}" />
		  <div class="pCompletoPopUp">
			<label class="formLabel" for="comboAreasSoporte">&Aacute;rea soporte: </label>
			
			<div class="separator"></div>
			
			<g:select id="comboAreasSoporte"
				name="comboAreasSoporte.id"
				from="${ar.com.telecom.pcs.entities.AreaSoporte.list()}"
				style="width: 80% !important;"
				optionKey="id" />
			
			</div>
		
			<div class="pCompleptoPopUp" style="padding-top:10px;">	
			<center>	
			
<%--				<input class="formSeccionSubmitButtonAnexo" onclick="agregaAreaSoporte(this.form);" type="button" value="Agregar"/>--%>
				<button id="btnCerrarPopSist" onClick="cerrarPopUpAreasSoporte();" class="formSeccionSubmitButtonAnexo">Cerrar</button>
				
				<g:submitToRemoteSecure
					class="formSeccionSubmitButtonAnexo"
					action="agregarNuevaAreaSoporte"
					update="dialog-NuevaAreaSoporte" onComplete="recargaAreasSoporte()" 
					value="Agregar"></g:submitToRemoteSecure>
					
<%--				<g:submitToRemote--%>
<%--					class="formSeccionSubmitButtonAnexo"--%>
<%--					action="agregarNuevaAreaSoporte"--%>
<%--					update="dialog-NuevaAreaSoporte" onComplete="recargaAreasSoporte('${pedidoInstance.id}');" --%>
<%--					value="Agregar" />--%>

			</center>
			</div>
	</g:form>
</div>