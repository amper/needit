<g:updateToken pedido="${pedidoInstance}"/>

<script type="text/javascript">
	jQuery(document).ready(
		function($) {
			jQuery("#comboOtrasPruebas").combobox();
		});
</script>

<div style="overflow: auto;">
	<g:form name="formPruebasOpc">
		<g:hiddenField name="pedidoId" value="${pedidoInstance.id}" />
		<div id="mensajes">
			<g:if test="${flash.message}">
				<div class="message">
					${flash.message}
				</div>
			</g:if>
		</div>		
		 <div class="pCompletoPopUp">
			<label class="formLabel" for='comboOtrasPruebas'>Otras Pruebas:</label>
			<div class="separator"></div>
			<g:select id="comboOtrasPruebas" class="${hasErrors(bean: pedidoInstance, field: 'tipoGestion?.id', 'errors')}" name="comboOtrasPruebas.id"  from="${otrasPruebas}" optionKey="id" noSelection="['null': '']" />
		</div>
	 
		<div class="pCompletoPopUp">
			<label class="formLabel" for="consolida">Consolidada:</label>
			<g:checkBox id="consolida" name="consolida" style="width:60px !important;"/>
		</div>
	
	<div class="separator-sinmargen">
	</div>

	<center>			
		<button id="btnCancelarPruebaOpcional" class="formSeccionSubmitButtonAnexo" value="Cancelar" name="Cancelar" onClick="cerrarPopUpPruebaOpcional();">Cerrar</button>
		<g:submitToRemoteSecure class="formSeccionSubmitButtonAnexo" controller="especificacionCambio" action="agregarOtraPrueba" onComplete="recargaTablaPruebaOpcional()" update="dialog-NuevaPrueba" value="Agregar" />
	</center>
	</g:form>
</div>