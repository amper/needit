

<%@ page import="ar.com.telecom.pcs.entities.CargaHoraIncurrida" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="layout" content="main" />
        <g:set var="entityName" value="${message(code: 'cargaHoraIncurrida.label', default: 'CargaHoraIncurrida')}" />
        <title><g:message code="default.edit.label" args="[entityName]" /></title>
    </head>
    <body>
        <div class="nav">
            <span class="menuButton"><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></span>
            <span class="menuButton"><g:link class="list" action="list"><g:message code="default.list.label" args="[entityName]" /></g:link></span>
            <span class="menuButton"><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></span>
        </div>
        <div class="body">
            <h1><g:message code="default.edit.label" args="[entityName]" /></h1>
            <g:if test="${flash.message}">
            <div class="message">${flash.message}</div>
            </g:if>
            <g:hasErrors bean="${cargaHoraIncurridaInstance}">
            <div class="errors">
                <g:renderErrors bean="${cargaHoraIncurridaInstance}" as="list" />
            </div>
            </g:hasErrors>
            <g:form method="post" >
                <g:hiddenField name="id" value="${cargaHoraIncurridaInstance?.id}" />
                <g:hiddenField name="version" value="${cargaHoraIncurridaInstance?.version}" />
                <div class="dialog">
                    <table>
                        <tbody>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                  <label for="cantidadHoras"><g:message code="cargaHoraIncurrida.cantidadHoras.label" default="Cantidad Horas" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: cargaHoraIncurridaInstance, field: 'cantidadHoras', 'errors')}">
                                    <g:textField name="cantidadHoras" value="${fieldValue(bean: cargaHoraIncurridaInstance, field: 'cantidadHoras')}" />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                  <label for="softwareFactory"><g:message code="cargaHoraIncurrida.softwareFactory.label" default="Software Factory" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: cargaHoraIncurridaInstance, field: 'softwareFactory', 'errors')}">
                                    <g:select name="softwareFactory.id" from="${ar.com.telecom.pcs.entities.SoftwareFactory.list()}" optionKey="id" value="${cargaHoraIncurridaInstance?.softwareFactory?.id}"  />
                                </td>
                            </tr>
                        
                        </tbody>
                    </table>
                </div>
                <div class="buttons">
                    <span class="button"><g:actionSubmit class="save" action="update" value="${message(code: 'default.button.update.label', default: 'Update')}" /></span>
                    <span class="button"><g:actionSubmit class="delete" action="delete" value="${message(code: 'default.button.delete.label', default: 'Delete')}" onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');" /></span>
                </div>
            </g:form>
        </div>
    </body>
</html>
