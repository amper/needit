
<%@ page import="ar.com.telecom.pcs.entities.CargaHoraIncurrida" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="layout" content="main" />
        <g:set var="entityName" value="${message(code: 'cargaHoraIncurrida.label', default: 'CargaHoraIncurrida')}" />
        <title><g:message code="default.list.label" args="[entityName]" /></title>
    </head>
    <body>
        <div class="nav">
            <span class="menuButton"><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></span>
            <span class="menuButton"><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></span>
        </div>
        <div class="body">
            <h1><g:message code="default.list.label" args="[entityName]" /></h1>
            <g:if test="${flash.message}">
            <div class="message">${flash.message}</div>
            </g:if>
            <div class="list">
                <table>
                    <thead>
                        <tr>
                        
                            <g:sortableColumn property="id" title="${message(code: 'cargaHoraIncurrida.id.label', default: 'Id')}" />
                        
                            <g:sortableColumn property="cantidadHoras" title="${message(code: 'cargaHoraIncurrida.cantidadHoras.label', default: 'Cantidad Horas')}" />
                        
                            <th><g:message code="cargaHoraIncurrida.softwareFactory.label" default="Software Factory" /></th>
                        
                        </tr>
                    </thead>
                    <tbody>
                    <g:each in="${cargaHoraIncurridaInstanceList}" status="i" var="cargaHoraIncurridaInstance">
                        <tr class="${(i % 2) == 0 ? 'odd' : 'even'}">
                        
                            <td><g:link action="show" id="${cargaHoraIncurridaInstance.id}">${fieldValue(bean: cargaHoraIncurridaInstance, field: "id")}</g:link></td>
                        
                            <td>${fieldValue(bean: cargaHoraIncurridaInstance, field: "cantidadHoras")}</td>
                        
                            <td>${fieldValue(bean: cargaHoraIncurridaInstance, field: "softwareFactory")}</td>
                        
                        </tr>
                    </g:each>
                    </tbody>
                </table>
            </div>
            <div class="paginateButtons">
                <g:paginate total="${cargaHoraIncurridaInstanceTotal}" />
            </div>
        </div>
    </body>
</html>
