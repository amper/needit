
<%@ page import="ar.com.telecom.pcs.entities.Sistema" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="layout" content="main" />
        <g:set var="entityName" value="${message(code: 'sistema.label', default: 'Sistema')}" />
        <title><g:message code="default.show.label" args="[entityName]" /></title>
    </head>
    <body>
        <div class="nav">
            <span class="menuButton"><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></span>
            <span class="menuButton"><g:link class="list" action="list"><g:message code="default.list.label" args="[entityName]" /></g:link></span>
            <span class="menuButton"><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></span>
        </div>
        <div class="body">
            <h1><g:message code="default.show.label" args="[entityName]" /></h1>
            <g:if test="${flash.message}">
            <div class="message">${flash.message}</div>
            </g:if>
            <div class="dialog">
                <table>
                    <tbody>
                    
                        <tr class="prop">
                            <td valign="top" class="name"><g:message code="sistema.id.label" default="Id" /></td>
                            
                            <td valign="top" class="value">${fieldValue(bean: sistemaInstance, field: "id")}</td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name"><g:message code="sistema.descripcion.label" default="Descripcion" /></td>
                            
                            <td valign="top" class="value">${fieldValue(bean: sistemaInstance, field: "descripcion")}</td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name"><g:message code="sistema.descripcionLarga.label" default="Descripcion Larga" /></td>
                            
                            <td valign="top" class="value">${fieldValue(bean: sistemaInstance, field: "descripcionLarga")}</td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name"><g:message code="sistema.grupoAdministrador.label" default="Grupo Administrador" /></td>
                            
                            <td valign="top" class="value">${fieldValue(bean: sistemaInstance, field: "grupoAdministrador")}</td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name"><g:message code="sistema.gerenciaSimplit.label" default="Gerencia Simplit" /></td>
                            
                            <td valign="top" class="value">${fieldValue(bean: sistemaInstance, field: "gerenciaSimplit")}</td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name"><g:message code="sistema.grupoReferenteSWF.label" default="Grupo Referente SWF" /></td>
                            
                            <td valign="top" class="value">${fieldValue(bean: sistemaInstance, field: "grupoReferenteSWF")}</td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name"><g:message code="sistema.responsableGestionarReleases.label" default="Responsable Gestionar Releases" /></td>
                            
                            <td valign="top" class="value">${fieldValue(bean: sistemaInstance, field: "responsableGestionarReleases")}</td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name"><g:message code="sistema.fechaBajaSimplit.label" default="Fecha Baja Simplit" /></td>
                            
                            <td valign="top" class="value"><g:formatDate date="${sistemaInstance?.fechaBajaSimplit}" /></td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name"><g:message code="sistema.gestionaRelease.label" default="Gestiona Release" /></td>
                            
                            <td valign="top" class="value"><g:formatBoolean boolean="${sistemaInstance?.gestionaRelease}" /></td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name"><g:message code="sistema.grupoLDAPGestionOperativa.label" default="Grupo LDAPG estion Operativa" /></td>
                            
                            <td valign="top" class="value">${fieldValue(bean: sistemaInstance, field: "grupoLDAPGestionOperativa")}</td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name"><g:message code="sistema.gestionaEvolutivos.label" default="Gestiona Evolutivos" /></td>
                            
                            <td valign="top" class="value"><g:formatBoolean boolean="${sistemaInstance?.gestionaEvolutivos}" /></td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name"><g:message code="sistema.impactoSOX.label" default="Impacto SOX" /></td>
                            
                            <td valign="top" class="value"><g:formatBoolean boolean="${sistemaInstance?.impactoSOX}" /></td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name"><g:message code="sistema.normaliza.label" default="Normaliza" /></td>
                            
                            <td valign="top" class="value"><g:formatBoolean boolean="${sistemaInstance?.normaliza}" /></td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name"><g:message code="sistema.softwareFactories.label" default="Software Factories" /></td>
                            
                            <td valign="top" style="text-align: left;" class="value">
                                <ul>
                                <g:each in="${sistemaInstance.softwareFactories}" var="s">
                                    <li><g:link controller="softwareFactory" action="show" id="${s.id}">${s?.encodeAsHTML()}</g:link></li>
                                </g:each>
                                </ul>
                            </td>
                            
                        </tr>
                    
                    </tbody>
                </table>
            </div>
            <div class="buttons">
                <g:form>
                    <g:hiddenField name="id" value="${sistemaInstance?.id}" />
                    <span class="button"><g:actionSubmit class="edit" action="edit" value="${message(code: 'default.button.edit.label', default: 'Edit')}" /></span>
                    <span class="button"><g:actionSubmit class="delete" action="delete" value="${message(code: 'default.button.delete.label', default: 'Delete')}" onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');" /></span>
                </g:form>
            </div>
        </div>
    </body>
</html>
