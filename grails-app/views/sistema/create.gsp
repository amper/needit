

<%@ page import="ar.com.telecom.pcs.entities.Sistema" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="layout" content="main" />
        <g:set var="entityName" value="${message(code: 'sistema.label', default: 'Sistema')}" />
        <title><g:message code="default.create.label" args="[entityName]" /></title>
    </head>
    <body>
        <div class="nav">
            <span class="menuButton"><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></span>
            <span class="menuButton"><g:link class="list" action="list"><g:message code="default.list.label" args="[entityName]" /></g:link></span>
        </div>
        <div class="body">
            <h1><g:message code="default.create.label" args="[entityName]" /></h1>
            <g:if test="${flash.message}">
            <div class="message">${flash.message}</div>
            </g:if>
            <g:hasErrors bean="${sistemaInstance}">
            <div class="errors">
                <g:renderErrors bean="${sistemaInstance}" as="list" />
            </div>
            </g:hasErrors>
            <g:form action="save" >
                <div class="dialog">
                    <table>
                        <tbody>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="descripcion"><g:message code="sistema.descripcion.label" default="Descripcion" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: sistemaInstance, field: 'descripcion', 'errors')}">
                                    <g:textField name="descripcion" maxlength="50" value="${sistemaInstance?.descripcion}" />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="descripcionLarga"><g:message code="sistema.descripcionLarga.label" default="Descripcion Larga" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: sistemaInstance, field: 'descripcionLarga', 'errors')}">
                                    <g:textField name="descripcionLarga" maxlength="250" value="${sistemaInstance?.descripcionLarga}" />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="grupoAdministrador"><g:message code="sistema.grupoAdministrador.label" default="Grupo Administrador" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: sistemaInstance, field: 'grupoAdministrador', 'errors')}">
                                    <g:textField name="grupoAdministrador" maxlength="100" value="${sistemaInstance?.grupoAdministrador}" />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="gerenciaSimplit"><g:message code="sistema.gerenciaSimplit.label" default="Gerencia Simplit" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: sistemaInstance, field: 'gerenciaSimplit', 'errors')}">
                                    <g:textField name="gerenciaSimplit" maxlength="100" value="${sistemaInstance?.gerenciaSimplit}" />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="grupoReferenteSWF"><g:message code="sistema.grupoReferenteSWF.label" default="Grupo Referente SWF" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: sistemaInstance, field: 'grupoReferenteSWF', 'errors')}">
                                    <g:textField name="grupoReferenteSWF" maxlength="100" value="${sistemaInstance?.grupoReferenteSWF}" />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="responsableGestionarReleases"><g:message code="sistema.responsableGestionarReleases.label" default="Responsable Gestionar Releases" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: sistemaInstance, field: 'responsableGestionarReleases', 'errors')}">
                                    <g:textField name="responsableGestionarReleases" maxlength="20" value="${sistemaInstance?.responsableGestionarReleases}" />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="fechaBajaSimplit"><g:message code="sistema.fechaBajaSimplit.label" default="Fecha Baja Simplit" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: sistemaInstance, field: 'fechaBajaSimplit', 'errors')}">
                                    <g:datePicker name="fechaBajaSimplit" precision="day" value="${sistemaInstance?.fechaBajaSimplit}" default="none" noSelection="['': '']" />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="gestionaRelease"><g:message code="sistema.gestionaRelease.label" default="Gestiona Release" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: sistemaInstance, field: 'gestionaRelease', 'errors')}">
                                    <g:checkBox name="gestionaRelease" value="${sistemaInstance?.gestionaRelease}" />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="grupoLDAPGestionOperativa"><g:message code="sistema.grupoLDAPGestionOperativa.label" default="Grupo LDAPG estion Operativa" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: sistemaInstance, field: 'grupoLDAPGestionOperativa', 'errors')}">
                                    <g:textField name="grupoLDAPGestionOperativa" maxlength="30" value="${sistemaInstance?.grupoLDAPGestionOperativa}" />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="gestionaEvolutivos"><g:message code="sistema.gestionaEvolutivos.label" default="Gestiona Evolutivos" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: sistemaInstance, field: 'gestionaEvolutivos', 'errors')}">
                                    <g:checkBox name="gestionaEvolutivos" value="${sistemaInstance?.gestionaEvolutivos}" />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="impactoSOX"><g:message code="sistema.impactoSOX.label" default="Impacto SOX" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: sistemaInstance, field: 'impactoSOX', 'errors')}">
                                    <g:checkBox name="impactoSOX" value="${sistemaInstance?.impactoSOX}" />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="normaliza"><g:message code="sistema.normaliza.label" default="Normaliza" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: sistemaInstance, field: 'normaliza', 'errors')}">
                                    <g:checkBox name="normaliza" value="${sistemaInstance?.normaliza}" />
                                </td>
                            </tr>
                        
                        </tbody>
                    </table>
                </div>
                <div class="buttons">
                    <span class="button"><g:submitButton name="create" class="save" value="${message(code: 'default.button.create.label', default: 'Create')}" /></span>
                </div>
            </g:form>
        </div>
    </body>
</html>
