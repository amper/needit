
<%@ page import="ar.com.telecom.pcs.entities.Sistema" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="layout" content="main" />
        <g:set var="entityName" value="${message(code: 'sistema.label', default: 'Sistema')}" />
        <title><g:message code="default.list.label" args="[entityName]" /></title>
    </head>
    <body>
        <div class="nav">
            <span class="menuButton"><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></span>
            <span class="menuButton"><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></span>
        </div>
        <div class="body">
            <h1><g:message code="default.list.label" args="[entityName]" /></h1>
            <g:if test="${flash.message}">
            <div class="message">${flash.message}</div>
            </g:if>
            <div class="list">
                <table>
                    <thead>
                        <tr>
                        
                            <g:sortableColumn property="id" title="${message(code: 'sistema.id.label', default: 'Id')}" />
                        
                            <g:sortableColumn property="descripcion" title="${message(code: 'sistema.descripcion.label', default: 'Descripcion')}" />
                        
                            <g:sortableColumn property="descripcionLarga" title="${message(code: 'sistema.descripcionLarga.label', default: 'Descripcion Larga')}" />
                        
                            <g:sortableColumn property="grupoAdministrador" title="${message(code: 'sistema.grupoAdministrador.label', default: 'Grupo Administrador')}" />
                        
                            <g:sortableColumn property="gerenciaSimplit" title="${message(code: 'sistema.gerenciaSimplit.label', default: 'Gerencia Simplit')}" />
                        
                            <g:sortableColumn property="grupoReferenteSWF" title="${message(code: 'sistema.grupoReferenteSWF.label', default: 'Grupo Referente SWF')}" />
                        
                        </tr>
                    </thead>
                    <tbody>
                    <g:each in="${sistemaInstanceList}" status="i" var="sistemaInstance">
                        <tr class="${(i % 2) == 0 ? 'odd' : 'even'}">
                        
                            <td><g:link action="show" id="${sistemaInstance.id}">${fieldValue(bean: sistemaInstance, field: "id")}</g:link></td>
                        
                            <td>${fieldValue(bean: sistemaInstance, field: "descripcion")}</td>
                        
                            <td>${fieldValue(bean: sistemaInstance, field: "descripcionLarga")}</td>
                        
                            <td>${fieldValue(bean: sistemaInstance, field: "grupoAdministrador")}</td>
                        
                            <td>${fieldValue(bean: sistemaInstance, field: "gerenciaSimplit")}</td>
                        
                            <td>${fieldValue(bean: sistemaInstance, field: "grupoReferenteSWF")}</td>
                        
                        </tr>
                    </g:each>
                    </tbody>
                </table>
            </div>
            <div class="paginateButtons">
                <g:paginate total="${sistemaInstanceTotal}" />
            </div>
        </div>
    </body>
</html>
