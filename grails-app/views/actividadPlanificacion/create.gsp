

<%@ page import="ar.com.telecom.pcs.entities.ActividadPlanificacion" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="layout" content="main" />
        <g:set var="entityName" value="${message(code: 'actividadPlanificacion.label', default: 'ActividadPlanificacion')}" />
        <title><g:message code="default.create.label" args="[entityName]" /></title>
    </head>
    <body>
        <div class="nav">
            <span class="menuButton"><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></span>
            <span class="menuButton"><g:link class="list" action="list"><g:message code="default.list.label" args="[entityName]" /></g:link></span>
        </div>
        <div class="body">
            <h1><g:message code="default.create.label" args="[entityName]" /></h1>
            <g:if test="${flash.message}">
            <div class="message">${flash.message}</div>
            </g:if>
            <g:hasErrors bean="${actividadPlanificacionInstance}">
            <div class="errors">
                <g:renderErrors bean="${actividadPlanificacionInstance}" as="list" />
            </div>
            </g:hasErrors>
            <g:form action="save" >
                <div class="dialog">
                    <table>
                        <tbody>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="descripcion"><g:message code="actividadPlanificacion.descripcion.label" default="Descripcion" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: actividadPlanificacionInstance, field: 'descripcion', 'errors')}">
                                    <g:textField name="descripcion" maxlength="70" value="${actividadPlanificacionInstance?.descripcion}" />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="fase"><g:message code="actividadPlanificacion.fase.label" default="Fase" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: actividadPlanificacionInstance, field: 'fase', 'errors')}">
                                    <g:select name="fase.id" from="${ar.com.telecom.pcs.entities.Fase.list()}" optionKey="id" value="${actividadPlanificacionInstance?.fase?.id}" noSelection="['null': '']" />
                                </td>
                            </tr>
                        
                        </tbody>
                    </table>
                </div>
                <div class="buttons">
                    <span class="button"><g:submitButton name="create" class="save" value="${message(code: 'default.button.create.label', default: 'Create')}" /></span>
                </div>
            </g:form>
        </div>
    </body>
</html>
