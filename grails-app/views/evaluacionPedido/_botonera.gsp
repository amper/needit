<g:updateToken />
<div id="botonera">
	<g:if test="${pedidoInstance.puedeModificarEvaluacionPedido(usuarioLogueado)}">
		<g:actionSubmit class="formSeccionSubmitButton" id="Simular"
			action="simularValidacion" value="Simular" />
	</g:if>
	<g:if test="${pedidoInstance.puedeReasignarEvaluacionPedido(usuarioLogueado)}">
		<g:actionSubmit class="formSeccionSubmitButton" id="Guardar" action="grabarFormulario" value="Guardar" />
	</g:if>
	<g:if test="${pedidoInstance.puedeModificarEvaluacionPedido(usuarioLogueado)}">
		<g:actionSubmit class="formSeccionSubmitButton" id="Denegar"
			action="denegarPedido" value="Denegar" />
		<g:actionSubmit class="formSeccionSubmitButton" id="Aprobar"
			action="aprobarPedido" value="Aprobar" />
	</g:if>
</div>