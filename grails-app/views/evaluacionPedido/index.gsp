<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta name="layout" content="mainApp" />
<g:javascript library="evaluacionPedido" />
<g:javascript library="boxUser" />

</head>
<body>
 
 
	<div class="contenedorTitulo">
		<g:titulo pedido="${pedidoInstance}" />
		<g:workflow pedidoId="${pedidoInstance?.id}" />
	</div>

	<div id="contenedorIzq">
		<g:if test="${flash.message}">
			<div class="message">
				${flash.message}
			</div>
		</g:if>
		<g:hasErrors bean="${pedidoInstance}">
			<div class="errors">
				<g:renderErrors bean="${pedidoInstance}" as="list" />
			</div>
		</g:hasErrors>
		<g:form class="formGeneral" useToken="true">
			<g:hiddenField name="pedidoId" id="pedidoId" value="${pedidoInstance?.id}" />
			
			<g:render template="/templates/workflowHidden" />
			<div id="formularioEvaluacion" style="margin-top: 0px;"></div>
			<script type="text/javascript">
				completarAsignatarios('${pedidoInstance.grupoGestionDemanda}', 'false', '${pedidoInstance?.legajoCoordinadorCambio }');
			</script>
		</g:form>
	</div>
	<!-- Lateral Derecho -->
	<g:render template="/templates/menuLateral" model="['pedidoInstance':pedidoInstance]"/>
	<!-- Fin Lateral Derecho -->


	<!-- fin contenedor izq -->

	<!-- 
	************
	************
	MODALS POPUP
	************
	************
	************
	-->

	<!-- popup ayuda seccion -->
	<div id="dialog-help" title="Ayuda Aprobar Pedido"
		style="display: none;">
		<div style="height: 350px; width: 100%; overflow: auto;">
			<p class="helpTitle">Seccion ayuda</p>
			<p>Hola esto es una seccion de ayuda Hola esto es una seccion de</p>
		</div>
	</div>
	<!-- fin popup ayuda seccion -->
</body>
</html>
