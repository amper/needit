<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta name="layout" content="mainApp" />
<g:javascript library="boxUser" />
</head>
<body>

	<div class="contenedorTitulo">
		<g:titulo pedido="${pedidoInstance}" />
		<g:workflow pedidoId="${pedidoInstance?.id}" />
	</div>
 
 
	<div id="contenedorIzq">
		<g:if test="${flash.message}">
			<div class="message">
				${flash.message}
			</div>
		</g:if>
		<g:hasErrors bean="${pedidoInstance}">
			<div class="errors">
				<g:renderErrors bean="${pedidoInstance}" as="list" />
			</div>
		</g:hasErrors>
		<g:form class="formGeneral" useToken="true">
			<g:hiddenField name="id" value="${pedidoInstance?.id}" />
			<g:render template="/templates/workflowHidden" />

			<div class="formGeneral">

				<div class="seccionHide">
					<g:cabeceraPedido pedido="${pedidoInstance}" />
					
					<p class='tituloSeccion'>Evaluaci&oacute;n del pedido</p>
					<div class='formSeccion'>
						<div class="pDos">
							<label class='formLabel'>Grupo gesti&oacute;n demanda
								asignado</label>
							<p class="info">
								${pedidoInstance?.grupoGestionDemanda}
							</p>
						</div>
						<g:if test="${pedidoInstance?.legajoUsuarioGestionDemanda}">
							<div class="pDos">
								<label class="formLabel" for='inputUser'>Asignatario</label>
								<div id="legajoUsuarioGestionDemanda" class="boxUser">
									<g:render template="/templates/userBoxHabilitador"
										model="['person':pedidoInstance?.legajoUsuarioGestionDemanda, 'box':'legajoUsuarioGestionDemanda', 'personsAutocomplete': personsAutocompleteAsign, 'habilitaAvanzar': habilitaAvanzar]"></g:render>
								</div>
							</div>
						</g:if>
						<div class="pDos" id="divPrioridad">
							<label class='formLabel'>Prioridad</label>
							<p class="info">
								${pedidoInstance?.prioridad}
							</p>
						</div>

						<g:if test="${pedidoInstance?.legajoCoordinadorCambio}">
							<div class="pDos" id="divCoordinador">
								<label class="formLabel" for='inputUser'>Coordinador
									cambio</label>
								<div id="legajoCoordinadorCambio" class="boxUser">
									<g:render template="/templates/userBox"
										model="['person':pedidoInstance?.legajoCoordinadorCambio, 'box':'legajoCoordinadorCambio', 'personsAutocomplete': personsAutocompleteCoord]"></g:render>
								</div>
							</div>
						</g:if>
						<div id="message"></div>
					</div>
				</div>


				<g:if test="${pedidoInstance?.getJustificacionFGD()}">
					<div id="divBotonera">
						<div class="seccionHide2">

							<p class='tituloSeccion'>Justificaci&oacute;n</p>
							<div class='formSeccion'>
								<p class="info">
									&nbsp;${pedidoInstance?.getJustificacionFGD()}
								</p>
							</div>

						</div>
					</div>
				</g:if>
				
				<g:render template="/templates/ultimaJustificacionDenegacion"
						     			model="['pedidoInstance': pedidoInstance, 'fase': pedidoInstance.faseEvaluacionCambio()]"></g:render>
			</div>

		</g:form>

	</div>

	<!-- fin contenedor izq -->

	<!-- Lateral Derecho -->
	<g:render template="/templates/menuLateral" model="['pedidoInstance':pedidoInstance]"/>
	<!-- Fin Lateral Derecho -->

	<!-- 
	************
	************
	MODALS POPUP
	************
	************
	************
	-->

	<!-- popup ayuda seccion -->
	<div id="dialog-help" title="Ayuda Aprobar Pedido"
		style="display: none;">
		<div style="height: 350px; width: 100%; overflow: auto;">
			<p class="helpTitle">Seccion ayuda</p>
			<p>Hola esto es una seccion de ayuda Hola esto es una seccion de</p>
		</div>
	</div>
	<!-- fin popup ayuda seccion -->
</body>
</html>
