<%@page import="ar.com.telecom.pcs.entities.RolAplicacion"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta name="layout" content="mainApp" />
<g:javascript library="evaluacionPedido" />
<g:javascript library="boxUser" />

</head>
<body>

	<div class="contenedorTitulo">
		<g:titulo pedido="${pedidoInstance}" />
		<g:workflow pedidoId="${pedidoInstance?.id}" />
	</div>
 
	<div id="contenedorIzq">
		<g:if test="${flash.message}">
			<div class="message">
				${flash.message}
			</div>
		</g:if>
		<g:hasErrors bean="${pedidoInstance}">
			<div class="errors">
				<g:renderErrors bean="${pedidoInstance}" as="list" />
			</div>
		</g:hasErrors>
		<g:form class="formGeneral" useToken="true">
			<g:hiddenField name="id" value="${pedidoInstance?.id}" />
			<g:render template="/templates/workflowHidden" />
			<div class="seccionHide">
				<g:cabeceraPedido pedido="${pedidoInstance}" />

				<p class='tituloSeccion'>Evaluaci&oacute;n del pedido</p>
				<div class='formSeccion'>
					<div class="pDos">
						<label class='formLabel validate'>Grupo gesti&oacute;n
							demanda asignado</label>
						<g:select id="grupoGestionDemanda" name="grupoGestionDemanda"
							from="${ar.com.telecom.pcs.entities.GrupoGestionDemanda.list()}"
							optionKey="grupoLDAP"
							value="${pedidoInstance?.grupoGestionDemanda}"
							noSelection="['null': '']" />
					</div>
<%--					<div class="pDos">--%>
<%--						<label class="formLabel validate" for='inputUser'>Asignatario</label>--%>
<%--											${pedidoInstance?.legajoUsuarioGestionDemanda}--%>
<%--						<div id="legajoUsuarioGestionDemanda" class="boxUser">--%>
<%--							<g:render template="/templates/userBoxHabilitador"--%>
<%--								model="['person':pedidoInstance?.legajoUsuarioGestionDemanda, 'box':'legajoUsuarioGestionDemanda', 'personsAutocomplete': personsAutocompleteAsign, 'usuarioLogueado': usuarioLogueado, 'habilitaAvanzar': habilitaAvanzar, pedidoId: pedidoInstance?.id]"></g:render>--%>
<%--						</div>--%>
<%--						<g:remoteLink title="Eliminar" class="limpiarBox" id="asig"--%>
<%--							update="legajoUsuarioGestionDemanda"--%>
<%--							params="['username': '', 'box':'legajoUsuarioGestionDemanda', 'usuarioLogueado': usuarioLogueado, pedidoId: pedidoInstance?.id]"--%>
<%--							action="completarUsuario"></g:remoteLink>--%>
<%--					</div>--%>
					
					<div class="pDos">
						<label class="formLabel validate" for='inputUser'>Asignatario</label>
						<%--					${pedidoInstance?.legajoUsuarioGestionDemanda}--%>
						<div id="legajoUsuarioGestionDemanda" class="boxUser">
							<g:render template="/templates/userBox"
								model="['person':pedidoInstance?.legajoUsuarioGestionDemanda, 'box':'legajoUsuarioGestionDemanda', 'personsAutocomplete': personsAutocompleteAsign, pedidoId: pedidoInstance?.id, 'pedidoInstance': pedidoInstance]"></g:render>
						</div>
						<div id="eliminarAsignatario" style="display: ${pedidoInstance?.puedeReasignarEvaluacionPedido(usuarioLogueado) ? 'block' : 'none' };">
							<g:secureRemoteLink title="Eliminar" class="limpiarBox" id="${RolAplicacion.GESTION_DEMANDA}"
								update="legajoUsuarioGestionDemanda"
								params="['username': '', 'box':'legajoUsuarioGestionDemanda', 'usuarioLogueado': usuarioLogueado, pedidoId: pedidoInstance?.id]"
								action="completarUsuario"></g:secureRemoteLink>
						</div>
					</div>

					<%--				<g:if test="${habilitaAvanzar}">--%>
					<div class="pDos" id="divPrioridad">
						<label class='formLabel'>Prioridad</label><br>
						<g:select id="comboPrioridad" name="prioridad.id"
							class="${hasErrors(bean: pedidoInstance, field: 'prioridad?.id', 'errors')}"
							from="${ar.com.telecom.pcs.entities.Prioridad.list()}"
							optionKey="id" value="${pedidoInstance?.prioridad?.id}"
							noSelection="['null': '']" />
					</div>

					<div class="pDos" id="divCoordinador">
						<label class="formLabel validate" for='inputUser'>Coordinador
							cambio</label>
						<div id="legajoCoordinadorCambio" class="boxUser">
							<g:render template="/templates/userBox"
								model="['person':pedidoInstance?.legajoCoordinadorCambio, 'box':'legajoCoordinadorCambio', 'personsAutocomplete': personsAutocompleteCoord, pedidoId: pedidoInstance?.id]"></g:render>
						</div>
						<g:secureRemoteLink title="Eliminar" class="limpiarBox" id="${RolAplicacion.COORDINADOR_CAMBIO}"
							update="legajoCoordinadorCambio"
							params="['username': '', 'box':'legajoCoordinadorCambio']"
							action="completarUsuario"></g:secureRemoteLink>
					</div>
					<%--				</g:if>--%>
					<div id="message"></div>
				</div>
			</div>
			<div id="divJustificacion">
				<div class="seccionHide2">

					<%--			<g:if test="${habilitaAvanzar}">--%>
					<p class='tituloSeccion'>Justificaci&oacute;n</p>
					<div class='formSeccion'>
						<div class="pCompleto">
						<g:textArea name="justificacionAprobacion"
							value="${pedidoInstance?.getJustificacionFGD()}"></g:textArea>
						</div>
					</div>

					<g:render template="/templates/ultimaJustificacionDenegacion"
						model="['pedidoInstance': pedidoInstance, 'fase': pedidoInstance.faseEvaluacionCambio()]"></g:render>

				</div>
			</div>
			
			<div id="botoneraAll" class="formSeccionButton"
				style="margin-top: 20px;">
				<g:render template="botonera"
					model="['pedidoInstance':pedidoInstance, 'controller':params.controller, 'usuarioLogueado':usuarioLogueado, 'box':'legajoUsuarioGestionDemanda']"></g:render>
			</div>

		</g:form>
	</div>
	<!-- Lateral Derecho -->
	<g:render template="/templates/menuLateral" model="['pedidoInstance':pedidoInstance]"/>
	<!-- Fin Lateral Derecho -->


	<!-- fin contenedor izq -->

	<!-- 
	************
	************
	MODALS POPUP
	************
	************
	************
	-->

	<!-- popup ayuda seccion -->
	<div id="dialog-help" title="Ayuda Aprobar Pedido"
		style="display: none;">
		<div style="height: 350px; width: 100%; overflow: auto;">
			<p class="helpTitle">Seccion ayuda</p>
			<p>Hola esto es una seccion de ayuda Hola esto es una seccion de</p>
		</div>
	</div>
	<!-- fin popup ayuda seccion -->
</body>
</html>
