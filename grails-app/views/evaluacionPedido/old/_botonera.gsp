<g:updateToken />
pedidoInstance: ${pedidoInstance }<br>
1: ${pedidoInstance?.aprobacionPendiente(pedidoInstance?.faseActual, usuarioLogueado)}

<div id="botonera" style="display: none;">

	<g:if test="${pedidoInstance?.aprobacionPendiente(pedidoInstance?.faseActual, usuarioLogueado)}">
	
		<g:actionSubmit class="formSeccionSubmitButton" id="Simular"
			action="simularValidacion" value="Simular" />
		<g:actionSubmit class="formSeccionSubmitButton" id="Guardar" action="grabarFormulario" value="Guardar" />						

		<g:if test="${!pedidoInstance?.estaSuspendido()}">
			<g:actionSubmit class="formSeccionSubmitButton" id="Denegar"
				action="denegarPedido" value="Denegar" />
			<g:actionSubmit class="formSeccionSubmitButton" id="Aprobar"
				action="aprobarPedido" value="Aprobar" />
		</g:if>			
	</g:if>
</div>
 
<div id="botoneraReducida" style="display: none;">

	<g:if test="${pedidoInstance?.aprobacionPendiente(pedidoInstance?.faseActual, usuarioLogueado)}">
	
		<g:actionSubmit class="formSeccionSubmitButton" id="GuardarReducido" action="grabarFormulario" value="Guardar" />
		
	</g:if>
		
</div>						


<input type="hidden" id="usuarioLogueado" name="usuarioLogueado" value="${usuarioLogueado}" />
<script>
var asignatario = "";
if ($('inputlegajoUsuarioGestionDemanda')) {
	asignatario = $('inputlegajoUsuarioGestionDemanda').value;
}
if (asignatario == document.getElementById("usuarioLogueado").value){
	initializeForm(null, null, ['divPrioridad', 'divCoordinador', 'divJustificacion', 'botoneraAll', 'botonera'], ['botoneraReducida']);
} else{
	initializeForm(null, null, ['botoneraAll', 'botoneraReducida'], ['divPrioridad', 'divCoordinador', 'divJustificacion', 'botonera']);
}
</script>