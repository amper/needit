<g:updateToken />
<%@page import="ar.com.telecom.pcs.entities.RolAplicacion"%>
<script type="text/javascript">
	jQuery(document).ready(
		function($) {
			jQuery("#grupoGestionDemanda").combobox();
			jQuery("#comboPrioridad").combobox();
			jQuery("#grupoGestionDemanda").change(function() {
				completarAsignatarios(this.value, true, '');
			});
		});
</script>
 
 
	
<div class="seccionHide">
	<g:cabeceraPedido pedido="${pedidoInstance}" generaHiddenPedidoId="false" />

	<p class='tituloSeccion'>Evaluaci&oacute;n del pedido</p>
	<div class='formSeccion'>
		<div class="pDos">
			<label class='formLabel validate'>Grupo gesti&oacute;n demanda asignado</label>
			<g:if test="${pedidoInstance.puedeModificarEvaluacionPedido(usuarioLogueado)}">
				<g:select id="grupoGestionDemanda" name="grupoGestionDemanda"
					from="${ar.com.telecom.pcs.entities.GrupoGestionDemanda.list()}"
					optionKey="grupoLDAP"
					value="${pedidoInstance?.grupoGestionDemanda}"
					noSelection="['null': '']" />
			</g:if>
			<g:else>
				<p class="info">
					${pedidoInstance?.grupoGestionDemanda}
				</p>
			</g:else>
		</div>
		
		<div class="pDos">
			<label class="formLabel validate" for='inputUser'>Asignatario</label>
			<div id="legajoUsuarioGestionDemanda" class="boxUser">
				<g:render template="/templates/userBox"
					model="['person':pedidoInstance?.legajoUsuarioGestionDemanda, 'box':'legajoUsuarioGestionDemanda', 'personsAutocomplete': personsAutocompleteAsign, pedidoId: pedidoInstance?.id]"></g:render>
			</div>
			<g:if test="${pedidoInstance.puedeReasignarEvaluacionPedido(usuarioLogueado)}">
				<g:secureRemoteLink title="Eliminar" class="limpiarBox" id="${RolAplicacion.GESTION_DEMANDA}"
					update="legajoUsuarioGestionDemanda"
					params="['username': '', 'box':'legajoUsuarioGestionDemanda', 'usuarioLogueado': usuarioLogueado, pedidoId: pedidoInstance?.id]"
					action="completarUsuario"></g:secureRemoteLink>
			</g:if>
		</div>

		<g:if test="${pedidoInstance.puedeModificarEvaluacionPedido(usuarioLogueado)}">

			<div class="pDos" id="divPrioridad">
				<label class='formLabel'>Prioridad</label><br>
				<g:select id="comboPrioridad" name="prioridad.id"
					class="${hasErrors(bean: pedidoInstance, field: 'prioridad?.id', 'errors')}"
					from="${ar.com.telecom.pcs.entities.Prioridad.list()}"
					optionKey="id" value="${pedidoInstance?.prioridad?.id}"
					noSelection="['null': '']" />
			</div>
			
			<div class="pDos" id="divCoordinador">
				<label class="formLabel validate" for='inputUser'>Coordinador cambio</label>
				<div id="legajoCoordinadorCambio" class="boxUser">
					<g:render template="/templates/userBox"
						model="['person':pedidoInstance?.legajoCoordinadorCambio, 'box':'legajoCoordinadorCambio', 'personsAutocomplete': personsAutocompleteCoord, pedidoId: pedidoInstance?.id]"></g:render>
				</div>
				<g:secureRemoteLink title="Eliminar" class="limpiarBox" id="${RolAplicacion.COORDINADOR_CAMBIO}"
					update="legajoCoordinadorCambio"
					params="['username': '', 'box':'legajoCoordinadorCambio']"
					action="completarUsuario"></g:secureRemoteLink>
			</div>
			<div id="message"></div>
			
		</g:if>
		<g:else>
		
			<div class="pDos" id="divPrioridad">
				<label class='formLabel'>Prioridad</label>
				<p class="info">
					${pedidoInstance?.prioridad}
				</p>
			</div>

			<g:if test="${pedidoInstance?.legajoCoordinadorCambio}">
				<div class="pDos" id="divCoordinador">
					<label class="formLabel" for='inputUser'>Coordinador
						cambio</label>
					<div id="legajoCoordinadorCambio" class="boxUser">
						<g:render template="/templates/userBox"
							model="['person':pedidoInstance?.legajoCoordinadorCambio, 'box':'legajoCoordinadorCambio', 'personsAutocomplete': personsAutocompleteCoord]"></g:render>
					</div>
				</div>
			</g:if>

		
		</g:else>
		
		
	</div>
</div>
<div id="divJustificacion">

	<g:if test="${pedidoInstance.puedeModificarEvaluacionPedido(usuarioLogueado)}">

		<div class="seccionHide2">

			<p class='tituloSeccion'>Justificaci&oacute;n</p>
			<div class='formSeccion'>
				<div class="pCompleto">
				<g:textArea name="justificacionAprobacion"
					value="${pedidoInstance?.getJustificacionFGD()}"></g:textArea>
				</div>
			</div>

			<g:render template="/templates/ultimaJustificacionDenegacion"
				model="['pedidoInstance': pedidoInstance, 'fase': pedidoInstance.faseEvaluacionCambio()]"></g:render>

		</div>
		
	</g:if>
	
</div>

<div id="botoneraAll" class="formSeccionButton" style="margin-top: 20px;">
	<g:render template="botonera"
		model="['pedidoInstance':pedidoInstance, 'controller':params.controller, 'usuarioLogueado':usuarioLogueado, 'box':'legajoUsuarioGestionDemanda']"></g:render>
</div>

