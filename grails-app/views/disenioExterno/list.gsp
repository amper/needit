
<%@ page import="ar.com.telecom.pcs.entities.DisenioExterno" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="layout" content="main" />
        <g:set var="entityName" value="${message(code: 'disenioExterno.label', default: 'DisenioExterno')}" />
        <title><g:message code="default.list.label" args="[entityName]" /></title>
    </head>
    <body>
        <div class="nav">
            <span class="menuButton"><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></span>
            <span class="menuButton"><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></span>
        </div>
        <div class="body">
            <h1><g:message code="default.list.label" args="[entityName]" /></h1>
            <g:if test="${flash.message}">
            <div class="message">${flash.message}</div>
            </g:if>
            <div class="list">
                <table>
                    <thead>
                        <tr>
                        
                            <g:sortableColumn property="id" title="${message(code: 'disenioExterno.id.label', default: 'Id')}" />
                        
                            <g:sortableColumn property="fechaRelease" title="${message(code: 'disenioExterno.fechaRelease.label', default: 'Fecha Release')}" />
                        
                            <g:sortableColumn property="numeroRelease" title="${message(code: 'disenioExterno.numeroRelease.label', default: 'Numero Release')}" />
                        
                            <g:sortableColumn property="comentarioAlcance" title="${message(code: 'disenioExterno.comentarioAlcance.label', default: 'Comentario Alcance')}" />
                        
                            <g:sortableColumn property="premisasSupuestosRestricciones" title="${message(code: 'disenioExterno.premisasSupuestosRestricciones.label', default: 'Premisas Supuestos Restricciones')}" />
                        
                            <g:sortableColumn property="descripcionFuncionalSolucion" title="${message(code: 'disenioExterno.descripcionFuncionalSolucion.label', default: 'Descripcion Funcional Solucion')}" />
                        
                        </tr>
                    </thead>
                    <tbody>
                    <g:each in="${disenioExternoInstanceList}" status="i" var="disenioExternoInstance">
                        <tr class="${(i % 2) == 0 ? 'odd' : 'even'}">
                        
                            <td><g:link action="show" id="${disenioExternoInstance.id}">${fieldValue(bean: disenioExternoInstance, field: "id")}</g:link></td>
                        
                            <td><g:formatDate date="${disenioExternoInstance.fechaRelease}" /></td>
                        
                            <td>${fieldValue(bean: disenioExternoInstance, field: "numeroRelease")}</td>
                        
                            <td>${fieldValue(bean: disenioExternoInstance, field: "comentarioAlcance")}</td>
                        
                            <td>${fieldValue(bean: disenioExternoInstance, field: "premisasSupuestosRestricciones")}</td>
                        
                            <td>${fieldValue(bean: disenioExternoInstance, field: "descripcionFuncionalSolucion")}</td>
                        
                        </tr>
                    </g:each>
                    </tbody>
                </table>
            </div>
            <div class="paginateButtons">
                <g:paginate total="${disenioExternoInstanceTotal}" />
            </div>
        </div>
    </body>
</html>
