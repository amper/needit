

<%@ page import="ar.com.telecom.pcs.entities.DisenioExterno" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="layout" content="main" />
        <g:set var="entityName" value="${message(code: 'disenioExterno.label', default: 'DisenioExterno')}" />
        <title><g:message code="default.create.label" args="[entityName]" /></title>
    </head>
    <body>
        <div class="nav">
            <span class="menuButton"><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></span>
            <span class="menuButton"><g:link class="list" action="list"><g:message code="default.list.label" args="[entityName]" /></g:link></span>
        </div>
        <div class="body">
            <h1><g:message code="default.create.label" args="[entityName]" /></h1>
            <g:if test="${flash.message}">
            <div class="message">${flash.message}</div>
            </g:if>
            <g:hasErrors bean="${disenioExternoInstance}">
            <div class="errors">
                <g:renderErrors bean="${disenioExternoInstance}" as="list" />
            </div>
            </g:hasErrors>
            <g:form action="save" >
                <div class="dialog">
                    <table>
                        <tbody>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="fechaRelease"><g:message code="disenioExterno.fechaRelease.label" default="Fecha Release" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: disenioExternoInstance, field: 'fechaRelease', 'errors')}">
                                    <g:datePicker name="fechaRelease" precision="day" value="${disenioExternoInstance?.fechaRelease}" default="none" noSelection="['': '']" />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="numeroRelease"><g:message code="disenioExterno.numeroRelease.label" default="Numero Release" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: disenioExternoInstance, field: 'numeroRelease', 'errors')}">
                                    <g:textField name="numeroRelease" maxlength="20" value="${disenioExternoInstance?.numeroRelease}" />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="comentarioAlcance"><g:message code="disenioExterno.comentarioAlcance.label" default="Comentario Alcance" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: disenioExternoInstance, field: 'comentarioAlcance', 'errors')}">
                                    <g:textField name="comentarioAlcance" value="${disenioExternoInstance?.comentarioAlcance}" />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="premisasSupuestosRestricciones"><g:message code="disenioExterno.premisasSupuestosRestricciones.label" default="Premisas Supuestos Restricciones" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: disenioExternoInstance, field: 'premisasSupuestosRestricciones', 'errors')}">
                                    <g:textField name="premisasSupuestosRestricciones" value="${disenioExternoInstance?.premisasSupuestosRestricciones}" />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="descripcionFuncionalSolucion"><g:message code="disenioExterno.descripcionFuncionalSolucion.label" default="Descripcion Funcional Solucion" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: disenioExternoInstance, field: 'descripcionFuncionalSolucion', 'errors')}">
                                    <g:textField name="descripcionFuncionalSolucion" value="${disenioExternoInstance?.descripcionFuncionalSolucion}" />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="interfacesInvolucradasSolucion"><g:message code="disenioExterno.interfacesInvolucradasSolucion.label" default="Interfaces Involucradas Solucion" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: disenioExternoInstance, field: 'interfacesInvolucradasSolucion', 'errors')}">
                                    <g:textField name="interfacesInvolucradasSolucion" value="${disenioExternoInstance?.interfacesInvolucradasSolucion}" />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="descripcionVolumetriaPerformance"><g:message code="disenioExterno.descripcionVolumetriaPerformance.label" default="Descripcion Volumetria Performance" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: disenioExternoInstance, field: 'descripcionVolumetriaPerformance', 'errors')}">
                                    <g:textField name="descripcionVolumetriaPerformance" value="${disenioExternoInstance?.descripcionVolumetriaPerformance}" />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="principalesCambiosProcesos"><g:message code="disenioExterno.principalesCambiosProcesos.label" default="Principales Cambios Procesos" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: disenioExternoInstance, field: 'principalesCambiosProcesos', 'errors')}">
                                    <g:textField name="principalesCambiosProcesos" value="${disenioExternoInstance?.principalesCambiosProcesos}" />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="definicionesPendientesWarnings"><g:message code="disenioExterno.definicionesPendientesWarnings.label" default="Definiciones Pendientes Warnings" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: disenioExternoInstance, field: 'definicionesPendientesWarnings', 'errors')}">
                                    <g:textField name="definicionesPendientesWarnings" value="${disenioExternoInstance?.definicionesPendientesWarnings}" />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="justificacionDisenioExterno"><g:message code="disenioExterno.justificacionDisenioExterno.label" default="Justificacion Disenio Externo" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: disenioExternoInstance, field: 'justificacionDisenioExterno', 'errors')}">
                                    <g:select name="justificacionDisenioExterno.id" from="${ar.com.telecom.pcs.entities.JustificacionDisenioExterno.list()}" optionKey="id" value="${disenioExternoInstance?.justificacionDisenioExterno?.id}" noSelection="['null': '']" />
                                </td>
                            </tr>
                        
                        </tbody>
                    </table>
                </div>
                <div class="buttons">
                    <span class="button"><g:submitButton name="create" class="save" value="${message(code: 'default.button.create.label', default: 'Create')}" /></span>
                </div>
            </g:form>
        </div>
    </body>
</html>
