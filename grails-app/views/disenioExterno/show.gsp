
<%@ page import="ar.com.telecom.pcs.entities.DisenioExterno" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="layout" content="main" />
        <g:set var="entityName" value="${message(code: 'disenioExterno.label', default: 'DisenioExterno')}" />
        <title><g:message code="default.show.label" args="[entityName]" /></title>
    </head>
    <body>
        <div class="nav">
            <span class="menuButton"><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></span>
            <span class="menuButton"><g:link class="list" action="list"><g:message code="default.list.label" args="[entityName]" /></g:link></span>
            <span class="menuButton"><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></span>
        </div>
        <div class="body">
            <h1><g:message code="default.show.label" args="[entityName]" /></h1>
            <g:if test="${flash.message}">
            <div class="message">${flash.message}</div>
            </g:if>
            <div class="dialog">
                <table>
                    <tbody>
                    
                        <tr class="prop">
                            <td valign="top" class="name"><g:message code="disenioExterno.id.label" default="Id" /></td>
                            
                            <td valign="top" class="value">${fieldValue(bean: disenioExternoInstance, field: "id")}</td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name"><g:message code="disenioExterno.fechaRelease.label" default="Fecha Release" /></td>
                            
                            <td valign="top" class="value"><g:formatDate date="${disenioExternoInstance?.fechaRelease}" /></td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name"><g:message code="disenioExterno.numeroRelease.label" default="Numero Release" /></td>
                            
                            <td valign="top" class="value">${fieldValue(bean: disenioExternoInstance, field: "numeroRelease")}</td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name"><g:message code="disenioExterno.comentarioAlcance.label" default="Comentario Alcance" /></td>
                            
                            <td valign="top" class="value">${fieldValue(bean: disenioExternoInstance, field: "comentarioAlcance")}</td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name"><g:message code="disenioExterno.premisasSupuestosRestricciones.label" default="Premisas Supuestos Restricciones" /></td>
                            
                            <td valign="top" class="value">${fieldValue(bean: disenioExternoInstance, field: "premisasSupuestosRestricciones")}</td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name"><g:message code="disenioExterno.descripcionFuncionalSolucion.label" default="Descripcion Funcional Solucion" /></td>
                            
                            <td valign="top" class="value">${fieldValue(bean: disenioExternoInstance, field: "descripcionFuncionalSolucion")}</td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name"><g:message code="disenioExterno.interfacesInvolucradasSolucion.label" default="Interfaces Involucradas Solucion" /></td>
                            
                            <td valign="top" class="value">${fieldValue(bean: disenioExternoInstance, field: "interfacesInvolucradasSolucion")}</td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name"><g:message code="disenioExterno.descripcionVolumetriaPerformance.label" default="Descripcion Volumetria Performance" /></td>
                            
                            <td valign="top" class="value">${fieldValue(bean: disenioExternoInstance, field: "descripcionVolumetriaPerformance")}</td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name"><g:message code="disenioExterno.principalesCambiosProcesos.label" default="Principales Cambios Procesos" /></td>
                            
                            <td valign="top" class="value">${fieldValue(bean: disenioExternoInstance, field: "principalesCambiosProcesos")}</td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name"><g:message code="disenioExterno.definicionesPendientesWarnings.label" default="Definiciones Pendientes Warnings" /></td>
                            
                            <td valign="top" class="value">${fieldValue(bean: disenioExternoInstance, field: "definicionesPendientesWarnings")}</td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name"><g:message code="disenioExterno.justificacionDisenioExterno.label" default="Justificacion Disenio Externo" /></td>
                            
                            <td valign="top" class="value"><g:link controller="justificacionDisenioExterno" action="show" id="${disenioExternoInstance?.justificacionDisenioExterno?.id}">${disenioExternoInstance?.justificacionDisenioExterno?.encodeAsHTML()}</g:link></td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name"><g:message code="disenioExterno.anexos.label" default="Anexos" /></td>
                            
                            <td valign="top" style="text-align: left;" class="value">
                                <ul>
                                <g:each in="${disenioExternoInstance.anexos}" var="a">
                                    <li><g:link controller="anexoPorTipo" action="show" id="${a.id}">${a?.encodeAsHTML()}</g:link></li>
                                </g:each>
                                </ul>
                            </td>
                            
                        </tr>
                    
                    </tbody>
                </table>
            </div>
            <div class="buttons">
                <g:form>
                    <g:hiddenField name="id" value="${disenioExternoInstance?.id}" />
                    <span class="button"><g:actionSubmit class="edit" action="edit" value="${message(code: 'default.button.edit.label', default: 'Edit')}" /></span>
                    <span class="button"><g:actionSubmit class="delete" action="delete" value="${message(code: 'default.button.delete.label', default: 'Delete')}" onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');" /></span>
                </g:form>
            </div>
        </div>
    </body>
</html>
