

<%@ page import="ar.com.telecom.pcs.entities.ControlHorasPedido" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="layout" content="main" />
        <g:set var="entityName" value="${message(code: 'controlHorasPedido.label', default: 'ControlHorasPedido')}" />
        <title><g:message code="default.create.label" args="[entityName]" /></title>
    </head>
    <body>
        <div class="nav">
            <span class="menuButton"><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></span>
            <span class="menuButton"><g:link class="list" action="list"><g:message code="default.list.label" args="[entityName]" /></g:link></span>
        </div>
        <div class="body">
            <h1><g:message code="default.create.label" args="[entityName]" /></h1>
            <g:if test="${flash.message}">
            <div class="message">${flash.message}</div>
            </g:if>
            <g:hasErrors bean="${controlHorasPedidoInstance}">
            <div class="errors">
                <g:renderErrors bean="${controlHorasPedidoInstance}" as="list" />
            </div>
            </g:hasErrors>
            <g:form action="save" >
                <div class="dialog">
                    <table>
                        <tbody>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="valorPedido"><g:message code="controlHorasPedido.valorPedido.label" default="Valor Pedido" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: controlHorasPedidoInstance, field: 'valorPedido', 'errors')}">
                                    <g:textField name="valorPedido" value="${fieldValue(bean: controlHorasPedidoInstance, field: 'valorPedido')}" />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="cantidadHoras"><g:message code="controlHorasPedido.cantidadHoras.label" default="Cantidad Horas" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: controlHorasPedidoInstance, field: 'cantidadHoras', 'errors')}">
                                    <g:textField name="cantidadHoras" value="${fieldValue(bean: controlHorasPedidoInstance, field: 'cantidadHoras')}" />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="pedido"><g:message code="controlHorasPedido.pedido.label" default="Pedido" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: controlHorasPedidoInstance, field: 'pedido', 'errors')}">
                                    <g:select name="pedido.id" from="${ar.com.telecom.pcs.entities.AbstractPedido.list()}" optionKey="id" value="${controlHorasPedidoInstance?.pedido?.id}"  />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="periodo"><g:message code="controlHorasPedido.periodo.label" default="Periodo" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: controlHorasPedidoInstance, field: 'periodo', 'errors')}">
                                    <g:select name="periodo.id" from="${ar.com.telecom.pcs.entities.Periodo.list()}" optionKey="id" value="${controlHorasPedidoInstance?.periodo?.id}"  />
                                </td>
                            </tr>
                        
                        </tbody>
                    </table>
                </div>
                <div class="buttons">
                    <span class="button"><g:submitButton name="create" class="save" value="${message(code: 'default.button.create.label', default: 'Create')}" /></span>
                </div>
            </g:form>
        </div>
    </body>
</html>
