
<%@ page import="ar.com.telecom.pcs.entities.ControlHorasPedido" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="layout" content="main" />
        <g:set var="entityName" value="${message(code: 'controlHorasPedido.label', default: 'ControlHorasPedido')}" />
        <title><g:message code="default.list.label" args="[entityName]" /></title>
    </head>
    <body>
        <div class="nav">
            <span class="menuButton"><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></span>
            <span class="menuButton"><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></span>
        </div>
        <div class="body">
            <h1><g:message code="default.list.label" args="[entityName]" /></h1>
            <g:if test="${flash.message}">
            <div class="message">${flash.message}</div>
            </g:if>
            <div class="list">
                <table>
                    <thead>
                        <tr>
                        
                            <g:sortableColumn property="id" title="${message(code: 'controlHorasPedido.id.label', default: 'Id')}" />
                        
                            <g:sortableColumn property="valorPedido" title="${message(code: 'controlHorasPedido.valorPedido.label', default: 'Valor Pedido')}" />
                        
                            <g:sortableColumn property="cantidadHoras" title="${message(code: 'controlHorasPedido.cantidadHoras.label', default: 'Cantidad Horas')}" />
                        
                            <th><g:message code="controlHorasPedido.pedido.label" default="Pedido" /></th>
                        
                            <th><g:message code="controlHorasPedido.periodo.label" default="Periodo" /></th>
                        
                        </tr>
                    </thead>
                    <tbody>
                    <g:each in="${controlHorasPedidoInstanceList}" status="i" var="controlHorasPedidoInstance">
                        <tr class="${(i % 2) == 0 ? 'odd' : 'even'}">
                        
                            <td><g:link action="show" id="${controlHorasPedidoInstance.id}">${fieldValue(bean: controlHorasPedidoInstance, field: "id")}</g:link></td>
                        
                            <td>${fieldValue(bean: controlHorasPedidoInstance, field: "valorPedido")}</td>
                        
                            <td>${fieldValue(bean: controlHorasPedidoInstance, field: "cantidadHoras")}</td>
                        
                            <td>${fieldValue(bean: controlHorasPedidoInstance, field: "pedido")}</td>
                        
                            <td>${fieldValue(bean: controlHorasPedidoInstance, field: "periodo")}</td>
                        
                        </tr>
                    </g:each>
                    </tbody>
                </table>
            </div>
            <div class="paginateButtons">
                <g:paginate total="${controlHorasPedidoInstanceTotal}" />
            </div>
        </div>
    </body>
</html>
