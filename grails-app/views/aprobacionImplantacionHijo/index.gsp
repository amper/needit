<%@page import="ar.com.telecom.util.NumberUtil"%>
<%@page import="ar.com.telecom.util.DateUtil"%>
<%@page import="ar.com.telecom.FaseMultiton"%>
<%@page import="ar.com.telecom.pcs.entities.RolAplicacion"%>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta name="layout" content="mainApp" />
<title>PCS</title>

<script type="text/javascript">
	var controllerActual = '${params.controller}';
</script>

<g:javascript library="animatedcollapse" />
<g:javascript library="aprobacionHijo" />
<g:javascript library="boxUser" />
<g:javascript library="animatedcollapse" />

</head> 
<body>

	<div class="contenedorTitulo">
		<g:titulo pedido="${impactoInstance?.pedido}" />
		<g:workflow pedidoId="${impactoInstance?.pedido?.id}" />
	</div>

		<div id="contenedorIzq">
			<g:if test="${flash.message}">
					<div class="message">
							${flash.message}
					</div>
			</g:if>
			<g:hasErrors bean="${impactoInstance?.pedido}">
				<div class="errors">
					<g:renderErrors bean="${impactoInstance?.pedido}" as="list" />
				</div>
			</g:hasErrors>
			<g:form class="formGeneral" useToken="true">
				<div class="seccionHide">
					<g:cabeceraPedido pedido="${impactoInstance?.pedido.parent}" />
					<g:render template="/templates/workflowHidden"/>
					<g:actionSubmit id="refreshView" controller="aprobacionImplantacionHijo" action="index" style="display:none" value="Refrescar vista" />

					<p class="tituloSeccion">Aprobaci&oacute;n implantaci&oacute;n</p>
					<div class="formSeccion">
						<div class="pDos">
							<label class='formLabel enRenglon'>Pedido hijo:</label>
							<g:select id="comboImpacto" name="comboImpacto"
									from="${impactos}" optionKey="id" optionValue="${{it.getDescripcion(impactoInstance?.pedido?.faseImplantacionHijo())}}"
									value="${impacto}" onChange="g_tabSeleccionado=0;"/>

							<g:if test="${impactoInstance?.pedido?.cumplioFecha(impactoInstance?.pedido?.faseImplantacionHijo())}">
                                <span title="Cumplido el ${DateUtil.toString(impactoInstance?.pedido?.getFecha(FaseMultiton.getFase(FaseMultiton.APROBACION_IMPLANTACION_HIJO)), true)}" id="pedido_estado_cumplido"></span>
							</g:if>

							<g:hiddenField name="impacto" id="impacto" value="${impacto}"></g:hiddenField>									
						</div>

						<div class="pDos">
								<g:if test="${!impactoInstance.esConsolidado() && tienePermisos && esElAprobador}">
									<label class="formLabel2" for='inputUser'>Aprobador</label>
									<div class="boxUser" id="aprobador">
										<g:render template="/templates/userBox"
											model="['person': aprobacionInstance?.usuario?: usuarioLogueado, 'box':'aprobador']"></g:render>
									</div>
								
									<input type="hidden" id="usuarioAnterior" name="usuarioAnterior" value="${aprobacionInstance?.usuario}" />

									<g:secureRemoteLink title="Eliminar" class="limpiarBox"
										update="aprobador"
										id="${RolAplicacion.APROBADOR_FASE}"
										params="['username': '', 'box':'aprobador', 'grupoResponsable':aprobacionInstance?.grupo, 'pedidoId':pedidoHijo?.parent?.id, 'impacto': impactoInstance?.pedido?.id]"
										action="completarUsuario">
									</g:secureRemoteLink>
								
							</g:if>
						</div>

						<div class="separator"></div>
						
						<div id="datosImpactoSeleccionado"></div>
						</div>

						<script type="text/javascript">
							seleccionImpacto($('comboImpacto').value);
						</script>
						
						<div id="botonera">
							<g:render template="botonera" model="['impactoInstance':impactoInstance, 'esElAprobador':esElAprobador, 'usuarioLogueado':usuarioLogueado]"></g:render>
						</div>
						
				</div>
			</g:form>
		</div>
		<!-- fin contenedor izq -->

		<!-- Lateral Derecho -->
	<g:render template="/templates/menuLateral" model="['pedidoInstance':impactoInstance?.pedido.parent]"/>
	<!-- Fin Lateral Derecho -->

		<!-- 
	************
	************
	MODALS POPUP
	************
	************
	************
	-->

		<!-- popup ayuda seccion -->
		<div id="dialog-help" title="Ayuda Aprobar Pedido" style="display: none;">
			<div style="height: 350px; width: 100%; overflow: auto;">
				<p class="helpTitle">Seccion ayuda</p>
				<p>Hola esto es una seccion de ayuda Hola esto es una seccion de</p>
			</div>
		</div>
		<!-- fin popup ayuda seccion -->
</body>
</html>
