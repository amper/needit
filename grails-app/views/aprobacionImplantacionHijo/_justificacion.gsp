<g:if test="${impactoInstance?.pedido?.puedeEditarAprobacionImplantacion(usuarioLogueado) && impactoInstance?.pedido?.tieneAprobacionesPendientes(usuarioLogueado)}">
	<div class="pDos">
		<label for="justificacionAprobacion" class="formLabel">Justificaci&oacute;n</label>
		<g:textArea name="justificacionAprobacion" value="${pedidoInstance?.getJustificacionAI(usuarioLogueado)}"></g:textArea>
	</div>
</g:if>
<%--<g:else>--%>
<%--	<g:if test="${impactoInstance?.pedido?.aproboAprobacionImplantacion()}">--%>
<%--		<div class="pDos">--%>
<%--			<g:if test="${impactoInstance?.pedido?.getJustificacionAI(usuarioLogueado)}">--%>
<%--				<label for="justificacionAprobacion" class="formLabel">Justificaci&oacute;n</label>--%>
<%--				<p class="info">${impactoInstance?.pedido?.getJustificacionAI(usuarioLogueado)}</p>--%>
<%--			</g:if>--%>
<%--			<g:else>--%>
<%--				<label for="justificacionAprobacion" class="formLabel">No se ingres&oacute; justificaci&oacute;n</label>--%>
<%--			</g:else>--%>
<%--		</div>--%>
<%--	</g:if>--%>
<%--</g:else>--%>
 