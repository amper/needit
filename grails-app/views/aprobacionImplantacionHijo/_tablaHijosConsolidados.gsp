<div class="list" style="text-align: center;">
    <table class="tablaAutoWidth">
      <tr>
      	<th class="encabezado0"/>
        <th>Pedido</th>
        <th>Sistema - Tipo de Impacto</th>
        <th>Grupo Responsable</th>
        <th>Asignatario</th>
        <th>Macroestado</th>
      </tr>
      <g:each in="${impactoInstance?.pedido?.pedidosHijos}" status="i" var="hijo">
	  <%nombreDiv = 'aprobacionesHijoDiv_' + hijo.id%>
      <tr class="${(i % 2) == 0 ? 'odd' : 'even'}" onClick="javascript:animatedcollapse.toggle('${nombreDiv}');">
        <td id="flecha_${hijo.id}" class="indicatorValoracion"></td>
        <td>${hijo.getNumero()}</td>
        <td>${hijo.tipoPlanificacion}</td>
        <td>${hijo.grupoResponsable}</td>
        <td><g:userLine etiqueta="${hijo?.legajoUsuarioResponsable}" legajo="${hijo?.legajoUsuarioResponsable}" controller="${params.controller}"/></td>
        <td>${hijo.faseActual.macroEstado}</td>
      </tr>
      <tr style="background-color:#96afb4;">
        <td colspan="6"><div id="${nombreDiv}" style="display: none"></div></td>
      </tr>
		<script type="text/javascript">
		  animatedcollapse.addDiv('${nombreDiv}', 'fade=0', 'hide=1', 'speed=100');
		</script>
       </g:each>

    </table>
</div> 

<%--<g:if test="${pedidoInstance?.hijosDisponiblesParaAprobacionImplantacion()}">--%>
<%--	<g:render template="justificacion" 	model="['pedidoInstance':pedidoInstance, 'impactoInstance': impactoInstance]"></g:render>--%>
<%--</g:if>--%>
<%--<g:else>--%>
<%--	<div class="separator"></div>--%>
<%--	<g:if test="${!pedidoInstance?.aproboAprobacionImplantacion()}">--%>
<%--		<div><b>Nota:</b> Esta actividad estar� disponible cuando todos los cambios hijos que la integran est�n en la fase Aprobaci�n implantacion consolidada.</div>--%>
<%--	</g:if>--%>
<%--	<g:else>--%>
<%--		<g:render template="justificacion" 	model="['pedidoInstance':pedidoInstance, 'impactoInstance': impactoInstance]"></g:render>--%>
<%--	</g:else>	--%>
<%--</g:else>--%>



<script type="text/javascript">
function aprobacionesToggle(divobj, state) {
	var id = divobj.id;
	var idImpacto = id.replace('aprobacionesHijoDiv_', '');

	if (state == 'block') {
		var sUrl = '<g:createLink controller="aprobacionImplantacionHijo" action="getAprobacionesHijo"/>' + '/' + idImpacto;
		new Ajax.Request(sUrl, {
			onSuccess: function(transport) {
				$(id).innerHTML = transport.responseText;
			}
		});
		$('flecha_'+idImpacto).style.background='url(../images/DownTriangleIcon.gif) no-repeat';
	} else {
		$(id).innerHTML = '';
		$('flecha_'+idImpacto).style.background='url(../images/RightTriangleIcon.gif) no-repeat';
	}
}

animatedcollapse.ontoggle = function($, divobj, state){ aprobacionesToggle(divobj, state); };
animatedcollapse.init();
</script>
