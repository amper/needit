<%@page import="ar.com.telecom.FaseMultiton"%>
<%@page import="ar.com.telecom.pcs.entities.RolAplicacion"%>
<%@page import="ar.com.telecom.util.DateUtil"%>

<div class="list" style="text-align: center;">
<g:set var="aprobaciones" value="${impactoInstance?.pedido?.aprobacionesDeFase(FaseMultiton.getFase(FaseMultiton.APROBACION_IMPLANTACION_HIJO))}" />

<g:if test="${!aprobaciones.isEmpty()}">
   <table style="width:90%;">
 	 <tr>
		<th>&Aacute;rea aprobadora</th>
		<th>Descripci&oacute;n</th>
		<th>Responsable</th>
		<th>Estado</th>
		<th>Fecha</th>
		<th>Justificaci&oacute;n</th>
	</tr>
	
	<g:each in="${aprobaciones}" status="i" var="aprobacion">
		 <tr class="${(i % 2) == 0 ? 'odd' : 'even'}">
			<td>${aprobacion.areaSoporte? aprobacion.areaSoporte : (aprobacion.rol ? RolAplicacion.findByCodigoRol(aprobacion.rol) : null)}</td>
			<td>${aprobacion?.descripcionAprobacionSolicitada}</td>
			<td><g:userDescription legajo="${aprobacion?.usuario}" pedido="${aprobacion?.id}" /></td>
			<td>${aprobacion.estadoDescripcion()}</td>
			<td>${DateUtil.toString(aprobacion.fechaCumplimiento)}</td>
			<td>${aprobacion.justificacion}</td>
		 </tr>
	</g:each> 
   </table>
</g:if>

<g:else>
	<br>
	<center>
		<b>El pedido ${impactoInstance?.pedido} no tiene aprobaciones en la fase aprobacion implantacion</b>
	</center>
</g:else>
</div>