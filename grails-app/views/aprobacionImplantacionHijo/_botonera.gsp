<%@page import="ar.com.telecom.pedido.especificacionImpacto.Impacto"%>
<g:if test="${tienePermisos && impactoInstance?.pedido?.puedeEditarAprobacionImplantacion(usuarioLogueado) && impactoInstance?.pedido?.tieneAprobacionesPendientes(usuarioLogueado)}">
	<div class="formSeccionButton" style="margin-top: 20px;">
		<g:actionSubmit class="formSeccionSubmitButton" id="Guardar" action="grabarFormulario" value="Guardar"/>
		<g:if test="${esElAprobador && !impactoInstance?.pedido?.puedeSoloGuardar(faseATrabajar, usuarioLogueado)}">
			<g:actionSubmit class="formSeccionSubmitButton" id="Denegar" action="denegarPedido" value="Denegar" />
			<g:actionSubmit class="formSeccionSubmitButton" id="Aprobar" action="aprobarPedido" value="Aprobar" />
		</g:if>
	</div>
</g:if>
 