
<%@ page import="ar.com.telecom.pcs.entities.TipoAnexo" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="layout" content="main" />
        <g:set var="entityName" value="${message(code: 'tipoAnexo.label', default: 'TipoAnexo')}" />
        <title><g:message code="default.list.label" args="[entityName]" /></title>
    </head>
    <body>
        <div class="nav">
            <span class="menuButton"><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></span>
            <span class="menuButton"><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></span>
        </div>
        <div class="body">
            <h1><g:message code="default.list.label" args="[entityName]" /></h1>
            <g:if test="${flash.message}">
            <div class="message">${flash.message}</div>
            </g:if>
            <div class="list">
                <table>
                    <thead>
                        <tr>
                        
                            <g:sortableColumn property="id" title="${message(code: 'tipoAnexo.id.label', default: 'Id')}" />
                        
                            <g:sortableColumn property="descripcion" title="${message(code: 'tipoAnexo.descripcion.label', default: 'Descripcion')}" />
                        
                            <th><g:message code="tipoAnexo.fase.label" default="Fase" /></th>
                        
                        </tr>
                    </thead>
                    <tbody>
                    <g:each in="${tipoAnexoInstanceList}" status="i" var="tipoAnexoInstance">
                        <tr class="${(i % 2) == 0 ? 'odd' : 'even'}">
                        
                            <td><g:link action="show" id="${tipoAnexoInstance.id}">${fieldValue(bean: tipoAnexoInstance, field: "id")}</g:link></td>
                        
                            <td>${fieldValue(bean: tipoAnexoInstance, field: "descripcion")}</td>
                        
                            <td>${fieldValue(bean: tipoAnexoInstance, field: "fase")}</td>
                        
                        </tr>
                    </g:each>
                    </tbody>
                </table>
            </div>
            <div class="paginateButtons">
                <g:paginate total="${tipoAnexoInstanceTotal}" />
            </div>
        </div>
    </body>
</html>
