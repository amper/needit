
<%@ page import="ar.com.telecom.pcs.entities.PedidoHijo" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="layout" content="main" />
        <g:set var="entityName" value="${message(code: 'pedidoHijo.label', default: 'PedidoHijo')}" />
        <title><g:message code="default.show.label" args="[entityName]" /></title>
    </head>
    <body>
        <div class="nav">
            <span class="menuButton"><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></span>
            <span class="menuButton"><g:link class="list" action="list"><g:message code="default.list.label" args="[entityName]" /></g:link></span>
            <span class="menuButton"><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></span>
        </div>
        <div class="body">
            <h1><g:message code="default.show.label" args="[entityName]" /></h1>
            <g:if test="${flash.message}">
            <div class="message">${flash.message}</div>
            </g:if>
            <div class="dialog">
                <table>
                    <tbody>
                    
                        <tr class="prop">
                            <td valign="top" class="name"><g:message code="pedidoHijo.id.label" default="Id" /></td>
                            
                            <td valign="top" class="value">${fieldValue(bean: pedidoHijoInstance, field: "id")}</td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name"><g:message code="pedidoHijo.titulo.label" default="Titulo" /></td>
                            
                            <td valign="top" class="value">${fieldValue(bean: pedidoHijoInstance, field: "titulo")}</td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name"><g:message code="pedidoHijo.legajoUsuarioCreador.label" default="Legajo Usuario Creador" /></td>
                            
                            <td valign="top" class="value">${fieldValue(bean: pedidoHijoInstance, field: "legajoUsuarioCreador")}</td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name"><g:message code="pedidoHijo.fechaUltimaModificacion.label" default="Fecha Ultima Modificacion" /></td>
                            
                            <td valign="top" class="value"><g:formatDate date="${pedidoHijoInstance?.fechaUltimaModificacion}" /></td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name"><g:message code="pedidoHijo.fechaCargaPedido.label" default="Fecha Carga Pedido" /></td>
                            
                            <td valign="top" class="value"><g:formatDate date="${pedidoHijoInstance?.fechaCargaPedido}" /></td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name"><g:message code="pedidoHijo.fechaCancelacion.label" default="Fecha Cancelacion" /></td>
                            
                            <td valign="top" class="value"><g:formatDate date="${pedidoHijoInstance?.fechaCancelacion}" /></td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name"><g:message code="pedidoHijo.fechaSuspension.label" default="Fecha Suspension" /></td>
                            
                            <td valign="top" class="value"><g:formatDate date="${pedidoHijoInstance?.fechaSuspension}" /></td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name"><g:message code="pedidoHijo.motivoCancelacion.label" default="Motivo Cancelacion" /></td>
                            
                            <td valign="top" class="value">${fieldValue(bean: pedidoHijoInstance, field: "motivoCancelacion")}</td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name"><g:message code="pedidoHijo.motivoSuspension.label" default="Motivo Suspension" /></td>
                            
                            <td valign="top" class="value">${fieldValue(bean: pedidoHijoInstance, field: "motivoSuspension")}</td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name"><g:message code="pedidoHijo.sistema.label" default="Sistema" /></td>
                            
                            <td valign="top" class="value"><g:link controller="sistema" action="show" id="${pedidoHijoInstance?.sistema?.id}">${pedidoHijoInstance?.sistema?.encodeAsHTML()}</g:link></td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name"><g:message code="pedidoHijo.areaSoporte.label" default="Area Soporte" /></td>
                            
                            <td valign="top" class="value"><g:link controller="areaSoporte" action="show" id="${pedidoHijoInstance?.areaSoporte?.id}">${pedidoHijoInstance?.areaSoporte?.encodeAsHTML()}</g:link></td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name"><g:message code="pedidoHijo.impactaBaseArquitectura.label" default="Impacta Base Arquitectura" /></td>
                            
                            <td valign="top" class="value"><g:formatBoolean boolean="${pedidoHijoInstance?.impactaBaseArquitectura}" /></td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name"><g:message code="pedidoHijo.requiereManualUsuario.label" default="Requiere Manual Usuario" /></td>
                            
                            <td valign="top" class="value"><g:formatBoolean boolean="${pedidoHijoInstance?.requiereManualUsuario}" /></td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name"><g:message code="pedidoHijo.actualizaCarpetaOperativa.label" default="Actualiza Carpeta Operativa" /></td>
                            
                            <td valign="top" class="value"><g:formatBoolean boolean="${pedidoHijoInstance?.actualizaCarpetaOperativa}" /></td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name"><g:message code="pedidoHijo.gradoSatisfaccion.label" default="Grado Satisfaccion" /></td>
                            
                            <td valign="top" class="value"><g:link controller="gradoSatisfaccion" action="show" id="${pedidoHijoInstance?.gradoSatisfaccion?.id}">${pedidoHijoInstance?.gradoSatisfaccion?.encodeAsHTML()}</g:link></td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name"><g:message code="pedidoHijo.comentarioCierre.label" default="Comentario Cierre" /></td>
                            
                            <td valign="top" class="value">${fieldValue(bean: pedidoHijoInstance, field: "comentarioCierre")}</td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name"><g:message code="pedidoHijo.leccionesAprendidas.label" default="Lecciones Aprendidas" /></td>
                            
                            <td valign="top" class="value">${fieldValue(bean: pedidoHijoInstance, field: "leccionesAprendidas")}</td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name"><g:message code="pedidoHijo.grupoResponsable.label" default="Grupo Responsable" /></td>
                            
                            <td valign="top" class="value">${fieldValue(bean: pedidoHijoInstance, field: "grupoResponsable")}</td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name"><g:message code="pedidoHijo.legajoUsuarioResponsable.label" default="Legajo Usuario Responsable" /></td>
                            
                            <td valign="top" class="value">${fieldValue(bean: pedidoHijoInstance, field: "legajoUsuarioResponsable")}</td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name"><g:message code="pedidoHijo.fechaCierreEspecificacion.label" default="Fecha Cierre Especificacion" /></td>
                            
                            <td valign="top" class="value"><g:formatDate date="${pedidoHijoInstance?.fechaCierreEspecificacion}" /></td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name"><g:message code="pedidoHijo.fechaFinActividades.label" default="Fecha Fin Actividades" /></td>
                            
                            <td valign="top" class="value"><g:formatDate date="${pedidoHijoInstance?.fechaFinActividades}" /></td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name"><g:message code="pedidoHijo.fechaFinEjecucionPAU.label" default="Fecha Fin Ejecucion PAU" /></td>
                            
                            <td valign="top" class="value"><g:formatDate date="${pedidoHijoInstance?.fechaFinEjecucionPAU}" /></td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name"><g:message code="pedidoHijo.fechaAprobacionPAU.label" default="Fecha Aprobacion PAU" /></td>
                            
                            <td valign="top" class="value"><g:formatDate date="${pedidoHijoInstance?.fechaAprobacionPAU}" /></td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name"><g:message code="pedidoHijo.fechaAprobacionImplantacion.label" default="Fecha Aprobacion Implantacion" /></td>
                            
                            <td valign="top" class="value"><g:formatDate date="${pedidoHijoInstance?.fechaAprobacionImplantacion}" /></td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name"><g:message code="pedidoHijo.fechaImplantacion.label" default="Fecha Implantacion" /></td>
                            
                            <td valign="top" class="value"><g:formatDate date="${pedidoHijoInstance?.fechaImplantacion}" /></td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name"><g:message code="pedidoHijo.fechaNormalizacion.label" default="Fecha Normalizacion" /></td>
                            
                            <td valign="top" class="value"><g:formatDate date="${pedidoHijoInstance?.fechaNormalizacion}" /></td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name"><g:message code="pedidoHijo.fechaCierre.label" default="Fecha Cierre" /></td>
                            
                            <td valign="top" class="value"><g:formatDate date="${pedidoHijoInstance?.fechaCierre}" /></td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name"><g:message code="pedidoHijo.actividades.label" default="Actividades" /></td>
                            
                            <td valign="top" style="text-align: left;" class="value">
                                <ul>
                                <g:each in="${pedidoHijoInstance.actividades}" var="a">
                                    <li><g:link controller="actividad" action="show" id="${a.id}">${a?.encodeAsHTML()}</g:link></li>
                                </g:each>
                                </ul>
                            </td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name"><g:message code="pedidoHijo.anexos.label" default="Anexos" /></td>
                            
                            <td valign="top" style="text-align: left;" class="value">
                                <ul>
                                <g:each in="${pedidoHijoInstance.anexos}" var="a">
                                    <li><g:link controller="anexoPorTipo" action="show" id="${a.id}">${a?.encodeAsHTML()}</g:link></li>
                                </g:each>
                                </ul>
                            </td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name"><g:message code="pedidoHijo.aprobaciones.label" default="Aprobaciones" /></td>
                            
                            <td valign="top" style="text-align: left;" class="value">
                                <ul>
                                <g:each in="${pedidoHijoInstance.aprobaciones}" var="a">
                                    <li><g:link controller="aprobacion" action="show" id="${a.id}">${a?.encodeAsHTML()}</g:link></li>
                                </g:each>
                                </ul>
                            </td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name"><g:message code="pedidoHijo.diseniosExternos.label" default="Disenios Externos" /></td>
                            
                            <td valign="top" style="text-align: left;" class="value">
                                <ul>
                                <g:each in="${pedidoHijoInstance.diseniosExternos}" var="d">
                                    <li><g:link controller="disenioExterno" action="show" id="${d.id}">${d?.encodeAsHTML()}</g:link></li>
                                </g:each>
                                </ul>
                            </td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name"><g:message code="pedidoHijo.estrategiasPrueba.label" default="Estrategias Prueba" /></td>
                            
                            <td valign="top" style="text-align: left;" class="value">
                                <ul>
                                <g:each in="${pedidoHijoInstance.estrategiasPrueba}" var="e">
                                    <li><g:link controller="estrategiaPrueba" action="show" id="${e.id}">${e?.encodeAsHTML()}</g:link></li>
                                </g:each>
                                </ul>
                            </td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name"><g:message code="pedidoHijo.faseActual.label" default="Fase Actual" /></td>
                            
                            <td valign="top" class="value"><g:link controller="fase" action="show" id="${pedidoHijoInstance?.faseActual?.id}">${pedidoHijoInstance?.faseActual?.encodeAsHTML()}</g:link></td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name"><g:message code="pedidoHijo.logModificaciones.label" default="Log Modificaciones" /></td>
                            
                            <td valign="top" style="text-align: left;" class="value">
                                <ul>
                                <g:each in="${pedidoHijoInstance.logModificaciones}" var="l">
                                    <li><g:link controller="logModificaciones" action="show" id="${l.id}">${l?.encodeAsHTML()}</g:link></li>
                                </g:each>
                                </ul>
                            </td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name"><g:message code="pedidoHijo.pedidoPadre.label" default="Pedido Padre" /></td>
                            
                            <td valign="top" class="value"><g:link controller="pedido" action="show" id="${pedidoHijoInstance?.pedidoPadre?.id}">${pedidoHijoInstance?.pedidoPadre?.encodeAsHTML()}</g:link></td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name"><g:message code="pedidoHijo.planificaciones.label" default="Planificaciones" /></td>
                            
                            <td valign="top" style="text-align: left;" class="value">
                                <ul>
                                <g:each in="${pedidoHijoInstance.planificaciones}" var="p">
                                    <li><g:link controller="planificacionEsfuerzo" action="show" id="${p.id}">${p?.encodeAsHTML()}</g:link></li>
                                </g:each>
                                </ul>
                            </td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name"><g:message code="pedidoHijo.tipoImpacto.label" default="Tipo Impacto" /></td>
                            
                            <td valign="top" class="value"><g:link controller="tipoImpacto" action="show" id="${pedidoHijoInstance?.tipoImpacto?.id}">${pedidoHijoInstance?.tipoImpacto?.encodeAsHTML()}</g:link></td>
                            
                        </tr>
                    
                    </tbody>
                </table>
            </div>
            <div class="buttons">
                <g:form>
                    <g:hiddenField name="id" value="${pedidoHijoInstance?.id}" />
                    <span class="button"><g:actionSubmit class="edit" action="edit" value="${message(code: 'default.button.edit.label', default: 'Edit')}" /></span>
                    <span class="button"><g:actionSubmit class="delete" action="delete" value="${message(code: 'default.button.delete.label', default: 'Delete')}" onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');" /></span>
                </g:form>
            </div>
        </div>
    </body>
</html>
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                           