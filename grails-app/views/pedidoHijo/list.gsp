
<%@ page import="ar.com.telecom.pcs.entities.PedidoHijo" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="layout" content="main" />
        <g:set var="entityName" value="${message(code: 'pedidoHijo.label', default: 'PedidoHijo')}" />
        <title><g:message code="default.list.label" args="[entityName]" /></title>
    </head>
    <body>
        <div class="nav">
            <span class="menuButton"><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></span>
            <span class="menuButton"><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></span>
        </div>
        <div class="body">
            <h1><g:message code="default.list.label" args="[entityName]" /></h1>
            <g:if test="${flash.message}">
            <div class="message">${flash.message}</div>
            </g:if>
            <div class="list">
                <table>
                    <thead>
                        <tr>
                        
                            <g:sortableColumn property="id" title="${message(code: 'pedidoHijo.id.label', default: 'Id')}" />
                        
                            <g:sortableColumn property="titulo" title="${message(code: 'pedidoHijo.titulo.label', default: 'Titulo')}" />
                        
                            <g:sortableColumn property="legajoUsuarioCreador" title="${message(code: 'pedidoHijo.legajoUsuarioCreador.label', default: 'Legajo Usuario Creador')}" />
                        
                            <g:sortableColumn property="fechaUltimaModificacion" title="${message(code: 'pedidoHijo.fechaUltimaModificacion.label', default: 'Fecha Ultima Modificacion')}" />
                        
                            <g:sortableColumn property="fechaCargaPedido" title="${message(code: 'pedidoHijo.fechaCargaPedido.label', default: 'Fecha Carga Pedido')}" />
                        
                            <g:sortableColumn property="fechaCancelacion" title="${message(code: 'pedidoHijo.fechaCancelacion.label', default: 'Fecha Cancelacion')}" />
                        
                        </tr>
                    </thead>
                    <tbody>
                    <g:each in="${pedidoHijoInstanceList}" status="i" var="pedidoHijoInstance">
                        <tr class="${(i % 2) == 0 ? 'odd' : 'even'}">
                        
                            <td><g:link action="show" id="${pedidoHijoInstance.id}">${fieldValue(bean: pedidoHijoInstance, field: "id")}</g:link></td>
                        
                            <td>${fieldValue(bean: pedidoHijoInstance, field: "titulo")}</td>
                        
                            <td>${fieldValue(bean: pedidoHijoInstance, field: "legajoUsuarioCreador")}</td>
                        
                            <td><g:formatDate date="${pedidoHijoInstance.fechaUltimaModificacion}" /></td>
                        
                            <td><g:formatDate date="${pedidoHijoInstance.fechaCargaPedido}" /></td>
                        
                            <td><g:formatDate date="${pedidoHijoInstance.fechaCancelacion}" /></td>
                        
                        </tr>
                    </g:each>
                    </tbody>
                </table>
            </div>
            <div class="paginateButtons">
                <g:paginate total="${pedidoHijoInstanceTotal}" />
            </div>
        </div>
    </body>
</html>
