

<%@ page import="ar.com.telecom.pcs.entities.PedidoHijo" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="layout" content="main" />
        <g:set var="entityName" value="${message(code: 'pedidoHijo.label', default: 'PedidoHijo')}" />
        <title><g:message code="default.create.label" args="[entityName]" /></title>
    </head>
    <body>
        <div class="nav">
            <span class="menuButton"><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></span>
            <span class="menuButton"><g:link class="list" action="list"><g:message code="default.list.label" args="[entityName]" /></g:link></span>
        </div>
        <div class="body">
            <h1><g:message code="default.create.label" args="[entityName]" /></h1>
            <g:if test="${flash.message}">
            <div class="message">${flash.message}</div>
            </g:if>
            <g:hasErrors bean="${pedidoHijoInstance}">
            <div class="errors">
                <g:renderErrors bean="${pedidoHijoInstance}" as="list" />
            </div>
            </g:hasErrors>
            <g:form action="save" >
                <div class="dialog">
                    <table>
                        <tbody>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="titulo"><g:message code="pedidoHijo.titulo.label" default="Titulo" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: pedidoHijoInstance, field: 'titulo', 'errors')}">
                                    <g:textField name="titulo" maxlength="200" value="${pedidoHijoInstance?.titulo}" />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="legajoUsuarioCreador"><g:message code="pedidoHijo.legajoUsuarioCreador.label" default="Legajo Usuario Creador" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: pedidoHijoInstance, field: 'legajoUsuarioCreador', 'errors')}">
                                    <g:textField name="legajoUsuarioCreador" value="${pedidoHijoInstance?.legajoUsuarioCreador}" />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="fechaUltimaModificacion"><g:message code="pedidoHijo.fechaUltimaModificacion.label" default="Fecha Ultima Modificacion" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: pedidoHijoInstance, field: 'fechaUltimaModificacion', 'errors')}">
                                    <g:datePicker name="fechaUltimaModificacion" precision="day" value="${pedidoHijoInstance?.fechaUltimaModificacion}" default="none" noSelection="['': '']" />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="fechaCargaPedido"><g:message code="pedidoHijo.fechaCargaPedido.label" default="Fecha Carga Pedido" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: pedidoHijoInstance, field: 'fechaCargaPedido', 'errors')}">
                                    <g:datePicker name="fechaCargaPedido" precision="day" value="${pedidoHijoInstance?.fechaCargaPedido}" default="none" noSelection="['': '']" />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="fechaCancelacion"><g:message code="pedidoHijo.fechaCancelacion.label" default="Fecha Cancelacion" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: pedidoHijoInstance, field: 'fechaCancelacion', 'errors')}">
                                    <g:datePicker name="fechaCancelacion" precision="day" value="${pedidoHijoInstance?.fechaCancelacion}" default="none" noSelection="['': '']" />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="fechaSuspension"><g:message code="pedidoHijo.fechaSuspension.label" default="Fecha Suspension" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: pedidoHijoInstance, field: 'fechaSuspension', 'errors')}">
                                    <g:datePicker name="fechaSuspension" precision="day" value="${pedidoHijoInstance?.fechaSuspension}" default="none" noSelection="['': '']" />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="motivoCancelacion"><g:message code="pedidoHijo.motivoCancelacion.label" default="Motivo Cancelacion" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: pedidoHijoInstance, field: 'motivoCancelacion', 'errors')}">
                                    <g:textField name="motivoCancelacion" value="${pedidoHijoInstance?.motivoCancelacion}" />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="motivoSuspension"><g:message code="pedidoHijo.motivoSuspension.label" default="Motivo Suspension" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: pedidoHijoInstance, field: 'motivoSuspension', 'errors')}">
                                    <g:textField name="motivoSuspension" value="${pedidoHijoInstance?.motivoSuspension}" />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="sistema"><g:message code="pedidoHijo.sistema.label" default="Sistema" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: pedidoHijoInstance, field: 'sistema', 'errors')}">
                                    <g:select name="sistema.id" from="${ar.com.telecom.pcs.entities.Sistema.list()}" optionKey="id" value="${pedidoHijoInstance?.sistema?.id}" noSelection="['null': '']" />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="areaSoporte"><g:message code="pedidoHijo.areaSoporte.label" default="Area Soporte" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: pedidoHijoInstance, field: 'areaSoporte', 'errors')}">
                                    <g:select name="areaSoporte.id" from="${ar.com.telecom.pcs.entities.AreaSoporte.list()}" optionKey="id" value="${pedidoHijoInstance?.areaSoporte?.id}" noSelection="['null': '']" />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="impactaBaseArquitectura"><g:message code="pedidoHijo.impactaBaseArquitectura.label" default="Impacta Base Arquitectura" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: pedidoHijoInstance, field: 'impactaBaseArquitectura', 'errors')}">
                                    <g:checkBox name="impactaBaseArquitectura" value="${pedidoHijoInstance?.impactaBaseArquitectura}" />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="requiereManualUsuario"><g:message code="pedidoHijo.requiereManualUsuario.label" default="Requiere Manual Usuario" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: pedidoHijoInstance, field: 'requiereManualUsuario', 'errors')}">
                                    <g:checkBox name="requiereManualUsuario" value="${pedidoHijoInstance?.requiereManualUsuario}" />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="actualizaCarpetaOperativa"><g:message code="pedidoHijo.actualizaCarpetaOperativa.label" default="Actualiza Carpeta Operativa" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: pedidoHijoInstance, field: 'actualizaCarpetaOperativa', 'errors')}">
                                    <g:checkBox name="actualizaCarpetaOperativa" value="${pedidoHijoInstance?.actualizaCarpetaOperativa}" />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="gradoSatisfaccion"><g:message code="pedidoHijo.gradoSatisfaccion.label" default="Grado Satisfaccion" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: pedidoHijoInstance, field: 'gradoSatisfaccion', 'errors')}">
                                    <g:select name="gradoSatisfaccion.id" from="${ar.com.telecom.pcs.entities.GradoSatisfaccion.list()}" optionKey="id" value="${pedidoHijoInstance?.gradoSatisfaccion?.id}" noSelection="['null': '']" />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="comentarioCierre"><g:message code="pedidoHijo.comentarioCierre.label" default="Comentario Cierre" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: pedidoHijoInstance, field: 'comentarioCierre', 'errors')}">
                                    <g:textField name="comentarioCierre" value="${pedidoHijoInstance?.comentarioCierre}" />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="leccionesAprendidas"><g:message code="pedidoHijo.leccionesAprendidas.label" default="Lecciones Aprendidas" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: pedidoHijoInstance, field: 'leccionesAprendidas', 'errors')}">
                                    <g:textField name="leccionesAprendidas" value="${pedidoHijoInstance?.leccionesAprendidas}" />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="grupoResponsable"><g:message code="pedidoHijo.grupoResponsable.label" default="Grupo Responsable" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: pedidoHijoInstance, field: 'grupoResponsable', 'errors')}">
                                    <g:textField name="grupoResponsable" maxlength="50" value="${pedidoHijoInstance?.grupoResponsable}" />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="legajoUsuarioResponsable"><g:message code="pedidoHijo.legajoUsuarioResponsable.label" default="Legajo Usuario Responsable" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: pedidoHijoInstance, field: 'legajoUsuarioResponsable', 'errors')}">
                                    <g:textField name="legajoUsuarioResponsable" maxlength="20" value="${pedidoHijoInstance?.legajoUsuarioResponsable}" />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="fechaCierreEspecificacion"><g:message code="pedidoHijo.fechaCierreEspecificacion.label" default="Fecha Cierre Especificacion" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: pedidoHijoInstance, field: 'fechaCierreEspecificacion', 'errors')}">
                                    <g:datePicker name="fechaCierreEspecificacion" precision="day" value="${pedidoHijoInstance?.fechaCierreEspecificacion}" default="none" noSelection="['': '']" />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="fechaFinActividades"><g:message code="pedidoHijo.fechaFinActividades.label" default="Fecha Fin Actividades" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: pedidoHijoInstance, field: 'fechaFinActividades', 'errors')}">
                                    <g:datePicker name="fechaFinActividades" precision="day" value="${pedidoHijoInstance?.fechaFinActividades}" default="none" noSelection="['': '']" />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="fechaFinEjecucionPAU"><g:message code="pedidoHijo.fechaFinEjecucionPAU.label" default="Fecha Fin Ejecucion PAU" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: pedidoHijoInstance, field: 'fechaFinEjecucionPAU', 'errors')}">
                                    <g:datePicker name="fechaFinEjecucionPAU" precision="day" value="${pedidoHijoInstance?.fechaFinEjecucionPAU}" default="none" noSelection="['': '']" />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="fechaAprobacionPAU"><g:message code="pedidoHijo.fechaAprobacionPAU.label" default="Fecha Aprobacion PAU" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: pedidoHijoInstance, field: 'fechaAprobacionPAU', 'errors')}">
                                    <g:datePicker name="fechaAprobacionPAU" precision="day" value="${pedidoHijoInstance?.fechaAprobacionPAU}" default="none" noSelection="['': '']" />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="fechaAprobacionImplantacion"><g:message code="pedidoHijo.fechaAprobacionImplantacion.label" default="Fecha Aprobacion Implantacion" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: pedidoHijoInstance, field: 'fechaAprobacionImplantacion', 'errors')}">
                                    <g:datePicker name="fechaAprobacionImplantacion" precision="day" value="${pedidoHijoInstance?.fechaAprobacionImplantacion}" default="none" noSelection="['': '']" />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="fechaImplantacion"><g:message code="pedidoHijo.fechaImplantacion.label" default="Fecha Implantacion" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: pedidoHijoInstance, field: 'fechaImplantacion', 'errors')}">
                                    <g:datePicker name="fechaImplantacion" precision="day" value="${pedidoHijoInstance?.fechaImplantacion}" default="none" noSelection="['': '']" />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="fechaNormalizacion"><g:message code="pedidoHijo.fechaNormalizacion.label" default="Fecha Normalizacion" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: pedidoHijoInstance, field: 'fechaNormalizacion', 'errors')}">
                                    <g:datePicker name="fechaNormalizacion" precision="day" value="${pedidoHijoInstance?.fechaNormalizacion}" default="none" noSelection="['': '']" />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="fechaCierre"><g:message code="pedidoHijo.fechaCierre.label" default="Fecha Cierre" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: pedidoHijoInstance, field: 'fechaCierre', 'errors')}">
                                    <g:datePicker name="fechaCierre" precision="day" value="${pedidoHijoInstance?.fechaCierre}" default="none" noSelection="['': '']" />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="faseActual"><g:message code="pedidoHijo.faseActual.label" default="Fase Actual" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: pedidoHijoInstance, field: 'faseActual', 'errors')}">
                                    <g:select name="faseActual.id" from="${ar.com.telecom.pcs.entities.Fase.list()}" optionKey="id" value="${pedidoHijoInstance?.faseActual?.id}"  />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="pedidoPadre"><g:message code="pedidoHijo.pedidoPadre.label" default="Pedido Padre" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: pedidoHijoInstance, field: 'pedidoPadre', 'errors')}">
                                    <g:select name="pedidoPadre.id" from="${ar.com.telecom.pcs.entities.Pedido.list()}" optionKey="id" value="${pedidoHijoInstance?.pedidoPadre?.id}"  />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="tipoImpacto"><g:message code="pedidoHijo.tipoImpacto.label" default="Tipo Impacto" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: pedidoHijoInstance, field: 'tipoImpacto', 'errors')}">
                                    <g:select name="tipoImpacto.id" from="${ar.com.telecom.pcs.entities.TipoImpacto.list()}" optionKey="id" value="${pedidoHijoInstance?.tipoImpacto?.id}"  />
                                </td>
                            </tr>
                        
                        </tbody>
                    </table>
                </div>
                <div class="buttons">
                    <span class="button"><g:submitButton name="create" class="save" value="${message(code: 'default.button.create.label', default: 'Create')}" /></span>
                </div>
            </g:form>
        </div>
    </body>
</html>
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            