<html>
<head>
  <meta http-equiv="Content-type" content="text/html; charset=UTF-8" />
  <title>NeedIT :: Administracion</title>

  <!--logo title-->
  <link rel="shortcut icon" href="${resource(dir:'images',file:'favicon.ico')}" type="image/x-icon" />

  <!--css-->
  <link rel="stylesheet" href="${resource(dir:'css',file:'main.css')}" />
  <link rel="stylesheet" href="${resource(dir:'css',file:'mainApp.css')}" />

  <!--js-->
  <g:javascript library="jquery-1.6.2" />
  <g:javascript library="jquery-ui-custom.min" />
  <g:javascript library="prototype" />
  <g:javascript library="common" />
  <g:javascript library="common-widget" />
  
  <g:layoutHead />
</head>
<body>
<img id="spinner" src="${resource(dir:'images',file:'ajax-spinner.gif')}" height="16px" width="16px" />

<div id="ContenedorGeneral">
 
  <div id="degradeFondo"></div>
  <div id="MenuHeader"></div>

  <div id="wrapper-body">
    <tmpl:/templates/menuSuperior />

    <div id="ContenedorPrincipal">
      <g:layoutBody />
    </div>
  </div>

  <div id="footer"><img src="${resource(dir:'images',file:'logoTelecom.png')}" /></div>
    
</div>

</body>
</html>