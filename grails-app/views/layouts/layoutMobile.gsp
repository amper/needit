<?xml version="1.0"?>
<!DOCTYPE html PUBLIC "-//WAPFORUM//DTD XHTML Mobile 1.0//EN"
 "http://www.wapforum.org/DTD/xhtml-mobile10.dtd">

<html>
<head>
  <meta http-equiv="Content-Type" content="application/xhtml+xml;charset=utf-8" />
  <title><g:message code="inbox.mobile.title" /></title>
  <link rel="shortcut icon" href="${resource(dir:'images',file:'LogoTelecom.jpg')}" type="image/x-icon" />
  <link rel="stylesheet" href="${resource(dir:'mobile',file:'mobile.css')}" />
  <g:javascript library="prototype" />
  <g:layoutHead />
</head>
<body>
<web:isBlackberry>
  <div id="mobile-body">
    <div id="mobile-body-content">
      <g:layoutBody />
    </div>
  </div>
</web:isBlackberry>
</body>
</html>
 