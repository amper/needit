<%@ page import="org.codehaus.groovy.grails.commons.ConfigurationHolder" %>
<html>
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
  <title>Bienvenido a Need It</title>
  <link rel="shortcut icon" href="${resource(dir:'images',file:'favicon.ico')}" type="image/x-icon" />
  <web:isBlackberry><link rel="stylesheet" href="${resource(dir:'mobile',file:'mobile.css')}" /></web:isBlackberry>
  <web:isNOTBlackberry><link rel="stylesheet" href="${resource(dir:'css',file:'login.css')}" /></web:isNOTBlackberry>
  <script type="text/javascript">
    function loginOnLoad(){
      var obj = document.getElementById('j_username');
      if (obj) {
      	obj.focus()
      }
    }

    function loginOnEnter(){
      var obj = document.getElementById('j_username');
      if (obj) {
    	  obj.value = obj.value.toLowerCase();
      }
    }
  </script>
  <g:layoutHead />
</head> 
<body onload="loginOnLoad();">
<web:isBlackberry>
  <div id="mobile-body">
    <div id="mobile-body-content">
      <div id="mobile-body-content-logo"><img src="${resource(dir:'mobile',file:'mobile-logo.png')}" /></div>
      <div id="mobile-body-content-form">
        <g:layoutBody />
      </div>
    </div>
  </div>
</web:isBlackberry>
<web:isNOTBlackberry>
  <div id="HeaderLogin">
    <table id="HeaderLogin-content">
      <tr>
        <td><div id="Logo"><img src="${resource(dir:'images/logos',file:ConfigurationHolder.config.imagen.needit)}" border="0"></div></td>
        <td><g:layoutBody /></td>
      </tr>
    </table>
  </div>
  <div id="FooterLogin">
    <table id="FooterLogin-content">
      <tr>
        <td class="FooterLogin-content-cellA"><g:link url="${ConfigurationHolder.config.needit.files.ayuda.url}" title="${message(code: 'index.login.ayuda.tooltip')}" target="_blank"></g:link><br />${message(code: 'index.login.ayuda.label')}</td>
        <td class="FooterLogin-content-cellB"><img src="${resource(dir:'images',file:'logo-teco-trans-white.png')}" width="95" height="63" border="0" /></td>
        <td class="FooterLogin-content-cellC"><a href="${ConfigurationHolder.config.needit.files.instructivo.url}" title="${message(code: 'index.login.perfiles.tooltip')}" target="_blank">&nbsp;</a><br />${message(code: 'index.login.perfiles.label')}</td>
      </tr>
    </table>
  </div>
</web:isNOTBlackberry>
</body>
</html>
