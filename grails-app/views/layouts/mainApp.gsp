<%@page import="org.codehaus.groovy.grails.web.servlet.mvc.SynchronizerToken"%>

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
<meta http-equiv="Content-type" content="text/html; charset=UTF-8" />
<title>NeedIT</title>

<!--logo title-->
<link rel="shortcut icon" href="${resource(dir:'images',file:'favicon.ico')}" type="image/x-icon" />

<!--css-->
<link rel="stylesheet" href="${resource(dir:'css/themes/custom-theme',file:'jquery-ui-1.8.16.custom.css')}" />
<link rel="stylesheet" href="${resource(dir:'css/themes/custom-theme',file:'jquery.multiselect.css')}" />
<link rel="stylesheet" href="${resource(dir:'css/themes/custom-theme',file:'jquery.multiselect.filter.css')}" />
<link rel="stylesheet" href="${resource(dir:'css',file:'mainApp.css')}" />

<!--js-->
<g:javascript library="jquery-1.6.2" />
<g:javascript library="jquery-ui-custom.min" />
<g:javascript library="jquery.multiselect" />
<g:javascript library="jquery.multiselect.filter" />
<g:javascript library="common" />
<g:javascript library="common-widget" />
<g:javascript library="prototype" />
<g:javascript library="error" />
<g:javascript library="synchronizerToken" />
<g:javascript library="submitSpinner" />
 
<g:javascript library="anexoPorTipo" />

<script type="text/javascript">
  var gNameApp = 'needIt';
  var gTokenKey = '${SynchronizerToken.KEY}';
  var gErrorURL = '<g:createLink controller="errorApp" action="index"/>';
  var gErrorAjaxURL = '<g:createLink controller="errorApp" action="index"/>';
</script>

<g:layoutHead />
</head>
<body>
<web:isNOTMsie>
  <div class="contenidoError">Herramienta no soportada por navegadores no corporativos</div>
</web:isNOTMsie>

<web:isMsie>
  <img id="spinner" src="${resource(dir:'images',file:'ajax-spinner.gif')}" height="16px" width="16px" />

  <div id="ContenedorGeneral">

    <div id="degradeFondo"></div>
    <div id="MenuHeader"></div>

    <div id="wrapper-body">
      <tmpl:/templates/menuSuperior />
      <!-- fin menu header -->

      <div id="ContenedorPrincipal">
        <g:layoutBody />
      </div>
    </div>

    <div id="footer">
      <img src="${resource(dir:'images',file:'logoTelecom.png')}" />
    </div>
  </div>
</web:isMsie>


<div id="dialog-ReasignarRol" title="Reasignar Rol" style="display:none;">
	
</div>
	
<div id="dialog-SuspenderPedido" title="Suspensi&oacute;n / Reanudaci&oacute;n de pedido" style="display:none;">
	
</div>

<g:hiddenField id="suspensionPop" name="suspensionPop" value="${params.muestraSuspensionPop}" />
<g:hiddenField name="controllerEnNavegacion" id="controllerEnNavegacion" value="${params.controller}"/>
<script type="text/javascript">
	jQuery(document).ready(
		function($) {
			$("#dialog-SuspenderPedido").dialog({
				autoOpen : false,
				resizable : false,
				height : "100%",
				width : 650,
				modal : true,
				closeOnEscape: false,
				open: function(event, ui) {
					  $(this).closest('.ui-dialog').find('.ui-dialog-titlebar-close').hide();
					}
			});

			if(document.getElementById('suspensionPop').value && document.getElementById('suspensionPop').value != 'false'){
				if(document.getElementById('btnSuspenderPedido') != null ){
					document.getElementById('btnSuspenderPedido').click();
				}else{
					document.getElementById('btnReanudarPedido').click();
				}
			}
		});
</script>

<script type="text/javascript">
	<g:if test="${muestraPopDireccion}">
		document.getElementById('muestraPopDireccion').value = true;
	 	document.getElementById('actionFromDireccion').value = '${actionFrom}';
		confirmarCambioDireccion();
	</g:if>
</script>


</body>
</html>