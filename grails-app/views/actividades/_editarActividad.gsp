<g:updateToken />
<script type="text/javascript">
	jQuery(document).ready(
		function($) {
			$("#tabs").tabs();
		});  
</script>
<g:form id="formActividad" name="formActividad" controller="construccionPadre" useToken="true">
		<div class="message" id="mensajes" style="display: none; "></div>
		<div class="errors" id="errores" style="display: none;"></div>
		<div class="formSeccion-content" style="width: 100%;">
				<div class="pCompletoPopUp pTitle" style="padding-bottom: 0px; margin-bottom: 2px; margin-left: 0px; margin-right: 0px; margin-top: 0px; width: 100%">
						<div class="pDosPopUpCorto" style="margin-bottom: 0px;">
								<label class="formLabel enRenglon">Grupo responsable</label> <span> ${impactoInstance?.pedido.grupoResponsable} </span>
						</div>

						<div class="pDosPopUpCorto" style="margin-bottom: 0px;">
								<label class="formLabel">Macroestado</label> <span> ${impactoInstance?.pedido.faseActual?.macroEstado?.descripcion} </span>
						</div>
						<div class="separator"></div>
						<g:if test="${impactoInstance?.pedido.tipoImpacto.asociadoSistemas}">
								<div class="pDosPopUpCorto" style="margin-bottom: 0px;">
										<label class="formLabel enRenglon">Sistema</label> <span> ${impactoInstance?.pedido.sistema?.descripcion} </span>
								</div>
						</g:if>
						<g:else>
								<div class="pDosPopUpCorto" style="margin-bottom: 0px;">
										<label class="formLabel enRenglon">&Aacute;rea de soporte</label> <span> ${impactoInstance?.pedido.areaSoporte?.descripcion} </span>
								</div>
						</g:else>

						<div class="pDosPopUpCorto" style="margin-bottom: 0px;">
								<label class="formLabel enRenglon">Fase:</label> <span> ${impactoInstance?.pedido.faseActual?.descripcion} </span>
						</div>
				</div>
				<g:render template="/actividades/tabEdicion" model="['pedidoHijo':impactoInstance?.pedido, 'actividad':actividad, 'tiposDeActividad':tiposDeActividad, 'responsablesActividad':responsablesActividad, 'simular':simular,'usuarioLogueado':usuarioLogueado, 'impactoInstance': impactoInstance, 'origenActividad': origenActividad, 'releases': releases, 'personsAutocomplete':personsAutocomplete]"></g:render>

				<div class="separator"></div>

		</div>
		<g:hiddenField id="impacto" name="idPedidoHijo" value="${impactoInstance?.pedido?.id}" />
		<g:hiddenField id="idActividad" name="idActividad" value="${actividad?.id}" />
</g:form>

