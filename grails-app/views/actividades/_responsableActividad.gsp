<g:updateToken pedido="${actividad?.pedido?.parent}" pedidoHijo="${actividad?.pedido}"/>
<%@page import="ar.com.telecom.pcs.entities.RolAplicacion"%>

<!-- Lo parseamos porque viene como texto -->
<% Boolean readOnly = new Boolean(params.show) %>
<label class="formLabel enRenglon validate">Asignatario
	<g:if test="${!readOnly}">
		<g:if test="${actividad.puedeReasignarResponsable(usuarioLogueado)}">
			<g:secureRemoteLink title="Eliminar" class="limpiarBox limpiarBoxSinMargin" update="legajoUsuarioResponsablePop" id="${RolAplicacion.RESPONSABLE_ACTIVIDAD}" params="['username': '', 'box':'legajoUsuarioResponsablePop', 'grupoResponsable':actividad.grupoResponsable, 'origenActividad': origenActividad, 'actividadId':actividad?.id]" action="completarUsuario"></g:secureRemoteLink>
		</g:if>
	</g:if> 
</label>
<div id="legajoUsuarioResponsablePop" class="boxUser" style="width: 275px;">
	<g:render template="/templates/userBox" model="['person':actividad?.legajoUsuarioResponsable, 'grupoResponsable': actividad?.grupoResponsable, 'box':'legajoUsuarioResponsablePop', 'personsAutocomplete': personsAutocomplete, 'usuarioLogueado': usuarioLogueado, 'pedidoHijo': pedidoHijo, 'actividad':actividad, 'origenActividad':origenActividad, 'cambiaResponsable':cambiaResponsable, 'cambiaGrupo':cambiaGrupo]"></g:render>
</div>
<g:hiddenField name="responsableActividadTemporal" value="${actividad?.legajoUsuarioResponsable }"/>
 