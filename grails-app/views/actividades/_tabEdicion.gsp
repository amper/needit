<g:updateToken />
<div class="demo">
	<div id="tabs" class="demoTabs" style="height: 80px;">
		<ul>
			<li><a href="#tabs-1">Descripci&oacute;n</a></li>
			<g:if test="${actividad.estaFinalizada()}">
				<li><a href="#tabs-2">Informaci&oacute;n cierre</a></li>
			</g:if>
		</ul>
		
			<div id="tabs-1">
				<g:render template="/actividades/solapaDescripcion" model="['pedidoHijo':impactoInstance?.pedido, 'actividad':actividad, 'tiposDeActividad':tiposDeActividad, 'responsablesActividad':responsablesActividad, 'show':params.show, 'simular':simular,'usuarioLogueado':usuarioLogueado, 'impactoInstance':impactoInstance, 'origenActividad': origenActividad, 'releases': releases, 'personsAutocomplete':personsAutocomplete]"></g:render>
			</div>
			<g:if test="${actividad.estaFinalizada()}">
				<div id="tabs-2">
					<g:render template="/actividades/solapaInformacionCierre" model="['pedidoHijo':impactoInstance?.pedido,'actividad':actividad, 'responsablesActividad':responsablesActividad, 'impactoInstance':impactoInstance]"></g:render>
				</div>
			</g:if>
	</div>
</div>

 