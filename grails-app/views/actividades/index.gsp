<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd"> 	
<%@page import="ar.com.telecom.util.NumberUtil"%>
<%@page import="ar.com.telecom.util.DateUtil"%>
<%@page import="ar.com.telecom.FaseMultiton"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta name="layout" content="mainApp" />

<script type="text/javascript">
	var controllerActual = '${params.controller}';
</script>

<g:javascript library="impacto" />
<g:javascript library="boxUser" />
<g:javascript library="animatedcollapse" />
<g:javascript library="actividades" />

</head>
<body>
	<div class="contenedorTitulo">
		<g:titulo pedido="${impactoInstance?impactoInstance?.pedido:pedidoInstance}" />
		<g:if test="${impactoInstance?.pedido?.id}">
			<g:workflow pedidoId="${impactoInstance?.pedido?.id}"/>
		</g:if>
		<g:else>
			<g:workflow pedidoId="${pedidoInstance?.id}"/>
		</g:else>
	</div>
 
		<div id="contenedorIzq">
				<g:if test="${flash.message}">
					<div class="message">
							${flash.message}
					</div>
				</g:if>
				<g:hasErrors bean="${impactoInstance?.pedido}">
						<div class="errors">
							<g:renderErrors bean="${impactoInstance ? impactoInstance?.pedido :pedidoInstance}" as="list" />
						</div>
				</g:hasErrors>
				
				<g:form class="formGeneral" id="formConstruccionPadre" name="formConstruccionPadre" action="grabarFormulario" useToken="true" >
					<div class="seccionHide">
						<g:cabeceraPedido pedido="${impactoInstance?.pedido?.parent?:pedidoInstance}" />
						<g:render template="/templates/workflowHidden"/>
						<g:actionSubmit id="refreshView" controller="construccionPadre" action="index" style="display:none" value="Refrescar vista" />
						<g:hiddenField name="hForm" id="hForm" value="formConstruccionPadre"/>
						<g:hiddenField name="hVengoPorAnexos" value="${params.hVengoPorAnexos}" />
						<g:hiddenField name="anexoFrom" id="anexoFrom" value=""/>
						
						<p class="tituloSeccion">${FaseMultiton.getFase(faseATrabajar)?.descripcion}</p>
			
						<div class="formSeccion">
						
							<div class="formSeccion-content">
								<div>
									<label class='formLabel enRenglon'>Pedido hijo:</label>
									<g:select id="impacto" name="impacto"
										from="${impactoInstance?.pedido?.parent?.pedidosHijosActivos}" optionKey="id"
										value="${impactoInstance?.pedido?.id}" 
										noSelection="['C': 'Seleccione un pedido hijo']" />
										<g:if test="${impactoInstance?.pedido?.cumplioFecha(FaseMultiton.getFase(faseATrabajar))}">
                                          <span title="Cumplido el ${DateUtil.toString(impactoInstance?.pedido?.getFecha(FaseMultiton.getFase(faseATrabajar)), true)}" id="pedido_estado_cumplido"></span>
										</g:if>
										<g:else>
                                          <span title="Pendiente" id="pedido_estado_pendiente"></span>
										</g:else>
								</div>

								<div class="separator"></div>

								<g:hiddenField id="faseATrabajar" name="faseATrabajar" value="${faseATrabajar}" />
								<script type="text/javascript">
									var g_PedidoFaseActual = "${impactoInstance?.pedido?.faseActual?.codigoFase}";
									var g_PedidoFaseVisualizacion = document.getElementById('faseATrabajar').value; 
								</script>

								<div id="datosHijoSeleccionado">
									
								</div>
								
							</div>

						</div>

						<div id="botonera">
						</div>
					
						<div id="message"></div>
					</div>
					<script type="text/javascript">
						var obj ='${impactoInstance?.id}';
						if(obj!=''){
							seleccionaHijo(obj, document.getElementById('botonera'));
						}
					</script>
					<div id="token"><g:render template="/templates/token"></g:render></div>
				</g:form>
		</div>
		<!-- fin contenedor izq -->
		
		<!-- Lateral Derecho -->
		<g:render template="/templates/menuLateral" model="['pedidoInstance': impactoInstance?.pedido?.parent?:pedidoInstance]"/>
		<!-- Fin Lateral Derecho -->
	
			<!-- 
		************
		************
		MODALS POPUP
		************
		************
		************
		-->
		<!-- popup ayuda seccion -->
		<div id="dialog-help" title="Ayuda Aprobar Pedido" style="display: none;">
				<div style="height: 350px; width: 100%; overflow: auto;">
						<p class="helpTitle">Seccion ayuda</p>
						<p>Hola esto es una seccion de ayuda Hola esto es una seccion de</p>
				</div>
		</div>
		<!-- fin popup ayuda seccion -->
		
		<!-- Popup editar actividad -->
		<div id="dialog-EditarActividad" title="Editar actividad" style="display: none;">
		</div>
		<!-- Fin popup editar actividad -->
		
		<div id="dialog-popupAnexoPorTipoActividad" title="Nuevo anexo para la actividad" style="display: none;">
		</div>
		
		<!-- Popup finalizar actividad -->
		<div id="dialog-FinalizarActividad" title="Finalizar actividad" style="display: none;">
		</div>
		<!-- Fin popup finalizar actividad -->
		
		<script>
			if ($('hVengoPorAnexos') && $('hVengoPorAnexos').value != ''){
				showPopUpActividad('${params.impacto}','${actividad?.id}',${!actividad?.estaFinalizada()}, '${false}');
				$('hVengoPorAnexos').value = '';
			}
		</script>
</body>
</html>
