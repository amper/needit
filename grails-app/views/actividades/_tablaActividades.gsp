<%@page import="java.beans.java_awt_BorderLayout_PersistenceDelegate"%>
<%@page import="ar.com.telecom.FaseMultiton"%>
<%@page import="ar.com.telecom.util.DateUtil"%>

<script type="text/javascript">
jQuery(document).ready(
		function($) {
			$("#impactaBaseArquitectura").combobox();
			$("#requiereManualUsuario").combobox();
			$("#actualizaCarpetaOperativa").combobox();
		});
</script>

<g:updateToken pedido="${impactoInstance?.pedido?.parent}" pedidoHijo="${impactoInstance?.pedido}"/>

<g:if test="${impactoInstance?.pedido?.tieneDisenioInterno(FaseMultiton.getFase(FaseMultiton.CONSTRUCCION_CAMBIO_HIJO)) && faseATrabajar.equals(FaseMultiton.CONSTRUCCION_CAMBIO_HIJO)}">
	<div id="divDisenioInterno">
	<div style="margin: 0px auto;">
	<g:if test="${esElResponsable && (!impactoInstance?.pedido?.cumplioFecha(FaseMultiton.getFase(FaseMultiton.CONSTRUCCION_CAMBIO_HIJO)) && impactoInstance?.pedido?.legajoUsuarioResponsable?.equalsIgnoreCase(usuarioLogueado))}">
		<div class="pTres">
			<label class="formLabel enRenglon" title="El cambio aplicativo debe reflejarse en la informaci&oacute;n de la Base de Conocimientos de Arquitectura (BCA)">Impacta en BCA</label>
			<g:select name="impactaBaseArquitectura" from="${opciones}" optionValue="key" optionKey="value" value="${impactoInstance?.pedido?.impactaBaseArquitectura}" noSelection="['null': 'Seleccionar...']" />
		</div>
		<div  class="pTres">
			<label class="formLabel enRenglon" title="Debe reflejarse informaci&oacute;n del presente Cambio en el Manual de Usuario de la Aplicaci&oacute;n impactada">Requiere Manual de Usuario</label>
			<g:select name="requiereManualUsuario" from="${opciones}" optionValue="key" optionKey="value"  value="${impactoInstance?.pedido?.requiereManualUsuario}" noSelection="['null': 'Seleccionar...']" />
		</div>
		<div  class="pTres">
			<label class="formLabel enRenglon" title="Debe reflejarse informaci&oacute;n del presente Cambio en la Carpeta Operativa (CO) de la Aplicaci&oacute;n impactada">Carpeta Operativa</label>
			<g:select name="actualizaCarpetaOperativa" from="${opciones}" optionValue="key" optionKey="value"  value="${impactoInstance?.pedido?.actualizaCarpetaOperativa}" noSelection="['null': 'Seleccionar...']" />
		</div>
	</g:if>
	<g:else>
		<div style="width: 28%; vertical-align: middle; float: left;">
			<label title="El cambio aplicativo debe reflejarse en la informaci&oacute;n de la Base de Conocimientos de Arquitectura (BCA)">Impacta en BCA:</label>&nbsp;<g:checkBoxStatusImage estado="${impactoInstance?.pedido?.impactaBaseArquitectura}" />
		</div>
		<div style="width: 40%; vertical-align: middle; float: left;">
			<label title="Debe reflejarse informaci&oacute;n del presente Cambio en el Manual de Usuario de la Aplicaci&oacute;n impactada">Requiere Manual de Usuario:</label>&nbsp;<g:checkBoxStatusImage estado="${impactoInstance?.pedido?.requiereManualUsuario}" />
		</div>
		<div style="width: 28%; vertical-align: middle; float: left;">
			<label title="Debe reflejarse informaci&oacute;n del presente Cambio en la Carpeta Operativa (CO) de la Aplicaci&oacute;n impactada">Carpeta Operativa:</label>&nbsp;<g:checkBoxStatusImage estado="${impactoInstance?.pedido?.actualizaCarpetaOperativa}" />
		</div>
	</g:else>
	</div>
	<div class="separatorGrande"></div>
	</div>
</g:if>

 
<g:if test="${impactoInstance?.pedido.getActividadesDeFase(faseATrabajar)}">
	 <table>
		<tr>
			<th title=" "> </th>
			<th title="Tipo de actividad" >Tipo de actividad</th>
			<th title="Grupo asignatario">Grupo asignatario</th>
			<th title="Asignatario" style="width:150px;">Asignatario</th>
<%--			<g:sortableColumn property="fechaInicio" title="Fecha inicio" />--%>
			<th title="Fecha fin">Fecha fin</th>
			<th title="Codigo Cierre" style="">C&oacute;digo Cierre</th>
			<g:if test="${impactoInstance?.pedido.mostrarCHPasajes()}"><th class="encabezado5">CH Pasajes</th></g:if>
			<th title="Observaciones" style="">Observaciones</th>
			<th title=" " class="columnaBoton"> </th>
			
			<g:if test="${esElResponsable}">
				<th title=" " class="columnaBoton"></th>
				<th title=" " class="columnaBoton"></th>
				<th title=" " class="columnaBoton"></th>
			</g:if>
						
		</tr>
	
		<g:each in="${impactoInstance?.pedido.getActividadesDeFase(faseATrabajar)}" var="actividad"  status="counter">
			<tr class="${(counter % 2) == 0 ? 'odd' : 'even'}">
				<td>${counter + 1}</td>
				<td>${actividad?.descripcion}</td>
				<td>${actividad.grupoResponsable.replace("_","_<br>")}</td> <!-- quieren un enter entre "_" -->
				<td style="width:150px;"> <g:userDescription legajo="${actividad.legajoUsuarioResponsable}" pedido="${counter}"/></td>
<%--				<td>${DateUtil.toString(actividad.fechaCreacion)}</td>--%>
				<td>${DateUtil.toString(actividad.fechaCierre)}</td>
				<td>${actividad.codigoCierre}</td>
				<g:if test="${impactoInstance?.pedido.mostrarCHPasajes()}">
					<td>${actividad?.numeroTicket}</td>
				</g:if>
				<td>${actividad.comentariosCierre}</td>
				<td><a href="javascript:showPopUpActividad('${impactoInstance?.pedido.id}','${actividad.id}',false, true);" class="visualizar sinMargenIzquierdo" title="Visualizar actividad"></a></td>

				<g:if test="${!actividad?.estaFinalizada()}">
					<g:if test="${actividad.puedeEditarActividad(usuarioLogueado) || actividad.puedeReasignarResponsable(usuarioLogueado)}">
						<td class="columnaBoton"><a href="javascript:showPopUpActividad('${impactoInstance?.pedido?.id}','${actividad.id}', ${true}, false);" class="modificar sinMargenIzquierdo" title="Modificar actividad"></a></td>
					</g:if>
					<g:else>
						<td class="columnaBoton"></td>
					</g:else>
					
					<g:if test="${actividad.esElAsignatario(usuarioLogueado)}">
						<td class="columnaBoton"><a href="javascript:showPopUpFinalizarActividad('${impactoInstance?.pedido.id}', '${actividad.id}',false, 'btnFinalizarGrilla');" class="finalizar sinMargenIzquierdo" title="Finalizar actividad"></a></td>
					</g:if>
					<g:else>
						<td class="columnaBoton"></td>
					</g:else>
					<g:if test="${actividad.esElAsignatario(usuarioLogueado)}">
						<td class="columnaBoton">
						<a href="javascript:showPopUpFinalizarActividad('${impactoInstance?.pedido.id}', '${actividad?.id}',false, 'btnCancelaGrilla');" class="eliminar sinMargenIzquierdo" title="Eliminar actividad"></a></td>
					</g:if>
					<g:else>
						<td class="columnaBoton"></td>
					</g:else>
				</g:if>
				<g:else>
					<td class="columnaBoton"></td>
					<td class="columnaBoton"></td>
					<td class="columnaBoton"></td>
				</g:else>

			</tr>
		</g:each>
	</table>

</g:if>

<div class="separator"></div>
<g:if test="${esElResponsable && estaEnFase && usuarioLogueado.equals(impactoInstance?.pedido?.legajoUsuarioResponsable) && !impactoInstance?.pedido?.estaSuspendido()}"> 
	<p align="center">
		<button onClick="javascript:showPopUpActividad('${impactoInstance?.pedido.id}',null,${true}, false);" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only agregar_actividad" title="Agregar actividad" id="btnAgregarActividad">Agregar actividad</button>
		<script type="text/javascript">
			if ( g_PedidoFaseActual == g_PedidoFaseVisualizacion){
				$('btnAgregarActividad').show();
			} else {
				$('btnAgregarActividad').hide();
			}
		</script>
	</p>
</g:if>
