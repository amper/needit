<%@page import="ar.com.telecom.FaseMultiton"%>
<%@page import="ar.com.telecom.util.DateUtil"%>

<g:updateToken pedido="${impactoInstance?.pedido?.parent}" pedidoHijo="${impactoInstance?.pedido}"/>

<g:if test="${impactoInstance?.pedido}">
		<script type="text/javascript">
			g_PedidoFaseActual = "${impactoInstance?.pedido?.faseActual?.codigoFase}";
			g_PedidoFaseVisualizacion = document.getElementById('faseATrabajar').value;
		</script>
		<div class="pDos">
			<div>
				<label class="formLabel">Fase actual</label>
				<div class="info infoNoMargin">${impactoInstance?.pedido?.faseActual}</div>
			</div>
			<div>
				<label class="formLabel">Grupo referente</label>
				<div class="info infoNoMargin">${impactoInstance?.pedido?.grupoResponsable}</div>
			</div>
			
			<g:if test="${impactoInstance?.pedido?.cumplioFecha(FaseMultiton.getFase(faseATrabajar))}">
				<div>
					<div class="info infoNoMargin"><label class="formLabel">La fase ${FaseMultiton.getFase(faseATrabajar)} fue cumplida el ${DateUtil.toString(impactoInstance?.pedido?.getFecha(FaseMultiton.getFase(faseATrabajar)))}</label></div>
				</div>
			</g:if>
		</div>
		  
		<div class="pDos">
		
			<label class="formLabel2" for='legajoUsuarioResponsable'>Asignatario</label>
			<div id="legajoResponsableConstruccionPadre" class="boxUser">
				<g:render template="/templates/userBox"
					model="['person':impactoInstance?.pedido?.legajoUsuarioResponsable, 'box':'legajoResponsableConstruccionPadre', 'pedidoHijo':impactoInstance.pedido,'personsAutocomplete': personsAutocomplete, 'grupoResponsable':pedidoHijo?.grupoResponsable]"></g:render>
			</div>
			<g:if test="${estaEnFase && esElResponsable && tienePermisos}">
				<g:secureRemoteLink title="Eliminar" class="limpiarBox"
					update="legajoResponsableConstruccionPadre"
					id="responsable" paramsJavascript="['aprobacionId': 'document.getElementById(\'aprobacionId\').value']"
					params="['username': '', 'box':'legajoResponsableConstruccionPadre', 'grupoResponsable':impactoInstance?.pedido?.grupoResponsable, 'pedidoId':impactoInstance?.pedido?.parent?.id, 'impacto':impactoInstance?.pedido.id, 'aprobacionId':aprobacion?.id]"
					action="completarUsuario">
				</g:secureRemoteLink>
			</g:if>
		</div>
	
	
	<div class="separator"></div>
	<div id="actividades">
		<g:render template="/actividades/tablaActividades" model="['impactoInstance':impactoInstance, 'usuarioLogueado':usuarioLogueado,'esElResponsable':esElResponsable, 'faseATrabajar':faseATrabajar, 'estaEnFase':estaEnFase, 'opciones':opciones, 'listaUsuariosGrupo':personsAutocomplete]"></g:render>
	</div>
</g:if>