<%@page import="ar.com.telecom.util.DateUtil"%>

<g:updateToken />

<div class="pDosPopUpCorto" style="margin-left: 0px;">
	<div class="pCompletoPopUp">
		<label class="formLabel">Fecha cierre:</label>
		<p class="info">${DateUtil.toString(actividad.fechaCierre)}</p>
	</div>
	<div class="pCompletoPopUp">
		<label class="formLabel">C&oacute;digo cierre:</label>
		<p class="info">${actividad.codigoCierre.descripcion}</p>
	</div>
</div>

<div class="pDosPopUpCorto" style="margin-left: 0px;">
	<label class="formLabel">Usuario:</label>
	<div id="legajoUsuarioResponsable" class="boxUser">
		<g:render template="/templates/userBox"
			model="['person':actividad?.legajoUsuarioCierre, 'box':'legajoUsuarioResponsable']"></g:render>
	</div>
</div>

<div class="separator"></div>

<div class="pCompletoPopUp">
	<label class="formLabel enRenglon">Comentarios:</label>
	<p class="info">${actividad.comentariosCierre}</p>
</div>

 
