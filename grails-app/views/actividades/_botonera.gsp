<g:updateToken />
<g:if test="${estaEnFase}">
	<center>
		<g:if test="${esElResponsable}">
			<div class="formSeccionButton" style="margin-top: 20px;">
				<g:if test="${!impactoInstance.pedido.estaSuspendido()}">		
					<g:actionSubmit class="formSeccionSubmitButton" id="Simular"
						action="simularValidacion" value="Simular" />
					<g:actionSubmit class="formSeccionSubmitButton" id="Guardar"
						action="grabarFormulario" value="Guardar" />
					<g:actionSubmit class="formSeccionSubmitButton" id="Aprobar"
						action="aprobarPedido" value="Finalizar fase" />
				</g:if>
				<g:else>
					<g:actionSubmit class="formSeccionSubmitButton" id="Simular"
						action="simularValidacion" value="Simular" />
					<g:actionSubmit class="formSeccionSubmitButton" id="Guardar"
						action="grabarFormulario" value="Guardar" />
				</g:else>			
			</div>
		</g:if>
	</center>
</g:if>	
   