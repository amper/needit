<%@page import="ar.com.telecom.pcs.entities.Release"%>
<%@page import="ar.com.telecom.pcs.entities.Versionador"%>
<%@page import="ar.com.telecom.util.DateUtil"%>

<g:updateToken />

<script type="text/javascript">
	jQuery(document).ready(
		function() {
			var idAct = document.getElementById("idActividad").value; 

			jQuery('.tituloSeccionGrupo').click(function() {
				jQuery(this).next().slideToggle('fast');
			});

			var grupoAsignatario;
			if (document.getElementById('comboGrupoAsignatario')) {
				grupoAsignatario = document.getElementById('comboGrupoAsignatario').value;
			} else {
				grupoAsignatario = document.getElementById('grupoResponsable').value;
			}
			var tipoActividad;
		    if (document.getElementById('comboTipoActividad')) {
				tipoActividad = document.getElementById('comboTipoActividad').value;
			} else {
				tipoActividad = document.getElementById('tipoActividad').value;
			}
			initFormularioCamposActividades(grupoAsignatario, tipoActividad);
			initPopUpEditarActividadForm();

		});
</script>
 
<div class="separator"></div>
<div class="conMargenChico conBordeChico" style="width: 100%;">
	<p class="tituloSeccionGrupo">Grupo / Usuario</p>
	<div class="formSeccion" style="margin-top: 2px;">
		<div class="pDosPopUp">
			<label class="formLabel enRenglon validate">Grupo responsable</label>
			<br>
			<g:if test="${actividad.puedeEditarGrupo(usuarioLogueado) && !show.toBoolean()}">
				<g:select id="comboGrupoAsignatario" name="grupoResponsable"
				from="${impactoInstance?.pedido?.gruposAsignatariosActividad}"
				value="${actividad.grupoResponsable}"
				noSelection="['null': '']" />
			</g:if>
			<g:else>
				<p class="info">${actividad.grupoResponsable}</p>
				<g:hiddenField id="grupoResponsable" name="grupoResponsable" value="${actividad.grupoResponsable}"/>
			</g:else>
		</div>
		<div class="pDosPopUp sinMargenIzquierdo">
			<div id="responsableActividad">
				<g:render template="/actividades/responsableActividad" model="[
				 'actividad':actividad, 
				 'usuarioLogueado': usuarioLogueado,
				 'responsablesActividad':responsablesActividad, 
				 'show':params.show as boolean, 'personsAutocomplete':personsAutocomplete, 'origenActividad':origenActividad]"></g:render>
			</div>
		</div>
	</div>
</div>
<div class="separator"></div>

<div id="formularioCamposActividades"></div>