<g:updateToken pedido="${actividad?.pedido?.parent}" pedidoHijo="${actividad?.pedido}"/>
<div id="anexoDiv${anexoFrom}" style="width: 100%;">
		<g:if test="${listAnexos}">
				<table width="100%"> 
						<tr>
								<g:sortableColumn property="archivo" title="Archivo" />
								<g:sortableColumn property="tipoAnexo" title="Tipo de anexo" />
								<g:sortableColumn property="descripcion" title="Descripci&oacute;n" />
								<g:if test="${actividad.puedeEditarActividad(usuarioLogueado) && !show.toBoolean()}">
										<g:sortableColumn property="Eliminar" title=" " />
								</g:if>
						</tr>
						<g:each in="${listAnexos}" var="arch">
								<tr>
										<td><g:link action="verAnexo" target="_blank" params="[anexoId: arch.id]">
														${arch}
												</g:link>
										</td>
										<td>
												${arch?.tipoAnexo}
										</td>
										<td>
												${arch?.descripcion}
										</td>
										<g:if test="${actividad.puedeEditarActividad(usuarioLogueado) && !show.toBoolean()}">
											<td>
												<g:secureRemoteLink action="eliminarAnexoActividad" params="[anexoId: arch.id, idActividad: actividad?.id, 'impacto': impacto, 'anexoFrom': anexoFrom]" update="anexoDiv${anexoFrom}">
													<img src="${resource(dir:'images',file:'delete.gif')}" width="10" height="10" />
												</g:secureRemoteLink>
											</td>
										</g:if>
								</tr>
						</g:each>
				</table>
		</g:if>
</div>
