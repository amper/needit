<g:updateToken pedido="${actividad?.pedido?.parent}" pedidoHijo="${actividad?.pedido}"/>

<div align="center">
		<div class="formSeccionButton" style="margin-top: 20px;">
				<button id="btnCancelarEditarActividad" class="formSeccionSubmitButtonAnexo" value="Cancelar" name="Cancelar" onClick="cerrarPopUpEditarActividad();" style="margin-right: 0px;">Cerrar</button>
				<%boolean show = params?.show?.toBoolean() %>
				<g:if test="${!show}">
						<g:if test="${actividad.puedeEditarActividad(usuarioLogueado)}">
								<g:submitToRemoteSecure controller="construccionPadre" action="simularActividad" value="Simular" update="formularioCamposActividades" class="formSeccionSubmitButtonAnexo" />

								<g:if test="${actividad?.id}">
									<button onClick="botonActividad(Form.serialize(this.form), '${params?.impacto}', '${actividad?.id}', 'btnRechazar')" class="formSeccionSubmitButtonAnexo" title="Rechazar actividad" style="margin-left: 2px; margin-right: 2px;">Rechazar</button>
									<button onClick="botonActividad(Form.serialize(this.form), '${params?.impacto}', '${actividad?.id}', 'btnFinalizarGrilla')" class="formSeccionSubmitButtonAnexo" style="margin: 0px;" title="Finalizar actividad">Finalizar</button>
								</g:if>
								<g:else>
									<g:submitToRemoteSecure controller="construccionPadre" action="grabaActividad" value="Aceptar y Nueva" onComplete="showPopUpActividad('${params?.impacto}',null,${true}, false);" id="aceptarynuevaActividad" update="dialog-EditarActividad" class="formSeccionSubmitButtonAnexo formSeccionSubmitButtonAnexoAncho" />
									<g:submitToRemoteSecure controller="construccionPadre" action="grabaActividad" value="Aceptar" onComplete="recargaTablaActividades('${params?.impacto}');cerrarPopUpEditarActividad();" id="aceptar" update="dialog-EditarActividad" class="formSeccionSubmitButtonAnexo" />
								</g:else>
						</g:if>
						<g:if test="${!actividad.id || actividad.puedeGrabar(usuarioLogueado)}">
							<g:submitToRemoteSecure name="buttonGrabaActividad" controller="construccionPadre" action="grabaActividad" value="Grabar" onLoading="showSpinner('spinner', true);" onComplete="recargaTablaActividades('${params?.impacto}');" update="dialog-EditarActividad" class="formSeccionSubmitButtonAnexo" style="${actividad?.estaFinalizada() ? 'display: none;' : 'display: visible;'}" />
						</g:if>
				</g:if>
		</div>
</div> 