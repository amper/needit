<%@page import="ar.com.telecom.pcs.entities.CodigoCierre"%>

<g:updateToken pedido="${actividad?.pedido?.parent}" pedidoHijo="${actividad?.pedido}"/>

<script type="text/javascript">
	jQuery(document).ready(
		function($) {
			jQuery('#comboCodigoCierre').combobox();
		});
</script> 
  
<div class="message" id="mensajesFinalizarActividad" style="display: none; "></div>
<div class="errors" id="erroresFinalizarActividad" style="display: none;"></div>
 
<g:form id="formFinActividad" name="formFinActividad" controller="construccionPadre" useToken="true">
		<g:hiddenField name="idActividad" value="${actividad?.id}" />
		<g:hiddenField name="impacto" value="${impactoInstance?.pedido?.id}" />
		<g:hiddenField name="recargaEditarPop" value="${recargaEditarPop}" />
		<g:hiddenField name="botonOrigen" value="${botonOrigen}" />
		<g:hiddenField id="actividadId" name="actividadId" value="${actividad?.id}" />
		<g:hiddenField id="actividadVersion" name="actividadVersion" value="${actividad?.version}" />
		
		<g:hasErrors bean="${actividad}">		
			<script type="text/javascript">
				if ($('erroresFinalizarActividad')){
					$('erroresFinalizarActividad').innerHTML = "${renderErrors(bean: actividad)}";
					$('erroresFinalizarActividad').show();
					$('mensajesFinalizarActividad').hide();
				}
			</script>
		</g:hasErrors>
		<g:if test="${flash.message}">
			<script type="text/javascript">
				if ($('mensajesFinalizarActividad')){
					$('mensajesFinalizarActividad').innerHTML = "${flash.message}";
					$('mensajesFinalizarActividad').show();
					$('erroresFinalizarActividad').hide();
				}
			</script>
		</g:if>

		<div id="finalizarActividad">
				<div>
						<label class="formLabel">Actividad:</label> <span>
								${actividad?.tipoActividad?.descripcion}
						</span>
				</div>
				<div class="separator"></div>

				<div>
						<label class="formLabel">C&oacute;digo de cierre:</label>
						<g:if test="${!actividad?.estaFinalizada()}">
								<g:select id="comboCodigoCierre" name="codigoCierre.id" from="${codigosCierre}" value="${actividad?.codigoCierre?.id}" optionKey="id" />
						</g:if>
						<g:else>
								<span>
										${actividad?.codigoCierre}
								</span>
						</g:else>
				</div>

				<div class="separator"></div>

				<div>
						<label class="formLabel">Comentarios:</label>
						<g:if test="${!actividad?.estaFinalizada()}">
								<g:textArea id="comentariosCierre" name="comentariosCierre" rows="3" class="textoComentario"></g:textArea>
						</g:if>
						<g:else>
								<span>
										${actividad?.comentariosCierre}
								</span>
						</g:else>
				</div>

				<br>

				<div class="formSeccionButton">
						<button id="btnCancelarFinActividad" class="formSeccionSubmitButtonAnexo" name="Cancelar" onClick="cerrarPopUpFinalizaActividad();">Cancelar</button>

						<g:if test="${!actividad?.estaFinalizada()}">
								<g:if test="${recargaEditarPop=='true'}">
										<button onClick="botonFinalizarActividad(Form.serialize(this.form), '${impactoInstance?.pedido.id}', '${actividad?.id}', '${!actividad?.estaFinalizada()}')" class="formSeccionSubmitButtonAnexo" title="Aceptar">Aceptar</button>
								</g:if>
								<g:else>
										<g:submitToRemoteSecure action="finalizarActividad" value="Aceptar" update="dialog-FinalizarActividad" onComplete="recargaTablaActividades('${impactoInstance?.pedido.id}');" class="formSeccionSubmitButtonAnexo" />
								</g:else>
						</g:if>
						<g:else>
								<script>cerrarPopUpFinalizaActividad();</script>
						</g:else>
				</div>

		</div>
</g:form>