<%@page import="ar.com.telecom.pcs.entities.Release"%>
<%@page import="ar.com.telecom.pcs.entities.Versionador"%>
<%@page import="ar.com.telecom.util.DateUtil"%>

<g:updateToken pedido="${actividad?.pedido?.parent}" pedidoHijo="${actividad?.pedido}"/>

<script type="text/javascript">
	jQuery(document).ready(
		function() {
			var idAct = document.getElementById("idActividad").value; 
			// Si es modificacion, plancho valor en datePicker
			var edita = ${actividad.puedeEditarActividad(usuarioLogueado)};
			
			jQuery('.tituloSeccion').click(function() {
				jQuery(this).next().slideToggle('fast');
			});
				
			jQuery("#datepicker-fechaSugerida").datepicker({
				minDate : +1
			});
			jQuery( "#datepicker-fechaSugerida" ).datepicker( "option", "dateFormat", "dd/mm/yy");
			
			if(edita){
				if (document.getElementById("fechaSugeridaHidden")){
					jQuery('#datepicker-fechaSugerida').val(document.getElementById("fechaSugeridaHidden").value);
				}
			}

			jQuery("#comboVersionador").combobox();
			jQuery("#comboRelease").combobox();

			var obj = '${actividad?.comentarios}';
		});
</script>

<g:if test="${actividad.id}">
	<script>
		document.getElementById('idActividad').value = '${actividad?.id}';
	</script>
</g:if>
 
<g:hiddenField id="actividadId" name="actividadId" value="${actividad?.id}" />
<g:hiddenField id="actividadVersion" name="actividadVersion" value="${actividad?.version}" />

<g:hasErrors bean="${actividad}">		
	<script type="text/javascript">
		if ($('errores')){
			$('errores').innerHTML = "${renderErrors(bean: actividad)}";
			$('errores').show();
			$('mensajes').hide();
		}
	</script>
</g:hasErrors>
<g:if test="${flash.message}">
	<script type="text/javascript">
		if ($('mensajes')){
			$('mensajes').innerHTML = "${flash.message}";
			$('mensajes').show();
			$('errores').hide();
		}
	</script>
</g:if>
<g:if test="${tipoActividad?.muestraURL() }">
		<div class="pDosPopUp conMargenChico conBordeChico">
			<p class="tituloSeccion">URL</p>
			<div class="formSeccion">
				<div class="pCompletoPopUp conMargenChico">
					<label class="formLabel enRenglon">URL</label>
					<g:if test="${actividad.puedeEditarActividad(usuarioLogueado) && !show.toBoolean()}">
						<input id="url" name="url" type="text" value="${actividad?.url}"></input>
					</g:if>
					<g:else>
						<a target="_blank" href="${actividad?.url}" id="linkURL">${actividad?.url}</a>
					</g:else>		
				</div>
				<div class="separator"></div>
			</div>
		</div>
	</g:if>
	
	<g:if test="${tipoActividad?.muestraPasajes() || tipoActividad?.muestraRelease() }">
		<div class="pDosPopUp conMargenChico conBordeChico">
			<p class="tituloSeccion">Simplit</p>
			<div class="formSeccion">
				<g:if test="${tipoActividad?.muestraPasajes() }">
					<div class="pDosPopUp conMargenChico" style="width: 280px;">
						<label class="formLabel enRenglon">Nro de Ticket Pasaje</label>
						<g:if test="${actividad.puedeEditarActividad(usuarioLogueado) && !show.toBoolean()}">	
							<input id="numeroTicket" name="numeroTicket" maxlength="20" type="text" value="${actividad?.numeroTicket}"></input>
						</g:if>
						<g:else>
							<p class="info">${actividad?.numeroTicket}</p>
						</g:else>		
					</div>
				</g:if>
				<g:if test="${tipoActividad?.muestraRelease() }">
					<div class="pDosPopUp conMargenChico" style="width: 280px;">
						<label class="formLabel enRenglon">Release</label>
						<g:if test="${actividad.puedeEditarActividad(usuarioLogueado) && !show.toBoolean()}">	
							<g:select id="comboRelease" name="release.id"
							from="${releases}" value="${actividad?.release?.id}"
							optionKey="id" noSelection="['null': '']" />
						</g:if>
						<g:else>
							<p class="info">${actividad.release}</p>
						</g:else>
					</div>
				</g:if>
			<div class="separator"></div>
			</div>
		</div>
	</g:if>
	
	<g:if test="${tipoActividad?.muestraAmbiente()}">
			<div class="pDosPopUp conMargenChico conBordeChico">
				<p class="tituloSeccion">Ambiente</p>
				<div class="formSeccion">
					<div class="pCompletoPopUp conMargenChico">
						<label class="formLabel enRenglon validate">Nombre servidor</label>
						<g:if test="${actividad.puedeEditarActividad(usuarioLogueado) && !show.toBoolean()}">	
							<input id="nombreServidor" name="nombreServidor" maxLength="30" type="text" value="${actividad?.nombreServidor}"></input>
						</g:if>
						<g:else>
							<p class="info conMargenChico">${actividad.nombreServidor}</p>
						</g:else>
					</div>
					
					<div class="pCompletoPopUp conMargenChico">
						<label class="formLabel enRenglon validate">Ambiente</label>
						<g:if test="${actividad.puedeEditarActividad(usuarioLogueado) && !show.toBoolean()}">	
							<input id="ambiente" name="ambiente" type="text" maxLength="30" value="${actividad?.ambiente}"></input>
						</g:if>
						<g:else>
							<p class="info conMargenChico">${actividad.ambiente}</p>
						</g:else>
					</div>
					<div class="separator"></div>
				</div>
			</div>
	</g:if>
	
	<g:if test="${(tipoActividad?.muestraVersionador())}">
		<div class="pDosPopUp conMargenChico conBordeChico">
			<p class="tituloSeccion">Versionador</p>
			<div class="formSeccion">
				<div class="pCompletoPopUp conMargenChico">
					<label class="formLabel enRenglon">Versionador</label>
					<g:if test="${actividad.puedeEditarActividad(usuarioLogueado) && !show.toBoolean()}">	
						<g:select id="comboVersionador" name="versionador.id"
						from="${Versionador.list()}"  value="${actividad?.versionador?.id}"
						optionKey="id" noSelection="['null': '']" />
					</g:if>			
					<g:else>
						<p class="info conMargenChico">${actividad.versionador}</p>
					</g:else>
				</div>
				
				<div class="pCompletoPopUp conMargenChico">
					<label class="formLabel enRenglon">Referencia</label>
					<g:if test="${actividad.puedeEditarActividad(usuarioLogueado) && !show.toBoolean()}">	
						<input id="numeroReferencia" name="numeroReferencia" maxLength="40" type="text" value="${actividad?.numeroReferencia}"></input>
					</g:if>
					<g:else>
						<p class="info conMargenChico">${actividad.numeroReferencia}</p>
					</g:else>
				</div>
				<div class="separator"></div>
			</div>
		</div>
	</g:if>

	<g:if test="${tipoActividad?.muestraMercury()}">
		<div class="pDosPopUp conMargenChico conBordeChico">
			<p class="tituloSeccion">Mercury</p>
			<div class="formSeccion">
				<div class="pCompletoPopUp conMargenChico">
					<label class="formLabel enRenglon">Dominio</label>
					<g:if test="${actividad.puedeEditarActividad(usuarioLogueado) && !show.toBoolean()}">	
						<input id="dominioMercury" name="dominioMercury" type="text" maxLength="40" value="${actividad?.dominioMercury}"></input>
					</g:if>
					<g:else>
						<p class="info conMargenChico">${actividad.dominioMercury}</p>
					</g:else>	
				</div>
				
				<div class="pCompletoPopUp conMargenChico">
					<label class="formLabel enRenglon">Proyecto</label>
					<g:if test="${actividad.puedeEditarActividad(usuarioLogueado) && !show.toBoolean()}">	
						<input id="proyectoMercury" name="proyectoMercury" type="text" maxLength="40" value="${actividad?.proyectoMercury}"></input>
					</g:if>
					<g:else>
						<p class="info conMargenChico">${actividad.proyectoMercury}</p>
					</g:else>	
				</div>
				<div class="separator"></div>
			</div>	
		</div>
	</g:if>
	
<div class="separator"></div>

<g:if test="${tipoActividad?.muestraFechaSugerida() || tipoActividad?.muestraComentarios()}">
	<div class="pCompletoPopUp">
		<g:if test="${tipoActividad?.muestraFechaSugerida()}">
			<div class="pDosPopUp" style="float: left; width: ${actividad.puedeEditarActividad(usuarioLogueado) && !show.toBoolean() ?'20%':'40%'}; margin-left: 0px;">
				<label class="formLabel enRenglon">Fecha fin sugerida</label>
				<g:if test="${actividad.puedeEditarActividad(usuarioLogueado) && !show.toBoolean()}">	
					<g:textField class="datePicker" name="fechaSugerida" id="datepicker-fechaSugerida" maxLength="10"/>
					<g:hiddenField id="fechaSugeridaHidden" name="fechaSugeridaHidden" value="${DateUtil.toString(actividad?.fechaSugerida)}" />
				</g:if>
				<g:else>
					<p class="info">${DateUtil.toString(actividad?.fechaSugerida)}</p>
				</g:else>
			</div>
		</g:if>
		<g:if test="${tipoActividad?.muestraComentarios()}">
			<div class="pCompletoPopUp sinMargenIzquierdo" style="float: left;">
				<g:if test="${actividad.puedeEditarActividad(usuarioLogueado) && !show.toBoolean()}">
					<label for="comentarioNuevo" class="formLabel enRenglon" style="margin-left: 15px;">Comentarios </label>
					<div style="margin-left: 15px;">
							<g:textArea id="comentarioNuevo" name="comentarioNuevo" value="${params.comentarioNuevo}"></g:textArea>
					</div>
				</g:if>
			</div>
		</g:if>
		<div class="separator"></div>
	</div>
</g:if>

<div class="separator"></div>

<g:if test="${actividad?.anexos || actividad.puedeEditarActividad(usuarioLogueado)}">
	<div class="conBordeChico" style="width: 100%; height: auto;">
	<p class="tituloSeccion">Anexos</p>
	<div class="formSeccion">
		<g:if test="${actividad.puedeEditarActividad(usuarioLogueado) && !show.toBoolean()}">
			<div style="float: left; margin: 10px;">
				<button onClick="botonNuevoAnexo(Form.serialize(this.form), '${impactoInstance?.pedido?.id}');" class="formSeccionSubmitButtonActividad" style="width: 125px" title="Nuevo Anexo" class="formSeccionSubmitButtonActividad">Nuevo Anexo</button>
			</div>	
		</g:if>
		<div style="width: ${(actividad.puedeEditarActividad(usuarioLogueado) && !show.toBoolean() )? '75%' : '100%'}; float: left;">
			<g:render template="/templates/tablaAnexos"
     			model="['fase': actividad?.fase?.codigoFase, 
     			'editable': actividad.puedeEditarActividad(usuarioLogueado) && !show.toBoolean(), 'pedidoInstance': actividad?.pedido?.parent, 'origen': actividad ]">
   			</g:render>
<%--			<g:render template="/actividades/anexoTabla" model="${['listAnexos': actividad?.anexos?.sort{it.id}, 'usuarioLogueado': usuarioLogueado, 'actividad': actividad, 'pedidoId': impactoInstance?.pedido.parent?.id, 'impacto': params.impacto, 'anexoFrom': impactoInstance?.pedido?.id, 'show': show]}"></g:render>--%>
		</div>
		<div class="separator"></div>
	</div>
	</div>
</g:if>
<div class="separator"></div>
<g:if test="${actividad?.comentarios}">
	<div class="conBordeChico" style="width: 100%; height: auto;">
		<p class="tituloSeccion">Historial</p>
		<div class="formSeccion">
			<div class="separator"></div>
			<div class="comentariosActividades" id="comentarios" style="width: 90%;">${actividad?.comentarios}</div>
			<div class="separator"></div>
		</div>
	</div>
</g:if>
<div>
	<g:render template="/actividades/botoneraActividad" model="['impactoInstance':impactoInstance, 'actividad':actividad, 'usuarioLogueado':usuarioLogueado]"></g:render>
</div>