<g:updateToken />
<script type="text/javascript">
	jQuery(document).ready(
		function() {
			jQuery("#comboTipoActividad").combobox();
		});
</script>
<div class="separator-sinmargen"></div>
<div class="pCompletoPopUp" style="width: 90%;">
	<label class="formLabel validate" style="width: auto; text-align: right; margin-left: 60px; margin-top: 10px; float: left;">Tipo de actividad:</label>
		<g:if test="${actividad.puedeEditarActividad(usuarioLogueado) && !show.toBoolean()}">
			<g:select id="comboTipoActividad" name="tipoActividad.id"
			from="${tiposDeActividad}"
			value="${origenActividad}"
			optionKey="id" noSelection="['null': '']" />
		</g:if>
		<g:else>
			<span class="info" style="width: 300px; float: left; margin-left: 5px;">${actividad?.descripcion}</span>
			<g:hiddenField id="tipoActividad" name="tipoActividad.id" value="${origenActividad}"/>
		</g:else>
</div>

<g:hiddenField name="impacto" value="${params.impacto}"/>
<g:hiddenField id="show" name="show" value="${params.show}"/>

<div id="formularioSolapaDescripcion">	
	<g:if test="${actividad?.id || simular}">
		<g:render template="/actividades/formularioSolapaDescripcion"	model="['pedidoHijo':impactoInstance?.pedido, 'actividad':actividad, 'tiposDeActividad':tiposDeActividad, 'responsablesActividad':responsablesActividad, 'tipoActividad':actividad?.tipoActividad, 'usuarioLogueado':usuarioLogueado, 'impactoInstance': impactoInstance, 'releases': releases, 'show':show, 'personsAutocomplete':personsAutocomplete, 'origenActividad':origenActividad]"></g:render>
	</g:if>		
</div> 