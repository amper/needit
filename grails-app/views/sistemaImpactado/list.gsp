
<%@ page import="ar.com.telecom.pcs.entities.SistemaImpactado" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="layout" content="main" />
        <g:set var="entityName" value="${message(code: 'sistemaImpactado.label', default: 'SistemaImpactado')}" />
        <title><g:message code="default.list.label" args="[entityName]" /></title>
    </head>
    <body>
        <div class="nav">
            <span class="menuButton"><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></span>
            <span class="menuButton"><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></span>
        </div>
        <div class="body">
            <h1><g:message code="default.list.label" args="[entityName]" /></h1>
            <g:if test="${flash.message}">
            <div class="message">${flash.message}</div>
            </g:if>
            <div class="list">
                <table>
                    <thead>
                        <tr>
                        
                            <g:sortableColumn property="id" title="${message(code: 'sistemaImpactado.id.label', default: 'Id')}" />
                        
                            <g:sortableColumn property="grupoReferente" title="${message(code: 'sistemaImpactado.grupoReferente.label', default: 'Grupo Referente')}" />
                        
                            <g:sortableColumn property="usuarioResponsable" title="${message(code: 'sistemaImpactado.usuarioResponsable.label', default: 'Usuario Responsable')}" />
                        
                            <th><g:message code="sistemaImpactado.sistema.label" default="Sistema" /></th>
                        
                            <th><g:message code="sistemaImpactado.tipoImpacto.label" default="Tipo Impacto" /></th>
                        
                        </tr>
                    </thead>
                    <tbody>
                    <g:each in="${sistemaImpactadoInstanceList}" status="i" var="sistemaImpactadoInstance">
                        <tr class="${(i % 2) == 0 ? 'odd' : 'even'}">
                        
                            <td><g:link action="show" id="${sistemaImpactadoInstance.id}">${fieldValue(bean: sistemaImpactadoInstance, field: "id")}</g:link></td>
                        
                            <td>${fieldValue(bean: sistemaImpactadoInstance, field: "grupoReferente")}</td>
                        
                            <td>${fieldValue(bean: sistemaImpactadoInstance, field: "usuarioResponsable")}</td>
                        
                            <td>${fieldValue(bean: sistemaImpactadoInstance, field: "sistema")}</td>
                        
                            <td>${fieldValue(bean: sistemaImpactadoInstance, field: "tipoImpacto")}</td>
                        
                        </tr>
                    </g:each>
                    </tbody>
                </table>
            </div>
            <div class="paginateButtons">
                <g:paginate total="${sistemaImpactadoInstanceTotal}" />
            </div>
        </div>
    </body>
</html>
