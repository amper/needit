

<%@ page import="ar.com.telecom.pcs.entities.SistemaImpactado" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="layout" content="main" />
        <g:set var="entityName" value="${message(code: 'sistemaImpactado.label', default: 'SistemaImpactado')}" />
        <title><g:message code="default.create.label" args="[entityName]" /></title>
    </head>
    <body>
        <div class="nav">
            <span class="menuButton"><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></span>
            <span class="menuButton"><g:link class="list" action="list"><g:message code="default.list.label" args="[entityName]" /></g:link></span>
        </div>
        <div class="body">
            <h1><g:message code="default.create.label" args="[entityName]" /></h1>
            <g:if test="${flash.message}">
            <div class="message">${flash.message}</div>
            </g:if>
            <g:hasErrors bean="${sistemaImpactadoInstance}">
            <div class="errors">
                <g:renderErrors bean="${sistemaImpactadoInstance}" as="list" />
            </div>
            </g:hasErrors>
            <g:form action="save" >
                <div class="dialog">
                    <table>
                        <tbody>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="grupoReferente"><g:message code="sistemaImpactado.grupoReferente.label" default="Grupo Referente" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: sistemaImpactadoInstance, field: 'grupoReferente', 'errors')}">
                                    <g:textField name="grupoReferente" maxlength="100" value="${sistemaImpactadoInstance?.grupoReferente}" />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="usuarioResponsable"><g:message code="sistemaImpactado.usuarioResponsable.label" default="Usuario Responsable" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: sistemaImpactadoInstance, field: 'usuarioResponsable', 'errors')}">
                                    <g:textField name="usuarioResponsable" maxlength="20" value="${sistemaImpactadoInstance?.usuarioResponsable}" />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="sistema"><g:message code="sistemaImpactado.sistema.label" default="Sistema" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: sistemaImpactadoInstance, field: 'sistema', 'errors')}">
                                    <g:select name="sistema.id" from="${ar.com.telecom.pcs.entities.Sistema.list()}" optionKey="id" value="${sistemaImpactadoInstance?.sistema?.id}"  />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="tipoImpacto"><g:message code="sistemaImpactado.tipoImpacto.label" default="Tipo Impacto" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: sistemaImpactadoInstance, field: 'tipoImpacto', 'errors')}">
                                    <g:select name="tipoImpacto.id" from="${ar.com.telecom.pcs.entities.TipoImpacto.list()}" optionKey="id" value="${sistemaImpactadoInstance?.tipoImpacto?.id}"  />
                                </td>
                            </tr>
                        
                        </tbody>
                    </table>
                </div>
                <div class="buttons">
                    <span class="button"><g:submitButton name="create" class="save" value="${message(code: 'default.button.create.label', default: 'Create')}" /></span>
                </div>
            </g:form>
        </div>
    </body>
</html>
