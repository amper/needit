
<%@ page import="ar.com.telecom.pcs.entities.CargaHora" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="layout" content="main" />
        <g:set var="entityName" value="${message(code: 'cargaHora.label', default: 'CargaHora')}" />
        <title><g:message code="default.list.label" args="[entityName]" /></title>
    </head>
    <body>
        <div class="nav">
            <span class="menuButton"><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></span>
            <span class="menuButton"><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></span>
        </div>
        <div class="body">
            <h1><g:message code="default.list.label" args="[entityName]" /></h1>
            <g:if test="${flash.message}">
            <div class="message">${flash.message}</div>
            </g:if>
            <div class="list">
                <table>
                    <thead>
                        <tr>
                        
                            <g:sortableColumn property="id" title="${message(code: 'cargaHora.id.label', default: 'Id')}" />
                        
                            <th><g:message code="cargaHora.softwareFactory.label" default="Software Factory" /></th>
                        
                            <th><g:message code="cargaHora.periodo.label" default="Periodo" /></th>
                        
                            <g:sortableColumn property="valorHora" title="${message(code: 'cargaHora.valorHora.label', default: 'Valor Hora')}" />
                        
                            <g:sortableColumn property="cantidadHoras" title="${message(code: 'cargaHora.cantidadHoras.label', default: 'Cantidad Horas')}" />
                        
                            <th><g:message code="cargaHora.detalle.label" default="Detalle" /></th>
                        
                        </tr>
                    </thead>
                    <tbody>
                    <g:each in="${cargaHoraInstanceList}" status="i" var="cargaHoraInstance">
                        <tr class="${(i % 2) == 0 ? 'odd' : 'even'}">
                        
                            <td><g:link action="show" id="${cargaHoraInstance.id}">${fieldValue(bean: cargaHoraInstance, field: "id")}</g:link></td>
                        
                            <td>${fieldValue(bean: cargaHoraInstance, field: "softwareFactory")}</td>
                        
                            <td>${fieldValue(bean: cargaHoraInstance, field: "periodo")}</td>
                        
                            <td>${fieldValue(bean: cargaHoraInstance, field: "valorHora")}</td>
                        
                            <td>${fieldValue(bean: cargaHoraInstance, field: "cantidadHoras")}</td>
                        
                            <td>${fieldValue(bean: cargaHoraInstance, field: "detalle")}</td>
                        
                        </tr>
                    </g:each>
                    </tbody>
                </table>
            </div>
            <div class="paginateButtons">
                <g:paginate total="${cargaHoraInstanceTotal}" />
            </div>
        </div>
    </body>
</html>
