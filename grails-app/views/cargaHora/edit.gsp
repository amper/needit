

<%@ page import="ar.com.telecom.pcs.entities.CargaHora" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="layout" content="main" />
        <g:set var="entityName" value="${message(code: 'cargaHora.label', default: 'CargaHora')}" />
        <title><g:message code="default.edit.label" args="[entityName]" /></title>
    </head>
    <body>
        <div class="nav">
            <span class="menuButton"><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></span>
            <span class="menuButton"><g:link class="list" action="list"><g:message code="default.list.label" args="[entityName]" /></g:link></span>
            <span class="menuButton"><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></span>
        </div>
        <div class="body">
            <h1><g:message code="default.edit.label" args="[entityName]" /></h1>
            <g:if test="${flash.message}">
            <div class="message">${flash.message}</div>
            </g:if>
            <g:hasErrors bean="${cargaHoraInstance}">
            <div class="errors">
                <g:renderErrors bean="${cargaHoraInstance}" as="list" />
            </div>
            </g:hasErrors>
            <g:form method="post" >
                <g:hiddenField name="id" value="${cargaHoraInstance?.id}" />
                <g:hiddenField name="version" value="${cargaHoraInstance?.version}" />
                <div class="dialog">
                    <table>
                        <tbody>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                  <label for="softwareFactory"><g:message code="cargaHora.softwareFactory.label" default="Software Factory" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: cargaHoraInstance, field: 'softwareFactory', 'errors')}">
                                    <g:select name="softwareFactory.id" from="${ar.com.telecom.pcs.entities.SoftwareFactory.list()}" optionKey="id" value="${cargaHoraInstance?.softwareFactory?.id}" noSelection="['null': '']" />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                  <label for="periodo"><g:message code="cargaHora.periodo.label" default="Periodo" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: cargaHoraInstance, field: 'periodo', 'errors')}">
                                    <g:select name="periodo.id" from="${ar.com.telecom.pcs.entities.Periodo.list()}" optionKey="id" value="${cargaHoraInstance?.periodo?.id}" noSelection="['null': '']" />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                  <label for="valorHora"><g:message code="cargaHora.valorHora.label" default="Valor Hora" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: cargaHoraInstance, field: 'valorHora', 'errors')}">
                                    <g:textField name="valorHora" value="${fieldValue(bean: cargaHoraInstance, field: 'valorHora')}" />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                  <label for="cantidadHoras"><g:message code="cargaHora.cantidadHoras.label" default="Cantidad Horas" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: cargaHoraInstance, field: 'cantidadHoras', 'errors')}">
                                    <g:textField name="cantidadHoras" value="${fieldValue(bean: cargaHoraInstance, field: 'cantidadHoras')}" />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                  <label for="detalle"><g:message code="cargaHora.detalle.label" default="Detalle" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: cargaHoraInstance, field: 'detalle', 'errors')}">
                                    <g:select name="detalle.id" from="${ar.com.telecom.pcs.entities.DetallePlanificacionSistema.list()}" optionKey="id" value="${cargaHoraInstance?.detalle?.id}"  />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                  <label for="totalMontoHoras"><g:message code="cargaHora.totalMontoHoras.label" default="Total Monto Horas" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: cargaHoraInstance, field: 'totalMontoHoras', 'errors')}">
                                    <g:textField name="totalMontoHoras" value="${fieldValue(bean: cargaHoraInstance, field: 'totalMontoHoras')}" />
                                </td>
                            </tr>
                        
                        </tbody>
                    </table>
                </div>
                <div class="buttons">
                    <span class="button"><g:actionSubmit class="save" action="update" value="${message(code: 'default.button.update.label', default: 'Update')}" /></span>
                    <span class="button"><g:actionSubmit class="delete" action="delete" value="${message(code: 'default.button.delete.label', default: 'Delete')}" onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');" /></span>
                </div>
            </g:form>
        </div>
    </body>
</html>
