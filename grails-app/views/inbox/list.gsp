<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta name="layout" content="mainApp" />
<g:javascript library="inbox" />
<g:javascript library="animatedcollapse" />
<g:javascript library="impacto" />
<g:javascript library="boxUser" />
</head>
<body>
	
	<g:form class="formGeneral" id="formPedido" name="formPedido" style="width: 100%;" useToken="true">
			<div class="pTres">
				<g:select id="comboboxTipoConsulta" class="formInput" name="tipoConsulta" from="${tiposConsulta}" optionKey="codigo" value="${params.tipoConsulta}" noSelection="['null': '']"   />
			</div>
			<div class="pTresInbox">
				<label class="formLabel">Rol / Grupo</label>
				<g:select id="comboboxRol" name="rolAplicacion.id" from="${rolGrupos}" optionKey="codigoRol" value="${params.rolAplicacion}" noSelection="['null': 'Todos']" />
			</div>
			 
	</g:form>
	<div id="contenedorIzq">
		<g:render template="/templates/inbox" model="['aprobacionInstanceList':aprobacionInstanceList, 'aprobacionInstanceTotal':aprobacionInstanceTotal]"></g:render>
	</div> <!-- fin contenedor izq -->

	<!-- Lateral Derecho -->
	<g:render template="/templates/menuLateral"/>
	<!-- Fin Lateral Derecho -->
</body>
</html>
