
<%@ page import="ar.com.telecom.pcs.entities.ReporteAprobacionEnviados" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="layout" content="main" />
        <g:set var="entityName" value="${message(code: 'reporteAprobacionEnviados.label', default: 'ReporteAprobacionEnviados')}" />
        <title><g:message code="default.list.label" args="[entityName]" /></title>
    </head>
    <body>
        <div class="nav">
            <span class="menuButton"><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></span>
            <span class="menuButton"><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></span>
        </div>
        <div class="body">
            <h1><g:message code="default.list.label" args="[entityName]" /></h1>
            <g:if test="${flash.message}">
            <div class="message">${flash.message}</div>
            </g:if>
            <div class="list">
                <table>
                    <thead>
                        <tr>
                        
                            <g:sortableColumn property="id" title="${message(code: 'reporteAprobacionEnviados.id.label', default: 'Id')}" />
                        
                            <g:sortableColumn property="codigoDireccion" title="${message(code: 'reporteAprobacionEnviados.codigoDireccion.label', default: 'Codigo Direccion')}" />
                        
                            <g:sortableColumn property="fecha" title="${message(code: 'reporteAprobacionEnviados.fecha.label', default: 'Fecha')}" />
                        
                        </tr>
                    </thead>
                    <tbody>
                    <g:each in="${reporteAprobacionEnviadosInstanceList}" status="i" var="reporteAprobacionEnviadosInstance">
                        <tr class="${(i % 2) == 0 ? 'odd' : 'even'}">
                        
                            <td><g:link action="show" id="${reporteAprobacionEnviadosInstance.id}">${fieldValue(bean: reporteAprobacionEnviadosInstance, field: "id")}</g:link></td>
                        
                            <td>${fieldValue(bean: reporteAprobacionEnviadosInstance, field: "codigoDireccion")}</td>
                        
                            <td><g:formatDate date="${reporteAprobacionEnviadosInstance.fecha}" /></td>
                        
                        </tr>
                    </g:each>
                    </tbody>
                </table>
            </div>
            <div class="paginateButtons">
                <g:paginate total="${reporteAprobacionEnviadosInstanceTotal}" />
            </div>
        </div>
    </body>
</html>
