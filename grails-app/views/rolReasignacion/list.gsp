
<%@ page import="ar.com.telecom.pcs.entities.RolReasignacion" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="layout" content="main" />
        <g:set var="entityName" value="${message(code: 'rolReasignacion.label', default: 'RolReasignacion')}" />
        <title><g:message code="default.list.label" args="[entityName]" /></title>
    </head>
    <body>
        <div class="nav">
            <span class="menuButton"><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></span>
            <span class="menuButton"><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></span>
        </div>
        <div class="body">
            <h1><g:message code="default.list.label" args="[entityName]" /></h1>
            <g:if test="${flash.message}">
            <div class="message">${flash.message}</div>
            </g:if>
            <div class="list">
                <table>
                    <thead>
                        <tr>
                        
                            <g:sortableColumn property="id" title="${message(code: 'rolReasignacion.id.label', default: 'Id')}" />
                        
                            <th><g:message code="rolReasignacion.rol.label" default="Rol" /></th>
                        
                        </tr>
                    </thead>
                    <tbody>
                    <g:each in="${rolReasignacionInstanceList}" status="i" var="rolReasignacionInstance">
                        <tr class="${(i % 2) == 0 ? 'odd' : 'even'}">
                        
                            <td><g:link action="show" id="${rolReasignacionInstance.id}">${fieldValue(bean: rolReasignacionInstance, field: "id")}</g:link></td>
                        
                            <td>${fieldValue(bean: rolReasignacionInstance, field: "rol")}</td>
                        
                        </tr>
                    </g:each>
                    </tbody>
                </table>
            </div>
            <div class="paginateButtons">
                <g:paginate total="${rolReasignacionInstanceTotal}" />
            </div>
        </div>
    </body>
</html>
