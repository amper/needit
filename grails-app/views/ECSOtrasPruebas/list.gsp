
<%@ page import="ar.com.telecom.pcs.entities.ECSOtrasPruebas" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="layout" content="main" />
        <g:set var="entityName" value="${message(code: 'ECSOtrasPruebas.label', default: 'ECSOtrasPruebas')}" />
        <title><g:message code="default.list.label" args="[entityName]" /></title>
    </head>
    <body>
        <div class="nav">
            <span class="menuButton"><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></span>
            <span class="menuButton"><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></span>
        </div>
        <div class="body">
            <h1><g:message code="default.list.label" args="[entityName]" /></h1>
            <g:if test="${flash.message}">
            <div class="message">${flash.message}</div>
            </g:if>
            <div class="list">
                <table>
                    <thead>
                        <tr>
                        
                            <g:sortableColumn property="id" title="${message(code: 'ECSOtrasPruebas.id.label', default: 'Id')}" />
                        
                            <th><g:message code="ECSOtrasPruebas.ecs.label" default="Ecs" /></th>
                        
                            <g:sortableColumn property="seraConsolidada" title="${message(code: 'ECSOtrasPruebas.seraConsolidada.label', default: 'Sera Consolidada')}" />
                        
                            <th><g:message code="ECSOtrasPruebas.tipoPrueba.label" default="Tipo Prueba" /></th>
                        
                        </tr>
                    </thead>
                    <tbody>
                    <g:each in="${ECSOtrasPruebasInstanceList}" status="i" var="ECSOtrasPruebasInstance">
                        <tr class="${(i % 2) == 0 ? 'odd' : 'even'}">
                        
                            <td><g:link action="show" id="${ECSOtrasPruebasInstance.id}">${fieldValue(bean: ECSOtrasPruebasInstance, field: "id")}</g:link></td>
                        
                            <td>${fieldValue(bean: ECSOtrasPruebasInstance, field: "ecs")}</td>
                        
                            <td><g:formatBoolean boolean="${ECSOtrasPruebasInstance.seraConsolidada}" /></td>
                        
                            <td>${fieldValue(bean: ECSOtrasPruebasInstance, field: "tipoPrueba")}</td>
                        
                        </tr>
                    </g:each>
                    </tbody>
                </table>
            </div>
            <div class="paginateButtons">
                <g:paginate total="${ECSOtrasPruebasInstanceTotal}" />
            </div>
        </div>
    </body>
</html>
