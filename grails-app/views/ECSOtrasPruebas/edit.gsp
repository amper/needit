

<%@ page import="ar.com.telecom.pcs.entities.ECSOtrasPruebas" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="layout" content="main" />
        <g:set var="entityName" value="${message(code: 'ECSOtrasPruebas.label', default: 'ECSOtrasPruebas')}" />
        <title><g:message code="default.edit.label" args="[entityName]" /></title>
    </head>
    <body>
        <div class="nav">
            <span class="menuButton"><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></span>
            <span class="menuButton"><g:link class="list" action="list"><g:message code="default.list.label" args="[entityName]" /></g:link></span>
            <span class="menuButton"><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></span>
        </div>
        <div class="body">
            <h1><g:message code="default.edit.label" args="[entityName]" /></h1>
            <g:if test="${flash.message}">
            <div class="message">${flash.message}</div>
            </g:if>
            <g:hasErrors bean="${ECSOtrasPruebasInstance}">
            <div class="errors">
                <g:renderErrors bean="${ECSOtrasPruebasInstance}" as="list" />
            </div>
            </g:hasErrors>
            <g:form method="post" >
                <g:hiddenField name="id" value="${ECSOtrasPruebasInstance?.id}" />
                <g:hiddenField name="version" value="${ECSOtrasPruebasInstance?.version}" />
                <div class="dialog">
                    <table>
                        <tbody>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                  <label for="ecs"><g:message code="ECSOtrasPruebas.ecs.label" default="Ecs" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: ECSOtrasPruebasInstance, field: 'ecs', 'errors')}">
                                    <g:select name="ecs.id" from="${ar.com.telecom.pcs.entities.ECS.list()}" optionKey="id" value="${ECSOtrasPruebasInstance?.ecs?.id}"  />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                  <label for="seraConsolidada"><g:message code="ECSOtrasPruebas.seraConsolidada.label" default="Sera Consolidada" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: ECSOtrasPruebasInstance, field: 'seraConsolidada', 'errors')}">
                                    <g:checkBox name="seraConsolidada" value="${ECSOtrasPruebasInstance?.seraConsolidada}" />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                  <label for="tipoPrueba"><g:message code="ECSOtrasPruebas.tipoPrueba.label" default="Tipo Prueba" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: ECSOtrasPruebasInstance, field: 'tipoPrueba', 'errors')}">
                                    <g:select name="tipoPrueba.id" from="${ar.com.telecom.pcs.entities.TipoPrueba.list()}" optionKey="id" value="${ECSOtrasPruebasInstance?.tipoPrueba?.id}"  />
                                </td>
                            </tr>
                        
                        </tbody>
                    </table>
                </div>
                <div class="buttons">
                    <span class="button"><g:actionSubmit class="save" action="update" value="${message(code: 'default.button.update.label', default: 'Update')}" /></span>
                    <span class="button"><g:actionSubmit class="delete" action="delete" value="${message(code: 'default.button.delete.label', default: 'Delete')}" onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');" /></span>
                </div>
            </g:form>
        </div>
    </body>
</html>
