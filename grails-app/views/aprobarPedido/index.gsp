
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta name="layout" content="mainApp" />
<g:javascript library="boxUser" />
<g:javascript library="tiny_mce/tiny_mce" />

<script type="text/javascript">
tinyMCE.init({
    mode : "exact",
    elements : "msgAdvertencia",
    theme : "advanced",
    readonly : true,
	skin : "o2k7", 
    plugins : "autolink,lists,pagebreak,style,layer,table,save,advhr,advimage,advlink,emotions,iespell,inlinepopups,insertdatetime,preview,media,searchreplace,print,contextmenu,paste,directionality,fullscreen,noneditable,visualchars,nonbreaking,xhtmlxtras,template,wordcount,advlist,autosave", 
            
    // Theme options - button# indicated the row# only
    theme_advanced_buttons1 : "newdocument,print,|,bold,italic,underline,|,fontsizeselect,|,cut,copy,paste,|,bullist,numlist,|,undo,redo,|,forecolor,backcolor,|,image,|,fullscreen,template",
    theme_advanced_buttons2 : "",
    theme_advanced_buttons3 : "",      
    theme_advanced_toolbar_location : "top",
    theme_advanced_toolbar_align : "left",
    theme_advanced_statusbar_location : "bottom",
    theme_advanced_resizing : true,
    paste_text_sticky : true,
    setup : function(ed) {
        ed.onInit.add(function(ed) {
          ed.pasteAsPlainText = true;
        });
    },
     
	template_external_list_url : "lists/template_list.js",

		//template_external_list_url : "lists/template_list.js",
		external_link_list_url : "lists/link_list.js",
		external_image_list_url : "lists/image_list.js",
		media_external_list_url : "lists/media_list.js"
});
</script>

</head>
<body>

	<div class="contenedorTitulo">
		<g:titulo pedido="${pedidoInstance}" />
		<g:workflow pedidoId="${pedidoInstance?.id}" />
	</div>

		<div id="contenedorIzq">
				<g:if test="${flash.message}">
						<div class="message">
								${flash.message}
						</div>
				</g:if>
				<g:hasErrors bean="${pedidoInstance}">
						<div class="errors">
								<g:renderErrors bean="${pedidoInstance}" as="list" />
						</div>
				</g:hasErrors>
				<g:form class="formGeneral" useToken="true">
						<g:hiddenField name="id" value="${pedidoInstance?.id}" />
						
						<div class="seccionHide">
						<g:cabeceraPedido pedido="${pedidoInstance}" />
						<g:render template="/templates/workflowHidden"/>
						
						<p class="tituloSeccion">Aprobaci&oacute;n</p>
						<div class="formSeccion">
								<g:if test="${pedidoInstance?.faseActual?.controllerNombre.equals(params.controller) || !pedidoInstance.faseActual}">
										<div class="pDos">
												<label for="justificacionAprobacion" class="formLabel">Justificaci&oacute;n</label>
												<g:textArea name="justificacionAprobacion" value="${pedidoInstance?.getJustificacionIU()}"></g:textArea>
										</div>
								</g:if>
								<div class="pDos">
									<p class="contenidoInfo" style="width: 100%; text-align: center; font-size: 13px;">${msgAdvertencia}</p>
								</div>
						</div>
						
<%--						<g:if test="${pedidoInstance?.ultimaJustificacionAP()}">--%>
<%--						--%>
<%--								<p class="tituloSeccion">Justificaci&oacute;n de desaprobaci&oacute;n</p>--%>
<%--								<div class="formSeccion">--%>
<%--										${pedidoInstance?.ultimaJustificacionAP()}--%>
<%--								</div>--%>
<%--						</g:if>--%>

							<g:render template="/templates/ultimaJustificacionDenegacion"
						     			model="['pedidoInstance': pedidoInstance, 'fase': pedidoInstance.faseAprobacionPedido()]"></g:render>
				     															
						</div>
						<g:render template="/templates/tablaAprobaciones" model="['pedidoInstance': pedidoInstance, 'aprobacionesList': aprobacionesList, 'etiqueta': 'Interlocutor Usuario']"></g:render>
						<div id="message"></div>
						
						<div class="formSeccionButton" style="margin-top: 20px;">
								<g:actionSubmit class="formSeccionSubmitButton" id="GuardarCambio"	action="guardarCambio" value="Guardar Cambio" style="display:none" />
								<g:if test="${pedidoInstance?.faseActual?.controllerNombre.equals(params.controller) || !pedidoInstance.faseActual || pedidoInstance.legajoInterlocutorUsuario.equals(org.springframework.security.core.context.SecurityContextHolder.getContext().getAuthentication().getPrincipal().getUsername())}">
									<g:actionSubmit class="formSeccionSubmitButton" id="Simular" action="simularValidacion" value="Simular" />
									<g:actionSubmit class="formSeccionSubmitButton" id="Guardar" action="grabarFormulario" value="Guardar"/>
									<g:actionSubmit class="formSeccionSubmitButton" id="Denegar" action="denegarPedido" value="Denegar" />
									<g:actionSubmit class="formSeccionSubmitButton" id="Aprobar" action="aprobarPedido" value="Aprobar" />
								</g:if>
						</div>
				</g:form>


		</div>
		<!-- fin contenedor izq -->

		<!-- Lateral Derecho -->
	<g:render template="/templates/menuLateral" model="['pedidoInstance':pedidoInstance]"/>
	<!-- Fin Lateral Derecho -->

		<!-- 
	************
	************
	MODALS POPUP
	************
	************
	************
	-->

		<!-- popup ayuda seccion -->
		<div id="dialog-help" title="Ayuda Aprobar Pedido" style="display: none;">
				<div style="height: 350px; width: 100%; overflow: auto;">
						<p class="helpTitle">Seccion ayuda</p>
						<p>Hola esto es una seccion de ayuda Hola esto es una seccion de</p>
				</div>
		</div>
		<!-- fin popup ayuda seccion -->
</body>
</html>
