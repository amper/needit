
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta name="layout" content="mainApp" />
<g:javascript library="boxUser" />
</head>
<body>

	<div class="contenedorTitulo">
		<g:titulo pedido="${pedidoInstance}" />
		<g:workflow pedidoId="${pedidoInstance?.id}"/>
	</div>
 
		<div id="contenedorIzq">
				<g:if test="${flash.message}">
						<div class="message">
								${flash.message}
						</div>
				</g:if>
				<g:hasErrors bean="${pedidoInstance}">
						<div class="errors">
								<g:renderErrors bean="${pedidoInstance}" as="list" />
						</div>
				</g:hasErrors>
				<g:form class="formGeneral" useToken="true">
						<g:hiddenField name="id" value="${pedidoInstance?.id}" />
						
						<div class="seccionHide">
						<g:cabeceraPedido pedido="${pedidoInstance}" />
						<g:render template="/templates/workflowHidden"/>

<%--						<p class="tituloSeccion">Aprobaci&oacute;n</p>--%>
<%--						<div class="formSeccion">--%>
<%--							<g:if test="${pedidoInstance?.getJustificacionIU()}">--%>
<%--								<div class="pDos">--%>
<%--									<label for="justificacionAprobacion" class="formLabel">Justificaci&oacute;n</label>--%>
<%--									<p class="info">--%>
<%--										${pedidoInstance?.getJustificacionIU()}--%>
<%--									</p>--%>
<%--								</div>--%>
<%--							</g:if>--%>
<%--						</div>						--%>

<%--						<g:if test="${pedidoInstance?.ultimaJustificacionAP()}">--%>
<%--						--%>
<%--								<p class="tituloSeccion">Justificaci&oacute;n de desaprobaci&oacute;n</p>--%>
<%--								<div class="formSeccion">--%>
<%--										${pedidoInstance?.ultimaJustificacionAP()}--%>
<%--								</div>--%>
<%--						</g:if>--%>

							<g:render template="/templates/ultimaJustificacionDenegacion"
						     			model="['pedidoInstance': pedidoInstance, 'fase': pedidoInstance.faseAprobacionPedido()]"></g:render>
						     			
							<g:render template="/templates/tablaAprobaciones" model="['pedidoInstance': pedidoInstance, 'aprobacionesList': aprobacionesList, 'etiqueta': 'Interlocutor Usuario']"></g:render>
						</div>
						<div id="message"></div>
				</g:form>


		</div>
		<!-- fin contenedor izq -->

		<!-- Lateral Derecho -->
	<g:render template="/templates/menuLateral" model="['pedidoInstance':pedidoInstance]"/>
	<!-- Fin Lateral Derecho -->

		<!-- 
	************
	************
	MODALS POPUP
	************
	************
	************
	-->

		<!-- popup ayuda seccion -->
		<div id="dialog-help" title="Ayuda Aprobar Pedido" style="display: none;">
				<div style="height: 350px; width: 100%; overflow: auto;">
						<p class="helpTitle">Seccion ayuda</p>
						<p>Hola esto es una seccion de ayuda Hola esto es una seccion de</p>
				</div>
		</div>
		<!-- fin popup ayuda seccion -->
</body>
</html>
