<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd"> 	
<%@page import="ar.com.telecom.util.NumberUtil"%>
<%@page import="ar.com.telecom.FaseMultiton"%>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta name="layout" content="mainApp" />

<g:javascript library="impacto" />
<g:javascript library="boxUser" />
<g:javascript library="animatedcollapse" />
<g:javascript library="construccionPadre" />
</head>
<body>

	<div class="contenedorTitulo">
		<g:titulo pedido="${pedidoInstance}" />
		<g:if test="${pedidoHijo?.id}">
			<g:workflow pedidoId="${pedidoHijo?.id}"/>
		</g:if>
		<g:else> 
			<g:workflow pedidoId="${pedidoInstance?.id}"/>
		</g:else>
	</div>

		<div id="contenedorIzq">
				<g:if test="${flash.message}">
					<div class="message">
							${flash.message}
					</div>
				</g:if>
				<g:hasErrors bean="${pedidoHijo}">
						<div class="errors">
								<g:renderErrors bean="${pedidoHijo}" as="list" />
						</div>
				</g:hasErrors>
				<g:form class="formGeneral" useToken="true">
<%--					<g:hiddenField id="pedidoId" name="pedidoId" value="${pedidoInstance?.id}" />--%>
					<div class="seccionHide">
						<g:cabeceraPedido pedido="${pedidoInstance}" />
						<g:render template="/templates/workflowHidden"/>
						<g:actionSubmit id="refreshView" action="index" style="display:none" value="Refrescar vista" />
						<p class="tituloSeccion">Ejecuci&oacute;n PAU</p>

						<div class="formSeccion">

							<div class="formSeccion-content">
								<div>
									<label class='formLabel enRenglon'>Pedido hijo:</label>
									<g:select id="pedidoHijo" name="pedidoHijo"
										from="${pedidoInstance?.pedidosHijosActivos}" optionKey="id"
										value="${pedidoHijo?.id}" 
										noSelection="['null': 'Ninguno']" />
								</div>
	
								<div class="separator"></div>

								<g:hiddenField id="faseATrabajar" name="faseATrabajar" value="${FaseMultiton.EJECUCION_SOPORTE_PAU_HIJO}" />

								<script type="text/javascript">
									var g_PedidoFaseActual = "${pedidoHijo?.faseActual?.codigoFase}";
									var g_PedidoFaseVisualizacion = document.getElementById('faseATrabajar').value;
								</script>


								<div id="datosHijoSeleccionado">
									
								</div>
								
								
								<script>
								var obj ='${pedidoHijo?.id}';
								if(obj!=''){
									seleccionaHijo(obj);
								}
								</script>
								
							</div>

						</div>
						
						<div id="botonera">
							<g:render template="botonera" model="['esElResponsable':esElResponsable, 'tienePermisos':tienePermisos, 'estaEnFase':pedidoHijo?.estaEnFase(FaseMultiton.EJECUCION_SOPORTE_PAU_HIJO)]"></g:render>
						</div>
						
<%--						<input type="hidden" id="controllerNombre" name="controllerNombre" />--%>
						<div id="message"></div>
					</div>
				</g:form>
		</div>
		<!-- fin contenedor izq -->
		
		<!-- Lateral Derecho -->
	<g:render template="/templates/menuLateral" model="['pedidoInstance':pedidoInstance]"/>
	<!-- Fin Lateral Derecho -->
	
			<!-- 
		************
		************
		MODALS POPUP
		************
		************
		************
		-->
		<!-- popup ayuda seccion -->
		<div id="dialog-help" title="Ayuda Aprobar Pedido" style="display: none;">
				<div style="height: 350px; width: 100%; overflow: auto;">
						<p class="helpTitle">Seccion ayuda</p>
						<p>Hola esto es una seccion de ayuda Hola esto es una seccion de</p>
				</div>
		</div>
		<!-- fin popup ayuda seccion -->
		
		<!-- popup Agrega actividad -->
<%--		<div id="dialog-AgregaActividad" title="Agregar actividad" style="display: none;">--%>
<%--			<g:render template="agregaActividad"/>--%>
<%--		</div>--%>
		<!-- fin popup Agrega actividad -->
		
		
		<!-- Popup editar actividad -->
		<div id="dialog-EditarActividad" title="Editar actividad" style="display: none;">
<%--			<g:render template="editarActividad" model="['pedidoHijo':pedidoHijo]"/>--%>
		</div>
		<!-- Fin popup editar actividad -->
		
		<!-- Popup finalizar actividad -->
		<div id="dialog-FinalizarActividad" title="Finalizar actividad" style="display: none;">
		</div>
		<!-- Fin popup finalizar actividad -->
		
</body>
</html>