<g:if test="${estaEnFase}">
	<center>
		<g:if test="${esElResponsable}">
			<div class="formSeccionButton" style="margin-top: 20px;">
				<g:actionSubmit class="formSeccionSubmitButton" id="Simular"
					controller="construccionPadre" action="simularValidacionHijo" value="Simular" />
				<g:actionSubmit class="formSeccionSubmitButton" id="Guardar"
					action="grabarFormulario" value="Guardar" />
				
				<g:if test="${!impactoInstance?.pedido?.estaSuspendido()}">
					<g:actionSubmit class="formSeccionSubmitButton" id="Aprobar"
						action="finalizarPedidoHijo" value="Finalizar fase" />
				</g:if>
			</div>
		</g:if>
		<g:if test="${!esElResponsable && tienePermisos}">
			<g:actionSubmit class="formSeccionSubmitButton" id="Guardar"
					action="grabarFormulario" value="Guardar" />
		</g:if>
	</center>
</g:if> 