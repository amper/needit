<g:set var="logueado" value="${org.springframework.security.core.context.SecurityContextHolder.getContext().getAuthentication().getPrincipal().getUsername()}" />
<%
  def cssClass = 'realizada'
  ultimaFase = "N"
%>
<g:each in="${fases}" var="fase">

  <g:if test="${faseActual?.id == fase?.id}">
<%
  ultimaFase = "S"
  cssClass = 'inactiva'
%> 
    <g:if test="${esHijo}">
                <!-- FASE ACTUAL DEL HIJO -->
                <td id="workflow-fase-hija" class="actual<g:if test="${params.controller.equalsIgnoreCase(fase.controllerNombre)}">-selected</g:if>" title="${fase.tooltip}">
                  <div id="fase${fase.id}" <g:if test="${params.controller.equalsIgnoreCase(fase.controllerNombre)}">class="fase-selected"</g:if>>
                  <img src="${resource(dir: 'css/images', file: 'workflow-spacer-1x1.gif')}" width="79" height="1" /><br />
                  <a href="javascript:submitNavegarFase('${fase.controllerNombre}','${logueado}','${faseActual.codigoFase}');">${fase.descripcion}</a>
                  <br /><img src="${resource(dir: 'css/images', file: 'workflow-spacer-1x1.gif')}" width="79" height="1" />
                  </div>
                </td>
    </g:if><g:else>
                <!-- FASE ACTUAL DEL PADRE -->
                <td id="workflow-fase" class="actual<g:if test="${params.controller.equalsIgnoreCase(fase.controllerNombre)}">-selected</g:if>" title="${fase.tooltip}">
                  <div id="fase${fase.id}" <g:if test="${params.controller.equalsIgnoreCase(fase.controllerNombre)}">class="fase-selected"</g:if>>
                  <img src="${resource(dir: 'css/images', file: 'workflow-spacer-1x1.gif')}" width="77" height="1" /><br />
                  <a href="javascript:submitNavegarFase('${fase.controllerNombre}','${logueado}','${faseActual.codigoFase}');">${fase.descripcion}</a>
                  <br /><img src="${resource(dir: 'css/images', file: 'workflow-spacer-1x1.gif')}" width="77" height="1" />
                  </div>
                </td>
    </g:else>

  </g:if>
  <g:else>

    <g:if test="${esHijo}">
                <!-- FASE ORDINARIA -- HIJO -->
                <td id="workflow-fase-hija" class="${cssClass}<g:if test="${params.controller.equalsIgnoreCase(fase.controllerNombre)}">-selected</g:if>" title="${fase.tooltip}">
                  <div id="fase${fase.id}" <g:if test="${params.controller.equalsIgnoreCase(fase.controllerNombre)}">class="fase-selected"</g:if>>
                  <img src="${resource(dir: 'css/images', file: 'workflow-spacer-1x1.gif')}" width="79" height="1" /><br />
      <g:if test="${ultimaFase && ultimaFase.equalsIgnoreCase('N')}">
                  <a href="javascript:submitNavegarFase('${fase.controllerNombre}','${logueado}','${faseActual.codigoFase}');">${fase.descripcion}</a>
      </g:if><g:else>
                  ${fase.descripcion}
      </g:else>
                  <br /><img src="${resource(dir: 'css/images', file: 'workflow-spacer-1x1.gif')}" width="79" height="1" />
                  </div>
                </td>
    </g:if>
    <g:else>
                <!-- FASE ORDINARIA -- PADRE -->
                <td id="workflow-fase" class="${cssClass}<g:if test="${params.controller.equalsIgnoreCase(fase.controllerNombre)}">-selected</g:if>" title="${fase.tooltip}">
                  <div id="fase${fase.id}" <g:if test="${params.controller.equalsIgnoreCase(fase.controllerNombre)}">class="fase-selected"</g:if>>
                  <img src="${resource(dir: 'css/images', file: 'workflow-spacer-1x1.gif')}" width="77" height="1" /><br />
      <g:if test="${ultimaFase && ultimaFase.equalsIgnoreCase('N')}">
                  <a href="javascript:submitNavegarFase('${fase.controllerNombre}','${logueado}','${faseActual.codigoFase}');">${fase.descripcion}</a>
      </g:if><g:else>
                  ${fase.descripcion}
      </g:else>
                  <br /><img src="${resource(dir: 'css/images', file: 'workflow-spacer-1x1.gif')}" width="77" height="1" />
                  </div>
                </td>

    </g:else>
  </g:else>

</g:each>