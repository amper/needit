<%@page import="ar.com.telecom.pcs.entities.AnexoListaBlanca"%>
<g:updateToken />
<!-- modal popup anexos -->
<div id="spinner"></div>

<g:uploadForm enctype="multipart/form-data" id="formAnexo" name="formAnexo" class="formAnexo" action="guardarActividadAnexo" useToken="true">

		<g:hiddenField name="pedidoId" value="${params.pedidoId}" />
		<g:hiddenField name="impacto" id="impacto" value="${params.impacto}"/>
		<g:hiddenField name="idActividad" id="idActividad" value="${params.idActividad}"/>
		 
		<%
			extensiones = AnexoListaBlanca?.list().extension.collect{ ext-> "'" + ext + "'"}
		%>
		
		<div class="pCompleto">
				<label for='anexos' class="formLabel">Archivo</label> <input class="formInput" type="file" id="anexoArchivo" name="anexoArchivo" />
		</div>
		<br>

		<div class="pCompleto">
				<label for='inputDescripcionAnexo' class="formLabel">Descripci&oacute;n</label>
				<g:textArea name="descripcionAnexo" id='inputDescripcionAnexo'></g:textArea>
		</div>

		<br>

		<script type="text/javascript">
			jQuery(document).ready(
			function($) {
				$( "#comboboxTipoAnexo" ).autocomplete();				
			});
		</script>

	<div class="pCompleto">
			<label for='comboboxTipoAnexo' class="formLabel">Tipo de Anexo</label>
			<g:if test="${tiposDeAnexo?.size() == 1}">
					<g:set scope="page" var="tipoAnexo" value="${tiposDeAnexo.first()}" />
					<p class="infoNoBorder">
							${tipoAnexo.descripcion}
					</p>
					<div style="display: none;">
							<g:select id="comboboxTipoAnexo" name="tipoAnexo.id" from="${tiposDeAnexo}" optionKey="id" value="${tipoAnexo.id}" />
					</div>
			</g:if>
			<g:else>
					<g:select id="comboboxTipoAnexo" name="tipoAnexo.id" from="${tiposDeAnexo}" optionKey="id" value="" noSelection="['null': '']" style="display: none;" />
			</g:else>
	</div>

	<br>
	<br>
	<br>
	<br>
	<br>
	
	<div align="center" class="formSeccionButton">
			<button id="cerrarAnexo" class="formSeccionSubmitButtonAnexo" onclick="cerrarPopUpAnexoPorTipoActividad();">Cerrar</button>
			<button id="aceptarAnexo" class="formSeccionSubmitButtonAnexo" onclick="validarAnexo(${extensiones});">Aceptar</button>
	</div>

</g:uploadForm>

<!-- fin de modal popup anexos -->

<script>
if ($('hVengoPorAnexos')){
	if ($('hVengoPorAnexos').value == 'true') {
		openDialog.delay(1);
		$('hVengoPorAnexos').value = '';
	}
}
</script>