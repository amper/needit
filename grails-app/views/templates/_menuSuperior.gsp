<%@page import="ar.com.telecom.pcs.entities.ParametrosSistema"%>
<%@ page import="org.codehaus.groovy.grails.commons.ConfigurationHolder" %>
    <div id="subMenuUsuario" onmouseover="mostrarMenuUsuario('subMenuUsuario', false)" onmouseout="mostrarMenuUsuario('subMenuUsuario', true)">
      <p><a id="MiPerilClick" href="#">Mi Perfil</a></p>
      <p><g:link controller="logout">Cerrar Sesi&oacute;n</g:link></p>
    </div>
    
<%
  //TODO: Meterlo que de movida llame al updater del inbox
  def listaParametros = ParametrosSistema.list()
  def parametro = listaParametros.first()
  def intervalInbox
  def intervalInboxAprobaciones

  if (listaParametros.isEmpty()) { 
    intervalInbox = 300
    intervalInboxAprobaciones = 300
  } else {
    intervalInbox = parametro.tiempoActualizacionInbox ?: 300
    intervalInboxAprobaciones = parametro.tiempoActualizacionInboxAprobaciones ?: 300
  }
  
%>     
    
    <div id="ContenedorMenu">
      <div id="Logo">
      	<img src="${resource(dir:'images/logos',file:ConfigurationHolder.config.imagen.needit)}" width="150px" height="100%" border="0">
      </div>
      <div id="Menu">
        <div class="menuItem" style="position:relative" title="Inbox">
          <g:link controller="inbox" class="menuHome${params.controller == 'inbox' ? 'On' : ''}"></g:link>
        <g:link controller="inbox"><div id="inboxCount" class="inbox-contadores"></div></g:link>
        </div>
        <g:set var="usuarioLogueado" value="${org.springframework.security.core.context.SecurityContextHolder.getContext().getAuthentication()?.getPrincipal()?.getUsername()}"></g:set>
        <g:if test="${g.puedeEditarRegistroPedido(usuarioLogueado: usuarioLogueado) == 'true'}">
	        <div class="menuItem" title="Nuevo pedido">
	          <g:link controller="registroPedido" action="nuevoPedido" class="menuNuevo${params.controller == 'registroPedido' ? 'On' : ''}"></g:link>
	        </div>
		</g:if>
         
        <div class="menuItem" style="position:relative" title="Inbox Aprobaciones">
          <g:link controller="inboxAprobaciones" class="menuInboxAprobaciones${params.controller == 'inboxAprobaciones' ? 'On' : ''}"></g:link>
        <g:link controller="inboxAprobaciones"><div id="inboxAprobacionesCount" class="inbox-contadores"></div></g:link>
        </div>
        <div class="menuItem" title="Busqueda Avanzada">
          <g:link controller="busqueda" class="menuBusqueda${params.controller == 'busqueda' ? 'On' : ''}"></g:link>
        </div>
        <div class="menuItem" title="Ayuda">
          <g:link url="${resource(dir:'ayuda',file:'index.gsp')}" target="_blank" class="menuAyuda${params.controller == 'ayuda' ? 'On' : ''}"></g:link>
        </div>
        <sec:ifAnyGranted roles="${parametro.roleAdmin}">
	        <div class="menuItem" title="Administracion">
	          <g:link controller="administracion" action="index" class="menuAdministracion${params.controller == 'administracion' ? 'On' : ''}"></g:link>
        	</div>
		</sec:ifAnyGranted>
		<sec:ifAnyGranted roles="${parametro.grupoAdmin}">
	        <div class="menuItem" title="Administracion">
	          <g:link controller="administracion" action="index" class="menuAdministracion${params.controller == 'administracion' ? 'On' : ''}"></g:link>
        	</div>
		</sec:ifAnyGranted>
      </div>
      <div id="Search">
        <g:form style="margin:0px;" controller="searchable" action="index">
          <g:textField id="InputSearch" class="searchInput" onfocus="this.style.backgroundColor='#fff'" onblur="this.style.backgroundColor='#c1d5d9'; this.value='';" name="id"></g:textField>
        </g:form>
      </div>
<sec:ifLoggedIn>
      <div id="MenuUsuario" onmouseover="mostrarMenuUsuario('subMenuUsuario', false)" onmouseout="mostrarMenuUsuario('subMenuUsuario', true)">
        <g:userBoxHeader/>
      </div>
</sec:ifLoggedIn>
    </div>
    
<sec:ifLoggedIn>
    <!-- popup mi perfil -->
	<div id="MiPerfilModal" title="Mi Perfil" style="display: none;">
			<g:miPerfilModal />
	</div>
</sec:ifLoggedIn>
    
    


<script type="text/javascript">


  var sIntervalInbox = ${intervalInbox};
  var sIntervalInboxAprobaciones = ${intervalInboxAprobaciones};

  var sUrl1 = '<g:createLink controller="inbox" action="inboxCount" />';
  var sUrl2 = '<g:createLink controller="inboxAprobaciones" action="inboxCount" />';

  var gTimer = setTimeout("ajaxLoader()", 7000);

  function ajaxLoader(){

    new Ajax.PeriodicalUpdater('inboxCount', sUrl1, {
      frequency: sIntervalInbox,
      onFailure: function (transport) {
          $('inboxCount').hide();
      },
      onSuccess: function (transport) {
        if (transport.responseText == '0') {
          $('inboxCount').hide();
        } else {
          $('inboxCount').show();
        }
      }
    });
    new Ajax.PeriodicalUpdater('inboxAprobacionesCount', sUrl2, {
      frequency: sIntervalInboxAprobaciones,
      onFailure: function (transport) {
        $('inboxAprobacionesCount').hide();
      },
      onSuccess: function (transport) {
        if (transport.responseText == '0') {
          $('inboxAprobacionesCount').hide();
        } else {
          $('inboxAprobacionesCount').show();
        }
        // TODO: Poner el error aca
      }
    });
    //gTimer.clearTimeOut();
  }

</script>