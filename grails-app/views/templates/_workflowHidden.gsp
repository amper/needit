		<%
		if (!params.action) {
			params.action = "index"
		}
		 %>
		<g:actionSubmit class="formSeccionSubmitButton" id="NavegarFase"
			action="navegarFase" value="${params.action}"
			style="display:none" />
		<input type="hidden" id="controllerNombre" name="controllerNombre" />
		<input type="hidden" id="vista" name="vista" value="${vista ? vista : "index"}" />

		<g:set var="pedidoOrigen" value="${impactoInstance ? impactoInstance.pedido : pedidoInstance}"/>
		<g:set var="aprobacion" value="${pedidoOrigen?.aprobacionPendiente(pedidoOrigen?.faseActual, usuarioLogueado) ?: pedidoOrigen?.aprobacionActual(pedidoOrigen?.faseActual, usuarioLogueado)}"/>

		<g:hiddenField id="aprobacionId" name="aprobacionId" value="${aprobacion?.id}" />
		
		<g:hiddenField name="confirmaCambioDireccion" id="confirmaCambioDireccion"></g:hiddenField>
		<g:hiddenField name="actionFromDireccion" id="actionFromDireccion"></g:hiddenField>
		<g:hiddenField name="muestraPopDireccion" id="muestraPopDireccion"></g:hiddenField>