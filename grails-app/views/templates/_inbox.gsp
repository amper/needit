		<%@page import="ar.com.telecom.pcsPlus.PedidoService"%>
		<%@page import="ar.com.telecom.pedido.consulta.Consulta"%>
		
		<g:updateToken />
		
		<g:if test="${flash.message}">
			<div class="message">
					<li>${flash.message}</li>
			</div>
		</g:if>
 
		<div class="list">
			<table>
				<thead>
					<tr>
						<th class="encabezado0"/>
						<g:sortableColumn property="id" class="encabezado1" title="N&uacute;mero" action="list" params="${params}"/>
						<g:sortableColumn property="titulo" class="encabezado2" title="T&iacute;tulo" action="list" params="${params}"/>
						<g:sortableColumn property="faseActual" class="encabezado3" title="Fase" action="list" params="${params}" />
						<g:sortableColumn property="fasePedido.macroEstado" class="encabezado4" action="list" title="Macroestado" params="${params}"/>
						<th class="encabezado6">Rol / Grupo</th>
						<th class="encabezado0"/>
					</tr>
				</thead>
				<tbody>
					<g:if test="${!pedidoInstanceList.isEmpty()}">
					<g:each in="${pedidoInstanceList}" status="i" var="pedidoInstance">
							<g:set scope="page" var="pedido" value="${pedidoInstance}" />
							<g:if test="${!params.tipoConsulta.equals(Consulta.EN_SEGUIMIENTO_PROPIOS)}">
								<g:divsInbox i="${i}" pedido="${ pedido }" usuarioLogueado="${usuarioLogueado}" tipoConsulta="${ tipoConsulta }" ></g:divsInbox>
							</g:if>
							<g:else>
								<g:divsInboxEnSeguimiento i="${i}" pedido="${ pedido }" usuarioLogueado="${usuarioLogueado}" tipoConsulta="${ tipoConsulta }" />
							</g:else>
					</g:each>
					</g:if>
					<g:else>
						<tr>
							<td colspan="7" style="text-align: center; font-style: italic;">No hay pendientes por el momento...</td>
						</tr>
					</g:else>
				</tbody>
			</table>
		</div>
		<div class="paginateButtons">
			<g:paginate params="${params}" action="verPendientes" total="${aprobacionInstanceTotal?aprobacionInstanceTotal:0}" />
		</div>
		
		<script type="text/javascript">
			jQuery(document).ready(function() {
				var token = $("org.codehaus.groovy.grails.SYNCHRONIZER_TOKEN").value;
				var steps = jQuery(".paginateButtons .step");
				jQuery.each(steps, function(i, ele) {
					var stepAttr = jQuery(ele).attr('href');
					jQuery(ele).attr('href', stepAttr + '&org.codehaus.groovy.grails.SYNCHRONIZER_TOKEN=' + token);
				});

				var prevLinkAttr = jQuery(".paginateButtons .prevLink").attr('href');
				jQuery(".paginateButtons .prevLink").attr('href', prevLinkAttr + '&org.codehaus.groovy.grails.SYNCHRONIZER_TOKEN=' + token);
				var nextLinkAttr = jQuery(".paginateButtons .nextLink").attr('href');
				jQuery(".paginateButtons .nextLink").attr('href', nextLinkAttr + '&org.codehaus.groovy.grails.SYNCHRONIZER_TOKEN=' + token);
			});
		</script>
		
		
		<g:if test="${pedidoInstanceList}">
			<% 
				params.format="excel"; 
				params.extension="xls"; 
			%>
			<a href="javascript:window.location='${createLink(params: params, action: "exportFilePedidosPadre")}';" >Excel Padres</a> <b>|</b>
			<a href="javascript:window.location='${createLink(params: params, action: "exportFilePedidosHijo")}';" >Excel Hijos</a> <b>|</b>
			<% 
				params.format="pdf"; 
				params.extension="pdf"; 
			%>
			<a href="javascript:window.location='${createLink(params: params, action: "exportFilePedidosPadre")}';" >PDF Padres</a> <b>|</b>
			<a href="javascript:window.location='${createLink(params: params, action: "exportFilePedidosHijo")}';" >PDF Hijos</a> 
			
		</g:if>

		<script>
			animatedcollapse.ontoggle=function($, divobj, state){ myOnToggle(divobj, state); };
			animatedcollapse.init();
		</script>