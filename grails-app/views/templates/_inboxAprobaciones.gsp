<%@page import="ar.com.telecom.pcsPlus.PedidoService"%>
<%@page import="ar.com.telecom.util.DateUtil"%>
<g:updateToken />
<g:uploadForm class="formGeneral" id="formInboxAprobaciones" name="formInboxAprobaciones" useToken="true" >
	<div style="text-align: left;">
			<g:if test="${!params?.inbox?.isEmpty() && flash.message}">
				<div class="message">
						<li>${flash.message}</li>
				</div>
			</g:if>
	
			<g:hasErrors bean="${resultAprobaciones?.errors}">
				<div class="errors">
					<g:renderErrors bean="${resultAprobaciones?.errors}" as="list" />
				</div>
			</g:hasErrors>
		
		
	</div>
	 
	<div class="list">
		<table>
			<thead>
				<tr>
					<th class="encabezado0"><g:checkBox id="checkSeleccionarTodos" name="todosInbox" onClick="selectCombos(this.id);" /></th>
					<g:sortableColumn property="pedido.id" title="N&uacute;mero" params="${params}"/>
					<g:sortableColumn property="pedido.titulo" title="T&iacute;tulo" params="${params}"/>
					<g:sortableColumn property="rol" title="Rol/Grupo" params="${params}"/>
					<g:sortableColumn property="pedido.faseActual" title="Fase" params="${params}" />
					<g:sortableColumn property="pedido.tipoImpacto" title="Sistema / AS / Tipo Impacto" params="${params}" />
					<g:sortableColumn property="fecha" title="En cola desde" params="${params}" />					
				</tr>
			</thead>
			<tbody>
				<g:if test="${!aprobacionInstanceList.isEmpty()}">
					<g:each in="${aprobacionInstanceList}" status="i" var="aprobacionInstance">
						<g:set scope="page" var="pedido" value="${aprobacionInstance?.pedido}" />
						<g:set scope="page" var="aprobacion" value="${aprobacionInstance}" />
						<% def link; %>
						<g:if test="${!pedido.esHijo()}">
							<% link = createLink(id: pedido.id,params: [pedidoId: pedido.id], controller: pedido?.faseActual?.controllerNombre); %>
						</g:if>
						<g:else>
							<% link = createLink(id: pedido.parent.id,params: [pedidoId: pedido.parent.id, impacto: pedido.id], controller: pedido?.faseActual?.controllerNombre); %>
						</g:else>
						
						<tr class="${(i % 2) == 0 ? 'odd' : 'even'}" style="cursor: pointer;">
							<td>
								<g:checkBox name="inbox" value="${aprobacion?.id}" checked="${params?.inbox?.contains(aprobacion?.id?.toString())}" onClick="unSelectCombos(this.id);" id="${aprobacion?.id}" class="checkAprobacion" />
							</td>
	
							<td onClick="javascript:window.location = '${link}'">
								<g:if test="${!pedido.esHijo()}">
									${pedido?.id}
								</g:if>
								<g:else>
									${pedido.getNumero()}
								</g:else>
							</td>
	
							<td onClick="javascript:window.location = '${link}'">
								${pedido?.titulo}
							</td>
	
							<td onClick="javascript:window.location = '${link}'">
								${pedido?.getRol(pedido?.faseActual, usuarioLogueado)}
							</td>
	
							<td onClick="javascript:window.location = '${link}'">
								${pedido?.faseActual}
							</td>
	
							<td onClick="javascript:window.location = '${link}'">
								<g:if test="${pedido.esHijo()}">
									${pedido?.tipoImpacto }
								</g:if>
								<g:else>
									N/A
								</g:else>
							</td>
							
							<td onClick="javascript:window.location = '${link}'">
								${DateUtil.toString(aprobacion.fecha)}
							</td>
	
						</tr>
					</g:each>
				</g:if>
				<g:else>
					<tr>
						<td colspan="7" style="text-align: center; font-style: italic;">No hay pendientes por el momento...</td>
					</tr>
				</g:else>
			</tbody>
		</table>
	</div>
	
	<div class="paginateButtons">
		<g:paginate total="${aprobacionInstanceTotal?aprobacionInstanceTotal:0}"/>
	</div>
	<script type="text/javascript">
		jQuery(document).ready(function() {
			var token = $("org.codehaus.groovy.grails.SYNCHRONIZER_TOKEN").value;
			var steps = jQuery(".paginateButtons .step");
			jQuery.each(steps, function(i, ele) {
				var stepAttr = jQuery(ele).attr('href');
				jQuery(ele).attr('href', stepAttr + '&org.codehaus.groovy.grails.SYNCHRONIZER_TOKEN=' + token);
			});
			
			var prevLinkAttr = jQuery(".paginateButtons .prevLink").attr('href');
			jQuery(".paginateButtons .prevLink").attr('href', prevLinkAttr + '&org.codehaus.groovy.grails.SYNCHRONIZER_TOKEN=' + token);
			var nextLinkAttr = jQuery(".paginateButtons .nextLink").attr('href');
			jQuery(".paginateButtons .nextLink").attr('href', nextLinkAttr + '&org.codehaus.groovy.grails.SYNCHRONIZER_TOKEN=' + token);
		});
	</script>
	<g:if test="${!aprobacionInstanceList.isEmpty()}">
		<div class="formSeccionButton" style="width: 70%" style="margin-top: 20px;">
			<div style="text-align: left; align: right;">
				<label for="justificacionAprobacion" class="formLabel" style="margin-left: 0px;">Justificaci&oacute;n</label>
				<g:textArea id="justificacionAprobacion" name="justificacionAprobacion"></g:textArea>
			</div>
			<br>
			<div align="center">
				<g:submitToRemoteSecure class="formSeccionSubmitButtonHistorial ui-button ui-widget ui-state-default ui-corner-all ui-state-hover" id="Denegar" action="denegarPedido" value="Denegacion Selec." update="aprobaciones" onLoading="showSpinner('spinner', true);" after="document.getElementById('justificacionAprobacion').value = '';" />
				<g:submitToRemoteSecure class="formSeccionSubmitButtonHistorial ui-button ui-widget ui-state-default ui-corner-all ui-state-hover" id="Aprobar" action="aprobarPedido" value="Aprobacion Selec." update="aprobaciones" onLoading="showSpinner('spinner', true);" after="document.getElementById('justificacionAprobacion').value = '';" />
			</div>
		</div>
	</g:if>
</g:uploadForm>