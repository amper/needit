 <g:updateToken pedido="${pedidoInstance}"/>
 
 <table>
  <thead>
	     <tr>
	     <th>&Aacute;reas de soporte impactadas</th>
	         
	         <g:if test="${ visible }"><g:sortableColumn property="Eliminar" title=" " /></g:if>
	     </tr>
 </thead>
 <g:if test="${areasSoporteSeleccionadas}">
 
		<g:set var="counter" value="${0}" />
		<tbody>
		<g:each in="${areasSoporteSeleccionadas}" status="i" var="area">
 		 <tr class="${(i % 2) == 0 ? 'odd' : 'even'}">
		      <td>${area.descripcion} </td>
		      <g:if test="${ visible }"><td>
		      	<g:secureRemoteLink title="Eliminar" class="eliminarItem" update="areasSoporte" controller="especificacionCambio" 
					params="${['pedidoId': pedidoInstance.id, 'areaId': area.id]}" id="${counter}"
					action="eliminarArea"></g:secureRemoteLink>  
  			</td></g:if>
 
         </tr>
		<g:set var="counter" value="${counter + 1}" />
		</g:each>
		</tbody>
		</g:if>
		<g:else>
			<tr><td colspan="2"  class="odd">No hay &aacute;reas de soporte impactadas</td></tr>
		</g:else>
</table>
