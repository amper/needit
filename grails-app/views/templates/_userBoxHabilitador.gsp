<g:updateToken />
<script type="text/javascript">

jQuery(document).ready(
		function($) {
			
			var persons = [
			   			
						   <g:each in="${personsAutocomplete}" var="user">
			   		
			    			{
			    				value: "${user?.legajo}",
			    				label: "${user?.legajo +"-"+user?.nombreApellido}"
			    			},	
			    		 </g:each>
			    		];
			 
			$("#inputUser${box}").autocomplete({
				minLength: 1,
				source : persons,
				focus: function( event, ui ) {
					$( "#persons" ).val( ui.item.label );
					return false;
				},
				select : function(event, ui) {
					completarUserBoxHabilitador(ui.item.value, '${box}', '${usuarioLogueado}');
					return false;
				}
			});

});
</script>
	
<g:if test="${person}">	
	<g:userBox box="${box}" legajo="${person}"/>
	<g:if test="${box == 'legajoUsuarioGestionDemanda'}">
		<script>
			recargaBotoneraEvaluacionCambio('${person}','${usuarioLogueado}', '${pedidoInstance?.id}');
		</script>
	</g:if>
</g:if>
<g:else>
	<g:if test="${box == 'legajoGerenteUsuario'}">
		<script>mensajeGerenteIT(true);</script>
	</g:if>
	<img id="fotoG${box}" class="img" onerror="javascript:UrlNotValid('fotoG${box}')"
		src="http://signomobile.telecom.com.ar/irj/go/km/docs/fotos/u000000"
		alt="foto" />
	<g:if test="${!personsAutocomplete?.isEmpty() }">
		<input class="userInput" type='text' id='inputUser${box}' value='' />
	</g:if>
	<g:else>
		<input class="userInput" type='text' id='inputUser${box}' value='' disabled/>
	</g:else>
</g:else>
<g:if test="${pedidoInstance?.puedeReasignarEvaluacionPedido(usuarioLogueado)}">
	<script>
		$('eliminarAsignatario').show();
	</script>
</g:if>
<g:else>
	<script>
		//$('eliminarAsignatario').hide();
	</script>
</g:else>
