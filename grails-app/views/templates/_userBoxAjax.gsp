<g:updateToken />

<g:if test="${person}">
	<g:userBox box="${box}" legajo="${person}"/>
</g:if>
<g:else>
	<img id="fotoG${box}" class="img" onerror="javascript:UrlNotValid('fotoG${box}')"
		src="http://signomobile.telecom.com.ar/irj/go/km/docs/fotos/u000000"
		alt="foto" />
		
	<input type="text" id="inputUser${box}" name="inputUser${box}" class="userInput ui-autocomplete-input"/>
	<div id="userBoxAjax-autocomplete_choices" class="autocomplete" style="display: none;"></div>
 
	<script type="text/javascript">
		new Ajax.Autocompleter("inputUser${box}", "userBoxAjax-autocomplete_choices", "${createLink(controller: 'registroPedido', action: method)}", {
		  paramName: "filtro",
		  minChars: 2,
		  updateElement : doUserSelection
		});
		
		function doUserSelection(li) {
		  completarUserBox(li.id, '${box}', '${boxParalelo}', '', '');
		}
		
	</script>
</g:else>

