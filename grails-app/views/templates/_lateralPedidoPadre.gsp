<g:if test="${pedidoInstance}">
<p class="tituloContenedor">Pedido Usuario</p>
<div class="contenidoContenedor">
	<g:link controller="${pedidoInstance.faseActual.controllerNombre}" params="['pedidoId': pedidoInstance.id]">
	<div class='itemContenedor' style="cursor:pointer;">
		<p><strong>N&uacute;mero:</strong> ${pedidoInstance.id}</p>
		<p><strong>T&iacute;tulo:</strong> ${pedidoInstance.titulo}</p>
		<p><strong>Tipo Pedido:</strong> ${pedidoInstance.tipoPedido}</p>
        <p><strong>Tipo Gesti&oacute;n:</strong> ${pedidoInstance.tipoGestion}</p>
		<p><strong>Estado Pedido: </strong><g:if test="${pedidoInstance.descripcionStatus.equalsIgnoreCase('')}">En curso...</g:if><g:indicadorEstadoPedido pedido="${pedidoInstance}" /></p>
	</div>
	</g:link>
</div>
</g:if> 