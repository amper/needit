<%@page import="ar.com.telecom.pcs.entities.TipoReferencia"%>
<g:updateToken />
<label class="formLabel">Referencia Origen</label>
<g:if test="${tipoReferencia?.equalsIgnoreCase(TipoReferencia.needIt)}">

	<script type="text/javascript">
		jQuery(document).ready(
			function($) {
				$('.busqueda-rapida-2').busquedaRapida();
			});
	</script>
	<input data-name="referenciaOrigen" name="referenciaOrigen" data-url="/registroPedido/busquedaAutocomplete" data-method-name="obtenerReferenciasOrigen" value="${pedidoInstance?.referenciaOrigen}" data-pedido-id="${pedidoInstance?.id}" data-valor-id="" class="busqueda-rapida-2 formInput" />
</g:if>
<g:else>
	<g:textField class="formInput" name="referenciaOrigen" value="${pedidoInstance?.referenciaOrigen}" />
</g:else>
 