<g:updateToken />
<script type="text/javascript"> 
jQuery(document).ready(
		function($) {
			
			var persons = [
						   <g:each in="${personsAutocomplete}" var="user" status="index">
			    			{
			    				value: "${user?.legajo}",
			    				label: "${user?.legajo +"-"+user?.nombreApellido}"
			    			}
			    			<g:if test="${index < personsAutocomplete.size() - 1}">
			    			,	
			    			</g:if>
			    		 </g:each>
			    		];

			$("#inputUser${box}").autocomplete({
				minLength: 1,
				source : persons,
				focus: function( event, ui ) {
					$( "#persons" ).val( ui.item.label );
					return false;
				},
				select : function(event, ui) {
					completarUserBox(ui.item.value, '${box}', '${boxParalelo}', '${pedidoHijo ? pedidoHijo?.id : pedido?.id}', '${pedidoHijo?.grupoResponsable}', '${actividad?.id}');
					return false;
				}
			});
});
</script>
<div id="userBoxAjax-autocomplete_choices" class="autocomplete" style="display: none;"></div>
<g:if test="${person}">
	<g:userBox box="${box}" legajo="${person}"/>
	<g:if test="${box == 'legajoUsuarioResponsable'}">
		<script>
			recargaBotonera('${person}','${usuarioLogueado}', $("impacto").value, '${pedidoHijo?.id}');
		</script>
	</g:if>
	<g:if test="${box == 'legajoResponsableConstruccionPadre' && cambiaResponsable}">
		<script>
			var impacto = '${pedidoHijo?.id}'; 
			if(!impacto){
				impacto = $("impacto").value; 
			}
					
			//permisosResponsableConstruccionPadre('${person}', '${pedidoHijo?.id}');
			permisosResponsableConstruccionPadre('${person}', impacto);
			recargaBotoneraActividades(impacto);
		</script>
	</g:if>
	
	<g:if test="${box == 'legajoUsuarioResponsablePop'}">
			<script>
				document.getElementById('responsableActividadTemporal').value = '${person}';
				initFormularioCamposActividadesSinGrabar('${origenActividad}');
			</script>
	</g:if>

	<g:if test="${box == 'aprobador' && cambiaResponsable}">
		<script>
			var sUrl = '<g:createLink controller="aprobacionImplantacionHijo" action="reasignaAprobaciones"/>';
			reasignaAprobador($F('impacto'), sUrl, $F('usuarioAnterior'),"${person}");
			$('usuarioAnterior').value = "${person}";
		</script>
	</g:if>
	
</g:if>
<g:else>
	<img id="fotoG${box}" class="img" onerror="javascript:UrlNotValid('fotoG${box}')"
		src="http://signomobile.telecom.com.ar/irj/go/km/docs/fotos/u000000"
		alt="foto" />
		<g:if test="${personsAutocomplete && !personsAutocomplete?.isEmpty() }">
			<input class="userInput" type='text' id='inputUser${box}' value='' />
		</g:if>
		<g:else>
			<input class="userInput" type='text' id='inputUser${box}' value='' disabled/>
		</g:else>
</g:else>