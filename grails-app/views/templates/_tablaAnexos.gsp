<%@page import="ar.com.telecom.FaseMultiton"%>
<% 
	def anexos
	def anexoDiv = nombreDiv ?: "anexosDiv"
	if (fase){
		anexos = origen?.getAnexos(FaseMultiton.getFase(fase))?.sort{it.nombreArchivo}
	}
	
%>
<g:updateToken />
<g:if test="${anexos}">
	<div id="${anexoDiv}">
		<label class="formLabelAnexos" for='combobox'>Anexos</label>
		<table class="tablaAnexos">
			<tr>
				<th>Archivo</th>
				<th>Tipo de anexo</th>
				<th>Descripci&oacute;n</th>
				<g:if test="${editable}">
					<g:sortableColumn property="Eliminar" title=" " />
				</g:if>
			</tr>

			<g:each in="${anexos}" var="arch">
				<tr>
					<td><g:link action="verAnexo" target="_blank"
							params="[anexoId: arch.id]">
							${arch}
						</g:link>
					</td>
					<td>
						${arch?.tipoAnexo}
					</td>
					<td>
						${arch?.descripcion}
					</td>
					<g:if test="${editable}">
						<td><g:secureRemoteLink action="eliminarAnexo"
								params="[anexoId: arch.id, pedidoId: pedidoInstance.id, fase:fase, 'nombreDiv':anexoDiv, 'impacto':impacto]"
								update="${anexoDiv }">
								<img src="${resource(dir:'images',file:'delete.gif')}" width="10"
									height="10" />
							</g:secureRemoteLink>
						</td>
					</g:if>
				</tr>
			</g:each>
		</table>
	</div>
</g:if>
<g:else>
	<div>No se adjuntaron anexos</div>
</g:else>
