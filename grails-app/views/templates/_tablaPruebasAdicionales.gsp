<g:updateToken pedido="${pedidoInstance?.parent}"/>
 <table>
   <thead>
	     <tr>
	         <th>Pruebas adicionales</th>
	         <th>Consolidada</th>
	         <g:if test="${edita}">
		         <th></th>
		         <th></th>
	         </g:if>
	     </tr>
 	</thead> 
		<g:set var="counter" value="${0}" />
	<tbody>
	<g:if test="${pruebasOpcionalesSeleccionadas}">
		<g:each in="${pruebasOpcionalesSeleccionadas}" status="i" var="prueba">
 		 <tr id="fila${counter}" class="${(i % 2) == 0 ? 'odd' : 'even'}">
<%--		      <td><button class="formSeccionSubmitButton" id="modificarPrueba${prueba.tipoPrueba.id}" name="modificarPrueba${prueba.tipoPrueba.id}" onClick="editOtrasPruebasPopUp('${prueba.tipoPrueba.descripcion}',${prueba.seraConsolidada},${counter})">${prueba.tipoPrueba.descripcion}</button></td>--%>
		      <td>${prueba.tipoPrueba.descripcion}</td>
		      <td style="text-align: center;"><g:checkBox id="consolida${prueba.tipoPrueba.id}" name="consolida${prueba.tipoPrueba.id}" value="${prueba.seraConsolidada}" disabled="true"/></td>
		      <g:if test="${edita}">
		      	<td style="text-align: center;"><a href="javascript:editOtrasPruebasPopUp('${prueba.tipoPrueba.descripcion}',${prueba.seraConsolidada},${counter})" title="Modificar" class="modificarItem"></a></td>
		      	<td style="text-align: center;"><g:secureRemoteLink action="eliminarPrueba" update="pruebasAdicionales" params="${['pedidoId': pedidoInstance.id, 'pruebaId': prueba.id]}" title="Eliminar" id="${counter}" class="eliminarItem"></g:secureRemoteLink>  </td>
		      </g:if>
         </tr>
		<g:set var="counter" value="${counter + 1}" />
		</g:each>
	</g:if>
	<g:else>
		<tr><td colspan="4"  class="odd">No hay pruebas opcionales seleccionadas</td></tr>
	</g:else>
	</tbody>
</table>
