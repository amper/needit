<div class='tituloPrincipal'>
<g:if test="${pedido?.id}">
  <label>
    Pedido ${pedido.numero} <g:indicadorEstadoPedido pedido="${pedido}" />&nbsp;-&nbsp;${pedido?.titulo}
  </label>
</g:if>
<g:else>
  <label>Nuevo pedido</label>
</g:else>
</div> 