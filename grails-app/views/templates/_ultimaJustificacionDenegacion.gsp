<%@page import="ar.com.telecom.util.DateUtil"%>
<div class="seccionHide2">
	<g:set var="aprobacion"
		value="${pedidoInstance?.ultimaDenegacion(fase)}" />
	<g:if test="${aprobacion}">
		<p class="tituloSeccion">Justificaci&oacute;n de
			denegaci&oacute;n</p>
		<div class="formSeccion">
			<div class="pTres">
				<label class="formLabel">Fase</label>
				<p class="info">
					&nbsp;${aprobacion.fase}
				</p>
							
				<label class="formLabel">Fecha de denegaci&oacute;n</label>
				<p class="info">
					&nbsp;${DateUtil.toString(aprobacion.fecha, true)}
				</p>
			</div>				
			<div class="pTres"> 
				<label class="formLabel">Usuario que deneg&oacute;</label>
				<div class="boxUser">
					<g:render template="/templates/userBox"
						model="['person': aprobacion?.usuario, 'box': 'legajoGerenteUsuarioCabecera']" />
				</div>
			</div>
			<div class="pTres">
				<label class="formLabel">Justificaci&oacute;n</label>
				<p class="info">
					&nbsp;${aprobacion.justificacion}
				</p>
			</div>
		</div>
	</g:if>
</div>