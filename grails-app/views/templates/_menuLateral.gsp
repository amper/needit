<script src="${resource(dir: 'js', file: 'dw_scroll_c.js')}" type="text/javascript"></script>

<script type="text/javascript">
  var g_scroll_menu_lateral;

  function init_dw_Scroll_lateral() {
    g_scroll_menu_lateral = new dw_scrollObj('menu-lateral-scroll', 'menu-lateral-scroll-content');
    g_scroll_menu_lateral.buildScrollControls('menu-lateral-scroll-scrollbar', 'v', 'mouseover', true);
  }

  if ( dw_scrollObj.isSupported() ) {
    dw_Event.add( window, 'load', init_dw_Scroll_lateral);
  }
</script>
 
<div id="contenedorDcho">
  <div class="menuContenedor" id="ocultarPanel"><img src="${resource(dir:'images',file:'ocultarPanel.png')}" /></div>
  <div id="menu-lateral-main">
    <div id="menu-lateral-scroll">
      <div id="menu-lateral-scroll-content" class="seccionHideLateral">
        <g:render template="/templates/lateralPedidoPadre" model="['pedidoInstance':pedidoInstance]"></g:render>
        <g:render template="/templates/lateralReferentes" model="['pedidoInstance':pedidoInstance]"></g:render>
        <g:render template="/templates/lateralRecursos" model="['pedidoInstance':pedidoInstance]"></g:render>
        <g:render template="/templates/lateralPedidosHijos" model="['pedidoInstance':pedidoInstance]"></g:render>
        <g:render template="/templates/lateralProcesosSoporte" model="['pedidoInstance':pedidoInstance]"></g:render>
      </div>
    </div>
    
    <div id="menu-lateral-scroll-scrollbar"></div>
  </div>
</div>

<!-- fin contenedor derecho -->
<div id="contenedorDchoMin">
  <div class="menuContenedor" id="mostrarPanel"><img src="${resource(dir:'images',file:'abrirPanel.png')}" /></div>
  <img class="titlePanel" src="${resource(dir:'images',file:'menuLateral.png')}" />
</div>