<h5>Datos de usuario</h5>
<div style="height: 90px;">
<img id='fotoMiPerfil' class="fotoMiPerfil" onerror="javascript:UrlNotValid('fotoMiPerfil')" src="http://signomobile.telecom.com.ar/irj/go/km/docs/fotos/${user.legajo.replace('u','0')}.JPG" alt='Foto Usuario' />
<p>
	<strong>Nombre y Apellido: </strong>${user?.nombreApellido}<br>
	<strong>Legajo: </strong>${user?.legajo}<br>
	<strong>Email: </strong>${user?.email}<br>
	<strong>Gerencia: </strong>${user?.gerencia}<br>
	<strong>Direccion: </strong>${user?.direccion}<br>
<p>
</div>

<h5>Mis Roles/Grupos</h5>
<ul> 
	<g:each in="${roles}" var="r">
		<g:if test="${!r.equals('ROLE_SOLO_CONSULTA') }">
		
			<li>${r.toString()[5..(r.toString().size()-1)]}</li>
		</g:if>
	</g:each>
	
	<g:if test="${esGerente}">
		<li>GERENTE_USUARIO</li>
	</g:if>
	<g:if test="${esAprobadorImpacto}">
		<li>APROBADOR_IMPACTO</li>
	</g:if>
</ul>
<p>Para solicitar nuevos roles ingrese a <a href="http://tuid" target="blank" style="color:blue">TuID</a></p>

