<g:if test="${pedidoInstance}">
<p class="tituloContenedor">Referentes</p>
		<div class="contenidoContenedor">
				<g:userLine legajo="${pedidoInstance?.legajoUsuarioCreador}" pedido="${pedidoInstance}" controller="${params.controller }" etiqueta="Usuario Creador"/>
				<g:userLine legajo="${pedidoInstance?.legajoGerenteUsuario}" pedido="${pedidoInstance}" controller="${params.controller }" etiqueta="Gerente Usuario"/>
				<g:userLine legajo="${pedidoInstance?.legajoInterlocutorUsuario}" pedido="${pedidoInstance}" controller="${params.controller }" etiqueta="Interlocutor Usuario"/>
				<g:userLine legajo="${pedidoInstance?.legajoCoordinadorCambio}" pedido="${pedidoInstance}" controller="${params.controller }" etiqueta="Coordinador del Cambio"/>
				<g:userLine legajo="${pedidoInstance?.legajoUsuarioGestionDemanda}" pedido="${pedidoInstance}" controller="${params.controller }" etiqueta="Referente Gesti�n de la Demanda"/>
				<g:userLine legajo="${pedidoInstance?.legajoUsuarioAprobadorEconomico}" pedido="${pedidoInstance}" controller="${params.controller }" etiqueta="Aprobador de Impacto"/>
		</div>
</g:if> 