<g:updateToken />
<script type="text/javascript">
jQuery(document).ready(
	function($) {
		$("#comboboxFase").multiselect({
			checkAllText: 'Todos',
			uncheckAllText: 'Ninguno',
			noneSelectedText: 'Seleccione una Fase',
			selectedText: function(numChecked, numTotal, checkedItems){
				return numChecked + ' de ' + numTotal + ' seleccionadas';
			},
			create : function(event, ui) {
				$(this).multiselect("uncheckAll");
				var contentDiv = $(this).multiselect().parent().find("div")[0];
				contentDiv.style.width = 350;
				if(fases != null) {
					$(this).multiselect("widget").find(":checkbox").each(function(i, ele) {
						if($.inArray(parseInt(ele.value), fases) != -1) {
							this.click();
						}
					});
				}
			}
		}).multiselectfilter({
			label: 'Buscar',
			width: 80
		});
	}
);
</script>
 
<g:if test="${listadoFases && !listadoFases?.isEmpty()}">
<div>
	<g:if test="${show}">
		<label>Fase</label><br>
	</g:if>
	<g:select id="comboboxFase" name="fase" from="${listadoFases}" optionKey="id" />
</div>
</g:if>