<p class='tituloSeccion'>Informaci&oacute;n b&aacute;sica del pedido</p>
<div class='formSeccion'>
	<div class="pCompleto">
		<label class='formLabelHistorial'>Pedido ${ pedidoInstance?.numero }</label>
		<g:if test="${pedidoInstance?.isCancelado()}">
			<a title="${pedidoInstance?.descripcionStatus}" class="pedido_cancelado" href='#'>&nbsp;</a>
		</g:if>
		<g:if test="${pedidoInstance?.isSuspendido()}">
			<a title="${pedidoInstance?.descripcionStatus}" class="pedido_suspendido" href='#'>&nbsp;</a>
		</g:if>
		<g:if test="${pedidoInstance?.isCerrado()}">
			<a title="${pedidoInstance?.descripcionStatus}" class="pedido_cerrado" href='#'>&nbsp;</a>
		</g:if>
		
		<label class='tituloPrincipal'>${ pedidoInstance?.titulo }</label>
	</div>
</div>
 