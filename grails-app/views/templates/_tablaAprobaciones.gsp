<g:if test="${aprobacionesList && !aprobacionesList.isEmpty()}">					
<p class="tituloSeccion">Aprobadores</p>
	<div class="formSeccion">
		<div class="list">
			<table>
				<thead>
					<tr>
						<g:sortableColumn property="grupo" title="&Aacute;rea aprobadora" params="${params}"/>
						<g:sortableColumn property="usuario" title="Usuario" params="${params}"/>
						<g:sortableColumn property="estado" title="Estado" params="${params}"/>
						<g:sortableColumn property="fecha" title="Fecha Aprobaci&oacute;n" params="${params}" />
						<g:sortableColumn property="justificacion"	title="Justificaci&oacute;n" params="${params}" />												
					</tr>
				</thead>
				<tbody> 
						<g:each in="${aprobacionesList}" status="i" var="aprobacionInstance">
							<g:set scope="page" var="aprobacion" value="${aprobacionInstance}" />
								<tr class="${(i % 2) == 0 ? 'odd' : 'even'}" style="cursor: pointer;">
									<td>
										${etiqueta}
									</td>
									<td>
										<g:userDescription legajo="${aprobacion?.usuario}"	pedido="${aprobacion?.pedido?.id}" />
									</td>
									<td>
										${aprobacionInstance?.estadoDescripcion()}
									</td>
									<td>
										<g:formatDate date="${aprobacionInstance?.fechaCumplimiento}" type="datetime" style="MEDIUM"/>
									</td>
									<td>
										${aprobacionInstance?.justificacion}
									</td>																
								</tr>
						</g:each>
				</tbody>
			</table>
		</div>
	</div>
</g:if>