<g:if test="${pedidoInstance}">
<p class="tituloContenedor">Accesos</p>
		<div class="contenidoContenedor">
		<a href="javascript:openPopUp('${createLink(controller: 'historial' , action: 'list', id: pedidoInstance?.id, params: [pedidoId: pedidoInstance?.id] )}')">
				<div class='itemContenedor' style="cursor:pointer;">
					<img src="${resource(dir:'images',file:'iconHistorial.png')}" class="imgMin"/>
					<p class='itemDescripcion' style='font-style:italic;'>Consulta el HISTORIAL de modificaciones</p>
				</div>
			</a>
			<a href="javascript:openPopUp('${createLink(controller: 'anexo' , action: 'list', id: pedidoInstance?.id, params: [pedidoId: pedidoInstance?.id] )}')">
				<div class='itemContenedor' style="cursor:pointer;">
					<img src="${resource(dir:'images',file:'iconAnexo.png')}" class="imgMin"/>
					<p class='itemDescripcion' style='font-style:italic;'>Consulta el listado de ANEXOS del pedido</p>
				</div>
				</a>
		</div>
</g:if> 