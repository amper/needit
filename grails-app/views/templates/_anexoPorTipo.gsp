<%@page import="ar.com.telecom.pcs.entities.AnexoListaBlanca"%>

<g:javascript library="anexoPorTipo" />
<g:updateToken />
<!-- modal popup anexos -->
<div id="spinner"></div>
 
<g:uploadForm enctype="multipart/form-data" id="formAnexo" name="formAnexo" class="formAnexo" action="guardarAnexo" useToken="true">

		<g:hiddenField name="pedidoId" value="${pedidoInstance.id}" />
		<g:hiddenField name="impacto" value="${impacto}" />
		<g:hiddenField id="anexoFrom" name="anexoFrom" value="${params.anexoFrom}" />
		<g:hiddenField name="tabSeleccionado" id="tabSeleccionado" value="${tabSeleccionado}"/>
		<%
			extensiones = AnexoListaBlanca?.list().extension.collect{ ext-> "'" + ext + "'"}
		%>

		<g:hiddenField name="controllerBackSuspension" id="controllerBackSuspension"/>
		<script>
			var ctrl = document.getElementById('controllerEnNavegacion');
			if (ctrl){
				document.getElementById('controllerBackSuspension').value = ctrl.value;
			}
		</script>
		
		<div class="pCompleto">
			<label for='anexos' class="formLabel">Archivo</label> <input class="formInput" type="file" id="anexoArchivo" name="anexoArchivo" />
		</div>

		<div class="separatorBotonera"></div>

		<div class="pCompleto">
				<label for='inputDescripcionAnexo' class="formLabel">Descripci&oacute;n</label>
				<g:textArea name="descripcionAnexo" id='inputDescripcionAnexo'></g:textArea>
		</div>

		<div class="separatorBotonera"></div>

		<div class="separatorBotonera"></div>

		<div class="pCompleto">
				<label for='comboboxTipoAnexo' class="formLabel">Tipo de Anexo</label>
				<g:if test="${tiposDeAnexo?.size() == 1}">
						<g:set scope="page" var="tipoAnexo" value="${tiposDeAnexo.first()}" />
						<p class="infoNoBorder">
								${tipoAnexo.descripcion}
						</p>
						<div style="display: none;">
							<g:select id="comboboxTipoAnexo" name="tipoAnexo.id" from="${tiposDeAnexo}" optionKey="id" value="${tipoAnexo.id}" />
						</div>
						<g:if test="${tipoAnexo?.descripcion.equalsIgnoreCase('Global')}">
							<g:hiddenField name="global" value="true" id="global" />
						</g:if>
				</g:if>
				<g:else>
					<g:select id="comboboxTipoAnexo" name="tipoAnexo.id" from="${tiposDeAnexo}" optionKey="id" value="" noSelection="['null': '']" style="display: none;" />
				</g:else>
		</div>

		<div class="separatorBotonera"></div>
		
		<div align="center" class="formSeccionButton">
				<button id="cerrarAnexo" class="formSeccionSubmitButtonAnexo">Cerrar</button>
				<button id="aceptarAnexo" class="formSeccionSubmitButtonAnexo" onclick="javascript:validarAnexo(${extensiones});">Aceptar</button>
		</div>

</g:uploadForm>

<script type="text/javascript">
jQuery(document).ready(
	function($) {
		$( "#comboboxTipoAnexo" ).combobox();	
		
		$("#cerrarAnexo").click(function() {
			$("#dialog-anexos").dialog("close");
			return false;
		});	
	});
</script>