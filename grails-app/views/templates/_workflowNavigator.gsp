<%@page import="ar.com.telecom.FaseMultiton"%>

<script src="${resource(dir: 'js', file: 'dw_scroll_c.js')}" type="text/javascript"></script>

<script type="text/javascript">
  var g_scroll_test;

  function init_dw_Scroll() {
    g_scroll_test = new dw_scrollObj('workflow-navigator-fases', 'workflow-navigator-fases-content');
    g_scroll_test.buildScrollControls('workflow-navigator-scrollbar', 'h', 'mouseover', true);
  }

  if ( dw_scrollObj.isSupported() ) {
    dw_Util.writeStyleSheet('${resource(dir: 'css', file: 'workflow-navigator.css')}');
    dw_Event.add( window, 'load', init_dw_Scroll);
  }
 
  function mostrarMenuWf(id, mostrar){
    if(mostrar){
      $(id).style.display='block';
    } else {
      $(id).style.display='none';
    }
  }

  function navegarFase(controllerNombre) {
    document.getElementById('controllerNombre').value = controllerNombre;
    document.getElementById('NavegarFase').click();
  }

  function guardaCambioPedido(controllerNombre) {
    document.getElementById('controllerNombre').value = controllerNombre;
    if (document.getElementById('GuardarCambio')){
    	document.getElementById('GuardarCambio').click();
    }else{
    	document.getElementById('NavegarFase').click();
    }
  }

  function submitNavegarFase(controllerNombre, usrlog, codigoFase) {
    switch (codigoFase) {
      case "${FaseMultiton.VALIDACION_PEDIDO}":
        var legajoGerente = document.getElementById('inputlegajoGerenteUsuario');
        cambioUsuario(legajoGerente, usrlog, '${message(code: 'msg.confirm.change.gerente', default: '')}', controllerNombre);
        break;
      case "${FaseMultiton.APROBACION_PEDIDO}":
        var legajoInterlocutor = document.getElementById('inputlegajoInterlocutorUsuario');
        cambioUsuario(legajoInterlocutor, usrlog, '${message(code: 'msg.confirm.change.interlocutor', default: '')}', controllerNombre);
        break;
      default:
    	navegarFase(controllerNombre);
    }
  }

  function cambioUsuario(legajo, usuarioLogueado, codigoMensajeUsuarioCambiado, controllerNombre) {
    // var page = "${params.action}";
    // no lo considera
    if (legajo && (legajo.value !== usuarioLogueado) && document.getElementById('vista') && document.getElementById('vista').value == 'index'){
       var continuar = confirm(codigoMensajeUsuarioCambiado);
       if (!continuar) {
         return
       }
    } 
    guardaCambioPedido(controllerNombre);
    // a futuro, habr�a que chequear solamente si cambi� el gerente o interlocutor usuario
    // navegarFase(controllerNombre);
  }

  function openPopUp(url){
      window.open(url,"NeedIT","menubar=1,resizable=1,scrollbars=yes,menubar=no,width=930,height=350");
  }
</script>

<div id="changeInterloc" title="Atenci&oacute;n" style="display: none;"><p>Dejara de ser interlocutor con lo cual no podra aprobar, esta seguro?</p></div>
<g:actionSubmit id="interChange" value="interChange" style="display:none" onclick="${changeInterloc.dialog("open");}" />

<g:if test="${oPedido}">

<div id="workflow-navigator">

  <div id="workflow-navigator-selector">
    <ul>
      <li id="workflow-navigator-selector-padre" <g:if test="${!esHijo}">class="selectedPadre"</g:if>><a href="${createLink(id: oPedido.id, params: [pedidoId: oPedido.id], controller: oPedido?.faseActual?.controllerNombre)}">PEDIDO: ${oPedido.numero}</a></li>
    <g:if test="${oPedido.pedidosHijos}">
      <g:each in="${oPedido.pedidosHijos}" var="item" status="index"><li <g:if test="${esHijo && (impacto.toString() == item.id.toString() || params?.impacto.toString() == item.id.toString())}">class="selected"</g:if>><a href="${createLink(id: oPedido.id, params: [pedidoId: oPedido.id, impacto: item.id], controller: item?.faseActual?.controllerNombre)}">${item.numero.substring(item.numero.indexOf('.') + 1, item.numero.length())} - ${item?.tipoImpacto.codigo} - ${item.sistema?item.sistema:item.areaSoporte}</a></li></g:each>
    </g:if>
    </ul>
  </div>

  <div id="workflow-navigator-fases">
    <div id="workflow-navigator-fases-content">

      <table cellspacing="0" cellpadding="0" id="uno">
        <tr>

  <g:if test="${fases}">
    <g:set var="logueado" value="${org.springframework.security.core.context.SecurityContextHolder.getContext().getAuthentication().getPrincipal().getUsername()}" />

    <g:if test="${esHijo}">

      <g:set var="ultimaFasePadre" value="N" />
      <g:set var="classFasePadre" value="realizada"/>
      <g:set var="ultimaFaseHija" value="N" />
      <g:set var="classFaseHija" value="realizada"/>

      <g:each in="${mapaFasesHijos}" var="mapaFase">
        <g:set var="fasePadre" value="${mapaFase.key}"/>
        <g:set var="fasesHijas" value="${mapaFase.value}"/>

        <g:if test="${fasePadre.equals(faseActual?.fasePadre)}">
          <g:set var="ultimaFasePadre" value="S" />
        </g:if>

        <g:if test="${fasePadre.equals(faseActual?.fasePadre) && ultimaFasePadre.equals('S')}">
          <g:set var="classFasePadre" value="actual"/>
        </g:if>

        <g:if test="${ultimaFaseHija.equals('S')}">
          <g:set var="classFasePadre" value="inactiva"/>
        </g:if>


<%--<g:render template="/templates/workflowNavigatorSubfases" model="['fases': fasesHijas, 'faseActual': faseActual, 'esHijo': esHijo]"></g:render>--%>
        <g:if test="${fasesHijas}">

          <td id="workflow-fase-compuesta" class="compuesta" title="${fasePadre.tooltip}">
            <div id="workflow-fase-compuesta-content">
              <span id="workflow-fase-compuesta-title"><g:if test="${fasesHijas.collect { faseHija -> faseHija.controllerNombre }.contains(params.controller)}">${fasePadre?.descripcion}</g:if><g:else>${fasePadre.descripcion}</g:else><br /></span>
              <table id="workflow-fase-compuesta-hijos">
                <tr>

          <g:each in="${fasesHijas}" var="faseHija">

            <g:if test="${faseActual?.id == faseHija?.id}">
              <g:set var="ultimaFaseHija" value="S" />
            </g:if>
    
            <g:if test="${ultimaFaseHija.equals('S')}">
              <g:set var="classFaseHija" value="inactiva"/>
            </g:if>

            <g:if test="${faseActual?.id == faseHija?.id && ultimaFaseHija.equals('S')}">
              <g:set var="classFaseHija" value="actual"/>
            </g:if>

                  <td id="workflow-fase-hija" class="${classFaseHija}<g:if test="${params.controller.equalsIgnoreCase(faseHija.controllerNombre)}">-selected</g:if>" title="${faseHija.tooltip}">
                    <div id="fase${faseHija.id}" <g:if test="${params.controller.equalsIgnoreCase(faseHija.controllerNombre)}">class="fase-selected"</g:if>>
                    <img src="${resource(dir: 'css/images', file: 'workflow-spacer-1x1.gif')}" width="79" height="1" /><br />
            <g:if test="${classFaseHija.equalsIgnoreCase('realizada') || classFaseHija.equalsIgnoreCase('actual')}">
                    <a href="javascript:submitNavegarFase('${faseHija.controllerNombre}','${logueado}','${faseHija.codigoFase}');">${faseHija.descripcion}</a>
            </g:if><g:else>
                    ${faseHija.descripcion}
            </g:else>
                    <br /><img src="${resource(dir: 'css/images', file: 'workflow-spacer-1x1.gif')}" width="79" height="1" />
                    </div>
                  </td>

          </g:each>

                </tr>            
              </table>
            </div>
          </td>
        </g:if>

        <g:else>
          <td id="workflow-fase" class="${classFasePadre}<g:if test="${params.controller.equalsIgnoreCase(fasePadre.controllerNombre)}">-selected</g:if>" title="${fasePadre.tooltip}">
            <div id="fase${fasePadre.id}">
              <img src="${resource(dir: 'css/images', file: 'workflow-spacer-1x1.gif')}" width="77" height="1" /><br />
          <g:if test="${ultimaFasePadre.equalsIgnoreCase('N')}">
              <a href="javascript:submitNavegarFase('${fasePadre.controllerNombre}','${logueado}','${fasePadre.codigoFase}');">${fasePadre.descripcion}</a>
          </g:if><g:else>
              ${fasePadre.descripcion}
          </g:else>
              <br /><img src="${resource(dir: 'css/images', file: 'workflow-spacer-1x1.gif')}" width="77" height="1" />
            </div>
          </td>
        </g:else>

      </g:each>
    </g:if>

    <g:else>
<g:render template="/templates/workflowNavigatorSubfases" model="['fases': fases, 'faseActual': faseActual]"></g:render>
    </g:else>
  </g:if>

        </tr>
      </table>
    </div>
  </div>
</div>

<div id="workflow-navigator-scrollbar"></div>

</g:if>

<g:hiddenField id="accionPopup" name="accionPopup" />
