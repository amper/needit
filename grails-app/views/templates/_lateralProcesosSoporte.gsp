<%@page import="ar.com.telecom.pedido.especificacionImpacto.Impacto"%>

<g:javascript library="suspension" />
<g:javascript library="reasignacion" />

<g:if test="${pedidoInstance}">
	<p class="tituloContenedor">Procesos de Soporte</p>
	<div class="contenidoContenedor">
		<g:if test="${pedidoInstance.muestraLinkRealesIncurridos()}">
			<g:link controller="realesIncurridos" id="${pedidoInstance.id}" params="['pedidoId':pedidoInstance.id]">
				<div class='itemContenedor' style="cursor:pointer;">
					<p class='itemDescripcion' style='font-style:italic;'>Reales Incurridos</p>
				</div>
			</g:link>
		</g:if>
		<g:if test="${pedidoInstance.puedeReasignar(usuarioLogueado)}">
			<div class='itemContenedor' style="cursor:pointer;">
				<p class='itemDescripcion' style='font-style:italic;'> <a id="btnReasignarRol" onClick="popReasignar('${pedidoInstance.id}')">Reasignar rol</a></p>
			</div>
		</g:if>
		<g:if test="${pedidoInstance.puedeSuspender(usuarioLogueado)}">
			<div class='itemContenedor' style="cursor:pointer;">
				<p class='itemDescripcion' style='font-style:italic;'> <a id="btnSuspenderPedido" onClick="popSuspenderPedido('${pedidoInstance.id}')">Suspender pedido</a></p>
			</div>
		</g:if>
		<g:if test="${pedidoInstance.puedeReanudar(usuarioLogueado)}">
			<div class='itemContenedor' style="cursor:pointer;">
				<p class='itemDescripcion' style='font-style:italic;'> <a id="btnReanudarPedido" onClick="popSuspenderPedido('${pedidoInstance.id}')">Reanudar pedido</a></p>
			</div>
		</g:if>
		<g:if test="${pedidoInstance.puedeReplanificar(usuarioLogueado) && false}">
			<a href="javascript:openPopUp('${createLink(controller: 'replanificacion' , action: 'index', id: pedidoInstance?.id, params: [pedidoId: pedidoInstance?.id, impacto: Impacto.IMPACTO_CONSOLIDADO] )}')">
				<div class='itemContenedor' style="cursor:pointer;">
					<p class='itemDescripcion' style='font-style:italic;'>Iniciar Replanificaci&oacute;n</a></p>
				</div>
			</a>
		</g:if>
	</div>
</g:if> 