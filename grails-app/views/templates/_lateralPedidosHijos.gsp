<g:if test="${pedidoInstance}">
<p class="tituloContenedor">Gestiones derivadas</p>
		<div class="contenidoContenedor">
				<g:each in="${pedidoInstance?.pedidosHijos}" var="hijo" status="i">
					<g:userPedidoHijo padre="${pedidoInstance}" hijo="${hijo}" index="${i+1}"/>
				</g:each>
		</div>
</g:if> 