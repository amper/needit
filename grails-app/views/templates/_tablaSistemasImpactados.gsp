 <g:updateToken pedido="${pedidoInstance}"/>
 
 <div class="list">
 <table>
 <thead>
     <tr>
         <th>Sistemas</th>
         <th>Tipo Impacto</th>
         <th>Grupo responsable</th>
         <th>Usuario</th>
         <g:if test="${ visible }"><td></td></g:if>
     </tr> 
</thead>
<g:if test="${sistemasImpactadosSeleccionados}">
<tbody>
		<g:set var="counter" value="${0}" />
		<g:each in="${sistemasImpactadosSeleccionados}" status="i" var="sistema">
 		<tr class="${(i % 2) == 0 ? 'odd' : 'even'}">
		      <td>${sistema.sistema?.descripcion} </td>
		      <td>${sistema.tipoImpacto?.descripcion} </td>
		      <td>${sistema.grupoReferente} </td>
		      <td><g:userDescription legajo="${sistema.usuarioResponsable}"/> </td>
<%--		      <td>${sistema.usuarioResponsable} </td>--%>
		      <g:if test="${ visible }">
		      	<td>
			      	<g:secureRemoteLink title="Eliminar" class="eliminarItem" update="sistemasImpactados" controller="especificacionCambio" 
					params="${['pedidoId': pedidoInstance.id, 'sistemaId': sistema.id]}" id="${counter}"
					action="eliminarSistema"></g:secureRemoteLink>  
		      	</td>
		      </g:if>
         </tr>
		<g:set var="counter" value="${counter + 1}" />
		</g:each>
</tbody>
</g:if>
<g:else>
	<tr><td colspan="4"  class="odd">No hay sistemas impactados</td></tr>
</g:else>
</table>
</div>

