
<%@ page import="ar.com.telecom.pcs.entities.TipoPrueba" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="layout" content="main" />
        <g:set var="entityName" value="${message(code: 'tipoPrueba.label', default: 'TipoPrueba')}" />
        <title><g:message code="default.list.label" args="[entityName]" /></title>
    </head>
    <body>
        <div class="nav">
            <span class="menuButton"><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></span>
            <span class="menuButton"><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></span>
        </div>
        <div class="body">
            <h1><g:message code="default.list.label" args="[entityName]" /></h1>
            <g:if test="${flash.message}">
            <div class="message">${flash.message}</div>
            </g:if>
            <div class="list">
                <table>
                    <thead>
                        <tr>
                        
                            <g:sortableColumn property="id" title="${message(code: 'tipoPrueba.id.label', default: 'Id')}" />
                        
                            <g:sortableColumn property="descripcion" title="${message(code: 'tipoPrueba.descripcion.label', default: 'Descripcion')}" />
                        
                            <g:sortableColumn property="opcional" title="${message(code: 'tipoPrueba.opcional.label', default: 'Opcional')}" />
                        
                        </tr>
                    </thead>
                    <tbody>
                    <g:each in="${tipoPruebaInstanceList}" status="i" var="tipoPruebaInstance">
                        <tr class="${(i % 2) == 0 ? 'odd' : 'even'}">
                        
                            <td><g:link action="show" id="${tipoPruebaInstance.id}">${fieldValue(bean: tipoPruebaInstance, field: "id")}</g:link></td>
                        
                            <td>${fieldValue(bean: tipoPruebaInstance, field: "descripcion")}</td>
                        
                            <td><g:formatBoolean boolean="${tipoPruebaInstance.opcional}" /></td>
                        
                        </tr>
                    </g:each>
                    </tbody>
                </table>
            </div>
            <div class="paginateButtons">
                <g:paginate total="${tipoPruebaInstanceTotal}" />
            </div>
        </div>
    </body>
</html>
