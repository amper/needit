<html>
<head>
  <meta name="layout" content="layoutMobile" />
  <script type="text/javascript">
    function reload() {
      window.location.reload(); 
    }

    function preSubmit(id) {
      var sText = $F('justificacionAprobacion');

      if (id == 'denegarMasivo' && sText == '') {
        document.getElementById('info').style.display = '';
        return false;
      }

      $('tipoAccion').value = id;
      $('inboxMobile').submit();
    }
 
    function selectCombos(id) {
      var combos = $$('.checkAprobacion');

      if ($(id).checked) {
        combos.each(function(e,i) {
          $(e.id).checked = true;
        });
      } else {
        combos.each(function(e,i) {
          $(e.id).checked = false;
        });
      }
      habilitarAprobar();
    }

    function unSelectCombos(id) {
      if ($('checkSeleccionarTodos').checked){
        $(id).checked = false;
      }
      $('checkSeleccionarTodos').checked = false;
      habilitarAprobar();
    }

    function habilitarAprobar() {
      var combos = $$('.checkAprobacion');
      var check = false;

      combos.each(function(e,i){
    	if ($(e.id).checked) {
          check = true;
        }
      });

      if (check) {
        $('mobile-body-content-list').show();
      } else {
    	$('mobile-body-content-list').hide();
      }
    }
  </script>
</head>
<body>
<web:isBlackberry>
        <tmpl:/inboxMobile/listaPedidos aprobacionLista="${aprobacionLista}" aprobacionTotal="${aprobacionTotal}" aprobacionUsuario="${aprobacionUsuario}" />
</web:isBlackberry>
</body>
</html>