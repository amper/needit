<g:form name="inboxMobile" action="accionMasiva" method="POST">
      <g:hiddenField name="offset" value="${params.offset}"/>
      <g:hiddenField name="max" value="${params.max}"/>
      <g:hiddenField name="sort" value="${params.sort}"/>
      <g:hiddenField name="order" value="${params.order}"/>
      <g:hiddenField name="tipoAccion" value=""/>
      <div id="mobile-body-content-header"><div style="float: left"><g:message code="inbox.mobile.list.title" /></div><div style="float: right; width: 16px"><a href="javascript:reload();"><img src="${resource(dir:'mobile',file:'mobile-btn-refresh.png')}" width="16" height="16"></a></div></div>
      <div id="mobile-body-content-list" style="display: none;">
        <table id="pedido-detalle">
          <tr>
            <td><g:message code="inbox.mobile.item.label.observaciones" /></td>
            <td><textarea rows="2" cols="25" name="justificacionAprobacion" id="justificacionAprobacion"></textarea></td>
          </tr>
          <tr>
            <td colspan="2"><span id="info" style="display: none"><g:message code="inbox.mobile.actions.info" /></span></td>
          </tr>
        </table>
        <table id="pedido-acciones">
          <tr>
            <td><a id="denegarMasivo" href="#" onclick="javascript:preSubmit(this.id);"><img src="${resource(dir:'mobile',file:'mobile-icon-action-denied.png')}" />&nbsp;<g:message code="inbox.mobile.actions.rechazar.masivo" /></a></td>
            <td><a id="aprobarMasivo" href="#" onclick="javascript:preSubmit(this.id);"><img src="${resource(dir:'mobile',file:'mobile-icon-action-accept.png')}" />&nbsp;<g:message code="inbox.mobile.actions.aprobar.masivo" /></a></td>
          </tr>
        </table>
      </div> 
      <div id="mobile-body-content-list">
        <g:if test="${!params?.inbox?.isEmpty() && flash.message}"><div class="errors">${flash.message}</div></g:if>
        <table cellspacing="1" cellpadding="1">
          <thead>
            <tr>
              <th><g:checkBox id="checkSeleccionarTodos" name="todosInbox" onClick="selectCombos(this.id);" /></th>
              <g:sortableColumn property="pedido.id" title="${message(code: 'inbox.mobile.list.column.id')}" params="${params}" class="columna1"/>
              <g:sortableColumn property="pedido.titulo" title="${message(code: 'inbox.mobile.list.column.titulo')}" params="${params}" class="columna2"/>
            </tr>
          </thead>
          <tbody>
<g:if test="${!aprobacionLista.isEmpty()}">
  <g:each in="${aprobacionLista}" status="i" var="aprobacionItem">
    <g:set scope="page" var="aprobacion" value="${aprobacionItem}" />
    <g:set scope="page" var="pedido" value="${aprobacionItem?.pedido}" />
            <tr>
              <td><g:checkBox id="${aprobacion?.id}" name="inbox" value="${aprobacion?.id}" checked="${params?.inbox?.contains(aprobacion?.id?.toString())}" class="checkAprobacion" onclick="javascript:unSelectCombos(this.id);" /></td>
              <td class="columna1"><a href="<g:createLink controller="inboxMobile" action="acciones" params="[id: aprobacion.id]"/>"><g:if test="${!pedido.esHijo()}">${pedido?.id}</g:if><g:else>${pedido.numero}</g:else></a></td>
              <td class="columna2"><a href="<g:createLink controller="inboxMobile" action="acciones" params="[id: aprobacion.id]"/>">${pedido?.titulo}</a></td>
            </tr>
  </g:each>
	<tr>
		<td colspan="3"><div class="paginateButtons"><g:paginate total="${aprobacionTotal ? aprobacionTotal : 0}"/></div></td>
	</tr>
</g:if>
<g:else>
            <tr>
              <td colspan="3" style="text-align: center; font-style: italic;"><g:message code="inbox.mobile.list.empty" /></td>
            </tr>
</g:else>
          </tbody>
        </table>
      </div>
</g:form>