      <div id="mobile-body-content-header"><div style="float: left"><g:message code="inbox.mobile.item.title" args="[aprobacion.pedido.id]"/></div><div style="float: right; width: 16px"><a href="<g:createLink controller="inboxMobile" action="index" />"><img src="${resource(dir:'mobile',file:'mobile-btn-back.png')}" width="16" height="14" /></a></div></div>
      <div id="mobile-body-content-list">
        <table id="pedido-detalle">
          <tr>
            <td><g:message code="inbox.mobile.item.label.titulo" /></td>
            <td>${aprobacion.pedido.titulo}</td>
          </tr>
          <tr>
            <td><g:message code="inbox.mobile.item.label.fase" /></td>
            <td>${aprobacion.pedido.faseActual}</td>
          </tr>
          <tr>
            <td><g:message code="inbox.mobile.item.label.sistema" /></td>
            <td><g:if test="${aprobacion.pedido.esHijo()}">${aprobacion.pedido?.tipoImpacto}</g:if><g:else>N/A</g:else></td>
          </tr>
          <tr>
            <td><g:message code="inbox.mobile.item.label.desde" /></td>
            <td><g:formatDate date="${aprobacion.fecha}" type="datetime" style="MEDIUM"/></td>
          </tr>
          <tr>
            <td><g:message code="inbox.mobile.item.label.observaciones" /></td>
            <td><textarea rows="2" cols="25" id="justificacionAprobacion">${aprobacion.justificacion}</textarea></td>
          </tr>
          <tr> 
            <td colspan="2"><span id="info" style="display: none"><g:message code="inbox.mobile.actions.info" /></span></td>
          </tr>
        </table>
        <table id="pedido-acciones">
          <tr>
            <td><a id="denegarPedido" href="#" onclick="javascript:submitAction(this.id);"><img src="${resource(dir:'mobile',file:'mobile-icon-action-denied.png')}" />&nbsp;<g:message code="inbox.mobile.actions.rechazar" /></a></td>
            <td><a id="aprobarPedido" href="#" onclick="javascript:submitAction(this.id);"><img src="${resource(dir:'mobile',file:'mobile-icon-action-accept.png')}" />&nbsp;<g:message code="inbox.mobile.actions.aprobar" /></a></td>
          </tr>
        </table>
      </div>

<script type="text/javascript">
  function submitAction(id) {
    var sUrl = '<g:createLink controller="inboxMobile" action="ACTION" params="[pedidoId: aprobacion.pedido.id, justificacionAprobacion: '']"/>';
    var sText = document.getElementById('justificacionAprobacion').value;
  
    if (id == 'denegarPedido' && sText == '') {
      document.getElementById('info').style.display = '';
      return false;
    }
  
    sUrl = sUrl.replace('ACTION', id);
    sUrl += sText;
    window.location = sUrl; 
  }
</script>