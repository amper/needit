<%@page import="ar.com.telecom.pcs.entities.RolAplicacion"%>
	
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta name="layout" content="mainApp" />
<g:javascript library="aprobarPedido" />
<g:javascript library="boxUser" />

</head>
<body>

	<div class="contenedorTitulo">
		<g:titulo pedido="${pedidoInstance}" />
		<g:workflow pedidoId="${pedidoInstance?.id}"/>
	</div>
 
		<div id="contenedorIzq">
				<g:if test="${flash.message}">
						<div class="message">
								${flash.message}
						</div>
				</g:if>
				<g:hasErrors bean="${pedidoInstance}">
						<div class="errors">
								<g:renderErrors bean="${pedidoInstance}" as="list" />
						</div>
				</g:hasErrors>
				<g:form class="formGeneral" useToken="true">
						<g:hiddenField name="id" value="${pedidoInstance?.id}" />
						
						<div class="seccionHide">
						<g:cabeceraPedido pedido="${pedidoInstance}" />
						<g:render template="/templates/workflowHidden"/>
						
						<p class="tituloSeccion">Interlocutor Usuario y Aprobaci&oacute;n</p>
						<div class="formSeccion">
								<div class="pDos">
										<label class="formLabel2 validate" for='inputIntUsuario'>Interlocutor Usuario</label>
										<div id="legajoInterlocutorUsuario" class="boxUser">
												<g:render template="/templates/userBox" model="['person':pedidoInstance?.legajoInterlocutorUsuario, 'box':'legajoInterlocutorUsuario', 'personsAutocomplete': personsAutocompleteInt]"></g:render>
										</div>
										<g:secureRemoteLink title="Eliminar" class="limpiarBox" update="legajoInterlocutorUsuario" controller="validarPedido" 
										params="['gerente': pedidoInstance?.legajoGerenteUsuario, 'username': '', 'box':'legajoInterlocutorUsuario', 'pedidoId': pedidoInstance?.id, 'id':pedidoInstance?.id]" id="${RolAplicacion.INTERLOCUTOR_USUARIO}"
										action="completarUsuario"></g:secureRemoteLink>
								</div>
								<div class="pDos">
										<label for="justificacionAprobacion" class="formLabel">Justificaci&oacute;n</label>
										<g:set var="logueado" value="${org.springframework.security.core.context.SecurityContextHolder.getContext().getAuthentication().getPrincipal().getUsername()}" />
										<g:if test="${logueado?.equalsIgnoreCase(pedidoInstance?.legajoGerenteUsuario)}">
												<g:textArea name="justificacionAprobacion" value="${pedidoInstance?.getJustificacionGU()}"></g:textArea>
										</g:if>
										<g:else>
											<p class="info">${pedidoInstance?.getJustificacionGU()}</p>
										</g:else>
								</div>
						</div>
<%--						<g:if test="${pedidoInstance?.getJustificacionIU()}">--%>
<%--								<p class="tituloSeccion">Justificaci&oacute;n de desaprobaci&oacute;n Interlocutor Usuario</p>--%>
<%--								<div class="formSeccion">--%>
<%--										${pedidoInstance?.getJustificacionIU()}--%>
<%--								</div>--%>
<%--						</g:if>--%>
							<g:render template="/templates/ultimaJustificacionDenegacion"
						     			model="['pedidoInstance': pedidoInstance, 'fase': pedidoInstance.faseValidacionPedido()]"></g:render>

							<g:render template="/templates/tablaAprobaciones" model="['pedidoInstance': pedidoInstance, 'aprobacionesList': aprobacionesList, 'etiqueta': 'Gerente Usuario']"></g:render>

						</div>
						<div id="message"></div>

						<div class="formSeccionButton" style="margin-top: 20px;">
								<g:actionSubmit class="formSeccionSubmitButton" id="GuardarCambio" action="guardarCambio" value="Guardar Cambio sesion" style="display:none" />
								<g:if test="${pedidoInstance?.faseActual?.controllerNombre.equals(params.controller) || !pedidoInstance.faseActual}">
									<g:actionSubmit class="formSeccionSubmitButton" id="Simular" action="simularValidacion" value="Simular" />
									<g:actionSubmit class="formSeccionSubmitButton" id="Denegar" action="denegarPedido" value="Denegar" />
									<g:actionSubmit class="formSeccionSubmitButton" id="Guardar" action="grabarFormulario" value="Guardar" />										
									<g:actionSubmit class="formSeccionSubmitButton" id="Aprobar" action="aprobarPedido" value="Aprobar" />
								
								</g:if>
						</div>

				</g:form>
			


		</div>
		<!-- fin contenedor izq -->
		
		<!-- Lateral Derecho -->
	<g:render template="/templates/menuLateral" model="['pedidoInstance':pedidoInstance]"/>
	<!-- Fin Lateral Derecho -->

		<!-- 
	************
	************
	MODALS POPUP
	************
	************
	************
	-->

		<!-- popup ayuda seccion -->
		<div id="dialog-help" title="Ayuda Aprobar Pedido" style="display: none;">
				<div style="height: 350px; width: 100%; overflow: auto;">
						<p class="helpTitle">Seccion ayuda</p>
						<p>Hola esto es una seccion de ayuda Hola esto es una seccion de</p>
				</div>
		</div>
		<!-- fin popup ayuda seccion -->
</body>
</html>
