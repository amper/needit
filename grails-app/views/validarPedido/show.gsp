
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta name="layout" content="mainApp" />
<g:javascript library="aprobarPedido" />
<g:javascript library="boxUser" />

</head>
<body>

	<div class="contenedorTitulo">
		<g:titulo pedido="${pedidoInstance}" />
		<g:workflow pedidoId="${pedidoInstance?.id}" />
	</div>
 
	<div id="contenedorIzq">
		<g:form class="formGeneral" useToken="true">
			<g:if test="${flash.message}">
				<div class="message">
					${flash.message}
				</div>
			</g:if>
			
		<div class="seccionHide">
			<g:cabeceraPedido pedido="${pedidoInstance}" />
			<g:render template="/templates/workflowHidden" model="['vista':'show']"/>
			<p class="tituloSeccion">Interlocutor Usuario y Aprobaci&oacute;n</p>
			<div class="formSeccion">
				<div class="pDos">
					<label class="formLabel2" for='inputIntUsuario'>Interlocutor
						Usuario</label>
					<g:if test="${pedidoInstance?.legajoInterlocutorUsuario != null}">
						<div id="legajoInterlocutorUsuario" class="boxUser">
							<g:render template="/templates/userBox"
								model="['person':pedidoInstance?.legajoInterlocutorUsuario, 'box':'legajoInterlocutorUsuario', 'personsAutocomplete': personsAutocompleteInt]"></g:render>
						</div>
					</g:if>
					<g:else>
						<p class="info">
						No definido
						</p>
					</g:else>
			</div>
			<g:if test="${pedidoInstance?.getJustificacionGU()}">
				<div class="pDos">
					<label for="justificacionAprobacion" class="formLabel">Justificaci&oacute;n</label>
					<p class="info">
						${pedidoInstance?.getJustificacionGU()}
					</p>
				</div>
			</g:if>
		</div>
<%--	<g:if test="${pedidoInstance?.getJustificacionIU()}">--%>
<%--	--%>
<%--			<p class="tituloSeccion">Justificaci&oacute;n de desaprobaci&oacute;n Interlocutor Usuario</p>--%>
<%--			<div class="formSeccion">--%>
<%--					${pedidoInstance?.getJustificacionIU()}--%>
<%--			</div>--%>
<%--	</g:if>--%>
		<g:render template="/templates/ultimaJustificacionDenegacion"
						     			model="['pedidoInstance': pedidoInstance, 'fase': pedidoInstance.faseValidacionPedido()]"></g:render>
						     			
		<g:render template="/templates/tablaAprobaciones" model="['pedidoInstance': pedidoInstance, 'aprobacionesList': aprobacionesList, 'etiqueta': 'Gerente Usuario']"></g:render>
	</div>
	<div id="message"></div>

	</g:form>


	</div>
	<!-- fin contenedor izq -->

	<!-- Lateral Derecho -->
	<g:render template="/templates/menuLateral" model="['pedidoInstance':pedidoInstance]"/>
	<!-- Fin Lateral Derecho -->


	<!-- 
	************
	************
	MODALS POPUP
	************
	************
	************
	-->

	<!-- popup ayuda seccion -->
	<div id="dialog-help" title="Ayuda Aprobar Pedido"
		style="display: none;">
		<div style="height: 350px; width: 100%; overflow: auto;">
			<p class="helpTitle">Seccion ayuda</p>
			<p>Hola esto es una \u00F3 \u00E1 \u00E9 seccion de ayuda Hola esto es una seccion de</p>
		</div>
	</div>
	<!-- fin popup ayuda seccion -->
</body>
</html>
