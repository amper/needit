
<%@ page import="ar.com.telecom.pcs.entities.TipoImpacto" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="layout" content="main" />
        <g:set var="entityName" value="${message(code: 'tipoImpacto.label', default: 'TipoImpacto')}" />
        <title><g:message code="default.list.label" args="[entityName]" /></title>
    </head>
    <body>
        <div class="nav">
            <span class="menuButton"><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></span>
            <span class="menuButton"><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></span>
        </div>
        <div class="body">
            <h1><g:message code="default.list.label" args="[entityName]" /></h1>
            <g:if test="${flash.message}">
            <div class="message">${flash.message}</div>
            </g:if>
            <div class="list">
                <table>
                    <thead>
                        <tr>
                        
                            <g:sortableColumn property="id" title="${message(code: 'tipoImpacto.id.label', default: 'Id')}" />
                        
                            <g:sortableColumn property="descripcion" title="${message(code: 'tipoImpacto.descripcion.label', default: 'Descripcion')}" />
                        
                            <g:sortableColumn property="codigoAgrupador" title="${message(code: 'tipoImpacto.codigoAgrupador.label', default: 'Codigo Agrupador')}" />
                        
                            <g:sortableColumn property="codigo" title="${message(code: 'tipoImpacto.codigo.label', default: 'Codigo')}" />
                        
                            <g:sortableColumn property="asociadoSistemas" title="${message(code: 'tipoImpacto.asociadoSistemas.label', default: 'Asociado Sistemas')}" />
                        
                            <g:sortableColumn property="ingresaTicketSolman" title="${message(code: 'tipoImpacto.ingresaTicketSolman.label', default: 'Ingresa Ticket Solman')}" />
                        
                        </tr>
                    </thead>
                    <tbody>
                    <g:each in="${tipoImpactoInstanceList}" status="i" var="tipoImpactoInstance">
                        <tr class="${(i % 2) == 0 ? 'odd' : 'even'}">
                        
                            <td><g:link action="show" id="${tipoImpactoInstance.id}">${fieldValue(bean: tipoImpactoInstance, field: "id")}</g:link></td>
                        
                            <td>${fieldValue(bean: tipoImpactoInstance, field: "descripcion")}</td>
                        
                            <td>${fieldValue(bean: tipoImpactoInstance, field: "codigoAgrupador")}</td>
                        
                            <td>${fieldValue(bean: tipoImpactoInstance, field: "codigo")}</td>
                        
                            <td><g:formatBoolean boolean="${tipoImpactoInstance.asociadoSistemas}" /></td>
                        
                            <td><g:formatBoolean boolean="${tipoImpactoInstance.ingresaTicketSolman}" /></td>
                        
                        </tr>
                    </g:each>
                    </tbody>
                </table>
            </div>
            <div class="paginateButtons">
                <g:paginate total="${tipoImpactoInstanceTotal}" />
            </div>
        </div>
    </body>
</html>
