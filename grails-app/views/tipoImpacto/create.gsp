

<%@ page import="ar.com.telecom.pcs.entities.TipoImpacto" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="layout" content="main" />
        <g:set var="entityName" value="${message(code: 'tipoImpacto.label', default: 'TipoImpacto')}" />
        <title><g:message code="default.create.label" args="[entityName]" /></title>
    </head>
    <body>
        <div class="nav">
            <span class="menuButton"><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></span>
            <span class="menuButton"><g:link class="list" action="list"><g:message code="default.list.label" args="[entityName]" /></g:link></span>
        </div>
        <div class="body">
            <h1><g:message code="default.create.label" args="[entityName]" /></h1>
            <g:if test="${flash.message}">
            <div class="message">${flash.message}</div>
            </g:if>
            <g:hasErrors bean="${tipoImpactoInstance}">
            <div class="errors">
                <g:renderErrors bean="${tipoImpactoInstance}" as="list" />
            </div>
            </g:hasErrors>
            <g:form action="save" >
                <div class="dialog">
                    <table>
                        <tbody>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="descripcion"><g:message code="tipoImpacto.descripcion.label" default="Descripcion" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: tipoImpactoInstance, field: 'descripcion', 'errors')}">
                                    <g:textField name="descripcion" maxlength="100" value="${tipoImpactoInstance?.descripcion}" />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="codigoAgrupador"><g:message code="tipoImpacto.codigoAgrupador.label" default="Codigo Agrupador" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: tipoImpactoInstance, field: 'codigoAgrupador', 'errors')}">
                                    <g:textField name="codigoAgrupador" maxlength="40" value="${tipoImpactoInstance?.codigoAgrupador}" />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="codigo"><g:message code="tipoImpacto.codigo.label" default="Codigo" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: tipoImpactoInstance, field: 'codigo', 'errors')}">
                                    <g:textField name="codigo" maxlength="3" value="${tipoImpactoInstance?.codigo}" />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="asociadoSistemas"><g:message code="tipoImpacto.asociadoSistemas.label" default="Asociado Sistemas" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: tipoImpactoInstance, field: 'asociadoSistemas', 'errors')}">
                                    <g:checkBox name="asociadoSistemas" value="${tipoImpactoInstance?.asociadoSistemas}" />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="ingresaTicketSolman"><g:message code="tipoImpacto.ingresaTicketSolman.label" default="Ingresa Ticket Solman" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: tipoImpactoInstance, field: 'ingresaTicketSolman', 'errors')}">
                                    <g:checkBox name="ingresaTicketSolman" value="${tipoImpactoInstance?.ingresaTicketSolman}" />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="participaDescripcionFasesHijos"><g:message code="tipoImpacto.participaDescripcionFasesHijos.label" default="Participa Descripcion Fases Hijos" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: tipoImpactoInstance, field: 'participaDescripcionFasesHijos', 'errors')}">
                                    <g:checkBox name="participaDescripcionFasesHijos" value="${tipoImpactoInstance?.participaDescripcionFasesHijos}" />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="permiteIngresarOtrosCostos"><g:message code="tipoImpacto.permiteIngresarOtrosCostos.label" default="Permite Ingresar Otros Costos" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: tipoImpactoInstance, field: 'permiteIngresarOtrosCostos', 'errors')}">
                                    <g:checkBox name="permiteIngresarOtrosCostos" value="${tipoImpactoInstance?.permiteIngresarOtrosCostos}" />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="sistemaImpactado"><g:message code="tipoImpacto.sistemaImpactado.label" default="Sistema Impactado" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: tipoImpactoInstance, field: 'sistemaImpactado', 'errors')}">
                                    <g:checkBox name="sistemaImpactado" value="${tipoImpactoInstance?.sistemaImpactado}" />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="tieneDisenoExterno"><g:message code="tipoImpacto.tieneDisenoExterno.label" default="Tiene Diseno Externo" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: tipoImpactoInstance, field: 'tieneDisenoExterno', 'errors')}">
                                    <g:checkBox name="tieneDisenoExterno" value="${tipoImpactoInstance?.tieneDisenoExterno}" />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="tieneEstrategiaPrueba"><g:message code="tipoImpacto.tieneEstrategiaPrueba.label" default="Tiene Estrategia Prueba" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: tipoImpactoInstance, field: 'tieneEstrategiaPrueba', 'errors')}">
                                    <g:checkBox name="tieneEstrategiaPrueba" value="${tipoImpactoInstance?.tieneEstrategiaPrueba}" />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="tienePlanificacionyEsfuerzo"><g:message code="tipoImpacto.tienePlanificacionyEsfuerzo.label" default="Tiene Planificaciony Esfuerzo" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: tipoImpactoInstance, field: 'tienePlanificacionyEsfuerzo', 'errors')}">
                                    <g:checkBox name="tienePlanificacionyEsfuerzo" value="${tipoImpactoInstance?.tienePlanificacionyEsfuerzo}" />
                                </td>
                            </tr>
                        
                        </tbody>
                    </table>
                </div>
                <div class="buttons">
                    <span class="button"><g:submitButton name="create" class="save" value="${message(code: 'default.button.create.label', default: 'Create')}" /></span>
                </div>
            </g:form>
        </div>
    </body>
</html>
