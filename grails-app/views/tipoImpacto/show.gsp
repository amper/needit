
<%@ page import="ar.com.telecom.pcs.entities.TipoImpacto" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="layout" content="main" />
        <g:set var="entityName" value="${message(code: 'tipoImpacto.label', default: 'TipoImpacto')}" />
        <title><g:message code="default.show.label" args="[entityName]" /></title>
    </head>
    <body>
        <div class="nav">
            <span class="menuButton"><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></span>
            <span class="menuButton"><g:link class="list" action="list"><g:message code="default.list.label" args="[entityName]" /></g:link></span>
            <span class="menuButton"><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></span>
        </div>
        <div class="body">
            <h1><g:message code="default.show.label" args="[entityName]" /></h1>
            <g:if test="${flash.message}">
            <div class="message">${flash.message}</div>
            </g:if>
            <div class="dialog">
                <table>
                    <tbody>
                    
                        <tr class="prop">
                            <td valign="top" class="name"><g:message code="tipoImpacto.id.label" default="Id" /></td>
                            
                            <td valign="top" class="value">${fieldValue(bean: tipoImpactoInstance, field: "id")}</td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name"><g:message code="tipoImpacto.descripcion.label" default="Descripcion" /></td>
                            
                            <td valign="top" class="value">${fieldValue(bean: tipoImpactoInstance, field: "descripcion")}</td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name"><g:message code="tipoImpacto.codigoAgrupador.label" default="Codigo Agrupador" /></td>
                            
                            <td valign="top" class="value">${fieldValue(bean: tipoImpactoInstance, field: "codigoAgrupador")}</td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name"><g:message code="tipoImpacto.codigo.label" default="Codigo" /></td>
                            
                            <td valign="top" class="value">${fieldValue(bean: tipoImpactoInstance, field: "codigo")}</td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name"><g:message code="tipoImpacto.asociadoSistemas.label" default="Asociado Sistemas" /></td>
                            
                            <td valign="top" class="value"><g:formatBoolean boolean="${tipoImpactoInstance?.asociadoSistemas}" /></td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name"><g:message code="tipoImpacto.fases.label" default="Fases" /></td>
                            
                            <td valign="top" style="text-align: left;" class="value">
                                <ul>
                                <g:each in="${tipoImpactoInstance.fases}" var="f">
                                    <li><g:link controller="fase" action="show" id="${f.id}">${f?.encodeAsHTML()}</g:link></li>
                                </g:each>
                                </ul>
                            </td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name"><g:message code="tipoImpacto.ingresaTicketSolman.label" default="Ingresa Ticket Solman" /></td>
                            
                            <td valign="top" class="value"><g:formatBoolean boolean="${tipoImpactoInstance?.ingresaTicketSolman}" /></td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name"><g:message code="tipoImpacto.participaDescripcionFasesHijos.label" default="Participa Descripcion Fases Hijos" /></td>
                            
                            <td valign="top" class="value"><g:formatBoolean boolean="${tipoImpactoInstance?.participaDescripcionFasesHijos}" /></td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name"><g:message code="tipoImpacto.permiteIngresarOtrosCostos.label" default="Permite Ingresar Otros Costos" /></td>
                            
                            <td valign="top" class="value"><g:formatBoolean boolean="${tipoImpactoInstance?.permiteIngresarOtrosCostos}" /></td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name"><g:message code="tipoImpacto.sistemaImpactado.label" default="Sistema Impactado" /></td>
                            
                            <td valign="top" class="value"><g:formatBoolean boolean="${tipoImpactoInstance?.sistemaImpactado}" /></td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name"><g:message code="tipoImpacto.tieneDisenoExterno.label" default="Tiene Diseno Externo" /></td>
                            
                            <td valign="top" class="value"><g:formatBoolean boolean="${tipoImpactoInstance?.tieneDisenoExterno}" /></td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name"><g:message code="tipoImpacto.tieneEstrategiaPrueba.label" default="Tiene Estrategia Prueba" /></td>
                            
                            <td valign="top" class="value"><g:formatBoolean boolean="${tipoImpactoInstance?.tieneEstrategiaPrueba}" /></td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name"><g:message code="tipoImpacto.tienePlanificacionyEsfuerzo.label" default="Tiene Planificaciony Esfuerzo" /></td>
                            
                            <td valign="top" class="value"><g:formatBoolean boolean="${tipoImpactoInstance?.tienePlanificacionyEsfuerzo}" /></td>
                            
                        </tr>
                    
                    </tbody>
                </table>
            </div>
            <div class="buttons">
                <g:form>
                    <g:hiddenField name="id" value="${tipoImpactoInstance?.id}" />
                    <span class="button"><g:actionSubmit class="edit" action="edit" value="${message(code: 'default.button.edit.label', default: 'Edit')}" /></span>
                    <span class="button"><g:actionSubmit class="delete" action="delete" value="${message(code: 'default.button.delete.label', default: 'Delete')}" onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');" /></span>
                </g:form>
            </div>
        </div>
    </body>
</html>
