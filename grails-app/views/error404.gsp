<%@ page import="ar.com.telecom.exceptions.BusinessException"%>
<%@ page import="ar.com.telecom.exceptions.SessionExpiredException"%>
<%@ page import="org.codehaus.groovy.grails.commons.ConfigurationHolder"%>
<html>
<head>
<title>NeedIt</title>
<meta name="layout" content="mainApp" />
<style type="text/css">
.message {
	border: 1px solid black;
	padding: 5px;
	background-color: #E9E9E9;
}

.stack {
	border: 1px solid black;
	padding: 5px;
	overflow: auto;
	height: 300px;
}
 
.snippet {
	padding: 5px;
	background-color: white;
	border: 1px solid black;
	margin: 3px;
	font-family: courier;
}
</style>
</head>

<body>
	<div class="contenidoInfo">
		<h5>
			Ha ingresado una direcci&oacute;n inv&aacute;lida para la aplicaci&oacute;n. <br> <br>
<%--			<g:link url="${ConfigurationHolder.config.grails.serverURL}/inbox">Ir al Inbox</g:link>--%>
			<div id="errorBody-footer"><g:link controller="inbox" action="index">Ir al Inbox</g:link></div>
		</h5>
	</div>
	<br>
</body>
</html>