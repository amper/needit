<%@ page import="org.codehaus.groovy.grails.commons.ConfigurationHolder" %>
<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
<meta name="layout" content="mainApp"/>
<title>needIt :: Ayuda</title>
</head>
<body>
  <div class="ayuda-body">
    <iframe src="${ConfigurationHolder.config.needit.files.ayuda.url}" width="100%" height="700" frameborder="0"></iframe>
  </div>
</body>
</html>  