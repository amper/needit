
<%@ page import="ar.com.telecom.pcs.entities.AnexoPorTipo" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="layout" content="main" />
        <g:set var="entityName" value="${message(code: 'anexoPorTipo.label', default: 'AnexoPorTipo')}" />
        <title><g:message code="default.list.label" args="[entityName]" /></title>
    </head>
    <body>
        <div class="nav">
            <span class="menuButton"><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></span>
            <span class="menuButton"><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></span>
        </div>
        <div class="body">
            <h1><g:message code="default.list.label" args="[entityName]" /></h1>
            <g:if test="${flash.message}">
            <div class="message">${flash.message}</div>
            </g:if>
            <div class="list">
                <table>
                    <thead>
                        <tr>
                        
                            <g:sortableColumn property="id" title="${message(code: 'anexoPorTipo.id.label', default: 'Id')}" />
                        
                            <g:sortableColumn property="legajo" title="${message(code: 'anexoPorTipo.legajo.label', default: 'Legajo')}" />
                        
                            <g:sortableColumn property="nombreArchivo" title="${message(code: 'anexoPorTipo.nombreArchivo.label', default: 'Nombre Archivo')}" />
                        
                            <g:sortableColumn property="archivo" title="${message(code: 'anexoPorTipo.archivo.label', default: 'Archivo')}" />
                        
                            <g:sortableColumn property="extension" title="${message(code: 'anexoPorTipo.extension.label', default: 'Extension')}" />
                        
                            <th><g:message code="anexoPorTipo.tipoAnexo.label" default="Tipo Anexo" /></th>
                        
                        </tr>
                    </thead>
                    <tbody>
                    <g:each in="${anexoPorTipoInstanceList}" status="i" var="anexoPorTipoInstance">
                        <tr class="${(i % 2) == 0 ? 'odd' : 'even'}">
                        
                            <td><g:link action="show" id="${anexoPorTipoInstance.id}">${fieldValue(bean: anexoPorTipoInstance, field: "id")}</g:link></td>
                        
                            <td>${fieldValue(bean: anexoPorTipoInstance, field: "legajo")}</td>
                        
                            <td>${fieldValue(bean: anexoPorTipoInstance, field: "nombreArchivo")}</td>
                        
                            <td>${fieldValue(bean: anexoPorTipoInstance, field: "archivo")}</td>
                        
                            <td>${fieldValue(bean: anexoPorTipoInstance, field: "extension")}</td>
                        
                            <td>${fieldValue(bean: anexoPorTipoInstance, field: "tipoAnexo")}</td>
                        
                        </tr>
                    </g:each>
                    </tbody>
                </table>
            </div>
            <div class="paginateButtons">
                <g:paginate total="${anexoPorTipoInstanceTotal}" />
            </div>
        </div>
    </body>
</html>
