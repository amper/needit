

<%@ page import="ar.com.telecom.pcs.entities.AnexoPorTipo" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="layout" content="main" />
        <g:set var="entityName" value="${message(code: 'anexoPorTipo.label', default: 'AnexoPorTipo')}" />
        <title><g:message code="default.create.label" args="[entityName]" /></title>
    </head>
    <body>
        <div class="nav">
            <span class="menuButton"><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></span>
            <span class="menuButton"><g:link class="list" action="list"><g:message code="default.list.label" args="[entityName]" /></g:link></span>
        </div>
        <div class="body">
            <h1><g:message code="default.create.label" args="[entityName]" /></h1>
            <g:if test="${flash.message}">
            <div class="message">${flash.message}</div>
            </g:if>
            <g:hasErrors bean="${anexoPorTipoInstance}">
            <div class="errors">
                <g:renderErrors bean="${anexoPorTipoInstance}" as="list" />
            </div>
            </g:hasErrors>
            <g:form action="save"  enctype="multipart/form-data">
                <div class="dialog">
                    <table>
                        <tbody>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="legajo"><g:message code="anexoPorTipo.legajo.label" default="Legajo" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: anexoPorTipoInstance, field: 'legajo', 'errors')}">
                                    <g:textField name="legajo" maxlength="20" value="${anexoPorTipoInstance?.legajo}" />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="nombreArchivo"><g:message code="anexoPorTipo.nombreArchivo.label" default="Nombre Archivo" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: anexoPorTipoInstance, field: 'nombreArchivo', 'errors')}">
                                    <g:textField name="nombreArchivo" maxlength="60" value="${anexoPorTipoInstance?.nombreArchivo}" />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="archivo"><g:message code="anexoPorTipo.archivo.label" default="Archivo" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: anexoPorTipoInstance, field: 'archivo', 'errors')}">
                                    <input type="file" id="archivo" name="archivo" />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="extension"><g:message code="anexoPorTipo.extension.label" default="Extension" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: anexoPorTipoInstance, field: 'extension', 'errors')}">
                                    <g:textField name="extension" value="${anexoPorTipoInstance?.extension}" />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="tipoAnexo"><g:message code="anexoPorTipo.tipoAnexo.label" default="Tipo Anexo" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: anexoPorTipoInstance, field: 'tipoAnexo', 'errors')}">
                                    <g:select name="tipoAnexo.id" from="${ar.com.telecom.pcs.entities.TipoAnexo.list()}" optionKey="id" value="${anexoPorTipoInstance?.tipoAnexo?.id}" noSelection="['null': '']" />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="descripcion"><g:message code="anexoPorTipo.descripcion.label" default="Descripcion" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: anexoPorTipoInstance, field: 'descripcion', 'errors')}">
                                    <g:textField name="descripcion" value="${anexoPorTipoInstance?.descripcion}" />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="fecha"><g:message code="anexoPorTipo.fecha.label" default="Fecha" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: anexoPorTipoInstance, field: 'fecha', 'errors')}">
                                    <g:datePicker name="fecha" precision="day" value="${anexoPorTipoInstance?.fecha}"  />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="fileContentType"><g:message code="anexoPorTipo.fileContentType.label" default="File Content Type" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: anexoPorTipoInstance, field: 'fileContentType', 'errors')}">
                                    <g:textField name="fileContentType" value="${anexoPorTipoInstance?.fileContentType}" />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="global"><g:message code="anexoPorTipo.global.label" default="Global" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: anexoPorTipoInstance, field: 'global', 'errors')}">
                                    <g:checkBox name="global" value="${anexoPorTipoInstance?.global}" />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="versionAnexo"><g:message code="anexoPorTipo.versionAnexo.label" default="Version Anexo" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: anexoPorTipoInstance, field: 'versionAnexo', 'errors')}">
                                    <g:textField name="versionAnexo" value="${fieldValue(bean: anexoPorTipoInstance, field: 'versionAnexo')}" />
                                </td>
                            </tr>
                        
                        </tbody>
                    </table>
                </div>
                <div class="buttons">
                    <span class="button"><g:submitButton name="create" class="save" value="${message(code: 'default.button.create.label', default: 'Create')}" /></span>
                </div>
            </g:form>
        </div>
    </body>
</html>
