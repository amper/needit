
<%@ page import="ar.com.telecom.pcs.entities.AnexoPorTipo" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="layout" content="main" />
        <g:set var="entityName" value="${message(code: 'anexoPorTipo.label', default: 'AnexoPorTipo')}" />
        <title><g:message code="default.show.label" args="[entityName]" /></title>
    </head>
    <body>
        <div class="nav">
            <span class="menuButton"><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></span>
            <span class="menuButton"><g:link class="list" action="list"><g:message code="default.list.label" args="[entityName]" /></g:link></span>
            <span class="menuButton"><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></span>
        </div>
        <div class="body">
            <h1><g:message code="default.show.label" args="[entityName]" /></h1>
            <g:if test="${flash.message}">
            <div class="message">${flash.message}</div>
            </g:if>
            <div class="dialog">
                <table>
                    <tbody>
                    
                        <tr class="prop">
                            <td valign="top" class="name"><g:message code="anexoPorTipo.id.label" default="Id" /></td>
                            
                            <td valign="top" class="value">${fieldValue(bean: anexoPorTipoInstance, field: "id")}</td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name"><g:message code="anexoPorTipo.legajo.label" default="Legajo" /></td>
                            
                            <td valign="top" class="value">${fieldValue(bean: anexoPorTipoInstance, field: "legajo")}</td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name"><g:message code="anexoPorTipo.nombreArchivo.label" default="Nombre Archivo" /></td>
                            
                            <td valign="top" class="value">${fieldValue(bean: anexoPorTipoInstance, field: "nombreArchivo")}</td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name"><g:message code="anexoPorTipo.archivo.label" default="Archivo" /></td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name"><g:message code="anexoPorTipo.extension.label" default="Extension" /></td>
                            
                            <td valign="top" class="value">${fieldValue(bean: anexoPorTipoInstance, field: "extension")}</td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name"><g:message code="anexoPorTipo.tipoAnexo.label" default="Tipo Anexo" /></td>
                            
                            <td valign="top" class="value"><g:link controller="tipoAnexo" action="show" id="${anexoPorTipoInstance?.tipoAnexo?.id}">${anexoPorTipoInstance?.tipoAnexo?.encodeAsHTML()}</g:link></td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name"><g:message code="anexoPorTipo.descripcion.label" default="Descripcion" /></td>
                            
                            <td valign="top" class="value">${fieldValue(bean: anexoPorTipoInstance, field: "descripcion")}</td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name"><g:message code="anexoPorTipo.fecha.label" default="Fecha" /></td>
                            
                            <td valign="top" class="value"><g:formatDate date="${anexoPorTipoInstance?.fecha}" /></td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name"><g:message code="anexoPorTipo.fileContentType.label" default="File Content Type" /></td>
                            
                            <td valign="top" class="value">${fieldValue(bean: anexoPorTipoInstance, field: "fileContentType")}</td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name"><g:message code="anexoPorTipo.global.label" default="Global" /></td>
                            
                            <td valign="top" class="value"><g:formatBoolean boolean="${anexoPorTipoInstance?.global}" /></td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name"><g:message code="anexoPorTipo.versionAnexo.label" default="Version Anexo" /></td>
                            
                            <td valign="top" class="value">${fieldValue(bean: anexoPorTipoInstance, field: "versionAnexo")}</td>
                            
                        </tr>
                    
                    </tbody>
                </table>
            </div>
            <div class="buttons">
                <g:form>
                    <g:hiddenField name="id" value="${anexoPorTipoInstance?.id}" />
                    <span class="button"><g:actionSubmit class="edit" action="edit" value="${message(code: 'default.button.edit.label', default: 'Edit')}" /></span>
                    <span class="button"><g:actionSubmit class="delete" action="delete" value="${message(code: 'default.button.delete.label', default: 'Delete')}" onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');" /></span>
                </g:form>
            </div>
        </div>
    </body>
</html>
