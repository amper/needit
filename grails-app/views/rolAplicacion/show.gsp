
<%@ page import="ar.com.telecom.pcs.entities.RolAplicacion" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="layout" content="main" />
        <g:set var="entityName" value="${message(code: 'rolAplicacion.label', default: 'RolAplicacion')}" />
        <title><g:message code="default.show.label" args="[entityName]" /></title>
    </head>
    <body>
        <div class="nav">
            <span class="menuButton"><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></span>
            <span class="menuButton"><g:link class="list" action="list"><g:message code="default.list.label" args="[entityName]" /></g:link></span>
            <span class="menuButton"><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></span>
        </div>
        <div class="body">
            <h1><g:message code="default.show.label" args="[entityName]" /></h1>
            <g:if test="${flash.message}">
            <div class="message">${flash.message}</div>
            </g:if>
            <div class="dialog">
                <table>
                    <tbody>
                    
                        <tr class="prop">
                            <td valign="top" class="name"><g:message code="rolAplicacion.id.label" default="Id" /></td>
                            
                            <td valign="top" class="value">${fieldValue(bean: rolAplicacionInstance, field: "id")}</td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name"><g:message code="rolAplicacion.descripcion.label" default="Descripcion" /></td>
                            
                            <td valign="top" class="value">${fieldValue(bean: rolAplicacionInstance, field: "descripcion")}</td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name"><g:message code="rolAplicacion.propiedadUsuariosEnvioMails.label" default="Propiedad Usuarios Envio Mails" /></td>
                            
                            <td valign="top" class="value">${fieldValue(bean: rolAplicacionInstance, field: "propiedadUsuariosEnvioMails")}</td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name"><g:message code="rolAplicacion.propiedadGrupoEnvioMails.label" default="Propiedad Grupo Envio Mails" /></td>
                            
                            <td valign="top" class="value">${fieldValue(bean: rolAplicacionInstance, field: "propiedadGrupoEnvioMails")}</td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name"><g:message code="rolAplicacion.codigoRol.label" default="Codigo Rol" /></td>
                            
                            <td valign="top" class="value">${fieldValue(bean: rolAplicacionInstance, field: "codigoRol")}</td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name"><g:message code="rolAplicacion.aplicaPadre.label" default="Aplica Padre" /></td>
                            
                            <td valign="top" class="value"><g:formatBoolean boolean="${rolAplicacionInstance?.aplicaPadre}" /></td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name"><g:message code="rolAplicacion.seleccionableEnListados.label" default="Seleccionable En Listados" /></td>
                            
                            <td valign="top" class="value"><g:formatBoolean boolean="${rolAplicacionInstance?.seleccionableEnListados}" /></td>
                            
                        </tr>
                    
                    </tbody>
                </table>
            </div>
            <div class="buttons">
                <g:form>
                    <g:hiddenField name="id" value="${rolAplicacionInstance?.id}" />
                    <span class="button"><g:actionSubmit class="edit" action="edit" value="${message(code: 'default.button.edit.label', default: 'Edit')}" /></span>
                    <span class="button"><g:actionSubmit class="delete" action="delete" value="${message(code: 'default.button.delete.label', default: 'Delete')}" onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');" /></span>
                </g:form>
            </div>
        </div>
    </body>
</html>
