

<%@ page import="ar.com.telecom.pcs.entities.RolAplicacion" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="layout" content="main" />
        <g:set var="entityName" value="${message(code: 'rolAplicacion.label', default: 'RolAplicacion')}" />
        <title><g:message code="default.create.label" args="[entityName]" /></title>
    </head>
    <body>
        <div class="nav">
            <span class="menuButton"><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></span>
            <span class="menuButton"><g:link class="list" action="list"><g:message code="default.list.label" args="[entityName]" /></g:link></span>
        </div>
        <div class="body">
            <h1><g:message code="default.create.label" args="[entityName]" /></h1>
            <g:if test="${flash.message}">
            <div class="message">${flash.message}</div>
            </g:if>
            <g:hasErrors bean="${rolAplicacionInstance}">
            <div class="errors">
                <g:renderErrors bean="${rolAplicacionInstance}" as="list" />
            </div>
            </g:hasErrors>
            <g:form action="save" >
                <div class="dialog">
                    <table>
                        <tbody>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="descripcion"><g:message code="rolAplicacion.descripcion.label" default="Descripcion" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: rolAplicacionInstance, field: 'descripcion', 'errors')}">
                                    <g:textField name="descripcion" maxlength="50" value="${rolAplicacionInstance?.descripcion}" />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="propiedadUsuariosEnvioMails"><g:message code="rolAplicacion.propiedadUsuariosEnvioMails.label" default="Propiedad Usuarios Envio Mails" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: rolAplicacionInstance, field: 'propiedadUsuariosEnvioMails', 'errors')}">
                                    <g:textField name="propiedadUsuariosEnvioMails" maxlength="100" value="${rolAplicacionInstance?.propiedadUsuariosEnvioMails}" />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="propiedadGrupoEnvioMails"><g:message code="rolAplicacion.propiedadGrupoEnvioMails.label" default="Propiedad Grupo Envio Mails" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: rolAplicacionInstance, field: 'propiedadGrupoEnvioMails', 'errors')}">
                                    <g:textField name="propiedadGrupoEnvioMails" maxlength="100" value="${rolAplicacionInstance?.propiedadGrupoEnvioMails}" />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="codigoRol"><g:message code="rolAplicacion.codigoRol.label" default="Codigo Rol" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: rolAplicacionInstance, field: 'codigoRol', 'errors')}">
                                    <g:textField name="codigoRol" maxlength="5" value="${rolAplicacionInstance?.codigoRol}" />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="aplicaPadre"><g:message code="rolAplicacion.aplicaPadre.label" default="Aplica Padre" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: rolAplicacionInstance, field: 'aplicaPadre', 'errors')}">
                                    <g:checkBox name="aplicaPadre" value="${rolAplicacionInstance?.aplicaPadre}" />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="seleccionableEnListados"><g:message code="rolAplicacion.seleccionableEnListados.label" default="Seleccionable En Listados" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: rolAplicacionInstance, field: 'seleccionableEnListados', 'errors')}">
                                    <g:checkBox name="seleccionableEnListados" value="${rolAplicacionInstance?.seleccionableEnListados}" />
                                </td>
                            </tr>
                        
                        </tbody>
                    </table>
                </div>
                <div class="buttons">
                    <span class="button"><g:submitButton name="create" class="save" value="${message(code: 'default.button.create.label', default: 'Create')}" /></span>
                </div>
            </g:form>
        </div>
    </body>
</html>
