
<%@ page import="ar.com.telecom.pcs.entities.RolAplicacion" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="layout" content="main" />
        <g:set var="entityName" value="${message(code: 'rolAplicacion.label', default: 'RolAplicacion')}" />
        <title><g:message code="default.list.label" args="[entityName]" /></title>
    </head>
    <body>
        <div class="nav">
            <span class="menuButton"><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></span>
            <span class="menuButton"><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></span>
        </div>
        <div class="body">
            <h1><g:message code="default.list.label" args="[entityName]" /></h1>
            <g:if test="${flash.message}">
            <div class="message">${flash.message}</div>
            </g:if>
            <div class="list">
                <table>
                    <thead>
                        <tr>
                        
                            <g:sortableColumn property="id" title="${message(code: 'rolAplicacion.id.label', default: 'Id')}" />
                        
                            <g:sortableColumn property="descripcion" title="${message(code: 'rolAplicacion.descripcion.label', default: 'Descripcion')}" />
                        
                            <g:sortableColumn property="propiedadUsuariosEnvioMails" title="${message(code: 'rolAplicacion.propiedadUsuariosEnvioMails.label', default: 'Propiedad Usuarios Envio Mails')}" />
                        
                            <g:sortableColumn property="propiedadGrupoEnvioMails" title="${message(code: 'rolAplicacion.propiedadGrupoEnvioMails.label', default: 'Propiedad Grupo Envio Mails')}" />
                        
                            <g:sortableColumn property="codigoRol" title="${message(code: 'rolAplicacion.codigoRol.label', default: 'Codigo Rol')}" />
                        
                            <g:sortableColumn property="aplicaPadre" title="${message(code: 'rolAplicacion.aplicaPadre.label', default: 'Aplica Padre')}" />
                        
                        </tr>
                    </thead>
                    <tbody>
                    <g:each in="${rolAplicacionInstanceList}" status="i" var="rolAplicacionInstance">
                        <tr class="${(i % 2) == 0 ? 'odd' : 'even'}">
                        
                            <td><g:link action="show" id="${rolAplicacionInstance.id}">${fieldValue(bean: rolAplicacionInstance, field: "id")}</g:link></td>
                        
                            <td>${fieldValue(bean: rolAplicacionInstance, field: "descripcion")}</td>
                        
                            <td>${fieldValue(bean: rolAplicacionInstance, field: "propiedadUsuariosEnvioMails")}</td>
                        
                            <td>${fieldValue(bean: rolAplicacionInstance, field: "propiedadGrupoEnvioMails")}</td>
                        
                            <td>${fieldValue(bean: rolAplicacionInstance, field: "codigoRol")}</td>
                        
                            <td><g:formatBoolean boolean="${rolAplicacionInstance.aplicaPadre}" /></td>
                        
                        </tr>
                    </g:each>
                    </tbody>
                </table>
            </div>
            <div class="paginateButtons">
                <g:paginate total="${rolAplicacionInstanceTotal}" />
            </div>
        </div>
    </body>
</html>
