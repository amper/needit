

<%@ page import="ar.com.telecom.Estructura" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="layout" content="main" />
        <g:set var="entityName" value="${message(code: 'estructura.label', default: 'Estructura')}" />
        <title><g:message code="default.edit.label" args="[entityName]" /></title>
    </head>
    <body>
        <div class="nav">
            <span class="menuButton"><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></span>
            <span class="menuButton"><g:link class="list" action="list"><g:message code="default.list.label" args="[entityName]" /></g:link></span>
            <span class="menuButton"><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></span>
        </div>
        <div class="body">
            <h1><g:message code="default.edit.label" args="[entityName]" /></h1>
            <g:if test="${flash.message}">
            <div class="message">${flash.message}</div>
            </g:if>
            <g:hasErrors bean="${estructuraInstance}">
            <div class="errors">
                <g:renderErrors bean="${estructuraInstance}" as="list" />
            </div>
            </g:hasErrors>
            <g:form method="post" >
                <g:hiddenField name="id" value="${estructuraInstance?.id}" />
                <g:hiddenField name="version" value="${estructuraInstance?.version}" />
                <div class="dialog">
                    <table>
                        <tbody>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                  <label for="gerenteAsociado"><g:message code="estructura.gerenteAsociado.label" default="Gerente Asociado" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: estructuraInstance, field: 'gerenteAsociado', 'errors')}">
                                    <g:select name="gerenteAsociado.id" from="${ar.com.telecom.Person.list()}" optionKey="id" value="${estructuraInstance?.gerenteAsociado?.id}" noSelection="['null': '']" />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                  <label for="codigoEstructura"><g:message code="estructura.codigoEstructura.label" default="Codigo Estructura" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: estructuraInstance, field: 'codigoEstructura', 'errors')}">
                                    <g:textField name="codigoEstructura" maxlength="60" value="${estructuraInstance?.codigoEstructura}" />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                  <label for="codigoEstructuraPadre"><g:message code="estructura.codigoEstructuraPadre.label" default="Codigo Estructura Padre" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: estructuraInstance, field: 'codigoEstructuraPadre', 'errors')}">
                                    <g:textField name="codigoEstructuraPadre" maxlength="60" value="${estructuraInstance?.codigoEstructuraPadre}" />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                  <label for="descripcion"><g:message code="estructura.descripcion.label" default="Descripcion" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: estructuraInstance, field: 'descripcion', 'errors')}">
                                    <g:textField name="descripcion" maxlength="60" value="${estructuraInstance?.descripcion}" />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                  <label for="esDireccion"><g:message code="estructura.esDireccion.label" default="Es Direccion" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: estructuraInstance, field: 'esDireccion', 'errors')}">
                                    <g:checkBox name="esDireccion" value="${estructuraInstance?.esDireccion}" />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                  <label for="nivelEstructura"><g:message code="estructura.nivelEstructura.label" default="Nivel Estructura" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: estructuraInstance, field: 'nivelEstructura', 'errors')}">
                                    <g:textField name="nivelEstructura" value="${fieldValue(bean: estructuraInstance, field: 'nivelEstructura')}" />
                                </td>
                            </tr>
                        
                        </tbody>
                    </table>
                </div>
                <div class="buttons">
                    <span class="button"><g:actionSubmit class="save" action="update" value="${message(code: 'default.button.update.label', default: 'Update')}" /></span>
                    <span class="button"><g:actionSubmit class="delete" action="delete" value="${message(code: 'default.button.delete.label', default: 'Delete')}" onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');" /></span>
                </div>
            </g:form>
        </div>
    </body>
</html>
