
<%@ page import="ar.com.telecom.Estructura" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="layout" content="main" />
        <g:set var="entityName" value="${message(code: 'estructura.label', default: 'Estructura')}" />
        <title><g:message code="default.list.label" args="[entityName]" /></title>
    </head>
    <body>
        <div class="nav">
            <span class="menuButton"><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></span>
            <span class="menuButton"><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></span>
        </div>
        <div class="body">
            <h1><g:message code="default.list.label" args="[entityName]" /></h1>
            <g:if test="${flash.message}">
            <div class="message">${flash.message}</div>
            </g:if>
            <div class="list">
                <table>
                    <thead>
                        <tr>
                        
                            <g:sortableColumn property="id" title="${message(code: 'estructura.id.label', default: 'Id')}" />
                        
                            <th><g:message code="estructura.gerenteAsociado.label" default="Gerente Asociado" /></th>
                        
                            <g:sortableColumn property="codigoEstructura" title="${message(code: 'estructura.codigoEstructura.label', default: 'Codigo Estructura')}" />
                        
                            <g:sortableColumn property="codigoEstructuraPadre" title="${message(code: 'estructura.codigoEstructuraPadre.label', default: 'Codigo Estructura Padre')}" />
                        
                            <g:sortableColumn property="descripcion" title="${message(code: 'estructura.descripcion.label', default: 'Descripcion')}" />
                        
                            <g:sortableColumn property="esDireccion" title="${message(code: 'estructura.esDireccion.label', default: 'Es Direccion')}" />
                        
                        </tr>
                    </thead>
                    <tbody>
                    <g:each in="${estructuraInstanceList}" status="i" var="estructuraInstance">
                        <tr class="${(i % 2) == 0 ? 'odd' : 'even'}">
                        
                            <td><g:link action="show" id="${estructuraInstance.id}">${fieldValue(bean: estructuraInstance, field: "id")}</g:link></td>
                        
                            <td>${fieldValue(bean: estructuraInstance, field: "gerenteAsociado")}</td>
                        
                            <td>${fieldValue(bean: estructuraInstance, field: "codigoEstructura")}</td>
                        
                            <td>${fieldValue(bean: estructuraInstance, field: "codigoEstructuraPadre")}</td>
                        
                            <td>${fieldValue(bean: estructuraInstance, field: "descripcion")}</td>
                        
                            <td><g:formatBoolean boolean="${estructuraInstance.esDireccion}" /></td>
                        
                        </tr>
                    </g:each>
                    </tbody>
                </table>
            </div>
            <div class="paginateButtons">
                <g:paginate total="${estructuraInstanceTotal}" />
            </div>
        </div>
    </body>
</html>
