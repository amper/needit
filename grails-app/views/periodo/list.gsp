
<%@ page import="ar.com.telecom.pcs.entities.Periodo" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="layout" content="main" />
        <g:set var="entityName" value="${message(code: 'periodo.label', default: 'Periodo')}" />
        <title><g:message code="default.list.label" args="[entityName]" /></title>
    </head>
    <body>
        <div class="nav">
            <span class="menuButton"><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></span>
            <span class="menuButton"><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></span>
        </div>
        <div class="body">
            <h1><g:message code="default.list.label" args="[entityName]" /></h1>
            <g:if test="${flash.message}">
            <div class="message">${flash.message}</div>
            </g:if>
            <div class="list">
                <table>
                    <thead>
                        <tr>
                        
                            <g:sortableColumn property="id" title="${message(code: 'periodo.id.label', default: 'Id')}" />
                        
                            <g:sortableColumn property="anio" title="${message(code: 'periodo.anio.label', default: 'Anio')}" />
                        
                            <g:sortableColumn property="mes" title="${message(code: 'periodo.mes.label', default: 'Mes')}" />
                        
                        </tr>
                    </thead>
                    <tbody>
                    <g:each in="${periodoInstanceList}" status="i" var="periodoInstance">
                        <tr class="${(i % 2) == 0 ? 'odd' : 'even'}">
                        
                            <td><g:link action="show" id="${periodoInstance.id}">${fieldValue(bean: periodoInstance, field: "id")}</g:link></td>
                        
                            <td>${fieldValue(bean: periodoInstance, field: "anio")}</td>
                        
                            <td>${fieldValue(bean: periodoInstance, field: "mes")}</td>
                        
                        </tr>
                    </g:each>
                    </tbody>
                </table>
            </div>
            <div class="paginateButtons">
                <g:paginate total="${periodoInstanceTotal}" />
            </div>
        </div>
    </body>
</html>
