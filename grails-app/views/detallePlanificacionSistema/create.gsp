

<%@ page import="ar.com.telecom.pcs.entities.DetallePlanificacionSistema" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="layout" content="main" />
        <g:set var="entityName" value="${message(code: 'detallePlanificacionSistema.label', default: 'DetallePlanificacionSistema')}" />
        <title><g:message code="default.create.label" args="[entityName]" /></title>
    </head>
    <body>
        <div class="nav">
            <span class="menuButton"><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></span>
            <span class="menuButton"><g:link class="list" action="list"><g:message code="default.list.label" args="[entityName]" /></g:link></span>
        </div>
        <div class="body">
            <h1><g:message code="default.create.label" args="[entityName]" /></h1>
            <g:if test="${flash.message}">
            <div class="message">${flash.message}</div>
            </g:if>
            <g:hasErrors bean="${detallePlanificacionSistemaInstance}">
            <div class="errors">
                <g:renderErrors bean="${detallePlanificacionSistemaInstance}" as="list" />
            </div>
            </g:hasErrors>
            <g:form action="save" >
                <div class="dialog">
                    <table>
                        <tbody>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="fechaDesde"><g:message code="detallePlanificacionSistema.fechaDesde.label" default="Fecha Desde" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: detallePlanificacionSistemaInstance, field: 'fechaDesde', 'errors')}">
                                    <g:datePicker name="fechaDesde" precision="day" value="${detallePlanificacionSistemaInstance?.fechaDesde}" default="none" noSelection="['': '']" />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="fechaHasta"><g:message code="detallePlanificacionSistema.fechaHasta.label" default="Fecha Hasta" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: detallePlanificacionSistemaInstance, field: 'fechaHasta', 'errors')}">
                                    <g:datePicker name="fechaHasta" precision="day" value="${detallePlanificacionSistemaInstance?.fechaHasta}" default="none" noSelection="['': '']" />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="actividadPlanificacion"><g:message code="detallePlanificacionSistema.actividadPlanificacion.label" default="Actividad Planificacion" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: detallePlanificacionSistemaInstance, field: 'actividadPlanificacion', 'errors')}">
                                    <g:select name="actividadPlanificacion.id" from="${ar.com.telecom.pcs.entities.ActividadPlanificacion.list()}" optionKey="id" value="${detallePlanificacionSistemaInstance?.actividadPlanificacion?.id}"  />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="planificacion"><g:message code="detallePlanificacionSistema.planificacion.label" default="Planificacion" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: detallePlanificacionSistemaInstance, field: 'planificacion', 'errors')}">
                                    <g:select name="planificacion.id" from="${ar.com.telecom.pcs.entities.PlanificacionEsfuerzo.list()}" optionKey="id" value="${detallePlanificacionSistemaInstance?.planificacion?.id}"  />
                                </td>
                            </tr>
                        
                        </tbody>
                    </table>
                </div>
                <div class="buttons">
                    <span class="button"><g:submitButton name="create" class="save" value="${message(code: 'default.button.create.label', default: 'Create')}" /></span>
                </div>
            </g:form>
        </div>
    </body>
</html>
