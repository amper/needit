
<%@ page import="ar.com.telecom.pcs.entities.DetallePlanificacionSistema" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="layout" content="main" />
        <g:set var="entityName" value="${message(code: 'detallePlanificacionSistema.label', default: 'DetallePlanificacionSistema')}" />
        <title><g:message code="default.list.label" args="[entityName]" /></title>
    </head>
    <body>
        <div class="nav">
            <span class="menuButton"><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></span>
            <span class="menuButton"><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></span>
        </div>
        <div class="body">
            <h1><g:message code="default.list.label" args="[entityName]" /></h1>
            <g:if test="${flash.message}">
            <div class="message">${flash.message}</div>
            </g:if>
            <div class="list">
                <table>
                    <thead>
                        <tr>
                        
                            <g:sortableColumn property="id" title="${message(code: 'detallePlanificacionSistema.id.label', default: 'Id')}" />
                        
                            <g:sortableColumn property="fechaDesde" title="${message(code: 'detallePlanificacionSistema.fechaDesde.label', default: 'Fecha Desde')}" />
                        
                            <g:sortableColumn property="fechaHasta" title="${message(code: 'detallePlanificacionSistema.fechaHasta.label', default: 'Fecha Hasta')}" />
                        
                            <th><g:message code="detallePlanificacionSistema.actividadPlanificacion.label" default="Actividad Planificacion" /></th>
                        
                            <th><g:message code="detallePlanificacionSistema.planificacion.label" default="Planificacion" /></th>
                        
                        </tr>
                    </thead>
                    <tbody>
                    <g:each in="${detallePlanificacionSistemaInstanceList}" status="i" var="detallePlanificacionSistemaInstance">
                        <tr class="${(i % 2) == 0 ? 'odd' : 'even'}">
                        
                            <td><g:link action="show" id="${detallePlanificacionSistemaInstance.id}">${fieldValue(bean: detallePlanificacionSistemaInstance, field: "id")}</g:link></td>
                        
                            <td><g:formatDate date="${detallePlanificacionSistemaInstance.fechaDesde}" /></td>
                        
                            <td><g:formatDate date="${detallePlanificacionSistemaInstance.fechaHasta}" /></td>
                        
                            <td>${fieldValue(bean: detallePlanificacionSistemaInstance, field: "actividadPlanificacion")}</td>
                        
                            <td>${fieldValue(bean: detallePlanificacionSistemaInstance, field: "planificacion")}</td>
                        
                        </tr>
                    </g:each>
                    </tbody>
                </table>
            </div>
            <div class="paginateButtons">
                <g:paginate total="${detallePlanificacionSistemaInstanceTotal}" />
            </div>
        </div>
    </body>
</html>
