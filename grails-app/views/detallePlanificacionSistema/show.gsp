
<%@ page import="ar.com.telecom.pcs.entities.DetallePlanificacionSistema" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="layout" content="main" />
        <g:set var="entityName" value="${message(code: 'detallePlanificacionSistema.label', default: 'DetallePlanificacionSistema')}" />
        <title><g:message code="default.show.label" args="[entityName]" /></title>
    </head>
    <body>
        <div class="nav">
            <span class="menuButton"><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></span>
            <span class="menuButton"><g:link class="list" action="list"><g:message code="default.list.label" args="[entityName]" /></g:link></span>
            <span class="menuButton"><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></span>
        </div>
        <div class="body">
            <h1><g:message code="default.show.label" args="[entityName]" /></h1>
            <g:if test="${flash.message}">
            <div class="message">${flash.message}</div>
            </g:if>
            <div class="dialog">
                <table>
                    <tbody>
                    
                        <tr class="prop">
                            <td valign="top" class="name"><g:message code="detallePlanificacionSistema.id.label" default="Id" /></td>
                            
                            <td valign="top" class="value">${fieldValue(bean: detallePlanificacionSistemaInstance, field: "id")}</td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name"><g:message code="detallePlanificacionSistema.fechaDesde.label" default="Fecha Desde" /></td>
                            
                            <td valign="top" class="value"><g:formatDate date="${detallePlanificacionSistemaInstance?.fechaDesde}" /></td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name"><g:message code="detallePlanificacionSistema.fechaHasta.label" default="Fecha Hasta" /></td>
                            
                            <td valign="top" class="value"><g:formatDate date="${detallePlanificacionSistemaInstance?.fechaHasta}" /></td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name"><g:message code="detallePlanificacionSistema.actividadPlanificacion.label" default="Actividad Planificacion" /></td>
                            
                            <td valign="top" class="value"><g:link controller="actividadPlanificacion" action="show" id="${detallePlanificacionSistemaInstance?.actividadPlanificacion?.id}">${detallePlanificacionSistemaInstance?.actividadPlanificacion?.encodeAsHTML()}</g:link></td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name"><g:message code="detallePlanificacionSistema.cargaHoras.label" default="Carga Horas" /></td>
                            
                            <td valign="top" style="text-align: left;" class="value">
                                <ul>
                                <g:each in="${detallePlanificacionSistemaInstance.cargaHoras}" var="c">
                                    <li><g:link controller="cargaHora" action="show" id="${c.id}">${c?.encodeAsHTML()}</g:link></li>
                                </g:each>
                                </ul>
                            </td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name"><g:message code="detallePlanificacionSistema.planificacion.label" default="Planificacion" /></td>
                            
                            <td valign="top" class="value"><g:link controller="planificacionEsfuerzo" action="show" id="${detallePlanificacionSistemaInstance?.planificacion?.id}">${detallePlanificacionSistemaInstance?.planificacion?.encodeAsHTML()}</g:link></td>
                            
                        </tr>
                    
                    </tbody>
                </table>
            </div>
            <div class="buttons">
                <g:form>
                    <g:hiddenField name="id" value="${detallePlanificacionSistemaInstance?.id}" />
                    <span class="button"><g:actionSubmit class="edit" action="edit" value="${message(code: 'default.button.edit.label', default: 'Edit')}" /></span>
                    <span class="button"><g:actionSubmit class="delete" action="delete" value="${message(code: 'default.button.delete.label', default: 'Delete')}" onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');" /></span>
                </g:form>
            </div>
        </div>
    </body>
</html>
