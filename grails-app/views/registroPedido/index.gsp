<%@page import="ar.com.telecom.FaseMultiton"%>
<%@page import="ar.com.telecom.pcs.entities.RolAplicacion"%>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta name="layout" content="mainApp" />
<script type="text/javascript" src="${resource(dir:'js/prototype',file:'scriptaculous.js')}"></script>
<g:javascript library="boxUser" />
<g:javascript library="tiny_mce/tiny_mce" />
<g:javascript library="registroPedido" />
<g:javascript library="anexoPorTipo" />
<g:javascript library="busquedaRapida" />

<script type="text/javascript">
tinyMCE.init({
    mode : "exact",
    elements : "inputDescripcion,inputAlcance",
    theme : "advanced",
	skin : "o2k7",
    plugins : "autolink,lists,pagebreak,style,layer,table,save,advhr,advimage,advlink,emotions,iespell,inlinepopups,insertdatetime,preview,media,searchreplace,print,contextmenu,paste,directionality,fullscreen,noneditable,visualchars,nonbreaking,xhtmlxtras,template,wordcount,advlist", 
    paste_text_sticky : true,
    setup : function(ed) {
        ed.onInit.add(function(ed) {
          ed.pasteAsPlainText = true;
        });
     },
             
    // Theme options - button# indicated the row# only
    theme_advanced_buttons1 : "newdocument,print,|,bold,italic,underline,|,fontsizeselect,|,cut,copy,paste,|,bullist,numlist,|,undo,redo,|,forecolor,backcolor,|,image,|,fullscreen,template",
    theme_advanced_buttons2 : "",
    theme_advanced_buttons3 : "",      
    theme_advanced_toolbar_location : "top",
    theme_advanced_toolbar_align : "left",
    theme_advanced_statusbar_location : "bottom",
    theme_advanced_resizing : true,

	template_external_list_url : "lists/template_list.js",

		//template_external_list_url : "lists/template_list.js",
		external_link_list_url : "lists/link_list.js",
		external_image_list_url : "lists/image_list.js",
		media_external_list_url : "lists/media_list.js"
});

tinyMCE.init({
    mode : "exact",
    elements : "inputObjetivo",
    theme : "advanced",
	skin : "o2k7",
	paste_auto_cleanup_on_paste : true,
    paste_remove_styles: true,
    paste_remove_styles_if_webkit: true,
    paste_strip_class_attributes: true,
    plugins : "autolink,lists,pagebreak,style,layer,table,save,advhr,advimage,advlink,emotions,iespell,inlinepopups,insertdatetime,preview,media,searchreplace,print,contextmenu,paste,directionality,fullscreen,noneditable,visualchars,nonbreaking,xhtmlxtras,template,wordcount,advlist", 
    paste_text_sticky : true,
    setup : function(ed) {
        ed.onInit.add(function(ed) {
          ed.pasteAsPlainText = true;
        });
     },
                 
    // Theme options - button# indicated the row# only
    theme_advanced_buttons1 : "newdocument,print,|,bold,italic,underline,|,fontsizeselect,|,cut,copy,paste",
    theme_advanced_buttons2 : "bullist,numlist,|,undo,redo,|,forecolor,backcolor,|,image,|,fullscreen,template",
    theme_advanced_buttons3 : "",      
    theme_advanced_toolbar_location : "top",
    theme_advanced_toolbar_align : "left",
    theme_advanced_statusbar_location : "bottom",
    theme_advanced_resizing : true,

	template_external_list_url : "lists/template_list.js",

		//template_external_list_url : "lists/template_list.js",
		external_link_list_url : "lists/link_list.js",
		external_image_list_url : "lists/image_list.js",
		media_external_list_url : "lists/media_list.js"
});


</script>
</head>
<body>

	<div class="contenedorTitulo">
		<g:titulo pedido="${pedidoInstance}" />
		<g:workflow pedidoId="${pedidoInstance?.id}" />
	</div>

	<div id="contenedorIzq">
		<g:if test="${flash.message}">
			<div class="message">
				${flash.message}
			</div>
		</g:if>
		<g:hasErrors bean="${pedidoInstance}">
			<div class="errors">
				<g:renderErrors bean="${pedidoInstance}" as="list" />
			</div>
		</g:hasErrors>

		<g:uploadForm class="formGeneral" id="formPedido" name="formPedido" action="guardarFormulario" useToken="true" >
			<g:render template="/templates/workflowHidden"/>
			<g:hiddenField name="hVengoPorAnexos" value="${params.controller}" />
			
			<g:hiddenField name="id" value="${pedidoInstance?.id}" />
			<g:hiddenField name="pedidoId" value="${pedidoInstance?.id}" />

			<div class="pCompleto" style="margin-bottom: 10px;">
				<label class="formLabel2 validate" for='inputTitulo'>T&iacute;tulo</label>
				<g:textField name="titulo"
					class="formInput2 required"
					id='titulo' value="${pedidoInstance?.titulo}"
					title="${eachError(bean:pedidoInstance,field:'titulo') {message(error:it)}}" />
			</div>
			
			<div>
				<span id="mensajeGerenteIT">${gerenteITMsg}</span>
			</div>

			<div class="seccionHide">
				<p class="tituloSeccion">Referentes</p>
				<div class="formSeccion">
					<div class="pTres">
						<label class="formLabel2" for='inputUser'>Usuario Final</label>
						<div class="boxUser" id="legajoUsuarioCreador">
							<g:render template="/templates/userBox"
								model="['person':pedidoInstance?.legajoUsuarioCreador?pedidoInstance.legajoUsuarioCreador: usuarioLogueado, 'box':'legajoUsuarioCreador']"></g:render>
						</div>
					</div>
					<div class="pTres">
						<label class="formLabel2 validate" for='inputGte'>Gerente Usuario</label>
						<div id="legajoGerenteUsuario" class="boxUser">
							<g:render template="/templates/userBoxAjax"
								model="['person':pedidoInstance?.legajoGerenteUsuario? pedidoInstance?.legajoGerenteUsuario:userGte?.legajo, 'box':'legajoGerenteUsuario', 'personsAutocomplete': personsAutocompleteGte, 'boxParalelo': 'legajoInterlocutorUsuario', 'method': 'obtenerGerentesAjax']"></g:render>
						</div>
						<g:secureRemoteLink title="Eliminar" class="limpiarBox"
							controller="registroPedido" update="legajoGerenteUsuario"
							id="${RolAplicacion.GERENTE_USUARIO}"
							params="['username': '', 'box':'legajoGerenteUsuario', 'boxParalelo': 'legajoInterlocutorUsuario', 'method': 'obtenerGerentesAjax']"
							action="completarUsuario"
							onComplete="actualizarBoxParalelo('${RolAplicacion.INTERLOCUTOR_USUARIO}')"></g:secureRemoteLink>

					</div>
					<div class="pTres">
						<label class="formLabel2 validate" for='inputIntUsuario'>Interlocutor
							Usuario</label>
						<div id="legajoInterlocutorUsuario" class="boxUser">
							<g:render template="/templates/userBox"
								model="['person':pedidoInstance?.legajoInterlocutorUsuario, 'box':'legajoInterlocutorUsuario', 'personsAutocomplete': personsAutocompleteInt]"></g:render>
						</div>
						<g:secureRemoteLink title="Eliminar" class="limpiarBox" id="${RolAplicacion.INTERLOCUTOR_USUARIO}"
							controller="registroPedido" update="legajoInterlocutorUsuario"
							params="['username': '', 'box':'legajoInterlocutorUsuario']"
							paramsJavascript="['gerente': 'document.getElementById(\'inputlegajoGerenteUsuario\').value']"
							action="completarUsuario"></g:secureRemoteLink>
					</div>
				</div>

				<p class="tituloSeccion">Pedido Funcional</p>
				<div class="formSeccion">
					<div class="pCompleto">
						<label for='inputDescripcion' class="formLabel2 validate">Descripci&oacute;n
							del pedido</label>
						<g:textArea name="descripcion" id='inputDescripcion'
							style="width: 100%;"
							value="${pedidoInstance?.descripcion}"></g:textArea>
					</div>
					<div class="pCompleto">
						<label for='inputAlcance' class="formLabel2 validate">Alcance</label>
						<g:textArea id='inputAlcance' name="alcance" style="width: 100%;"
							value="${pedidoInstance?.alcance}"></g:textArea>
					</div>
					<div class="pDos" style="margin-left: 20px;">
						<label for='inputInformacion' class="formLabel validate">Objetivo</label>
						<g:textArea id='inputObjetivo' name="objetivo"
							value="${pedidoInstance?.objetivo}"></g:textArea>
					</div>
					<div class="pTres">
						<label class="formLabel validate" for='comboboxTipoPedido'>Tipo
							de Pedido</label>
						<g:select id="comboboxTipoPedido"
							name="tipoPedido.id"
							from="${ar.com.telecom.pcs.entities.TipoPedido.list()}"
							optionKey="id" value="${pedidoInstance?.tipoPedido?.id}"
							noSelection="['null': '']" />
					</div>
				</div>

				<p class="tituloSeccion">Informaci&oacute;n adicional</p>
				<div class="formSeccion" style="display: none;">

					<div class="pDos">
						<label for='inputInformacion' class="formLabel">Fuera de
							Alcance</label>
						<g:textArea name="fueraAlcance"
							value="${pedidoInstance?.fueraAlcance}"></g:textArea>
					</div>
					<div class="pDos">
						<label for='inputInformacion' class="formLabel">Informaci&oacute;n
							Adicional</label>
						<g:textArea name="infoAdicional"
							value="${pedidoInstance?.infoAdicional}"></g:textArea>
					</div>
					<div class="pDos">
						<label for='inputBeneficios' class="formLabel">Beneficios
							Esperados</label>
						<g:textArea name="beneficiosEsperados"
							value="${pedidoInstance?.beneficiosEsperados}"></g:textArea>
					</div>
					<div class="pDos">
						<label for="inputRiesgos" class="formLabel">Riesgos
							Observables</label>
						<g:textArea name="riesgosObservables"
							value="${pedidoInstance?.riesgosObservables}"></g:textArea>
					</div>

					<div class="pDos">
						<label class="formLabel" for='datepicker-rpid'>Fecha de
							Implementaci&oacute;n Sugerida</label>
						<g:textField name="fechaDeseadaImplementacion" class="formInput date" id='datepicker-rpid' maxLength="10" />
						<g:hiddenField name="fechaHidden"
							value="${pedidoInstance?.fechaDeseadaImplementacion?.format('dd/MM/yyyy')}" />
					</div>
					<div class="pDos">
						<label class="formLabel" for='combobox'>Sistema Sugerido</label>
							<input data-name="sistemaSugerido" data-url="/registroPedido/busquedaAutocomplete" data-method-name="obtenerSistemas" value="${pedidoInstance?.sistemaSugerido}" data-valor-id="${pedidoInstance?.sistemaSugerido?.id}" class="busqueda-rapida formInput" />
					</div>
					<div class="pDos">
						<label class="formLabel" for='comboboxTipoReferencia'>Tipo
							de referencia origen</label>
						<g:select id="comboboxTipoReferencia" class="formInput"
							name="tipoReferencia.id"
							from="${ar.com.telecom.pcs.entities.TipoReferencia.list()}"
							optionKey="id" value="${pedidoInstance?.tipoReferencia?.id}"
							noSelection="['null': '']" />
					</div>
					
					<div class="pDos" id="divReferenciaOrigen">
						<g:render template="/templates/referenciaOrigen" model="['pedidoInstance':pedidoInstance, 'tipoReferencia':pedidoInstance?.tipoReferencia?.descripcion]"></g:render>
					</div>

<%--					<div class="pDos">--%>
<%--						<g:if test="${g.puedeEditarRegistroPedido(usuarioLogueado: usuarioLogueado) == 'true' }">--%>
<%--							<label for='nuevoAnexo' class="formLabel">Anexos</label>--%>
<%--							<button id="nuevoAnexo" class="formSeccionSubmitButton">Nuevo anexo</button>--%>
<%--							<g:hiddenField name="hForm" id="hForm" value="formPedido"/>--%>
<%--						</g:if>--%>
<%--					</div>--%>
				</div>

				<p class="tituloSeccion">Anexos</p>
				<div class="formSeccion" style="display: none;">


					<div class="pCompleto">
<%--						<label class="formLabel" for='combobox'>Anexos</label>--%>
<%--						<g:if test="${pedidoInstance?.anexos}">--%>
<%--							<ul>--%>
<%--								<g:each in="${pedidoInstance?.anexos}" var="arch">--%>
<%--									<li><g:link action="verAnexo" target="_blank"--%>
<%--											params="[anexoId: arch.id]">--%>
<%--											${arch}--%>
<%--										</g:link>&nbsp;&nbsp;&nbsp; <g:link action="eliminarAnexo"--%>
<%--											params="[pedidoId: pedidoInstance?.id, anexoId: arch.id]">--%>
<%--											<img src="${resource(dir:'images',file:'delete.gif')}"--%>
<%--												width="10" height="10" />--%>
<%--										</g:link>--%>
<%--									</li>--%>
<%--								</g:each>--%>
<%--							</ul>--%>
<%--						</g:if>--%>
							<g:if test="${g.puedeEditarRegistroPedido(usuarioLogueado: userSesion?.legajo) == 'true' }">
								<button id="nuevoAnexo" class="formSeccionSubmitButton" style="margin: 8px !important;">Nuevo anexo</button>
								<g:hiddenField name="hForm" id="hForm" value="formPedido"/>
							</g:if>
							
							<g:render template="/templates/tablaAnexos"
				     			model="['fase': FaseMultiton.REGISTRACION_PEDIDO, 
				     			'editable': true, 'pedidoInstance': pedidoInstance, 'origen': pedidoInstance ]">
			     			</g:render>
					</div>
				</div>


			</div>


<%--				<div class="seccionHide2">--%>
<%--					<g:if test="${pedidoInstance?.ultimaJustificacionRP()}">--%>
<%--						<p class="tituloSeccion">Justificaci&oacute;n de desaprobaci&oacute;n</p>--%>
<%--						<div class="formSeccion">--%>
<%--							<p class="info">&nbsp;${pedidoInstance?.ultimaJustificacionRP()}</p>--%>
<%--						</div>--%>
<%--					</g:if>--%>
<%--				</div>--%>
					<g:render template="/templates/ultimaJustificacionDenegacion"
				     			model="['pedidoInstance': pedidoInstance, 'fase': pedidoInstance.faseRegistracionPedido()]"></g:render>


<%--			</div>--%>
			<!-- fin demo-show -->

			<div id="message"></div>
			<br>
			<div class="formSeccionButton" style="margin-top: 20px;">
			
				<g:if test="${g.puedeEditarRegistroPedido(usuarioLogueado: usuarioLogueado) == 'true' }">
					<g:actionSubmit class="formSeccionSubmitButton" id="GuardarCambio"
						action="guardarCambio" value="Guardar Cambio" style="display:none" />
					<g:if test="${pedidoInstance?.faseActual?.controllerNombre.equals(params.controller) || !pedidoInstance.faseActual}">
						<g:actionSubmit class="formSeccionSubmitButton" id="Simular" controller="registroPedido" action="validarFormulario" value="Simular" />
						<g:actionSubmit class="formSeccionSubmitButton" id="Cancelar2" action="cancelarPedido" value="Cancelar" onClick="if (!confirm('El pedido sera cancelado, esta seguro?')) return false;" />
						<g:actionSubmit class="formSeccionSubmitButton" controller="consolidacionImpacto2" action="guardarFormulario" value="Guardar"  />
						<g:actionSubmit class="formSeccionSubmitButton"	action="finalizarFormulario" value="Enviar" />
					</g:if>
				</g:if>
			</div>

		</g:uploadForm>

	</div>
	
	<!-- fin contenedor izq -->

	<!-- Lateral Derecho -->
	<g:render template="/templates/menuLateral" model="['pedidoInstance':pedidoInstance]"/>
	<!-- Fin Lateral Derecho -->

	<!-- 
	************
	************
	MODALS POPUP
	************
	************
	************
	-->

	<!-- popup ayuda seccion -->
	<div id="dialog-help" title="Ayuda Ingresar Pedido"
		style="display: none;">
		<div style="height: 350px; width: 100%; overflow: auto;">
			<p class="helpTitle">Seccion ayuda</p>
			<p>Hola esto es una seccion de ayuda Hola esto es una seccion de</p>
		</div>
	</div>
	<!-- fin popup ayuda seccion -->
	
<%--	<div id="MenuFijo">--%>
<%--		<div class="boton" onclick="document.getElementById('Simular').click();">Simular</div>--%>
<%--		<div class="boton" onclick="document.getElementById('Cancelar2').click();">Cancelar</div>--%>
<%--		<div class="boton" onclick="document.getElementById('guardarFormulario').click();">Guardar</div>--%>
<%--		<div class="boton" onclick="document.getElementById('finalizarFormulario').click();">Finalizar</div>--%>
<%--	</div>--%>
								
		<div id="dialog-anexos" title="Nuevo Anexo" style="display: none;">
			<g:if test="${!pedidoInstance.id}">
				<g:render template="/templates/anexoPorTipo" model="['pedidoInstance': pedidoInstance]"></g:render>			
			</g:if>
		</div>
<%--	<g:render template="/templates/anexoPorTipo"--%>
<%--		model="['person':pedidoInstance?.legajoUsuarioCreador?pedidoInstance.legajoUsuarioCreador: usuarioLogueado, 'box':'legajoUsuarioCreador', 'pedidoInstance': pedidoInstance]"></g:render>--%>

<%--<g:render template="/templates/footerCargaAnexo" model="['pedidoInstance': pedidoInstance]"></g:render>--%>

</body>
</html>