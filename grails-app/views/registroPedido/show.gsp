<%@ page import="java.util.Date"%>
<%@ page import="ar.com.telecom.FaseMultiton"%>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta name="layout" content="mainApp" />
<g:javascript library="registroPedido" />
<g:javascript library="boxUser" />
<g:javascript library="tiny_mce/tiny_mce" />
<title>NeedIt</title>


<script type="text/javascript">
tinyMCE.init({
    mode : "exact",
    elements : "inputDescripcion,inputAlcance,inputObjetivo",
    theme : "advanced",
    readonly : true,
	skin : "o2k7",
    plugins : "autolink,lists,pagebreak,style,layer,table,save,advhr,advimage,advlink,emotions,iespell,inlinepopups,insertdatetime,preview,media,searchreplace,print,contextmenu,paste,directionality,fullscreen,noneditable,visualchars,nonbreaking,xhtmlxtras,template,wordcount,advlist,autosave", 
             
    // Theme options - button# indicated the row# only
    theme_advanced_buttons1 : "newdocument,print,|,bold,italic,underline,|,fontsizeselect,|,cut,copy,paste,|,bullist,numlist,|,undo,redo,|,forecolor,backcolor,|,image,|,fullscreen,template",
    theme_advanced_buttons2 : "",
    theme_advanced_buttons3 : "",      
    theme_advanced_toolbar_location : "top",
    theme_advanced_toolbar_align : "left",
    theme_advanced_statusbar_location : "bottom",
    theme_advanced_resizing : true,
    paste_text_sticky : true,
    setup : function(ed) {
        ed.onInit.add(function(ed) {
          ed.pasteAsPlainText = true;
        });
    },
	template_external_list_url : "lists/template_list.js",

		//template_external_list_url : "lists/template_list.js",
		external_link_list_url : "lists/link_list.js",
		external_image_list_url : "lists/image_list.js",
		media_external_list_url : "lists/media_list.js"
});
</script>


</head>
<body>

	<div class="contenedorTitulo">
		<g:titulo pedido="${pedidoInstance}" />
		<g:workflow pedidoId="${pedidoInstance?.id}" />
	</div>

	<div id="contenedorIzq">
		<g:uploadForm class="formGeneral" useToken="true">
			<g:render template="/templates/workflowHidden"/>
			<g:hiddenField name="pedidoId" value="${pedidoInstance?.id}" />
			
			<g:if test="${flash.message}">
				<div class="message">
					${flash.message}
				</div>
			</g:if>


			<div class="seccionHide">
				<p class="tituloSeccion">Referentes</p>
				<div class="formSeccion">
					<div class="pTres">
						<label class="formLabel2" for='inputUser'>Usuario Final</label>
						<div class="boxUser" id="boxUser">
							<g:render template="/templates/userBox"
								model="['person':pedidoInstance.legajoUsuarioCreador, 'box':'boxUser']"></g:render>
						</div>
					</div>
					<div class="pTres">
						<label class="formLabel2" for='inputGte'>Gerente Usuario</label>
						<div id="boxUserGte" class="boxUser">
							<g:render template="/templates/userBox"
								model="['person':pedidoInstance.legajoGerenteUsuario, 'box':'boxUserGte', 'personsAutocomplete': personsAutocompleteGte]"></g:render>
						</div>

					</div>
					<div class="pTres">
						<label class="formLabel2" for='inputIntUsuario'>Interlocutor
							Usuario</label>

						<g:if test="${pedidoInstance?.legajoInterlocutorUsuario != null}">
							<div id="boxIntUser" class="boxUser">
								<g:render template="/templates/userBox"
									model="['person':pedidoInstance.legajoInterlocutorUsuario, 'box':'boxIntUser', 'personsAutocomplete': personsAutocompleteInt]"></g:render>
							</div>
						</g:if>
						<g:else>
							<p class="info">No definido</p>
						</g:else>
					</div>
				</div>

				<p class="tituloSeccion">Pedido Funcional</p>
				<div class="formSeccion">
					<div class="pCompleto">
						<label for='inputDescripcion' class="formLabel">Descripci&oacute;n del pedido</label>
							<div class="infoTextArea">
<%--								${pedidoInstance?.descripcion}--%>

						<g:textArea name="descripcion" id='inputDescripcion'
							style="width: 100%;"
							value="${pedidoInstance?.descripcion}"></g:textArea>

								
						</div>

					</div>
					<div class="pCompleto">
						<label for='inputAlcance' class="formLabel">Alcance</label>
						<div class="infoTextArea">
<%--							${pedidoInstance?.alcance}--%>
						
						<g:textArea id='inputAlcance' name="alcance" style="width: 100%;"
							value="${pedidoInstance?.alcance}"></g:textArea>
						
						</div>

					</div>
					<div class="pDos">
						<label for='inputInformacion' class="formLabel">Objetivo</label>
						<div class="infoTextArea">
<%--							${pedidoInstance?.objetivo}--%>
							
						<g:textArea id='inputObjetivo' name="objetivo"
							value="${pedidoInstance?.objetivo}" ></g:textArea>							
						</div>

					</div>
					<div class="pDos">
						<label class="formLabel" for='comboboxTipoPedido'>Tipo de
							Pedido</label>
						<p class="info">
							${pedidoInstance?.tipoPedido}
						</p>
					</div>
				</div>
				
				<p class="tituloSeccion">Informaci&oacute;n adicional</p>
				<g:if test="${!pedidoInstance?.fueraAlcance && !pedidoInstance?.infoAdicional && !pedidoInstance?.beneficiosEsperados && !pedidoInstance?.riesgosObservables && !pedidoInstance?.fechaDeseadaImplementacion && !pedidoInstance?.sistemaSugerido && !pedidoInstance?.tipoReferencia && !pedidoInstance?.referenciaOrigen && !pedidoInstance?.anexos}">
					<div class="formSeccion" style="display: none;">
						<div class="pDos">No se carg&oacute; informaci&oacute;n adicional</div>
					</div>
				</g:if>
				<g:else>
					<div class="formSeccion" style="display: none;">
						<g:if test="${pedidoInstance?.fueraAlcance}" >
							<div class="pDos">
								<label for='inputInformacion' class="formLabel">Fuera de
									Alcance</label>
								<p class="infoTextArea">
									${pedidoInstance?.fueraAlcance}
								</p>
							</div>
						</g:if>
						<g:if test="${pedidoInstance?.infoAdicional}" >
							<div class="pDos">
								<label for='inputInformacion' class="formLabel">Informaci&oacute;n
									Adicional</label>
								<p class="info">
									${pedidoInstance?.infoAdicional}
								</p>
							</div>
						</g:if>
						<g:if test="${pedidoInstance?.beneficiosEsperados}" >
							<div class="pDos">
								<label for='inputBeneficios' class="formLabel">Beneficios
									Esperados</label>
								<p class="infoTextArea">
									${pedidoInstance?.beneficiosEsperados}
								</p>
							</div>
						</g:if>
						<g:if test="${pedidoInstance?.riesgosObservables}" >
							<div class="pDos">
								<label for="inputRiesgos" class="formLabel">Riesgos
									Observables</label>
								<p class="info">
									${pedidoInstance?.riesgosObservables}
								</p>
							</div>
						</g:if>
						<g:if test="${pedidoInstance?.fechaDeseadaImplementacion}" >
							<div class="pDos">
								<label class="formLabel" for='datepicker-rpid'>Fecha de
									Implementaci&oacute;n Sugerida</label>
								<p class="info">
									<g:formatDate format="dd/MM/yyyy"
										date="${pedidoInstance?.fechaDeseadaImplementacion}" />
								</p>
							</div>
						</g:if>
						<g:if test="${pedidoInstance?.sistemaSugerido}" >
							<div class="pDos">
								<label class="formLabel" for='combobox'>Sistema Sugerido</label>
								<p class="info">
									${pedidoInstance?.sistemaSugerido}
								</p>
							</div>
						</g:if>
						<g:if test="${pedidoInstance?.tipoReferencia}" >
							<div class="pDos">
								<label class="formLabel" for='comboboxTipoReferencia'>Tipo
									de referencia origen</label>
								<p class="info">
									${pedidoInstance?.tipoReferencia}
								</p>
							</div>
						</g:if>
						<g:if test="${pedidoInstance?.referenciaOrigen}" >
							<div class="pDos">
								<label class="formLabel">Referencia Origen</label>
								<p class="info">
									${pedidoInstance?.referenciaOrigen}
								</p>
							</div>
						</g:if>
						<g:if test="${pedidoInstance?.anexos}" >
							<div class="pDos">
<%--								<label class="formLabel">Anexos</label>--%>
<%--								<div class="pArch">--%>
									<!-- este CSS tiene underline para el LINK -->
<%--									<g:if test="${pedidoInstance?.anexos}">--%>
<%--										<ul>--%>
<%--											<g:each in="${pedidoInstance?.anexos}" var="arch">--%>
<%--												<li>--%>
<%--													<g:link action="verAnexo" target="_blank"--%>
<%--															params="[anexoId: arch.id]">--%>
<%--															${arch}--%>
<%--													</g:link>--%>
<%--													<g:link controller="registroPedido" action="viewFile"--%>
<%--														target="_blank"--%>
<%--														params="[name: arch, idPedido: pedidoInstance?.id]">--%>
<%--														${arch}--%>
<%--													</g:link>--%>
<%--												</li>--%>
<%--											</g:each>--%>
<%--										</ul>--%>
<%--									</g:if>--%>
<%--								</div>--%>
							</div>
						</g:if>
					</div>
				</g:else>
			<br>
<%--				<div class="seccionHide2">--%>
<%--					<g:if test="${pedidoInstance?.ultimaJustificacionRP()}">--%>
<%--						<p class="tituloSeccion">Justificaci&oacute;n de desaprobaci&oacute;n</p>--%>
<%--						<div class="formSeccion">--%>
<%--							<p class="info">&nbsp;${pedidoInstance?.ultimaJustificacionRP()}</p>--%>
<%--						</div>--%>
<%--					</g:if>--%>
<%--				</div>--%>

				<p class="tituloSeccion">Anexos</p>
				<div class="formSeccion" style="display: none;">


					<div class="pCompleto">
						<g:render template="/templates/tablaAnexos"
			     			model="['fase': FaseMultiton.REGISTRACION_PEDIDO, 
			     			'editable': false, 'pedidoInstance': pedidoInstance, 'origen': pedidoInstance ]">
		     			</g:render>
					</div>
				</div>			

			</div>
			<g:render template="/templates/ultimaJustificacionDenegacion"
		     			model="['pedidoInstance': pedidoInstance, 'fase': pedidoInstance.faseRegistracionPedido()]"></g:render>
			<div id="message"></div>
		</g:uploadForm>

	</div>
	<!-- fin contenedor izq -->

	<!-- Lateral Derecho -->
	<g:render template="/templates/menuLateral" model="['pedidoInstance':pedidoInstance]"/>
	<!-- Fin Lateral Derecho -->

	<!-- 
	************
	************
	MODALS POPUP
	************
	************
	************
	-->

	<!-- popup ayuda seccion -->
	<div id="dialog-help" title="Ayuda Ingresar Pedido"
		style="display: none;">
		<div style="height: 350px; width: 100%; overflow: auto;">
			<p class="helpTitle">Seccion ayuda</p>
			<p>Hola esto es una seccion de ayuda Hola esto es una seccion de</p>
		</div>
	</div>
	<!-- fin popup ayuda seccion -->
</body>
</html>
