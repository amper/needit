<%@page import="org.codehaus.groovy.grails.web.servlet.mvc.SynchronizerToken"%>
<html>
<head>
<title>NeedIt</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<link rel="shortcut icon"
	href="${resource(dir:'images',file:'LogoTelecom.jpg')}"
	type="image/x-icon" />

<!--css-->
<link rel="stylesheet" href="${resource(dir:'css/themes/custom-theme',file:'jquery-ui-1.8.16.custom.css')}" />
<link rel="stylesheet" href="${resource(dir:'css/themes/custom-theme',file:'jquery.multiselect.css')}" />
<link rel="stylesheet" href="${resource(dir:'css/themes/custom-theme',file:'jquery.multiselect.filter.css')}" />
<link rel="stylesheet" href="${resource(dir:'css',file:'mainApp.css')}" />
 
<script type="text/javascript">
	// Defaults
	var macroEstados = ${params.multiselect_comboboxMacroestado?params.multiselect_comboboxMacroestado as List:'null'};
	var rolAplicaciones = ${params.multiselect_comboboxRolAplicacion?params.multiselect_comboboxRolAplicacion as List:'null'}; 
	var fases = ${params.multiselect_comboboxFase?params.multiselect_comboboxFase as List:'null'};
	var tipoEventos = ${listEventosSeleccionados?:'null'};
</script>

<!--js-->
<g:javascript library="jquery-1.6.2" />
<g:javascript library="jquery-ui-custom.min" />
<g:javascript library="jquery.multiselect" />
<g:javascript library="jquery.multiselect.filter" />
<g:javascript library="common" />
<g:javascript library="common-widget" />
<g:javascript library="prototype" />
<g:javascript library="submitSpinner" />
<g:javascript library="DateUtil" />
<g:javascript library="historial" />
<g:javascript library="boxUser" />
<g:javascript library="checkBoxList" />
<g:javascript library="synchronizerToken" />

<script type="text/javascript">
	var gNameApp = 'needIt';
	var gTokenKey = '${SynchronizerToken.KEY}';
</script>
</head>

<body onunload="cierreVentana('busqueda','${stringParams}');">
	<img id="spinner" src="<g:createLinkTo dir='/images' file='ajax-spinner.gif'/>" />
	<div id="ContenedorGeneral">
		<div id="ContenedorPrincipal">
		<div>
			<p class="tituloPrincipal">Historial</p>
		</div>
		
		<div id="contenedorIzqPopUp">			
			<!-- Campos de busqueda -->
			<g:uploadForm name="formHistorial" action="buscarHistorial" class="formGeneral" useToken="true">
				<g:hiddenField name="pedidoId" id="pedidoId" value="${params?.pedidoId}" />
				<div class="seccionHide">
					<g:render template="/templates/encabezadoMinimo" model="['pedidoInstance':pedidoInstance]"></g:render>
					<p class="tituloSeccion">Filtros</p>
					<div class="formSeccion">
					
					<div id="anexoMain" class="tag-dosDiv">

						<div class="columna1">
					
							<div>
								<label for='comboboxMacroestado'>Macroestado</label><br>
								<g:select id="comboboxMacroestado" name="macroEstado" from="${ar.com.telecom.pcs.entities.MacroEstado.list()}" optionKey="id" />
							</div>
							<div id="comboboxFaseHistorial">
								<script type="text/javascript">obtenerFases('');</script>
								<g:render template="/templates/comboFasesHistorial" model="['show': true]"></g:render>
							</div>
							<div>
								<label for='comboboxRolAplicacion'>Rol</label><br>
								<g:select id="comboboxRolAplicacion" name="rolAplicacion" from="${ar.com.telecom.pcs.entities.RolAplicacion.list()}" optionKey="id" />
							</div>
							<div>
								<label>Tipo de evento</label><br>
								<g:select id="comboboxTipoEvento" name="tipoEvento" from="${listEventos.entrySet().key}" />
							</div>
						</div>
						
						<div class="columna1">
							<div>
								<label for='descripcion'>Descripci&oacute;n contiene</label><br>
								<g:textField name="descripcion" class="formInput" style="margin-left: 0px; width: 60%;"  id='descripcion' value="${params?.descripcion?.trim()}"  maxLength="255"/>
							</div>
							
							<div class="pDos" style="width: 100%; margin-left: 0px;">
								<div style="float: left;">
									<label for='fechaDesde'>Fecha desde</label><br>
									<g:textField name="fechaDesde" class="formInput date" style="margin-left: 0px; width: 100px;" id='datepicker-fechaDesde-rpid' maxLength="10" />
									<g:hiddenField name="fechaDesdeHidden" value="${params?.fechaDesde?.format('dd/MM/yyyy')}" />
									<g:hiddenField name="fechaHoyHidden" value="${(new Date()).format('dd/MM/yyyy')}" />
								</div>
								<div style="float: left;">
									<label for='fechaHasta'>Fecha hasta</label><br>
									<g:textField name="fechaHasta" class="formInput date"  style="margin-left: 0px; width: 100px;" id='datepicker-fechaHasta-rpid' maxLength="10" />
									<g:hiddenField name="fechaHastaHidden" value="${params?.fechaHasta?.format('dd/MM/yyyy')}" />
								</div>
							</div>
							
							<div style="width: 80%;">
								<label for='usuario'>Usuario</label>
								<div id="legajoUsuario" class="boxUser">
									<g:render template="/templates/userBox"	model="['person':params?.legajoUsuario, 'box':'legajoUsuario', 'personsAutocomplete': personsAutocomplete, 'pedidoID':pedidoInstance?.id]"></g:render>
								</div>
								<g:secureRemoteLink title="Eliminar" id="historial" class="limpiarBox" controller="historial" update="legajoUsuario"	params="['username': '', 'box':'legajoUsuario', 'pedidoId':pedidoInstance?.id]" action="completarUsuario"></g:secureRemoteLink>
							</div>
						</div>
					</div>
					</div>
					<div class="pCompletoHistorial">
						<button class="formSeccionSubmitButtonHistorial ui-button ui-widget ui-state-default ui-corner-all ui-state-hover" onclick="validarFormulario();"/>Buscar</button>
						<button class="formSeccionSubmitButtonHistorial ui-button ui-widget ui-state-default ui-corner-all ui-state-hover" onclick="clear_form_elements(); obtenerFases(''); seleccion('tipoEvento', ${listEventos.entrySet().size()}, false);"/>Limpiar</button>	
					</div>	
			</g:uploadForm>
			
			<div id="list" class="list">
				<table>
					<thead>
						<tr>
							<g:sortableColumn property="macroEstado" title="Macroestado" params="${params}" action="list" />
							<g:sortableColumn property="fase" title="Fase" params="${params}" action="list"/>
							<g:sortableColumn property="tipoEvento" title="Tipo evento" params="${params}" action="list"/>
							<g:sortableColumn property="legajo" title="Usuario" params="${params}" action="list"/>
							<g:sortableColumn property="rol" title="Rol" params="${params}" action="list"/>
							<g:sortableColumn property="fechaDesde" title="Fecha Inicio" params="${params}" action="list"/>
							<g:sortableColumn property="fechaHasta" title="Fecha Fin" params="${params}" action="list"/>
							<th>Duraci&oacute;n</th>
							<g:sortableColumn property="descripcionEvento" title="Descripci&oacute;n" params="${params}" action="list"/>
						</tr>
					</thead>
					<tbody>
						<g:if test="${logModificaciones}">
							<g:each in="${logModificaciones}" status="i" var="logModificacionInstance">
								<g:set scope="page" var="elem" value="${logModificacionInstance}" />
								<tr class="${(i % 2) == 0 ? 'odd' : 'even'}" style="cursor: pointer;">
									
									<td>
										${elem?.fase?.macroEstado}
									</td>
					
									<td>
										${elem.fase}
									</td>
																
									<td>
										<g:tipoEvento tipoEvento="${ elem?.tipoEvento.toString() }" />
									</td>
									
									<td>
										<g:if test="${elem.legajo}">
											<g:userDescription legajo="${elem?.legajo}" pedido="${elem?.id }"/>
										</g:if>
										<g:else>
											Evento Autom&aacute;tico
										</g:else>
									</td>
									
									<td>
										${elem?.rol}
									</td>
									
									<td>
										<g:formatDate date="${elem.fechaDesde}" type="datetime" style="MEDIUM"/> 
									</td>
									<td>
										<g:formatDate date="${elem.fechaHasta}" type="datetime" style="MEDIUM"/> 
									</td>
									
									<td>
										<%
										def duration
										if (elem.fechaHasta!=null && elem.fechaDesde!=null){
											use(groovy.time.TimeCategory) {
									            duration = elem.fechaHasta - elem.fechaDesde
									        }
										}
										%>
										<g:if test="${duration?.years}">${duration?.years} <g:if test="${duration?.years>1}">a&ntilde;os</g:if><g:else>a&ntilde;o</g:else><br></g:if>
										<g:if test="${duration?.months}">${duration?.months} <g:if test="${duration?.months>1}">meses</g:if><g:else>mes</g:else><br></g:if>
										<g:if test="${duration?.days}">${duration?.days} <g:if test="${duration?.days>1}">d&iacute;as</g:if><g:else>d&iacute;a</g:else><br></g:if>
										<g:if test="${duration?.hours}">${duration?.hours} <g:if test="${duration?.hours>1}">horas</g:if><g:else>hora</g:else><br></g:if>
										<g:if test="${duration?.minutes}">${duration?.minutes} <g:if test="${duration?.minutes>1}">minutos</g:if><g:else>minuto</g:else><br></g:if>
									</td>
															
									<td>
										${ elem?.descripcionEvento }
									</td>
					
								</tr>
							</g:each>
						</g:if>
						<g:else>
							<tr class="odd" style="cursor: pointer;">
								<td colspan="9" style="text-align: center; font-style: italic;">No hay Historial para el filtro seleccionado</td>
							</tr>
						</g:else>
						</tbody>
					</table>
				</div>
				<div class="paginateButtons">
					<g:paginate params="${params}" action="buscarHistorial" id="${pedidoInstance?.id}" total="${logModificacionesTotal?logModificacionesTotal:0}"/>
				</div>
				
				<script type="text/javascript">
					jQuery(document).ready(function() {
						var token = $("org.codehaus.groovy.grails.SYNCHRONIZER_TOKEN").value;
						var steps = jQuery(".paginateButtons .step");
						jQuery.each(steps, function(i, ele) {
							var stepAttr = jQuery(ele).attr('href');
							jQuery(ele).attr('href', stepAttr + '&org.codehaus.groovy.grails.SYNCHRONIZER_TOKEN=' + token);
						});
						
						var prevLinkAttr = jQuery(".paginateButtons .prevLink").attr('href');
						jQuery(".paginateButtons .prevLink").attr('href', prevLinkAttr + '&org.codehaus.groovy.grails.SYNCHRONIZER_TOKEN=' + token);
						var nextLinkAttr = jQuery(".paginateButtons .nextLink").attr('href');
						jQuery(".paginateButtons .nextLink").attr('href', nextLinkAttr + '&org.codehaus.groovy.grails.SYNCHRONIZER_TOKEN=' + token);
					});
				</script>
				<g:if test="${logModificaciones}">
					<export:formats formats="['excel', 'pdf']" action="exportFile" params="${params}" />
				</g:if>
			</div> 
		</div>
	</div>
</body>
</html>