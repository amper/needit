

<%@ page import="ar.com.telecom.pcs.entities.Fase" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="layout" content="main" />
        <g:set var="entityName" value="${message(code: 'fase.label', default: 'Fase')}" />
        <title><g:message code="default.create.label" args="[entityName]" /></title>
    </head>
    <body>
        <div class="nav">
            <span class="menuButton"><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></span>
            <span class="menuButton"><g:link class="list" action="list"><g:message code="default.list.label" args="[entityName]" /></g:link></span>
        </div>
        <div class="body">
            <h1><g:message code="default.create.label" args="[entityName]" /></h1>
            <g:if test="${flash.message}">
            <div class="message">${flash.message}</div>
            </g:if>
            <g:hasErrors bean="${faseInstance}">
            <div class="errors">
                <g:renderErrors bean="${faseInstance}" as="list" />
            </div>
            </g:hasErrors>
            <g:form action="save" >
                <div class="dialog">
                    <table>
                        <tbody>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="descripcion"><g:message code="fase.descripcion.label" default="Descripcion" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: faseInstance, field: 'descripcion', 'errors')}">
                                    <g:textField name="descripcion" maxlength="100" value="${faseInstance?.descripcion}" />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="tooltip"><g:message code="fase.tooltip.label" default="Tooltip" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: faseInstance, field: 'tooltip', 'errors')}">
                                    <g:textArea name="tooltip" cols="40" rows="5" value="${faseInstance?.tooltip}" />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="controllerNombre"><g:message code="fase.controllerNombre.label" default="Controller Nombre" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: faseInstance, field: 'controllerNombre', 'errors')}">
                                    <g:textField name="controllerNombre" maxlength="50" value="${faseInstance?.controllerNombre}" />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="codigoFase"><g:message code="fase.codigoFase.label" default="Codigo Fase" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: faseInstance, field: 'codigoFase', 'errors')}">
                                    <g:textField name="codigoFase" maxlength="10" value="${faseInstance?.codigoFase}" />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="macroEstado"><g:message code="fase.macroEstado.label" default="Macro Estado" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: faseInstance, field: 'macroEstado', 'errors')}">
                                    <g:select name="macroEstado.id" from="${ar.com.telecom.pcs.entities.MacroEstado.list()}" optionKey="id" value="${faseInstance?.macroEstado?.id}" noSelection="['null': '']" />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="faseDenegacion"><g:message code="fase.faseDenegacion.label" default="Fase Denegacion" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: faseInstance, field: 'faseDenegacion', 'errors')}">
                                    <g:select name="faseDenegacion.id" from="${ar.com.telecom.pcs.entities.Fase.list()}" optionKey="id" value="${faseInstance?.faseDenegacion?.id}" noSelection="['null': '']" />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="fasePadre"><g:message code="fase.fasePadre.label" default="Fase Padre" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: faseInstance, field: 'fasePadre', 'errors')}">
                                    <g:select name="fasePadre.id" from="${ar.com.telecom.pcs.entities.Fase.list()}" optionKey="id" value="${faseInstance?.fasePadre?.id}" noSelection="['null': '']" />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="aplicaPadre"><g:message code="fase.aplicaPadre.label" default="Aplica Padre" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: faseInstance, field: 'aplicaPadre', 'errors')}">
                                    <g:checkBox name="aplicaPadre" value="${faseInstance?.aplicaPadre}" />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="descripcionEnBaseAHijos"><g:message code="fase.descripcionEnBaseAHijos.label" default="Descripcion En Base AH ijos" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: faseInstance, field: 'descripcionEnBaseAHijos', 'errors')}">
                                    <g:checkBox name="descripcionEnBaseAHijos" value="${faseInstance?.descripcionEnBaseAHijos}" />
                                </td>
                            </tr>
                        
                        </tbody>
                    </table>
                </div>
                <div class="buttons">
                    <span class="button"><g:submitButton name="create" class="save" value="${message(code: 'default.button.create.label', default: 'Create')}" /></span>
                </div>
            </g:form>
        </div>
    </body>
</html>
