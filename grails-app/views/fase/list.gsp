
<%@ page import="ar.com.telecom.pcs.entities.Fase" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="layout" content="main" />
        <g:set var="entityName" value="${message(code: 'fase.label', default: 'Fase')}" />
        <title><g:message code="default.list.label" args="[entityName]" /></title>
    </head>
    <body>
        <div class="nav">
            <span class="menuButton"><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></span>
            <span class="menuButton"><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></span>
        </div>
        <div class="body">
            <h1><g:message code="default.list.label" args="[entityName]" /></h1>
            <g:if test="${flash.message}">
            <div class="message">${flash.message}</div>
            </g:if>
            <div class="list">
                <table>
                    <thead>
                        <tr>
                        
                            <g:sortableColumn property="id" title="${message(code: 'fase.id.label', default: 'Id')}" />
                        
                            <g:sortableColumn property="descripcion" title="${message(code: 'fase.descripcion.label', default: 'Descripcion')}" />
                        
                            <g:sortableColumn property="tooltip" title="${message(code: 'fase.tooltip.label', default: 'Tooltip')}" />
                        
                            <g:sortableColumn property="controllerNombre" title="${message(code: 'fase.controllerNombre.label', default: 'Controller Nombre')}" />
                        
                            <g:sortableColumn property="codigoFase" title="${message(code: 'fase.codigoFase.label', default: 'Codigo Fase')}" />
                        
                            <th><g:message code="fase.macroEstado.label" default="Macro Estado" /></th>
                        
                        </tr>
                    </thead>
                    <tbody>
                    <g:each in="${faseInstanceList}" status="i" var="faseInstance">
                        <tr class="${(i % 2) == 0 ? 'odd' : 'even'}">
                        
                            <td><g:link action="show" id="${faseInstance.id}">${fieldValue(bean: faseInstance, field: "id")}</g:link></td>
                        
                            <td>${fieldValue(bean: faseInstance, field: "descripcion")}</td>
                        
                            <td>${fieldValue(bean: faseInstance, field: "tooltip")}</td>
                        
                            <td>${fieldValue(bean: faseInstance, field: "controllerNombre")}</td>
                        
                            <td>${fieldValue(bean: faseInstance, field: "codigoFase")}</td>
                        
                            <td>${fieldValue(bean: faseInstance, field: "macroEstado")}</td>
                        
                        </tr>
                    </g:each>
                    </tbody>
                </table>
            </div>
            <div class="paginateButtons">
                <g:paginate total="${faseInstanceTotal}" />
            </div>
        </div>
    </body>
</html>
