
<%@ page import="ar.com.telecom.pcs.entities.Fase" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="layout" content="main" />
        <g:set var="entityName" value="${message(code: 'fase.label', default: 'Fase')}" />
        <title><g:message code="default.show.label" args="[entityName]" /></title>
    </head>
    <body>
        <div class="nav">
            <span class="menuButton"><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></span>
            <span class="menuButton"><g:link class="list" action="list"><g:message code="default.list.label" args="[entityName]" /></g:link></span>
            <span class="menuButton"><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></span>
        </div>
        <div class="body">
            <h1><g:message code="default.show.label" args="[entityName]" /></h1>
            <g:if test="${flash.message}">
            <div class="message">${flash.message}</div>
            </g:if>
            <div class="dialog">
                <table>
                    <tbody>
                    
                        <tr class="prop">
                            <td valign="top" class="name"><g:message code="fase.id.label" default="Id" /></td>
                            
                            <td valign="top" class="value">${fieldValue(bean: faseInstance, field: "id")}</td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name"><g:message code="fase.descripcion.label" default="Descripcion" /></td>
                            
                            <td valign="top" class="value">${fieldValue(bean: faseInstance, field: "descripcion")}</td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name"><g:message code="fase.tooltip.label" default="Tooltip" /></td>
                            
                            <td valign="top" class="value">${fieldValue(bean: faseInstance, field: "tooltip")}</td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name"><g:message code="fase.controllerNombre.label" default="Controller Nombre" /></td>
                            
                            <td valign="top" class="value">${fieldValue(bean: faseInstance, field: "controllerNombre")}</td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name"><g:message code="fase.codigoFase.label" default="Codigo Fase" /></td>
                            
                            <td valign="top" class="value">${fieldValue(bean: faseInstance, field: "codigoFase")}</td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name"><g:message code="fase.macroEstado.label" default="Macro Estado" /></td>
                            
                            <td valign="top" class="value"><g:link controller="macroEstado" action="show" id="${faseInstance?.macroEstado?.id}">${faseInstance?.macroEstado?.encodeAsHTML()}</g:link></td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name"><g:message code="fase.faseDenegacion.label" default="Fase Denegacion" /></td>
                            
                            <td valign="top" class="value"><g:link controller="fase" action="show" id="${faseInstance?.faseDenegacion?.id}">${faseInstance?.faseDenegacion?.encodeAsHTML()}</g:link></td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name"><g:message code="fase.fasePadre.label" default="Fase Padre" /></td>
                            
                            <td valign="top" class="value"><g:link controller="fase" action="show" id="${faseInstance?.fasePadre?.id}">${faseInstance?.fasePadre?.encodeAsHTML()}</g:link></td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name"><g:message code="fase.aplicaPadre.label" default="Aplica Padre" /></td>
                            
                            <td valign="top" class="value"><g:formatBoolean boolean="${faseInstance?.aplicaPadre}" /></td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name"><g:message code="fase.descripcionEnBaseAHijos.label" default="Descripcion En Base AH ijos" /></td>
                            
                            <td valign="top" class="value"><g:formatBoolean boolean="${faseInstance?.descripcionEnBaseAHijos}" /></td>
                            
                        </tr>
                    
                    </tbody>
                </table>
            </div>
            <div class="buttons">
                <g:form>
                    <g:hiddenField name="id" value="${faseInstance?.id}" />
                    <span class="button"><g:actionSubmit class="edit" action="edit" value="${message(code: 'default.button.edit.label', default: 'Edit')}" /></span>
                    <span class="button"><g:actionSubmit class="delete" action="delete" value="${message(code: 'default.button.delete.label', default: 'Delete')}" onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');" /></span>
                </g:form>
            </div>
        </div>
    </body>
</html>
