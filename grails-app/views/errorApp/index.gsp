<%@ page import="org.codehaus.groovy.grails.commons.ConfigurationHolder"%>
<html>
<head>
<title>NeedIt</title>
<meta name="layout" content="mainApp" />
<link rel="stylesheet" href="${resource(dir:'css',file:'error.css')}" />
<style type="text/css">
.message {
	border: 1px solid black;
	padding: 5px;
	background-color: #E9E9E9;
}

.stack {
	border: 1px solid black;
	padding: 5px;
	overflow: auto;
	height: 300px;
}

.snippet {
	padding: 5px;
	background-color: white;
	border: 1px solid black;
	margin: 3px;
	font-family: courier;
}
</style> 
</head>
<body>

		<div id="errorBody" class="<g:errorClass type='${params?.type}'/>">
				<h4>
						<g:if test="${params?.msg}">
								<div id="errorBody-mensaje">
										${params?.msg}
								</div>
						</g:if>
						<br/>
						<g:if test="${systemError}">
							<g:codigoError />
						</g:if>
				</h4>
				<div id="errorBody-footer">
					<g:link controller="inbox" action="index">Ir al Inbox</g:link>
				</div>
				</h5>
		</div>

</body>
</html>