<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta name="layout" content="layoutLogin" />
</head>
<body>
	<web:isBlackberry>
		<g:link class="link-entrar" controller="inboxMobile" action="index">
			<img src="${resource(dir:'mobile',file:'mobile-btn-entrar.png')}" />
		</g:link>
	</web:isBlackberry>

	<web:isNOTBlackberry>
		<div>
		<div id="Login">
			<p style="margin-top: 65px; margin-left: 150px; text-align: right;">
				<g:link class="indexEntrar" controller="inbox" action="index">
					<img src="${resource(dir:'images',file:'entrar.png')}" />
					<span>Entrar</span>
				</g:link>
			</p>
		</div>
			<div class='login_message'>Usted no tiene accesos a la
				aplicaci�n. Por favor gestione los roles por <a href="http://tuid">TuID</a>.</div>
		</div>
	</web:isNOTBlackberry>

</body> 
</html>
