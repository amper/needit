<g:updateToken/>
<%@ page import="ar.com.telecom.exceptions.ConcurrentAccessException"%>
<%@ page import="ar.com.telecom.exceptions.BusinessException"%>
<%@ page import="ar.com.telecom.exceptions.SessionExpiredException"%>
<%@ page import="org.codehaus.groovy.grails.commons.ConfigurationHolder"%>
<html>
<head>
<title>NeedIt</title>
<meta name="layout" content="mainApp" />
<style type="text/css">
.message {
	border: 1px solid black;
	padding: 5px;
	background-color: #E9E9E9;
}

.stack {
	border: 1px solid black;
	padding: 5px;
	overflow: auto;
	height: 300px;
}

.snippet { 
	padding: 5px;
	background-color: white;
	border: 1px solid black;
	margin: 3px;
	font-family: courier;
}
</style>
</head>

<body>
	<div class="${contenido}">
		<h5>
			${mensajeError}
			<br>
			<br>
			<g:if test="${systemError}">
				<g:codigoError/> 
			</g:if>
		</h5>
		<h5>
			<br>
			<g:link controller="inbox" action="index">Ir al Inbox</g:link>
			<g:if test="${link}">
				 ${link}
			</g:if>			
		</h5>
		<g:if env="development" test="${systemError}">
			<br>
			<h5>Stack Trace</h5>
				<div>
					<div>
						${params}
					</div>
					<g:each in="${exception.stackTraceLines}">
						${it.encodeAsHTML()}<br />
					</g:each>
				</div>
		</g:if>
	</div>
</body>
</html>