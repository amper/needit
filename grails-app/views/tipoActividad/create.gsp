

<%@ page import="ar.com.telecom.pcs.entities.TipoActividad" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="layout" content="main" />
        <g:set var="entityName" value="${message(code: 'tipoActividad.label', default: 'TipoActividad')}" />
        <title><g:message code="default.create.label" args="[entityName]" /></title>
    </head>
    <body>
        <div class="nav">
            <span class="menuButton"><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></span>
            <span class="menuButton"><g:link class="list" action="list"><g:message code="default.list.label" args="[entityName]" /></g:link></span>
        </div>
        <div class="body">
            <h1><g:message code="default.create.label" args="[entityName]" /></h1>
            <g:if test="${flash.message}">
            <div class="message">${flash.message}</div>
            </g:if>
            <g:hasErrors bean="${tipoActividadInstance}">
            <div class="errors">
                <g:renderErrors bean="${tipoActividadInstance}" as="list" />
            </div>
            </g:hasErrors>
            <g:form action="save" >
                <div class="dialog">
                    <table>
                        <tbody>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="descripcion"><g:message code="tipoActividad.descripcion.label" default="Descripcion" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: tipoActividadInstance, field: 'descripcion', 'errors')}">
                                    <g:textField name="descripcion" maxlength="70" value="${tipoActividadInstance?.descripcion}" />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="estrategiaComentarios"><g:message code="tipoActividad.estrategiaComentarios.label" default="Estrategia Comentarios" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: tipoActividadInstance, field: 'estrategiaComentarios', 'errors')}">
                                    <g:textField name="estrategiaComentarios" maxlength="5" value="${tipoActividadInstance?.estrategiaComentarios}" />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="estrategiaFechaSugerida"><g:message code="tipoActividad.estrategiaFechaSugerida.label" default="Estrategia Fecha Sugerida" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: tipoActividadInstance, field: 'estrategiaFechaSugerida', 'errors')}">
                                    <g:textField name="estrategiaFechaSugerida" maxlength="5" value="${tipoActividadInstance?.estrategiaFechaSugerida}" />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="estrategiaAnexos"><g:message code="tipoActividad.estrategiaAnexos.label" default="Estrategia Anexos" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: tipoActividadInstance, field: 'estrategiaAnexos', 'errors')}">
                                    <g:textField name="estrategiaAnexos" maxlength="5" value="${tipoActividadInstance?.estrategiaAnexos}" />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="estrategiaPasajes"><g:message code="tipoActividad.estrategiaPasajes.label" default="Estrategia Pasajes" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: tipoActividadInstance, field: 'estrategiaPasajes', 'errors')}">
                                    <g:textField name="estrategiaPasajes" maxlength="5" value="${tipoActividadInstance?.estrategiaPasajes}" />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="estrategiaAmbiente"><g:message code="tipoActividad.estrategiaAmbiente.label" default="Estrategia Ambiente" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: tipoActividadInstance, field: 'estrategiaAmbiente', 'errors')}">
                                    <g:textField name="estrategiaAmbiente" maxlength="5" value="${tipoActividadInstance?.estrategiaAmbiente}" />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="estrategiaVersionador"><g:message code="tipoActividad.estrategiaVersionador.label" default="Estrategia Versionador" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: tipoActividadInstance, field: 'estrategiaVersionador', 'errors')}">
                                    <g:textField name="estrategiaVersionador" maxlength="5" value="${tipoActividadInstance?.estrategiaVersionador}" />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="estrategiaMercury"><g:message code="tipoActividad.estrategiaMercury.label" default="Estrategia Mercury" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: tipoActividadInstance, field: 'estrategiaMercury', 'errors')}">
                                    <g:textField name="estrategiaMercury" maxlength="5" value="${tipoActividadInstance?.estrategiaMercury}" />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="estrategiaRelease"><g:message code="tipoActividad.estrategiaRelease.label" default="Estrategia Release" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: tipoActividadInstance, field: 'estrategiaRelease', 'errors')}">
                                    <g:textField name="estrategiaRelease" maxlength="5" value="${tipoActividadInstance?.estrategiaRelease}" />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="estrategiaURL"><g:message code="tipoActividad.estrategiaURL.label" default="Estrategia URL" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: tipoActividadInstance, field: 'estrategiaURL', 'errors')}">
                                    <g:textField name="estrategiaURL" maxlength="5" value="${tipoActividadInstance?.estrategiaURL}" />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="obligatoriedad"><g:message code="tipoActividad.obligatoriedad.label" default="Obligatoriedad" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: tipoActividadInstance, field: 'obligatoriedad', 'errors')}">
                                    <g:textField name="obligatoriedad" maxlength="2" value="${tipoActividadInstance?.obligatoriedad}" />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="criterioTailoring"><g:message code="tipoActividad.criterioTailoring.label" default="Criterio Tailoring" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: tipoActividadInstance, field: 'criterioTailoring', 'errors')}">
                                    <g:select name="criterioTailoring.id" from="${ar.com.telecom.pcs.entities.CriterioTailoring.list()}" optionKey="id" value="${tipoActividadInstance?.criterioTailoring?.id}" noSelection="['null': '']" />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="criterioValidacionActividad"><g:message code="tipoActividad.criterioValidacionActividad.label" default="Criterio Validacion Actividad" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: tipoActividadInstance, field: 'criterioValidacionActividad', 'errors')}">
                                    <g:select name="criterioValidacionActividad.id" from="${ar.com.telecom.pcs.entities.CriterioValidacionActividad.list()}" optionKey="id" value="${tipoActividadInstance?.criterioValidacionActividad?.id}" noSelection="['null': '']" />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="fase"><g:message code="tipoActividad.fase.label" default="Fase" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: tipoActividadInstance, field: 'fase', 'errors')}">
                                    <g:select name="fase.id" from="${ar.com.telecom.pcs.entities.Fase.list()}" optionKey="id" value="${tipoActividadInstance?.fase?.id}"  />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="tipoGestion"><g:message code="tipoActividad.tipoGestion.label" default="Tipo Gestion" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: tipoActividadInstance, field: 'tipoGestion', 'errors')}">
                                    <g:select name="tipoGestion.id" from="${ar.com.telecom.pcs.entities.TipoGestion.list()}" optionKey="id" value="${tipoActividadInstance?.tipoGestion?.id}"  />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="tipoImpacto"><g:message code="tipoActividad.tipoImpacto.label" default="Tipo Impacto" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: tipoActividadInstance, field: 'tipoImpacto', 'errors')}">
                                    <g:select name="tipoImpacto.id" from="${ar.com.telecom.pcs.entities.TipoImpacto.list()}" optionKey="id" value="${tipoActividadInstance?.tipoImpacto?.id}"  />
                                </td>
                            </tr>
                        
                        </tbody>
                    </table>
                </div>
                <div class="buttons">
                    <span class="button"><g:submitButton name="create" class="save" value="${message(code: 'default.button.create.label', default: 'Create')}" /></span>
                </div>
            </g:form>
        </div>
    </body>
</html>
