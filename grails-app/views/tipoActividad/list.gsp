
<%@ page import="ar.com.telecom.pcs.entities.TipoActividad" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="layout" content="main" />
        <g:set var="entityName" value="${message(code: 'tipoActividad.label', default: 'TipoActividad')}" />
        <title><g:message code="default.list.label" args="[entityName]" /></title>
    </head>
    <body>
        <div class="nav">
            <span class="menuButton"><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></span>
            <span class="menuButton"><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></span>
        </div>
        <div class="body">
            <h1><g:message code="default.list.label" args="[entityName]" /></h1>
            <g:if test="${flash.message}">
            <div class="message">${flash.message}</div>
            </g:if>
            <div class="list">
                <table>
                    <thead>
                        <tr>
                        
                            <g:sortableColumn property="id" title="${message(code: 'tipoActividad.id.label', default: 'Id')}" />
                        
                            <g:sortableColumn property="descripcion" title="${message(code: 'tipoActividad.descripcion.label', default: 'Descripcion')}" />
                        
                            <g:sortableColumn property="estrategiaComentarios" title="${message(code: 'tipoActividad.estrategiaComentarios.label', default: 'Estrategia Comentarios')}" />
                        
                            <g:sortableColumn property="estrategiaFechaSugerida" title="${message(code: 'tipoActividad.estrategiaFechaSugerida.label', default: 'Estrategia Fecha Sugerida')}" />
                        
                            <g:sortableColumn property="estrategiaAnexos" title="${message(code: 'tipoActividad.estrategiaAnexos.label', default: 'Estrategia Anexos')}" />
                        
                            <g:sortableColumn property="estrategiaPasajes" title="${message(code: 'tipoActividad.estrategiaPasajes.label', default: 'Estrategia Pasajes')}" />
                        
                        </tr>
                    </thead>
                    <tbody>
                    <g:each in="${tipoActividadInstanceList}" status="i" var="tipoActividadInstance">
                        <tr class="${(i % 2) == 0 ? 'odd' : 'even'}">
                        
                            <td><g:link action="show" id="${tipoActividadInstance.id}">${fieldValue(bean: tipoActividadInstance, field: "id")}</g:link></td>
                        
                            <td>${fieldValue(bean: tipoActividadInstance, field: "descripcion")}</td>
                        
                            <td>${fieldValue(bean: tipoActividadInstance, field: "estrategiaComentarios")}</td>
                        
                            <td>${fieldValue(bean: tipoActividadInstance, field: "estrategiaFechaSugerida")}</td>
                        
                            <td>${fieldValue(bean: tipoActividadInstance, field: "estrategiaAnexos")}</td>
                        
                            <td>${fieldValue(bean: tipoActividadInstance, field: "estrategiaPasajes")}</td>
                        
                        </tr>
                    </g:each>
                    </tbody>
                </table>
            </div>
            <div class="paginateButtons">
                <g:paginate total="${tipoActividadInstanceTotal}" />
            </div>
        </div>
    </body>
</html>
