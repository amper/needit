
<%@ page import="ar.com.telecom.pcs.entities.TipoActividad" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="layout" content="main" />
        <g:set var="entityName" value="${message(code: 'tipoActividad.label', default: 'TipoActividad')}" />
        <title><g:message code="default.show.label" args="[entityName]" /></title>
    </head>
    <body>
        <div class="nav">
            <span class="menuButton"><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></span>
            <span class="menuButton"><g:link class="list" action="list"><g:message code="default.list.label" args="[entityName]" /></g:link></span>
            <span class="menuButton"><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></span>
        </div>
        <div class="body">
            <h1><g:message code="default.show.label" args="[entityName]" /></h1>
            <g:if test="${flash.message}">
            <div class="message">${flash.message}</div>
            </g:if>
            <div class="dialog">
                <table>
                    <tbody>
                    
                        <tr class="prop">
                            <td valign="top" class="name"><g:message code="tipoActividad.id.label" default="Id" /></td>
                            
                            <td valign="top" class="value">${fieldValue(bean: tipoActividadInstance, field: "id")}</td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name"><g:message code="tipoActividad.descripcion.label" default="Descripcion" /></td>
                            
                            <td valign="top" class="value">${fieldValue(bean: tipoActividadInstance, field: "descripcion")}</td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name"><g:message code="tipoActividad.estrategiaComentarios.label" default="Estrategia Comentarios" /></td>
                            
                            <td valign="top" class="value">${fieldValue(bean: tipoActividadInstance, field: "estrategiaComentarios")}</td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name"><g:message code="tipoActividad.estrategiaFechaSugerida.label" default="Estrategia Fecha Sugerida" /></td>
                            
                            <td valign="top" class="value">${fieldValue(bean: tipoActividadInstance, field: "estrategiaFechaSugerida")}</td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name"><g:message code="tipoActividad.estrategiaAnexos.label" default="Estrategia Anexos" /></td>
                            
                            <td valign="top" class="value">${fieldValue(bean: tipoActividadInstance, field: "estrategiaAnexos")}</td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name"><g:message code="tipoActividad.estrategiaPasajes.label" default="Estrategia Pasajes" /></td>
                            
                            <td valign="top" class="value">${fieldValue(bean: tipoActividadInstance, field: "estrategiaPasajes")}</td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name"><g:message code="tipoActividad.estrategiaAmbiente.label" default="Estrategia Ambiente" /></td>
                            
                            <td valign="top" class="value">${fieldValue(bean: tipoActividadInstance, field: "estrategiaAmbiente")}</td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name"><g:message code="tipoActividad.estrategiaVersionador.label" default="Estrategia Versionador" /></td>
                            
                            <td valign="top" class="value">${fieldValue(bean: tipoActividadInstance, field: "estrategiaVersionador")}</td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name"><g:message code="tipoActividad.estrategiaMercury.label" default="Estrategia Mercury" /></td>
                            
                            <td valign="top" class="value">${fieldValue(bean: tipoActividadInstance, field: "estrategiaMercury")}</td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name"><g:message code="tipoActividad.estrategiaRelease.label" default="Estrategia Release" /></td>
                            
                            <td valign="top" class="value">${fieldValue(bean: tipoActividadInstance, field: "estrategiaRelease")}</td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name"><g:message code="tipoActividad.estrategiaURL.label" default="Estrategia URL" /></td>
                            
                            <td valign="top" class="value">${fieldValue(bean: tipoActividadInstance, field: "estrategiaURL")}</td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name"><g:message code="tipoActividad.obligatoriedad.label" default="Obligatoriedad" /></td>
                            
                            <td valign="top" class="value">${fieldValue(bean: tipoActividadInstance, field: "obligatoriedad")}</td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name"><g:message code="tipoActividad.criterioTailoring.label" default="Criterio Tailoring" /></td>
                            
                            <td valign="top" class="value"><g:link controller="criterioTailoring" action="show" id="${tipoActividadInstance?.criterioTailoring?.id}">${tipoActividadInstance?.criterioTailoring?.encodeAsHTML()}</g:link></td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name"><g:message code="tipoActividad.criterioValidacionActividad.label" default="Criterio Validacion Actividad" /></td>
                            
                            <td valign="top" class="value"><g:link controller="criterioValidacionActividad" action="show" id="${tipoActividadInstance?.criterioValidacionActividad?.id}">${tipoActividadInstance?.criterioValidacionActividad?.encodeAsHTML()}</g:link></td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name"><g:message code="tipoActividad.fase.label" default="Fase" /></td>
                            
                            <td valign="top" class="value"><g:link controller="fase" action="show" id="${tipoActividadInstance?.fase?.id}">${tipoActividadInstance?.fase?.encodeAsHTML()}</g:link></td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name"><g:message code="tipoActividad.tipoGestion.label" default="Tipo Gestion" /></td>
                            
                            <td valign="top" class="value"><g:link controller="tipoGestion" action="show" id="${tipoActividadInstance?.tipoGestion?.id}">${tipoActividadInstance?.tipoGestion?.encodeAsHTML()}</g:link></td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name"><g:message code="tipoActividad.tipoImpacto.label" default="Tipo Impacto" /></td>
                            
                            <td valign="top" class="value"><g:link controller="tipoImpacto" action="show" id="${tipoActividadInstance?.tipoImpacto?.id}">${tipoActividadInstance?.tipoImpacto?.encodeAsHTML()}</g:link></td>
                            
                        </tr>
                    
                    </tbody>
                </table>
            </div>
            <div class="buttons">
                <g:form>
                    <g:hiddenField name="id" value="${tipoActividadInstance?.id}" />
                    <span class="button"><g:actionSubmit class="edit" action="edit" value="${message(code: 'default.button.edit.label', default: 'Edit')}" /></span>
                    <span class="button"><g:actionSubmit class="delete" action="delete" value="${message(code: 'default.button.delete.label', default: 'Delete')}" onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');" /></span>
                </g:form>
            </div>
        </div>
    </body>
</html>
