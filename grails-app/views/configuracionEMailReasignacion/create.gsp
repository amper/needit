

<%@ page import="ar.com.telecom.pcs.entities.ConfiguracionEMailReasignacion" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="layout" content="main" />
        <g:set var="entityName" value="${message(code: 'configuracionEMailReasignacion.label', default: 'ConfiguracionEMailReasignacion')}" />
        <g:javascript library="tiny_mce/tiny_mce" />
        <title><g:message code="default.create.label" args="[entityName]" /></title>
<script type="text/javascript">
tinyMCE.init({
    mode : "exact",
    elements : "texto",
    theme : "advanced",
	skin : "o2k7",
    plugins : "autolink,lists,pagebreak,style,layer,table,save,advhr,advimage,advlink,emotions,iespell,inlinepopups,insertdatetime,preview,media,searchreplace,print,contextmenu,paste,directionality,fullscreen,noneditable,visualchars,nonbreaking,xhtmlxtras,template,wordcount,advlist", 
            
    // Theme options - button# indicated the row# only
    theme_advanced_buttons1 : "newdocument,print,|,bold,italic,underline,|,fontsizeselect,|,cut,copy,paste,|,bullist,numlist,|,undo,redo,|,forecolor,backcolor,|,image,|,fullscreen,template",
    theme_advanced_buttons2 : "",
    theme_advanced_buttons3 : "",      
    theme_advanced_toolbar_location : "top",
    theme_advanced_toolbar_align : "left",
    theme_advanced_statusbar_location : "bottom",
    theme_advanced_resizing : true,
    paste_text_sticky : true,
    setup : function(ed) {
        ed.onInit.add(function(ed) {
          ed.pasteAsPlainText = true;
        });
     },
     
	template_external_list_url : "lists/template_list.js",

		//template_external_list_url : "lists/template_list.js",
		external_link_list_url : "lists/link_list.js",
		external_image_list_url : "lists/image_list.js",
		media_external_list_url : "lists/media_list.js"
});

</script>         
        
    </head>
    <body>
        <div class="nav">
            <span class="menuButton"><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></span>
            <span class="menuButton"><g:link class="list" action="list"><g:message code="default.list.label" args="[entityName]" /></g:link></span>
        </div>
        <div class="body">
            <h1><g:message code="default.create.label" args="[entityName]" /></h1>
            <g:if test="${flash.message}">
            <div class="message">${flash.message}</div>
            </g:if>
            <g:hasErrors bean="${configuracionEMailReasignacionInstance}">
            <div class="errors">
                <g:renderErrors bean="${configuracionEMailReasignacionInstance}" as="list" />
            </div>
            </g:hasErrors>
            <g:form action="save" >
                <div class="dialog">
                    <table>
                        <tbody>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="asunto"><g:message code="configuracionEMailReasignacion.asunto.label" default="Asunto" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: configuracionEMailReasignacionInstance, field: 'asunto', 'errors')}">
                                    <g:textArea name="asunto" cols="40" rows="5" value="${configuracionEMailReasignacionInstance?.asunto}" />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="fase"><g:message code="configuracionEMailReasignacion.fase.label" default="Fase" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: configuracionEMailReasignacionInstance, field: 'fase', 'errors')}">
                                    <g:select name="fase.id" from="${ar.com.telecom.pcs.entities.Fase.list()}" optionKey="id" value="${configuracionEMailReasignacionInstance?.fase?.id}" noSelection="['null': '']" />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="actual"><g:message code="configuracionEMailReasignacion.actual.label" default="Actual" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: configuracionEMailReasignacionInstance, field: 'actual', 'errors')}">
                                    <g:checkBox name="actual" value="${configuracionEMailReasignacionInstance?.actual}" />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="rol"><g:message code="configuracionEMailReasignacion.rol.label" default="Rol" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: configuracionEMailReasignacionInstance, field: 'rol', 'errors')}">
                                    <g:select name="rol.id" from="${ar.com.telecom.pcs.entities.RolAplicacion.list()}" optionKey="id" value="${configuracionEMailReasignacionInstance?.rol?.id}"  />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="texto"><g:message code="configuracionEMailReasignacion.texto.label" default="Texto" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: configuracionEMailReasignacionInstance, field: 'texto', 'errors')}">
                                    <g:textField name="texto" value="${configuracionEMailReasignacionInstance?.texto}" />
                                </td>
                            </tr>
                        
                        </tbody>
                    </table>
                </div>
                <div class="buttons">
                    <span class="button"><g:submitButton name="create" class="save" value="${message(code: 'default.button.create.label', default: 'Create')}" /></span>
                </div>
            </g:form>
        </div>
    </body>
</html>
