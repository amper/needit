
<%@ page import="ar.com.telecom.pcs.entities.ConfiguracionEMailReasignacion" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="layout" content="main" />
        <g:set var="entityName" value="${message(code: 'configuracionEMailReasignacion.label', default: 'ConfiguracionEMailReasignacion')}" />
        <title><g:message code="default.list.label" args="[entityName]" /></title>
    </head>
    <body>
        <div class="nav">
            <span class="menuButton"><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></span>
            <span class="menuButton"><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></span>
        </div>
        <div class="body">
            <h1><g:message code="default.list.label" args="[entityName]" /></h1>
            <g:if test="${flash.message}">
            <div class="message">${flash.message}</div>
            </g:if>
            <div class="list">
                <table>
                    <thead>
                        <tr>
                        
                            <g:sortableColumn property="id" title="${message(code: 'configuracionEMailReasignacion.id.label', default: 'Id')}" />
                        
                            <g:sortableColumn property="asunto" title="${message(code: 'configuracionEMailReasignacion.asunto.label', default: 'Asunto')}" />
                        
                            <th><g:message code="configuracionEMailReasignacion.fase.label" default="Fase" /></th>
                        
                            <g:sortableColumn property="actual" title="${message(code: 'configuracionEMailReasignacion.actual.label', default: 'Actual')}" />
                        
                            <th><g:message code="configuracionEMailReasignacion.rol.label" default="Rol" /></th>
                        
                            <g:sortableColumn property="texto" title="${message(code: 'configuracionEMailReasignacion.texto.label', default: 'Texto')}" />
                        
                        </tr>
                    </thead>
                    <tbody>
                    <g:each in="${configuracionEMailReasignacionInstanceList}" status="i" var="configuracionEMailReasignacionInstance">
                        <tr class="${(i % 2) == 0 ? 'odd' : 'even'}">
                        
                            <td><g:link action="show" id="${configuracionEMailReasignacionInstance.id}">${fieldValue(bean: configuracionEMailReasignacionInstance, field: "id")}</g:link></td>
                        
                            <td>${fieldValue(bean: configuracionEMailReasignacionInstance, field: "asunto")}</td>
                        
                            <td>${fieldValue(bean: configuracionEMailReasignacionInstance, field: "fase")}</td>
                        
                            <td><g:formatBoolean boolean="${configuracionEMailReasignacionInstance.actual}" /></td>
                        
                            <td>${fieldValue(bean: configuracionEMailReasignacionInstance, field: "rol")}</td>
                        
                            <td>${fieldValue(bean: configuracionEMailReasignacionInstance, field: "texto")}</td>
                        
                        </tr>
                    </g:each>
                    </tbody>
                </table>
            </div>
            <div class="paginateButtons">
                <g:paginate total="${configuracionEMailReasignacionInstanceTotal}" />
            </div>
        </div>
    </body>
</html>
