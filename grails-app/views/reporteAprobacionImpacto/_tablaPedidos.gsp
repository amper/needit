<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html>
<head>
<meta http-equiv="Content-type" content="text/html; charset=UTF-8" />
<meta http-equiv="X-UA-Compatible" content="IE=8" /> 
<style type="text/css" media="print">

@page {
	margin: 0in;
	size: landscape;
}

.titulo {
	color: #444;
	font-size: 16px;
	text-align: center;
	align: "center";
}

table {
	cellspacing: 0;
	cellpadding: 0;
	border-collapse: collapse;
	width: 100%;
}

table, table td {
	border: 1px solid #AAAAAA;
}

table.resumen td{
	background: #3e8a9a;
	font-weight: bold;
	color: #FFFFFF;
}
 
body {
	font-size: 11px;
	font-family: arial, helvetica;
}

.separador {
	clear: both;
	height: 10px;
}

.headerPedidos{
	background: #c1d5d9;
}

.headerPedidos td{
	height: 20px;
}

.headerPedidos th{
	height: 20px;
	font-weight: bold;
	border: 1px solid #aaaaaa;
}

.odd {
	background: #e0e9eb;
}

.even {
	background: #fff;
}

.gestion {
	background: #3e8a9a;
	font-weight: bold;
	padding: 5px;
	width: 99%;
	color: #FFFFFF;
}

</style>
</head>
<body>
	<div>
		<div class="titulo">
			Reporte de Aprobaci&oacute;n de Impacto - ${reporte?.usuarios[0]?.direccion} (${fechaReporte})
		</div>
		
		<div class="separador">&nbsp;</div>
		
		<g:if test="${reporte.pedidosEstandar}">
			<g:render template="/reporteAprobacionImpacto/tablaPedido" model="['pedidos':reporte.pedidosEstandar, 'esEmergencia':false, 'reporte':reporte]"></g:render>
			<div class="separador">&nbsp;</div>
		</g:if>
		<g:if test="${reporte.pedidosEmergencia}">
			<g:render template="/reporteAprobacionImpacto/tablaPedido" model="['pedidos':reporte.pedidosEmergencia, 'esEmergencia':true, 'reporte':reporte]"></g:render>
			<div class="separador">&nbsp;</div>
		</g:if>
		<g:if test="${nota}">
			<div>
				<u>Notas:</u><br />
				${nota}
			</div>
		</g:if>
	</div>
	<div style="clear: both;"></div>
</body>
</html>