<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "_http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@page import="ar.com.telecom.util.NumberUtil"%> 
<html>
<head>
<meta http-equiv="Content-type" content="text/html; charset=UTF-8" />
<meta http-equiv="X-UA-Compatible" content="IE=8" /> 
<title>Reporte de Aprobaci&oacute;n de Impacto</title>
<style>

.titulo {
	color: #444;
	font-size: 16px;
	text-align: left;
}

.encabezado {
	float: left;
	font-size: 14x;
	text-align: left; 
}

table {
	cellspacing: 0;
	cellpadding: 0;
	border-collapse: collapse;
	width: 100%;
}
 
<%--table, table td {--%>
<%--	border: 1px solid #AAAAAA;--%>
<%--}--%>

body {
	font-size: 11px;
	font-family: arial, helvetica;
}

.separador {
	clear: both;
	height: 20px;
}

</style>
</head>
<body>
	<g:each in="${reportes}" var="reporte">
		<g:render template="/reporteAprobacionImpacto/email" model="['reporte':reporte]"></g:render>
	</g:each>
</body>
</html>