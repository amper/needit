<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<%@page import="ar.com.telecom.util.NumberUtil"%>
<%@page import="ar.com.telecom.util.DateUtil"%>  
<html>
<head>
<meta http-equiv="Content-type" content="text/html; charset=UTF-8" />
<meta http-equiv="X-UA-Compatible" content="IE=8" /> 
<title>Reporte de Aprobaci&oacute;n de Impacto</title>
<style>

.titulo {
	color: #444;
	font-size: 16px;
	text-align: left;
}

.encabezado {
	float: left;
	font-size: 14x;
	text-align: left; 
}
 
table {
	cellspacing: 0;
	cellpadding: 0;
	border-collapse: collapse;
	width: 50%;
}

table.resumen td{
	background: #FFFFFF;
	font-weight: bold;
	color: #000000;
}

table.resumen td.valores{
	background: #FFFFFF;
	font-weight: normal;
	color: #000000;
	text-align: right; 
	border: none; 
}

</style>
</head>
<body>
	<div>
		<div class="titulo">
			Reporte de Aprobaci&oacute;n de Impacto - ${reporte?.usuarios[0]?.direccion} (${fechaReporte})
		</div>
		
		<div class="separador">&nbsp;</div>
		
		
		<g:if test="${reporte.pedidosEstandar || reporte.pedidosEmergencia}">
			<div class="separador">&nbsp;</div>
			<table class="resumen">
				<tr>
					<td>Cantidad de pedidos: ${reporte.pedidosEstandar.size()+reporte.pedidosEmergencia.size()}</td>
				</tr>
				<tr>
					<td>Monto total aprobado: <g:formatNumber number="${reporte.getTotal(reporte?.pedidosEstandar)+reporte.getTotal(reporte?.pedidosEmergencia)}" type="currency" currencyCode="ARS" /></td>
				</tr>
				<tr>
					<td>Horas totales aprobadas: ${reporte.getTotalHoras(reporte?.pedidosEstandar)+reporte.getTotalHoras(reporte?.pedidosEmergencia)}</td>
				</tr>
			</table>	
			
		</g:if>
		<g:else>
			<div class="encabezado">
				No existen cambios estimados y aprobados para el mes del asunto, originados por integrantes de esta Direcci&oacute;n.
			</div>
		</g:else>
	</div>
	<div style="clear: both;"></div>
</body>
</html>