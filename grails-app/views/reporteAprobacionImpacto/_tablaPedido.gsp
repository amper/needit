<%@page import="ar.com.telecom.util.NumberUtil"%>
<%@page import="ar.com.telecom.util.DateUtil"%>  

<div class="gestion">
	Gesti&oacute;n ${esEmergencia.toBoolean()?"Emergencia":"Estandar"}
</div>

<div class="separador">&nbsp;</div>

<table class="headerPedidos">
	<tr>
		<th align="left">N&uacute;mero</th>
		<th align="left">T&iacute;tulo</th>
		<th align="center">Horas</th>
		<th align="center">Monto</th>
		<th align="left">Aprob&oacute; impacto</th>
		<th align="left">Interlocutor Usuario</th>
		<th align="center">Fecha  de aprobaci&oacute;n</th>
		<th align="center">Fecha fin planificada</th>
		<th align="left">Tipo de pedido</th>
	</tr> 
	<g:each in="${pedidos}" var="pedido" status="i">
		<tr class="${(i % 2) == 0 ? 'odd' : 'even'}">
			<td align="right">${pedido.numero}</td>
			<td align="left">${org.apache.commons.lang.StringEscapeUtils.escapeHtml(pedido.titulo)}</td>
			<td align="right">${pedido.totalHorasPlanificadas()}</td>
			<td align="right"><g:formatNumber number="${pedido.totalGeneral()}" type="currency" currencyCode="ARS" /></td>
			<td align="left">${pedido.getNombreApellido(pedido.legajoUsuarioAprobadorEconomico)}</td>
			<td align="left">${pedido.getNombreApellido(pedido.legajoInterlocutorUsuario)}</td>
			<td align="center">${DateUtil.toString(pedido.fechaAprobacionEconomica)}</td>
			<td align="center">${DateUtil.toString(pedido.fechaFinPlanificada)}</td>
			<td align="left">${pedido.tipoPedido}</td>
		</tr>
	</g:each>
	<tr>
		<td colspan="5">Monto total aprobado: <g:formatNumber number="${reporte.getTotal(pedidos)}" type="currency" currencyCode="ARS" /></td>
		<td colspan="4">Total de horas: ${reporte.getTotalHoras(pedidos)}</td>
	</tr>
	<g:if test="${esEmergencia.toBoolean() &&  !pedidos.isEmpty()}">
		<tr>
			<td colspan="9" align="center">(*) Los N/A corresponden a pedidos de cambio de tipo de gesti&oacute;n de Emergencia que no tienen aprobaci&oacute;n de impacto.</td>
		</tr>
	</g:if>
</table>