<div class="formSeccion-content">

	<%--<g:if test="${pedidosHijosNoConsolidados.isEmpty()}">--%>
	<g:if test="${pedidosHijosConsolidados.isEmpty()}">
		<div>No hay pedidos hijos no consolidados</div>
	</g:if>
	<g:else>
	<%--		from="${pedidosHijosNoConsolidados}"--%>
		<g:select id="comboHijosNoConsolidados" name="pedidoHijo.id"
			from="${pedidosHijosConsolidados}"
			optionKey="id"
			noSelection="['null': '']"
		/>
	</g:else>
	
	<div class="separator"></div>
	<div id="datosHijoSeleccionado"></div>
	
<%--	<g:render template="/templates/ultimaJustificacionDenegacion"--%>
<%--		model="['pedidoInstance': pedidoInstance, 'fase': pedidoInstance.faseAprobacionPedido()]">--%>
<%--	</g:render>--%>
	
	<div id="message"></div>
	<div id="botoneraPau" class="formSeccionButton" style="margin-top: 20px; display: none;">
		<g:if test="${pedidoInstance?.puedeEditarAprobacionPAU(usuarioLogueado)}">
			<g:actionSubmit class="formSeccionSubmitButton" id="Simular" action="simularValidacion" value="Simular" />
			<g:actionSubmit class="formSeccionSubmitButton" id="Guardar" action="grabarFormulario" value="Guardar"/>
			<g:actionSubmit class="formSeccionSubmitButton" id="Denegar" action="denegarPedido" value="Denegar" />
			<g:actionSubmit class="formSeccionSubmitButton" id="Aprobar" action="aprobarPedido" value="Aprobar" />
		</g:if>
	</div>
 
</div>