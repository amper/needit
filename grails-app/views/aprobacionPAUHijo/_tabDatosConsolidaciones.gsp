<div class="demo" >
	<div id="tabs" class="demoTabs">
		<ul>
			<li><a href="#tabs-1">PAU Consolidadas</a>
			<li><a href="#tabs-2">PAU No Consolidadas</a>
		</ul>

		<div id="tabs-1">
			<g:render template="hijosConsolidados"></g:render>
		</div>
		
		<div id="tabs-2">
			<g:render template="hijosNoConsolidados"></g:render>
		</div>
	</div>
</div>

 