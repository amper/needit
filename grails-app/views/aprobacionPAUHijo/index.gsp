<%@page import="ar.com.telecom.FaseMultiton"%>
<%@page import="ar.com.telecom.util.DateUtil"%>
<%@page import="ar.com.telecom.util.NumberUtil"%>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta name="layout" content="mainApp" />
<title>PCS</title>

<script type="text/javascript">
	var controllerActual = '${params.controller}';
</script>

<g:javascript library="animatedcollapse" />
<g:javascript library="aprobacionHijo" />
<g:javascript library="boxUser" />

</head>
<body>

	<div class="contenedorTitulo">
		<g:titulo pedido="${impactoInstance?.pedido}" />
		<g:workflow pedidoId="${impactoInstance?.pedido?.id}" />
	</div>

		<div id="contenedorIzq">
			<g:if test="${flash.message}">
					<div class="message">
						${flash.message}
					</div>
			</g:if>
			<g:hasErrors bean="${impactoInstance?.pedido}">
				<div class="errors">
					<g:renderErrors bean="${impactoInstance?.pedido}" as="list" />
				</div>
			</g:hasErrors>
			<g:form class="formGeneral" useToken="true">
				<div class="seccionHide">
					<g:cabeceraPedido pedido="${impactoInstance?.pedido.parent}" />
					<g:render template="/templates/workflowHidden"/>
					<g:actionSubmit id="refreshView" controller="aprobacionPAUHijo" action="index" style="display:none" value="Refrescar vista" />
<%--					<g:hiddenField name="pedidoHijoId" id="pedidoHijoId" value="${pedidoHijo?.id}"></g:hiddenField>--%>

					<p class="tituloSeccion">Aprobaci&oacute;n PAU</p>
					<div class="formSeccion">
						<div class="pDos">
							<label class='formLabel enRenglon' style="margin-left:30px;">Pedido hijo:</label>
							<g:select id="comboImpacto" name="comboImpacto"
									from="${impactos}" optionKey="id" optionValue="${{it.getDescripcion(pedidoInstance.faseAprobacionPAUHijo())}}"
									value="${comboImpacto}" onChange="g_tabSeleccionado=0;"/>

							<g:if test="${impactoInstance?.pedido?.cumplioFecha(impactoInstance?.pedido?.faseImplantacionHijo())}">
                                <span title="Cumplido el ${DateUtil.toString(impactoInstance?.pedido?.getFecha(FaseMultiton.getFase(FaseMultiton.APROBACION_PAU_HIJO)), true)}" id="pedido_estado_cumplido"></span>
							</g:if>
							
							<g:hiddenField name="impacto" id="impacto" value="${impacto}"></g:hiddenField>
						</div>
						<g:if test="${impactoInstance?.pedido?.cumplioFecha(FaseMultiton.getFase(faseATrabajar))}">
							<div class="pDos">
								<div class="info infoNoMargin"><label class="formLabel">La fase ${FaseMultiton.getFase(faseATrabajar)} fue cumplida el ${DateUtil.toString(impactoInstance?.pedido?.getFecha(FaseMultiton.getFase(faseATrabajar)))}</label></div>
							</div>
						</g:if>

						<div class="separator"></div>
	
						
					<div class="pDos">
						<div>
							<label class="formLabel">Fase actual</label>
							<div class="info">${impactoInstance?.pedido?.faseActual}</div>
						</div>
						<div>
							<label class="formLabel">Grupo referente</label>
							<div class="info">${impactoInstance?.pedido?.grupoResponsable}</div>
						</div>
					</div>
						
					<g:if test="${impactoInstance?.pedido?.esHijo()}">
						<div class="pDos">
							<label class="formLabel2" for='legajoUsuarioResponsable'>Interlocutor Usuario:</label>
							<div id="legajoResponsableAprobacionPAU" class="boxUser">
								<g:render template="/templates/userBox"
									model="['person':impactoInstance?.pedido?.parent.legajoInterlocutorUsuario, 'box':'legajoResponsableAprobacionPAU', 'pedidoHijo':impactoInstance?.pedido,'personsAutocomplete': personsAutocomplete, 'grupoResponsable':impactoInstance?.pedido?.grupoResponsable]"></g:render>
							</div>
						</div>
					</g:if>
					<g:else>
						<div class="pDos">
							<label class="formLabel2" for='legajoUsuarioResponsable'>Interlocutor Usuario:</label>
							<div id="legajoResponsableAprobacionPAU" class="boxUser">
								<g:render template="/templates/userBox"
									model="['person':impactoInstance?.pedido.parent.legajoInterlocutorUsuario, 'box':'legajoResponsableAprobacionPAU', 'pedidoHijo':impactoInstance?.pedido,'personsAutocomplete': personsAutocomplete, 'grupoResponsable':impactoInstance?.pedido?.grupoResponsable]"></g:render>
							</div>
						</div>
					</g:else>
							
		<div class="separator"></div>
					
					<div id="datosImpactoSeleccionado"></div>
					<script type="text/javascript">
						seleccionImpacto($('comboImpacto').value);
					</script>
						
					</div>
					<div id="message"></div>

					<div id="botonera">
					</div>

				</div>
			</g:form>
		</div>
		<!-- fin contenedor izq -->

		<!-- Lateral Derecho -->
	<g:render template="/templates/menuLateral" model="['pedidoInstance':impactoInstance.pedido.parent]"/>
	<!-- Fin Lateral Derecho -->

		<!-- 
	************
	************
	MODALS POPUP
	************
	************
	************
	-->

		<!-- popup ayuda seccion -->
		<div id="dialog-help" title="Ayuda Aprobar Pedido" style="display: none;">
			<div style="height: 350px; width: 100%; overflow: auto;">
				<p class="helpTitle">Seccion ayuda</p>
				<p>Hola esto es una seccion de ayuda Hola esto es una seccion de</p>
			</div>
		</div>
		<!-- fin popup ayuda seccion -->
</body>
</html>
 