<g:if test="${pedidosHijosConsolidados.isEmpty()}">
	<div>No hay pedidos hijos consolidados</div>
</g:if>
<g:else>
	<div class="list">
	    <table class="tablaAutoWidth">
	        <thead>
	            <tr>
					<th>Pedido</th>
					<th>Sistema - Tipo de Impacto</th>
					<th>Grupo Responsable</th>
					<th>Asignatario</th>
					<th>Macroestado</th>
	            </tr>
	        </thead>
	        <tbody>
	        <g:each in="${pedidosHijosConsolidados}" status="i" var="hijo">
	            <tr class="${(i % 2) == 0 ? 'odd' : 'even'}">
					<td>${hijo.id}</td>
					<td>${hijo.tipoPlanificacion}</td>
					<td>${hijo.grupoResponsable}</td>
					<td>
<%--							<g:render template="/templates/userBox"--%>
<%--								model="['person': hijo?.legajoUsuarioResponsable, 'box': 'legajoGerenteUsuarioCabecera']" />--%>
						<g:userLine etiqueta="${hijo?.legajoUsuarioResponsable}" legajo="${hijo?.legajoUsuarioResponsable}" controller="${params.controller }"/>
					</td>
					<td>${hijo.faseActual.macroEstado}</td>
	            </tr>
	        </g:each>
	        </tbody>
	    </table>
	</div>
</g:else>
