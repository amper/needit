<g:if test="${impactoInstance?.pedido?.puedeEditarAprobacionPAU(usuarioLogueado)}">
	<div class="pDos">
		<label class="formlabel" for="justificacionAprobacion" style="margin-left:30px;">Justificaci&oacute;n</label>
		<g:textArea name="justificacionAprobacion" value="${pedidoInstance?.getJustificacionAP()}" style="margin-left:25px;"></g:textArea>
	</div>

	<div class="separador"></div>
	
	<div style="margin-top: 20px; text-align: center;">
	</div>
</g:if>
<g:else>
	<g:if test="${impactoInstance?.pedido?.aproboAprobacionPAU()}">
		<div class="pDos">
			<g:if test="${impactoInstance?.pedido?.getJustificacionAP()}">
				<label for="justificacionAprobacion" class="formLabel" style="margin-left:0px;">Justificaci&oacute;n</label>
				<p class="info" style="margin-left:0px;">${impactoInstance?.pedido?.getJustificacionAP()}</p>
			</g:if>
			<g:else>
				<label for="justificacionAprobacion">No se ingres&oacute; justificaci&oacute;n</label>
			</g:else>
		</div>
	</g:if>
</g:else>
 