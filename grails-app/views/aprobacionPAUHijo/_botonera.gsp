<%@page import="ar.com.telecom.pedido.especificacionImpacto.Impacto"%>
<g:if test="${impactoInstance?.pedido?.puedeEditarAprobacionPAU(usuarioLogueado)}">
	<div class="formSeccionButton" style="margin-top: 20px;">

		<g:if test="${!Impacto.aplicaAPadre(impacto)}">
			<g:actionSubmit class="formSeccionSubmitButton" id="Simular" action="simularValidacion" value="Simular" />
		</g:if>
		<g:actionSubmit class="formSeccionSubmitButton" id="Guardar" action="grabarFormulario" value="Guardar"/>

		<g:if test="${!impactoInstance?.pedido?.puedeSoloGuardar(faseATrabajar, usuarioLogueado)}">
			<g:actionSubmit class="formSeccionSubmitButton" id="Denegar" action="denegarPedido" value="Denegar" />
			<g:actionSubmit class="formSeccionSubmitButton" id="Aprobar" action="aprobarPedido" value="Aprobar" />
		</g:if>
	</div>
</g:if> 

<script type="text/javascript">
	confirmarCambioDireccion();
</script>
