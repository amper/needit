<div class="list" style="text-align: center;">
    <table class="tablaAutoWidth">
        <thead>
            <tr>
				<th>Pedido</th>
				<th>Sistema - Tipo de Impacto</th>
				<th>Grupo Responsable</th>
				<th>Asignatario</th>
				<th>Macroestado</th>
            </tr>
        </thead>
        <tbody>
        <g:each in="${pedidosHijosConsolidados}" status="i" var="hijo">
            <tr class="${(i % 2) == 0 ? 'odd' : 'even'}">
				<td>${hijo.getNumero()}</td>
				<td>${hijo.tipoPlanificacion}</td>
				<td>${hijo.grupoResponsable}</td>
				<td>
					<g:userLine etiqueta="${hijo?.legajoUsuarioResponsable}" legajo="${hijo?.legajoUsuarioResponsable}" controller="${params.controller}"/>
				</td>
				<td>${hijo.faseActual.macroEstado}</td>
            </tr>
        </g:each>
        </tbody>
    </table>
</div>
 
<g:if test="${pedidoInstance?.hijosDisponiblesParaAprobacionPAU()}">
		<g:render template="pedidoHijoNoConsolidado" model="['pedidoInstance': pedidoInstance, 'impactoInstance': impactoInstance, 'usuarioLogueado':usuarioLogueado]"></g:render>
</g:if>
<g:else>
	<div class="separator"></div>
	<g:if test="${!pedidoInstance?.aproboAprobacionPAU()}">
		<div><b>Nota:</b> Esta actividad estar&#225 disponible cuando todos los cambios hijos que la integran est&#233n en la fase Aprobaci&#243n PAU consolidada.</div>
	</g:if>
	<g:else>
		<g:render template="pedidoHijoNoConsolidado" model="['pedidoInstance': pedidoInstance, 'impactoInstance': impactoInstance, 'usuarioLogueado':usuarioLogueado]"></g:render>
	</g:else>	
</g:else>