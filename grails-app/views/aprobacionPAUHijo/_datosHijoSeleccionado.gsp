<g:if test="${pedidoHijo}">
<div class="formSeccion">

	<div class="pDos">
		<label for='grupoResponsable' class="formLabel">T&iacute;tulo:</label>
		<br>
		<span>${pedidoHijo.titulo ? pedidoHijo.titulo : 'N/A'}</span>							
	</div>

	<div class="pDos">
		<label class="formLabel2" for='legajoUsuarioCreador'>Usuario Creador:</label>
		<div id="legajoResponsable" class="boxUser">
			<g:render template="/templates/userBox"
				model="['person':pedidoHijo?.legajoUsuarioCreador, 'box':'legajoResponsable', 'pedidoHijo':pedidoHijo]"></g:render>
		</div>
	</div>

	<div class="pDos">
		<label for='grupoResponsable' class="formLabel">Grupo referente:</label>
		<br>
		<span>${pedidoHijo.grupoResponsable ? pedidoHijo.grupoResponsable : 'N/A'}</span>							
	</div>

	<div class="pDos">
		<label class="formLabel2" for='legajoUsuarioResponsable'>Asignatario</label>
		<div id="legajoResponsableConstruccionPadre" class="boxUser">
			<g:render template="/templates/userBox"
				model="['person':pedidoHijo?.legajoUsuarioResponsable, 'box':'legajoResponsableConstruccionPadre', 'pedidoHijo':pedidoHijo,'personsAutocomplete': personsAutocomplete]"></g:render>
		</div> 
	</div>

	<div class="pDos">
		<label for='grupoResponsable' class="formLabel">Tipo de Impacto:</label>
		<br>
		<span>${pedidoHijo.tipoImpacto ? pedidoHijo.tipoImpacto : 'N/A'}</span>							
	</div>

	<div class="pDos">
		<label for='grupoResponsable' class="formLabel">Fase Actual:</label>
		<br>
		<span>${pedidoHijo.faseActual ? pedidoHijo.faseActual : 'N/A'}</span>							
	</div>
	
	<div class="pDos">
		<label for='grupoResponsable' class="formLabel">Sistema:</label>
		<br>
		<span>${pedidoHijo.sistema ? pedidoHijo.sistema : 'N/A'}</span>							
	</div>

	<div class="pDos">
		<label for='grupoResponsable' class="formLabel">&Aacute;rea Soporte:</label>
		<br>
		<span>${pedidoHijo.areaSoporte ? pedidoHijo.areaSoporte : 'N/A'}</span>							
	</div>

	<div class="separator"></div>
<%--	--%>
<%--	<div id="actividades">--%>
<%--		<g:render template="tablaActividades" model="['pedidoHijo':pedidoHijo, 'esElResponsable':esElResponsable]"></g:render>							--%>
<%--	</div>--%>

</div>

</g:if>