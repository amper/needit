<%@page import="ar.com.telecom.util.DateUtil"%>
<%@page import="ar.com.telecom.FaseMultiton"%>
<%@page import="ar.com.telecom.pcs.entities.RolAplicacion"%>

<g:updateToken />

<p class="tituloSeccion">Datos de la especificaci&oacute;n</p>
<div class="formSeccion">
	<div class="pDos" style="width: 53%;">
		<label class='formLabel'>Impacto:</label><br>
		<g:select id="comboImpactos" name="comboImpactos" from="${pedidoInstance.getImpactos()}" optionKey="id" value="${impacto}" />
<%--		<span title="${impactoInstance.estiloDescripcion}" id="pedido_${impactoInstance.estilo}"></span>--%>
<%--		<g:if test="${impactoInstance.puedeReenviar(usuarioLogueado)}">--%>
<%--			<a title="Reenviar" class="formLabel2 reenviar" href="javascript:openPopUpReenviar('${pedidoInstance?.id }', '${impactoInstance?.id}','${pedidoInstance?.aprobacionPendiente(pedido?.faseActual, usuarioLogueado)}');"></a>							--%>
<%--		</g:if>--%>
		<g:if test="${trabajoSobreHijo}">
			<div id="grupoRSWF" style="padding-top: 10px;">
				<g:render template="grupoRSWF" model="['grupo':grupo]"></g:render>
			</div>
		</g:if>
	</div>
	<div class="pDos" style="width: 40%;">
		<div>
			<label class="formLabel2">Planificacic&oacute;n y Esfuerzo - versi&oacute;n: </label>
			<a title="Buscar versi&oacute;n anterior" class="formLabel2 buscarVersionAnterior" href="javascript:openPopUpReenviar();"></a>
		</div>
		<div class="separator"></div>
		<g:render template="/replanificacion/asignatarios"	model="['impactoInstance':impactoInstance, 'impacto':impacto, 'usuarioLogueado':usuarioLogueado]"></g:render>
	</div>
</div>