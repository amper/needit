<g:updateToken />

<table id="tablaFasesPlanificacionEsfuerzo" cellspacing="0" cellpadding="0">
<g:if test="${detalleReplanificacion}">
	<tr>
		<th>Fase planificaci&oacute;n</th>
		<th>Desde</th>
		<th>Hasta</th>
		<th>Total Horas</th>
		<th style="text-align:right;">Monto (en Pesos)</th>
		<g:if test="${edit}">	
			<th style="width:10px;" > </th>
		</g:if>		
		<g:else>
			<th></th>
		</g:else>		
    </tr>
</g:if>

<%--<g:if test="${edit}">--%>
<!-- EDIT -->
	<g:each in="${planificacionActual?.detalles.findAll { detalle -> detalle.esDeSistema()}.sort{ it.id }}" var="detalle"  status="i">	
		<tr id="fila_${detalle.id}" style="padding: 0px;" class="${(i % 2) == 0 ? 'odd' : 'even'}">
			<g:render template="/replanificacion/filaDetalleEnReplanificacion" model="${['detalle': detalle, 'edit': planificacionActual.estaEnReplanificacion(), 'detalleReplanificacion':detalleReplanificacion]}"></g:render>
		</tr>
	</g:each>
<%--</g:if>--%>
<%--<g:else>--%>
<%--<!-- SHOW -->--%>
<%--	<g:each in="${planificacionActual?.detalles.findAll { detalle -> detalle.esDeSistema()}.sort{ it.id }}" var="detalle" status="i">--%>
<%--	 <tr id="fila_${detalle.id}" style="padding: 0px;" class="${(i % 2) == 0 ? 'odd' : 'even'}">--%>
<%----%>
<%--		<td id="fase1">--%>
<%--			${detalle.actividadPlanificacion.descripcion}--%>
<%--		</td>--%>
<%--		--%>
<%--		<td>--%>
<%--			${DateUtil.toString(detalle.fechaDesde)}--%>
<%--		</td>--%>
<%--		<td>${DateUtil.toString(detalle.fechaHasta)}</td>--%>
<%--		--%>
<%--		<td style="width: 70px; text-align: right;">--%>
<%--			<g:if test="${detalle?.totalHoras()}">--%>
<%--				${detalle?.totalHoras()} --%>
<%--			</g:if> --%>
<%--		</td>--%>
<%--		<td style="text-align: right;">${NumberUtil.setearDecimalesDefaultConComa(detalle.totalMontoHoras())}</td>--%>
<%--		<td>--%>
<%--			<g:if test="${detalle?.totalHoras()}">--%>
<%--				<a href="javascript:mostrarHorasSistemaPopup('fase1','${pedidoHijo?.sistema}','${DateUtil.toString(detalle.fechaDesde)}','${DateUtil.toString(detalle.fechaHasta)}','${detalle?.id}','${planificacionActual?.id }','${impacto}','${pedidoHijo?.id}');" class="visualizar sinMargenIzquierdo" title="Visualizar detalle"></a>--%>
<%--			</g:if>--%>
<%--		</td>--%>
<%--	 </tr>--%>
<%--	</g:each>--%>
<%--</g:else>--%>
</table>