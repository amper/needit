<table id="tablaFasesPlanificacionEsfuerzo" cellspacing="0" cellpadding="0">
	<tr>
		<th style="width: 200px">Fase planificaci&oacute;n</th>
		<th style="width: 100px">Desde</th>
		<th style="width: 100px">Hasta</th>
		<th style="text-align: right;">Total Horas</th>
		<th style="text-align: right;">Monto (en Pesos)</th>
	</tr>

	<!-- SHOW -->
	<g:each in="${consolidacionList}" var="detalle" status="i">
	 <tr style="padding: 0px;" class="${(i % 2) == 0 ? 'odd' : 'even'}">

		<td id="fase1">
			${detalle.getAt(0)}
		</td>
		
		<td>${DateUtil.toString(detalle.getAt(1))}</td>
		<td>${DateUtil.toString(detalle.getAt(2))}</td>
		
		<td style="text-align: right; width: 70px;" > ${detalle?.getAt(3)} </td>
		<td style="text-align: right;">${NumberUtil.toString(detalle.getAt(4))}</td>
	 </tr>
	</g:each> 
</table>	
<div class="separatorGrande"></div>
<table cellspacing="0" cellpadding="0">
	<tr><td colspan="2" style="background: #c1d5d9;">Impactos</td></tr>
	<g:each in="${pedidoInstance.getImpactos()}" var="impactoItem" status="i">
		<g:if test="${!impactoItem.esConsolidado()}">
			<tr style="padding: 0px;" class="${(i % 2) == 0 ? 'odd' : 'even'}">
				<td>
					${impactoItem}
					<g:if test="${!impactoItem.pedido.isEnReplanificacion()}">
						<a title="Enviar Replanificaci&oacute;n" class="formLabel2 replanificar" href="javascript:enviarReplanificacion('${impactoItem.id }');"></a>
					</g:if>						
				</td>
				<td>
					<g:if test="${!impactoItem.pedido.isEnReplanificacion()}">
						<div align="right">
							<label>Comentarios:&nbsp;</label>
							<g:textField name="comentario${impactoItem.id}" id="comentario${impactoItem.id}" value=""/>
						</div>
					</g:if>
				</td>
			</tr>
		</g:if>
	</g:each>
</table>
