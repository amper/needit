<%@page import="ar.com.telecom.util.DateUtil"%>
<%@page import="ar.com.telecom.util.NumberUtil"%>
<%@page import="ar.com.telecom.pedido.especificacionImpacto.Impacto"%>

<g:updateToken/>
<% 
	def listaDetalle = detalleReplanificacion[detalle?.actividadPlanificacion?.id]
%> 

<td id="fase${detalle.id}">
	${detalle.actividadPlanificacion.descripcion}
	<input type="hidden" id="detalleId_${detalle.id}" name="detalleId_${detalle.id}" class="detalleReplanificacion" />
</td>

<td>
	<span>
		<label style="width: 75px;">${DateUtil.toString(listaDetalle[0]?.fechaDesde)}&nbsp;</label>
		<g:if test="${edit}">
			<g:textField id="desde_${detalle.id}" class="datePicker" name="desde_${detalle.id}" style="background-image:none !important;" onChange="validaFechasCompletas('${detalle.id}')" />
			<g:hiddenField name="fdesde_${detalle.id}" id="fdesde_${detalle.id}" value="${ar.com.telecom.util.DateUtil.toString(detalle.fechaDesde)}" />
		</g:if>
	</span>
</td>
<td>
	<span>
	 	<label style="width: 75px;">${DateUtil.toString(listaDetalle[0]?.fechaHasta)}&nbsp;</label>
	 	<g:if test="${edit}">
			<g:textField id="hasta_${detalle.id}" class="datePicker" name="hasta_${detalle.id}" style="background-image:none !important;" onChange="validaFechasCompletas('${detalle.id}')" />
			<g:hiddenField name="fhasta_${detalle.id}" id="fhasta_${detalle.id}" value="${ar.com.telecom.util.DateUtil.toString(detalle.fechaHasta)}" />
		</g:if>
	</span>
</td>
<td>
	<span>
		<label style="width: 65px; text-align: right;">${listaDetalle[0]?.totalHoras()}&nbsp;</label>
		<g:if test="${edit}">
			<g:textField id="cargaHoras_${detalle.id}" name="cargaHoras_${detalle.id}" value="${detalle?.totalHoras()}" size="5"/>
		</g:if>
	</span> 
</td>
<td style="text-align: right;">
		${NumberUtil.setearDecimalesDefaultConComa(listaDetalle[0]?.totalMontoHoras())}
</td>
<td>
	<g:if test="${edit}">
		<g:secureRemoteLink title="Limpiar" class="limpiar"
		update="fila_${detalle.id}" params="['detalleId':detalle.id, 'impacto':impacto, 'pedidoId':impactoInstance?.pedido?.id, 'aprobacionId': aprobacion?.id]"
		action="limpiaDetallePlanificacion" onComplete="recargaCostoTotal('${impacto}', '${impactoInstance?.pedido?.id}');" />
	</g:if>
</td>
<g:if test="${edit}">
	<script type="text/javascript">
	jQuery(document).ready( function($) {
		$("#desde_" +${detalle.id}).glDatePicker({
			position: "relative",
			onChange: function(target, newDate){
				var day = newDate.getDate();
				var month = (newDate.getMonth() + 1);
				if (day < 10){
					day = "0" + day;
				}
				if (month < 10){
					month = "0" + month;
				}
				target.val(
		            day + "/" +
		            month + "/" +
		            newDate.getFullYear() 
		        );
		    }
		});
		$("#hasta_" +${detalle.id}).glDatePicker({
			position: "relative",
			onChange: function(target, newDate){
				var day = newDate.getDate();
				var month = (newDate.getMonth() + 1);
				if (day < 10){
					day = "0" + day;
				}
				if (month < 10){
					month = "0" + month;
				}
				target.val(
		            day + "/" +
		            month + "/" +
		            newDate.getFullYear() 
		        );
		    }
		});
		document.getElementById('desde_'+${detalle.id}).value = document.getElementById('fdesde_'+${detalle.id}).value;
		document.getElementById('hasta_'+${detalle.id}).value = document.getElementById('fhasta_'+${detalle.id}).value;
	});
	</script>
</g:if>
