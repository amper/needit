<%@page import="ar.com.telecom.util.DateUtil"%>
<%@page import="ar.com.telecom.util.NumberUtil"%>

<g:updateToken />

<br>
<br>
<div align="right" id="costoTotalCambio">
	<g:render template="/replanificacion/costoTotalCambio" model="['impactoInstance':impactoInstance]"></g:render>
</div>
<div id="mensaje">
<%--Aca podnria cuando se grabo correctamente o fallo--%>
</div>
<g:if test="${consolidacionList}">
	<div id="tablaImpactos">
		<g:render template="/replanificacion/tablaImpactos" model="['pedidoInstance':pedidoInstance]"></g:render>
	</div>
</g:if>
<g:else>
	<%-- NED: Aca deberia preguntar si puedeEditarDetalleReplanificacion()--%>
	<g:render template="/replanificacion/tablaDetallesSistemaEnReplanificacion" model="['edit': true, 'planificacionActual':planificacionActual, 'detalleReplanificacion':detalleReplanificacion]"></g:render>
</g:else>	