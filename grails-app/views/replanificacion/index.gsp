<%@page import="org.codehaus.groovy.grails.web.servlet.mvc.SynchronizerToken"%>
<html>
<head>
<title>NeedIt</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<link rel="shortcut icon" href="${resource(dir:'images',file:'LogoTelecom.jpg')}" type="image/x-icon" />

<!--css-->
<link rel="stylesheet" href="${resource(dir:'css/themes/custom-theme',file:'jquery-ui-1.8.16.custom.css')}" />
<link rel="stylesheet" href="${resource(dir:'css/themes/custom-theme',file:'jquery.multiselect.css')}" />
<link rel="stylesheet" href="${resource(dir:'css/themes/custom-theme',file:'jquery.multiselect.filter.css')}" />
<link rel="stylesheet" href="${resource(dir:'css',file:'glDatePicker.css')}" />
<link rel="stylesheet" href="${resource(dir:'css',file:'mainApp.css')}" />
 
<!--js-->
<g:javascript library="jquery-1.6.2" />
<g:javascript library="jquery-ui-custom.min" />
<g:javascript library="jquery.multiselect" />
<g:javascript library="jquery.multiselect.filter" />
<g:javascript library="common" />
<g:javascript library="common-widget" />
<g:javascript library="prototype" />
<g:javascript library="error" />
<g:javascript library="synchronizerToken" />
<g:javascript library="submitSpinner" />
<g:javascript library="DateUtil" />

<g:javascript library="impacto" />
<g:javascript library="animatedcollapse" />
<g:javascript library="replanificacion" />
<g:javascript library="boxUser" />
<g:javascript library="glDatePicker" />

<script type="text/javascript">
	var gNameApp = 'needIt';
	var gTokenKey = '${SynchronizerToken.KEY}';
	var gErrorURL = '<g:createLink controller="errorApp" action="index"/>';
	var gErrorAjaxURL = '<g:createLink controller="errorApp" action="index"/>';
</script>
</head>

<body onunload="cierreVentana('replanificacion','${stringParams}');">
	<img id="spinner" src="<g:createLinkTo dir='/images' file='ajax-spinner.gif'/>" />
	<div id="ContenedorGeneral">
		<div id="ContenedorPrincipal">
		
		<div>
			<p class="tituloPrincipal" style="padding-bottom: 0px;">Replanificaci&oacute;n</p>
		</div>
		
		<div id="contenedorIzqPopUp">			
			<g:uploadForm name="formReplanificacion" action="replanificar" class="formGeneral" useToken="true">
				<g:hiddenField name="pedidoId" value="${pedidoInstance?.id}" />
				<g:hiddenField name="impacto" id="impacto" value="${impacto}" />
				<g:actionSubmit id="refreshView" action="cambiaGrupoRSWF" style="display:none" value="Refrescar vista" />
				
				<div class="seccionHide">
					<g:render template="/templates/encabezadoMinimo" model="['pedidoInstance':pedidoInstance]"></g:render>
					<g:render template="/replanificacion/headerReplanificacion" model="['pedidoInstance':pedidoInstance, 'impactoInstance':impactoInstance, 'pedidoATrabajar':pedidoATrabajar, 'impacto':impacto]"></g:render>
					
					<g:if test="${impactoInstance.tienePlanificacionYEsfuerzo()}">
						<div class="seccionHide2">
							<p class="tituloSeccion">Planificaci&oacute;n y esfuerzo</p>
							<div class="formSeccion" style="background-color: #FFF; text-align: center;">
								<div style="margin: 0px auto; width: 98%" id="planificacionYEsfuerzo">
									<g:render template="/replanificacion/planificacionYEsfuerzoReplanificacion"></g:render>
								</div>
							</div>
						</div>
					</g:if>
					<g:render template="/replanificacion/botonera" model="['impactoInstance':impactoInstance, 'impacto':impacto, 'pedidoInstance':pedidoInstance]"></g:render>
				</div>
				<div id="token"><g:render template="/templates/token"></g:render></div>
			</g:uploadForm>
			</div> 
		</div>
			
	</div>
</body>
</html>
