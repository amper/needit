<g:updateToken/>
<br/>

<div align="center">
	<g:if test="${impactoInstance.esConsolidado()}">
		<g:actionSubmit class="formSeccionSubmitButton" id="Cancelar" action="cancelarReplanificacion" value="Cancelar" onClick="if (!confirm('Desea cancelar la replanificación?')) return false;" />			
	</g:if>
	<g:else>
		<g:actionSubmit class="formSeccionSubmitButton" id="Simular" action="simularValidacion" value="Simular" />
		<g:actionSubmit class="formSeccionSubmitButton" id="No Replanifica" action="noReplanifica" value="No Replanifica" />
		<g:actionSubmit class="formSeccionSubmitButton" id="Grabar" action="grabaFechas" value="Grabar" onClick="grabarHoras('${pedidoInstance.id}', '${impacto}'); return false" />
<%--		<g:actionSubmit class="formSeccionSubmitButton" id="Grabar" action="grabaFechas" value="Grabar" />--%>
		<g:actionSubmit class="formSeccionSubmitButton" id="Enviar" action="enviarReplanificacion" value="Enviar" />
	</g:else>
</div>