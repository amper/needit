<g:if test="${impactoInstance.aplicaAPadre(impacto)}">
	<label class="formLabel2" for='inputIntUsuario'>Coordinador cambio</label>
	<div id="legajoCoordinadorCambio" class="boxUser">
		<g:render template="/templates/userBox"
			model="['person':impactoInstance.criterioImpacto.pedido.legajoCoordinadorCambio, 'box':'legajoCoordinadorCambio']"></g:render>
	</div>
</g:if>
<g:if test="${impactoInstance.aplicaAHijo(impacto)}">
	<label class="formLabel2 validate" for='inputIntUsuario'>Asignatario</label>
	<div id="legajoUsuarioResponsable" class="boxUser">
		<g:render template="/templates/userBox"
			model="['person':impactoInstance?.pedido?.legajoUsuarioResponsable, 'box':'legajoUsuarioResponsable', 'impacto': impacto]"></g:render>
	</div>
</g:if>