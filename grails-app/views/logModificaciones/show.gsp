
<%@ page import="ar.com.telecom.pcs.entities.LogModificaciones" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="layout" content="main" />
        <g:set var="entityName" value="${message(code: 'logModificaciones.label', default: 'LogModificaciones')}" />
        <title><g:message code="default.show.label" args="[entityName]" /></title>
    </head>
    <body>
        <div class="nav">
            <span class="menuButton"><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></span>
            <span class="menuButton"><g:link class="list" action="list"><g:message code="default.list.label" args="[entityName]" /></g:link></span>
            <span class="menuButton"><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></span>
        </div>
        <div class="body">
            <h1><g:message code="default.show.label" args="[entityName]" /></h1>
            <g:if test="${flash.message}">
            <div class="message">${flash.message}</div>
            </g:if>
            <div class="dialog">
                <table>
                    <tbody>
                    
                        <tr class="prop">
                            <td valign="top" class="name"><g:message code="logModificaciones.id.label" default="Id" /></td>
                            
                            <td valign="top" class="value">${fieldValue(bean: logModificacionesInstance, field: "id")}</td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name"><g:message code="logModificaciones.legajo.label" default="Legajo" /></td>
                            
                            <td valign="top" class="value">${fieldValue(bean: logModificacionesInstance, field: "legajo")}</td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name"><g:message code="logModificaciones.fechaDesde.label" default="Fecha Desde" /></td>
                            
                            <td valign="top" class="value"><g:formatDate date="${logModificacionesInstance?.fechaDesde}" /></td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name"><g:message code="logModificaciones.fechaHasta.label" default="Fecha Hasta" /></td>
                            
                            <td valign="top" class="value"><g:formatDate date="${logModificacionesInstance?.fechaHasta}" /></td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name"><g:message code="logModificaciones.tipoEvento.label" default="Tipo Evento" /></td>
                            
                            <td valign="top" class="value">${fieldValue(bean: logModificacionesInstance, field: "tipoEvento")}</td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name"><g:message code="logModificaciones.rol.label" default="Rol" /></td>
                            
                            <td valign="top" class="value">${fieldValue(bean: logModificacionesInstance, field: "rol")}</td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name"><g:message code="logModificaciones.actividad.label" default="Actividad" /></td>
                            
                            <td valign="top" class="value"><g:link controller="actividad" action="show" id="${logModificacionesInstance?.actividad?.id}">${logModificacionesInstance?.actividad?.encodeAsHTML()}</g:link></td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name"><g:message code="logModificaciones.descripcionEvento.label" default="Descripcion Evento" /></td>
                            
                            <td valign="top" class="value">${fieldValue(bean: logModificacionesInstance, field: "descripcionEvento")}</td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name"><g:message code="logModificaciones.fase.label" default="Fase" /></td>
                            
                            <td valign="top" class="value"><g:link controller="fase" action="show" id="${logModificacionesInstance?.fase?.id}">${logModificacionesInstance?.fase?.encodeAsHTML()}</g:link></td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name"><g:message code="logModificaciones.pedido.label" default="Pedido" /></td>
                            
                            <td valign="top" class="value"><g:link controller="abstractPedido" action="show" id="${logModificacionesInstance?.pedido?.id}">${logModificacionesInstance?.pedido?.encodeAsHTML()}</g:link></td>
                            
                        </tr>
                    
                    </tbody>
                </table>
            </div>
            <div class="buttons">
                <g:form>
                    <g:hiddenField name="id" value="${logModificacionesInstance?.id}" />
                    <span class="button"><g:actionSubmit class="edit" action="edit" value="${message(code: 'default.button.edit.label', default: 'Edit')}" /></span>
                    <span class="button"><g:actionSubmit class="delete" action="delete" value="${message(code: 'default.button.delete.label', default: 'Delete')}" onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');" /></span>
                </g:form>
            </div>
        </div>
    </body>
</html>
