
<%@ page import="ar.com.telecom.pcs.entities.LogModificaciones" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="layout" content="main" />
        <g:set var="entityName" value="${message(code: 'logModificaciones.label', default: 'LogModificaciones')}" />
        <title><g:message code="default.list.label" args="[entityName]" /></title>
    </head>
    <body>
        <div class="nav">
            <span class="menuButton"><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></span>
            <span class="menuButton"><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></span>
        </div>
        <div class="body">
            <h1><g:message code="default.list.label" args="[entityName]" /></h1>
            <g:if test="${flash.message}">
            <div class="message">${flash.message}</div>
            </g:if>
            <div class="list">
                <table>
                    <thead>
                        <tr>
                        
                            <g:sortableColumn property="id" title="${message(code: 'logModificaciones.id.label', default: 'Id')}" />
                        
                            <g:sortableColumn property="legajo" title="${message(code: 'logModificaciones.legajo.label', default: 'Legajo')}" />
                        
                            <g:sortableColumn property="fechaDesde" title="${message(code: 'logModificaciones.fechaDesde.label', default: 'Fecha Desde')}" />
                        
                            <g:sortableColumn property="fechaHasta" title="${message(code: 'logModificaciones.fechaHasta.label', default: 'Fecha Hasta')}" />
                        
                            <g:sortableColumn property="tipoEvento" title="${message(code: 'logModificaciones.tipoEvento.label', default: 'Tipo Evento')}" />
                        
                            <g:sortableColumn property="rol" title="${message(code: 'logModificaciones.rol.label', default: 'Rol')}" />
                        
                        </tr>
                    </thead>
                    <tbody>
                    <g:each in="${logModificacionesInstanceList}" status="i" var="logModificacionesInstance">
                        <tr class="${(i % 2) == 0 ? 'odd' : 'even'}">
                        
                            <td><g:link action="show" id="${logModificacionesInstance.id}">${fieldValue(bean: logModificacionesInstance, field: "id")}</g:link></td>
                        
                            <td>${fieldValue(bean: logModificacionesInstance, field: "legajo")}</td>
                        
                            <td><g:formatDate date="${logModificacionesInstance.fechaDesde}" /></td>
                        
                            <td><g:formatDate date="${logModificacionesInstance.fechaHasta}" /></td>
                        
                            <td>${fieldValue(bean: logModificacionesInstance, field: "tipoEvento")}</td>
                        
                            <td>${fieldValue(bean: logModificacionesInstance, field: "rol")}</td>
                        
                        </tr>
                    </g:each>
                    </tbody>
                </table>
            </div>
            <div class="paginateButtons">
                <g:paginate total="${logModificacionesInstanceTotal}" />
            </div>
        </div>
    </body>
</html>
