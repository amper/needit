

<%@ page import="ar.com.telecom.pcs.entities.LogModificaciones" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="layout" content="main" />
        <g:set var="entityName" value="${message(code: 'logModificaciones.label', default: 'LogModificaciones')}" />
        <title><g:message code="default.edit.label" args="[entityName]" /></title>
    </head>
    <body>
        <div class="nav">
            <span class="menuButton"><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></span>
            <span class="menuButton"><g:link class="list" action="list"><g:message code="default.list.label" args="[entityName]" /></g:link></span>
            <span class="menuButton"><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></span>
        </div>
        <div class="body">
            <h1><g:message code="default.edit.label" args="[entityName]" /></h1>
            <g:if test="${flash.message}">
            <div class="message">${flash.message}</div>
            </g:if>
            <g:hasErrors bean="${logModificacionesInstance}">
            <div class="errors">
                <g:renderErrors bean="${logModificacionesInstance}" as="list" />
            </div>
            </g:hasErrors>
            <g:form method="post" >
                <g:hiddenField name="id" value="${logModificacionesInstance?.id}" />
                <g:hiddenField name="version" value="${logModificacionesInstance?.version}" />
                <div class="dialog">
                    <table>
                        <tbody>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                  <label for="legajo"><g:message code="logModificaciones.legajo.label" default="Legajo" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: logModificacionesInstance, field: 'legajo', 'errors')}">
                                    <g:textField name="legajo" maxlength="20" value="${logModificacionesInstance?.legajo}" />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                  <label for="fechaDesde"><g:message code="logModificaciones.fechaDesde.label" default="Fecha Desde" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: logModificacionesInstance, field: 'fechaDesde', 'errors')}">
                                    <g:datePicker name="fechaDesde" precision="day" value="${logModificacionesInstance?.fechaDesde}" default="none" noSelection="['': '']" />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                  <label for="fechaHasta"><g:message code="logModificaciones.fechaHasta.label" default="Fecha Hasta" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: logModificacionesInstance, field: 'fechaHasta', 'errors')}">
                                    <g:datePicker name="fechaHasta" precision="day" value="${logModificacionesInstance?.fechaHasta}" default="none" noSelection="['': '']" />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                  <label for="tipoEvento"><g:message code="logModificaciones.tipoEvento.label" default="Tipo Evento" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: logModificacionesInstance, field: 'tipoEvento', 'errors')}">
                                    <g:textField name="tipoEvento" maxlength="2" value="${logModificacionesInstance?.tipoEvento}" />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                  <label for="rol"><g:message code="logModificaciones.rol.label" default="Rol" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: logModificacionesInstance, field: 'rol', 'errors')}">
                                    <g:textField name="rol" value="${logModificacionesInstance?.rol}" />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                  <label for="actividad"><g:message code="logModificaciones.actividad.label" default="Actividad" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: logModificacionesInstance, field: 'actividad', 'errors')}">
                                    <g:select name="actividad.id" from="${ar.com.telecom.pcs.entities.Actividad.list()}" optionKey="id" value="${logModificacionesInstance?.actividad?.id}" noSelection="['null': '']" />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                  <label for="descripcionEvento"><g:message code="logModificaciones.descripcionEvento.label" default="Descripcion Evento" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: logModificacionesInstance, field: 'descripcionEvento', 'errors')}">
                                    <g:textField name="descripcionEvento" value="${logModificacionesInstance?.descripcionEvento}" />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                  <label for="fase"><g:message code="logModificaciones.fase.label" default="Fase" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: logModificacionesInstance, field: 'fase', 'errors')}">
                                    <g:select name="fase.id" from="${ar.com.telecom.pcs.entities.Fase.list()}" optionKey="id" value="${logModificacionesInstance?.fase?.id}"  />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                  <label for="pedido"><g:message code="logModificaciones.pedido.label" default="Pedido" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: logModificacionesInstance, field: 'pedido', 'errors')}">
                                    <g:select name="pedido.id" from="${ar.com.telecom.pcs.entities.AbstractPedido.list()}" optionKey="id" value="${logModificacionesInstance?.pedido?.id}"  />
                                </td>
                            </tr>
                        
                        </tbody>
                    </table>
                </div>
                <div class="buttons">
                    <span class="button"><g:actionSubmit class="save" action="update" value="${message(code: 'default.button.update.label', default: 'Update')}" /></span>
                    <span class="button"><g:actionSubmit class="delete" action="delete" value="${message(code: 'default.button.delete.label', default: 'Delete')}" onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');" /></span>
                </div>
            </g:form>
        </div>
    </body>
</html>
