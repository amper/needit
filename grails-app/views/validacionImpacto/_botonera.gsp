<g:updateToken />
<div class="formSeccionButton" style="margin-top: 20px;">
	<g:if test="${pedidoInstance.puedeEditarValidacionImpacto(usuarioLogueado)}">
		<g:actionSubmit class="formSeccionSubmitButton" id="Guardar" action="grabarFormulario" value="Guardar" />
		
		<g:if test="${!pedidoInstance.estaSuspendido()}">
			<g:actionSubmit class="formSeccionSubmitButton" id="Denegar" action="denegarPedido" value="Denegar" />
			<g:actionSubmit class="formSeccionSubmitButton" id="Aprobar" action="aprobarPedido" value="Aprobar" />
		</g:if>
	</g:if>
	<g:else>
		<g:if test="${pedidoInstance.puedeReasignarAprobadorImpacto(usuarioLogueado)}">
			<g:actionSubmit class="formSeccionSubmitButton" id="Guardar" action="grabarFormulario" value="Guardar" />
		</g:if>
	</g:else>
</div> 