<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@page import="ar.com.telecom.util.NumberUtil"%> 	
<%@page import="ar.com.telecom.util.DateUtil"%>
<%@page import="ar.com.telecom.pcs.entities.RolAplicacion"%>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta name="layout" content="mainApp" />

<g:javascript library="impacto" />
<g:javascript library="boxUser" />
<g:javascript library="animatedcollapse" />


</head>
<body> 

	<div class="contenedorTitulo">
		<g:titulo pedido="${pedidoInstance}" />
		<g:workflow pedidoId="${pedidoInstance?.id}"/>
	</div>

		<div id="contenedorIzq">
				<g:if test="${flash.message}">
					<div class="message">
							${flash.message}
					</div>
				</g:if>
				<g:hasErrors bean="${pedidoInstance}">
						<div class="errors">
								<g:renderErrors bean="${pedidoInstance}" as="list" />
						</div>
				</g:hasErrors>
				<g:form class="formGeneral" useToken="true">
					<g:hiddenField name="id" value="${pedidoInstance?.id}" />

					<div class="seccionHide">
						<g:cabeceraPedido pedido="${pedidoInstance}" />
						<g:render template="/templates/workflowHidden"/>
							
						<p class="tituloSeccion">Informaci&oacute;n de Valorizaci&oacute;n del Cambio</p>
						<div class="formSeccion">
							<div class="pCompleto">
								<table class="TablaValidacionImpactoHeader" border="0" cellspacing="0" cellpadding="0">
										<tr>
											<td width="60%">Valorizaci&oacute;n del Cambio</td>
											<td width="20%" style="text-align: right;">Total horas</td>
											<td width="20%" style="text-align: right;">Monto (en Pesos)</td>
										</tr>
								</table>

								<g:each in="${totalizacion.detalle}" status="i" var="detalle">
									<g:divsValoraciones id="${i}" detalle="${detalle}"></g:divsValoraciones>
								</g:each>
									
								<table class="TablaValidacionImpactoHeader" border="0" cellspacing="0" cellpadding="0" id="footerTablaValidacion">
										<tr height="20">
												<td width="60%">Total general</td>
												<td width="20%" style="text-align: right;">${totalizacion.totalHoras}</td>
												<td width="20%" style="text-align: right;">${NumberUtil.toString(totalizacion.totalMontoHoras)}</td>
										</tr>
								</table>
							</div>
						</div>
							
							<p class="tituloSeccion">Fechas de Implementaci&oacute;n</p>								
							<div class="formSeccion">
								<div class="pDos">
									<label class="formLabel">Fecha Desde:</label>
									<p class="info">${DateUtil.toString(fechaImplementacionDesde)}</p>
								</div>
								
								<div class="pDos">
									<label class="formLabel" for='datepicker-fechaVencimientoPlanificacion'>Fecha vencimiento planificaci&oacute;n:</label>
									<p class="info">${DateUtil.toString(pedidoInstance?.fechaVencimientoPlanificacion)}</p>
								</div>
								
								<div class="pDos">
										<label class="formLabel" for='datepicker-fechaHasta'>Fecha Hasta: &nbsp;</label>
										<p class="info">${DateUtil.toString(fechaImplementacionHasta)}</p>
								</div>
								
								<div class="pDos">
									<label class="formLabel" for='comentarios'>Comentarios:</label>
									<p class="info">${pedidoInstance?.comentarioActual()}</p>
								</div>
							</div>
							
							<p class="tituloSeccion">Aprobadores</p>
							<div class="formSeccion">
								<div class="list">
									<table>
										<thead>
											<tr>
												<g:sortableColumn property="grupo" title="&Aacute;rea aprobadora" params="${params}"/>
												<g:sortableColumn property="usuario" title="Usuario" params="${params}"/>
												<g:sortableColumn property="estado" title="Estado" params="${params}"/>
												<g:sortableColumn property="fecha" title="Fecha Aprobaci&oacute;n" params="${params}" />
												<g:sortableColumn property="justificacion"	title="Justificaci&oacute;n" params="${params}" />												
											</tr>
										</thead>
										<tbody>
												<g:each in="${aprobacionesList}" status="i" var="aprobacionInstance">
													<g:set scope="page" var="aprobacion" value="${aprobacionInstance}" />
														<tr class="${(i % 2) == 0 ? 'odd' : 'even'}" style="cursor: pointer;">
															<td>
																${aprobacionInstance.areaSoporte?aprobacionInstance.areaSoporte:"Interlocutor usuario"}
															</td>
															<td>
																<g:userDescription legajo="${aprobacion?.usuario}"	pedido="${aprobacion?.pedido?.id}" />
															</td>
															<td>
																${aprobacionInstance?.estadoDescripcion()}
															</td>
															<td>
																<g:formatDate date="${aprobacionInstance?.fechaCumplimiento}" type="datetime" style="MEDIUM"/>
															</td>
															<td>
																${aprobacionInstance?.justificacion}
															</td>																
														</tr>
												</g:each>
										</tbody>
									</table>
								</div>
								<g:if test="${!pedidoInstance?.pedidoDeEmergencia()}">
									<div class="pDos">
										<label class="formLabel2 validate" for='inputAprobador'>Aprobador de Impacto</label>
										<div id="legajoUsuarioAprobadorEconomico" class="boxUser">
											<g:render template="/templates/userBox"	model="['person':pedidoInstance?.legajoUsuarioAprobadorEconomico? pedidoInstance?.legajoUsuarioAprobadorEconomico: aprobadorDefault, 'box':'legajoUsuarioAprobadorEconomico', 'personsAutocomplete': personsAutocomplete, 'pedidoId': pedidoInstance?.id]"></g:render>
										</div>
										<g:if test="${pedidoInstance.puedeReasignarAprobadorImpacto(usuarioLogueado)}">
											<g:secureRemoteLink title="Eliminar" class="limpiarBox"
												controller="registroPedido" update="legajoUsuarioAprobadorEconomico"
												id="${RolAplicacion.APROBADOR_IMPACTO}"
												params="['username': '', 'box':'legajoUsuarioAprobadorEconomico', 'pedidoId':pedidoInstance?.id]"
												action="completarUsuario"></g:secureRemoteLink>
										</g:if>
									</div>
								</g:if>
									
								<div class="pDos">
									<g:if test="${pedidoInstance.puedeEditarValidacionImpacto(usuarioLogueado)}">
										<label for="justificacionAprobacion" class="formLabel">Justificaci&oacute;n</label>
										<g:textArea name="justificacionAprobacion" value="${pedidoInstance.aprobacionesPendientesPara(usuarioLogueado).isEmpty() ? "" : pedidoInstance.aprobacionesPendientesPara(usuarioLogueado).first().justificacion}"></g:textArea>
									</g:if>
								</div>
								
							</div>
												
							<g:render template="/templates/ultimaJustificacionDenegacion"
					     			model="['pedidoInstance': pedidoInstance, 'fase': pedidoInstance.faseValidacionImpacto()]"></g:render>
							
							<div id="botonera">
								<g:render template="botonera" model="['pedidoInstance':pedidoInstance, 'usuarioLogueado':usuarioLogueado]"></g:render>
							</div>
						</div>
				</g:form>
		</div>
		<!-- fin contenedor izq -->
		
		<!-- Lateral Derecho -->
	<g:render template="/templates/menuLateral" model="['pedidoInstance':pedidoInstance]"/>
	<!-- Fin Lateral Derecho -->
	
			<!-- 
		************
		************
		MODALS POPUP
		************
		************
		************
		-->
		<!-- popup ayuda seccion -->
		<div id="dialog-help" title="Ayuda Aprobar Pedido" style="display: none;">
				<div style="height: 350px; width: 100%; overflow: auto;">
						<p class="helpTitle">Seccion ayuda</p>
						<p>Hola esto es una seccion de ayuda Hola esto es una seccion de</p>
				</div>
		</div>
		<!-- fin popup ayuda seccion -->
		

</body>
</html>
