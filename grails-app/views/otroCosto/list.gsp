
<%@ page import="ar.com.telecom.pcs.entities.OtroCosto" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="layout" content="main" />
        <g:set var="entityName" value="${message(code: 'otroCosto.label', default: 'OtroCosto')}" />
        <title><g:message code="default.list.label" args="[entityName]" /></title>
    </head>
    <body>
        <div class="nav">
            <span class="menuButton"><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></span>
            <span class="menuButton"><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></span>
        </div>
        <div class="body">
            <h1><g:message code="default.list.label" args="[entityName]" /></h1>
            <g:if test="${flash.message}">
            <div class="message">${flash.message}</div>
            </g:if>
            <div class="list">
                <table>
                    <thead>
                        <tr>
                        
                            <g:sortableColumn property="id" title="${message(code: 'otroCosto.id.label', default: 'Id')}" />
                        
                            <g:sortableColumn property="detalle" title="${message(code: 'otroCosto.detalle.label', default: 'Detalle')}" />
                        
                            <g:sortableColumn property="costo" title="${message(code: 'otroCosto.costo.label', default: 'Costo')}" />
                        
                            <th><g:message code="otroCosto.tipoCosto.label" default="Tipo Costo" /></th>
                        
                        </tr>
                    </thead>
                    <tbody>
                    <g:each in="${otroCostoInstanceList}" status="i" var="otroCostoInstance">
                        <tr class="${(i % 2) == 0 ? 'odd' : 'even'}">
                        
                            <td><g:link action="show" id="${otroCostoInstance.id}">${fieldValue(bean: otroCostoInstance, field: "id")}</g:link></td>
                        
                            <td>${fieldValue(bean: otroCostoInstance, field: "detalle")}</td>
                        
                            <td>${fieldValue(bean: otroCostoInstance, field: "costo")}</td>
                        
                            <td>${fieldValue(bean: otroCostoInstance, field: "tipoCosto")}</td>
                        
                        </tr>
                    </g:each>
                    </tbody>
                </table>
            </div>
            <div class="paginateButtons">
                <g:paginate total="${otroCostoInstanceTotal}" />
            </div>
        </div>
    </body>
</html>
