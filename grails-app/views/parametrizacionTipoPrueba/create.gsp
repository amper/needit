

<%@ page import="ar.com.telecom.pcs.entities.ParametrizacionTipoPrueba" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="layout" content="main" />
        <g:set var="entityName" value="${message(code: 'parametrizacionTipoPrueba.label', default: 'ParametrizacionTipoPrueba')}" />
        <title><g:message code="default.create.label" args="[entityName]" /></title>
    </head>
    <body>
        <div class="nav">
            <span class="menuButton"><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></span>
            <span class="menuButton"><g:link class="list" action="list"><g:message code="default.list.label" args="[entityName]" /></g:link></span>
        </div>
        <div class="body">
            <h1><g:message code="default.create.label" args="[entityName]" /></h1>
            <g:if test="${flash.message}">
            <div class="message">${flash.message}</div>
            </g:if>
            <g:hasErrors bean="${parametrizacionTipoPruebaInstance}">
            <div class="errors">
                <g:renderErrors bean="${parametrizacionTipoPruebaInstance}" as="list" />
            </div>
            </g:hasErrors>
            <g:form action="save" >
                <div class="dialog">
                    <table>
                        <tbody>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="tipoImpacto"><g:message code="parametrizacionTipoPrueba.tipoImpacto.label" default="Tipo Impacto" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: parametrizacionTipoPruebaInstance, field: 'tipoImpacto', 'errors')}">
                                    <g:select name="tipoImpacto.id" from="${ar.com.telecom.pcs.entities.TipoImpacto.list()}" optionKey="id" value="${parametrizacionTipoPruebaInstance?.tipoImpacto?.id}" noSelection="['null': '']" />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="estrategiaRealiza"><g:message code="parametrizacionTipoPrueba.estrategiaRealiza.label" default="Estrategia Realiza" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: parametrizacionTipoPruebaInstance, field: 'estrategiaRealiza', 'errors')}">
                                    <g:textField name="estrategiaRealiza" maxlength="3" value="${parametrizacionTipoPruebaInstance?.estrategiaRealiza}" />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="estrategiaConsolidacion"><g:message code="parametrizacionTipoPrueba.estrategiaConsolidacion.label" default="Estrategia Consolidacion" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: parametrizacionTipoPruebaInstance, field: 'estrategiaConsolidacion', 'errors')}">
                                    <g:textField name="estrategiaConsolidacion" maxlength="3" value="${parametrizacionTipoPruebaInstance?.estrategiaConsolidacion}" />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="tipoGestion"><g:message code="parametrizacionTipoPrueba.tipoGestion.label" default="Tipo Gestion" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: parametrizacionTipoPruebaInstance, field: 'tipoGestion', 'errors')}">
                                    <g:select name="tipoGestion.id" from="${ar.com.telecom.pcs.entities.TipoGestion.list()}" optionKey="id" value="${parametrizacionTipoPruebaInstance?.tipoGestion?.id}"  />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="tipoPrueba"><g:message code="parametrizacionTipoPrueba.tipoPrueba.label" default="Tipo Prueba" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: parametrizacionTipoPruebaInstance, field: 'tipoPrueba', 'errors')}">
                                    <g:select name="tipoPrueba.id" from="${ar.com.telecom.pcs.entities.TipoPrueba.list()}" optionKey="id" value="${parametrizacionTipoPruebaInstance?.tipoPrueba?.id}"  />
                                </td>
                            </tr>
                        
                        </tbody>
                    </table>
                </div>
                <div class="buttons">
                    <span class="button"><g:submitButton name="create" class="save" value="${message(code: 'default.button.create.label', default: 'Create')}" /></span>
                </div>
            </g:form>
        </div>
    </body>
</html>
