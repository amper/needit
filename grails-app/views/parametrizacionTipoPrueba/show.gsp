
<%@ page import="ar.com.telecom.pcs.entities.ParametrizacionTipoPrueba" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="layout" content="main" />
        <g:set var="entityName" value="${message(code: 'parametrizacionTipoPrueba.label', default: 'ParametrizacionTipoPrueba')}" />
        <title><g:message code="default.show.label" args="[entityName]" /></title>
    </head>
    <body>
        <div class="nav">
            <span class="menuButton"><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></span>
            <span class="menuButton"><g:link class="list" action="list"><g:message code="default.list.label" args="[entityName]" /></g:link></span>
            <span class="menuButton"><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></span>
        </div>
        <div class="body">
            <h1><g:message code="default.show.label" args="[entityName]" /></h1>
            <g:if test="${flash.message}">
            <div class="message">${flash.message}</div>
            </g:if>
            <div class="dialog">
                <table>
                    <tbody>
                    
                        <tr class="prop">
                            <td valign="top" class="name"><g:message code="parametrizacionTipoPrueba.id.label" default="Id" /></td>
                            
                            <td valign="top" class="value">${fieldValue(bean: parametrizacionTipoPruebaInstance, field: "id")}</td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name"><g:message code="parametrizacionTipoPrueba.tipoImpacto.label" default="Tipo Impacto" /></td>
                            
                            <td valign="top" class="value"><g:link controller="tipoImpacto" action="show" id="${parametrizacionTipoPruebaInstance?.tipoImpacto?.id}">${parametrizacionTipoPruebaInstance?.tipoImpacto?.encodeAsHTML()}</g:link></td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name"><g:message code="parametrizacionTipoPrueba.estrategiaRealiza.label" default="Estrategia Realiza" /></td>
                            
                            <td valign="top" class="value">${fieldValue(bean: parametrizacionTipoPruebaInstance, field: "estrategiaRealiza")}</td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name"><g:message code="parametrizacionTipoPrueba.estrategiaConsolidacion.label" default="Estrategia Consolidacion" /></td>
                            
                            <td valign="top" class="value">${fieldValue(bean: parametrizacionTipoPruebaInstance, field: "estrategiaConsolidacion")}</td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name"><g:message code="parametrizacionTipoPrueba.tipoGestion.label" default="Tipo Gestion" /></td>
                            
                            <td valign="top" class="value"><g:link controller="tipoGestion" action="show" id="${parametrizacionTipoPruebaInstance?.tipoGestion?.id}">${parametrizacionTipoPruebaInstance?.tipoGestion?.encodeAsHTML()}</g:link></td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name"><g:message code="parametrizacionTipoPrueba.tipoPrueba.label" default="Tipo Prueba" /></td>
                            
                            <td valign="top" class="value"><g:link controller="tipoPrueba" action="show" id="${parametrizacionTipoPruebaInstance?.tipoPrueba?.id}">${parametrizacionTipoPruebaInstance?.tipoPrueba?.encodeAsHTML()}</g:link></td>
                            
                        </tr>
                    
                    </tbody>
                </table>
            </div>
            <div class="buttons">
                <g:form>
                    <g:hiddenField name="id" value="${parametrizacionTipoPruebaInstance?.id}" />
                    <span class="button"><g:actionSubmit class="edit" action="edit" value="${message(code: 'default.button.edit.label', default: 'Edit')}" /></span>
                    <span class="button"><g:actionSubmit class="delete" action="delete" value="${message(code: 'default.button.delete.label', default: 'Delete')}" onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');" /></span>
                </g:form>
            </div>
        </div>
    </body>
</html>
