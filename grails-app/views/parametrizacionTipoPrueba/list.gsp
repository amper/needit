
<%@ page import="ar.com.telecom.pcs.entities.ParametrizacionTipoPrueba" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="layout" content="main" />
        <g:set var="entityName" value="${message(code: 'parametrizacionTipoPrueba.label', default: 'ParametrizacionTipoPrueba')}" />
        <title><g:message code="default.list.label" args="[entityName]" /></title>
    </head>
    <body>
        <div class="nav">
            <span class="menuButton"><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></span>
            <span class="menuButton"><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></span>
        </div>
        <div class="body">
            <h1><g:message code="default.list.label" args="[entityName]" /></h1>
            <g:if test="${flash.message}">
            <div class="message">${flash.message}</div>
            </g:if>
            <div class="list">
                <table>
                    <thead>
                        <tr>
                        
                            <g:sortableColumn property="id" title="${message(code: 'parametrizacionTipoPrueba.id.label', default: 'Id')}" />
                        
                            <th><g:message code="parametrizacionTipoPrueba.tipoImpacto.label" default="Tipo Impacto" /></th>
                        
                            <g:sortableColumn property="estrategiaRealiza" title="${message(code: 'parametrizacionTipoPrueba.estrategiaRealiza.label', default: 'Estrategia Realiza')}" />
                        
                            <g:sortableColumn property="estrategiaConsolidacion" title="${message(code: 'parametrizacionTipoPrueba.estrategiaConsolidacion.label', default: 'Estrategia Consolidacion')}" />
                        
                            <th><g:message code="parametrizacionTipoPrueba.tipoGestion.label" default="Tipo Gestion" /></th>
                        
                            <th><g:message code="parametrizacionTipoPrueba.tipoPrueba.label" default="Tipo Prueba" /></th>
                        
                        </tr>
                    </thead>
                    <tbody>
                    <g:each in="${parametrizacionTipoPruebaInstanceList}" status="i" var="parametrizacionTipoPruebaInstance">
                        <tr class="${(i % 2) == 0 ? 'odd' : 'even'}">
                        
                            <td><g:link action="show" id="${parametrizacionTipoPruebaInstance.id}">${fieldValue(bean: parametrizacionTipoPruebaInstance, field: "id")}</g:link></td>
                        
                            <td>${fieldValue(bean: parametrizacionTipoPruebaInstance, field: "tipoImpacto")}</td>
                        
                            <td>${fieldValue(bean: parametrizacionTipoPruebaInstance, field: "estrategiaRealiza")}</td>
                        
                            <td>${fieldValue(bean: parametrizacionTipoPruebaInstance, field: "estrategiaConsolidacion")}</td>
                        
                            <td>${fieldValue(bean: parametrizacionTipoPruebaInstance, field: "tipoGestion")}</td>
                        
                            <td>${fieldValue(bean: parametrizacionTipoPruebaInstance, field: "tipoPrueba")}</td>
                        
                        </tr>
                    </g:each>
                    </tbody>
                </table>
            </div>
            <div class="paginateButtons">
                <g:paginate total="${parametrizacionTipoPruebaInstanceTotal}" />
            </div>
        </div>
    </body>
</html>
