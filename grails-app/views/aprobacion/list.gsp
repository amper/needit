
<%@ page import="ar.com.telecom.pcs.entities.Aprobacion" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="layout" content="main" />
        <g:set var="entityName" value="${message(code: 'aprobacion.label', default: 'Aprobacion')}" />
        <title><g:message code="default.list.label" args="[entityName]" /></title>
    </head>
    <body>
        <div class="nav">
            <span class="menuButton"><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></span>
            <span class="menuButton"><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></span>
        </div>
        <div class="body">
            <h1><g:message code="default.list.label" args="[entityName]" /></h1>
            <g:if test="${flash.message}">
            <div class="message">${flash.message}</div>
            </g:if>
            <div class="list">
                <table>
                    <thead>
                        <tr>
                        
                            <g:sortableColumn property="id" title="${message(code: 'aprobacion.id.label', default: 'Id')}" />
                        
                            <g:sortableColumn property="fecha" title="${message(code: 'aprobacion.fecha.label', default: 'Fecha')}" />
                        
                            <g:sortableColumn property="grupo" title="${message(code: 'aprobacion.grupo.label', default: 'Grupo')}" />
                        
                            <g:sortableColumn property="usuario" title="${message(code: 'aprobacion.usuario.label', default: 'Usuario')}" />
                        
                            <g:sortableColumn property="justificacion" title="${message(code: 'aprobacion.justificacion.label', default: 'Justificacion')}" />
                        
                            <g:sortableColumn property="descripcionAprobacionSolicitada" title="${message(code: 'aprobacion.descripcionAprobacionSolicitada.label', default: 'Descripcion Aprobacion Solicitada')}" />
                        
                        </tr>
                    </thead>
                    <tbody>
                    <g:each in="${aprobacionInstanceList}" status="i" var="aprobacionInstance">
                        <tr class="${(i % 2) == 0 ? 'odd' : 'even'}">
                        
                            <td><g:link action="show" id="${aprobacionInstance.id}">${fieldValue(bean: aprobacionInstance, field: "id")}</g:link></td>
                        
                            <td><g:formatDate date="${aprobacionInstance.fecha}" /></td>
                        
                            <td>${fieldValue(bean: aprobacionInstance, field: "grupo")}</td>
                        
                            <td>${fieldValue(bean: aprobacionInstance, field: "usuario")}</td>
                        
                            <td>${fieldValue(bean: aprobacionInstance, field: "justificacion")}</td>
                        
                            <td>${fieldValue(bean: aprobacionInstance, field: "descripcionAprobacionSolicitada")}</td>
                        
                        </tr>
                    </g:each>
                    </tbody>
                </table>
            </div>
            <div class="paginateButtons">
                <g:paginate total="${aprobacionInstanceTotal}" />
            </div>
        </div>
    </body>
</html>
