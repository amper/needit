
<%@ page import="ar.com.telecom.pcs.entities.Aprobacion" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="layout" content="main" />
        <g:set var="entityName" value="${message(code: 'aprobacion.label', default: 'Aprobacion')}" />
        <title><g:message code="default.show.label" args="[entityName]" /></title>
    </head>
    <body>
        <div class="nav">
            <span class="menuButton"><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></span>
            <span class="menuButton"><g:link class="list" action="list"><g:message code="default.list.label" args="[entityName]" /></g:link></span>
            <span class="menuButton"><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></span>
        </div>
        <div class="body">
            <h1><g:message code="default.show.label" args="[entityName]" /></h1>
            <g:if test="${flash.message}">
            <div class="message">${flash.message}</div>
            </g:if>
            <div class="dialog">
                <table>
                    <tbody>
                    
                        <tr class="prop">
                            <td valign="top" class="name"><g:message code="aprobacion.id.label" default="Id" /></td>
                            
                            <td valign="top" class="value">${fieldValue(bean: aprobacionInstance, field: "id")}</td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name"><g:message code="aprobacion.fecha.label" default="Fecha" /></td>
                            
                            <td valign="top" class="value"><g:formatDate date="${aprobacionInstance?.fecha}" /></td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name"><g:message code="aprobacion.grupo.label" default="Grupo" /></td>
                            
                            <td valign="top" class="value">${fieldValue(bean: aprobacionInstance, field: "grupo")}</td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name"><g:message code="aprobacion.usuario.label" default="Usuario" /></td>
                            
                            <td valign="top" class="value">${fieldValue(bean: aprobacionInstance, field: "usuario")}</td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name"><g:message code="aprobacion.justificacion.label" default="Justificacion" /></td>
                            
                            <td valign="top" class="value">${fieldValue(bean: aprobacionInstance, field: "justificacion")}</td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name"><g:message code="aprobacion.descripcionAprobacionSolicitada.label" default="Descripcion Aprobacion Solicitada" /></td>
                            
                            <td valign="top" class="value">${fieldValue(bean: aprobacionInstance, field: "descripcionAprobacionSolicitada")}</td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name"><g:message code="aprobacion.areaSoporte.label" default="Area Soporte" /></td>
                            
                            <td valign="top" class="value"><g:link controller="areaSoporte" action="show" id="${aprobacionInstance?.areaSoporte?.id}">${aprobacionInstance?.areaSoporte?.encodeAsHTML()}</g:link></td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name"><g:message code="aprobacion.rol.label" default="Rol" /></td>
                            
                            <td valign="top" class="value">${fieldValue(bean: aprobacionInstance, field: "rol")}</td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name"><g:message code="aprobacion.estado.label" default="Estado" /></td>
                            
                            <td valign="top" class="value">${fieldValue(bean: aprobacionInstance, field: "estado")}</td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name"><g:message code="aprobacion.fase.label" default="Fase" /></td>
                            
                            <td valign="top" class="value"><g:link controller="fase" action="show" id="${aprobacionInstance?.fase?.id}">${aprobacionInstance?.fase?.encodeAsHTML()}</g:link></td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name"><g:message code="aprobacion.pedido.label" default="Pedido" /></td>
                            
                            <td valign="top" class="value"><g:link controller="abstractPedido" action="show" id="${aprobacionInstance?.pedido?.id}">${aprobacionInstance?.pedido?.encodeAsHTML()}</g:link></td>
                            
                        </tr>
                    
                    </tbody>
                </table>
            </div>
            <div class="buttons">
                <g:form>
                    <g:hiddenField name="id" value="${aprobacionInstance?.id}" />
                    <span class="button"><g:actionSubmit class="edit" action="edit" value="${message(code: 'default.button.edit.label', default: 'Edit')}" /></span>
                    <span class="button"><g:actionSubmit class="delete" action="delete" value="${message(code: 'default.button.delete.label', default: 'Delete')}" onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');" /></span>
                </g:form>
            </div>
        </div>
    </body>
</html>
