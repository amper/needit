

<%@ page import="ar.com.telecom.pcs.entities.Aprobacion" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="layout" content="main" />
        <g:set var="entityName" value="${message(code: 'aprobacion.label', default: 'Aprobacion')}" />
        <title><g:message code="default.edit.label" args="[entityName]" /></title>
    </head>
    <body>
        <div class="nav">
            <span class="menuButton"><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></span>
            <span class="menuButton"><g:link class="list" action="list"><g:message code="default.list.label" args="[entityName]" /></g:link></span>
            <span class="menuButton"><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></span>
        </div>
        <div class="body">
            <h1><g:message code="default.edit.label" args="[entityName]" /></h1>
            <g:if test="${flash.message}">
            <div class="message">${flash.message}</div>
            </g:if>
            <g:hasErrors bean="${aprobacionInstance}">
            <div class="errors">
                <g:renderErrors bean="${aprobacionInstance}" as="list" />
            </div>
            </g:hasErrors>
            <g:form method="post" >
                <g:hiddenField name="id" value="${aprobacionInstance?.id}" />
                <g:hiddenField name="version" value="${aprobacionInstance?.version}" />
                <div class="dialog">
                    <table>
                        <tbody>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                  <label for="fecha"><g:message code="aprobacion.fecha.label" default="Fecha" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: aprobacionInstance, field: 'fecha', 'errors')}">
                                    <g:datePicker name="fecha" precision="day" value="${aprobacionInstance?.fecha}" default="none" noSelection="['': '']" />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                  <label for="grupo"><g:message code="aprobacion.grupo.label" default="Grupo" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: aprobacionInstance, field: 'grupo', 'errors')}">
                                    <g:textField name="grupo" value="${aprobacionInstance?.grupo}" />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                  <label for="usuario"><g:message code="aprobacion.usuario.label" default="Usuario" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: aprobacionInstance, field: 'usuario', 'errors')}">
                                    <g:textField name="usuario" value="${aprobacionInstance?.usuario}" />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                  <label for="justificacion"><g:message code="aprobacion.justificacion.label" default="Justificacion" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: aprobacionInstance, field: 'justificacion', 'errors')}">
                                    <g:textField name="justificacion" value="${aprobacionInstance?.justificacion}" />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                  <label for="descripcionAprobacionSolicitada"><g:message code="aprobacion.descripcionAprobacionSolicitada.label" default="Descripcion Aprobacion Solicitada" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: aprobacionInstance, field: 'descripcionAprobacionSolicitada', 'errors')}">
                                    <g:textField name="descripcionAprobacionSolicitada" maxlength="60" value="${aprobacionInstance?.descripcionAprobacionSolicitada}" />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                  <label for="areaSoporte"><g:message code="aprobacion.areaSoporte.label" default="Area Soporte" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: aprobacionInstance, field: 'areaSoporte', 'errors')}">
                                    <g:select name="areaSoporte.id" from="${ar.com.telecom.pcs.entities.AreaSoporte.list()}" optionKey="id" value="${aprobacionInstance?.areaSoporte?.id}" noSelection="['null': '']" />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                  <label for="rol"><g:message code="aprobacion.rol.label" default="Rol" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: aprobacionInstance, field: 'rol', 'errors')}">
                                    <g:textField name="rol" value="${aprobacionInstance?.rol}" />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                  <label for="estado"><g:message code="aprobacion.estado.label" default="Estado" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: aprobacionInstance, field: 'estado', 'errors')}">
                                    <g:textField name="estado" value="${aprobacionInstance?.estado}" />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                  <label for="fase"><g:message code="aprobacion.fase.label" default="Fase" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: aprobacionInstance, field: 'fase', 'errors')}">
                                    <g:select name="fase.id" from="${ar.com.telecom.pcs.entities.Fase.list()}" optionKey="id" value="${aprobacionInstance?.fase?.id}"  />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                  <label for="pedido"><g:message code="aprobacion.pedido.label" default="Pedido" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: aprobacionInstance, field: 'pedido', 'errors')}">
                                    <g:select name="pedido.id" from="${ar.com.telecom.pcs.entities.AbstractPedido.list()}" optionKey="id" value="${aprobacionInstance?.pedido?.id}"  />
                                </td>
                            </tr>
                        
                        </tbody>
                    </table>
                </div>
                <div class="buttons">
                    <span class="button"><g:actionSubmit class="save" action="update" value="${message(code: 'default.button.update.label', default: 'Update')}" /></span>
                    <span class="button"><g:actionSubmit class="delete" action="delete" value="${message(code: 'default.button.delete.label', default: 'Delete')}" onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');" /></span>
                </div>
            </g:form>
        </div>
    </body>
</html>
