
<%@ page import="ar.com.telecom.pcs.entities.AbstractPedido" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="layout" content="main" />
        <g:set var="entityName" value="${message(code: 'abstractPedido.label', default: 'AbstractPedido')}" />
        <title><g:message code="default.show.label" args="[entityName]" /></title>
    </head>
    <body>
        <div class="nav">
            <span class="menuButton"><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></span>
            <span class="menuButton"><g:link class="list" action="list"><g:message code="default.list.label" args="[entityName]" /></g:link></span>
            <span class="menuButton"><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></span>
        </div>
        <div class="body">
            <h1><g:message code="default.show.label" args="[entityName]" /></h1>
            <g:if test="${flash.message}">
            <div class="message">${flash.message}</div>
            </g:if>
            <div class="dialog">
                <table>
                    <tbody>
                    
                        <tr class="prop">
                            <td valign="top" class="name"><g:message code="abstractPedido.id.label" default="Id" /></td>
                            
                            <td valign="top" class="value">${fieldValue(bean: abstractPedidoInstance, field: "id")}</td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name"><g:message code="abstractPedido.titulo.label" default="Titulo" /></td>
                            
                            <td valign="top" class="value">${fieldValue(bean: abstractPedidoInstance, field: "titulo")}</td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name"><g:message code="abstractPedido.legajoUsuarioCreador.label" default="Legajo Usuario Creador" /></td>
                            
                            <td valign="top" class="value">${fieldValue(bean: abstractPedidoInstance, field: "legajoUsuarioCreador")}</td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name"><g:message code="abstractPedido.fechaUltimaModificacion.label" default="Fecha Ultima Modificacion" /></td>
                            
                            <td valign="top" class="value"><g:formatDate date="${abstractPedidoInstance?.fechaUltimaModificacion}" /></td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name"><g:message code="abstractPedido.fechaCargaPedido.label" default="Fecha Carga Pedido" /></td>
                            
                            <td valign="top" class="value"><g:formatDate date="${abstractPedidoInstance?.fechaCargaPedido}" /></td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name"><g:message code="abstractPedido.fechaCancelacion.label" default="Fecha Cancelacion" /></td>
                            
                            <td valign="top" class="value"><g:formatDate date="${abstractPedidoInstance?.fechaCancelacion}" /></td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name"><g:message code="abstractPedido.fechaSuspension.label" default="Fecha Suspension" /></td>
                            
                            <td valign="top" class="value"><g:formatDate date="${abstractPedidoInstance?.fechaSuspension}" /></td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name"><g:message code="abstractPedido.fechaReplanificacion.label" default="Fecha Replanificacion" /></td>
                            
                            <td valign="top" class="value"><g:formatDate date="${abstractPedidoInstance?.fechaReplanificacion}" /></td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name"><g:message code="abstractPedido.motivoCancelacion.label" default="Motivo Cancelacion" /></td>
                            
                            <td valign="top" class="value">${fieldValue(bean: abstractPedidoInstance, field: "motivoCancelacion")}</td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name"><g:message code="abstractPedido.motivoSuspension.label" default="Motivo Suspension" /></td>
                            
                            <td valign="top" class="value"><g:link controller="motivoSuspension" action="show" id="${abstractPedidoInstance?.motivoSuspension?.id}">${abstractPedidoInstance?.motivoSuspension?.encodeAsHTML()}</g:link></td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name"><g:message code="abstractPedido.comentarioSuspension.label" default="Comentario Suspension" /></td>
                            
                            <td valign="top" class="value">${fieldValue(bean: abstractPedidoInstance, field: "comentarioSuspension")}</td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name"><g:message code="abstractPedido.comentarioReanudacion.label" default="Comentario Reanudacion" /></td>
                            
                            <td valign="top" class="value">${fieldValue(bean: abstractPedidoInstance, field: "comentarioReanudacion")}</td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name"><g:message code="abstractPedido.comentarioRealesIncurridos.label" default="Comentario Reales Incurridos" /></td>
                            
                            <td valign="top" class="value">${fieldValue(bean: abstractPedidoInstance, field: "comentarioRealesIncurridos")}</td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name"><g:message code="abstractPedido.fechaCierre.label" default="Fecha Cierre" /></td>
                            
                            <td valign="top" class="value"><g:formatDate date="${abstractPedidoInstance?.fechaCierre}" /></td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name"><g:message code="abstractPedido.comentarioReasignacion.label" default="Comentario Reasignacion" /></td>
                            
                            <td valign="top" class="value">${fieldValue(bean: abstractPedidoInstance, field: "comentarioReasignacion")}</td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name"><g:message code="abstractPedido.anexos.label" default="Anexos" /></td>
                            
                            <td valign="top" style="text-align: left;" class="value">
                                <ul>
                                <g:each in="${abstractPedidoInstance.anexos}" var="a">
                                    <li><g:link controller="anexoPorTipo" action="show" id="${a.id}">${a?.encodeAsHTML()}</g:link></li>
                                </g:each>
                                </ul>
                            </td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name"><g:message code="abstractPedido.aprobaciones.label" default="Aprobaciones" /></td>
                            
                            <td valign="top" style="text-align: left;" class="value">
                                <ul>
                                <g:each in="${abstractPedidoInstance.aprobaciones}" var="a">
                                    <li><g:link controller="aprobacion" action="show" id="${a.id}">${a?.encodeAsHTML()}</g:link></li>
                                </g:each>
                                </ul>
                            </td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name"><g:message code="abstractPedido.diseniosExternos.label" default="Disenios Externos" /></td>
                            
                            <td valign="top" style="text-align: left;" class="value">
                                <ul>
                                <g:each in="${abstractPedidoInstance.diseniosExternos}" var="d">
                                    <li><g:link controller="disenioExterno" action="show" id="${d.id}">${d?.encodeAsHTML()}</g:link></li>
                                </g:each>
                                </ul>
                            </td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name"><g:message code="abstractPedido.estrategiasPrueba.label" default="Estrategias Prueba" /></td>
                            
                            <td valign="top" style="text-align: left;" class="value">
                                <ul>
                                <g:each in="${abstractPedidoInstance.estrategiasPrueba}" var="e">
                                    <li><g:link controller="estrategiaPrueba" action="show" id="${e.id}">${e?.encodeAsHTML()}</g:link></li>
                                </g:each>
                                </ul>
                            </td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name"><g:message code="abstractPedido.faseActual.label" default="Fase Actual" /></td>
                            
                            <td valign="top" class="value"><g:link controller="fase" action="show" id="${abstractPedidoInstance?.faseActual?.id}">${abstractPedidoInstance?.faseActual?.encodeAsHTML()}</g:link></td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name"><g:message code="abstractPedido.logModificaciones.label" default="Log Modificaciones" /></td>
                            
                            <td valign="top" style="text-align: left;" class="value">
                                <ul>
                                <g:each in="${abstractPedidoInstance.logModificaciones}" var="l">
                                    <li><g:link controller="logModificaciones" action="show" id="${l.id}">${l?.encodeAsHTML()}</g:link></li>
                                </g:each>
                                </ul>
                            </td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name"><g:message code="abstractPedido.planificaciones.label" default="Planificaciones" /></td>
                            
                            <td valign="top" style="text-align: left;" class="value">
                                <ul>
                                <g:each in="${abstractPedidoInstance.planificaciones}" var="p">
                                    <li><g:link controller="planificacionEsfuerzo" action="show" id="${p.id}">${p?.encodeAsHTML()}</g:link></li>
                                </g:each>
                                </ul>
                            </td>
                            
                        </tr>
                    
                    </tbody>
                </table>
            </div>
            <div class="buttons">
                <g:form>
                    <g:hiddenField name="id" value="${abstractPedidoInstance?.id}" />
                    <span class="button"><g:actionSubmit class="edit" action="edit" value="${message(code: 'default.button.edit.label', default: 'Edit')}" /></span>
                    <span class="button"><g:actionSubmit class="delete" action="delete" value="${message(code: 'default.button.delete.label', default: 'Delete')}" onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');" /></span>
                </g:form>
            </div>
        </div>
    </body>
</html>
