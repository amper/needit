

<%@ page import="ar.com.telecom.pcs.entities.AbstractPedido" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="layout" content="main" />
        <g:set var="entityName" value="${message(code: 'abstractPedido.label', default: 'AbstractPedido')}" />
        <title><g:message code="default.create.label" args="[entityName]" /></title>
    </head>
    <body>
        <div class="nav">
            <span class="menuButton"><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></span>
            <span class="menuButton"><g:link class="list" action="list"><g:message code="default.list.label" args="[entityName]" /></g:link></span>
        </div>
        <div class="body">
            <h1><g:message code="default.create.label" args="[entityName]" /></h1>
            <g:if test="${flash.message}">
            <div class="message">${flash.message}</div>
            </g:if>
            <g:hasErrors bean="${abstractPedidoInstance}">
            <div class="errors">
                <g:renderErrors bean="${abstractPedidoInstance}" as="list" />
            </div>
            </g:hasErrors>
            <g:form action="save" >
                <div class="dialog">
                    <table>
                        <tbody>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="titulo"><g:message code="abstractPedido.titulo.label" default="Titulo" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: abstractPedidoInstance, field: 'titulo', 'errors')}">
                                    <g:textField name="titulo" maxlength="200" value="${abstractPedidoInstance?.titulo}" />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="legajoUsuarioCreador"><g:message code="abstractPedido.legajoUsuarioCreador.label" default="Legajo Usuario Creador" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: abstractPedidoInstance, field: 'legajoUsuarioCreador', 'errors')}">
                                    <g:textField name="legajoUsuarioCreador" value="${abstractPedidoInstance?.legajoUsuarioCreador}" />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="fechaUltimaModificacion"><g:message code="abstractPedido.fechaUltimaModificacion.label" default="Fecha Ultima Modificacion" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: abstractPedidoInstance, field: 'fechaUltimaModificacion', 'errors')}">
                                    <g:datePicker name="fechaUltimaModificacion" precision="day" value="${abstractPedidoInstance?.fechaUltimaModificacion}" default="none" noSelection="['': '']" />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="fechaCargaPedido"><g:message code="abstractPedido.fechaCargaPedido.label" default="Fecha Carga Pedido" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: abstractPedidoInstance, field: 'fechaCargaPedido', 'errors')}">
                                    <g:datePicker name="fechaCargaPedido" precision="day" value="${abstractPedidoInstance?.fechaCargaPedido}" default="none" noSelection="['': '']" />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="fechaCancelacion"><g:message code="abstractPedido.fechaCancelacion.label" default="Fecha Cancelacion" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: abstractPedidoInstance, field: 'fechaCancelacion', 'errors')}">
                                    <g:datePicker name="fechaCancelacion" precision="day" value="${abstractPedidoInstance?.fechaCancelacion}" default="none" noSelection="['': '']" />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="fechaSuspension"><g:message code="abstractPedido.fechaSuspension.label" default="Fecha Suspension" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: abstractPedidoInstance, field: 'fechaSuspension', 'errors')}">
                                    <g:datePicker name="fechaSuspension" precision="day" value="${abstractPedidoInstance?.fechaSuspension}" default="none" noSelection="['': '']" />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="fechaReplanificacion"><g:message code="abstractPedido.fechaReplanificacion.label" default="Fecha Replanificacion" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: abstractPedidoInstance, field: 'fechaReplanificacion', 'errors')}">
                                    <g:datePicker name="fechaReplanificacion" precision="day" value="${abstractPedidoInstance?.fechaReplanificacion}" default="none" noSelection="['': '']" />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="motivoCancelacion"><g:message code="abstractPedido.motivoCancelacion.label" default="Motivo Cancelacion" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: abstractPedidoInstance, field: 'motivoCancelacion', 'errors')}">
                                    <g:textField name="motivoCancelacion" value="${abstractPedidoInstance?.motivoCancelacion}" />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="motivoSuspension"><g:message code="abstractPedido.motivoSuspension.label" default="Motivo Suspension" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: abstractPedidoInstance, field: 'motivoSuspension', 'errors')}">
                                    <g:select name="motivoSuspension.id" from="${ar.com.telecom.pcs.entities.MotivoSuspension.list()}" optionKey="id" value="${abstractPedidoInstance?.motivoSuspension?.id}" noSelection="['null': '']" />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="comentarioSuspension"><g:message code="abstractPedido.comentarioSuspension.label" default="Comentario Suspension" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: abstractPedidoInstance, field: 'comentarioSuspension', 'errors')}">
                                    <g:textField name="comentarioSuspension" value="${abstractPedidoInstance?.comentarioSuspension}" />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="comentarioReanudacion"><g:message code="abstractPedido.comentarioReanudacion.label" default="Comentario Reanudacion" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: abstractPedidoInstance, field: 'comentarioReanudacion', 'errors')}">
                                    <g:textField name="comentarioReanudacion" value="${abstractPedidoInstance?.comentarioReanudacion}" />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="comentarioRealesIncurridos"><g:message code="abstractPedido.comentarioRealesIncurridos.label" default="Comentario Reales Incurridos" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: abstractPedidoInstance, field: 'comentarioRealesIncurridos', 'errors')}">
                                    <g:textField name="comentarioRealesIncurridos" value="${abstractPedidoInstance?.comentarioRealesIncurridos}" />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="fechaCierre"><g:message code="abstractPedido.fechaCierre.label" default="Fecha Cierre" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: abstractPedidoInstance, field: 'fechaCierre', 'errors')}">
                                    <g:datePicker name="fechaCierre" precision="day" value="${abstractPedidoInstance?.fechaCierre}" default="none" noSelection="['': '']" />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="comentarioReasignacion"><g:message code="abstractPedido.comentarioReasignacion.label" default="Comentario Reasignacion" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: abstractPedidoInstance, field: 'comentarioReasignacion', 'errors')}">
                                    <g:textField name="comentarioReasignacion" value="${abstractPedidoInstance?.comentarioReasignacion}" />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="faseActual"><g:message code="abstractPedido.faseActual.label" default="Fase Actual" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: abstractPedidoInstance, field: 'faseActual', 'errors')}">
                                    <g:select name="faseActual.id" from="${ar.com.telecom.pcs.entities.Fase.list()}" optionKey="id" value="${abstractPedidoInstance?.faseActual?.id}"  />
                                </td>
                            </tr>
                        
                        </tbody>
                    </table>
                </div>
                <div class="buttons">
                    <span class="button"><g:submitButton name="create" class="save" value="${message(code: 'default.button.create.label', default: 'Create')}" /></span>
                </div>
            </g:form>
        </div>
    </body>
</html>
