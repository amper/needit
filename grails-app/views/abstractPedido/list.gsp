
<%@ page import="ar.com.telecom.pcs.entities.AbstractPedido" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="layout" content="main" />
        <g:set var="entityName" value="${message(code: 'abstractPedido.label', default: 'AbstractPedido')}" />
        <title><g:message code="default.list.label" args="[entityName]" /></title>
    </head>
    <body>
        <div class="nav">
            <span class="menuButton"><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></span>
            <span class="menuButton"><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></span>
        </div>
        <div class="body">
            <h1><g:message code="default.list.label" args="[entityName]" /></h1>
            <g:if test="${flash.message}">
            <div class="message">${flash.message}</div>
            </g:if>
            <div class="list">
                <table>
                    <thead>
                        <tr>
                        
                            <g:sortableColumn property="id" title="${message(code: 'abstractPedido.id.label', default: 'Id')}" />
                        
                            <g:sortableColumn property="titulo" title="${message(code: 'abstractPedido.titulo.label', default: 'Titulo')}" />
                        
                            <g:sortableColumn property="legajoUsuarioCreador" title="${message(code: 'abstractPedido.legajoUsuarioCreador.label', default: 'Legajo Usuario Creador')}" />
                        
                            <g:sortableColumn property="fechaUltimaModificacion" title="${message(code: 'abstractPedido.fechaUltimaModificacion.label', default: 'Fecha Ultima Modificacion')}" />
                        
                            <g:sortableColumn property="fechaCargaPedido" title="${message(code: 'abstractPedido.fechaCargaPedido.label', default: 'Fecha Carga Pedido')}" />
                        
                            <g:sortableColumn property="fechaCancelacion" title="${message(code: 'abstractPedido.fechaCancelacion.label', default: 'Fecha Cancelacion')}" />
                        
                        </tr>
                    </thead>
                    <tbody>
                    <g:each in="${abstractPedidoInstanceList}" status="i" var="abstractPedidoInstance">
                        <tr class="${(i % 2) == 0 ? 'odd' : 'even'}">
                        
                            <td><g:link action="show" id="${abstractPedidoInstance.id}">${fieldValue(bean: abstractPedidoInstance, field: "id")}</g:link></td>
                        
                            <td>${fieldValue(bean: abstractPedidoInstance, field: "titulo")}</td>
                        
                            <td>${fieldValue(bean: abstractPedidoInstance, field: "legajoUsuarioCreador")}</td>
                        
                            <td><g:formatDate date="${abstractPedidoInstance.fechaUltimaModificacion}" /></td>
                        
                            <td><g:formatDate date="${abstractPedidoInstance.fechaCargaPedido}" /></td>
                        
                            <td><g:formatDate date="${abstractPedidoInstance.fechaCancelacion}" /></td>
                        
                        </tr>
                    </g:each>
                    </tbody>
                </table>
            </div>
            <div class="paginateButtons">
                <g:paginate total="${abstractPedidoInstanceTotal}" />
            </div>
        </div>
    </body>
</html>
