<%@page import="ar.com.telecom.pcs.entities.RolAplicacion"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta name="layout" content="mainApp" />
<g:javascript library="boxUser" />
</head>
<body>

	<div class="contenedorTitulo">
		<g:titulo pedido="${pedidoInstance}" />
		<g:workflow pedidoId="${pedidoInstance?.id}" />
	</div>

	<div id="contenedorIzq">
		<g:if test="${flash.message}">
			<div class="message">
				${flash.message}
			</div>
		</g:if> 
		<g:hasErrors bean="${pedidoInstance}">
			<div class="errors">
				<g:renderErrors bean="${pedidoInstance}" as="list" />
			</div>
		</g:hasErrors>
		<g:form class="formGeneral" useToken="true">
			<g:hiddenField name="id" value="${pedidoInstance?.id}" />

			<div class="seccionHide">
				<g:cabeceraPedido pedido="${pedidoInstance}" />
				<g:render template="/templates/workflowHidden" />

				<p class="tituloSeccion">Aprobaci&oacute;n</p>
				<div class="formSeccion">
					<div class="list">
						<table>
							<thead>
								<tr>
									<g:sortableColumn property="grupo"
										title="&Aacute;rea aprobadora" params="${params}" />
									<g:sortableColumn property="usuario" title="Usuario"
										params="${params}" />
									<g:sortableColumn property="estado" title="Estado"
										params="${params}" />
									<g:sortableColumn property="fecha"
										title="Fecha Aprobaci&oacute;n" params="${params}" />
									<g:sortableColumn property="justificacion"
										title="Justificaci&oacute;n" params="${params}" />
									<%--<g:sortableColumn property="fase" title="Fase" params="${params}" />--%>
								</tr>
							</thead>
							<tbody>
								<g:each in="${aprobacionesList}" status="i"
									var="aprobacionInstance">
									<g:set scope="page" var="aprobacion"
										value="${aprobacionInstance}" />
									<tr class="${(i % 2) == 0 ? 'odd' : 'even'}"
										style="cursor: pointer;">

										<td>
											${aprobacionInstance.areaSoporte? aprobacionInstance.areaSoporte : RolAplicacion.findByCodigoRol(aprobacionInstance.rol)}
										</td>
										<td><g:userDescription
												legajo="${aprobacionInstance?.usuario}"
												pedido="${aprobacion?.id}" /></td>
										<td>
											${aprobacionInstance?.estadoDescripcion()}
										</td>
										<td><g:formatDate
												date="${aprobacionInstance?.fechaCumplimiento}"
												type="datetime" style="MEDIUM" /></td>
										<td>
											${aprobacionInstance?.justificacion}
										</td>
										<%--														<td>--%>
										<%--															${aprobacionInstance?.fase}--%>
										<%--														</td>			--%>
									</tr>
								</g:each>
							</tbody>
						</table>
					</div>
					<g:render template="/templates/ultimaJustificacionDenegacion"
						     			model="['pedidoInstance': pedidoInstance, 'fase': pedidoInstance.faseAprobacionCambio()]"></g:render>
						     			
					<g:set var="permiteJustificar" value="${pedidoInstance?.puedeEditarAprobacionCambio(usuarioLogueado)}"/>
											     			
					<g:if test="${permiteJustificar}">
						<div class="pDos">
							<label for="justificacionAprobacion" class="formLabel">Justificaci&oacute;n</label>
							<g:textArea name="justificacionAprobacion"
								value="${justificacionAprobacion}"></g:textArea>
						</div>
					</g:if>
				</div>
			</div>
			<div id="message"></div>
			<g:if test="${permiteJustificar}">
				<div class="formSeccionButton" style="margin-top: 20px;">
<%--					<g:if--%>
<%--						test="${pedidoInstance?.faseActual?.controllerNombre.equals(params.controller) || !pedidoInstance.faseActual || pedidoInstance.legajoInterlocutorUsuario.equals(org.springframework.security.core.context.SecurityContextHolder.getContext().getAuthentication().getPrincipal().getUsername())}">--%>
						<g:actionSubmit class="formSeccionSubmitButton" id="Denegar"
							action="denegarPedido" value="Denegar" />
						<g:actionSubmit class="formSeccionSubmitButton" id="Guardar"
							action="grabarFormulario" value="Guardar" />
						<g:actionSubmit class="formSeccionSubmitButton" id="Aprobar"
							action="aprobarPedido" value="Aprobar" />
<%--					</g:if>--%>
				</div>
			</g:if>
		</g:form>


	</div>
	<!-- fin contenedor izq -->

	<!-- Lateral Derecho -->
	<g:render template="/templates/menuLateral" model="['pedidoInstance':pedidoInstance]"/>
	<!-- Fin Lateral Derecho -->

	<!-- 
	************
	************
	MODALS POPUP
	************
	************
	************
	-->

	<!-- popup ayuda seccion -->
	<div id="dialog-help" title="Ayuda Aprobar Pedido"
		style="display: none;">
		<div style="height: 350px; width: 100%; overflow: auto;">
			<p class="helpTitle">Seccion ayuda</p>
			<p>Hola esto es una seccion de ayuda Hola esto es una seccion de</p>
		</div>
	</div>
	<!-- fin popup ayuda seccion -->
</body>
</html>
