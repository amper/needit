<g:updateToken />
<div class="list" style="text-align: center;">
    <table>
      <tr>
        <th> </th>
        <th>N&uacute;mero</th>
        <th>Sistema</th>
      </tr>
      
      <g:each in="${impactoInstance?.pedido?.pedidosHijos}" status="i" var="hijo">
      <tr class="${(i % 2) == 0 ? 'odd' : 'even'}" onClick="javascript:animatedcollapse.toggle('fila_${hijo.id}');">
        <td id="flecha_${hijo.id}" class="indicatorValoracion"></td>
        <td>${hijo.getNumero()}</td>
        <td>${hijo.toString()}</td>
      </tr>
	      <tr style="background-color:#96afb4;" >
	        <td colspan="6">
	        	<div id="fila_${hijo.id}" style="display: none">
	        		<g:render template="gruposCargados" model="['pedido': hijo]"></g:render>
	        	</div>
	        </td>
	      </tr> 
		  <script type="text/javascript">
			animatedcollapse.addDiv('fila_${hijo.id}', 'fade=0', 'hide=1', 'speed=100');
		  </script>
       </g:each>

    </table>
</div>




<script type="text/javascript">
function aprobacionesToggle(divobj, state) {
	var id = divobj.id;
	var idHijo = id.replace('fila_', '');
	if (state == 'block') {
		$('flecha_'+idHijo).style.background='url("${resource(dir: 'images', file: 'DownTriangleIcon.gif')}") no-repeat';
	} else {
		$('flecha_'+idHijo).style.background='url("${resource(dir: 'images', file: 'RightTriangleIcon.gif')}") no-repeat';
	}
}

//animatedcollapse.ontoggle = function($, divobj, state){ aprobacionesToggle(divobj, state); };
animatedcollapse.init(); contentValoracion

</script>