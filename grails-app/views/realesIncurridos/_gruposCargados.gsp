<b><%@page import="ar.com.telecom.pedido.especificacionImpacto.Impacto"%>
<%@page import="ar.com.telecom.pcs.entities.SoftwareFactory"%>
<g:updateToken />
<center>
<g:if test="${!pedido.getGruposPlanificacion().isEmpty() || (impacto && Impacto.aplicaACoordinacion(impacto))}">
	<table class="pCompleto">
		<tr>
			<th class="centrado"/>Grupo</th>
			<th class="centrado"/>RI Cargados</th>
			<th class="centrado"/>RI Cerrados</th>
	    </tr>

	    <g:if test="${impacto && Impacto.aplicaACoordinacion(impacto)}">
		    <tr class="even">
				<td>Horas de coordinaci&oacute;n</td>
				<td  class="centrado"><g:checkBoxStatusImage estado="${pedido.grupoTieneCargadoRealIncurrido(SoftwareFactory.grupoLDAPHorasCoordinacion)}"/></td>
				<td class="centrado"><g:checkBoxStatusImage estado="${pedido.grupoTieneCerradoRealIncurrido(SoftwareFactory.grupoLDAPHorasCoordinacion)}"/></td>
		    </tr>
	    </g:if>  
	    <g:each in="${pedido.getGruposPlanificacion()}" status="i" var="grupo">
		    <tr class="${(i % 2) == 0 ? 'odd' : 'even'}">
				<td> ${grupo}</td>
				<td  class="centrado"><g:checkBoxStatusImage estado="${pedido.grupoTieneCargadoRealIncurrido(grupo)}"/></td>
				<td class="centrado"><g:checkBoxStatusImage estado="${pedido.grupoTieneCerradoRealIncurrido(grupo)}"/></td>
		    </tr>
	    </g:each>
	    
	</table>
</g:if>
<g:else>
	<br>
	<center>
		<b>${pedido.textoSinGrupoRealIncurrido()}</b>
	</center>
</g:else>
</center>