<%@page import="ar.com.telecom.util.DateUtil"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta name="layout" content="mainApp" />

<g:javascript library="realesIncurridos" />
<g:javascript library="boxUser" />
<g:javascript library="animatedcollapse" />

</head>
<body>

	<div class="contenedorTitulo">
		<g:titulo pedido="${impactoInstance?.pedido}" />
		<g:workflow pedidoId="${impactoInstance?.pedido?.id}" />
	</div>

	<div id="contenedorIzq">
 
		<g:form id="formPedido" name="formPedido" class="formGeneral" useToken="true" >
			<div class="seccionHide">
				<g:render template="/templates/workflowHidden" />
				<g:actionSubmit id="refreshView" controller="realesIncurridos" action="index" style="display:none" value="Refrescar vista" />
				<g:hiddenField name="pedidoHijoId" id="pedidoHijoId" value="${pedidoHijo?.id}"></g:hiddenField>
				<g:hiddenField name="tabSeleccionado" id="tabSeleccionado" value="${params.tabSeleccionado}"/>
				
				<g:cabeceraPedido pedido="${pedidoInstance}" />
				<p class="tituloSeccion">Reales incurridos</p>
				<div class="formSeccion">
					<div class="pDos">
						<label class='formLabel'>Impacto:</label>
						<g:select id="impacto" name="impacto"
							from="${pedidoInstance.getImpactos()}" optionKey="id"
							value="${impacto}" />
						
						<br>
						<br>
							
						<label class='formLabel'>Grupo:</label>
						<g:select id="grupo" name="grupo"
							from="${gruposList}" 
							value="${grupoResponsable}" 
							noSelection="['null': 'Todos']" />
						
					</div>
					<div class="pDos">

						<g:if test="${trabajaSobrePadre || trabajaSobreCoord}">
							<label class="formLabel2" for='inputIntUsuario'>Coordinador cambio</label>
							<div id="legajoCoordinadorCambio" class="boxUser">
								<g:render template="/templates/userBox"
									model="['person':pedidoInstance?.legajoCoordinadorCambio, 'box':'legajoCoordinadorCambio', 'personsAutocomplete': null]"></g:render>
							</div>
						</g:if>
						
<%--						<g:if test="${trabajaSobreCoord}">--%>
<%--								<label class="formLabel2 validate" for='inputIntUsuario'>Coordinador cambio</label>--%>
<%--								<div id="legajoCoordinadorCambio2" class="boxUser">--%>
<%--									<g:render template="/templates/userBox"--%>
<%--										model="['person':pedidoInstance?.legajoCoordinadorCambio, 'box':'legajoCoordinadorCambio2', 'personsAutocomplete': personsAutocompleteAsign]"></g:render>--%>
<%--								</div>--%>
<%--						</g:if>--%>
<%----%>
						<g:if test="${trabajoSobreHijo}">
<%--							<g:if test="${pedidoHijo?.legajoUsuarioResponsable}">--%>
									<label class="formLabel2 validate" for='inputIntUsuario'>Asignatario</label>
									<div id="legajoUsuarioResponsable" class="boxUser">
										<g:render template="/templates/userBox"
											model="['person':pedidoHijo?.legajoUsuarioResponsable, 'box':'legajoUsuarioResponsable', 'personsAutocomplete': personsAutocompleteAsign, 'pedidoHijoId':pedidoHijo?.id, 'impacto': impacto]"></g:render>
									</div>
<%--							</g:if>--%>
<%--							<g:else>--%>
<%--								<br>--%>
<%--								<div class="msgPlanificacionEsf">Este pedido no posee un usuario responsable asignado</div>--%>
<%--							</g:else>--%>
						</g:if>
					</div>
					
					<div class="separator"></div>
					<br>
					<div class="pCompleto">
						<div id="datosImpactoSeleccionado"></div>
							<script type="text/javascript">
								seleccionGrupo($('pedidoId').value, $('impacto').value, $('grupo').value);
							</script>
						</div>
					</div>
				
			</div>
		
		</g:form>
</div>
	<g:render template="/templates/menuLateral" model="['pedidoInstance':pedidoInstance]"/>
</body>
</html>
