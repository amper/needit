<%@page import="ar.com.telecom.util.DateUtil"%>
<g:updateToken pedido="${impactoInstance?.pedido?.parent}" pedidoHijo="${impactoInstance?.pedido}"/>
<%
totalHoras = detalle.totalHoras(grupo)
totalHorasIncurridas = detalle.totalHorasIncurridas(grupo)
if(totalHoras == 0){ totalHoras = '' }
if(totalHorasIncurridas == 0){ totalHorasIncurridas = '' }
%>
<g:set var="pedidoOrigen" value="${impactoInstance ? impactoInstance.pedido : pedidoInstance}"/>
<g:set var="aprobacion" value="${pedidoOrigen?.aprobacionPendiente(pedidoOrigen?.faseActual, usuarioLogueado)}"/>
<td style="width: 200px">
	${detalle.getDescripcion()}
</td>
<td style="width: 60px">
	${DateUtil.toString(detalle.fechaDesde)}
</td>
<td style="width: 60px">
	${DateUtil.toString(detalle.fechaHasta)}
</td>
<td style="width: 100px; text-align: right;">
	${totalHoras} 
</td>
<g:if
	test="${!impactoInstance.esConsolidado() && grupo && detalle.puedeEditar(impactoInstance.pedido, usuarioLogueado, grupo) && !detalle.confirmoRealesIncurridos(grupo) && impactoInstance.editaRealesIncurridos(usuarioLogueado)}">
	<td style="width: 100px; text-align: right;"><g:textField
			id="horasIncurridas_${detalle.id}" name="incu_${detalle.id}"
			style="margin-top:0px;text-align:right;"
			onkeypress='soloNumeros(event,null)' value="${totalHorasIncurridas}" />
	</td>

	<td style="width: 20px"><g:secureRemoteLink title="Limpiar"
			class="limpiar sinMargenIzquierdo" update="inc_${detalle.id}"
			params="['detalleId':detalle.id, impacto:impactoInstance.id, grupo:grupo, 'aprobacionId':aprobacion?.id]"
			action="limpiaDetalle"
			onComplete="actualizaGrupos('${impactoInstance.getId()}', '${impactoInstance.pedido.id}')">
		</g:secureRemoteLink></td>

	<td style="width: 20px"><a
		href="javascript:confirmaHorasIncurridas('${detalle.id}', '${grupo}', '${impactoInstance.getId()}', '${impactoInstance.pedido.id}');"
		class="finalizar sinMargenIzquierdo" title="Confirmar"></a></td>

	<td style="width: 20px"><g:secureRemoteLink
			title="Copiar horas incurridas de las planificadas"
			class="copiar sinMargenIzquierdo" update="inc_${detalle.id}"
			params="['detalleId':detalle.id, impacto:impactoInstance.id, grupo:grupo, 'aprobacionId':aprobacion?.id]"
			action="copiaHorasPlanificadasAIncurridas"
			onComplete="actualizaGrupos('${impactoInstance.getId()}', '${impactoInstance.pedido.id}')">
		</g:secureRemoteLink></td>
</g:if>
<g:else>
	<td style="width: 100px; text-align: right;"><span>
			${totalHorasIncurridas}
	</span>
	</td>
	<td style="width: 20px"></td>
	<td style="width: 20px"></td>
	<td style="width: 20px"></td>
</g:else>
