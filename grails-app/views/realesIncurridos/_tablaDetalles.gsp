<%@page import="ar.com.telecom.util.NumberUtil"%>
<%@page import="ar.com.telecom.util.DateUtil"%>
<g:updateToken />
<center>
<g:if test = "${!impactoInstance.esConsolidado()}">
	<table class="pCompleto">
		<tr>
			<th class="centrado"/>Fase Planificaci&oacute;n / Tarea </th>
			<th class="centrado" style="width: 60px"/>Fecha desde </th>
			<th class="centrado" style="width: 60px"/>Fecha hasta </th>
			<th class="centrado" style="width: 100px"/>Total hrs planificadas</th>
			<th class="centrado" style="width: 100px"/>Total hrs incurridas</th>
		</tr>	
	    <g:each in="${detallesPlanificacionFiltrados}" status="i" var="detalle">
		    <tr id="inc_${detalle.id}" class="${(i % 2) == 0 ? 'odd' : 'even'}">
		    	<g:render template="filaDetalle" model="['detalle': detalle, 'grupo':grupo, 'impactoInstance':impactoInstance, 'usuarioLogueado':usuarioLogueado]"></g:render>
		    </tr>
	    </g:each>
	</table>
		
	<div class="separator"></div>
	<br>
	 
	<div id="gruposCargados" class= "pDos"> 
		<g:render template="gruposCargados" model="['pedido': impactoInstance.pedido, impacto:impactoInstance.id.toString()]"></g:render>
	</div>
	
	<div class= "pDos"> 
		<g:if test="${!impactoInstance.esConsolidado()}">
			<g:if test="${impactoInstance.editaRealesIncurridos(usuarioLogueado)}">	
				<div style="text-align:left;">
					<label for="justificacionAprobacion" class="formLabel">Comentarios:</label>
					<g:textArea name="comentarioRealesIncurridos" value="${impactoInstance.pedido.comentarioRealesIncurridos}"></g:textArea>
				</div>
			</g:if>
			<g:else>
				<div style="text-align:left;">
					<label for="justificacionAprobacion" class="formLabel">Comentarios:</label>
					<p> ${impactoInstance.pedido.comentarioRealesIncurridos} </p>
				</div>
			</g:else>
		</g:if>
	</div>
	
	<div class="separator"></div>
	<br>
	
	<g:if test="${impactoInstance.editaRealesIncurridos(usuarioLogueado)}">
		<g:actionSubmit class="formSeccionSubmitButton" id="Guardar" action="grabarFormulario" value="Guardar" style="width:80px;"/>
	</g:if>
</g:if>
<g:else>

	<g:if test="${consolidacionList}">
	<table>
		<tr>
			<th style="width: 200px">Fase planificaci&oacute;n / Tarea </th>
			<th style="width: 100px">Desde</th>
			<th style="width: 100px">Hasta</th>
			<th style="text-align: right;">Total Horas</th>
			<th style="text-align: right;">Monto (en Pesos)</th>
			<th style="text-align: right;">Total Horas incurridas</th>
		</tr>
	
	<!-- SHOW -->
		<g:each in="${consolidacionList}" var="detalle" status="i">
		 <tr style="padding: 0px;" class="${(i % 2) == 0 ? 'odd' : 'even'}">
	
			<td id="fase1">
				${detalle.getAt(0)}
			</td>
			
			<td>${DateUtil.toString(detalle.getAt(1))}</td>
			<td>${DateUtil.toString(detalle.getAt(2))}</td>
			
			<td style="text-align: right; width: 70px;" > ${detalle?.getAt(3)} </td>
			<td style="text-align: right;">${NumberUtil.toString(detalle.getAt(4))}</td>
			<td style="text-align: right;">${detalle.getAt(5)}</td>
		 </tr>
		</g:each>
	</table>	
	
	<div class="separator"></div>
	<br>
<%--	<div id="gruposCargados" class= "pDos"> --%>
<%--		<g:render template="gruposCargadosConsolidado" model="['impactoInstance': impactoInstance]"></g:render>--%>
<%--	</div>--%>
	
	
	
	</g:if>				
	<g:else>
	
	<br>
	
	<table class="pCompleto">
		<tr>
			<th class="centrado"/>Fase Planificaci&oacute;n / Tarea </th>
			<th class="centrado" style="width: 60px"/>Fecha desde </th>
			<th class="centrado" style="width: 60px"/>Fecha hasta </th>
			<th class="centrado" style="width: 100px"/>Total hrs planificadas</th>
			<th class="centrado" style="width: 100px"/>Total hrs incurridas</th>
		</tr>	
	    <g:each in="${detallesPlanificacionFiltrados}" status="i" var="detalle">
		    <tr id="inc_${detalle.id}" class="${(i % 2) == 0 ? 'odd' : 'even'}">
		    	<g:render template="filaDetalle" model="['detalle': detalle, 'grupo':grupo, 'impactoInstance':impactoInstance, 'usuarioLogueado':usuarioLogueado]"></g:render>
		    </tr>
	    </g:each>
	</table>
	
	
	</g:else>
	
		<g:set var="resultAreaSoportes" value="${impactoInstance.pedido.generarTablaAreaSoporteIncurridas()}"></g:set>
		<g:if test="${resultAreaSoportes && !resultAreaSoportes.isEmpty()}">
			<div style="position: relative; text-align: left; width: 100%;">
				<table class="TablaValidacionImpactoHeader" border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td colspan="2" width="30%">&Aacute;rea de Soporte</td>
						<td width="15%">Desde</td>
						<td width="15%">Hasta</td>
						<td width="17%" style="text-align: right;">Total horas</td>
						<td width="20%" style="text-align: right;">Monto (en Pesos)</td>
						<td width="20%" style="text-align: right;">Total horas inc.</td>
					</tr>
				</table>
				<g:each in="${resultAreaSoportes}" status="i" var="areaSoporte">
					<g:divsAreaSoporte areaSoportes="${areaSoporte}" id="areaSoporte_${i}" realesIncurridos="true"/>
				</g:each>
			</div>
		</g:if>

		<div id="gruposCargados" class= "pDos"> 
			<g:render template="gruposCargadosConsolidado" model="['impactoInstance': impactoInstance]"></g:render>
		</div>
		
</g:else>
</center>

<script>
animatedcollapse.ontoggle=function($, divobj, state){ myOnToggle(divobj, state); };
animatedcollapse.init();
</script>