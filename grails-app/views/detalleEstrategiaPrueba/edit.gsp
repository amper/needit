

<%@ page import="ar.com.telecom.pcs.entities.DetalleEstrategiaPrueba" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="layout" content="main" />
        <g:set var="entityName" value="${message(code: 'detalleEstrategiaPrueba.label', default: 'DetalleEstrategiaPrueba')}" />
        <title><g:message code="default.edit.label" args="[entityName]" /></title>
    </head>
    <body>
        <div class="nav">
            <span class="menuButton"><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></span>
            <span class="menuButton"><g:link class="list" action="list"><g:message code="default.list.label" args="[entityName]" /></g:link></span>
            <span class="menuButton"><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></span>
        </div>
        <div class="body">
            <h1><g:message code="default.edit.label" args="[entityName]" /></h1>
            <g:if test="${flash.message}">
            <div class="message">${flash.message}</div>
            </g:if>
            <g:hasErrors bean="${detalleEstrategiaPruebaInstance}">
            <div class="errors">
                <g:renderErrors bean="${detalleEstrategiaPruebaInstance}" as="list" />
            </div>
            </g:hasErrors>
            <g:form method="post" >
                <g:hiddenField name="id" value="${detalleEstrategiaPruebaInstance?.id}" />
                <g:hiddenField name="version" value="${detalleEstrategiaPruebaInstance?.version}" />
                <div class="dialog">
                    <table>
                        <tbody>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                  <label for="justificacionNoRealizacion"><g:message code="detalleEstrategiaPrueba.justificacionNoRealizacion.label" default="Justificacion No Realizacion" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: detalleEstrategiaPruebaInstance, field: 'justificacionNoRealizacion', 'errors')}">
                                    <g:select name="justificacionNoRealizacion.id" from="${ar.com.telecom.pcs.entities.Justificacion.list()}" optionKey="id" value="${detalleEstrategiaPruebaInstance?.justificacionNoRealizacion?.id}" noSelection="['null': '']" />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                  <label for="seRealiza"><g:message code="detalleEstrategiaPrueba.seRealiza.label" default="Se Realiza" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: detalleEstrategiaPruebaInstance, field: 'seRealiza', 'errors')}">
                                    <g:checkBox name="seRealiza" value="${detalleEstrategiaPruebaInstance?.seRealiza}" />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                  <label for="seConsolida"><g:message code="detalleEstrategiaPrueba.seConsolida.label" default="Se Consolida" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: detalleEstrategiaPruebaInstance, field: 'seConsolida', 'errors')}">
                                    <g:checkBox name="seConsolida" value="${detalleEstrategiaPruebaInstance?.seConsolida}" />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                  <label for="estrategiaPrueba"><g:message code="detalleEstrategiaPrueba.estrategiaPrueba.label" default="Estrategia Prueba" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: detalleEstrategiaPruebaInstance, field: 'estrategiaPrueba', 'errors')}">
                                    <g:select name="estrategiaPrueba.id" from="${ar.com.telecom.pcs.entities.EstrategiaPrueba.list()}" optionKey="id" value="${detalleEstrategiaPruebaInstance?.estrategiaPrueba?.id}"  />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                  <label for="tipoPrueba"><g:message code="detalleEstrategiaPrueba.tipoPrueba.label" default="Tipo Prueba" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: detalleEstrategiaPruebaInstance, field: 'tipoPrueba', 'errors')}">
                                    <g:select name="tipoPrueba.id" from="${ar.com.telecom.pcs.entities.TipoPrueba.list()}" optionKey="id" value="${detalleEstrategiaPruebaInstance?.tipoPrueba?.id}"  />
                                </td>
                            </tr>
                        
                        </tbody>
                    </table>
                </div>
                <div class="buttons">
                    <span class="button"><g:actionSubmit class="save" action="update" value="${message(code: 'default.button.update.label', default: 'Update')}" /></span>
                    <span class="button"><g:actionSubmit class="delete" action="delete" value="${message(code: 'default.button.delete.label', default: 'Delete')}" onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');" /></span>
                </div>
            </g:form>
        </div>
    </body>
</html>
