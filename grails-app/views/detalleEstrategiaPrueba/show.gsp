
<%@ page import="ar.com.telecom.pcs.entities.DetalleEstrategiaPrueba" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="layout" content="main" />
        <g:set var="entityName" value="${message(code: 'detalleEstrategiaPrueba.label', default: 'DetalleEstrategiaPrueba')}" />
        <title><g:message code="default.show.label" args="[entityName]" /></title>
    </head>
    <body>
        <div class="nav">
            <span class="menuButton"><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></span>
            <span class="menuButton"><g:link class="list" action="list"><g:message code="default.list.label" args="[entityName]" /></g:link></span>
            <span class="menuButton"><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></span>
        </div>
        <div class="body">
            <h1><g:message code="default.show.label" args="[entityName]" /></h1>
            <g:if test="${flash.message}">
            <div class="message">${flash.message}</div>
            </g:if>
            <div class="dialog">
                <table>
                    <tbody>
                    
                        <tr class="prop">
                            <td valign="top" class="name"><g:message code="detalleEstrategiaPrueba.id.label" default="Id" /></td>
                            
                            <td valign="top" class="value">${fieldValue(bean: detalleEstrategiaPruebaInstance, field: "id")}</td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name"><g:message code="detalleEstrategiaPrueba.justificacionNoRealizacion.label" default="Justificacion No Realizacion" /></td>
                            
                            <td valign="top" class="value"><g:link controller="justificacion" action="show" id="${detalleEstrategiaPruebaInstance?.justificacionNoRealizacion?.id}">${detalleEstrategiaPruebaInstance?.justificacionNoRealizacion?.encodeAsHTML()}</g:link></td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name"><g:message code="detalleEstrategiaPrueba.seRealiza.label" default="Se Realiza" /></td>
                            
                            <td valign="top" class="value"><g:formatBoolean boolean="${detalleEstrategiaPruebaInstance?.seRealiza}" /></td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name"><g:message code="detalleEstrategiaPrueba.seConsolida.label" default="Se Consolida" /></td>
                            
                            <td valign="top" class="value"><g:formatBoolean boolean="${detalleEstrategiaPruebaInstance?.seConsolida}" /></td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name"><g:message code="detalleEstrategiaPrueba.estrategiaPrueba.label" default="Estrategia Prueba" /></td>
                            
                            <td valign="top" class="value"><g:link controller="estrategiaPrueba" action="show" id="${detalleEstrategiaPruebaInstance?.estrategiaPrueba?.id}">${detalleEstrategiaPruebaInstance?.estrategiaPrueba?.encodeAsHTML()}</g:link></td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name"><g:message code="detalleEstrategiaPrueba.tipoPrueba.label" default="Tipo Prueba" /></td>
                            
                            <td valign="top" class="value"><g:link controller="tipoPrueba" action="show" id="${detalleEstrategiaPruebaInstance?.tipoPrueba?.id}">${detalleEstrategiaPruebaInstance?.tipoPrueba?.encodeAsHTML()}</g:link></td>
                            
                        </tr>
                    
                    </tbody>
                </table>
            </div>
            <div class="buttons">
                <g:form>
                    <g:hiddenField name="id" value="${detalleEstrategiaPruebaInstance?.id}" />
                    <span class="button"><g:actionSubmit class="edit" action="edit" value="${message(code: 'default.button.edit.label', default: 'Edit')}" /></span>
                    <span class="button"><g:actionSubmit class="delete" action="delete" value="${message(code: 'default.button.delete.label', default: 'Delete')}" onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');" /></span>
                </g:form>
            </div>
        </div>
    </body>
</html>
