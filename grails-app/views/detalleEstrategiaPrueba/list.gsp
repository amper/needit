
<%@ page import="ar.com.telecom.pcs.entities.DetalleEstrategiaPrueba" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="layout" content="main" />
        <g:set var="entityName" value="${message(code: 'detalleEstrategiaPrueba.label', default: 'DetalleEstrategiaPrueba')}" />
        <title><g:message code="default.list.label" args="[entityName]" /></title>
    </head>
    <body>
        <div class="nav">
            <span class="menuButton"><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></span>
            <span class="menuButton"><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></span>
        </div>
        <div class="body">
            <h1><g:message code="default.list.label" args="[entityName]" /></h1>
            <g:if test="${flash.message}">
            <div class="message">${flash.message}</div>
            </g:if>
            <div class="list">
                <table>
                    <thead>
                        <tr>
                        
                            <g:sortableColumn property="id" title="${message(code: 'detalleEstrategiaPrueba.id.label', default: 'Id')}" />
                        
                            <th><g:message code="detalleEstrategiaPrueba.justificacionNoRealizacion.label" default="Justificacion No Realizacion" /></th>
                        
                            <g:sortableColumn property="seRealiza" title="${message(code: 'detalleEstrategiaPrueba.seRealiza.label', default: 'Se Realiza')}" />
                        
                            <g:sortableColumn property="seConsolida" title="${message(code: 'detalleEstrategiaPrueba.seConsolida.label', default: 'Se Consolida')}" />
                        
                            <th><g:message code="detalleEstrategiaPrueba.estrategiaPrueba.label" default="Estrategia Prueba" /></th>
                        
                            <th><g:message code="detalleEstrategiaPrueba.tipoPrueba.label" default="Tipo Prueba" /></th>
                        
                        </tr>
                    </thead>
                    <tbody>
                    <g:each in="${detalleEstrategiaPruebaInstanceList}" status="i" var="detalleEstrategiaPruebaInstance">
                        <tr class="${(i % 2) == 0 ? 'odd' : 'even'}">
                        
                            <td><g:link action="show" id="${detalleEstrategiaPruebaInstance.id}">${fieldValue(bean: detalleEstrategiaPruebaInstance, field: "id")}</g:link></td>
                        
                            <td>${fieldValue(bean: detalleEstrategiaPruebaInstance, field: "justificacionNoRealizacion")}</td>
                        
                            <td><g:formatBoolean boolean="${detalleEstrategiaPruebaInstance.seRealiza}" /></td>
                        
                            <td><g:formatBoolean boolean="${detalleEstrategiaPruebaInstance.seConsolida}" /></td>
                        
                            <td>${fieldValue(bean: detalleEstrategiaPruebaInstance, field: "estrategiaPrueba")}</td>
                        
                            <td>${fieldValue(bean: detalleEstrategiaPruebaInstance, field: "tipoPrueba")}</td>
                        
                        </tr>
                    </g:each>
                    </tbody>
                </table>
            </div>
            <div class="paginateButtons">
                <g:paginate total="${detalleEstrategiaPruebaInstanceTotal}" />
            </div>
        </div>
    </body>
</html>
