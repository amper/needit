<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	
	<meta name="layout" content="mainApp" />
	<script language="javascript">
		jQuery(document).ready(function() {
			var t;
			
			jQuery("#pruebaValue").autocomplete({
				source: new Array(),
				select: onAutoCompleteSelected,
			    focus: function() { return false; }
			});
			 
			function serviceCall() {
				jQuery.getJSON('/needIt/registroPedido/busquedaAutocomplete?methodName=obtenerSistemas&word='+jQuery("#pruebaValue").val(), function(data) {
					var result = new Array();
				    jQuery.each(data, function(index, item) {
				       result.push({label: item.descripcion, value: item.id});
				    });
				    jQuery("#pruebaValue").autocomplete({
					    source: result,
					    select: onAutoCompleteSelected,
					    focus: function() { return false; }
					});
				    jQuery("#pruebaValue").autocomplete("search");
				});
			}
			
			function onAutoCompleteSelected(event, ui) {
				jQuery("#pruebaValue").val(ui.item.label);
				jQuery("#prueba.id").val(ui.item.value);
				jQuery("#prueba").val('[id:' + ui.item.value + ']');
				return false;
			}
			
			jQuery("#pruebaValue").keyup(function(event) { // tecla backspace habilitada
				var isAlphaNumeric = String.fromCharCode(event.keyCode).match(/[A-Za-z0-9]+/g);
				if(isAlphaNumeric || event.keyCode == 8) {
					clearTimeout(t);
					t = setTimeout( serviceCall, 500);
				}
			});

			jQuery("#example").multiselect();
			
		});
	</script>
</head>

<body>
	<h2>HTML unescape</h2>
	<p>${'<p>hola</p><br /><br /><p>chau</p>'.decodeHTML()}</p>
	<select id="example" name="example" multiple="multiple">
		<option value="1">Option 1</option>
		<option value="2">Option 2</option>
		<option value="3">Option 3</option>
		<option value="4">Option 4</option>
		<option value="5">Option 5</option>
	</select>
<%--	<g:busquedaRapida name="sistema" methodName="obtenerSistemas" />--%>
<%--	<g:busquedaRapida name="referenciaOrigen" methodName="obtenerReferenciasOrigen" value="" pedidoId=""/>--%>
	<input type="text" id="pruebaValue" name="pruebaValue" />
	<input type="hidden" name="prueba.id" value="" />
	<input type="hidden" id="prueba" name="prueba" value="" />
</body>
</html>