
<%@ page import="ar.com.telecom.pcs.entities.TareaSoporte" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="layout" content="main" />
        <g:set var="entityName" value="${message(code: 'tareaSoporte.label', default: 'TareaSoporte')}" />
        <title><g:message code="default.list.label" args="[entityName]" /></title>
    </head>
    <body>
        <div class="nav">
            <span class="menuButton"><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></span>
            <span class="menuButton"><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></span>
        </div>
        <div class="body">
            <h1><g:message code="default.list.label" args="[entityName]" /></h1>
            <g:if test="${flash.message}">
            <div class="message">${flash.message}</div>
            </g:if>
            <div class="list">
                <table>
                    <thead>
                        <tr>
                        
                            <g:sortableColumn property="id" title="${message(code: 'tareaSoporte.id.label', default: 'Id')}" />
                        
                            <g:sortableColumn property="descripcion" title="${message(code: 'tareaSoporte.descripcion.label', default: 'Descripcion')}" />
                        
                            <th><g:message code="tareaSoporte.areaSoporte.label" default="Area Soporte" /></th>
                        
                            <th><g:message code="tareaSoporte.tipoActividad.label" default="Tipo Actividad" /></th>
                        
                        </tr>
                    </thead>
                    <tbody>
                    <g:each in="${tareaSoporteInstanceList}" status="i" var="tareaSoporteInstance">
                        <tr class="${(i % 2) == 0 ? 'odd' : 'even'}">
                        
                            <td><g:link action="show" id="${tareaSoporteInstance.id}">${fieldValue(bean: tareaSoporteInstance, field: "id")}</g:link></td>
                        
                            <td>${fieldValue(bean: tareaSoporteInstance, field: "descripcion")}</td>
                        
                            <td>${fieldValue(bean: tareaSoporteInstance, field: "areaSoporte")}</td>
                        
                            <td>${fieldValue(bean: tareaSoporteInstance, field: "tipoActividad")}</td>
                        
                        </tr>
                    </g:each>
                    </tbody>
                </table>
            </div>
            <div class="paginateButtons">
                <g:paginate total="${tareaSoporteInstanceTotal}" />
            </div>
        </div>
    </body>
</html>
