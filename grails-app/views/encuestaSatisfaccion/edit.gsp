

<%@ page import="ar.com.telecom.pcs.entities.EncuestaSatisfaccion" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="layout" content="main" />
        <g:set var="entityName" value="${message(code: 'encuestaSatisfaccion.label', default: 'EncuestaSatisfaccion')}" />
        <title><g:message code="default.edit.label" args="[entityName]" /></title>
    </head>
    <body>
        <div class="nav">
            <span class="menuButton"><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></span>
            <span class="menuButton"><g:link class="list" action="list"><g:message code="default.list.label" args="[entityName]" /></g:link></span>
            <span class="menuButton"><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></span>
        </div>
        <div class="body">
            <h1><g:message code="default.edit.label" args="[entityName]" /></h1>
            <g:if test="${flash.message}">
            <div class="message">${flash.message}</div>
            </g:if>
            <g:hasErrors bean="${encuestaSatisfaccionInstance}">
            <div class="errors">
                <g:renderErrors bean="${encuestaSatisfaccionInstance}" as="list" />
            </div>
            </g:hasErrors>
            <g:form method="post" >
                <g:hiddenField name="id" value="${encuestaSatisfaccionInstance?.id}" />
                <g:hiddenField name="version" value="${encuestaSatisfaccionInstance?.version}" />
                <div class="dialog">
                    <table>
                        <tbody>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                  <label for="calidadSolucion"><g:message code="encuestaSatisfaccion.calidadSolucion.label" default="Calidad Solucion" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: encuestaSatisfaccionInstance, field: 'calidadSolucion', 'errors')}">
                                    <g:textField name="calidadSolucion" value="${fieldValue(bean: encuestaSatisfaccionInstance, field: 'calidadSolucion')}" />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                  <label for="tiempoResolucion"><g:message code="encuestaSatisfaccion.tiempoResolucion.label" default="Tiempo Resolucion" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: encuestaSatisfaccionInstance, field: 'tiempoResolucion', 'errors')}">
                                    <g:textField name="tiempoResolucion" value="${fieldValue(bean: encuestaSatisfaccionInstance, field: 'tiempoResolucion')}" />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                  <label for="adecuacionNecesidades"><g:message code="encuestaSatisfaccion.adecuacionNecesidades.label" default="Adecuacion Necesidades" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: encuestaSatisfaccionInstance, field: 'adecuacionNecesidades', 'errors')}">
                                    <g:textField name="adecuacionNecesidades" value="${fieldValue(bean: encuestaSatisfaccionInstance, field: 'adecuacionNecesidades')}" />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                  <label for="valoracionGeneral"><g:message code="encuestaSatisfaccion.valoracionGeneral.label" default="Valoracion General" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: encuestaSatisfaccionInstance, field: 'valoracionGeneral', 'errors')}">
                                    <g:textField name="valoracionGeneral" value="${fieldValue(bean: encuestaSatisfaccionInstance, field: 'valoracionGeneral')}" />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                  <label for="gradoSatisfaccionCCRSWF"><g:message code="encuestaSatisfaccion.gradoSatisfaccionCCRSWF.label" default="Grado Satisfaccion CCRSWF" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: encuestaSatisfaccionInstance, field: 'gradoSatisfaccionCCRSWF', 'errors')}">
                                    <g:textField name="gradoSatisfaccionCCRSWF" value="${fieldValue(bean: encuestaSatisfaccionInstance, field: 'gradoSatisfaccionCCRSWF')}" />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                  <label for="agilidadNeedIT"><g:message code="encuestaSatisfaccion.agilidadNeedIT.label" default="Agilidad Need IT" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: encuestaSatisfaccionInstance, field: 'agilidadNeedIT', 'errors')}">
                                    <g:textField name="agilidadNeedIT" value="${fieldValue(bean: encuestaSatisfaccionInstance, field: 'agilidadNeedIT')}" />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                  <label for="gradoSatisfaccionProcesoDesarrollo"><g:message code="encuestaSatisfaccion.gradoSatisfaccionProcesoDesarrollo.label" default="Grado Satisfaccion Proceso Desarrollo" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: encuestaSatisfaccionInstance, field: 'gradoSatisfaccionProcesoDesarrollo', 'errors')}">
                                    <g:textField name="gradoSatisfaccionProcesoDesarrollo" value="${fieldValue(bean: encuestaSatisfaccionInstance, field: 'gradoSatisfaccionProcesoDesarrollo')}" />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                  <label for="comentariosSolucion"><g:message code="encuestaSatisfaccion.comentariosSolucion.label" default="Comentarios Solucion" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: encuestaSatisfaccionInstance, field: 'comentariosSolucion', 'errors')}">
                                    <g:textField name="comentariosSolucion" value="${encuestaSatisfaccionInstance?.comentariosSolucion}" />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                  <label for="comentariosGestion"><g:message code="encuestaSatisfaccion.comentariosGestion.label" default="Comentarios Gestion" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: encuestaSatisfaccionInstance, field: 'comentariosGestion', 'errors')}">
                                    <g:textField name="comentariosGestion" value="${encuestaSatisfaccionInstance?.comentariosGestion}" />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                  <label for="comentariosHerramienta"><g:message code="encuestaSatisfaccion.comentariosHerramienta.label" default="Comentarios Herramienta" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: encuestaSatisfaccionInstance, field: 'comentariosHerramienta', 'errors')}">
                                    <g:textField name="comentariosHerramienta" value="${encuestaSatisfaccionInstance?.comentariosHerramienta}" />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                  <label for="comentariosProceso"><g:message code="encuestaSatisfaccion.comentariosProceso.label" default="Comentarios Proceso" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: encuestaSatisfaccionInstance, field: 'comentariosProceso', 'errors')}">
                                    <g:textField name="comentariosProceso" value="${encuestaSatisfaccionInstance?.comentariosProceso}" />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                  <label for="pedido"><g:message code="encuestaSatisfaccion.pedido.label" default="Pedido" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: encuestaSatisfaccionInstance, field: 'pedido', 'errors')}">
                                    <g:select name="pedido.id" from="${ar.com.telecom.pcs.entities.Pedido.list()}" optionKey="id" value="${encuestaSatisfaccionInstance?.pedido?.id}"  />
                                </td>
                            </tr>
                        
                        </tbody>
                    </table>
                </div>
                <div class="buttons">
                    <span class="button"><g:actionSubmit class="save" action="update" value="${message(code: 'default.button.update.label', default: 'Update')}" /></span>
                    <span class="button"><g:actionSubmit class="delete" action="delete" value="${message(code: 'default.button.delete.label', default: 'Delete')}" onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');" /></span>
                </div>
            </g:form>
        </div>
    </body>
</html>
