
<%@ page import="ar.com.telecom.pcs.entities.EncuestaSatisfaccion" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="layout" content="main" />
        <g:set var="entityName" value="${message(code: 'encuestaSatisfaccion.label', default: 'EncuestaSatisfaccion')}" />
        <title><g:message code="default.show.label" args="[entityName]" /></title>
    </head>
    <body>
        <div class="nav">
            <span class="menuButton"><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></span>
            <span class="menuButton"><g:link class="list" action="list"><g:message code="default.list.label" args="[entityName]" /></g:link></span>
            <span class="menuButton"><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></span>
        </div>
        <div class="body">
            <h1><g:message code="default.show.label" args="[entityName]" /></h1>
            <g:if test="${flash.message}">
            <div class="message">${flash.message}</div>
            </g:if>
            <div class="dialog">
                <table>
                    <tbody>
                    
                        <tr class="prop">
                            <td valign="top" class="name"><g:message code="encuestaSatisfaccion.id.label" default="Id" /></td>
                            
                            <td valign="top" class="value">${fieldValue(bean: encuestaSatisfaccionInstance, field: "id")}</td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name"><g:message code="encuestaSatisfaccion.calidadSolucion.label" default="Calidad Solucion" /></td>
                            
                            <td valign="top" class="value">${fieldValue(bean: encuestaSatisfaccionInstance, field: "calidadSolucion")}</td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name"><g:message code="encuestaSatisfaccion.tiempoResolucion.label" default="Tiempo Resolucion" /></td>
                            
                            <td valign="top" class="value">${fieldValue(bean: encuestaSatisfaccionInstance, field: "tiempoResolucion")}</td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name"><g:message code="encuestaSatisfaccion.adecuacionNecesidades.label" default="Adecuacion Necesidades" /></td>
                            
                            <td valign="top" class="value">${fieldValue(bean: encuestaSatisfaccionInstance, field: "adecuacionNecesidades")}</td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name"><g:message code="encuestaSatisfaccion.valoracionGeneral.label" default="Valoracion General" /></td>
                            
                            <td valign="top" class="value">${fieldValue(bean: encuestaSatisfaccionInstance, field: "valoracionGeneral")}</td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name"><g:message code="encuestaSatisfaccion.gradoSatisfaccionCCRSWF.label" default="Grado Satisfaccion CCRSWF" /></td>
                            
                            <td valign="top" class="value">${fieldValue(bean: encuestaSatisfaccionInstance, field: "gradoSatisfaccionCCRSWF")}</td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name"><g:message code="encuestaSatisfaccion.agilidadNeedIT.label" default="Agilidad Need IT" /></td>
                            
                            <td valign="top" class="value">${fieldValue(bean: encuestaSatisfaccionInstance, field: "agilidadNeedIT")}</td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name"><g:message code="encuestaSatisfaccion.gradoSatisfaccionProcesoDesarrollo.label" default="Grado Satisfaccion Proceso Desarrollo" /></td>
                            
                            <td valign="top" class="value">${fieldValue(bean: encuestaSatisfaccionInstance, field: "gradoSatisfaccionProcesoDesarrollo")}</td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name"><g:message code="encuestaSatisfaccion.comentariosSolucion.label" default="Comentarios Solucion" /></td>
                            
                            <td valign="top" class="value">${fieldValue(bean: encuestaSatisfaccionInstance, field: "comentariosSolucion")}</td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name"><g:message code="encuestaSatisfaccion.comentariosGestion.label" default="Comentarios Gestion" /></td>
                            
                            <td valign="top" class="value">${fieldValue(bean: encuestaSatisfaccionInstance, field: "comentariosGestion")}</td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name"><g:message code="encuestaSatisfaccion.comentariosHerramienta.label" default="Comentarios Herramienta" /></td>
                            
                            <td valign="top" class="value">${fieldValue(bean: encuestaSatisfaccionInstance, field: "comentariosHerramienta")}</td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name"><g:message code="encuestaSatisfaccion.comentariosProceso.label" default="Comentarios Proceso" /></td>
                            
                            <td valign="top" class="value">${fieldValue(bean: encuestaSatisfaccionInstance, field: "comentariosProceso")}</td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name"><g:message code="encuestaSatisfaccion.pedido.label" default="Pedido" /></td>
                            
                            <td valign="top" class="value"><g:link controller="pedido" action="show" id="${encuestaSatisfaccionInstance?.pedido?.id}">${encuestaSatisfaccionInstance?.pedido?.encodeAsHTML()}</g:link></td>
                            
                        </tr>
                    
                    </tbody>
                </table>
            </div>
            <div class="buttons">
                <g:form>
                    <g:hiddenField name="id" value="${encuestaSatisfaccionInstance?.id}" />
                    <span class="button"><g:actionSubmit class="edit" action="edit" value="${message(code: 'default.button.edit.label', default: 'Edit')}" /></span>
                    <span class="button"><g:actionSubmit class="delete" action="delete" value="${message(code: 'default.button.delete.label', default: 'Delete')}" onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');" /></span>
                </g:form>
            </div>
        </div>
    </body>
</html>
