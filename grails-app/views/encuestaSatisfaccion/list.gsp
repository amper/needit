
<%@ page import="ar.com.telecom.pcs.entities.EncuestaSatisfaccion" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="layout" content="main" />
        <g:set var="entityName" value="${message(code: 'encuestaSatisfaccion.label', default: 'EncuestaSatisfaccion')}" />
        <title><g:message code="default.list.label" args="[entityName]" /></title>
    </head>
    <body>
        <div class="nav">
            <span class="menuButton"><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></span>
            <span class="menuButton"><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></span>
        </div>
        <div class="body">
            <h1><g:message code="default.list.label" args="[entityName]" /></h1>
            <g:if test="${flash.message}">
            <div class="message">${flash.message}</div>
            </g:if>
            <div class="list">
                <table>
                    <thead>
                        <tr>
                        
                            <g:sortableColumn property="id" title="${message(code: 'encuestaSatisfaccion.id.label', default: 'Id')}" />
                        
                            <g:sortableColumn property="calidadSolucion" title="${message(code: 'encuestaSatisfaccion.calidadSolucion.label', default: 'Calidad Solucion')}" />
                        
                            <g:sortableColumn property="tiempoResolucion" title="${message(code: 'encuestaSatisfaccion.tiempoResolucion.label', default: 'Tiempo Resolucion')}" />
                        
                            <g:sortableColumn property="adecuacionNecesidades" title="${message(code: 'encuestaSatisfaccion.adecuacionNecesidades.label', default: 'Adecuacion Necesidades')}" />
                        
                            <g:sortableColumn property="valoracionGeneral" title="${message(code: 'encuestaSatisfaccion.valoracionGeneral.label', default: 'Valoracion General')}" />
                        
                            <g:sortableColumn property="gradoSatisfaccionCCRSWF" title="${message(code: 'encuestaSatisfaccion.gradoSatisfaccionCCRSWF.label', default: 'Grado Satisfaccion CCRSWF')}" />
                        
                        </tr>
                    </thead>
                    <tbody>
                    <g:each in="${encuestaSatisfaccionInstanceList}" status="i" var="encuestaSatisfaccionInstance">
                        <tr class="${(i % 2) == 0 ? 'odd' : 'even'}">
                        
                            <td><g:link action="show" id="${encuestaSatisfaccionInstance.id}">${fieldValue(bean: encuestaSatisfaccionInstance, field: "id")}</g:link></td>
                        
                            <td>${fieldValue(bean: encuestaSatisfaccionInstance, field: "calidadSolucion")}</td>
                        
                            <td>${fieldValue(bean: encuestaSatisfaccionInstance, field: "tiempoResolucion")}</td>
                        
                            <td>${fieldValue(bean: encuestaSatisfaccionInstance, field: "adecuacionNecesidades")}</td>
                        
                            <td>${fieldValue(bean: encuestaSatisfaccionInstance, field: "valoracionGeneral")}</td>
                        
                            <td>${fieldValue(bean: encuestaSatisfaccionInstance, field: "gradoSatisfaccionCCRSWF")}</td>
                        
                        </tr>
                    </g:each>
                    </tbody>
                </table>
            </div>
            <div class="paginateButtons">
                <g:paginate total="${encuestaSatisfaccionInstanceTotal}" />
            </div>
        </div>
    </body>
</html>
