

<%@ page import="ar.com.telecom.pcs.entities.UmbralAprobacionEconomicaDireccion" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="layout" content="main" />
        <g:set var="entityName" value="${message(code: 'umbralAprobacionEconomicaDireccion.label', default: 'UmbralAprobacionEconomicaDireccion')}" />
        <title><g:message code="default.create.label" args="[entityName]" /></title>
    </head>
    <body>
        <div class="nav">
            <span class="menuButton"><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></span>
            <span class="menuButton"><g:link class="list" action="list"><g:message code="default.list.label" args="[entityName]" /></g:link></span>
        </div>
        <div class="body">
            <h1><g:message code="default.create.label" args="[entityName]" /></h1>
            <g:if test="${flash.message}">
            <div class="message">${flash.message}</div>
            </g:if>
            <g:hasErrors bean="${umbralAprobacionEconomicaDireccionInstance}">
            <div class="errors">
                <g:renderErrors bean="${umbralAprobacionEconomicaDireccionInstance}" as="list" />
            </div>
            </g:hasErrors>
            <g:form action="save" >
                <div class="dialog">
                    <table>
                        <tbody>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="direccion"><g:message code="umbralAprobacionEconomicaDireccion.direccion.label" default="Direccion" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: umbralAprobacionEconomicaDireccionInstance, field: 'direccion', 'errors')}">
                                    <g:textField name="direccion" maxlength="50" value="${umbralAprobacionEconomicaDireccionInstance?.direccion}" />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="rangoDesde"><g:message code="umbralAprobacionEconomicaDireccion.rangoDesde.label" default="Rango Desde" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: umbralAprobacionEconomicaDireccionInstance, field: 'rangoDesde', 'errors')}">
                                    <g:textField name="rangoDesde" value="${fieldValue(bean: umbralAprobacionEconomicaDireccionInstance, field: 'rangoDesde')}" />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="rangoHasta"><g:message code="umbralAprobacionEconomicaDireccion.rangoHasta.label" default="Rango Hasta" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: umbralAprobacionEconomicaDireccionInstance, field: 'rangoHasta', 'errors')}">
                                    <g:textField name="rangoHasta" value="${fieldValue(bean: umbralAprobacionEconomicaDireccionInstance, field: 'rangoHasta')}" />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="tipoGestion"><g:message code="umbralAprobacionEconomicaDireccion.tipoGestion.label" default="Tipo Gestion" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: umbralAprobacionEconomicaDireccionInstance, field: 'tipoGestion', 'errors')}">
                                    <g:select name="tipoGestion.id" from="${ar.com.telecom.pcs.entities.TipoGestion.list()}" optionKey="id" value="${umbralAprobacionEconomicaDireccionInstance?.tipoGestion?.id}"  />
                                </td>
                            </tr>
                        
                        </tbody>
                    </table>
                </div>
                <div class="buttons">
                    <span class="button"><g:submitButton name="create" class="save" value="${message(code: 'default.button.create.label', default: 'Create')}" /></span>
                </div>
            </g:form>
        </div>
    </body>
</html>
