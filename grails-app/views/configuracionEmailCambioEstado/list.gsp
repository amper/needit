
<%@ page import="ar.com.telecom.pcs.entities.ConfiguracionEmailCambioEstado" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="layout" content="main" />
        <g:set var="entityName" value="${message(code: 'configuracionEmailCambioEstado.label', default: 'ConfiguracionEmailCambioEstado')}" />
        <title><g:message code="default.list.label" args="[entityName]" /></title>
    </head>
    <body>
        <div class="nav">
            <span class="menuButton"><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></span>
            <span class="menuButton"><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></span>
        </div>
        <div class="body">
            <h1><g:message code="default.list.label" args="[entityName]" /></h1>
            <g:if test="${flash.message}">
            <div class="message">${flash.message}</div>
            </g:if>
            <div class="list">
                <table>
                    <thead>
                        <tr>
                        
                            <g:sortableColumn property="id" title="${message(code: 'configuracionEmailCambioEstado.id.label', default: 'Id')}" />
                        
                            <g:sortableColumn property="asunto" title="${message(code: 'configuracionEmailCambioEstado.asunto.label', default: 'Asunto')}" />
                        
                            <th><g:message code="configuracionEmailCambioEstado.faseActual.label" default="Fase Actual" /></th>
                        
                            <th><g:message code="configuracionEmailCambioEstado.faseSiguiente.label" default="Fase Siguiente" /></th>
                        
                            <g:sortableColumn property="texto" title="${message(code: 'configuracionEmailCambioEstado.texto.label', default: 'Texto')}" />
                        
                        </tr>
                    </thead>
                    <tbody>
                    <g:each in="${configuracionEmailCambioEstadoInstanceList}" status="i" var="configuracionEmailCambioEstadoInstance">
                        <tr class="${(i % 2) == 0 ? 'odd' : 'even'}">
                        
                            <td><g:link action="show" id="${configuracionEmailCambioEstadoInstance.id}">${fieldValue(bean: configuracionEmailCambioEstadoInstance, field: "id")}</g:link></td>
                        
                            <td>${fieldValue(bean: configuracionEmailCambioEstadoInstance, field: "asunto")}</td>
                        
                            <td>${fieldValue(bean: configuracionEmailCambioEstadoInstance, field: "faseActual")}</td>
                        
                            <td>${fieldValue(bean: configuracionEmailCambioEstadoInstance, field: "faseSiguiente")}</td>
                        
                            <td>${fieldValue(bean: configuracionEmailCambioEstadoInstance, field: "texto")}</td>
                        
                        </tr>
                    </g:each>
                    </tbody>
                </table>
            </div>
            <div class="paginateButtons">
                <g:paginate total="${configuracionEmailCambioEstadoInstanceTotal}" />
            </div>
        </div>
    </body>
</html>
