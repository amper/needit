
<%@ page import="ar.com.telecom.pcs.entities.ConfiguracionEmailCambioEstado" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="layout" content="main" />
        <g:set var="entityName" value="${message(code: 'configuracionEmailCambioEstado.label', default: 'ConfiguracionEmailCambioEstado')}" />
        <title><g:message code="default.show.label" args="[entityName]" /></title>
    </head>
    <body>
        <div class="nav">
            <span class="menuButton"><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></span>
            <span class="menuButton"><g:link class="list" action="list"><g:message code="default.list.label" args="[entityName]" /></g:link></span>
            <span class="menuButton"><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></span>
        </div>
        <div class="body">
            <h1><g:message code="default.show.label" args="[entityName]" /></h1>
            <g:if test="${flash.message}">
            <div class="message">${flash.message}</div>
            </g:if>
            <div class="dialog">
                <table>
                    <tbody>
                    
                        <tr class="prop">
                            <td valign="top" class="name"><g:message code="configuracionEmailCambioEstado.id.label" default="Id" /></td>
                            
                            <td valign="top" class="value">${fieldValue(bean: configuracionEmailCambioEstadoInstance, field: "id")}</td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name"><g:message code="configuracionEmailCambioEstado.asunto.label" default="Asunto" /></td>
                            
                            <td valign="top" class="value">${fieldValue(bean: configuracionEmailCambioEstadoInstance, field: "asunto")}</td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name"><g:message code="configuracionEmailCambioEstado.faseActual.label" default="Fase Actual" /></td>
                            
                            <td valign="top" class="value"><g:link controller="fase" action="show" id="${configuracionEmailCambioEstadoInstance?.faseActual?.id}">${configuracionEmailCambioEstadoInstance?.faseActual?.encodeAsHTML()}</g:link></td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name"><g:message code="configuracionEmailCambioEstado.faseSiguiente.label" default="Fase Siguiente" /></td>
                            
                            <td valign="top" class="value"><g:link controller="fase" action="show" id="${configuracionEmailCambioEstadoInstance?.faseSiguiente?.id}">${configuracionEmailCambioEstadoInstance?.faseSiguiente?.encodeAsHTML()}</g:link></td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name"><g:message code="configuracionEmailCambioEstado.roles.label" default="Roles" /></td>
                            
                            <td valign="top" style="text-align: left;" class="value">
                                <ul>
                                <g:each in="${configuracionEmailCambioEstadoInstance.roles}" var="r">
                                    <li><g:link controller="rolAplicacion" action="show" id="${r.id}">${r?.encodeAsHTML()}</g:link></li>
                                </g:each>
                                </ul>
                            </td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name"><g:message code="configuracionEmailCambioEstado.texto.label" default="Texto" /></td>
                            
                            <td valign="top" class="value">${fieldValue(bean: configuracionEmailCambioEstadoInstance, field: "texto")}</td>
                            
                        </tr>
                    
                    </tbody>
                </table>
            </div>
            <div class="buttons">
                <g:form>
                    <g:hiddenField name="id" value="${configuracionEmailCambioEstadoInstance?.id}" />
                    <span class="button"><g:actionSubmit class="edit" action="edit" value="${message(code: 'default.button.edit.label', default: 'Edit')}" /></span>
                    <span class="button"><g:actionSubmit class="delete" action="delete" value="${message(code: 'default.button.delete.label', default: 'Delete')}" onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');" /></span>
                </g:form>
            </div>
        </div>
    </body>
</html>
