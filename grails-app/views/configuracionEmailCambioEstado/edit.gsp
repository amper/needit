

<%@ page import="ar.com.telecom.pcs.entities.ConfiguracionEmailCambioEstado" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="layout" content="main" />
        <g:javascript library="tiny_mce/tiny_mce" />
        <g:set var="entityName" value="${message(code: 'configuracionEmailCambioEstado.label', default: 'ConfiguracionEmailCambioEstado')}" />
        <title><g:message code="default.edit.label" args="[entityName]" /></title>
        <script type="text/javascript">
tinyMCE.init({
    mode : "exact",
    elements : "texto",
    theme : "advanced",
	skin : "o2k7",
    plugins : "autolink,lists,pagebreak,style,layer,table,save,advhr,advimage,advlink,emotions,iespell,inlinepopups,insertdatetime,preview,media,searchreplace,print,contextmenu,paste,directionality,fullscreen,noneditable,visualchars,nonbreaking,xhtmlxtras,template,wordcount,advlist", 
            
    // Theme options - button# indicated the row# only
    theme_advanced_buttons1 : "newdocument,print,|,bold,italic,underline,|,fontsizeselect,|,cut,copy,paste,|,bullist,numlist,|,undo,redo,|,forecolor,backcolor,|,image,|,fullscreen,template",
    theme_advanced_buttons2 : "",
    theme_advanced_buttons3 : "",      
    theme_advanced_toolbar_location : "top",
    theme_advanced_toolbar_align : "left",
    theme_advanced_statusbar_location : "bottom",
    theme_advanced_resizing : true,
    paste_text_sticky : true,
    setup : function(ed) {
        ed.onInit.add(function(ed) {
          ed.pasteAsPlainText = true;
        });
    },
	template_external_list_url : "lists/template_list.js",

		//template_external_list_url : "lists/template_list.js",
		external_link_list_url : "lists/link_list.js",
		external_image_list_url : "lists/image_list.js",
		media_external_list_url : "lists/media_list.js"
});

</script>         
    </head>
    <body>
        <div class="nav">
            <span class="menuButton"><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></span>
            <span class="menuButton"><g:link class="list" action="list"><g:message code="default.list.label" args="[entityName]" /></g:link></span>
            <span class="menuButton"><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></span>
        </div>
        <div class="body">
            <h1><g:message code="default.edit.label" args="[entityName]" /></h1>
            <g:if test="${flash.message}">
            <div class="message">${flash.message}</div>
            </g:if>
            <g:hasErrors bean="${configuracionEmailCambioEstadoInstance}">
            <div class="errors">
                <g:renderErrors bean="${configuracionEmailCambioEstadoInstance}" as="list" />
            </div>
            </g:hasErrors>
            <g:form method="post" >
                <g:hiddenField name="id" value="${configuracionEmailCambioEstadoInstance?.id}" />
                <g:hiddenField name="version" value="${configuracionEmailCambioEstadoInstance?.version}" />
                <div class="dialog">
                    <table>
                        <tbody>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                  <label for="asunto"><g:message code="configuracionEmailCambioEstado.asunto.label" default="Asunto" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: configuracionEmailCambioEstadoInstance, field: 'asunto', 'errors')}">
                                    <g:textArea name="asunto" cols="40" rows="5" value="${configuracionEmailCambioEstadoInstance?.asunto}" />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                  <label for="faseActual"><g:message code="configuracionEmailCambioEstado.faseActual.label" default="Fase Actual" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: configuracionEmailCambioEstadoInstance, field: 'faseActual', 'errors')}">
                                    <g:select name="faseActual.id" from="${ar.com.telecom.pcs.entities.Fase.list()}" optionKey="id" value="${configuracionEmailCambioEstadoInstance?.faseActual?.id}"  />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                  <label for="faseSiguiente"><g:message code="configuracionEmailCambioEstado.faseSiguiente.label" default="Fase Siguiente" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: configuracionEmailCambioEstadoInstance, field: 'faseSiguiente', 'errors')}">
                                    <g:select name="faseSiguiente.id" from="${ar.com.telecom.pcs.entities.Fase.list()}" optionKey="id" value="${configuracionEmailCambioEstadoInstance?.faseSiguiente?.id}"  />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                  <label for="roles"><g:message code="configuracionEmailCambioEstado.roles.label" default="Roles" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: configuracionEmailCambioEstadoInstance, field: 'roles', 'errors')}">
                                    <g:select name="roles" from="${ar.com.telecom.pcs.entities.RolAplicacion.list()}" multiple="yes" optionKey="id" size="5" value="${configuracionEmailCambioEstadoInstance?.roles*.id}" />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                  <label for="texto"><g:message code="configuracionEmailCambioEstado.texto.label" default="Texto" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: configuracionEmailCambioEstadoInstance, field: 'texto', 'errors')}">
                                    <g:textField name="texto" value="${configuracionEmailCambioEstadoInstance?.texto}" />
                                </td>
                            </tr>
                        
                        </tbody>
                    </table>
                </div>
                <div class="buttons">
                    <span class="button"><g:actionSubmit class="save" action="update" value="${message(code: 'default.button.update.label', default: 'Update')}" /></span>
                    <span class="button"><g:actionSubmit class="delete" action="delete" value="${message(code: 'default.button.delete.label', default: 'Delete')}" onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');" /></span>
                </div>
            </g:form>
        </div>
    </body>
</html>
