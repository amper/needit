
<%@ page import="ar.com.telecom.pcs.entities.PlanificacionEsfuerzo" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="layout" content="main" />
        <g:set var="entityName" value="${message(code: 'planificacionEsfuerzo.label', default: 'PlanificacionEsfuerzo')}" />
        <title><g:message code="default.list.label" args="[entityName]" /></title>
    </head>
    <body>
        <div class="nav">
            <span class="menuButton"><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></span>
            <span class="menuButton"><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></span>
        </div>
        <div class="body">
            <h1><g:message code="default.list.label" args="[entityName]" /></h1>
            <g:if test="${flash.message}">
            <div class="message">${flash.message}</div>
            </g:if>
            <div class="list">
                <table>
                    <thead>
                        <tr>
                        
                            <g:sortableColumn property="id" title="${message(code: 'planificacionEsfuerzo.id.label', default: 'Id')}" />
                        
                            <g:sortableColumn property="tipoPlanificacion" title="${message(code: 'planificacionEsfuerzo.tipoPlanificacion.label', default: 'Tipo Planificacion')}" />
                        
                            <th><g:message code="planificacionEsfuerzo.numeroRelease.label" default="Numero Release" /></th>
                        
                            <g:sortableColumn property="fechaRelease" title="${message(code: 'planificacionEsfuerzo.fechaRelease.label', default: 'Fecha Release')}" />
                        
                            <g:sortableColumn property="numeroSOLMAN" title="${message(code: 'planificacionEsfuerzo.numeroSOLMAN.label', default: 'Numero SOLMAN')}" />
                        
                            <g:sortableColumn property="comentarioSistema" title="${message(code: 'planificacionEsfuerzo.comentarioSistema.label', default: 'Comentario Sistema')}" />
                        
                        </tr>
                    </thead>
                    <tbody>
                    <g:each in="${planificacionEsfuerzoInstanceList}" status="i" var="planificacionEsfuerzoInstance">
                        <tr class="${(i % 2) == 0 ? 'odd' : 'even'}">
                        
                            <td><g:link action="show" id="${planificacionEsfuerzoInstance.id}">${fieldValue(bean: planificacionEsfuerzoInstance, field: "id")}</g:link></td>
                        
                            <td>${fieldValue(bean: planificacionEsfuerzoInstance, field: "tipoPlanificacion")}</td>
                        
                            <td>${fieldValue(bean: planificacionEsfuerzoInstance, field: "numeroRelease")}</td>
                        
                            <td><g:formatDate date="${planificacionEsfuerzoInstance.fechaRelease}" /></td>
                        
                            <td>${fieldValue(bean: planificacionEsfuerzoInstance, field: "numeroSOLMAN")}</td>
                        
                            <td>${fieldValue(bean: planificacionEsfuerzoInstance, field: "comentarioSistema")}</td>
                        
                        </tr>
                    </g:each>
                    </tbody>
                </table>
            </div>
            <div class="paginateButtons">
                <g:paginate total="${planificacionEsfuerzoInstanceTotal}" />
            </div>
        </div>
    </body>
</html>
