

<%@ page import="ar.com.telecom.pcs.entities.PlanificacionEsfuerzo" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="layout" content="main" />
        <g:set var="entityName" value="${message(code: 'planificacionEsfuerzo.label', default: 'PlanificacionEsfuerzo')}" />
        <title><g:message code="default.edit.label" args="[entityName]" /></title>
    </head>
    <body>
        <div class="nav">
            <span class="menuButton"><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></span>
            <span class="menuButton"><g:link class="list" action="list"><g:message code="default.list.label" args="[entityName]" /></g:link></span>
            <span class="menuButton"><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></span>
        </div>
        <div class="body">
            <h1><g:message code="default.edit.label" args="[entityName]" /></h1>
            <g:if test="${flash.message}">
            <div class="message">${flash.message}</div>
            </g:if>
            <g:hasErrors bean="${planificacionEsfuerzoInstance}">
            <div class="errors">
                <g:renderErrors bean="${planificacionEsfuerzoInstance}" as="list" />
            </div>
            </g:hasErrors>
            <g:form method="post" >
                <g:hiddenField name="id" value="${planificacionEsfuerzoInstance?.id}" />
                <g:hiddenField name="version" value="${planificacionEsfuerzoInstance?.version}" />
                <div class="dialog">
                    <table>
                        <tbody>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                  <label for="tipoPlanificacion"><g:message code="planificacionEsfuerzo.tipoPlanificacion.label" default="Tipo Planificacion" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: planificacionEsfuerzoInstance, field: 'tipoPlanificacion', 'errors')}">
                                    <g:textField name="tipoPlanificacion" maxlength="100" value="${planificacionEsfuerzoInstance?.tipoPlanificacion}" />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                  <label for="numeroRelease"><g:message code="planificacionEsfuerzo.numeroRelease.label" default="Numero Release" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: planificacionEsfuerzoInstance, field: 'numeroRelease', 'errors')}">
                                    <g:select name="numeroRelease.id" from="${ar.com.telecom.pcs.entities.Release.list()}" optionKey="id" value="${planificacionEsfuerzoInstance?.numeroRelease?.id}" noSelection="['null': '']" />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                  <label for="fechaRelease"><g:message code="planificacionEsfuerzo.fechaRelease.label" default="Fecha Release" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: planificacionEsfuerzoInstance, field: 'fechaRelease', 'errors')}">
                                    <g:datePicker name="fechaRelease" precision="day" value="${planificacionEsfuerzoInstance?.fechaRelease}" default="none" noSelection="['': '']" />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                  <label for="numeroSOLMAN"><g:message code="planificacionEsfuerzo.numeroSOLMAN.label" default="Numero SOLMAN" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: planificacionEsfuerzoInstance, field: 'numeroSOLMAN', 'errors')}">
                                    <g:textField name="numeroSOLMAN" value="${planificacionEsfuerzoInstance?.numeroSOLMAN}" />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                  <label for="comentarioSistema"><g:message code="planificacionEsfuerzo.comentarioSistema.label" default="Comentario Sistema" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: planificacionEsfuerzoInstance, field: 'comentarioSistema', 'errors')}">
                                    <g:textField name="comentarioSistema" value="${planificacionEsfuerzoInstance?.comentarioSistema}" />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                  <label for="detalles"><g:message code="planificacionEsfuerzo.detalles.label" default="Detalles" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: planificacionEsfuerzoInstance, field: 'detalles', 'errors')}">
                                    
<ul>
<g:each in="${planificacionEsfuerzoInstance?.detalles?}" var="d">
    <li><g:link controller="detallePlanificacion" action="show" id="${d.id}">${d?.encodeAsHTML()}</g:link></li>
</g:each>
</ul>
<g:link controller="detallePlanificacion" action="create" params="['planificacionEsfuerzo.id': planificacionEsfuerzoInstance?.id]">${message(code: 'default.add.label', args: [message(code: 'detallePlanificacion.label', default: 'DetallePlanificacion')])}</g:link>

                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                  <label for="estado"><g:message code="planificacionEsfuerzo.estado.label" default="Estado" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: planificacionEsfuerzoInstance, field: 'estado', 'errors')}">
                                    <g:textField name="estado" value="${planificacionEsfuerzoInstance?.estado}" />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                  <label for="fechaCreacion"><g:message code="planificacionEsfuerzo.fechaCreacion.label" default="Fecha Creacion" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: planificacionEsfuerzoInstance, field: 'fechaCreacion', 'errors')}">
                                    <g:datePicker name="fechaCreacion" precision="day" value="${planificacionEsfuerzoInstance?.fechaCreacion}"  />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                  <label for="otrosCostos"><g:message code="planificacionEsfuerzo.otrosCostos.label" default="Otros Costos" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: planificacionEsfuerzoInstance, field: 'otrosCostos', 'errors')}">
                                    <g:select name="otrosCostos" from="${ar.com.telecom.pcs.entities.OtroCosto.list()}" multiple="yes" optionKey="id" size="5" value="${planificacionEsfuerzoInstance?.otrosCostos*.id}" />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                  <label for="versionPES"><g:message code="planificacionEsfuerzo.versionPES.label" default="Version PES" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: planificacionEsfuerzoInstance, field: 'versionPES', 'errors')}">
                                    <g:textField name="versionPES" value="${fieldValue(bean: planificacionEsfuerzoInstance, field: 'versionPES')}" />
                                </td>
                            </tr>
                        
                        </tbody>
                    </table>
                </div>
                <div class="buttons">
                    <span class="button"><g:actionSubmit class="save" action="update" value="${message(code: 'default.button.update.label', default: 'Update')}" /></span>
                    <span class="button"><g:actionSubmit class="delete" action="delete" value="${message(code: 'default.button.delete.label', default: 'Delete')}" onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');" /></span>
                </div>
            </g:form>
        </div>
    </body>
</html>
