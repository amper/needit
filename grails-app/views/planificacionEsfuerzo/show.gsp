
<%@ page import="ar.com.telecom.pcs.entities.PlanificacionEsfuerzo" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="layout" content="main" />
        <g:set var="entityName" value="${message(code: 'planificacionEsfuerzo.label', default: 'PlanificacionEsfuerzo')}" />
        <title><g:message code="default.show.label" args="[entityName]" /></title>
    </head>
    <body>
        <div class="nav">
            <span class="menuButton"><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></span>
            <span class="menuButton"><g:link class="list" action="list"><g:message code="default.list.label" args="[entityName]" /></g:link></span>
            <span class="menuButton"><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></span>
        </div>
        <div class="body">
            <h1><g:message code="default.show.label" args="[entityName]" /></h1>
            <g:if test="${flash.message}">
            <div class="message">${flash.message}</div>
            </g:if>
            <div class="dialog">
                <table>
                    <tbody>
                    
                        <tr class="prop">
                            <td valign="top" class="name"><g:message code="planificacionEsfuerzo.id.label" default="Id" /></td>
                            
                            <td valign="top" class="value">${fieldValue(bean: planificacionEsfuerzoInstance, field: "id")}</td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name"><g:message code="planificacionEsfuerzo.tipoPlanificacion.label" default="Tipo Planificacion" /></td>
                            
                            <td valign="top" class="value">${fieldValue(bean: planificacionEsfuerzoInstance, field: "tipoPlanificacion")}</td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name"><g:message code="planificacionEsfuerzo.numeroRelease.label" default="Numero Release" /></td>
                            
                            <td valign="top" class="value"><g:link controller="release" action="show" id="${planificacionEsfuerzoInstance?.numeroRelease?.id}">${planificacionEsfuerzoInstance?.numeroRelease?.encodeAsHTML()}</g:link></td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name"><g:message code="planificacionEsfuerzo.fechaRelease.label" default="Fecha Release" /></td>
                            
                            <td valign="top" class="value"><g:formatDate date="${planificacionEsfuerzoInstance?.fechaRelease}" /></td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name"><g:message code="planificacionEsfuerzo.numeroSOLMAN.label" default="Numero SOLMAN" /></td>
                            
                            <td valign="top" class="value">${fieldValue(bean: planificacionEsfuerzoInstance, field: "numeroSOLMAN")}</td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name"><g:message code="planificacionEsfuerzo.comentarioSistema.label" default="Comentario Sistema" /></td>
                            
                            <td valign="top" class="value">${fieldValue(bean: planificacionEsfuerzoInstance, field: "comentarioSistema")}</td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name"><g:message code="planificacionEsfuerzo.detalles.label" default="Detalles" /></td>
                            
                            <td valign="top" style="text-align: left;" class="value">
                                <ul>
                                <g:each in="${planificacionEsfuerzoInstance.detalles}" var="d">
                                    <li><g:link controller="detallePlanificacion" action="show" id="${d.id}">${d?.encodeAsHTML()}</g:link></li>
                                </g:each>
                                </ul>
                            </td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name"><g:message code="planificacionEsfuerzo.estado.label" default="Estado" /></td>
                            
                            <td valign="top" class="value">${fieldValue(bean: planificacionEsfuerzoInstance, field: "estado")}</td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name"><g:message code="planificacionEsfuerzo.fechaCreacion.label" default="Fecha Creacion" /></td>
                            
                            <td valign="top" class="value"><g:formatDate date="${planificacionEsfuerzoInstance?.fechaCreacion}" /></td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name"><g:message code="planificacionEsfuerzo.otrosCostos.label" default="Otros Costos" /></td>
                            
                            <td valign="top" style="text-align: left;" class="value">
                                <ul>
                                <g:each in="${planificacionEsfuerzoInstance.otrosCostos}" var="o">
                                    <li><g:link controller="otroCosto" action="show" id="${o.id}">${o?.encodeAsHTML()}</g:link></li>
                                </g:each>
                                </ul>
                            </td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name"><g:message code="planificacionEsfuerzo.versionPES.label" default="Version PES" /></td>
                            
                            <td valign="top" class="value">${fieldValue(bean: planificacionEsfuerzoInstance, field: "versionPES")}</td>
                            
                        </tr>
                    
                    </tbody>
                </table>
            </div>
            <div class="buttons">
                <g:form>
                    <g:hiddenField name="id" value="${planificacionEsfuerzoInstance?.id}" />
                    <span class="button"><g:actionSubmit class="edit" action="edit" value="${message(code: 'default.button.edit.label', default: 'Edit')}" /></span>
                    <span class="button"><g:actionSubmit class="delete" action="delete" value="${message(code: 'default.button.delete.label', default: 'Delete')}" onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');" /></span>
                </g:form>
            </div>
        </div>
    </body>
</html>
