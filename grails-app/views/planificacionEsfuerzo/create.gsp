

<%@ page import="ar.com.telecom.pcs.entities.PlanificacionEsfuerzo" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="layout" content="main" />
        <g:set var="entityName" value="${message(code: 'planificacionEsfuerzo.label', default: 'PlanificacionEsfuerzo')}" />
        <title><g:message code="default.create.label" args="[entityName]" /></title>
    </head>
    <body>
        <div class="nav">
            <span class="menuButton"><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></span>
            <span class="menuButton"><g:link class="list" action="list"><g:message code="default.list.label" args="[entityName]" /></g:link></span>
        </div>
        <div class="body">
            <h1><g:message code="default.create.label" args="[entityName]" /></h1>
            <g:if test="${flash.message}">
            <div class="message">${flash.message}</div>
            </g:if>
            <g:hasErrors bean="${planificacionEsfuerzoInstance}">
            <div class="errors">
                <g:renderErrors bean="${planificacionEsfuerzoInstance}" as="list" />
            </div>
            </g:hasErrors>
            <g:form action="save" >
                <div class="dialog">
                    <table>
                        <tbody>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="tipoPlanificacion"><g:message code="planificacionEsfuerzo.tipoPlanificacion.label" default="Tipo Planificacion" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: planificacionEsfuerzoInstance, field: 'tipoPlanificacion', 'errors')}">
                                    <g:textField name="tipoPlanificacion" maxlength="100" value="${planificacionEsfuerzoInstance?.tipoPlanificacion}" />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="numeroRelease"><g:message code="planificacionEsfuerzo.numeroRelease.label" default="Numero Release" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: planificacionEsfuerzoInstance, field: 'numeroRelease', 'errors')}">
                                    <g:select name="numeroRelease.id" from="${ar.com.telecom.pcs.entities.Release.list()}" optionKey="id" value="${planificacionEsfuerzoInstance?.numeroRelease?.id}" noSelection="['null': '']" />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="fechaRelease"><g:message code="planificacionEsfuerzo.fechaRelease.label" default="Fecha Release" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: planificacionEsfuerzoInstance, field: 'fechaRelease', 'errors')}">
                                    <g:datePicker name="fechaRelease" precision="day" value="${planificacionEsfuerzoInstance?.fechaRelease}" default="none" noSelection="['': '']" />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="numeroSOLMAN"><g:message code="planificacionEsfuerzo.numeroSOLMAN.label" default="Numero SOLMAN" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: planificacionEsfuerzoInstance, field: 'numeroSOLMAN', 'errors')}">
                                    <g:textField name="numeroSOLMAN" value="${planificacionEsfuerzoInstance?.numeroSOLMAN}" />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="comentarioSistema"><g:message code="planificacionEsfuerzo.comentarioSistema.label" default="Comentario Sistema" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: planificacionEsfuerzoInstance, field: 'comentarioSistema', 'errors')}">
                                    <g:textField name="comentarioSistema" value="${planificacionEsfuerzoInstance?.comentarioSistema}" />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="estado"><g:message code="planificacionEsfuerzo.estado.label" default="Estado" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: planificacionEsfuerzoInstance, field: 'estado', 'errors')}">
                                    <g:textField name="estado" value="${planificacionEsfuerzoInstance?.estado}" />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="fechaCreacion"><g:message code="planificacionEsfuerzo.fechaCreacion.label" default="Fecha Creacion" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: planificacionEsfuerzoInstance, field: 'fechaCreacion', 'errors')}">
                                    <g:datePicker name="fechaCreacion" precision="day" value="${planificacionEsfuerzoInstance?.fechaCreacion}"  />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="versionPES"><g:message code="planificacionEsfuerzo.versionPES.label" default="Version PES" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: planificacionEsfuerzoInstance, field: 'versionPES', 'errors')}">
                                    <g:textField name="versionPES" value="${fieldValue(bean: planificacionEsfuerzoInstance, field: 'versionPES')}" />
                                </td>
                            </tr>
                        
                        </tbody>
                    </table>
                </div>
                <div class="buttons">
                    <span class="button"><g:submitButton name="create" class="save" value="${message(code: 'default.button.create.label', default: 'Create')}" /></span>
                </div>
            </g:form>
        </div>
    </body>
</html>
