<%@page import="ar.com.telecom.util.NumberUtil"%>
<%@page import="ar.com.telecom.util.DateUtil"%>

<g:updateToken />

<br>
<br>
<button id="showCargaHoras" style="display: none;"></button>
	<div id="totalCambio" style="text-align: right;">
		<g:if test="${impactoInstance?.esDeSistema() && (impactoInstance?.puedeEditarSistema(usuarioLogueado) || impactoInstance?.puedeVisualizarSistema()) && impactoInstance.costoTotalCambio()}">
			<g:render template="costoTotalCambio" model="['impactoInstance':impactoInstance]"></g:render>
		</g:if>
	</div>
		<!-- DETALLES PARA SISTEMAS -->
		<g:if test="${impactoInstance?.esDeSistema()}">
			<g:if test="${impactoInstance?.esConsolidado()}">
				<g:render template="tablaConsolidacionPlanificacion" model="${['edit': false, 'consolidacionList': consolidacionList]}"></g:render>
				<g:if test="${impactoInstance.habilitaAcciones(usuarioLogueado)}" >
					<g:if test="${trabajaSobrePadre || trabajaSobreCoord}">
						<br>
						<div class="separator"></div>
						<div class="seccionHide2">
							<p class="tituloSeccion">Info adicional del pedido</p>							
							<div class="formSeccion">
								<div class="pDos">
									<label for="fechaVencimientoPlanificacion-rpid" class="formLabel2 validate">Fecha vto. planificaci&oacute;n:</label><br>
									<g:textField id="fechaVencimientoPlanificacion-rpid" class="datePicker"
										name="fechaVencimientoPlanificacion"
										style="background-image:none !important;"  maxLength="10"
										value="${ar.com.telecom.util.DateUtil.toString(pedidoInstance?.fechaVencimientoPlanificacion)}"
									 /><br>
									<g:hiddenField name="ffechaVencimientoPlanificacion"
										id="ffechaVencimientoPlanificacion"
										value="${ar.com.telecom.util.DateUtil.toString(pedidoInstance?.fechaVencimientoPlanificacion)}" 
										/>
								</div>
								<div class="pDos">
									<label class="formLabel2" for='comentarioSistema' style="text-align: left !important;">Comentarios:</label><br>
									<g:textArea style="width: 220px;" id="comentarioSistema" name="comentarioSistema">${planificacionActual?.comentarioSistema?.trim()}</g:textArea>
								</div>
							</div>
						</div>
					</g:if>
				</g:if>
				<g:else>
					<div class="tag-dosDiv" style="border: none;">
						<div class="columna1" style="align: right;">
							<label class="formLabel validate">Fecha vto. planificaci&oacute;n:</label>
							<p class="info">${ar.com.telecom.util.DateUtil.toString(pedidoInstance?.fechaVencimientoPlanificacion)}</p>
						</div>
						<div class="columna2">
							<label for='comentarioSistema' style="text-align: left;">Comentarios:</label>
							<p class="info">${planificacionActual?.comentarioSistema?.trim()}</p>
						</div>
					</div>

				</g:else>
			</g:if>
			<g:else>
				<g:if test="${impactoInstance?.puedeEditarSistema(usuarioLogueado)}">
					<g:render template="tablaDetallesSistema" model="${['edit': true]}"></g:render>
				</g:if>
				<g:else>
					<g:if test="${impactoInstance?.puedeVisualizarSistema() || impactoInstance?.puedeReasignar(usuarioLogueado)}">
						<g:render template="tablaDetallesSistema"
							model="${['edit': false]}"></g:render>
					</g:if>
					<g:else>
						<div class="msgPlanificacionEsf">La planificaci&oacute;n de actividades de sistema se encuentra
							pendiente, todav&iacute;a no podr&aacute; visualizar la
							informaci&oacute;n</div>
					</g:else>
				</g:else>
			</g:else>
		</g:if>
		
		<!-- DETALLES PARA AREAS DE SOPORTE -->
		<g:if test="${impactoInstance?.esDeAreaDeSoporte()}">
			<table id="tablaFasesPlanificacionEsfuerzo">
				<tr><td>
					<div id="tareas">
							<g:render
								template="tablaTareas"
								model="${['impacto': impacto, 'detalles': planificacionActual?.detalles.sort{it.id}, 'puedeEditar': impactoInstance?.puedeEditarAreaSoporte(usuarioLogueado) ]}">
							</g:render>
					</div>
					<br>
					<g:if test="${impactoInstance?.puedeEditarAreaSoporte(usuarioLogueado)}">
						<div align="center">
							<button id="addCargaTareas" class="formSeccionSubmitButton">Agregar actividad</button>
						</div>
					</g:if>
				</td></tr>
			</table>
		</g:if>

<br>
<g:if test="${pedidoHijo.planificaActividadesSistema() && impactoInstance?.puedeEditarSistema(usuarioLogueado)}">
	<g:if test="${trabajaSobrePadre}">
		<center>
			<button id="reenviaRSWF" class="formSeccionSubmitButton"
				style="margin-right: 5px;">Reenviar RSWF</button>
			<button id="editar" class="formSeccionSubmitButton">Editar</button>
		</center>
		<br>
	</g:if>


	<div class="tag-dosDiv">
		<div>
			<g:if test="${impactoInstance?.cargaRelease()}">
				<div class="pDos">
					<label>N&uacute;mero de release:</label><br>
					<g:select id="numeroRelease" name="numeroRelease.id" from="${releases}"
						optionKey="id" value="${planificacionActual.numeroRelease?.id}"
						noSelection="[ ' ': '']" />
				</div>
				<div class="pDos">
					<label>Fecha release:</label><br>

					<g:textField id="fechaRelease" class="datePicker"
						name="fechaRelease" style="background-image:none !important;" />
					<g:hiddenField name="ffechaRelease" id="ffechaRelease"
						value="${ar.com.telecom.util.DateUtil.toString(planificacionActual.fechaRelease)}" />
				</div>
			</g:if>
			<g:if test="${impactoInstance?.cargaReferenciaSOLMAN()}">
				<div>
					<label class="validate">Referencia SOLMAN:</label><br> <input
						type="text" value="${planificacionActual.numeroSOLMAN}"
						id="numeroSOLMAN" name="numeroSOLMAN">
				</div>
			</g:if>
		</div>
	</div>
</g:if>
<g:else>
	<g:if test="${impactoInstance?.cargaRelease()}">
		<div class="pDos">
			<label>N&uacute;mero de release:</label><br>
			<span class="info" style="width: 40%">${planificacionActual.numeroRelease?.descripcion}</span>
		</div>
		<div class="pDos">
			<label>Fecha release:</label><br>
			<span class="info" style="width: 40%">${ar.com.telecom.util.DateUtil.toString(planificacionActual.fechaRelease)}</span>
		</div>
	</g:if>	
	
	<g:if test="${impactoInstance?.cargaReferenciaSOLMAN()}">
		<div class="pCompleto">
			<label>Referencia SOLMAN:</label>
			<span class="info" style="width: 40%">${planificacionActual.numeroSOLMAN}</span>
		</div>
	</g:if>
</g:else>

<div style="clear: both; margin-bottom: 20px;"></div>
<g:set var="planificaYPuedeEditar"
	value="${(pedidoHijo.planificaActividadesSistema() && impactoInstance?.puedeEditarSistema(usuarioLogueado)) || (pedidoHijo.planificaTareasSoporte() && impactoInstance?.puedeEditarAreaSoporte(usuarioLogueado)) }"
	scope="page"
/>
<div style="clear: both; margin-bottom: 20px;"></div>
<g:if test="${planificaYPuedeEditar}">
	<div class="tag-dosDiv">
		<div class="columna1">
			<label style="font-weight: bold; width: 100px;" for='inputOtros'>Otros costos:</label>
			<button id="addOtrosCostos" class="formSeccionSubmitButton">Agregar costo</button>
		</div>
		<div class="columna2">
<%--				<g:if test="${planificaYPuedeEditar}">--%>
<%--					<label for="justificacionAprobacion" style="font-weight: bold; text-align: left;" >Justificaci&oacute;n:</label>--%>
<%--					<g:textArea style="width: 220px; padding: 8px;" name="justificacionAprobacion" value="${pedidoHijo.justificacion(pedidoHijo?.faseActual)}"></g:textArea>--%>
<%--				</g:if>--%>
		</div>	
	</div>
</g:if>

	<g:if test="${impactoInstance?.esConsolidado()}">
		<g:if test="${resultOtrosCostos && !resultOtrosCostos?.isEmpty()}">
			<div id="listOtrosCostos" style="position: relative; text-align: left; width: 100%;">
				<table class="TablaValidacionImpactoHeader" border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td colspan="2" width="50%">Tipo de costo adicional</td>
						<td width="30%">Detalle</td>
						<td width="20%" style="text-align: right;">Monto (en Pesos)</td>
					</tr>
				</table>
				<g:each in="${resultOtrosCostos}" status="i" var="otroCosto">
					<g:divsOtrosCostos otroCostos="${otroCosto}" id="otroCosto_${i}" />
				</g:each>
				<br>
			</div>
		</g:if>
		<g:if test="${resultAreaSoportes && !resultAreaSoportes.isEmpty()}">
			<div style="position: relative; text-align: left; width: 100%;">
				<table class="TablaValidacionImpactoHeader" border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td colspan="2" width="40%">&Aacute;rea de Soporte</td>
						<td width="15%">Desde</td>
						<td width="15%">Hasta</td>
						<td width="17%" style="text-align: right;">Total horas</td>
						<td width="20%" style="text-align: right;">Monto (en Pesos)</td>
					</tr>
				</table>
				<g:each in="${resultAreaSoportes}" status="i" var="areaSoporte">
					<g:divsAreaSoporte areaSoportes="${areaSoporte}" id="areaSoporte_${i}" />
				</g:each>
			</div>
		</g:if>
	</g:if>
	<g:else>
		<div id="otrosCostos" style="position: relative; text-align: left;">
			<g:render
				template="tablaOtrosCostos"
				model="${['otrosCostos': planificacionActual?.otrosCostos, 'totalCostos': planificacionActual?.totalMontoOtrosCostos(), planificaYPuedeEditar:planificaYPuedeEditar]}">
			</g:render>
		</div>
	</g:else>


<%--	Genero un boton oculto fuera del formulario para que no se actualice por ajax y pueda pegarle al jquery del doc ready--%>
<button id="btnCerrarOtrosCostos" style="display: none;"></button>

