<%@page import="ar.com.telecom.util.NumberUtil"%>
<%@page import="ar.com.telecom.util.DateUtil"%>

<g:updateToken />

<g:if test="${consolidacionList}">
<table id="tablaFasesPlanificacionEsfuerzo" cellspacing="0" cellpadding="0">
	<tr>
		<th style="width: 200px">Fase planificaci&oacute;n</th>
		<th style="width: 100px">Desde</th>
		<th style="width: 100px">Hasta</th>
		<th style="text-align: right;">Total Horas</th>
		<th style="text-align: right;">Monto (en Pesos)</th>
	</tr>

<!-- SHOW -->
	<g:each in="${consolidacionList}" var="detalle" status="i">
	 <tr style="padding: 0px;" class="${(i % 2) == 0 ? 'odd' : 'even'}">

		<td id="fase1">
			${detalle.getAt(0)}
		</td>
		
		<td>${DateUtil.toString(detalle.getAt(1))}</td>
		<td>${DateUtil.toString(detalle.getAt(2))}</td>
		
		<td style="text-align: right; width: 70px;" > ${detalle?.getAt(3)} </td>
		<td style="text-align: right;">${NumberUtil.toString(detalle.getAt(4))}</td>
	 </tr>
	</g:each> 
</table>	
</g:if>
<g:else>
	<br>
	<div class="msgPlanificacionEsf">
		Todav&iacute;a no hay informaci&oacute;n suficiente para armar el consolidado de planificaci&oacute;n de sistemas
	</div>
</g:else>	