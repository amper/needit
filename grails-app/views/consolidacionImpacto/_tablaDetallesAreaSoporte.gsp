<%@page import="ar.com.telecom.util.NumberUtil"%>

<g:updateToken />

<table id="tablaFasesPlanificacionEsfuerzo">
<g:if test="${!planificacionActual?.detalles.findAll { detalle -> detalle.esDeAreaDeSoporte()}.isEmpty()}">
	<tr>
		<th style="width: 200px">Actividad</th>
		<th style="width: 100px">Comentario</th>
		<th style="width: 100px">Desde</th>
		<th style="width: 100px">Hasta</th>
		<th style="width: 10px">Total Horas</th>
		<g:if test="${edit}">
			<th style="width:10px;"> </th>
		</g:if>	
	</tr>
</g:if>
<g:if test="${edit}">
	<!-- EDIT --> 
	<g:each in="${planificacionActual?.detalles.findAll { detalle -> detalle.esDeAreaDeSoporte()}.sort{ it.id }}" var="detalle">
		<tr style="padding: 0px;">
			<td id="fase1">${detalle.tareaSoporte?.descripcion}</td>
			<td>${detalle.comentarioDetalleAreaSoporte}</td>
			<td><g:textField id="desde_${detalle.id}" class="datePicker"
					name="date1" value="${detalle.fechaDesde}"
					onChange="validaFechasCompletas('${detalle.id}')" />
			</td>
			<td><g:textField id="hasta_${detalle.id}" class="datePicker"
					name="date2" value="${detalle.fechaHasta}"
					onChange="validaFechasCompletas('${detalle.id}')" />
			</td>
	
			<td style="width: 70px;"><input id="horas_${detalle.id}"
				type="text" value="160"
				style="display: none; width: 45px; float: left;" />
			</td>
				<td><button class="limpiar"></button>
			</td>
		</tr>
	</g:each>
</g:if>
<g:else>
<!-- SHOW -->
	<g:each in="${planificacionActual?.detalles.findAll { detalle -> detalle.esDeAreaDeSoporte()}.sort{ it.id }}" var="detalle">
		<tr style="padding: 0px;">
			<td id="fase1">${detalle.tareaSoporte?.descripcion}</td>
			<td>${detalle.comentarioDetalleAreaSoporte}</td>
			<td>${detalle.fechaDesde}</td>
			<td>${detalle.fechaHasta}</td>
			<td style="width: 70px;"> ${NumberUtil.seteaComas(totalHoras())}</td>
		</tr>
	</g:each>
</g:else>
</table>