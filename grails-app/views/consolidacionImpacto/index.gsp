<%@page import="ar.com.telecom.util.DateUtil"%>
<%@page import="ar.com.telecom.FaseMultiton"%>
<%@page import="ar.com.telecom.pcs.entities.RolAplicacion"%>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta name="layout" content="mainApp" />

<link rel="stylesheet" href="${resource(dir:'css',file:'glDatePicker.css')}" />

<g:javascript library="impacto" />
<g:javascript library="animatedcollapse" />
<g:javascript library="anexoPorTipo" />
<g:javascript library="consolidacionImpacto" />
<g:javascript library="boxUser" />
<g:javascript library="glDatePicker" />
 
</head>
<body>
<g:if test="${params.tabSeleccionado}">
	<script>
		g_tabSeleccionado = "${params.tabSeleccionado}";
	</script>
</g:if>

	<div class="contenedorTitulo">
		<g:titulo pedido="${impactoInstance?.pedido}" />
		<g:workflow pedidoId="${impactoInstance?.pedido?.id}" />
	</div>

	<div id="contenedorIzq">
		<g:if test="${flash.message}">
			<div class="message">
				${flash.message}
			</div>
		</g:if>

		<g:hasErrors bean="${impactoInstance?.pedido}">
			<div class="errors">
				<g:renderErrors bean="${impactoInstance?.pedido}" as="list" />
			</div>
		</g:hasErrors>

		<g:form id="formPedido" name="formPedido" class="formGeneral" action="grabarFormulario" useToken="true">
			<div class="seccionHide">
				<g:render template="/templates/workflowHidden" />
				<g:actionSubmit id="refreshView" action="completaGrupoRSWF"
					style="display:none" value="Refrescar vista" />
				<g:hiddenField name="pedidoHijoId" id="pedidoHijoId"
					value="${pedidoHijo?.id}"></g:hiddenField>
<%--				<g:hiddenField name="pedidoId" id="pedidoId"--%>
<%--					value="${pedido?.id}"></g:hiddenField>--%>
				<g:hiddenField name="hVengoPorAnexos" value="${params.controller}" />
				<g:hiddenField name="hForm" id="hForm" value="formPedido"/>
				<g:hiddenField name="anexoFrom" id="anexoFrom" value=""/>
				<g:hiddenField name="tabSeleccionado" id="tabSeleccionado" value="${params.tabSeleccionado}"/>
				
				<g:cabeceraPedido pedido="${pedidoInstance}" />
				<p class="tituloSeccion">Datos de la especificaci&oacute;n</p>
				<div class="formSeccion">
					<div class="pDos" style="width: 53%;">
						<label class='formLabel'>Impacto:</label><br>
						<g:select id="impacto" name="impacto"
							from="${pedidoInstance.getImpactos()}" optionKey="id"
							value="${impacto}" onChange="g_tabSeleccionado=0;" />
                        <span title="${impactoInstance.estiloDescripcion}" id="pedido_${impactoInstance.estilo}"></span>
<%--						<a title="${impactoInstance.estiloDescripcion}" class="formLabel2 ${impactoInstance.estilo}" href="#"></a>--%>
<%--						<g:if test="${pedidoHijo.cerroEspecificacion() && pedidoInstance.legajoCoordinadorCambio.equals(usuarioLogueado) && (!impactoInstance.aplicaAPadre(impacto) && !impactoInstance.aplicaACoordinacion(impacto))}">--%>
						<g:if test="${impactoInstance.puedeReenviar(usuarioLogueado)}">
							<a title="Reenviar" class="formLabel2 reenviar" href="javascript:openPopUpReenviar('${pedidoInstance?.id }', '${impactoInstance?.id}','${pedidoInstance?.aprobacionPendiente(pedido?.faseActual, usuarioLogueado)}');"></a>							
						</g:if>
						<g:if test="${trabajoSobreHijo}">
							<div id="grupoRSWF" style="padding-top: 10px;">
								<g:render template="grupoRSWF" model="['grupo':grupo]"></g:render>
							</div>
						</g:if>
						<g:if test="${impactoInstance.aplicaAPadre(impacto)}">
							<label class='formLabel'>Prioridad</label><br>
							<g:select id="comboPrioridad" name="prioridad.id"
								from="${ar.com.telecom.pcs.entities.Prioridad.list()}"
								optionKey="id" value="${impactoInstance?.pedido?.prioridad?.id}"
								noSelection="['null': 'Ninguna']" />
						</g:if>
					</div>
					<div class="pDos" style="width: 40%;">

						<g:if test="${impactoInstance.aplicaAPadre(impacto)}">
							<label class="formLabel2" for='inputIntUsuario'>Coordinador cambio</label>
							<div id="legajoCoordinadorCambio" class="boxUser">
								<g:render template="/templates/userBox"
									model="['person':impactoInstance.criterioImpacto.pedido.legajoCoordinadorCambio, 'box':'legajoCoordinadorCambio', 'personsAutocomplete': null]"></g:render>
							</div>
						</g:if>
						<g:if test="${impactoInstance.aplicaACoordinacion(impacto)}">
								<label class="formLabel2 validate" for='inputIntUsuario'>Coordinador cambio</label>
								<div id="legajoCoordinadorCambio" class="boxUser">
									<g:render template="/templates/userBox"
										model="['person':pedidoInstance?.legajoCoordinadorCambio, 'box':'legajoCoordinadorCambio', 'personsAutocomplete': personsAutocompleteAsign]"></g:render>
								</div>
						</g:if>

						<g:if test="${impactoInstance.aplicaAHijo(impacto)}">
<%--							<g:if test="${pedidoHijo?.legajoUsuarioResponsable}">--%>
									<label class="formLabel2 validate" for='inputIntUsuario'>Asignatario</label>
									<div id="legajoUsuarioResponsable" class="boxUser">
										<g:render template="/templates/userBox"
											model="['person':pedidoHijo?.legajoUsuarioResponsable, 'box':'legajoUsuarioResponsable', 'personsAutocomplete': personsAutocompleteAsign, 'pedidoHijoId':pedidoHijo?.id, 'impacto': impacto]"></g:render>
									</div>
									<g:if test="${impactoInstance.puedeReasignar(usuarioLogueado)}">
										<g:secureRemoteLink title="Eliminar" class="limpiarBox" id="${RolAplicacion.RESPONSABLE_HIJO}"
											update="legajoUsuarioResponsable"
											params="['username': '', 'box':'legajoUsuarioResponsable', 'pedidoHijoId':pedidoHijo?.id, 'impacto': impacto]"
											action="completarUsuario"
											onComplete="ajaxCall({container: 'botonera', controller:'consolidacionImpacto',
											action:'recargaBotonera',parameters:{username: '${usuarioLogueado}',
											pedidoId: '${impactoInstance?.pedido.parent.id}', impacto: '${impactoInstance?.pedido.id }'}});"></g:secureRemoteLink>
									</g:if>
<%--							</g:if>--%>
<%--							<g:else>--%>
<%--								<br>--%>
<%--								<div class="msgPlanificacionEsf">Este pedido no posee un usuario responsable asignado</div>--%>
<%--							</g:else>--%>
						</g:if>
					</div>
					<g:if test="${pedidoHijo.puedeEditarEspecificacionImpacto(usuarioLogueado) && pedidoHijo.obtenerLogReenviar()}">
						<div class="pCompleto" style="margin-left: 100px;">
							<div class="pDos" style="width: 40%;">
								<label class="formLabel2">Usuario que reenvi&oacute;</label>
								<div id="legajoUsuarioReenviar" class="boxUser">
									<g:render template="/templates/userBox"
									model="['person':pedidoInstance?.legajoCoordinadorCambio, 'box':'legajoUsuarioReenviar', 
									'personsAutocomplete': personsAutocompleteAsign, 'impacto': impacto]"></g:render>
								</div>
							</div>
							<div style="float: left; margin-top: 30px;">
								<label class="formLabel2">Observaciones:</label>
								<p class="info">${pedidoHijo.obtenerLogReenviar()?.descripcionEvento}</p>
							</div>
						</div>
					</g:if>
				</div>
				<g:render template="tabPedido"></g:render>

				<g:if test="${!impactoInstance.aplicaAHijo(impacto)}">
					<g:render template="/templates/ultimaJustificacionDenegacion"
						     			model="['pedidoInstance': pedidoInstance, 'fase': pedidoInstance.faseConsolidacionImpacto()]"></g:render>
				</g:if>
				<g:else>
					<g:render template="/templates/ultimaJustificacionDenegacion"
						     			model="['pedidoInstance': pedidoHijo, 'fase': pedidoHijo.faseEspecificacionImpacto()]"></g:render>

				</g:else>
				
				<g:if test="${(pedidoHijo.planificaActividadesSistema() && impactoInstance?.puedeEditarSistema(usuarioLogueado)) || (pedidoHijo.planificaTareasSoporte() && impactoInstance?.puedeEditarAreaSoporte(usuarioLogueado)) }">
					<p class="tituloSeccion">Justificaci&oacute;n</p>
					<div class="formSeccion" style="background-color: #FFF; text-align: center;">
						<div style="margin: 0px auto; width: 90%">
						<br>
							<label for="justificacionAprobacion" style="font-weight: bold; text-align: left; vertical-align:top; width: 220px;" >Justificaci&oacute;n:</label><br>
							<g:textArea style="width: 220px; padding: 8px;" name="justificacionAprobacion" value="${pedidoHijo.justificacion(pedidoHijo?.faseActual)}"></g:textArea>
						</div>
					</div>				
				</g:if>				
				
				
				<div id="botonera">
					<g:render template="botonera"></g:render>
				</div>

			</div>
			<div id="token"><g:render template="/templates/token"></g:render></div>
		</g:form>
</div>

<!-- Lateral Derecho -->
	<g:render template="/templates/menuLateral" model="['pedidoInstance':pedidoInstance]"/>
	<!-- Fin Lateral Derecho -->
		<!-- popup Otros Costos -->
		<div id="dialog-OtrosCostos" title="Agregar otros costos" style="display: none;">
<%--			<g:render template="otrosCostos" model="['pedido':pedidoInstance, 'impacto': impacto, 'tiposCosto': tiposCosto]"></g:render>--%>
		</div>
		<!-- popup Otros Costos -->

		<!-- popup Carga Tareas -->
		<div id="dialog-CargaTareas" title="Agregar nuevas tareas" style="display: none;">
<%--			<g:render template="popupCargaTareas" model="${['impacto': impacto, 'tareas':tareas, 'valorHora':valorHora]}">--%>
<%--			</g:render>--%>
		</div>
		<!-- popup Carga Tareas -->

		<!-- popup Cargar Horas -->
		<div id="dialog-CargaHoras"
			title="Cargar horas con y sin gesti&oacute;n de capacidad"
			style="display: none;">
			<g:form name="formCargaHoras">
				<div id="bodypopupcargahoras">
					<g:render template="bodyCargaHoras"></g:render>
				</div>
				<g:hiddenField id="idPlanificacion" name="idPlanificacion"	value="${planificacionActual.id}" />
				<g:hiddenField name="pedidoId" value="${pedidoInstance.id}" />
				<g:hiddenField name="impacto" value="${impactoInstance?.id}" />
			</g:form>
		</div>
		<%-- fin Cargar Horas --%>

		<%-- Carga Anexo --%>
		<div id="dialog-anexos" title="Nuevo Anexo" style="display: none;">
		</div>
		<%-- fin Carga Anexo --%>
		
		<div id="dialog-Reenviar" title="Reenviar" style="display: none;">
<%--			<g:render template="/consolidacionImpacto/reenviar" model="['pedidoInstance':pedidoInstance, 'impactoInstance':impactoInstance]"></g:render>--%>
		</div>		
		
		<%-- Carga Otras Pruebas --%>
		<div id="dialog-NuevaPrueba" title="Ingreso prueba opcional" style="display: none;">
			<div style="width: 100%; overflow: auto;">
		

			</div>
		</div>		
		<%-- fin Carga Otras Pruebas --%>		
</body>
</html>
