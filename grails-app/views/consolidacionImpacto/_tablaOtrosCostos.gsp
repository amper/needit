<%@page import="ar.com.telecom.util.NumberUtil"%>

<g:updateToken pedido="${impactoInstance?.pedido?.parent}" pedidoHijo="${impactoInstance?.pedido}"/>

<g:if test="${otrosCostos}">
<%--	<g:if test="${planificaYPuedeEditar || impactoInstance.pedido.cerroEspecificacion()}" >--%>

		<div id="divTareas">
			 <table style="width: 95%" style="border: 0px"> 
				<tr>
					<th style="width: 40%">Tipo de costo adicional</th>
					<th style="width: 30%">Detalle</th>
					<th style="width: 30%; text-align: right">Monto (en Pesos)</th>
					<g:if test="${planificaYPuedeEditar}">
						<td></td>
					</g:if>
				</tr>
			
				<g:set var="counter" value="${0}" />
				<g:each in="${otrosCostos}" var="otroCosto">
			
			 		<tr id="fila${counter}">
						<td id="fase1">${otroCosto?.tipoCosto?.descripcion}</td>
						<td>${otroCosto?.detalle}</td>
						<td style="text-align: right;">${NumberUtil.setearDecimalesDefaultConComa(otroCosto?.costo)}</td>
			 
						<g:if test="${planificaYPuedeEditar }">
						
							<g:set var="pedidoOrigen" value="${impactoInstance ? impactoInstance.pedido : pedidoInstance}"/>
							<g:set var="aprobacion" value="${pedidoOrigen?.aprobacionActual(pedidoOrigen?.faseActual, usuarioLogueado)}"/>
		
							<td>
								<g:secureRemoteLink title="Eliminar costo" 
									update="otrosCostos"
									params="['otroCostoId':otroCosto?.id, 'pedidoId':pedidoHijo?.id,'impacto':impacto, 'otroCosto':otroCosto, 'aprobacionId': aprobacion?.id]"
									action="eliminaOtroCosto" 
									onComplete="recargaCostoTotal('${impacto}', '${pedidoHijo?.id}');"
									after="new Ajax.Updater('contenedorTipoCosto', '/' + gNameApp + '/consolidacionImpacto/recargarComboCostos',{asynchronous:true,evalScripts:true,parameters:'pedidoId=${pedidoHijo?.id}&impacto=${impacto}&costoEliminado=${otroCosto.id}'});return false;" >
									<img src="${resource(dir:'images',file:'delete.gif')}" alt="Eliminar" width="10" height="10" />							
								</g:secureRemoteLink>
							</td>
						</g:if>		      
			        </tr>
			
					<g:set var="counter" value="${counter + 1}" />
				</g:each>
			
				<tr>
					<td colspan="4" style="padding:0px;"><hr style="border:none;height:1px;"/></td>
				</tr>
				<tr >
					<td colspan="2" style="font-weight:bold; text-align:right">Total:&nbsp;</td>
					<td style="font-weight:bold; text-align: right;">${NumberUtil.setearDecimalesDefaultConComa(totalCostos)}</td>
					<g:if test="${planificaYPuedeEditar}">
						<td></td>
					</g:if>			
				</tr>
			</table>
		</div>
<%--	</g:if>--%>
</g:if>
