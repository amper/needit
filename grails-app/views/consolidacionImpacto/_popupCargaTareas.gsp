<%@page import="ar.com.telecom.util.NumberUtil"%>

<g:updateToken pedido="${impactoInstance?.pedido?.parent}" pedidoHijo="${impactoInstance?.pedido}"/>

<div id="cargaNuevaTarea">
	<script type="text/javascript">
		jQuery(document).ready(
			function($) {
	
				$("#comboTareaSoporte").combobox();

				$("#fechaDesdeDatepicker-rpid").datepicker();
				$("#fechaDesdeDatepicker-rpid" ).datepicker( "option", "dateFormat", "dd/mm/yy");
				if (document.getElementById("fechaDesdeHidden"))
					$("#fechaDesdeDatepicker-rpid").val(document.getElementById("fechaDesdeHidden").value);
				
				$("#fechaHastaDatepicker-rpid").datepicker();
				$("#fechaHastaDatepicker-rpid" ).datepicker( "option", "dateFormat", "dd/mm/yy");
				if (document.getElementById("fechaHastaHidden"))
					$("#fechaHastaDatepicker-rpid").val(document.getElementById("fechaHastaHidden").value);
				
<%--				calcularTotalCargaTareas();--%>
			});
			 
	</script>
	
	<g:if test="${flash.message}">
		<div class="message" id="mensajes">
			${flash.message}
		</div>
	</g:if>
	<g:hasErrors bean="${pedido}">
		<div class="errors" id="errores">
			<g:renderErrors bean="${pedido}" as="list" />
		</div>
	</g:hasErrors>
	
	<form id="cargaTarea" name="cargaTarea" useToken="true" >
	
	<g:hiddenField name="impacto" id="impacto" value="${impacto}"></g:hiddenField>
	
	<div class="pDos">
		<label style="font-weight: bold;">Actividad:</label><br>
		<g:select id="comboTareaSoporte" name="tareaSoporte.id"	from="${tareas}" value="${detalle?.tareaSoporte?.id}" optionKey="id" />
	</div>
	
	<div class="pDos">
		<label style="font-weight: bold;">Grupo:</label>
		<p class="info" style="width: auto;">${grupo}</p>
		<g:hiddenField name="grupo" id="grupo" value="${grupo}" />
	</div>
	
	<div class="pDos">
		<label style="font-weight: bold;">Fecha desde:</label>
		<g:textField name="fechaDesde" class="formInput date" id='fechaDesdeDatepicker-rpid' onClick="showDatePickerTarea('fechaDesdeDatepicker-rpid')"/>
		<g:hiddenField name="fechaDesdeHidden" value="${detalle?.fechaDesde?.format('dd/MM/yyyy')}" />
	</div>
	
	<div class="pDos">
		<label style="font-weight: bold;">Horas:</label><br>
<%--		<g:textField class="inputCorto" name="cantidadHoras" id="cantidadHoras" value="${detalle?.cantidadHoras ? detalle.cantidadHoras : 0}" onKeyUp="javascript:calcularTotalCargaTareas();" onkeypress='soloNumerosSinSimbolos(event)' />--%>
		
<%--		<g:textField id="cantidadHoras" name="cantidadHoras" maxlength="5" class="inputCorto" type="text" value="${detalle?.cantidadHoras ? detalle.cantidadHoras : 0}" onKeyUp="javascript:calcularTotalCargaTareas();" onkeypress='soloNumerosSinSimbolos(event)' />--%>
		<g:textField id="cantidadHoras" name="cantidadHoras" maxlength="5" class="inputCorto" type="text" onkeypress='soloNumeros(event,null)' onKeyUp="setCantidadHora(this.value); calcularTotalCargaTareas(this.value);" value="${cargaHora?.cantidadHoras}"/>
	</div>
	
	<div class="separator">
	</div>
	
	<div class="pDos">
		<label style="font-weight: bold;">Fecha hasta:</label>
		<g:textField name="fechaHasta" size="11" class="formInput date" id='fechaHastaDatepicker-rpid' onClick="showDatePickerTarea('fechaHastaDatepicker-rpid')" />
		<g:hiddenField name="fechaHastaHidden" value="${detalle?.fechaHasta?.format('dd/MM/yyyy')}" />
	</div>
	
	
	<div class="pDos">
		<label style="font-weight: bold;">$ x hora:</label><br>
		<g:set scope="page" var="valorHoraOriginal" value="${detalle?.valorHora ? detalle?.valorHora : valorHora}"/>
		<g:textField class="inputCorto" name="valorHora" id="valorHora" value="${NumberUtil.seteaComas(valorHoraOriginal)}" onKeyUp="setValorHora(this.value);calcularTotalCargaTareas(this.value);" onkeypress='soloNumeros(event, true)' />
		<g:hiddenField id="valorDefaultHora" name="valorDefaultHora" value="${valorHora}"/>
	</div>
	
	<div class="separator">
	</div>
	
	<div class="pDos">
	</div>
	
	<div class="pDos">
		<div class="pTotal">
			<label style="font-weight: bold;">Monto (en Pesos): </label><br>
			<div>
				<div class="formLabel totalShow" id="totalCargaHora"></div>
			</div>
		</div>
	</div>
	
	<div class="separator">
	</div>
	
	<div class="pCompleto">
		<label style="font-weight: bold;">Comentarios:</label><br>
		<g:textArea name="comentarioDetalleAreaSoporte" style="width: 560px; height: 80px;" value="${detalle?.comentarioDetalleAreaSoporte}"></g:textArea>
	</div>
	
	<div class="separatorBotonera">
	</div>
	
	<div align="center">
		<button id="btnCancelarCargaTarea" class="formSeccionSubmitButtonAnexo" value="Cancelar" name="Cancelar" onClick="cerrarPopUpAgregarCargar();">Cerrar</button>			
		&nbsp;
		<g:submitToRemoteSecure
			class="formSeccionSubmitButtonAnexo" 
			controller="consolidacionImpacto" action="agregaTarea"
			update="cargaNuevaTarea" onComplete="recargarTabla('${impacto}','${grupo}')" id="btnAceptaryNuevaCargaTarea" name="btnAceptarCargaTarea" after="limpiarPopUpCargaTarea();"
			value="Agregar" />
	</div>
	
	</form>
</div>
