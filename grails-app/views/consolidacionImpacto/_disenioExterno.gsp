<%@ page import="ar.com.telecom.FaseMultiton"%>
<g:updateToken />

<g:javascript library="tiny_mce/tiny_mce" />
<script type="text/javascript">

tinyMCE.init({
    mode : "exact",
    elements : "premisasSupuestosRestricciones,descripcionVolumetriaPerformance,descripcionFuncionalSolucion,principalesCambiosProcesos,interfacesInvolucradasSolucion,definicionesPendientesWarnings",
    theme : "advanced",
	skin : "o2k7",
    plugins : "autolink,lists,pagebreak,style,layer,table,save,advhr,advimage,advlink,emotions,iespell,inlinepopups,insertdatetime,preview,media,searchreplace,print,contextmenu,paste,directionality,fullscreen,noneditable,visualchars,nonbreaking,xhtmlxtras,template,wordcount,advlist", 
             
    // Theme options - button# indicated the row# only
    theme_advanced_buttons1 : "newdocument,print,|,bold,italic,underline,|,fontsizeselect,|,cut,copy,paste",
    theme_advanced_buttons2 : "bullist,numlist,|,undo,redo,|,forecolor,backcolor,|,image,|,fullscreen,template",
    theme_advanced_buttons3 : "",      
    theme_advanced_toolbar_location : "top",
    theme_advanced_toolbar_align : "left",
    theme_advanced_statusbar_location : "bottom",
    theme_advanced_resizing : true,
    paste_text_sticky : true,
    setup : function(ed) {
        ed.onInit.add(function(ed) {
          ed.pasteAsPlainText = true;
        });
    },
	template_external_list_url : "lists/template_list.js",

		//template_external_list_url : "lists/template_list.js",
		external_link_list_url : "lists/link_list.js",
		external_image_list_url : "lists/image_list.js",
		media_external_list_url : "lists/media_list.js"
});
</script>

	<div class="tag-dosDiv">
		<div class="columna1">
			<div>
			<label for='premisasSupuestosRestricciones' >Premisas, supuestos y restricciones:</label>
			<g:if test="${impactoInstance.puedeEditar(usuarioLogueado)}">
				<g:textArea id="premisasSupuestosRestricciones" name="premisasSupuestosRestricciones" style="width:220px;">${disenio?.premisasSupuestosRestricciones}</g:textArea>
			</g:if>
			<g:else>
				<p>${disenio?.premisasSupuestosRestricciones}</p>
			</g:else>
			</div>
		</div>
		<div class="columna1">	
			<div>
			<label for='descripcionVolumetriaPerformance' >Descripci&oacute;n de volumetr&iacute;a y performance:</label>
			<g:if test="${impactoInstance.puedeEditar(usuarioLogueado)}">
				<g:textArea id="descripcionVolumetriaPerformance" name="descripcionVolumetriaPerformance" style="width:220px;">${disenio?.descripcionVolumetriaPerformance}</g:textArea>
			</g:if>
			<g:else>
				<p>${disenio?.descripcionVolumetriaPerformance}</p>
			</g:else>
			</div>
		</div>
	</div>

	<div class="tag-dosDiv">
		<div class="columna1">
			<div>
			<label for='descripcionFuncionalSolucion' >Descripci&oacute;n funcional de la soluci&oacute;n:</label>
			<g:if test="${impactoInstance.puedeEditar(usuarioLogueado)}">
				<g:textArea id="descripcionFuncionalSolucion" name="descripcionFuncionalSolucion" style="width:220px;">${disenio?.descripcionFuncionalSolucion}</g:textArea>
			</g:if>
			<g:else>
				<p>${disenio?.descripcionFuncionalSolucion}</p>
			</g:else>
			</div>
		</div>
		<div class="columna1">	
			<div>
			<label for='principalesCambiosProcesos' >Principales cambios a los procesos:</label>
			<g:if test="${impactoInstance.puedeEditar(usuarioLogueado)}">
				<g:textArea id="principalesCambiosProcesos" name="principalesCambiosProcesos" style="width:220px;">${disenio?.principalesCambiosProcesos}</g:textArea>
			</g:if>
			<g:else>
				<p>${disenio?.principalesCambiosProcesos}</p>
			</g:else>
			</div>
		</div>
	</div>

	<div class="tag-dosDiv">
		<div class="columna1">
			<div>
			<label for='interfacesInvolucradasSolucion' >Interfaces involucradas en la soluci&oacute;n:</label>
			<g:if test="${impactoInstance.puedeEditar(usuarioLogueado)}">
				<g:textArea id="interfacesInvolucradasSolucion" name="interfacesInvolucradasSolucion" style="width:220px;">${disenio?.interfacesInvolucradasSolucion}</g:textArea>
			</g:if>
			<g:else>
				<p>${disenio?.interfacesInvolucradasSolucion}</p>
			</g:else>
			</div>
		</div>
		<div class="columna1">	
			<div>
			<label for='definicionesPendientesWarnings' >Definiciones pendientes:</label>
			<g:if test="${impactoInstance.puedeEditar(usuarioLogueado)}">
				<g:textArea id="definicionesPendientesWarnings" name="definicionesPendientesWarnings" style="width:220px;">${disenio?.definicionesPendientesWarnings}</g:textArea>
			</g:if>
			<g:else>
				<p>${disenio?.definicionesPendientesWarnings}</p>
			</g:else>
			</div>
		</div>
	</div>
	
	<div class="tag-dosDiv">
		<div class="columna1">
			<div>
			<g:if test="${impactoInstance.puedeEditar(usuarioLogueado)}">
				<label for='comboJustificacionDE' >Justificaci&oacute;n por no realizaci&oacute;n de Dise&ntilde;o Externo:</label><br>
				
				<g:select id="comboJustificacionDE" 
				name="justificacionDisenioExterno.id" 
				class="${hasErrors(bean: pedidoInstance, field: 'justificacionDisenioExterno?.id', 'errors')}"
				from="${ar.com.telecom.pcs.entities.JustificacionDisenioExterno.list()}"
				optionKey="id"
				value="${disenio?.justificacionDisenioExterno?.id}"
				noSelection="['null': '']" />
				
				<g:remoteLink title="Limpiar" class="limpiar" before="limpiarComboJustificacion(); return false;" ></g:remoteLink>
				
				<%-- Lo deje, no ejecuta nada del lado del server--%>
			</g:if>
			<g:else>
				<g:if test="${disenio?.justificacionDisenioExterno != null}">
					<label for='comboJustificacionDE' >Justificaci&oacute;n:</label>
					<p class="info" style="padding-bottom:5px">${disenio?.justificacionDisenioExterno}</p>
				</g:if>
			</g:else>
			
			
			</div>
		</div>
		
		<div class="columna1">	
			<div>
					<div class="pDos">
						<g:if test="${impactoInstance.puedeEditar(usuarioLogueado)}">
							<label for='nuevoAnexoDisenioExterno' class="formLabel" style="margin-left: 0px;">Anexos</label>
							<button id="nuevoAnexoDisenioExterno" class="formSeccionSubmitButton">Nuevo anexo</button>
						</g:if>
					</div>
			</div>
		</div>
	</div>

	<g:render template="/templates/tablaAnexos"
		model="['fase': impactoInstance?.pedido.esHijo()?FaseMultiton.ESPECIFICACION_IMPACTO_HIJO:FaseMultiton.CONSOLIDACION_IMPACTO, 
		'editable': impactoInstance.puedeEditar(usuarioLogueado), 'pedidoInstance': impactoInstance?.pedido, 'origen': impactoInstance?.pedido?.disenioExternoActual, 'nombreDiv': 'disenioAnexos', 'impacto': impactoInstance?.id  ]">
   	</g:render>
	
	