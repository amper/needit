<%@page import="ar.com.telecom.util.NumberUtil"%>
<%@page import="ar.com.telecom.util.DateUtil"%>

<g:updateToken/>

<div id="divTareas">	
	<table style="width: 100%" style="border: 0px">
		<tr>
			<th style="width: 30%">Actividad</th>
			<th style="width: 25%">Comentario</th>
			<th style="width: 10%">Desde</th>
			<th style="width: 10%">Hasta</th>
			<th align="right" style="width: 23%">Monto (en Pesos)</th>
			<g:if test="${puedeEditar}">
				<td></td>
			</g:if>
		</tr>
	 
	<!-- SHOW -->
	<g:if test="${!detalles?.isEmpty()}">
		<g:set var="sumaTotalHoras" value="${0}" />
		<g:each in="${detalles}" var="detalle" status="i">
			<tr style="padding: 0px;" class="${(i % 2) == 0 ? 'odd' : 'even'}">
				<td id="fase1">${detalle?.tareaSoporte}</td>
				<td id="fase1">${detalle?.comentarioDetalleAreaSoporte}</td>
				<td>${DateUtil.toString(detalle?.fechaDesde)}</td>
				<td>${DateUtil.toString(detalle?.fechaHasta)}</td>
				
				<% def totalHora = 0; %>
				<g:if test="${detalle?.cantidadHoras && detalle?.valorHora}">
					<% totalHora = detalle?.cantidadHoras * detalle?.valorHora; %>
				</g:if>				
				<td style="text-align: right;">${NumberUtil.toString(totalHora.toString())}</td>
				<g:set var="sumaTotalHoras" value="${sumaTotalHoras + totalHora}" />
				<g:if test="${puedeEditar}">
					<td>
						<g:set var="pedidoOrigen" value="${impactoInstance ? impactoInstance.pedido : pedidoInstance}"/>
						<g:set var="aprobacion" value="${pedidoOrigen?.aprobacionActual(pedidoOrigen?.faseActual, usuarioLogueado)}"/>
						<g:secureRemoteLink action="eliminarTarea" params="['impacto': impacto, 'detalleId': detalle.id, 'aprobacionId': aprobacion?.id]" update="divTareas" >
							<img src="${resource(dir:'images',file:'delete.gif')}" alt="Eliminar" width="10" height="10" />
				  		</g:secureRemoteLink>
				  	</td>
			  	</g:if>
		 	</tr>
		</g:each>
	</g:if>
	<g:else>
		<tr>
			<td colspan="6" style="text-align: center; font-weight: bold; font-style: italic;">No hay Actividades agregadas por el momento...</td>
		</tr>
	</g:else>
	<g:if test="${sumaTotalHoras}">
		<tr>
			<td colspan="6" style="padding:0px;"><hr style="border:none;height:1px;"/></td>
		</tr>
		<tr>
			<td colspan="4" style="text-align: right; font-weight:bold;">Total:&nbsp;</td>
			<td colspan="2" style="font-weight:bold;">${NumberUtil.toString(sumaTotalHoras.toString())}</td>
		</tr>
	</g:if>
	</table>
</div>
