
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta name="layout" content="mainApp" />
<g:javascript library="boxUser" />
<g:javascript library="consolidacionImpacto" />
</head>
<body>

	<div class="contenedorTitulo">
		<g:titulo pedido="${pedidoInstance}" />
		<g:workflow pedidoId="${pedidoInstance?.id}" />
	</div>
 
	<div id="contenedorIzq">
		<g:if test="${flash.message}">
			<div class="message">
				${flash.message}
			</div>
		</g:if>
		<g:hasErrors bean="${pedidoInstance}">
			<div class="errors">
				<g:renderErrors bean="${pedidoInstance}" as="list" />
			</div>
		</g:hasErrors>
		<g:form class="formGeneral" useToken="true">
			<g:hiddenField name="id" value="${pedidoInstance?.id}" />
			<g:cabeceraPedido pedido="${pedidoInstance}" />
			<g:render template="/templates/workflowHidden"/>
			
			<div class="formGeneral">
				<center>
					<div>
						<br/>
						<img src="/needIt/images/underConstruction2.jpg"/>
					</div>
					<div>
						Esta funcionalidad est&aacute;
						prevista para la siguiente iteraci&oacute;n
					</div>
				</center> 
			</div>

			<div id="message"></div>
		</g:form>


	</div>
	<!-- fin contenedor izq -->

	<!-- Lateral Derecho -->
	<g:render template="/templates/menuLateral" model="['pedidoInstance':pedidoInstance]"/>
	<!-- Fin Lateral Derecho -->


	<!-- 
	************
	************
	MODALS POPUP
	************
	************
	************
	-->

	<!-- popup ayuda seccion -->
	<div id="dialog-help" title="Ayuda Aprobar Pedido"
		style="display: none;">
		<div style="height: 350px; width: 100%; overflow: auto;">
			<p class="helpTitle">Seccion ayuda</p>
			<p>Hola esto es una seccion de ayuda Hola esto es una seccion de</p>
		</div>
	</div>
	<!-- fin popup ayuda seccion -->
</body>
</html>
