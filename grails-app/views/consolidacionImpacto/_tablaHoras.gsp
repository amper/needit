<%@page import="ar.com.telecom.util.NumberUtil"%>

<g:updateToken/>

<table style="width: 100%">
		<tr>
<%--			<g:sortableColumn property="grupo" title="Grupo" style="width:50%" />--%>
<%--			<g:sortableColumn property="periodo" title="Periodo" />--%>
<%--			<g:sortableColumn property="horas" title="Horas" />--%>
<%--			<g:sortableColumn property="precioHora" title="Valor hora" />--%>
<%--			<g:sortableColumn property="total" title="Total" />--%>
<%--			<g:sortableColumn property="limpiar" title=" " />--%>
			<th style="width: 50%">Grupo</th>
			<th style="width: 12%">Per&iacute;odo</th>
			<th style="width: 10%; text-align: right;">Horas</th>
			<th style="width: 13%; text-align: right;">Valor hora</th>
			<th style="width: 10%; text-align: right;">Total</th>
			<g:if test="${!show?.toBoolean()}">
				<td style="width: 5%"></td>
			</g:if>
		</tr>

		<g:set var="counter" value="${0}" />
		<%
		def detalleHorasSorted = detalleHoras?.sort{ [it.softwareFactory, it.periodo]}
		 %>
		 
		<g:each in="${detalleHorasSorted}" var="detalleHora">
 		 <tr id="fila${counter}">
			<td>${detalleHora?.softwareFactory?.descripcion}</td>
			<td>${detalleHora?.periodo?.toString()}</td>
			<td style="text-align: right;">${detalleHora?.totalHoras()}</td>
			<td style="text-align: right;">${NumberUtil.setearDecimalesDefaultConComa(detalleHora?.valorHora)}</td>
			<td style="text-align: right;">${NumberUtil.setearDecimalesDefaultConComa(detalleHora?.totalMontoHoras())}</td>
			<g:if test="${!show?.toBoolean()}">
				<td>
				<g:set var="pedidoOrigen" value="${impactoInstance ? impactoInstance.pedido : pedidoInstance}"/>
				<g:set var="aprobacion" value="${pedidoOrigen?.aprobacionActual(pedidoOrigen?.faseActual, usuarioLogueado)}"/>
				<g:secureRemoteLink title="Eliminar carga hora" class="eliminar"
				  update="tablaCargaHoras"
				  params="['cargaHoraId': detalleHora?.id, 'detalleId':detalleId, 'idPlanificacion':idPlanificacion,'pedidoId': impactoInstance?.pedido?.parent?.id, 'impacto':impacto, 'pedidoATrabajar':pedidoATrabajar, 'aprobacionId':aprobacion?.id]"
				  action="eliminaCargaHoraDetalleSistema"
				  onComplete="actualizaFilaDetallePlanificacionSist('${detalleId}','${idPlanificacion}', '${impacto}', '${impactoInstance?.pedido?.parent?.id}');recargaCostoTotal('${impacto}', '${pedidoATrabajar}');"></g:secureRemoteLink></td>
			</g:if>
		</tr>
		<g:set var="counter" value="${counter + 1}" />
		</g:each>
</table>	