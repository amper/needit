<%@page import="ar.com.telecom.pcs.entities.CodigoCierre"%>

<g:updateToken pedido="${impactoInstance?.pedido?.parent}" pedidoHijo="${impactoInstance?.pedido}"/>

<g:if test="${flash.message}">
	<div class="message" id="mensajes">
		${flash.message}
	</div>
</g:if>

<div class="errors" id="errores" style="display: none;"><li>El campo de Observaciones es obligatorio.</li></div>

<g:form id="formReenviar" name="formReenviar" action="reenviar" controller="consolidacionImpacto" useToken="true">
	<g:hiddenField name="impacto" id="impacto" value="${impactoInstance.id}"></g:hiddenField>
	<g:hiddenField name="pedidoId" id="pedidoId" value="${pedido?.id}"></g:hiddenField>
	<div>
		<div>
			<label class="formLabel">Observaciones:</label><br>
			<div align="center"><g:textArea id="observaciones" name="observaciones" rows="3" class="textoComentario" style="padding: 8px;"></g:textArea></div>
		</div>
		<div class="separatorBotonera"></div>
		<div align="center">
			<button id="btnCerrarReenviar" class="formSeccionSubmitButtonAnexo" name="Cerrar" onClick="cerrarPopUpReenviar();">Cerrar</button>&nbsp;
			<button id="btnReenviar" class="formSeccionSubmitButtonAnexo" name="Cerrar" onClick="reenviar(this.form);">Reenviar</button>
<%--			<g:submitToRemoteSecure action="reenviar" value="Reenviar" update="dialog-Reenviar" onComplete="cerrarPopUpReenviar();" class="formSeccionSubmitButtonAnexo"/>--%>
		</div>
	</div>
</g:form> 