<g:updateToken/>

<%@page import="ar.com.telecom.util.NumberUtil"%>
	<script type="text/javascript">
		jQuery(document).ready(
			function($) {
				$("#comboSoftwareFactories").combobox();
				$("#comboPeriodo").combobox();
				<g:if test="${!show}">
					calcularTotal('formCargaHoras' , 'cantidadHoras', 'valorHora', 'labelTotal');
				</g:if>
			});
	</script>
	<g:hiddenField id="detalleId" name="detalleId" value="${detalle?.id}"/>
	<g:if test="${flash.message}">
		<div class="message" style = "width:100%;">
			${flash.message}
		</div>
	</g:if>
 
	<g:hasErrors bean="${pedido}">
		<div class="errors">
			<g:renderErrors bean="${pedido}" as="list" />
		</div>
	</g:hasErrors>
	<div class="flechaNav">
		<g:if test="${!esElPrimero}">
			<g:secureRemoteLink 
				class="flechaIzquierda"
				title="Fase anterior"
				update="bodypopupcargahoras"
				params="['detalles': planificacionActual?.detallesAEditar()?.id, 'idDetalleActual':detalle?.id, 'idPlanificacionActual':planificacionActual?.id, boton:'atras', 'pedidoId':pedidoATrabajar, 'impacto':impacto, 'show': show]"
				action="muevePopCargaHora">&nbsp;
			</g:secureRemoteLink>
		</g:if>
	</div>
	<div class="contenedorPopUp">
			<div class="pDosPopUp pTitle" style="height: auto;">
				<label class="formLabel">Sistema:</label><p id="sistemaSeleccionado"></p>
			</div>
			<script type="javascript">
			    document.getElementById('sistemaSeleccionado').innerHTML = document.getElementById('impacto-input').value;
			</script>
			
			<div class="pDosPopUp pTitle" style="height: auto;">
				<label class="formLabel">Fase:</label><p id="faseSeleccionada">${detalle?.actividadPlanificacion?.descripcion}</p>
			</div>
			
			<g:if test="${!show?.toBoolean()}">
				<div class="pDosPopUp">
					<g:if test="${!impactoInstance.esDeCoordinacion()}">	
						<label class="formLabel">Grupo:</label>
						<g:select id="comboSoftwareFactories" name="softwareFactory.id" onChange="asignarValorHoraDefault('${impacto}');"
							from="${gruposSWF}"
							value="${cargaHora?.softwareFactory?.id}"
							optionKey="id" noSelection="['null': '']" />
					</g:if>
				</div>
			
				<div class="pDosPopUp">
					<label class="formLabel">Per&iacute;odo:</label>
					<g:select id="comboPeriodo" name="periodo.id" onChange="asignarValorHoraDefault('${impacto}');"
						from="${periodos}" 
						value="${cargaHora?.periodo?.id}"
						optionKey="id" noSelection="['null': '']" />
				</div>
			
		
				<div class="separator"></div>
			
				<div class="pTresPopUp pTresPopUpCorto">
						<label class="formLabel">Horas:</label> 
						<g:textField id="cantidadHoras" name="cantidadHoras" maxlength="5" class="inputCorto" type="text" onkeypress='soloNumeros(event,null)' onKeyUp="calculaTotalCargaHoraPop()" value="${cargaHora?.cantidadHoras}"/>
				</div>
				
				<div class="pTresPopUp pTresPopUpCorto">
					<div id="valorDeHora">
						<g:render template="valorHora" />
		<%--				<g:textField id="valorHora" name="valorHora" maxlength="8" class="inputCorto" type="text" onkeypress='soloNumeros(event,true)' onKeyUp="calculaTotalCargaHoraPop()" value="${NumberUtil.seteaComas(cargaHora?.valorHora)}"/>--%>
					</div>
				</div>
				
				<div class="pTresPopUp pTresPopUpCorto">
					<label class="formLabel">$ total:</label>
					<div class="formLabel totalShow" id="labelTotal" style="text-align: center;"></div> 
		<%--			<p class="formLabel totalShow" id="labelTotal" style="text-align: center;"></p>--%>
				</div>		
				
				<div class="separator"></div>
							
				<div id="contenedorBtnAgregar">
					<button id="btnCancelarCargaHoras" class="formSeccionSubmitButtonAnexo" value="Cancelar" name="Cancelar" onClick="cerrarPopUpCargaHoras();">Cerrar</button>
				
					<g:submitToRemoteSecure
						class="formSeccionSubmitButtonAnexo"
						controller="consolidacionImpacto" action="agregaCargaHorasSistemas"	update="bodypopupcargahoras"
						onComplete="actualizaFilaDetallePlanificacionSist('${detalle?.id}','${planificacionActual.id}', '${impacto}', '${pedidoATrabajar}'); recargaCostoTotal('${impacto}', '${pedidoATrabajar}');"  value="Agregar" />
				</div>
			</g:if>
		<div id="tablaCargaHoras">
			<g:render template="tablaHoras" model="['detalleHoras':detalle?.cargaHoras, 'detalleId':detalle?.id, 'idPlanificacion': planificacionActual?.id, 'impacto':impacto, 'pedidoATrabajar':pedidoATrabajar, 'show': show, 'impactoInstance': impactoInstance, 'usuarioLogueado': usuarioLogueado]"></g:render>
		</div>	
		
	</div>
	
	
	<div class="flechaNav">
		<g:if test="${!esElUltimo}">
			<g:secureRemoteLink 
				class="flechaDerecha"
				title="Fase siguiente"
				update="bodypopupcargahoras"
				params="['detalles': planificacionActual?.detallesAEditar()?.id, 'idDetalleActual':detalle?.id, 'idPlanificacionActual':planificacionActual?.id, boton:'adelante', 'pedidoId':pedidoATrabajar, 'impacto':impacto, 'show': show]"
				action="muevePopCargaHora">&nbsp;
			</g:secureRemoteLink>
		</g:if>
	</div>