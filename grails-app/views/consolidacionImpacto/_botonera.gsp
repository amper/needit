<g:updateToken/>
<br/>
<center>
	<g:if test="${impactoInstance.habilitaAcciones(usuarioLogueado)}" >
		<div class="formSeccionButton" style="margin-top: 20px;">
			<g:actionSubmit class="formSeccionSubmitButton" id="Simular"
				action="simularValidacion" value="Simular" />
			<g:actionSubmit class="formSeccionSubmitButton" id="Guardar"
				action="grabarFormulario" value="Guardar" />
			
			<g:if test="${!impactoInstance.pedido.estaSuspendido() }">	
				<g:if test="${impactoInstance.pedido.esHijo() }">
					<%--Si es INTEGER es un pedido HIJO, esos SI se pueden DENEGAR, los padres NO--%>
					<g:actionSubmit class="formSeccionSubmitButton" id="Denegar" 
						action="denegarPedido" value="Denegar" />
				</g:if>
				<g:actionSubmit class="formSeccionSubmitButton" id="Aprobar"
					action="aprobarPedido" value="Aprobar" />
			</g:if>
		</div>
	</g:if>
	<g:else>
		<g:if test="${impactoInstance.habilitaGuardar(usuarioLogueado)}" >
			<g:actionSubmit class="formSeccionSubmitButton" id="Guardar"
				action="grabarFormulario" value="Guardar" />
		</g:if>
	</g:else>
</center> 