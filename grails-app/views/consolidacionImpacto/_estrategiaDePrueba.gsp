<%@ page import="ar.com.telecom.FaseMultiton"%>
<g:updateToken />
<div id="tiposDePrueba">
	<g:render template="tablaTiposPrueba" />
</div>
<br>
	<div class="tag-dosDiv">
		<div class="pDos" style="text-align: center !important;">
			<!--  falta chequear que no sea el padre, en ese caso no deber�as poder agregar? -->
			<g:if test="${impactoInstance.puedeAgregarOtraPrueba(usuarioLogueado) }">
				<button id="nuevaPrueba" class="formSeccionSubmitButton">Agregar prueba opcional</button>
			</g:if>
		</div>
		<div class="pDos" style="text-align: center !important;">
			<g:if test="${impactoInstance.puedeEditar(usuarioLogueado)}">
<%--				<label for='nuevoAnexoEstrategiaPrueba' class="formLabel">Anexos</label>--%>
				<button id="nuevoAnexoEstrategiaPrueba" class="formSeccionSubmitButton">Nuevo anexo</button>
			</g:if>
		</div>
	</div> 
	
		<br>
			<g:render template="/templates/tablaAnexos"
				model="['fase': impactoInstance?.pedido.esHijo()?FaseMultiton.ESPECIFICACION_IMPACTO_HIJO:FaseMultiton.CONSOLIDACION_IMPACTO, 
				'editable': impactoInstance.puedeEditar(usuarioLogueado), 'pedidoInstance': impactoInstance?.pedido, 'origen': impactoInstance?.pedido.estrategiaPruebaActual, 'nombreDiv': 'estrategiaPruebaAnexos', 'impacto': impactoInstance?.id  ]">
   			</g:render>
	<br>
	
	<div>
		<g:if test="${impactoInstance.puedeEditar(usuarioLogueado)}">
			<label style="vertical-align:top;"class="formLabel" for='comentario' >Comentarios:</label>
			<g:textArea id="comentario" name="comentario" style="width:220px;margin-top:0px;">${estrategiaPrueba?.comentario}</g:textArea>
		</g:if>
		<g:else>
			<g:if test="${estrategiaPrueba?.comentario}">
				<label style="vertical-align:top;"class="formLabel" for='comentario' >Comentarios:</label>
				<p class="info">${estrategiaPrueba?.comentario}</p>
			</g:if>
		</g:else>
	</div>
	
	
	
