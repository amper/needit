<g:updateToken />
<%--<div class="demo" >--%>
<%--	<div id="tabs" class="demoTabs">--%>
<%--		<ul>--%>
<%--			<g:if test="${impactoInstance.tienePlanificacionYEsfuerzo()}">--%>
<%--				<li><a href="#tabs-1">Planificaci&oacute;n y esfuerzo</a></li>--%>
<%--			</g:if>--%>
<%--			--%>
<%--			<g:if test="${impactoInstance.tieneEstrategiaPrueba()}">--%>
<%--				<li><a href="#tabs-2">Estrategia de prueba</a></li>--%>
<%--			</g:if>	--%>
<%--			<g:if test="${impactoInstance.tieneDisenioExterno()}">	--%>
<%--				<li><a href="#tabs-3">Dise�o externo</a></li>--%>
<%--			</g:if>--%>
<%--		</ul>--%>

		<g:if test="${impactoInstance.tienePlanificacionYEsfuerzo()}">
			<div id="tabs-1" class="seccionHide3">
				<p class="tituloSeccion">Planificaci&oacute;n y esfuerzo</p>
				<div class="formSeccion" style="background-color: #FFF; text-align: center;">
					<div style="margin: 0px auto; width: 80%">
						<g:render template="planificacionYEsfuerzo"></g:render>
					</div>
				</div>
			</div>
		</g:if>
		 
		<g:if test="${impactoInstance.tieneEstrategiaPrueba()}">		
			<div id="tabs-2" class="seccionHide4">
				<p class="tituloSeccion">Estrategia de prueba</p>
				<div class="formSeccion" style="background-color: #FFF; text-align: center;">
					<div style="margin: 0px auto; width: 90%">
						<g:render template="estrategiaDePrueba" model="['impactoInstance' :impactoInstance]"></g:render>
					</div>
				</div>
			</div>
		</g:if>		
		
		<g:if test="${impactoInstance.tieneDisenioExterno()}">
			<div id="tabs-3" class="seccionHide5">
				<p class="tituloSeccion">Dise&ntilde;o externo</p>
				<div class="formSeccion" style="background-color: #FFF; text-align: center;">
					<div style="margin: 0px auto; width: 80%">
						<g:render template="disenioExterno" model="['disenio':disenioExterno, 'impactoInstance' :impactoInstance]"></g:render>
					</div>
				</div>
			</div>
		</g:if>
<%--	</div>--%>
<%--</div>--%>

