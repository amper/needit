<g:updateToken pedido="${impactoInstance?.pedido?.parent}" pedidoHijo="${impactoInstance?.pedido}"/>

<%--popup Pruebas AGREGAR--%>
<script type="text/javascript">
jQuery(document).ready(
		function($) {
			$("#tipoPrueba").combobox();
		});
</script>
<div id="dialog-ModificaPrueba">
	<g:form name="formPruebasOpc">
	
		<g:hiddenField name="pedidoId" value="${pedidoInstance.id}" />
		<g:hiddenField name="impacto" value="${impacto}" />

		<div id="mensajes">
			<g:if test="${flash.message}">
				<div class="message">
					${flash.message}
				</div>
			</g:if>
		</div>
 
		<g:hasErrors bean="${pedidoATrabajar}">
			<div class="errors">
				<g:renderErrors bean="${pedidoATrabajar}" as="list" />
			</div>
		</g:hasErrors>


		 <div class="pCompleto">
			<label class="formLabel2" for='comboOtrasPruebas'>Otras Pruebas:</label><br>
			<g:select id="tipoPrueba"
				name="tipoPrueba.id" 
				from="${otrasPruebas}" 
				optionKey="id"
				/>
		</div>
	
		<div class="separator"></div>
		
		<div class="pCompleto">
			<label class="formLabel2" for='consolida'>Consolida:</label><br>
			<g:checkBox id="seConsolida" name="seConsolida" class="inputCorto" style="width: 30% !important;" />
		</div>
	
		<div class="separator"></div>

		<div align="center">
			<button id="btnCancelarCargaOtrasPruebas" value="Cancelar" class="formSeccionSubmitButtonAnexo" name="Cancelar" onClick="cerrarPopUpCargaOtrasPruebas();">Cerrar</button>
			&nbsp;
			<g:submitToRemoteSecure class="formSeccionSubmitButtonAnexo"  
					action="agregarOtraPrueba" update="dialog-NuevaPrueba" onComplete="recargaTablaOtrasPruebas('${estrategiaPrueba?.id}', '${impacto}', '${pedidoInstance?.id}')"
					value="Agregar" />
		</div>
	</g:form>
</div>
<!--	fin popup-->
