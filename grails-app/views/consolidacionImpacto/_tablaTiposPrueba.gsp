<g:updateToken />
<div id="tablaPruebasCompletas" style="width: 100%;">
<g:if test="${(pedidoHijo.id.equals(pedidoInstance.id) && pedidoInstance.getPedidosHijosEspecificados()) || !pedidoHijo.id.equals(pedidoInstance.id)}">
	<g:if test="${impactoInstance.puedeEditar(usuarioLogueado)}">
		<g:hiddenField name="cargaEstrategiaPruebas" value="S"/>
	</g:if>
	<g:else>	
		<g:hiddenField name="cargaEstrategiaPruebas" value="N"/>
	</g:else>
	<table class="TablaConsolidacionImpactoHeader" cellspacing="0" cellpadding="0">
	<tr>
		<th rowspan="2"><center><b>Tipo</b></center></th>
		<th colspan="2">Estrategia padre</th>
	    <TH colspan="4" style="padding-right:0px !important;">Estrategia hijo</th>
	</tr>
	
	<tr>
		<th>&iquest;Realiza prueba?</th>
		<th>&iquest;Prueba consolidada?</th>
		<th>&iquest;Realiza prueba?</th>
		<th>&iquest;Prueba consolidada?</th>
		<th style="padding:0px !important;">Justificaci&oacute;n de la no prueba</th>
		<th style="width:5%;">&nbsp;</th>
	</tr> 
	<g:each in="${estrategiaPrueba?.getDetallesConsolidados(pedidoInstance)}" var="detalle" status="i">
		<g:if test="${impactoInstance.puedeEditar(usuarioLogueado)}">
			<g:divsTiposPrueba id="${detalle.id}" clase="${(i % 2) == 0 ? 'odd' : 'even'}" detalle="${detalle}" pedido="${pedidoInstance}" pedidoHijo="${pedidoHijo}" impactoInstance="${impactoInstance}" usuarioLogueado="${usuarioLogueado}"></g:divsTiposPrueba>
		</g:if>
		<g:else>
			<g:divsTiposPruebaSinCheck id="${detalle.id}" clase="${(i % 2) == 0 ? 'odd' : 'even'}" detalle="${detalle}" pedido="${pedidoInstance}" pedidoHijo="${pedidoHijo}" impactoInstance="${impactoInstance}" usuarioLogueado="${usuarioLogueado}"></g:divsTiposPruebaSinCheck>
		</g:else>
	</g:each>
	</table>
</g:if>
<g:else>
	<br>
	<div class="msgPlanificacionEsf">
		Todav&iacute;a no hay informaci&oacute;n suficiente para armar el consolidado de la estrategia de prueba
	</div>
</g:else>

</div>