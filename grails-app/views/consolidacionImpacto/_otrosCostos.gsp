<g:updateToken pedido="${impactoInstance?.pedido?.parent}" pedidoHijo="${impactoInstance?.pedido}"/>

<div id="cargaOtrosCostos">
	<g:form id="formOtrosCostos" name="formOtrosCostos">
		<g:hiddenField name="pedidoId" value="${pedido.id}" />
		<g:hiddenField name="impacto" value="${impacto}" />

		<div class="separator"></div>
		
		<div id="mensajes">
			<g:if test="${flash.message}">
				<div class="message">
					${flash.message}
				</div>
			</g:if>
		</div>

		<g:hasErrors bean="${pedidoATrabajar}">
			<div class="errors">
				<g:renderErrors bean="${pedidoATrabajar}" as="list" />
			</div>
		</g:hasErrors>
		 
		<div class="formSeccion">
			<div class="pCompleto" id="contenedorTipoCosto">
				<g:render template="comboTiposCostos" model="['tiposCosto': tiposCosto]">
				</g:render>
	
			</div>
			
			<div class="separator"></div>
			
			<div class="pCompleto">
				<label class="formLabel2">Detalle:</label><br>
				<g:textField id="detalle" name="detalle" type="text" value="${otroCosto?.detalle}" style="width: 50%;"/>
			</div>
			
			<div class="separator"></div>
			
			<div class="pCompleto">
				<label class="formLabel2" style="width: 150px;">Monto (en Pesos):</label><br>
				<g:textField id="costo" name="costo" type="text" value="${otroCosto?.costo}" style="width: 80px;" onkeypress='soloNumeros(event,true)'/>
			</div>
			
			<div class="separator"></div>
			
			<div align="center">
				<button id="cierraPop" onClick="cerrarPopUpOtrosCostos();" class="formSeccionSubmitButtonAnexo">Cerrar</button>
				&nbsp;	
				<g:submitToRemoteSecure
					class="formSeccionSubmitButtonAnexo"
					style="margin-right:10px;"
					controller="consolidacionImpacto" action="agregarOtroCosto"
					update="cargaOtrosCostos" onComplete="recargaOtrosCostos('${pedido.id}', '${impacto}'); recargaCostoTotal('${impacto}', '${pedidoATrabajar.id}');" after="new Ajax.Updater('contenedorTipoCosto','/' + gNameApp + '/consolidacionImpacto/recargarComboCostos',{asynchronous:true,evalScripts:true,parameters:'pedidoId=${pedido.id}&impacto=${impacto}'});return false;" value="Agregar" />
			</div>
		</div>
	</g:form>
</div>	