<%@page import="ar.com.telecom.util.NumberUtil"%>
<%@page import="ar.com.telecom.pedido.especificacionImpacto.Impacto"%>

<g:updateToken/>
<td id="fase1">
	${detalle.actividadPlanificacion.descripcion}
</td>

<g:set var="pedidoOrigen" value="${impactoInstance ? impactoInstance.pedido : pedidoInstance}"/>
<g:set var="aprobacion" value="${pedidoOrigen?.aprobacionActual(pedidoOrigen?.faseActual, usuarioLogueado)}"/>

<td>
	<g:textField id="desde_${detalle.id}" class="datePicker"
	name="desde_${detalle.id}" style="background-image:none !important;"
	onChange="validaFechasCompletas('${detalle.id}')" />
<%--		onClick="showDatePicker('desde','${detalle.id}',true)" /> --%>
	<g:hiddenField
		name="fdesde_${detalle.id}" id="fdesde_${detalle.id}"
		value="${ar.com.telecom.util.DateUtil.toString(detalle.fechaDesde)}" />
</td>
<td>
 
	<g:textField id="hasta_${detalle.id}" class="datePicker"
		name="hasta_${detalle.id}" style="background-image:none !important;"
		onChange="validaFechasCompletas('${detalle.id}')" />
<%--		onClick="showDatePicker('hasta','${detalle.id}',true)" /> --%>
	<g:hiddenField
		name="fhasta_${detalle.id}" id="fhasta_${detalle.id}"
		value="${ar.com.telecom.util.DateUtil.toString(detalle.fechaHasta)}" />
</td>
<td style="width: 70px;"><g:if test="${detalle.puedeCargarHoras()}">
		<label id="horas_${detalle.id}" style="width: 45px; float: left; text-align: right;">
			${detalle?.totalHoras()} 
		</label>
		<input id="agregaHoras_${detalle.id}" class="agregaHoras"
			title="Cargar horas" type="button"
			onclick="cargarHorasSistemaPopup('fase1','${impactoInstance?.pedido?.sistema}', '${detalle.id}', '${planificacionActual?.id }','${impacto}','${impactoInstance?.pedido?.parent?.id}', '${aprobacion?.id }');" />
	</g:if> 
	<g:else>
		<label id="horas_${detalle.id}"
			style="display: none; width: 45px; float: left; text-align: right;"> ${detalle?.totalHoras()}
		</label>
		
		<input id="agregaHoras_${detalle.id}" class="agregaHoras"
			title="Cargar horas" type="button"
			onclick="cargarHorasSistemaPopup('fase1','${impactoInstance?.pedido?.sistema}','${detalle.id}','${planificacionActual.id}','${impacto}','${impactoInstance?.pedido?.parent?.id}', '${aprobacion?.id }');"
			style="display: none;" />
	</g:else>
</td>
<td style="text-align: right;">
	<g:if test="${detalle.puedeCargarHoras()}">
		${NumberUtil.setearDecimalesDefaultConComa(detalle.totalMontoHoras())}
	</g:if>
</td>
<td>
<%--	<g:set var="pedidoOrigen" value="${impactoInstance ? impactoInstance.pedido : pedidoInstance}"/>--%>
<%--	<g:set var="aprobacion" value="${pedidoOrigen?.aprobacionPendiente(pedidoOrigen?.faseActual, usuarioLogueado)}"/>--%>
	<g:secureRemoteLink title="Limpiar" class="limpiar"
		update="fila_${detalle.id}" params="['detalleId':detalle.id, 'impacto':impacto, 'pedidoId':impactoInstance?.pedido?.id, 'aprobacionId': aprobacion?.id]"
		action="limpiaDetallePlanificacion" onComplete="recargaCostoTotal('${impacto}', '${impactoInstance?.pedido?.id}');" />
</td>

<script type="text/javascript">
		jQuery(document).ready(
			function($) {
				$("#desde_" +${detalle.id}).glDatePicker({
					position: "relative",
					onChange: function(target, newDate)
				    {
						validaFechasCompletas('${detalle.id}');
						var day = newDate.getDate();
						var month = (newDate.getMonth() + 1);
						if (day < 10){
							day = "0" + day;
						}
						if (month < 10){
							month = "0" + month;
						}
						target.val
				        (
				            day + "/" +
				            month + "/" +
				            newDate.getFullYear() 
				        );
				    }
				});
				$("#hasta_" +${detalle.id}).glDatePicker({
					position: "relative",
					onChange: function(target, newDate)
				    {
						validaFechasCompletas('${detalle.id}');
						var day = newDate.getDate();
						var month = (newDate.getMonth() + 1);
						if (day < 10){
							day = "0" + day;
						}
						if (month < 10){
							month = "0" + month;
						}
						target.val
				        (
				            day + "/" +
				            month + "/" +
				            newDate.getFullYear() 
				        );
				    }
				});
				document.getElementById('desde_'+${detalle.id}).value = document.getElementById('fdesde_'+${detalle.id}).value;
				document.getElementById('hasta_'+${detalle.id}).value = document.getElementById('fhasta_'+${detalle.id}).value;
			});
</script>
