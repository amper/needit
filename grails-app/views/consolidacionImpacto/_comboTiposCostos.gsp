<g:updateToken />
<script type="text/javascript">
	jQuery(document).ready(
		function($) {
			$("#comboTipoCosto").combobox();
		});
</script>

<label class="formLabel2">Tipo de costo:</label>
<br>
<g:select
	id="comboTipoCosto"
	name="tipoCosto.id"
	from="${tiposCosto}"
	value="${otroCosto?.tipoCosto?.id}"
	optionKey="id"
/> 