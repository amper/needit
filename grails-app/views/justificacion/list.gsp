
<%@ page import="ar.com.telecom.pcs.entities.Justificacion" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="layout" content="main" />
        <g:set var="entityName" value="${message(code: 'justificacion.label', default: 'Justificacion')}" />
        <title><g:message code="default.list.label" args="[entityName]" /></title>
    </head>
    <body>
        <div class="nav">
            <span class="menuButton"><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></span>
            <span class="menuButton"><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></span>
        </div>
        <div class="body">
            <h1><g:message code="default.list.label" args="[entityName]" /></h1>
            <g:if test="${flash.message}">
            <div class="message">${flash.message}</div>
            </g:if>
            <div class="list">
                <table>
                    <thead>
                        <tr>
                        
                            <g:sortableColumn property="id" title="${message(code: 'justificacion.id.label', default: 'Id')}" />
                        
                            <g:sortableColumn property="descripcion" title="${message(code: 'justificacion.descripcion.label', default: 'Descripcion')}" />
                        
                            <th><g:message code="justificacion.tipoPrueba.label" default="Tipo Prueba" /></th>
                        
                        </tr>
                    </thead>
                    <tbody>
                    <g:each in="${justificacionInstanceList}" status="i" var="justificacionInstance">
                        <tr class="${(i % 2) == 0 ? 'odd' : 'even'}">
                        
                            <td><g:link action="show" id="${justificacionInstance.id}">${fieldValue(bean: justificacionInstance, field: "id")}</g:link></td>
                        
                            <td>${fieldValue(bean: justificacionInstance, field: "descripcion")}</td>
                        
                            <td>${fieldValue(bean: justificacionInstance, field: "tipoPrueba")}</td>
                        
                        </tr>
                    </g:each>
                    </tbody>
                </table>
            </div>
            <div class="paginateButtons">
                <g:paginate total="${justificacionInstanceTotal}" />
            </div>
        </div>
    </body>
</html>
