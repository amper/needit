

<%@ page import="ar.com.telecom.pcs.entities.AreaSoporte" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="layout" content="main" />
        <g:set var="entityName" value="${message(code: 'areaSoporte.label', default: 'AreaSoporte')}" />
        <title><g:message code="default.create.label" args="[entityName]" /></title>
    </head>
    <body>
        <div class="nav">
            <span class="menuButton"><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></span>
            <span class="menuButton"><g:link class="list" action="list"><g:message code="default.list.label" args="[entityName]" /></g:link></span>
        </div>
        <div class="body">
            <h1><g:message code="default.create.label" args="[entityName]" /></h1>
            <g:if test="${flash.message}">
            <div class="message">${flash.message}</div>
            </g:if>
            <g:hasErrors bean="${areaSoporteInstance}">
            <div class="errors">
                <g:renderErrors bean="${areaSoporteInstance}" as="list" />
            </div>
            </g:hasErrors>
            <g:form action="save" >
                <div class="dialog">
                    <table>
                        <tbody>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="descripcion"><g:message code="areaSoporte.descripcion.label" default="Descripcion" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: areaSoporteInstance, field: 'descripcion', 'errors')}">
                                    <g:textField name="descripcion" maxlength="80" value="${areaSoporteInstance?.descripcion}" />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="grupoAdministrador"><g:message code="areaSoporte.grupoAdministrador.label" default="Grupo Administrador" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: areaSoporteInstance, field: 'grupoAdministrador', 'errors')}">
                                    <g:textField name="grupoAdministrador" maxlength="100" value="${areaSoporteInstance?.grupoAdministrador}" />
                                </td>
                            </tr>
                        
                        </tbody>
                    </table>
                </div>
                <div class="buttons">
                    <span class="button"><g:submitButton name="create" class="save" value="${message(code: 'default.button.create.label', default: 'Create')}" /></span>
                </div>
            </g:form>
        </div>
    </body>
</html>
