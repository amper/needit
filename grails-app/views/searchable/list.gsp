
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta name="layout" content="mainApp" />
<g:javascript library="boxUser" />
</head>
<body>
	<div class="contenedorTitulo">
		<p class="tituloPrincipal">Resultados de la b&uacute;squeda</p>
	</div>
	<div id="contenedorIzq">
		<g:if test="${flash.message}">
			<div class="errors">
					<li>${flash.message}</li>
			</div>
		</g:if>

		<div class="list"> 
			<table>
				<thead>
					<tr>
						<g:sortableColumn property="id"
							title="N&uacute;mero" params="${params}" />
							
						<g:sortableColumn property="titulo"
							title="T&iacute;tulo" />

						<g:sortableColumn property="macroEstado"
							title="Macroestado" />

						<g:sortableColumn property="faseActual"
							title="Fase" />
							
						<g:sortableColumn property="legajoInterlocutorUsuario"
							title="Interlocutor Usuario" />



					</tr>
				</thead>
				<tbody>
					<g:each in="${pedidos}" status="i" var="elem">
						<g:link controller="${elem?.faseActual?.controllerNombre}" id="${elem.id}" params="${['pedidoId': elem.id]}">
							<tr class="${(i % 2) == 0 ? 'odd' : 'even'}"
								style="cursor: pointer;">

								<td>
									${elem.id}
								</td>

								<td>
									${elem.titulo}
								</td>

								<td>
									${elem?.faseActual?.macroEstado}
								</td>

								<td>
									${elem?.faseActual}
								</td>
								<td>
									<g:userDescription legajo="${elem?.legajoInterlocutorUsuario}" pedido="${elem?.id }"/>
								</td>

							</tr>
						</g:link>
					</g:each>
				</tbody>
			</table>
		</div>
		<div class="paginateButtons">
			<g:paginate total="${pedidosTotal}" id="${textoBusqueda}"/>
		</div>
	
	</div> <!-- fin contenedor izq -->
	<div id="contenedorDcho">
		<div class="menuContenedor" id="ocultarPanel">
			<img src="${resource(dir:'images',file:'ocultarPanel.png')}" />
		</div>
		<div class="tituloContenedor">Favoritos</div>
		<div class="contenidoContenedor" style="height: 130px;">
			<div class="itemContenedor">
				<g:link controller="#">
					<span class="itemTitulo">Req 24000</span>
				</g:link>
				<span class="itemEstado">En documentacion</span>
				<p class="itemDescripcion">Desconexion del AS400</p>
			</div>
			<div class="itemContenedor">
				<g:link controller="#">
					<span class="itemTitulo">Req 24000</span>
				</g:link>
				<span class="itemEstado">En documentacion</span>
				<p class="itemDescripcion">Desconexion del AS400</p>
			</div>
			<div class="itemContenedor">
				<g:link controller="#">
					<span class="itemTitulo">Req 24000</span>
				</g:link>
				<span class="itemEstado">En documentacion</span>
				<p class="itemDescripcion">Desconexion del AS400</p>
			</div>
		</div>

		

	</div>
	<!-- fin contenedor derecho -->
	<div id="contenedorDchoMin">
		<div class="menuContenedor" id="mostrarPanel">
			<img src="${resource(dir:'images',file:'abrirPanel.png')}" />
		</div>
		<img class="titlePanel"
			src="${resource(dir:'images',file:'menuLateral.png')}" />
	</div>
</body>
</html>
