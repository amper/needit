
<%@ page import="ar.com.telecom.pcs.entities.UmbralAprobacionEconomica" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="layout" content="main" />
        <g:set var="entityName" value="${message(code: 'umbralAprobacionEconomica.label', default: 'UmbralAprobacionEconomica')}" />
        <title><g:message code="default.list.label" args="[entityName]" /></title>
    </head>
    <body>
        <div class="nav">
            <span class="menuButton"><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></span>
            <span class="menuButton"><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></span>
        </div>
        <div class="body">
            <h1><g:message code="default.list.label" args="[entityName]" /></h1>
            <g:if test="${flash.message}">
            <div class="message">${flash.message}</div>
            </g:if>
            <div class="list">
                <table>
                    <thead>
                        <tr>
                        
                            <g:sortableColumn property="id" title="${message(code: 'umbralAprobacionEconomica.id.label', default: 'Id')}" />
                        
                            <g:sortableColumn property="rangoDesde" title="${message(code: 'umbralAprobacionEconomica.rangoDesde.label', default: 'Rango Desde')}" />
                        
                            <g:sortableColumn property="rangoHasta" title="${message(code: 'umbralAprobacionEconomica.rangoHasta.label', default: 'Rango Hasta')}" />
                        
                            <th><g:message code="umbralAprobacionEconomica.tipoGestion.label" default="Tipo Gestion" /></th>
                        
                        </tr>
                    </thead>
                    <tbody>
                    <g:each in="${umbralAprobacionEconomicaInstanceList}" status="i" var="umbralAprobacionEconomicaInstance">
                        <tr class="${(i % 2) == 0 ? 'odd' : 'even'}">
                        
                            <td><g:link action="show" id="${umbralAprobacionEconomicaInstance.id}">${fieldValue(bean: umbralAprobacionEconomicaInstance, field: "id")}</g:link></td>
                        
                            <td>${fieldValue(bean: umbralAprobacionEconomicaInstance, field: "rangoDesde")}</td>
                        
                            <td>${fieldValue(bean: umbralAprobacionEconomicaInstance, field: "rangoHasta")}</td>
                        
                            <td>${fieldValue(bean: umbralAprobacionEconomicaInstance, field: "tipoGestion")}</td>
                        
                        </tr>
                    </g:each>
                    </tbody>
                </table>
            </div>
            <div class="paginateButtons">
                <g:paginate total="${umbralAprobacionEconomicaInstanceTotal}" />
            </div>
        </div>
    </body>
</html>
