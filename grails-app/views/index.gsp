<html>
<head>
<meta name="layout" content="layoutLogin" />
</head>
<body>
	<web:isBlackberry>
		<g:link class="link-entrar" controller="inboxMobile" action="index">
			<img src="${resource(dir:'mobile',file:'mobile-btn-entrar.png')}" />
		</g:link>
	</web:isBlackberry>

	<web:isNOTBlackberry> 
		<web:isNOTMsie>
			<div class="contenidoError">Ingrese por navegador corporativo Internet Explorer</div>
		</web:isNOTMsie>
		<web:isMsie>
			<g:if test="${session.message}">
				<div class="message">
					${session.message}
				</div>
			</g:if>
			<div id="Login">
				<p style="margin-top: 65px; margin-left: 150px; text-align: right;">
					<g:link class="indexEntrar" controller="inbox" action="index">
						<img src="${resource(dir:'images',file:'entrar.png')}" />
						<span>Entrar</span>
					</g:link>
				</p>
			</div>
		</web:isMsie>
	</web:isNOTBlackberry>
</body>
</html>
