
<%@ page import="ar.com.telecom.pcs.entities.ParametrosSistema" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="layout" content="main" />
        <g:set var="entityName" value="${message(code: 'parametrosSistema.label', default: 'ParametrosSistema')}" />
        <title><g:message code="default.list.label" args="[entityName]" /></title>
    </head>
    <body>
        <div class="nav">
            <span class="menuButton"><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></span>
            <span class="menuButton"><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></span>
        </div>
        <div class="body">
            <h1><g:message code="default.list.label" args="[entityName]" /></h1>
            <g:if test="${flash.message}">
            <div class="message">${flash.message}</div>
            </g:if>
            <div class="list">
                <table>
                    <thead>
                        <tr>
                        
                            <g:sortableColumn property="id" title="${message(code: 'parametrosSistema.id.label', default: 'Id')}" />
                        
                            <g:sortableColumn property="grupoLDAPUsuarioFinal" title="${message(code: 'parametrosSistema.grupoLDAPUsuarioFinal.label', default: 'Grupo LDAPU suario Final')}" />
                        
                            <g:sortableColumn property="grupoLDAPInterlocutorUsuario" title="${message(code: 'parametrosSistema.grupoLDAPInterlocutorUsuario.label', default: 'Grupo LDAPI nterlocutor Usuario')}" />
                        
                            <g:sortableColumn property="grupoLDAPFuncionGestionDemandaGral" title="${message(code: 'parametrosSistema.grupoLDAPFuncionGestionDemandaGral.label', default: 'Grupo LDAPF uncion Gestion Demanda Gral')}" />
                        
                            <g:sortableColumn property="grupoLDAPCoordinadorPedido" title="${message(code: 'parametrosSistema.grupoLDAPCoordinadorPedido.label', default: 'Grupo LDAPC oordinador Pedido')}" />
                        
                            <g:sortableColumn property="grupoAdmin" title="${message(code: 'parametrosSistema.grupoAdmin.label', default: 'Grupo Admin')}" />
                        
                        </tr>
                    </thead>
                    <tbody>
                    <g:each in="${parametrosSistemaInstanceList}" status="i" var="parametrosSistemaInstance">
                        <tr class="${(i % 2) == 0 ? 'odd' : 'even'}">
                        
                            <td><g:link action="show" id="${parametrosSistemaInstance.id}">${fieldValue(bean: parametrosSistemaInstance, field: "id")}</g:link></td>
                        
                            <td>${fieldValue(bean: parametrosSistemaInstance, field: "grupoLDAPUsuarioFinal")}</td>
                        
                            <td>${fieldValue(bean: parametrosSistemaInstance, field: "grupoLDAPInterlocutorUsuario")}</td>
                        
                            <td>${fieldValue(bean: parametrosSistemaInstance, field: "grupoLDAPFuncionGestionDemandaGral")}</td>
                        
                            <td>${fieldValue(bean: parametrosSistemaInstance, field: "grupoLDAPCoordinadorPedido")}</td>
                        
                            <td>${fieldValue(bean: parametrosSistemaInstance, field: "grupoAdmin")}</td>
                        
                        </tr>
                    </g:each>
                    </tbody>
                </table>
            </div>
            <div class="paginateButtons">
                <g:paginate total="${parametrosSistemaInstanceTotal}" />
            </div>
        </div>
    </body>
</html>
