

<%@ page import="ar.com.telecom.pcs.entities.ParametrosSistema" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="layout" content="main" />
        <g:set var="entityName" value="${message(code: 'parametrosSistema.label', default: 'ParametrosSistema')}" />
        <title><g:message code="default.edit.label" args="[entityName]" /></title>
    </head>
    <body>
        <div class="nav">
            <span class="menuButton"><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></span>
            <span class="menuButton"><g:link class="list" action="list"><g:message code="default.list.label" args="[entityName]" /></g:link></span>
            <span class="menuButton"><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></span>
        </div>
        <div class="body">
            <h1><g:message code="default.edit.label" args="[entityName]" /></h1>
            <g:if test="${flash.message}">
            <div class="message">${flash.message}</div>
            </g:if>
            <g:hasErrors bean="${parametrosSistemaInstance}">
            <div class="errors">
                <g:renderErrors bean="${parametrosSistemaInstance}" as="list" />
            </div>
            </g:hasErrors>
            <g:form method="post" >
                <g:hiddenField name="id" value="${parametrosSistemaInstance?.id}" />
                <g:hiddenField name="version" value="${parametrosSistemaInstance?.version}" />
                <div class="dialog">
                    <table>
                        <tbody>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                  <label for="grupoLDAPUsuarioFinal"><g:message code="parametrosSistema.grupoLDAPUsuarioFinal.label" default="Grupo LDAPU suario Final" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: parametrosSistemaInstance, field: 'grupoLDAPUsuarioFinal', 'errors')}">
                                    <g:textField name="grupoLDAPUsuarioFinal" maxlength="100" value="${parametrosSistemaInstance?.grupoLDAPUsuarioFinal}" />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                  <label for="grupoLDAPInterlocutorUsuario"><g:message code="parametrosSistema.grupoLDAPInterlocutorUsuario.label" default="Grupo LDAPI nterlocutor Usuario" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: parametrosSistemaInstance, field: 'grupoLDAPInterlocutorUsuario', 'errors')}">
                                    <g:textField name="grupoLDAPInterlocutorUsuario" maxlength="100" value="${parametrosSistemaInstance?.grupoLDAPInterlocutorUsuario}" />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                  <label for="grupoLDAPFuncionGestionDemandaGral"><g:message code="parametrosSistema.grupoLDAPFuncionGestionDemandaGral.label" default="Grupo LDAPF uncion Gestion Demanda Gral" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: parametrosSistemaInstance, field: 'grupoLDAPFuncionGestionDemandaGral', 'errors')}">
                                    <g:textField name="grupoLDAPFuncionGestionDemandaGral" maxlength="100" value="${parametrosSistemaInstance?.grupoLDAPFuncionGestionDemandaGral}" />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                  <label for="grupoLDAPCoordinadorPedido"><g:message code="parametrosSistema.grupoLDAPCoordinadorPedido.label" default="Grupo LDAPC oordinador Pedido" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: parametrosSistemaInstance, field: 'grupoLDAPCoordinadorPedido', 'errors')}">
                                    <g:textField name="grupoLDAPCoordinadorPedido" maxlength="100" value="${parametrosSistemaInstance?.grupoLDAPCoordinadorPedido}" />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                  <label for="grupoAdmin"><g:message code="parametrosSistema.grupoAdmin.label" default="Grupo Admin" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: parametrosSistemaInstance, field: 'grupoAdmin', 'errors')}">
                                    <g:textField name="grupoAdmin" maxlength="100" value="${parametrosSistemaInstance?.grupoAdmin}" />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                  <label for="tooltipNotasDesarrollo"><g:message code="parametrosSistema.tooltipNotasDesarrollo.label" default="Tooltip Notas Desarrollo" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: parametrosSistemaInstance, field: 'tooltipNotasDesarrollo', 'errors')}">
                                    <g:textField name="tooltipNotasDesarrollo" maxlength="250" value="${parametrosSistemaInstance?.tooltipNotasDesarrollo}" />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                  <label for="cartelAdvertenciaGerentesIT"><g:message code="parametrosSistema.cartelAdvertenciaGerentesIT.label" default="Cartel Advertencia Gerentes IT" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: parametrosSistemaInstance, field: 'cartelAdvertenciaGerentesIT', 'errors')}">
                                    <g:textArea name="cartelAdvertenciaGerentesIT" cols="40" rows="5" value="${parametrosSistemaInstance?.cartelAdvertenciaGerentesIT}" />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                  <label for="mensajeAdvertenciaValidar"><g:message code="parametrosSistema.mensajeAdvertenciaValidar.label" default="Mensaje Advertencia Validar" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: parametrosSistemaInstance, field: 'mensajeAdvertenciaValidar', 'errors')}">
                                    <g:textField name="mensajeAdvertenciaValidar" value="${parametrosSistemaInstance?.mensajeAdvertenciaValidar}" />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                  <label for="mailEmisorPCS"><g:message code="parametrosSistema.mailEmisorPCS.label" default="Mail Emisor PCS" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: parametrosSistemaInstance, field: 'mailEmisorPCS', 'errors')}">
                                    <g:textField name="mailEmisorPCS" value="${parametrosSistemaInstance?.mailEmisorPCS}" />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                  <label for="mailAdministradorFuncional"><g:message code="parametrosSistema.mailAdministradorFuncional.label" default="Mail Administrador Funcional" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: parametrosSistemaInstance, field: 'mailAdministradorFuncional', 'errors')}">
                                    <g:textField name="mailAdministradorFuncional" value="${parametrosSistemaInstance?.mailAdministradorFuncional}" />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                  <label for="tiempoActualizacionInbox"><g:message code="parametrosSistema.tiempoActualizacionInbox.label" default="Tiempo Actualizacion Inbox" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: parametrosSistemaInstance, field: 'tiempoActualizacionInbox', 'errors')}">
                                    <g:textField name="tiempoActualizacionInbox" value="${fieldValue(bean: parametrosSistemaInstance, field: 'tiempoActualizacionInbox')}" />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                  <label for="tiempoActualizacionInboxAprobaciones"><g:message code="parametrosSistema.tiempoActualizacionInboxAprobaciones.label" default="Tiempo Actualizacion Inbox Aprobaciones" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: parametrosSistemaInstance, field: 'tiempoActualizacionInboxAprobaciones', 'errors')}">
                                    <g:textField name="tiempoActualizacionInboxAprobaciones" value="${fieldValue(bean: parametrosSistemaInstance, field: 'tiempoActualizacionInboxAprobaciones')}" />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                  <label for="repeticionHoras"><g:message code="parametrosSistema.repeticionHoras.label" default="Repeticion Horas" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: parametrosSistemaInstance, field: 'repeticionHoras', 'errors')}">
                                    <g:textField name="repeticionHoras" value="${fieldValue(bean: parametrosSistemaInstance, field: 'repeticionHoras')}" />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                  <label for="notaReporteAprobacionImpacto"><g:message code="parametrosSistema.notaReporteAprobacionImpacto.label" default="Nota Reporte Aprobacion Impacto" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: parametrosSistemaInstance, field: 'notaReporteAprobacionImpacto', 'errors')}">
                                    <g:textField name="notaReporteAprobacionImpacto" value="${parametrosSistemaInstance?.notaReporteAprobacionImpacto}" />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                  <label for="auditaMails"><g:message code="parametrosSistema.auditaMails.label" default="Audita Mails" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: parametrosSistemaInstance, field: 'auditaMails', 'errors')}">
                                    <g:checkBox name="auditaMails" value="${parametrosSistemaInstance?.auditaMails}" />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                  <label for="enviaMails"><g:message code="parametrosSistema.enviaMails.label" default="Envia Mails" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: parametrosSistemaInstance, field: 'enviaMails', 'errors')}">
                                    <g:checkBox name="enviaMails" value="${parametrosSistemaInstance?.enviaMails}" />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                  <label for="maximoJerarquiaAprobador"><g:message code="parametrosSistema.maximoJerarquiaAprobador.label" default="Maximo Jerarquia Aprobador" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: parametrosSistemaInstance, field: 'maximoJerarquiaAprobador', 'errors')}">
                                    <g:textField name="maximoJerarquiaAprobador" value="${fieldValue(bean: parametrosSistemaInstance, field: 'maximoJerarquiaAprobador')}" />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                  <label for="maximoJerarquiaGerente"><g:message code="parametrosSistema.maximoJerarquiaGerente.label" default="Maximo Jerarquia Gerente" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: parametrosSistemaInstance, field: 'maximoJerarquiaGerente', 'errors')}">
                                    <g:textField name="maximoJerarquiaGerente" value="${fieldValue(bean: parametrosSistemaInstance, field: 'maximoJerarquiaGerente')}" />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                  <label for="minimoJerarquiaAprobador"><g:message code="parametrosSistema.minimoJerarquiaAprobador.label" default="Minimo Jerarquia Aprobador" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: parametrosSistemaInstance, field: 'minimoJerarquiaAprobador', 'errors')}">
                                    <g:textField name="minimoJerarquiaAprobador" value="${fieldValue(bean: parametrosSistemaInstance, field: 'minimoJerarquiaAprobador')}" />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                  <label for="minimoJerarquiaGerente"><g:message code="parametrosSistema.minimoJerarquiaGerente.label" default="Minimo Jerarquia Gerente" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: parametrosSistemaInstance, field: 'minimoJerarquiaGerente', 'errors')}">
                                    <g:textField name="minimoJerarquiaGerente" value="${fieldValue(bean: parametrosSistemaInstance, field: 'minimoJerarquiaGerente')}" />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                  <label for="tipoGestionDefault"><g:message code="parametrosSistema.tipoGestionDefault.label" default="Tipo Gestion Default" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: parametrosSistemaInstance, field: 'tipoGestionDefault', 'errors')}">
                                    <g:select name="tipoGestionDefault.id" from="${ar.com.telecom.pcs.entities.TipoGestion.list()}" optionKey="id" value="${parametrosSistemaInstance?.tipoGestionDefault?.id}"  />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                  <label for="valorHoraSWFTecoDefault"><g:message code="parametrosSistema.valorHoraSWFTecoDefault.label" default="Valor Hora SWFT eco Default" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: parametrosSistemaInstance, field: 'valorHoraSWFTecoDefault', 'errors')}">
                                    <g:textField name="valorHoraSWFTecoDefault" value="${fieldValue(bean: parametrosSistemaInstance, field: 'valorHoraSWFTecoDefault')}" />
                                </td>
                            </tr>
                        
                        </tbody>
                    </table>
                </div>
                <div class="buttons">
                    <span class="button"><g:actionSubmit class="save" action="update" value="${message(code: 'default.button.update.label', default: 'Update')}" /></span>
                    <span class="button"><g:actionSubmit class="delete" action="delete" value="${message(code: 'default.button.delete.label', default: 'Delete')}" onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');" /></span>
                </div>
            </g:form>
        </div>
    </body>
</html>
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                             