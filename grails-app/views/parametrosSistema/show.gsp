
<%@ page import="ar.com.telecom.pcs.entities.ParametrosSistema" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="layout" content="main" />
        <g:set var="entityName" value="${message(code: 'parametrosSistema.label', default: 'ParametrosSistema')}" />
        <title><g:message code="default.show.label" args="[entityName]" /></title>
    </head>
    <body>
        <div class="nav">
            <span class="menuButton"><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></span>
            <span class="menuButton"><g:link class="list" action="list"><g:message code="default.list.label" args="[entityName]" /></g:link></span>
            <span class="menuButton"><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></span>
        </div>
        <div class="body">
            <h1><g:message code="default.show.label" args="[entityName]" /></h1>
            <g:if test="${flash.message}">
            <div class="message">${flash.message}</div>
            </g:if>
            <div class="dialog">
                <table>
                    <tbody>
                    
                        <tr class="prop">
                            <td valign="top" class="name"><g:message code="parametrosSistema.id.label" default="Id" /></td>
                            
                            <td valign="top" class="value">${fieldValue(bean: parametrosSistemaInstance, field: "id")}</td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name"><g:message code="parametrosSistema.grupoLDAPUsuarioFinal.label" default="Grupo LDAPU suario Final" /></td>
                            
                            <td valign="top" class="value">${fieldValue(bean: parametrosSistemaInstance, field: "grupoLDAPUsuarioFinal")}</td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name"><g:message code="parametrosSistema.grupoLDAPInterlocutorUsuario.label" default="Grupo LDAPI nterlocutor Usuario" /></td>
                            
                            <td valign="top" class="value">${fieldValue(bean: parametrosSistemaInstance, field: "grupoLDAPInterlocutorUsuario")}</td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name"><g:message code="parametrosSistema.grupoLDAPFuncionGestionDemandaGral.label" default="Grupo LDAPF uncion Gestion Demanda Gral" /></td>
                            
                            <td valign="top" class="value">${fieldValue(bean: parametrosSistemaInstance, field: "grupoLDAPFuncionGestionDemandaGral")}</td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name"><g:message code="parametrosSistema.grupoLDAPCoordinadorPedido.label" default="Grupo LDAPC oordinador Pedido" /></td>
                            
                            <td valign="top" class="value">${fieldValue(bean: parametrosSistemaInstance, field: "grupoLDAPCoordinadorPedido")}</td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name"><g:message code="parametrosSistema.grupoAdmin.label" default="Grupo Admin" /></td>
                            
                            <td valign="top" class="value">${fieldValue(bean: parametrosSistemaInstance, field: "grupoAdmin")}</td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name"><g:message code="parametrosSistema.tooltipNotasDesarrollo.label" default="Tooltip Notas Desarrollo" /></td>
                            
                            <td valign="top" class="value">${fieldValue(bean: parametrosSistemaInstance, field: "tooltipNotasDesarrollo")}</td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name"><g:message code="parametrosSistema.cartelAdvertenciaGerentesIT.label" default="Cartel Advertencia Gerentes IT" /></td>
                            
                            <td valign="top" class="value">${fieldValue(bean: parametrosSistemaInstance, field: "cartelAdvertenciaGerentesIT")}</td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name"><g:message code="parametrosSistema.mensajeAdvertenciaValidar.label" default="Mensaje Advertencia Validar" /></td>
                            
                            <td valign="top" class="value">${fieldValue(bean: parametrosSistemaInstance, field: "mensajeAdvertenciaValidar")}</td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name"><g:message code="parametrosSistema.mailEmisorPCS.label" default="Mail Emisor PCS" /></td>
                            
                            <td valign="top" class="value">${fieldValue(bean: parametrosSistemaInstance, field: "mailEmisorPCS")}</td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name"><g:message code="parametrosSistema.mailAdministradorFuncional.label" default="Mail Administrador Funcional" /></td>
                            
                            <td valign="top" class="value">${fieldValue(bean: parametrosSistemaInstance, field: "mailAdministradorFuncional")}</td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name"><g:message code="parametrosSistema.tiempoActualizacionInbox.label" default="Tiempo Actualizacion Inbox" /></td>
                            
                            <td valign="top" class="value">${fieldValue(bean: parametrosSistemaInstance, field: "tiempoActualizacionInbox")}</td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name"><g:message code="parametrosSistema.tiempoActualizacionInboxAprobaciones.label" default="Tiempo Actualizacion Inbox Aprobaciones" /></td>
                            
                            <td valign="top" class="value">${fieldValue(bean: parametrosSistemaInstance, field: "tiempoActualizacionInboxAprobaciones")}</td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name"><g:message code="parametrosSistema.repeticionHoras.label" default="Repeticion Horas" /></td>
                            
                            <td valign="top" class="value">${fieldValue(bean: parametrosSistemaInstance, field: "repeticionHoras")}</td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name"><g:message code="parametrosSistema.notaReporteAprobacionImpacto.label" default="Nota Reporte Aprobacion Impacto" /></td>
                            
                            <td valign="top" class="value">${fieldValue(bean: parametrosSistemaInstance, field: "notaReporteAprobacionImpacto")}</td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name"><g:message code="parametrosSistema.auditaMails.label" default="Audita Mails" /></td>
                            
                            <td valign="top" class="value"><g:formatBoolean boolean="${parametrosSistemaInstance?.auditaMails}" /></td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name"><g:message code="parametrosSistema.enviaMails.label" default="Envia Mails" /></td>
                            
                            <td valign="top" class="value"><g:formatBoolean boolean="${parametrosSistemaInstance?.enviaMails}" /></td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name"><g:message code="parametrosSistema.maximoJerarquiaAprobador.label" default="Maximo Jerarquia Aprobador" /></td>
                            
                            <td valign="top" class="value">${fieldValue(bean: parametrosSistemaInstance, field: "maximoJerarquiaAprobador")}</td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name"><g:message code="parametrosSistema.maximoJerarquiaGerente.label" default="Maximo Jerarquia Gerente" /></td>
                            
                            <td valign="top" class="value">${fieldValue(bean: parametrosSistemaInstance, field: "maximoJerarquiaGerente")}</td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name"><g:message code="parametrosSistema.minimoJerarquiaAprobador.label" default="Minimo Jerarquia Aprobador" /></td>
                            
                            <td valign="top" class="value">${fieldValue(bean: parametrosSistemaInstance, field: "minimoJerarquiaAprobador")}</td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name"><g:message code="parametrosSistema.minimoJerarquiaGerente.label" default="Minimo Jerarquia Gerente" /></td>
                            
                            <td valign="top" class="value">${fieldValue(bean: parametrosSistemaInstance, field: "minimoJerarquiaGerente")}</td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name"><g:message code="parametrosSistema.tipoGestionDefault.label" default="Tipo Gestion Default" /></td>
                            
                            <td valign="top" class="value"><g:link controller="tipoGestion" action="show" id="${parametrosSistemaInstance?.tipoGestionDefault?.id}">${parametrosSistemaInstance?.tipoGestionDefault?.encodeAsHTML()}</g:link></td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name"><g:message code="parametrosSistema.valorHoraSWFTecoDefault.label" default="Valor Hora SWFT eco Default" /></td>
                            
                            <td valign="top" class="value">${fieldValue(bean: parametrosSistemaInstance, field: "valorHoraSWFTecoDefault")}</td>
                            
                        </tr>
                    
                    </tbody>
                </table>
            </div>
            <div class="buttons">
                <g:form>
                    <g:hiddenField name="id" value="${parametrosSistemaInstance?.id}" />
                    <span class="button"><g:actionSubmit class="edit" action="edit" value="${message(code: 'default.button.edit.label', default: 'Edit')}" /></span>
                    <span class="button"><g:actionSubmit class="delete" action="delete" value="${message(code: 'default.button.delete.label', default: 'Delete')}" onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');" /></span>
                </g:form>
            </div>
        </div>
    </body>
</html>
