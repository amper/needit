<%@page import="ar.com.telecom.pcs.entities.RolAplicacion"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta name="layout" content="mainApp" />
<g:javascript library="boxUser" />
<g:javascript library="finalizacionPedido" />
</head>
<body>

	<div class="contenedorTitulo">
		<g:titulo pedido="${pedidoInstance}" />
		<g:workflow pedidoId="${pedidoInstance?.id}" />
	</div>
 
	<div id="contenedorIzq">
		<g:if test="${flash.message}">
			<div class="message">
				${flash.message}
			</div>
		</g:if>
		<g:hasErrors bean="${pedidoInstance}">
			<div class="errors">
				<g:renderErrors bean="${pedidoInstance}" as="list" />
			</div>
		</g:hasErrors>
		<g:form class="formGeneral" useToken="true">
			<g:hiddenField name="id" value="${pedidoInstance?.id}" />

			<div class="seccionHide">
				<g:cabeceraPedido pedido="${pedidoInstance}" />
				<g:render template="/templates/workflowHidden" />

				<p class="tituloSeccion">Finalizaci&oacute;n</p>
				<div class="formSeccion">
					
					<div class="separatorBotonera"></div>
					<div>
						<label class="formLabel" style="font-size: 12pt;">Bas&aacute;ndose en su experiencia, por favor valore los siguientes puntos, de una escala de 1 a 5</label><br>
						<label class="formLabel" style="font-size: 12pt;">(1: Deficiente, 5: Excelente)</label>
					</div>
					<div class="separator"></div>
					<div>
						<label class="formLabel" style="font-size: 10pt;">A) Para  este requerimiento en particular</label><br><br><br>
						<label class="formLabel" style="font-size: 10pt;">A.1) Sobre la soluci&oacute;n brindada</label>
					</div>
					
					<div class="separator"></div>
					<div>
						<label class="formLabel" style="width: 200px">Calidad de la soluci&oacute;n: </label>
						<span>${encuestaSatisfaccion?.getEscalaSatisfaccion(encuestaSatisfaccion?.calidadSolucion)}</span>
					</div>
					<div class="separator"></div>
					<div>
						<label class="formLabel" style="width: 200px">Tiempo de Resoluci&oacute;n: </label>
						<span>${encuestaSatisfaccion?.getEscalaSatisfaccion(encuestaSatisfaccion?.tiempoResolucion)}</span>
					</div>
					<div class="separator"></div>
					<div>
						<label class="formLabel" style="width: 200px">Adecuaci&oacute;n a sus necesidades: </label>
						<span>${encuestaSatisfaccion?.getEscalaSatisfaccion(encuestaSatisfaccion?.adecuacionNecesidades)}</span>
					</div>
					<div class="separator"></div>
					<div>
						<label class="formLabel" style="width: 200px">Valoraci&oacute;n general: </label>
						<span>${encuestaSatisfaccion?.getEscalaSatisfaccion(encuestaSatisfaccion?.valoracionGeneral)}</span>
					</div>
					<div class="separator"></div>
					<div class="pCompleto">
						<label class="formLabel" for='comentariosSolucion'>Comentarios</label><br>
						<p class="info">${encuestaSatisfaccion?.comentariosSolucion}</p>
					</div>
					<div class="separator"><hr/></div>
					
					<div>
						<label class="formLabel" style="font-size: 10pt;">A.2) Sobre la gesti&oacute;n por parte del Coordinador / Responsable Software Factory del pedido</label>
					</div>
					<div class="separator"></div>
					<div>
						<label class="formLabel" style="width: 200px">Grado de satisfacci&oacute;n CC/RSWF: </label>
						<span>${encuestaSatisfaccion?.getEscalaSatisfaccion(encuestaSatisfaccion?.gradoSatisfaccionCCRSWF)}</span>
					</div>
					<div class="separator"></div>
					
					<div class="pCompleto">
						<label class="formLabel" for='comentariosGestion'>Comentarios</label><br>
						<p class="info">${encuestaSatisfaccion?.comentariosGestion}</p>
					</div>
					<div class="separator"><hr/></div>
					
					<div>
						<label class="formLabel" style="font-size: 10pt;">B) Para la gesti&oacute;n de requerimientos evolutivos en general</label><br>
						<label class="formLabel" style="font-size: 10pt;">B.1) Sobre la Herramienta NeedIT:</label>
					</div>

					<div class="separator"></div>
					<div>
						<label class="formLabel" style="width: 200px">Agilidad en el uso de NeedIT: </label>
						<span>${encuestaSatisfaccion?.getEscalaSatisfaccion(encuestaSatisfaccion?.agilidadNeedIT)}</span>
					</div>
					<div class="separator"></div>
					
					<div class="pCompleto">
						<label class="formLabel" for='comentariosHerramienta'>Comentarios</label><br>
						<p class="info">${encuestaSatisfaccion?.comentariosHerramienta}</p>
					</div>
					<div class="separator"><hr/></div>
					
					<div>
						<label class="formLabel" style="font-size: 10pt;">B.2) Sobre el proceso de gesti&oacute;n de evolutivos</label>
					</div>
					
					<div class="separator"></div>
					<div>
						<label class="formLabel" style="width: 200px">Grado de satisfacci&oacute;n con el proceso de desarrollo: </label>
						<span>${encuestaSatisfaccion?.getEscalaSatisfaccion(encuestaSatisfaccion?.gradoSatisfaccionProcesoDesarrollo)}</span>
					</div>
					<div class="separator"></div>

					<div class="pCompleto">
						<label class="formLabel" for='comentariosProceso'>Comentarios</label><br>
						<p class="info">${encuestaSatisfaccion?.comentariosProceso}</p>
					</div>
				</div>
			</div>
			<div id="message"></div>
		</g:form>


	</div>
	<!-- fin contenedor izq -->

	<!-- Lateral Derecho -->
	<g:render template="/templates/menuLateral" />
	<!-- Fin Lateral Derecho -->

	<!-- 
	************
	************
	MODALS POPUP
	************
	************
	************
	-->

	<!-- popup ayuda seccion -->
	<div id="dialog-help" title="Ayuda Aprobar Pedido"
		style="display: none;">
		<div style="height: 350px; width: 100%; overflow: auto;">
			<p class="helpTitle">Seccion ayuda</p>
			<p>Hola esto es una seccion de ayuda Hola esto es una seccion de</p>
		</div>
	</div>
	<!-- fin popup ayuda seccion -->
</body>
</html>
