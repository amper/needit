<%@page import="ar.com.telecom.pcs.entities.RolAplicacion"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta name="layout" content="mainApp" />
<g:javascript library="boxUser" />
<g:javascript library="finalizacionPedido" />
</head>
<body>

	<div class="contenedorTitulo">
		<g:titulo pedido="${pedidoInstance}" />
		<g:workflow pedidoId="${pedidoInstance?.id}" />
	</div>
 
	<div id="contenedorIzq">
		<g:if test="${flash.message}">
			<div class="message">
				${flash.message}
			</div>
		</g:if>
		<g:hasErrors bean="${pedidoInstance}">
			<div class="errors">
				<g:renderErrors bean="${pedidoInstance}" as="list" />
			</div>
		</g:hasErrors>
		<g:form class="formGeneral" useToken="true">
			<g:hiddenField name="id" value="${pedidoInstance?.id}" />

			<div class="seccionHide">
				<g:cabeceraPedido pedido="${pedidoInstance}" />
				<g:render template="/templates/workflowHidden" />

				<p class="tituloSeccion">Finalizaci&oacute;n</p>
				<div class="formSeccion">
					<div class="separatorBotonera"></div>
					<div>
						<label class="formLabel" style="font-size: 12pt;">Bas&aacute;ndose en su experiencia, por favor valore los siguientes puntos, de una escala de 1 a 5</label><br>
						<label class="formLabel" style="font-size: 12pt;">(1: Deficiente, 5: Excelente)</label>
					</div>
					<div class="separator"></div>
					<div>
						<label class="formLabel" style="font-size: 10pt;">A) Para  este requerimiento en particular</label><br><br><br>
						<label class="formLabel" style="font-size: 10pt;">A.1) Sobre la soluci&oacute;n brindada</label>
					</div>
					
					<div class="separator"></div>
					<div>
						<label class="formLabel" style="width: 200px">Calidad de la soluci&oacute;n: </label>
						<span>
							<g:radio name="calidadSolucion" value="0" checked="true"/>Sin Respuesta
							<g:each in="${listEscalaSatisfaccion.entrySet()}" var="escala">
								<g:radio name="calidadSolucion" value="${escala.value}" checked="${encuestaSatisfaccion?.calidadSolucion==escala.value}"/>${escala.key}
							</g:each>
						</span>
					</div>
					<div class="separator"></div>
					<div>
						<label class="formLabel" style="width: 200px">Tiempo de Resoluci&oacute;n: </label>
						<span>
							<g:radio name="tiempoResolucion" value="0" checked="true"/>Sin Respuesta
							<g:each in="${listEscalaSatisfaccion.entrySet()}" var="escala" >
								<g:radio name="tiempoResolucion" value="${escala.value}" checked="${encuestaSatisfaccion?.tiempoResolucion==escala.value}"/>${escala.key}
							</g:each>
						</span>
					</div>
					<div class="separator"></div>
					<div>
						<label class="formLabel" style="width: 200px">Adecuaci&oacute;n a sus necesidades: </label>
						<span>
							<g:radio name="adecuacionNecesidades" value="0" checked="true"/>Sin Respuesta
							<g:each in="${listEscalaSatisfaccion.entrySet()}" var="escala" >
								<g:radio name="adecuacionNecesidades" value="${escala.value}" checked="${encuestaSatisfaccion?.adecuacionNecesidades==escala.value}"/>${escala.key}
							</g:each>
						</span>
					</div>
					<div class="separator"></div>
					<div>
						<label class="formLabel" style="width: 200px">Valoraci&oacute;n general: </label>
						<span>
							<g:radio name="valoracionGeneral" value="0" checked="true"/>Sin Respuesta
							<g:each in="${listEscalaSatisfaccion.entrySet()}" var="escala" >
								<g:radio name="valoracionGeneral" value="${escala.value}" checked="${encuestaSatisfaccion?.valoracionGeneral==escala.value}"/>${escala.key}
							</g:each>
						</span>
					</div>
					<div class="separator"></div>
					<div class="pCompleto">
						<label class="formLabel" for='comentariosSolucion'>Comentarios</label><br>
						<g:textArea id="comentariosSolucion" name="comentariosSolucion" value="${encuestaSatisfaccion?.comentariosSolucion}"></g:textArea>
					</div>
					<div class="separator"><hr/></div>
					
					<div>
						<label class="formLabel" style="font-size: 10pt;">A.2) Sobre la gesti&oacute;n por parte del Coordinador / Responsable Software Factory del pedido</label>
					</div>
					<div class="separator"></div>
					<div>
						<label class="formLabel" style="width: 200px">Grado de satisfacci&oacute;n CC/RSWF: </label>
						<span>
							<g:radio name="gradoSatisfaccionCCRSWF" value="0" checked="true"/>Sin Respuesta
							<g:each in="${listEscalaSatisfaccion.entrySet()}" var="escala" >
								<g:radio name="gradoSatisfaccionCCRSWF" value="${escala.value}" checked="${encuestaSatisfaccion?.gradoSatisfaccionCCRSWF==escala.value}"/>${escala.key}
							</g:each>
						</span>
					</div>
					<div class="separator"></div>
					
					<div class="pCompleto">
						<label class="formLabel" for='comentariosGestion'>Comentarios</label><br>
						<g:textArea id="comentariosGestion" name="comentariosGestion" value="${encuestaSatisfaccion?.comentariosGestion}"></g:textArea>
					</div>
					<div class="separator"><hr/></div>
					
					<div>
						<label class="formLabel" style="font-size: 10pt;">B) Para la gesti&oacute;n de requerimientos evolutivos en general</label><br>
						<label class="formLabel" style="font-size: 10pt;">B.1) Sobre la Herramienta NeedIT:</label>
					</div>

					<div class="separator"></div>
					<div>
						<label class="formLabel" style="width: 200px">Agilidad en el uso de NeedIT: </label>
						<span>
							<g:radio name="agilidadNeedIT" value="0" checked="true"/>Sin Respuesta
							<g:each in="${listEscalaSatisfaccion.entrySet()}" var="escala" >
								<g:radio name="agilidadNeedIT" value="${escala.value}" checked="${encuestaSatisfaccion?.agilidadNeedIT==escala.value}"/>${escala.key}
							</g:each>
						</span>
					</div>
					<div class="separator"></div>
					
					<div class="pCompleto">
						<label class="formLabel" for='comentariosHerramienta'>Comentarios</label><br>
						<g:textArea id="comentariosHerramienta" name="comentariosHerramienta" value="${encuestaSatisfaccion?.comentariosHerramienta}"></g:textArea>
					</div>
					<div class="separator"><hr/></div>
					
					<div>
						<label class="formLabel" style="font-size: 10pt;">B.2) Sobre el proceso de gesti&oacute;n de evolutivos</label>
					</div>
					
					<div class="separator"></div>
					<div>
						<label class="formLabel" style="width: 200px">Grado de satisfacci&oacute;n con el proceso de desarrollo: </label>
						<span>
							<g:radio name="gradoSatisfaccionProcesoDesarrollo" value="0" checked="true"/>Sin Respuesta
							<g:each in="${listEscalaSatisfaccion.entrySet()}" var="escala" >
								<g:radio name="gradoSatisfaccionProcesoDesarrollo" value="${escala.value}" checked="${encuestaSatisfaccion?.gradoSatisfaccionProcesoDesarrollo==escala.value}" />${escala.key}
							</g:each>
						</span>
					</div>
					<div class="separator"></div>

					<div class="pCompleto">
						<label class="formLabel" for='comentariosProceso'>Comentarios</label><br>
						<g:textArea id="comentariosProceso" name="comentariosProceso" value="${encuestaSatisfaccion?.comentariosProceso}"></g:textArea>
					</div>
				</div>
			</div>
			<div id="message"></div>
			<g:if test="${permiteLlenarEncuesta}">
				<div class="formSeccionButton" style="margin-top: 20px;">
					<g:if test="${pedidoInstance?.faseActual?.controllerNombre.equals(params.controller) || !pedidoInstance.faseActual || pedidoInstance.legajoInterlocutorUsuario.equals(usuarioLogueado)}">
						<g:actionSubmit class="formSeccionSubmitButton" id="Guardar" action="grabarFormulario" value="Guardar" />
						<g:actionSubmit class="formSeccionSubmitButton" id="Aprobar" action="aprobarPedido" value="Aprobar" />
					</g:if>
				</div>
			</g:if>
		</g:form>


	</div>
	<!-- fin contenedor izq -->

	<!-- Lateral Derecho -->
	<g:render template="/templates/menuLateral" />
	<!-- Fin Lateral Derecho -->

	<!-- 
	************
	************
	MODALS POPUP
	************
	************
	************
	-->

	<!-- popup ayuda seccion -->
	<div id="dialog-help" title="Ayuda Aprobar Pedido"
		style="display: none;">
		<div style="height: 350px; width: 100%; overflow: auto;">
			<p class="helpTitle">Seccion ayuda</p>
			<p>Hola esto es una seccion de ayuda Hola esto es una seccion de</p>
		</div>
	</div>
	<!-- fin popup ayuda seccion -->
</body>
</html>
