<div style="width: 100%">
	<div class="pDos" style="width: 100%;">
		<label class="formLabel2">Direcci&oacute;n / Gerencia requirente:</label>
		<g:textField id="estructuraRequirente" name="estructuraRequirente" value="${variante?.estructuraRequirente}" />
	</div>
	<div class="separator"></div>
	<div style="border: 1px solid #AAAAAA;">
		<div class="pDos" style="width: 70%;">
			<label class="formLabel2">Usuario: <g:secureRemoteLink title="Eliminar" id="allUsers" class="limpiarBox" controller="busqueda" update="legajoUsuario" params="['username': '', 'box':'legajoUsuario', 'method': 'obtenerUsuariosAjax']" action="completarUsuario"></g:secureRemoteLink></label>
			<div style="float: left; margin-left: 40px;">
				<div id="legajoUsuario" class="boxUser">
					<g:render template="/templates/userBoxAjax"
						model="['person':variante?.legajoUsuario, 'box':'legajoUsuario', 'boxParalelo':'', 'personsAutocomplete': personsAutocomplete, 'method': 'obtenerUsuariosAjax']"></g:render>
				</div>
			</div>
		</div>
		<div class="separator"></div>
		<div>
			<label class="formLabel2">Rol:</label>
			<div style="margin-left: 40px; width: 100%; text-align: left;">
				<g:select id="comboboxRol" name="rolesABuscar" from="${ar.com.telecom.pcs.entities.RolAplicacion.list()}" optionKey="codigoRol" />
			</div>
		</div>
	</div>
	<div class="separator"></div>
	<div>
		<label class="formLabel2">Grupo:</label>
		<div id="divGrupo" style="margin-left: 40px; width: 100%; text-align: left;">
			<g:select id="comboboxGrupo" name="gruposABuscar" from="${grupos}"  />
		</div>
	</div>
	<div class="separator"></div>
	<div style="border: 1px solid #AAAAAA; padding-bottom: 10px;">
		<div>
			<label class="formLabel2">Estado:</label>
			<div id="divEstados" style="margin-left: 40px; width: 100%; text-align: left;">
				<g:select id="comboboxEstado" name="estados" from="${opciones_estados.entrySet().key}"  />
			</div>
		</div>
		<div class="separator"></div>
		<div>
			<div style="width: 200px; float: left;">
				<label for='fechaDesde' class="formLabel2">Fecha desde:</label>
				<div style="margin-left: 40px;">
					<g:textField name="fechaDesde" class="formInput date" style="width: 100px; margin-left: 0px;" id='datepicker-fechaDesde-rpid' maxLength="10" />
					<g:hiddenField name="fechaDesdeHidden" value="${variante?.fechaDesde?.format('dd/MM/yyyy')}" />
				</div>
			</div>
			<div style="width: 200px;">
				<label for='fechaHasta' class="formLabel2">Fecha hasta:</label>
				<div style="margin-left: 40px;">
					<g:textField name="fechaHasta" class="formInput date" style="width: 100px; margin-left: 5px;" id='datepicker-fechaHasta-rpid' maxLength="10" />
					<g:hiddenField name="fechaHastaHidden" value="${variante?.fechaHasta?.format('dd/MM/yyyy')}" />
				</div>
			</div>
		</div>
	</div>
</div>