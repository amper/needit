<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta name="layout" content="mainApp" />
<title>Need IT</title>
<script type="text/javascript" src="${resource(dir:'js/prototype',file:'scriptaculous.js')}"></script>
<g:javascript library="boxUser" />
<g:javascript library="busquedaAvanzada" />
<g:javascript library="animatedcollapse" />
<g:javascript library="impacto" />
<script>
	gCambioMacroestado = true;
	gCambioFase = true;

	// Defaults
	var tiposImpacto = ${variante?.tiposImpacto?variante.tiposImpacto.id as List:'null'};
	var areaSoporte = ${variante?.areasSoporte?variante.areasSoporte.id as List:'null'};
	var macroEstados = ${variante?.macroestados?variante.macroestados.id:'null'};
	var fases = ${variante?.fases?variante.fases.id as List:'null'};
	var roles = ${variante?.roles?variante?.roles as List:'null'};
	var grupos = ${variante?.grupos?variante?.grupos as List:'null'};
	var estados = ${variante?.estados?variante?.estados as List:'null'};
</script>
</head>

<body>
	<img id="spinner" src="<g:resource dir='/images' file='ajax-spinner.gif'/>" />
	<g:form class="formGeneral" id="formBusqueda" name="formBusqueda" action="buscarPedidos" useToken="true" >
		<div class="ContenedorGeneral">
			<p class="tituloPrincipal">B&uacute;squeda de Cambios</p>
		</div>
		
		<div class="message" id="message" style="display: none;"></div>

		<div class="ContenedorGeneral">
			<div class="pCompleto">
				<label class="formLabel2" for="comboboxTipoBusqueda">Tipo de b&uacute;squeda:</label>
				<g:select id="comboboxTipoBusqueda" class="formInput"
					name="tipoBusqueda" onChange="mostrarDivs(this.value);"
					from="${tiposDeBusqueda.entrySet()}" optionKey="key"
					optionValue="value" value="${params?.tipoBusqueda}" />
			</div>
		</div>
		
		<div class="seccionHide" id="table-busqueda-pedidos-fila1">
			<p class="tituloSeccion">B&uacute;squeda Est&aacute;ndar</p>
			<div class="formSeccion">
				<div  id="table-busqueda-pedidos-fila1-content">
					<div style="float: left; width: 40%;">
						<g:render template="/busqueda/tablaParametrosEstandar" />
						<g:render template="/busqueda/tablaEstadoActual" />
					</div>
					<div style="float: left; width: 45%;">
						<g:render template="/busqueda/tablaImpactos" />
					</div>
				</div>
			</div>
		</div>

		<div class="seccionHide2" id="table-busqueda-pedidos-fila2" style="display: none;">
			<p class="tituloSeccion">B&uacute;squeda Avanzada</p>
			<div class="formSeccion">
			
			<div  id="table-busqueda-pedidos-fila2-content">
					<div style="float: left; width: 50%;">
						<g:render template="/busqueda/tablaParametrosAvanzada" />
						<g:render template="/busqueda/tablaSistemaSugerido" />
					</div>
					<div style="float: left; width: 4%;"></div>
					<div style="float: left; width: 45%;">
						<g:render template="/busqueda/tablaTrazabilidad" />
					</div>
				</div>
			</div>
		</div>

		<div align="center">
			<input type="button" class="formSeccionSubmitButtonHistorial ui-button ui-widget ui-state-default ui-corner-all" value="Listado de Variantes" onClick="showPopUpVariantes('${usuarioLogueado}');" />
			<input type="button" class="formSeccionSubmitButtonHistorial ui-button ui-widget ui-state-default ui-corner-all" value="Nueva Variante" onClick="showPopUpNuevaVariantes();" />
			<input type="button" class="formSeccionSubmitButtonHistorial ui-button ui-widget ui-state-default ui-corner-all" value="Buscar" onClick="validarFormulario();" />
			<input type="button" class="formSeccionSubmitButtonHistorial ui-button ui-widget ui-state-default ui-corner-all" value="Limpiar" onClick="limpiarFormulario();" />
<%--			<input type="button" class="formSeccionSubmitButtonHistorial ui-button ui-widget ui-state-default ui-corner-all" value="Actual" onClick="mostrarValor();" />--%>
		</div>
		
		<div id="dialog-popupListadoVariantes" title="Listado de variantes" style="display: none;"></div>
		<div id="dialog-popupGuardarVariantes" title="Nueva variante" style="display: none;"><g:render template="/busqueda/guardarVariante"></g:render> </div>

	</g:form>

	<div id="contenedorIzq">
		<div class="list">
			<table>
				<thead>
					<tr>
						<th class="encabezado0"/>
						<g:sortableColumn property="id" class="encabezado1" title="N&uacute;mero" params="${params}"/>
						<g:sortableColumn property="titulo" class="encabezado2" title="T&iacute;tulo" params="${params}"/>
						<g:sortableColumn property="faseActual" class="encabezado3" title="Fase" params="${params}" />
						<g:sortableColumn property="fasePedido.macroEstado" class="encabezado4" title="Macroestado" params="${params}"/>
						<th class="encabezado6">Rol/Grupo asignado</th>
						<th/>
					</tr>
				</thead>
				<tbody>
					<g:if test="${pedidosIntancesList && !pedidosIntancesList.isEmpty()}">
						<g:each in="${pedidosIntancesList}" status="i" var="pedidoInstance">
							<g:set scope="page" var="pedido" value="${pedidoInstance}" />
							<g:divsInbox i="${i}" pedido="${ pedido }" usuarioLogueado="${usuarioLogueado}" tipoConsulta="${ tipoConsulta }"></g:divsInbox>
						</g:each>
					</g:if>
					<g:else>
						<tr>
							<td colspan="7" style="text-align: center; font-style: italic;">No hay pedidos para el criterio de b&uacute;squeda elegido</td>
						</tr>
					</g:else>
				</tbody>
			</table>
		</div>
		<div class="paginateButtons">
		<% params.macroestado = null %>
		<% params.fases = null %>
			<g:paginate params="${params}" action="buscarPedidos" total="${params.totalCount?:0}" />
		</div>
		
		<g:if test="${pedidoInstanceList}">
			<% 
				params.format="excel"; 
				params.extension="xls"; 
			%>
			<a href="javascript:window.location='${createLink(params: params, action: "exportFilePedidosPadre")}';" >Excel Padres</a> <b>|</b>
			<a href="javascript:window.location='${createLink(params: params, action: "exportFilePedidosHijo")}';" >Excel Hijos</a> <b>|</b>
			<% 
				params.format="pdf"; 
				params.extension="pdf"; 
			%>
			<a href="javascript:window.location='${createLink(params: params, action: "exportFilePedidosPadre")}';" >PDF Padres</a> <b>|</b>
			<a href="javascript:window.location='${createLink(params: params, action: "exportFilePedidosHijo")}';" >PDF Hijos</a> 
			
		</g:if>
		
		<script>
			animatedcollapse.ontoggle=function($, divobj, state){ myOnToggle(divobj, state); };
			animatedcollapse.init();
		</script>		
	</div>
<script type="text/javascript">
	mostrarDivs('${variante?.tipoBusqueda}');
</script>
</body>
</html>
