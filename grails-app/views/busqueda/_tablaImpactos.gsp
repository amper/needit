<div style="width: 100%">
	<div class="pDos" style="width: 100%;">
		<label class="formLabel2">Sistemas impactados:</label>
		<div style="width: 49%; float: left;">
			<g:select id="comboboxOpcionesSistemasImpactados"
			class="formInput" 
			name="criterioSistemaImpactado"
			from="${opciones.entrySet()}"
			value="${variante?.criterioSistemaImpactado}"
			optionKey="key" optionValue="value" 
			noSelection="['null': '']" />
		</div>
		<div style="width: 49%; float: left;">
			<g:textField id="valorSistemaImpactado" name="valorSistemaImpactado" value="${variante?.valorSistemaImpactado}" />
		</div>
	</div>
	<div class="separator"></div>
	<div>
		<label class="formLabel2">Tipo de Impacto:</label>
		<div style="margin-left: 40px; width: 100%; text-align: left;">
			<g:select id="comboboxTipoImpacto" name="criterioTipoImpacto" from="${ar.com.telecom.pcs.entities.TipoImpacto.list()}" optionKey="id" />
		</div>
	</div> 
	<div class="separator"></div>
	<div>
		<label class="formLabel2">&Aacute;reas de soporte impactadas:</label>
		<div style="margin-left: 40px; width: 100%; text-align: left;">
			<g:select id="comboboxAreaSoporte" name="criterioAreaSoporte" from="${ar.com.telecom.pcs.entities.AreaSoporte.list()}" optionKey="id"  />
		</div>
	</div>
	<div class="separator"></div>
</div>