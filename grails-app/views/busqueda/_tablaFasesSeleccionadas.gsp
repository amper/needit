<g:if test="${listadoFases && !listadoFases?.isEmpty()}">
		<div> 
			<g:checkBoxList items="${listadoFases}" name="fase" seleccionados="${fasesSeleccionadas}" optionKey="id" height="${listadoFases && listadoFases.size > 0 ? listadoFases.size * 21: 400}" />
		</div>
</g:if> 