<div class="pDos" style="width: 100%">
	<div>
		<label class="formLabel2">T&iacute;tulo contiene:</label>
		<g:textField id="titulo" name="titulo" value="${variante?.titulo}" />
	</div>
	<div>
		<label class="formLabel2">Tipo de gesti&oacute;n:</label>
		<g:select id="comboboxTipoGestion"
		class="formInput" 
		name="tipoGestion.id"
		from="${ar.com.telecom.pcs.entities.TipoGestion.list()}"
		optionKey="id" value="${variante?.tipoGestion?.id}"
		noSelection="['null': '']" />
	</div>
</div> 