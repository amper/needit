<g:updateToken />
<div style="width: 100%">
	<div>
		<label class="formLabel2">Macroestado:</label>
		<div style="margin-left: 40px; width: 100%; text-align: left;">
			<g:select id="comboboxMacroestado" name="macroestado" from="${ar.com.telecom.pcs.entities.MacroEstado.list()}" optionKey="id" />
		</div>
	</div>
	<div>
		<label class="formLabel2">Fase:</label>
		<div id="comboboxFaseBusqueda" style="margin-left: 40px; width: 100%; text-align: left;">
<%--			<script type="text/javascript">obtenerFases('');</script>--%>
			<g:render template="/templates/comboFasesHistorial" model="['show': false]"></g:render>
		</div>
	</div>
</div> 