<g:updateToken />
<g:form name="formListadoVariante" id="formListadoVariante" action="buscarPedidos" useToken="true" controller="busqueda">
	<g:hiddenField name="varianteId"/>
	<label class="formLabel" style="margin-left: 0px;">Variantes guardadas:</label>
	<div style="height: 10px;"></div>
	<div class="list">
		<table>
			<tbody>
				<g:if test="${listadoVariantes}">
					<tr style="background-color: #c1d5d9;">
						<td>
							Nombre de la variante
						</td>
						<td style="text-align: center">
							Eliminar
						</td>
					</tr>
					<g:each in="${listadoVariantes}" status="i" var="variante">
						<tr class="${(i % 2) == 0 ? 'odd' : 'even'}">
							<td>
								<a href="javascript:cargarVariante('${variante.id}');">${variante.nombreVariante}</a>
							</td>
							<td style="text-align: center">
								<g:secureRemoteLink action="eliminarVariante"
								  params="['varianteId': variante.id]" update="dialog-popupListadoVariantes" onComplete="resizeListadoVariantes();" >
				 				  <img src="${resource(dir:'images',file:'delete.gif')}"
							 	 width="10" height="10" />
								</g:secureRemoteLink>
							</td>
						</tr>
					</g:each>
				</g:if>
				<g:else>
					<tr>
						<td style="text-align: center; font-style: italic;">No hay variantes guardadas para el usuario logueado</td>
					</tr>
				</g:else>
			</tbody>
		</table>
	</div>
	<div style="height: 20px;"></div>
</g:form>
