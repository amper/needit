<%@page import="ar.com.telecom.pcsPlus.PedidoService"%>
<%@page import="ar.com.telecom.pedido.consulta.Consulta"%>

<g:updateToken />
<g:if test="${flash.message}">
	<div class="message">
			<li>${flash.message}</li>
	</div>
</g:if>

<div class="list"> 
	<table>
		<thead>
			<tr>
				<th class="encabezado0"/>
				<g:sortableColumn property="pedido.id" class="encabezado1" title="N&uacute;mero" params="${params}"/>
				<g:sortableColumn property="pedido.titulo" class="encabezado2" title="T&iacute;tulo" params="${params}"/>
				<g:sortableColumn property="pedido.faseActual" class="encabezado3" title="Fase" params="${params}" />
				<g:sortableColumn property="fasePedido.macroEstado" class="encabezado4" title="Macroestado" params="${params}"/>
<%--				<th class="encabezado5">Sistema</th>--%>
				<g:sortableColumn property="rol" class="encabezado6" title="Rol/Grupo asignado" params="${params}"/>
				<th/>
			</tr>
		</thead>
		<tbody>
			<g:if test="${pedidosIntancesList && !pedidosIntancesList.isEmpty()}">
				<g:each in="${pedidosIntancesList}" status="i" var="pedidoInstance">
					<g:set scope="page" var="pedido" value="${pedidoInstance}" />
					<g:divsInbox i="${i}" pedido="${ pedido }" usuarioLogueado="${usuarioLogueado}"></g:divsInbox>
				</g:each>
			</g:if>
			<g:else>
				<tr>
					<td colspan="7" style="text-align: center; font-style: italic;">No hay pedidos para el criterio de b&uacute;squeda elegido</td>
				</tr>
			</g:else>
		</tbody>
	</table>
</div>
<div class="paginateButtons">
	<g:paginate total="${pedidoInstanceTotal?pedidoInstanceTotal:0}" params="${params}" />
</div>
<g:if test="${pedidoInstanceList}">
	<% 
		params.format="excel"; 
		params.extension="xls"; 
	%>
	<a href="javascript:window.location='${createLink(params: params, action: "exportFilePedidosPadre")}';" >Excel Padres</a> <b>|</b>
	<a href="javascript:window.location='${createLink(params: params, action: "exportFilePedidosHijo")}';" >Excel Hijos</a> <b>|</b>
	<% 
		params.format="pdf"; 
		params.extension="pdf"; 
	%>
	<a href="javascript:window.location='${createLink(params: params, action: "exportFilePedidosPadre")}';" >PDF Padres</a> <b>|</b>
	<a href="javascript:window.location='${createLink(params: params, action: "exportFilePedidosHijo")}';" >PDF Hijos</a> 
	
</g:if>

<script>
	animatedcollapse.ontoggle=function($, divobj, state){ myOnToggle(divobj, state); };
	animatedcollapse.init();
</script>