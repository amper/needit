<div style="width: 100%">
	<div class="pDos" style="width: 100%;">
		<label class="formLabel2">Sistema sugerido:</label>
		<div style="width: 49%; float: left;">
			<g:select id="comboboxOpcionesSistemasSugeridos"
			class="formInput" 
			name="criterioSistemaSugerido"
			from="${opciones.entrySet()}"
			value="${variante?.criterioSistemaSugerido }"
			optionKey="key" optionValue="value" 
			noSelection="['null': '']" />
		</div>
		<div style="width: 49%; float: left;">
			<g:textField name="valorSistemaSugerido" size="10" id="valorSistemaSugerido" value="${variante?.valorSistemaSugerido}" />
		</div>
	</div>
	<div class="pDos" style="width: 100%;">
		<label class="formLabel2">Prioridad:</label>
		<div style="width: 49%; float: left;">
			<g:select id="comboboxOpcionesPrioridad"
			class="formInput" 
			name="criterioPrioridad"
			from="${opciones.entrySet()}"
			value="${variante?.criterioPrioridad }"
			optionKey="key" optionValue="value" 
			noSelection="['null': '']" />
		</div>
		<div style="width: 49%; float: left;">
			<g:textField name="valorPrioridad" size="10" id="valorPrioridad" value="${variante?.valorPrioridad}" />
		</div>
	</div>
</div> 