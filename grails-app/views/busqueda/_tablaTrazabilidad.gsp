<div style="width: 100%">
	<div class="pDos" style="width: 100%;">
		<label class="formLabel2">Referencia origen:</label>
		<div style="width: 49%; float: left;">
			<g:select id="comboboxTipoReferencia" name="tipoReferencia.id" 
			from="${ar.com.telecom.pcs.entities.TipoReferencia.list()}" 
			optionKey="id" value="${variante?.tipoReferencia?.id}"
			noSelection="['null': '']" />
		</div>
	</div> 
	<div class="pDos" style="width: 100%;">
		<label class="formLabel2">N&uacute;mero:</label>
		<div style="width: 49%; float: left;">
			<g:select id="comboboxOpcionesTipoReferencia"
			class="formInput" 
			name="criterioTipoReferencia"
			from="${opciones.entrySet()}"
			value="${variante?.criterioTipoReferencia}"
			optionKey="key" optionValue="value" 
			noSelection="['null': '']" />
		</div>
		<div style="width: 49%; float: left;">
			<g:textField name="valorTipoReferencia" size="10" id="valorTipoReferencia" value="${variante?.valorTipoReferencia}" />
		</div>
	</div>
	<div><hr/></div>
	<div class="pDos" style="width: 100%;">
		<label class="formLabel2">Solman:</label>
		<div style="width: 49%; float: left;">
				<g:select id="comboboxOpcionesSolman"
				class="formInput" 
				name="criterioSolman"
				from="${opciones_numericas.entrySet()}"
				value="${ variante?.criterioSolman }"
				optionKey="key" optionValue="value" 
				noSelection="['null': '']" />
		</div>
		<div style="width: 49%; float: left;">
			<g:textField name="valorSolman"  size="10" id="valorSolman" value="${ variante?.valorSolman }" />
		</div>
	</div>
	<div class="pDos" style="width: 100%;">
		<label class="formLabel2">Release:</label>
		<div style="width: 49%; float: left;">
			<g:select id="comboboxOpcionesRelease"
			class="formInput" 
			name="criterioRelease"
			from="${opciones.entrySet()}"
			value="${ variante?.criterioRelease }"
			optionKey="key" optionValue="value" 
			noSelection="['null': '']" />
		</div>
		<div style="width: 49%; float: left;">
			<g:textField name="valorRelease" size="10" id="valorRelease" value="${variante?.valorRelease }" />
		</div>
	</div>
	<div class="pDos" style="width: 100%;">
		<label class="formLabel2">SimpliT PaP:</label>
		<div style="width: 49%; float: left;">
			<g:select id="comboboxOpcionesSimplitPaP"
			class="formInput" 
			name="criterioSimplitPaP"
			from="${opciones_numericas.entrySet()}"
			value="${ variante?.criterioSimplitPaP }"
			optionKey="key" optionValue="value" 
			noSelection="['null': '']" />
		</div>
		<div style="width: 49%; float: left;">
			<g:textField name="valorSimplitPaP" size="10" id="valorSimplitPaP" value="${variante?.valorSimplitPaP }" />
		</div>
	</div>
	<div class="pDos" style="width: 100%;">
		<label class="formLabel2">SimpliT PAU:</label>
		<div style="width: 49%; float: left;">
				<g:select id="comboboxOpcionesSimplitPAU"
				class="formInput" 
				name="criterioSimplitPAU"
				from="${opciones_numericas.entrySet()}"
				value="${ variante?.criterioSimplitPAU }"
				optionKey="key" optionValue="value" 
				noSelection="['null': '']" />
		</div>
		<div style="width: 49%; float: left;">
			<g:textField name="valorSimplitPAU"  size="10" id="valorSimplitPAU" value="${variante?.valorSimplitPAU }" />
		</div>
	</div>
</div>