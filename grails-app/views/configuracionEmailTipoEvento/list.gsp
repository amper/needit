
<%@ page import="ar.com.telecom.pcs.entities.ConfiguracionEmailTipoEvento" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="layout" content="main" />
        <g:set var="entityName" value="${message(code: 'configuracionEmailTipoEvento.label', default: 'ConfiguracionEmailTipoEvento')}" />
        <title><g:message code="default.list.label" args="[entityName]" /></title>
    </head>
    <body>
        <div class="nav">
            <span class="menuButton"><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></span>
            <span class="menuButton"><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></span>
        </div>
        <div class="body">
            <h1><g:message code="default.list.label" args="[entityName]" /></h1>
            <g:if test="${flash.message}">
            <div class="message">${flash.message}</div>
            </g:if>
            <div class="list">
                <table>
                    <thead>
                        <tr>
                        
                            <g:sortableColumn property="id" title="${message(code: 'configuracionEmailTipoEvento.id.label', default: 'Id')}" />
                        
                            <g:sortableColumn property="asunto" title="${message(code: 'configuracionEmailTipoEvento.asunto.label', default: 'Asunto')}" />
                        
                            <th><g:message code="configuracionEmailTipoEvento.fase.label" default="Fase" /></th>
                        
                            <g:sortableColumn property="texto" title="${message(code: 'configuracionEmailTipoEvento.texto.label', default: 'Texto')}" />
                        
                            <g:sortableColumn property="tipoEvento" title="${message(code: 'configuracionEmailTipoEvento.tipoEvento.label', default: 'Tipo Evento')}" />
                        
                        </tr>
                    </thead>
                    <tbody>
                    <g:each in="${configuracionEmailTipoEventoInstanceList}" status="i" var="configuracionEmailTipoEventoInstance">
                        <tr class="${(i % 2) == 0 ? 'odd' : 'even'}">
                        
                            <td><g:link action="show" id="${configuracionEmailTipoEventoInstance.id}">${fieldValue(bean: configuracionEmailTipoEventoInstance, field: "id")}</g:link></td>
                        
                            <td>${fieldValue(bean: configuracionEmailTipoEventoInstance, field: "asunto")}</td>
                        
                            <td>${fieldValue(bean: configuracionEmailTipoEventoInstance, field: "fase")}</td>
                        
                            <td>${fieldValue(bean: configuracionEmailTipoEventoInstance, field: "texto")}</td>
                        
                            <td>${fieldValue(bean: configuracionEmailTipoEventoInstance, field: "tipoEvento")}</td>
                        
                        </tr>
                    </g:each>
                    </tbody>
                </table>
            </div>
            <div class="paginateButtons">
                <g:paginate total="${configuracionEmailTipoEventoInstanceTotal}" />
            </div>
        </div>
    </body>
</html>
