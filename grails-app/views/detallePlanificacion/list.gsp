
<%@ page import="ar.com.telecom.pcs.entities.DetallePlanificacion" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="layout" content="main" />
        <g:set var="entityName" value="${message(code: 'detallePlanificacion.label', default: 'DetallePlanificacion')}" />
        <title><g:message code="default.list.label" args="[entityName]" /></title>
    </head>
    <body>
        <div class="nav">
            <span class="menuButton"><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></span>
            <span class="menuButton"><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></span>
        </div>
        <div class="body">
            <h1><g:message code="default.list.label" args="[entityName]" /></h1>
            <g:if test="${flash.message}">
            <div class="message">${flash.message}</div>
            </g:if>
            <div class="list">
                <table>
                    <thead>
                        <tr>
                        
                            <g:sortableColumn property="id" title="${message(code: 'detallePlanificacion.id.label', default: 'Id')}" />
                        
                            <g:sortableColumn property="fechaDesde" title="${message(code: 'detallePlanificacion.fechaDesde.label', default: 'Fecha Desde')}" />
                        
                            <g:sortableColumn property="fechaHasta" title="${message(code: 'detallePlanificacion.fechaHasta.label', default: 'Fecha Hasta')}" />
                        
                            <th><g:message code="detallePlanificacion.planificacion.label" default="Planificacion" /></th>
                        
                        </tr>
                    </thead>
                    <tbody>
                    <g:each in="${detallePlanificacionInstanceList}" status="i" var="detallePlanificacionInstance">
                        <tr class="${(i % 2) == 0 ? 'odd' : 'even'}">
                        
                            <td><g:link action="show" id="${detallePlanificacionInstance.id}">${fieldValue(bean: detallePlanificacionInstance, field: "id")}</g:link></td>
                        
                            <td><g:formatDate date="${detallePlanificacionInstance.fechaDesde}" /></td>
                        
                            <td><g:formatDate date="${detallePlanificacionInstance.fechaHasta}" /></td>
                        
                            <td>${fieldValue(bean: detallePlanificacionInstance, field: "planificacion")}</td>
                        
                        </tr>
                    </g:each>
                    </tbody>
                </table>
            </div>
            <div class="paginateButtons">
                <g:paginate total="${detallePlanificacionInstanceTotal}" />
            </div>
        </div>
    </body>
</html>
