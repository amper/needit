
<%@ page import="ar.com.telecom.pcs.entities.Actividad" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="layout" content="main" />
        <g:set var="entityName" value="${message(code: 'actividad.label', default: 'Actividad')}" />
        <title><g:message code="default.list.label" args="[entityName]" /></title>
    </head>
    <body>
        <div class="nav">
            <span class="menuButton"><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></span>
            <span class="menuButton"><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></span>
        </div>
        <div class="body">
            <h1><g:message code="default.list.label" args="[entityName]" /></h1>
            <g:if test="${flash.message}">
            <div class="message">${flash.message}</div>
            </g:if>
            <div class="list">
                <table>
                    <thead>
                        <tr>
                        
                            <g:sortableColumn property="id" title="${message(code: 'actividad.id.label', default: 'Id')}" />
                        
                            <g:sortableColumn property="grupoResponsable" title="${message(code: 'actividad.grupoResponsable.label', default: 'Grupo Responsable')}" />
                        
                            <g:sortableColumn property="legajoUsuarioResponsable" title="${message(code: 'actividad.legajoUsuarioResponsable.label', default: 'Legajo Usuario Responsable')}" />
                        
                            <g:sortableColumn property="fechaCierre" title="${message(code: 'actividad.fechaCierre.label', default: 'Fecha Cierre')}" />
                        
                            <th><g:message code="actividad.codigoCierre.label" default="Codigo Cierre" /></th>
                        
                            <g:sortableColumn property="legajoUsuarioCierre" title="${message(code: 'actividad.legajoUsuarioCierre.label', default: 'Legajo Usuario Cierre')}" />
                        
                        </tr>
                    </thead>
                    <tbody>
                    <g:each in="${actividadInstanceList}" status="i" var="actividadInstance">
                        <tr class="${(i % 2) == 0 ? 'odd' : 'even'}">
                        
                            <td><g:link action="show" id="${actividadInstance.id}">${fieldValue(bean: actividadInstance, field: "id")}</g:link></td>
                        
                            <td>${fieldValue(bean: actividadInstance, field: "grupoResponsable")}</td>
                        
                            <td>${fieldValue(bean: actividadInstance, field: "legajoUsuarioResponsable")}</td>
                        
                            <td><g:formatDate date="${actividadInstance.fechaCierre}" /></td>
                        
                            <td>${fieldValue(bean: actividadInstance, field: "codigoCierre")}</td>
                        
                            <td>${fieldValue(bean: actividadInstance, field: "legajoUsuarioCierre")}</td>
                        
                        </tr>
                    </g:each>
                    </tbody>
                </table>
            </div>
            <div class="paginateButtons">
                <g:paginate total="${actividadInstanceTotal}" />
            </div>
        </div>
    </body>
</html>
