
<%@ page import="ar.com.telecom.pcs.entities.Actividad" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="layout" content="main" />
        <g:set var="entityName" value="${message(code: 'actividad.label', default: 'Actividad')}" />
        <title><g:message code="default.show.label" args="[entityName]" /></title>
    </head>
    <body>
        <div class="nav">
            <span class="menuButton"><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></span>
            <span class="menuButton"><g:link class="list" action="list"><g:message code="default.list.label" args="[entityName]" /></g:link></span>
            <span class="menuButton"><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></span>
        </div>
        <div class="body">
            <h1><g:message code="default.show.label" args="[entityName]" /></h1>
            <g:if test="${flash.message}">
            <div class="message">${flash.message}</div>
            </g:if>
            <div class="dialog">
                <table>
                    <tbody>
                    
                        <tr class="prop">
                            <td valign="top" class="name"><g:message code="actividad.id.label" default="Id" /></td>
                            
                            <td valign="top" class="value">${fieldValue(bean: actividadInstance, field: "id")}</td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name"><g:message code="actividad.grupoResponsable.label" default="Grupo Responsable" /></td>
                            
                            <td valign="top" class="value">${fieldValue(bean: actividadInstance, field: "grupoResponsable")}</td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name"><g:message code="actividad.legajoUsuarioResponsable.label" default="Legajo Usuario Responsable" /></td>
                            
                            <td valign="top" class="value">${fieldValue(bean: actividadInstance, field: "legajoUsuarioResponsable")}</td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name"><g:message code="actividad.fechaCierre.label" default="Fecha Cierre" /></td>
                            
                            <td valign="top" class="value"><g:formatDate date="${actividadInstance?.fechaCierre}" /></td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name"><g:message code="actividad.codigoCierre.label" default="Codigo Cierre" /></td>
                            
                            <td valign="top" class="value"><g:link controller="codigoCierre" action="show" id="${actividadInstance?.codigoCierre?.id}">${actividadInstance?.codigoCierre?.encodeAsHTML()}</g:link></td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name"><g:message code="actividad.legajoUsuarioCierre.label" default="Legajo Usuario Cierre" /></td>
                            
                            <td valign="top" class="value">${fieldValue(bean: actividadInstance, field: "legajoUsuarioCierre")}</td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name"><g:message code="actividad.comentariosCierre.label" default="Comentarios Cierre" /></td>
                            
                            <td valign="top" class="value">${fieldValue(bean: actividadInstance, field: "comentariosCierre")}</td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name"><g:message code="actividad.comentarios.label" default="Comentarios" /></td>
                            
                            <td valign="top" class="value">${fieldValue(bean: actividadInstance, field: "comentarios")}</td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name"><g:message code="actividad.fechaSugerida.label" default="Fecha Sugerida" /></td>
                            
                            <td valign="top" class="value"><g:formatDate date="${actividadInstance?.fechaSugerida}" /></td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name"><g:message code="actividad.numeroTicket.label" default="Numero Ticket" /></td>
                            
                            <td valign="top" class="value">${fieldValue(bean: actividadInstance, field: "numeroTicket")}</td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name"><g:message code="actividad.nombreServidor.label" default="Nombre Servidor" /></td>
                            
                            <td valign="top" class="value">${fieldValue(bean: actividadInstance, field: "nombreServidor")}</td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name"><g:message code="actividad.ambiente.label" default="Ambiente" /></td>
                            
                            <td valign="top" class="value">${fieldValue(bean: actividadInstance, field: "ambiente")}</td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name"><g:message code="actividad.numeroReferencia.label" default="Numero Referencia" /></td>
                            
                            <td valign="top" class="value">${fieldValue(bean: actividadInstance, field: "numeroReferencia")}</td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name"><g:message code="actividad.dominioMercury.label" default="Dominio Mercury" /></td>
                            
                            <td valign="top" class="value">${fieldValue(bean: actividadInstance, field: "dominioMercury")}</td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name"><g:message code="actividad.proyectoMercury.label" default="Proyecto Mercury" /></td>
                            
                            <td valign="top" class="value">${fieldValue(bean: actividadInstance, field: "proyectoMercury")}</td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name"><g:message code="actividad.release.label" default="Release" /></td>
                            
                            <td valign="top" class="value"><g:link controller="release" action="show" id="${actividadInstance?.release?.id}">${actividadInstance?.release?.encodeAsHTML()}</g:link></td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name"><g:message code="actividad.versionador.label" default="Versionador" /></td>
                            
                            <td valign="top" class="value"><g:link controller="versionador" action="show" id="${actividadInstance?.versionador?.id}">${actividadInstance?.versionador?.encodeAsHTML()}</g:link></td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name"><g:message code="actividad.url.label" default="Url" /></td>
                            
                            <td valign="top" class="value">${fieldValue(bean: actividadInstance, field: "url")}</td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name"><g:message code="actividad.descripcion.label" default="Descripcion" /></td>
                            
                            <td valign="top" class="value">${fieldValue(bean: actividadInstance, field: "descripcion")}</td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name"><g:message code="actividad.anexos.label" default="Anexos" /></td>
                            
                            <td valign="top" style="text-align: left;" class="value">
                                <ul>
                                <g:each in="${actividadInstance.anexos}" var="a">
                                    <li><g:link controller="anexoPorTipo" action="show" id="${a.id}">${a?.encodeAsHTML()}</g:link></li>
                                </g:each>
                                </ul>
                            </td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name"><g:message code="actividad.estado.label" default="Estado" /></td>
                            
                            <td valign="top" class="value"><g:link controller="estadoActividad" action="show" id="${actividadInstance?.estado?.id}">${actividadInstance?.estado?.encodeAsHTML()}</g:link></td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name"><g:message code="actividad.fase.label" default="Fase" /></td>
                            
                            <td valign="top" class="value"><g:link controller="fase" action="show" id="${actividadInstance?.fase?.id}">${actividadInstance?.fase?.encodeAsHTML()}</g:link></td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name"><g:message code="actividad.fechaCreacion.label" default="Fecha Creacion" /></td>
                            
                            <td valign="top" class="value"><g:formatDate date="${actividadInstance?.fechaCreacion}" /></td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name"><g:message code="actividad.pedido.label" default="Pedido" /></td>
                            
                            <td valign="top" class="value"><g:link controller="abstractPedido" action="show" id="${actividadInstance?.pedido?.id}">${actividadInstance?.pedido?.encodeAsHTML()}</g:link></td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name"><g:message code="actividad.tipoActividad.label" default="Tipo Actividad" /></td>
                            
                            <td valign="top" class="value"><g:link controller="tipoActividad" action="show" id="${actividadInstance?.tipoActividad?.id}">${actividadInstance?.tipoActividad?.encodeAsHTML()}</g:link></td>
                            
                        </tr>
                    
                    </tbody>
                </table>
            </div>
            <div class="buttons">
                <g:form>
                    <g:hiddenField name="id" value="${actividadInstance?.id}" />
                    <span class="button"><g:actionSubmit class="edit" action="edit" value="${message(code: 'default.button.edit.label', default: 'Edit')}" /></span>
                    <span class="button"><g:actionSubmit class="delete" action="delete" value="${message(code: 'default.button.delete.label', default: 'Delete')}" onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');" /></span>
                </g:form>
            </div>
        </div>
    </body>
</html>
