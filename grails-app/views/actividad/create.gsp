

<%@ page import="ar.com.telecom.pcs.entities.Actividad" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="layout" content="main" />
        <g:set var="entityName" value="${message(code: 'actividad.label', default: 'Actividad')}" />
        <title><g:message code="default.create.label" args="[entityName]" /></title>
    </head>
    <body>
        <div class="nav">
            <span class="menuButton"><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></span>
            <span class="menuButton"><g:link class="list" action="list"><g:message code="default.list.label" args="[entityName]" /></g:link></span>
        </div>
        <div class="body">
            <h1><g:message code="default.create.label" args="[entityName]" /></h1>
            <g:if test="${flash.message}">
            <div class="message">${flash.message}</div>
            </g:if>
            <g:hasErrors bean="${actividadInstance}">
            <div class="errors">
                <g:renderErrors bean="${actividadInstance}" as="list" />
            </div>
            </g:hasErrors>
            <g:form action="save" >
                <div class="dialog">
                    <table>
                        <tbody>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="grupoResponsable"><g:message code="actividad.grupoResponsable.label" default="Grupo Responsable" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: actividadInstance, field: 'grupoResponsable', 'errors')}">
                                    <g:textField name="grupoResponsable" maxlength="50" value="${actividadInstance?.grupoResponsable}" />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="legajoUsuarioResponsable"><g:message code="actividad.legajoUsuarioResponsable.label" default="Legajo Usuario Responsable" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: actividadInstance, field: 'legajoUsuarioResponsable', 'errors')}">
                                    <g:textField name="legajoUsuarioResponsable" maxlength="20" value="${actividadInstance?.legajoUsuarioResponsable}" />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="fechaCierre"><g:message code="actividad.fechaCierre.label" default="Fecha Cierre" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: actividadInstance, field: 'fechaCierre', 'errors')}">
                                    <g:datePicker name="fechaCierre" precision="day" value="${actividadInstance?.fechaCierre}" default="none" noSelection="['': '']" />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="codigoCierre"><g:message code="actividad.codigoCierre.label" default="Codigo Cierre" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: actividadInstance, field: 'codigoCierre', 'errors')}">
                                    <g:select name="codigoCierre.id" from="${ar.com.telecom.pcs.entities.CodigoCierre.list()}" optionKey="id" value="${actividadInstance?.codigoCierre?.id}" noSelection="['null': '']" />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="legajoUsuarioCierre"><g:message code="actividad.legajoUsuarioCierre.label" default="Legajo Usuario Cierre" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: actividadInstance, field: 'legajoUsuarioCierre', 'errors')}">
                                    <g:textField name="legajoUsuarioCierre" maxlength="10" value="${actividadInstance?.legajoUsuarioCierre}" />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="comentariosCierre"><g:message code="actividad.comentariosCierre.label" default="Comentarios Cierre" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: actividadInstance, field: 'comentariosCierre', 'errors')}">
                                    <g:textField name="comentariosCierre" value="${actividadInstance?.comentariosCierre}" />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="comentarios"><g:message code="actividad.comentarios.label" default="Comentarios" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: actividadInstance, field: 'comentarios', 'errors')}">
                                    <g:textField name="comentarios" value="${actividadInstance?.comentarios}" />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="fechaSugerida"><g:message code="actividad.fechaSugerida.label" default="Fecha Sugerida" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: actividadInstance, field: 'fechaSugerida', 'errors')}">
                                    <g:datePicker name="fechaSugerida" precision="day" value="${actividadInstance?.fechaSugerida}" default="none" noSelection="['': '']" />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="numeroTicket"><g:message code="actividad.numeroTicket.label" default="Numero Ticket" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: actividadInstance, field: 'numeroTicket', 'errors')}">
                                    <g:textField name="numeroTicket" maxlength="20" value="${actividadInstance?.numeroTicket}" />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="nombreServidor"><g:message code="actividad.nombreServidor.label" default="Nombre Servidor" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: actividadInstance, field: 'nombreServidor', 'errors')}">
                                    <g:textField name="nombreServidor" maxlength="25" value="${actividadInstance?.nombreServidor}" />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="ambiente"><g:message code="actividad.ambiente.label" default="Ambiente" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: actividadInstance, field: 'ambiente', 'errors')}">
                                    <g:textField name="ambiente" maxlength="10" value="${actividadInstance?.ambiente}" />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="numeroReferencia"><g:message code="actividad.numeroReferencia.label" default="Numero Referencia" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: actividadInstance, field: 'numeroReferencia', 'errors')}">
                                    <g:textField name="numeroReferencia" maxlength="40" value="${actividadInstance?.numeroReferencia}" />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="dominioMercury"><g:message code="actividad.dominioMercury.label" default="Dominio Mercury" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: actividadInstance, field: 'dominioMercury', 'errors')}">
                                    <g:textField name="dominioMercury" maxlength="40" value="${actividadInstance?.dominioMercury}" />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="proyectoMercury"><g:message code="actividad.proyectoMercury.label" default="Proyecto Mercury" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: actividadInstance, field: 'proyectoMercury', 'errors')}">
                                    <g:textField name="proyectoMercury" maxlength="40" value="${actividadInstance?.proyectoMercury}" />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="release"><g:message code="actividad.release.label" default="Release" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: actividadInstance, field: 'release', 'errors')}">
                                    <g:select name="release.id" from="${ar.com.telecom.pcs.entities.Release.list()}" optionKey="id" value="${actividadInstance?.release?.id}" noSelection="['null': '']" />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="versionador"><g:message code="actividad.versionador.label" default="Versionador" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: actividadInstance, field: 'versionador', 'errors')}">
                                    <g:select name="versionador.id" from="${ar.com.telecom.pcs.entities.Versionador.list()}" optionKey="id" value="${actividadInstance?.versionador?.id}" noSelection="['null': '']" />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="url"><g:message code="actividad.url.label" default="Url" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: actividadInstance, field: 'url', 'errors')}">
                                    <g:textField name="url" maxlength="250" value="${actividadInstance?.url}" />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="descripcion"><g:message code="actividad.descripcion.label" default="Descripcion" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: actividadInstance, field: 'descripcion', 'errors')}">
                                    <g:textField name="descripcion" value="${actividadInstance?.descripcion}" />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="estado"><g:message code="actividad.estado.label" default="Estado" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: actividadInstance, field: 'estado', 'errors')}">
                                    <g:select name="estado.id" from="${ar.com.telecom.pcs.entities.EstadoActividad.list()}" optionKey="id" value="${actividadInstance?.estado?.id}"  />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="fase"><g:message code="actividad.fase.label" default="Fase" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: actividadInstance, field: 'fase', 'errors')}">
                                    <g:select name="fase.id" from="${ar.com.telecom.pcs.entities.Fase.list()}" optionKey="id" value="${actividadInstance?.fase?.id}"  />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="fechaCreacion"><g:message code="actividad.fechaCreacion.label" default="Fecha Creacion" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: actividadInstance, field: 'fechaCreacion', 'errors')}">
                                    <g:datePicker name="fechaCreacion" precision="day" value="${actividadInstance?.fechaCreacion}"  />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="pedido"><g:message code="actividad.pedido.label" default="Pedido" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: actividadInstance, field: 'pedido', 'errors')}">
                                    <g:select name="pedido.id" from="${ar.com.telecom.pcs.entities.AbstractPedido.list()}" optionKey="id" value="${actividadInstance?.pedido?.id}"  />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="tipoActividad"><g:message code="actividad.tipoActividad.label" default="Tipo Actividad" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: actividadInstance, field: 'tipoActividad', 'errors')}">
                                    <g:select name="tipoActividad.id" from="${ar.com.telecom.pcs.entities.TipoActividad.list()}" optionKey="id" value="${actividadInstance?.tipoActividad?.id}"  />
                                </td>
                            </tr>
                        
                        </tbody>
                    </table>
                </div>
                <div class="buttons">
                    <span class="button"><g:submitButton name="create" class="save" value="${message(code: 'default.button.create.label', default: 'Create')}" /></span>
                </div>
            </g:form>
        </div>
    </body>
</html>
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                  