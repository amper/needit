
<%@ page import="ar.com.telecom.pcs.entities.StockMensualSWF" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="layout" content="main" />
        <g:set var="entityName" value="${message(code: 'stockMensualSWF.label', default: 'StockMensualSWF')}" />
        <title><g:message code="default.list.label" args="[entityName]" /></title>
    </head>
    <body>
        <div class="nav">
            <span class="menuButton"><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></span>
            <span class="menuButton"><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></span>
        </div>
        <div class="body">
            <h1><g:message code="default.list.label" args="[entityName]" /></h1>
            <g:if test="${flash.message}">
            <div class="message">${flash.message}</div>
            </g:if>
            <div class="list">
                <table>
                    <thead>
                        <tr>
                        
                            <g:sortableColumn property="id" title="${message(code: 'stockMensualSWF.id.label', default: 'Id')}" />
                        
                            <g:sortableColumn property="fechaDesde" title="${message(code: 'stockMensualSWF.fechaDesde.label', default: 'Fecha Desde')}" />
                        
                            <g:sortableColumn property="fechaHasta" title="${message(code: 'stockMensualSWF.fechaHasta.label', default: 'Fecha Hasta')}" />
                        
                            <th><g:message code="stockMensualSWF.softwareFactory.label" default="Software Factory" /></th>
                        
                            <g:sortableColumn property="stockMensual" title="${message(code: 'stockMensualSWF.stockMensual.label', default: 'Stock Mensual')}" />
                        
                            <g:sortableColumn property="valor" title="${message(code: 'stockMensualSWF.valor.label', default: 'Valor')}" />
                        
                        </tr>
                    </thead>
                    <tbody>
                    <g:each in="${stockMensualSWFInstanceList}" status="i" var="stockMensualSWFInstance">
                        <tr class="${(i % 2) == 0 ? 'odd' : 'even'}">
                        
                            <td><g:link action="show" id="${stockMensualSWFInstance.id}">${fieldValue(bean: stockMensualSWFInstance, field: "id")}</g:link></td>
                        
                            <td><g:formatDate date="${stockMensualSWFInstance.fechaDesde}" /></td>
                        
                            <td><g:formatDate date="${stockMensualSWFInstance.fechaHasta}" /></td>
                        
                            <td>${fieldValue(bean: stockMensualSWFInstance, field: "softwareFactory")}</td>
                        
                            <td>${fieldValue(bean: stockMensualSWFInstance, field: "stockMensual")}</td>
                        
                            <td>${fieldValue(bean: stockMensualSWFInstance, field: "valor")}</td>
                        
                        </tr>
                    </g:each>
                    </tbody>
                </table>
            </div>
            <div class="paginateButtons">
                <g:paginate total="${stockMensualSWFInstanceTotal}" />
            </div>
        </div>
    </body>
</html>
