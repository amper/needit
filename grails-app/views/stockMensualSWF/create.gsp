

<%@ page import="ar.com.telecom.pcs.entities.StockMensualSWF" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="layout" content="main" />
        <g:set var="entityName" value="${message(code: 'stockMensualSWF.label', default: 'StockMensualSWF')}" />
        <title><g:message code="default.create.label" args="[entityName]" /></title>
    </head>
    <body>
        <div class="nav">
            <span class="menuButton"><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></span>
            <span class="menuButton"><g:link class="list" action="list"><g:message code="default.list.label" args="[entityName]" /></g:link></span>
        </div>
        <div class="body">
            <h1><g:message code="default.create.label" args="[entityName]" /></h1>
            <g:if test="${flash.message}">
            <div class="message">${flash.message}</div>
            </g:if>
            <g:hasErrors bean="${stockMensualSWFInstance}">
            <div class="errors">
                <g:renderErrors bean="${stockMensualSWFInstance}" as="list" />
            </div>
            </g:hasErrors>
            <g:form action="save" >
                <div class="dialog">
                    <table>
                        <tbody>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="fechaDesde"><g:message code="stockMensualSWF.fechaDesde.label" default="Fecha Desde" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: stockMensualSWFInstance, field: 'fechaDesde', 'errors')}">
                                    <g:datePicker name="fechaDesde" precision="day" value="${stockMensualSWFInstance?.fechaDesde}" default="none" noSelection="['': '']" />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="fechaHasta"><g:message code="stockMensualSWF.fechaHasta.label" default="Fecha Hasta" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: stockMensualSWFInstance, field: 'fechaHasta', 'errors')}">
                                    <g:datePicker name="fechaHasta" precision="day" value="${stockMensualSWFInstance?.fechaHasta}" default="none" noSelection="['': '']" />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="softwareFactory"><g:message code="stockMensualSWF.softwareFactory.label" default="Software Factory" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: stockMensualSWFInstance, field: 'softwareFactory', 'errors')}">
                                    <g:select name="softwareFactory.id" from="${ar.com.telecom.pcs.entities.SoftwareFactory.list()}" optionKey="id" value="${stockMensualSWFInstance?.softwareFactory?.id}"  />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="stockMensual"><g:message code="stockMensualSWF.stockMensual.label" default="Stock Mensual" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: stockMensualSWFInstance, field: 'stockMensual', 'errors')}">
                                    <g:textField name="stockMensual" value="${fieldValue(bean: stockMensualSWFInstance, field: 'stockMensual')}" />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="valor"><g:message code="stockMensualSWF.valor.label" default="Valor" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: stockMensualSWFInstance, field: 'valor', 'errors')}">
                                    <g:textField name="valor" value="${fieldValue(bean: stockMensualSWFInstance, field: 'valor')}" />
                                </td>
                            </tr>
                        
                        </tbody>
                    </table>
                </div>
                <div class="buttons">
                    <span class="button"><g:submitButton name="create" class="save" value="${message(code: 'default.button.create.label', default: 'Create')}" /></span>
                </div>
            </g:form>
        </div>
    </body>
</html>
