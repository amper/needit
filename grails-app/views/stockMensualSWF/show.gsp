
<%@ page import="ar.com.telecom.pcs.entities.StockMensualSWF" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="layout" content="main" />
        <g:set var="entityName" value="${message(code: 'stockMensualSWF.label', default: 'StockMensualSWF')}" />
        <title><g:message code="default.show.label" args="[entityName]" /></title>
    </head>
    <body>
        <div class="nav">
            <span class="menuButton"><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></span>
            <span class="menuButton"><g:link class="list" action="list"><g:message code="default.list.label" args="[entityName]" /></g:link></span>
            <span class="menuButton"><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></span>
        </div>
        <div class="body">
            <h1><g:message code="default.show.label" args="[entityName]" /></h1>
            <g:if test="${flash.message}">
            <div class="message">${flash.message}</div>
            </g:if>
            <div class="dialog">
                <table>
                    <tbody>
                    
                        <tr class="prop">
                            <td valign="top" class="name"><g:message code="stockMensualSWF.id.label" default="Id" /></td>
                            
                            <td valign="top" class="value">${fieldValue(bean: stockMensualSWFInstance, field: "id")}</td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name"><g:message code="stockMensualSWF.fechaDesde.label" default="Fecha Desde" /></td>
                            
                            <td valign="top" class="value"><g:formatDate date="${stockMensualSWFInstance?.fechaDesde}" /></td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name"><g:message code="stockMensualSWF.fechaHasta.label" default="Fecha Hasta" /></td>
                            
                            <td valign="top" class="value"><g:formatDate date="${stockMensualSWFInstance?.fechaHasta}" /></td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name"><g:message code="stockMensualSWF.softwareFactory.label" default="Software Factory" /></td>
                            
                            <td valign="top" class="value"><g:link controller="softwareFactory" action="show" id="${stockMensualSWFInstance?.softwareFactory?.id}">${stockMensualSWFInstance?.softwareFactory?.encodeAsHTML()}</g:link></td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name"><g:message code="stockMensualSWF.stockMensual.label" default="Stock Mensual" /></td>
                            
                            <td valign="top" class="value">${fieldValue(bean: stockMensualSWFInstance, field: "stockMensual")}</td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name"><g:message code="stockMensualSWF.valor.label" default="Valor" /></td>
                            
                            <td valign="top" class="value">${fieldValue(bean: stockMensualSWFInstance, field: "valor")}</td>
                            
                        </tr>
                    
                    </tbody>
                </table>
            </div>
            <div class="buttons">
                <g:form>
                    <g:hiddenField name="id" value="${stockMensualSWFInstance?.id}" />
                    <span class="button"><g:actionSubmit class="edit" action="edit" value="${message(code: 'default.button.edit.label', default: 'Edit')}" /></span>
                    <span class="button"><g:actionSubmit class="delete" action="delete" value="${message(code: 'default.button.delete.label', default: 'Delete')}" onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');" /></span>
                </g:form>
            </div>
        </div>
    </body>
</html>
