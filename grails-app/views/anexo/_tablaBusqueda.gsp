<table>
	<thead>
		<tr>
			<g:sortableColumn property="tipoAnexo.fase" title="Fase" params="${params}" action="list"/>
			<g:sortableColumn property="tipoAnexo" title="Tipo de anexo" params="${params}" action="list"/>
			<g:sortableColumn property="descripcion" title="Descripci&oacute;n" params="${params}" action="list"/>
			<g:sortableColumn property="sistema" title="Sistema" params="${params}" action="list"/>
			<g:sortableColumn property="nombreArchivo" title="Documento" params="${params}" action="list"/>
			<g:sortableColumn property="fecha" title="Fecha" params="${params}" action="list"/>
			<g:sortableColumn property="legajo" title="Usuario" params="${params}" action="list"/>
			<g:sortableColumn property="rol" title="Rol" params="${params}" action="list"/>
			<th>Eliminar</th>
		</tr>
	</thead>
	<tbody> 
		<g:if test="${listadoAnexos}">
			<g:each in="${listadoAnexos}" status="i" var="listadoAnexosInstance">
				<g:set scope="page" var="elem" value="${listadoAnexosInstance}" />
				<tr class="${(i % 2) == 0 ? 'odd' : 'even'}" style="cursor: pointer;">
					<td>
						${elem.esGlobal() ? "Global" : elem?.tipoAnexo?.fase}
					</td>
					<td>
						${elem?.tipoAnexo}
					</td>
					<td>
						${ elem?.descripcion }
					</td>
					<td>
						&nbsp; <%-- DEFINIR DE DONDE SALE EL SISTEMA --%>
					</td>
					<td class="pAnexo">
						<g:link action="verAnexo" target="_blank" params="[anexoId: elem.id, pedidoId: pedidoInstance?.id]" >
							${ elem?.nombreArchivo.replace(" "," <br>").replace("_","_<br>") }${ elem?.extension }
						</g:link>
					</td>
					<td>
						<g:formatDate date="${elem.fecha}" type="datetime" style="MEDIUM"/> 
					</td>
					<td>
						 <g:userDescription legajo="${elem?.legajo}" pedido="${elem.id }"/>
					</td>
					<td>
						${pedidoInstance.getRolUsuario(elem?.tipoAnexo?.fase, elem?.legajo)}
					</td>
					<td>
						<g:if test="${pedidoInstance.puedeEliminarAnexoGlobal(elem,usuarioLogueado) && elem.esGlobal()}">
							<a href="javascript:confirmaBorrarAnexoGlobal('${createLink(action: 'eliminarAnexo', params:[pedidoId: pedidoInstance?.id, anexoId: elem.id]) }')">
								<img src="${resource(dir:'images',file:'delete.gif')}" width="10" height="10" />
							</a>
						</g:if>
					</td>
				</tr>
			</g:each>
		</g:if>
		<g:else>
			<tr class="odd" style="cursor: pointer;">
				<td colspan="9" style="text-align: center; font-style: italic;">No hay Anexos para este pedido</td>
			</tr>
		</g:else>
	</tbody>
	 <%-- TODO: EXPORTAR A EXCEL --%>
</table>
<g:if test="${listadoAnexos}">
	<div class="paginateButtons">
		<g:paginate params="${params}" action="list" id="${pedidoInstance?.id}" total="${listadoAnexos.totalCount}"/>
	</div>
</g:if>
<g:if test="${listadoAnexos}">
	<export:formats formats="['excel', 'pdf']" action="exportFile" params="${params}" />
</g:if>
