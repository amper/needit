<%@page import="org.codehaus.groovy.grails.web.servlet.mvc.SynchronizerToken"%>
<html>
<head>
<title>NeedIT</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<link rel="shortcut icon"
	href="${resource(dir:'images',file:'LogoTelecom.jpg')}"
	type="image/x-icon" />

<!--css-->
<link rel="stylesheet" href="${resource(dir:'css/themes/custom-theme',file:'jquery-ui-1.8.16.custom.css')}" />
<link rel="stylesheet" href="${resource(dir:'css/themes/custom-theme',file:'jquery.multiselect.css')}" />
<link rel="stylesheet" href="${resource(dir:'css/themes/custom-theme',file:'jquery.multiselect.filter.css')}" />
<link rel="stylesheet" href="${resource(dir:'css',file:'mainApp.css')}" />

<!--js-->
 
<g:javascript library="jquery-1.6.2" />
<g:javascript library="jquery-ui-custom.min" />
<g:javascript library="jquery.multiselect" />
<g:javascript library="jquery.multiselect.filter" />
<g:javascript library="common" />
<g:javascript library="common-widget" />
<g:javascript library="prototype" />
<g:javascript library="submitSpinner" />
<g:javascript library="DateUtil" />
<g:javascript library="anexo" />
<g:javascript library="boxUser" />
<g:javascript library="anexoPorTipo" />
<g:javascript library="checkBoxList" />
<g:javascript library="synchronizerToken" />

<script type="text/javascript">
	var gNameApp = 'needIt';
	var gTokenKey = '${SynchronizerToken.KEY}';
</script>

</head>

<body onunload="cierreVentana('anexo','${stringParams}');">
	<img id="spinner" src="<g:createLinkTo dir='/images' file='ajax-spinner.gif'/>" />
	<div id="ContenedorGeneral">
		<div id="ContenedorPrincipal">
		<div>
			<p class="tituloPrincipal">Anexos</p>
		</div>
		
		<div id="contenedorIzqPopUp">
			<!-- Campos de busqueda -->
			<g:uploadForm name="formListAnexo" id="formListAnexo" class="formGeneral" action="buscarAnexo" useToken="true">
				<g:hiddenField name="pedidoId" id="pedidoId" value="${params?.pedidoId}" />
				<g:hiddenField name="hVengoPorAnexos" value="${params.hVengoPorAnexos}" />
				<div class="seccionHide">
					<g:render template="/templates/encabezadoMinimo" model="['pedidoInstance':pedidoInstance]"></g:render>
					<p class="tituloSeccion">Filtros</p>

						<div id="anexoMain" class="tag-dosDiv">

							<div class="columna1">
								
								<div>
									<label for='comboboxFase'>Fase</label><br>
									<g:select id="comboboxFase" name="fase" from="${ar.com.telecom.pcs.entities.Fase.list()}" optionKey="id" />
								</div>
								<div>
									<label for='comboboxTiposAnexos'>Tipo de anexo</label><br />
									<g:select id="comboboxTiposAnexos" name="anexoTipo" from="${ar.com.telecom.pcs.entities.TipoAnexo.list().unique()}" optionKey="id" />
								</div>
								<div>
									<label for='comboboxRolAplicacion'>Rol</label><br />
									<g:select id="comboboxRolAplicacion" name="rolAplicacion" from="${ar.com.telecom.pcs.entities.RolAplicacion.list()}" optionKey="id" />
								</div>

							</div>

							<div class="columna1">
								<div>
									<label for='descripcion'>Descripci&oacute;n contiene</label><br>
									<g:textField name="descripcion" class="formInput" style="margin-left: 0px; width: 60%;" id='descripcion' value="${params?.descripcion?.trim()}"  maxLength="255"/>
								</div>
								<div class="pDos" style="width: 100%; margin-left: 0px;">
									<div style="float: left;">
										<label for='fechaDesde'>Fecha desde</label><br>
										<g:textField name="fechaDesde" class="formInput date" style="margin-left: 0px; width: 100px;" id='datepicker-fechaDesde-rpid' maxLength="10" />
										<g:hiddenField name="fechaDesdeHidden" value="${params?.fechaDesde?.format('dd/MM/yyyy')}" />
									</div>
									<div style="float: left;">
										<label for='fechaHasta'>Fecha hasta</label><br>
										<g:textField name="fechaHasta" class="formInput date" style="margin-left: 0px; width: 100px;" id='datepicker-fechaHasta-rpid' maxLength="10" />
										<g:hiddenField name="fechaHastaHidden" value="${params?.fechaHasta?.format('dd/MM/yyyy')}" />
									</div>
								</div>
								<div style="width: 80%;">
									<label for='usuario'>Usuario</label>
									<div id="legajoUsuario" class="boxUser">
										<g:render template="/templates/userBox"	model="['person':params?.legajoUsuario, 'box':'legajoUsuario', 'personsAutocomplete': personsAutocomplete, 'pedidoID':pedidoInstance?.id]"></g:render>
									</div>
									<g:secureRemoteLink title="Eliminar" id="historial" class="limpiarBox" controller="anexo" update="legajoUsuario"	params="['username': '', 'box':'legajoUsuario', 'pedidoId':pedidoInstance?.id]" action="completarUsuario"></g:secureRemoteLink>
								</div>
							</div>	

						</div>
						
						<div class="pCompletoHistorial">
							<g:submitToRemoteSecure update="list" id="busqueda" onLoading="showSpinner('spinner', true);" onComplete="showSpinner('spinner', false);" action="buscarAnexo" value="Buscar" class="formSeccionSubmitButtonHistorial ui-button ui-widget ui-state-default ui-corner-all ui-state-hover" />
							<button class="formSeccionSubmitButtonHistorial ui-button ui-widget ui-state-default ui-corner-all ui-state-hover" onclick="clear_form_elements();"/>Limpiar</button>
							<button id="nuevoAnexoGlobal" class="formSeccionSubmitButtonHistorial ui-button ui-widget ui-state-default ui-corner-all ui-state-hover">Agregar anexo global</button>
							<g:hiddenField name="hForm" id="hForm" value="formListAnexo"/><!-- Necesario para crear un anexo -->		
						</div>
				</div>
			</g:uploadForm>
			
			<div id="list" class="list"></div>
			<script type="text/javascript">
				new Ajax.Updater(list, '/' + gNameApp + '/anexo/buscarAnexo/busqueda',{
					asynchronous:true,
					evalScripts:true,
					parameters:{
						${stringParams}
					}
				});
			</script>
		</div> <!-- fin contenedor izq -->
		
		
		</div>
	</div>
	
	<div id="dialog-anexos" title="Nuevo Anexo" style="display: none;"></div>
	
<%--	<g:render template="/templates/anexoPorTipo"--%>
<%--		model="['person':pedidoInstance?.legajoUsuarioCreador?pedidoInstance.legajoUsuarioCreador:userSesion?.legajo, 'box':'legajoUsuarioCreador', 'pedidoInstance': pedidoInstance]"></g:render>--%>
</body>
</html>