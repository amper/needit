
<%@ page import="ar.com.telecom.pcs.entities.SoftwareFactory" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="layout" content="main" />
        <g:set var="entityName" value="${message(code: 'softwareFactory.label', default: 'SoftwareFactory')}" />
        <title><g:message code="default.list.label" args="[entityName]" /></title>
    </head>
    <body>
        <div class="nav">
            <span class="menuButton"><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></span>
            <span class="menuButton"><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></span>
        </div>
        <div class="body">
            <h1><g:message code="default.list.label" args="[entityName]" /></h1>
            <g:if test="${flash.message}">
            <div class="message">${flash.message}</div>
            </g:if>
            <div class="list">
                <table>
                    <thead>
                        <tr>
                        
                            <g:sortableColumn property="id" title="${message(code: 'softwareFactory.id.label', default: 'Id')}" />
                        
                            <g:sortableColumn property="descripcion" title="${message(code: 'softwareFactory.descripcion.label', default: 'Descripcion')}" />
                        
                            <g:sortableColumn property="responsableCargaCapacidad" title="${message(code: 'softwareFactory.responsableCargaCapacidad.label', default: 'Responsable Carga Capacidad')}" />
                        
                            <g:sortableColumn property="grupoLDAP" title="${message(code: 'softwareFactory.grupoLDAP.label', default: 'Grupo LDAP')}" />
                        
                            <g:sortableColumn property="asociadoSistema" title="${message(code: 'softwareFactory.asociadoSistema.label', default: 'Asociado Sistema')}" />
                        
                            <g:sortableColumn property="gestionaCapacidad" title="${message(code: 'softwareFactory.gestionaCapacidad.label', default: 'Gestiona Capacidad')}" />
                        
                        </tr>
                    </thead>
                    <tbody>
                    <g:each in="${softwareFactoryInstanceList}" status="i" var="softwareFactoryInstance">
                        <tr class="${(i % 2) == 0 ? 'odd' : 'even'}">
                        
                            <td><g:link action="show" id="${softwareFactoryInstance.id}">${fieldValue(bean: softwareFactoryInstance, field: "id")}</g:link></td>
                        
                            <td>${fieldValue(bean: softwareFactoryInstance, field: "descripcion")}</td>
                        
                            <td>${fieldValue(bean: softwareFactoryInstance, field: "responsableCargaCapacidad")}</td>
                        
                            <td>${fieldValue(bean: softwareFactoryInstance, field: "grupoLDAP")}</td>
                        
                            <td><g:formatBoolean boolean="${softwareFactoryInstance.asociadoSistema}" /></td>
                        
                            <td><g:formatBoolean boolean="${softwareFactoryInstance.gestionaCapacidad}" /></td>
                        
                        </tr>
                    </g:each>
                    </tbody>
                </table>
            </div>
            <div class="paginateButtons">
                <g:paginate total="${softwareFactoryInstanceTotal}" />
            </div>
        </div>
    </body>
</html>
