

<%@ page import="ar.com.telecom.pcs.entities.SoftwareFactory" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="layout" content="main" />
        <g:set var="entityName" value="${message(code: 'softwareFactory.label', default: 'SoftwareFactory')}" />
        <title><g:message code="default.edit.label" args="[entityName]" /></title>
    </head>
    <body>
        <div class="nav">
            <span class="menuButton"><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></span>
            <span class="menuButton"><g:link class="list" action="list"><g:message code="default.list.label" args="[entityName]" /></g:link></span>
            <span class="menuButton"><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></span>
        </div>
        <div class="body">
            <h1><g:message code="default.edit.label" args="[entityName]" /></h1>
            <g:if test="${flash.message}">
            <div class="message">${flash.message}</div>
            </g:if>
            <g:hasErrors bean="${softwareFactoryInstance}">
            <div class="errors">
                <g:renderErrors bean="${softwareFactoryInstance}" as="list" />
            </div>
            </g:hasErrors>
            <g:form method="post" >
                <g:hiddenField name="id" value="${softwareFactoryInstance?.id}" />
                <g:hiddenField name="version" value="${softwareFactoryInstance?.version}" />
                <div class="dialog">
                    <table>
                        <tbody>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                  <label for="descripcion"><g:message code="softwareFactory.descripcion.label" default="Descripcion" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: softwareFactoryInstance, field: 'descripcion', 'errors')}">
                                    <g:textField name="descripcion" maxlength="70" value="${softwareFactoryInstance?.descripcion}" />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                  <label for="responsableCargaCapacidad"><g:message code="softwareFactory.responsableCargaCapacidad.label" default="Responsable Carga Capacidad" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: softwareFactoryInstance, field: 'responsableCargaCapacidad', 'errors')}">
                                    <g:textField name="responsableCargaCapacidad" maxlength="20" value="${softwareFactoryInstance?.responsableCargaCapacidad}" />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                  <label for="grupoLDAP"><g:message code="softwareFactory.grupoLDAP.label" default="Grupo LDAP" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: softwareFactoryInstance, field: 'grupoLDAP', 'errors')}">
                                    <g:textField name="grupoLDAP" maxlength="50" value="${softwareFactoryInstance?.grupoLDAP}" />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                  <label for="asociadoSistema"><g:message code="softwareFactory.asociadoSistema.label" default="Asociado Sistema" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: softwareFactoryInstance, field: 'asociadoSistema', 'errors')}">
                                    <g:checkBox name="asociadoSistema" value="${softwareFactoryInstance?.asociadoSistema}" />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                  <label for="gestionaCapacidad"><g:message code="softwareFactory.gestionaCapacidad.label" default="Gestiona Capacidad" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: softwareFactoryInstance, field: 'gestionaCapacidad', 'errors')}">
                                    <g:checkBox name="gestionaCapacidad" value="${softwareFactoryInstance?.gestionaCapacidad}" />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                  <label for="manoObraPropia"><g:message code="softwareFactory.manoObraPropia.label" default="Mano Obra Propia" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: softwareFactoryInstance, field: 'manoObraPropia', 'errors')}">
                                    <g:checkBox name="manoObraPropia" value="${softwareFactoryInstance?.manoObraPropia}" />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                  <label for="sistemas"><g:message code="softwareFactory.sistemas.label" default="Sistemas" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: softwareFactoryInstance, field: 'sistemas', 'errors')}">
                                    
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                  <label for="stockMensualHoras"><g:message code="softwareFactory.stockMensualHoras.label" default="Stock Mensual Horas" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: softwareFactoryInstance, field: 'stockMensualHoras', 'errors')}">
                                    <g:select name="stockMensualHoras" from="${ar.com.telecom.pcs.entities.StockMensualSWF.list()}" multiple="yes" optionKey="id" size="5" value="${softwareFactoryInstance?.stockMensualHoras*.id}" />
                                </td>
                            </tr>
                        
                        </tbody>
                    </table>
                </div>
                <div class="buttons">
                    <span class="button"><g:actionSubmit class="save" action="update" value="${message(code: 'default.button.update.label', default: 'Update')}" /></span>
                    <span class="button"><g:actionSubmit class="delete" action="delete" value="${message(code: 'default.button.delete.label', default: 'Delete')}" onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');" /></span>
                </div>
            </g:form>
        </div>
    </body>
</html>
