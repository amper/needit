
<%@ page import="ar.com.telecom.pcs.entities.AreaSoporteAprobarFase" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="layout" content="main" />
        <g:set var="entityName" value="${message(code: 'areaSoporteAprobarFase.label', default: 'AreaSoporteAprobarFase')}" />
        <title><g:message code="default.list.label" args="[entityName]" /></title>
    </head>
    <body>
        <div class="nav">
            <span class="menuButton"><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></span>
            <span class="menuButton"><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></span>
        </div>
        <div class="body">
            <h1><g:message code="default.list.label" args="[entityName]" /></h1>
            <g:if test="${flash.message}">
            <div class="message">${flash.message}</div>
            </g:if>
            <div class="list">
                <table>
                    <thead>
                        <tr>
                        
                            <g:sortableColumn property="id" title="${message(code: 'areaSoporteAprobarFase.id.label', default: 'Id')}" />
                        
                            <th><g:message code="areaSoporteAprobarFase.areaSoporte.label" default="Area Soporte" /></th>
                        
                            <th><g:message code="areaSoporteAprobarFase.fase.label" default="Fase" /></th>
                        
                        </tr>
                    </thead>
                    <tbody>
                    <g:each in="${areaSoporteAprobarFaseInstanceList}" status="i" var="areaSoporteAprobarFaseInstance">
                        <tr class="${(i % 2) == 0 ? 'odd' : 'even'}">
                        
                            <td><g:link action="show" id="${areaSoporteAprobarFaseInstance.id}">${fieldValue(bean: areaSoporteAprobarFaseInstance, field: "id")}</g:link></td>
                        
                            <td>${fieldValue(bean: areaSoporteAprobarFaseInstance, field: "areaSoporte")}</td>
                        
                            <td>${fieldValue(bean: areaSoporteAprobarFaseInstance, field: "fase")}</td>
                        
                        </tr>
                    </g:each>
                    </tbody>
                </table>
            </div>
            <div class="paginateButtons">
                <g:paginate total="${areaSoporteAprobarFaseInstanceTotal}" />
            </div>
        </div>
    </body>
</html>
