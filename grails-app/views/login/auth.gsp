<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta name="layout" content="layoutLogin" />
</head> 
<body>
	<script language="javascript">
function login() {
	document.getElementById('j_username').value = document.getElementById('j_username').value.toLowerCase();
<%--	document.getElementById('loginForm').submit();--%>
}
</script>
	<web:isBlackberry>
		<form action="${postUrl}" method="POST" id="loginForm">
			<table id="mobile-body-content-form-login" cellspacing="5"
				cellpadding="5">
				<tr>
					<td class="loginForm-labels"><label for="j_username">Usuario:</label>
					</td>
					<td class="loginForm-inputs"><input type="text"
						name="j_username" id="j_username" value="${request.remoteUser}"
						class="loginForm-input" />
					</td>
				</tr>
				<tr>
					<td class="loginForm-labels"><label for="j_password">Contrase&ntilde;a:</label>
					</td>
					<td class="loginForm-inputs"><input type="password"
						name="j_password" id="j_password" class="loginForm-input" />
					</td>
				</tr>
				<tr>
					<td colspan="2" class="loginForm-inputs"><input type="submit"
						id="buttonLogin" value="Entrar" class="loginForm-button"
						onclick="loginOnEnter();" /></td>
				</tr>
				<g:if test="${flash.message}">
					<tr>
						<td colspan="2" class="login_message">
							${flash.message}
						</td>
					</tr>
				</g:if>
			</table>
		</form>
	</web:isBlackberry>
	<web:isNOTBlackberry>
		<web:isNOTMsie>
			<div class="contenidoError">Ingrese por navegador corporativo
				Internet Explorer</div>
		</web:isNOTMsie>
		<web:isMsie>
	
		<div id="Login">
			<form action='${postUrl}' method='POST' id='loginForm' class='cssform'>
				<p>
					<label for='j_username'>Usuario</label> <input type='text'
						class='text_' name='j_username' id='j_username'
						value='${request.remoteUser}' style="backgraound-color: #C1D5D9;"
						onfocus="this.style.backgroundColor='#FFF'; this.style.color='#000';"
						onblur="this.style.backgroundColor='#C1D5D9'; this.style.color='#000';"
						/>
				</p>
				<p>
					<label for='j_password'>Contrase&ntilde;a</label>
					 <input
						type='password' class='text_' name='j_password' id='j_password'
						style="background-color: #C1D5D9;"
						onfocus="this.style.backgroundColor='#FFF'; this.style.color='#000';"
						onblur="this.style.backgroundColor='#C1D5D9'; this.style.color='#000';" 
						/>
				</p>
				<div style="padding-top: 15px;">
					<input type="image" src="../images/login.png" alt="Ingresar al sistema" onclick="javascript:login();"/>
				</div>
			</form>
			<g:if test='${flash.message}'>
				<div class='login_message'>
					${flash.message}
				</div>
			</g:if>
		</div>
		</web:isMsie>
	</web:isNOTBlackberry>
</body>
</html>
