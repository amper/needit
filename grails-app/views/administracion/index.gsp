<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
<meta name="layout" content="layoutAdmin"/>
<title>needIt :: Administracion</title>
</head>
<body>

  <div class="ayuda-body">
    <h1>Administraci&oacute;n:</h1>
    <ul>
<g:each in="${listaControllers}" var="item">
  <g:unless test="${item.logicalPropertyName.equalsIgnoreCase('administracion')}">
      <li><g:link controller="${item.logicalPropertyName}">${item.logicalPropertyName}</g:link></li>
  </g:unless>
</g:each>
    </ul>
  </div>
 
</body>
</html>