package pcs

import grails.test.*

class PedidoServiceTests extends GroovyTestCase {
	
	def pedidoService

	protected void tearDown() {
		super.tearDown()
	}

	void testGetRolesGrupo() {
		
		def params = []
		def legajo = "u546049"
		
		def roles = pedidoService.getRolesGrupos(params, legajo)
		
		assertNotNull roles
		
		assertEquals 3, roles.size()
	}
}
