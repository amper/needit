package ar.com.telecom.pcs.entities

import grails.test.*
import junit.framework.Assert

class ValidacionCamposCruzadosControllerTests extends ControllerUnitTestCase {
	Pedido pedidoOk
	Pedido pedidoMal
	Actividad actividadOk
	Actividad actividadMal
	ValidacionCamposCruzados validator
	
    protected void setUp() {
        super.setUp()
		pedidoOk = new Pedido()
		pedidoOk.titulo = "Titulo"
		pedidoMal = new Pedido()
		validator = new ValidacionCamposCruzados()
		validator.agregarCampoAValidar("comentarios")
		validator.agregarCampoAValidar("fechaSugerida")
		validator.agregarCampoAValidar("numeroTicket")
		actividadOk = new Actividad(pedido: pedidoOk, fechaSugerida: new Date())
		actividadMal = new Actividad(pedido: pedidoMal)
    }

    protected void tearDown() {
        super.tearDown()
    }

	/**
	 * Validación correcta de un pedido
	 */
    void testValidacionOk() {
		Assert.assertTrue(validator.doValidar(actividadOk))
    }
	
	/**
	* Validación fallida de un pedido
	*/
   void testValidacionMal() {
	   Assert.assertFalse(validator.doValidar(actividadMal))
   }
}
