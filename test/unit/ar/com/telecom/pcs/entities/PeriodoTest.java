package ar.com.telecom.pcs.entities;

import grails.test.GrailsUnitTestCase;

import java.util.Date;

import junit.framework.Assert;

public class PeriodoTest extends GrailsUnitTestCase{

	private Periodo diciembre2011;
	private Periodo enero2012;
	
	
	public void setUp() {
		//mockDomain(Periodo)
		diciembre2011 = new Periodo();
		diciembre2011.setMes(12);
		diciembre2011.setAnio(2011);
		enero2012 = new Periodo();
		enero2012.setMes(1);
		enero2012.setAnio(2012);
	}

	
	public void testBetween() {
		Assert.assertTrue(diciembre2011.between(new Date(1, 1, 106), new Date(1, 1, 120)));
		Assert.assertFalse(diciembre2011.between(new Date(1, 1, 112), new Date(1, 1, 120)));
	}

	
	public void testGetPeriodos() {
		//fail("Not yet implemented");
	}

	
	public void testEqualsObject() {
		//fail("Not yet implemented");
	}

}
