SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Dodain
-- Create date: 08/11/2012
-- Description:	Cancela Pedido
-- =============================================
IF EXISTS (SELECT name 
	   FROM   sysobjects 
	   WHERE  name = N'asociarDireccionesUsuario' 
	   AND 	  type = 'P')
    DROP PROCEDURE asociarDireccionesUsuario
GO

CREATE PROCEDURE asociarDireccionesUsuario(@MAX_JERARQUIA INT)
AS

SET NOCOUNT ON

/*
DECLARE @MAX_JERARQUIA INT 

SET @MAX_JERARQUIA = '040'
*/

SELECT usuarios.legajo as legajo, 
       gerencias.codigo as direccion
  INTO #UsuariosDireccion
  FROM ume..tbUsuarios usuarios,
       ume..tbEstructuras gerencias
 WHERE usuarios.gerencia_id = gerencias.id
   AND gerencias.codigo_direccion IS NULL
   AND gerencias.jerarquia > @MAX_JERARQUIA
   
CREATE TABLE #tmpJerarquias
(jerarquia varchar(5) not null)
   
INSERT INTO #tmpJerarquias
SELECT DISTINCT 
       gerencias.jerarquia
  FROM ume..tbEstructuras gerencias
 WHERE gerencias.codigo_direccion IS NULL
   AND gerencias.jerarquia > @MAX_JERARQUIA

DECLARE @Jerarquia AS varchar(5)

SELECT TOP 1 @Jerarquia = jerarquia
  FROM #tmpJerarquias 
 ORDER BY jerarquia DESC
  
WHILE (@@ROWCOUNT > 0)
BEGIN
	UPDATE #UsuariosDireccion 
	SET direccion = direcciones.codigo
	FROM ume..tbEstructuras gerencias
	     JOIN ume..tbEstructuras direcciones 
	     ON gerencias.codigo_padre = direcciones.codigo
	WHERE #UsuariosDireccion.direccion = gerencias.codigo
	AND gerencias.codigo_direccion IS NULL
	AND gerencias.jerarquia = @jerarquia
	AND direcciones.jerarquia > @MAX_JERARQUIA
	
	
	DELETE 
	FROM #tmpJerarquias
	WHERE jerarquia = @Jerarquia
	
	SELECT TOP 1 @Jerarquia = jerarquia
	FROM #tmpJerarquias 
	ORDER BY jerarquia DESC	
	
END

SELECT *
  FROM #UsuariosDireccion

