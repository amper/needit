SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Dodain
-- Create date: 08/11/2012
-- Description:	Cancela Pedido
-- =============================================
IF EXISTS (SELECT name 
	   FROM   sysobjects 
	   WHERE  name = N'cancelarPedido' 
	   AND 	  type = 'P')
    DROP PROCEDURE cancelarPedido
GO

CREATE PROCEDURE cancelarPedido 
	@pedido_id INT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON

    DECLARE @CANCELADO VARCHAR
    DECLARE @PENDIENTE VARCHAR
    DECLARE @FECHA_CANCELACION DATE
    DECLARE @CANTIDAD_PEDIDOS INT
    
    SET @CANTIDAD_PEDIDOS = (       
    SELECT COUNT(*) 
      FROM abstract_pedido
     WHERE ID = @pedido_id)
      
    IF (@CANTIDAD_PEDIDOS = 0) 
    BEGIN
       RAISERROR (N'NO EXISTE EL PEDIDO %d',
					15,
					1,
					@pedido_id)
	   WITH SETERROR
	   
	   RETURN
    END

    CREATE TABLE #PedidosRelacionados
    (Pedido_id INT)
    
    INSERT INTO #PedidosRelacionados
    VALUES (@pedido_id)
    
    INSERT INTO #PedidosRelacionados
    SELECT ap.id
      FROM abstract_pedido ap
     WHERE ap.pedido_padre_id = @pedido_id
    
    SET @CANCELADO = 'K'
    SET @PENDIENTE = 'P'
    SET @FECHA_CANCELACION = GETDATE()
	
    UPDATE abstract_pedido
       SET fecha_cancelacion = @FECHA_CANCELACION
     WHERE id IN (SELECT Pedido_id FROM #PedidosRelacionados)

    UPDATE aprobacion
       SET estado = @CANCELADO
     WHERE pedido_id IN (SELECT Pedido_id FROM #PedidosRelacionados)
       AND estado = @PENDIENTE

    INSERT INTO log_modificaciones 
        (version, 
        descripcion_evento, 
        fase_id, 
        fecha_desde, 
        fecha_hasta, 
        pedido_id, 
        tipo_evento, 
        log_modificaciones_idx)
     SELECT         
        0, 
        'Cancelación del pedido', 
        (select fase_actual_id from abstract_pedido where id = pr.Pedido_id), 
	    @FECHA_CANCELACION,
        @FECHA_CANCELACION,
        pr.Pedido_id,
        @CANCELADO,
        (select ISNULL(MAX(log_modificaciones_idx) + 1, 0) from log_modificaciones where pedido_id = pr.Pedido_id)
     FROM #PedidosRelacionados pr

END
GO
