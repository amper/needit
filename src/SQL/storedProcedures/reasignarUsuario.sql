USE [needItPI]
GO
/****** Object:  StoredProcedure [dbo].[cancelarPedido]    Script Date: 12/20/2012 10:18:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==================================================================================================
-- Author:		Dodain
-- Create date: 20/12/2012
-- Description:	Reasigna un usuario en un pedido puntual, si necesita hacerse para pedidos hijos hay
--              que ejecutar este stored procedure para cada hijo particular
-- ==================================================================================================
IF EXISTS (SELECT name 
	   FROM   sysobjects 
	   WHERE  name = N'reasignarUsuario' 
	   AND 	  type = 'P')
    DROP PROCEDURE reasignarUsuario
GO

CREATE PROCEDURE reasignarUsuario
	@pedido_id INT,
	@usuario_old VARCHAR(255),
	@usuario_new VARCHAR(255),
	@rol VARCHAR(255)  -- OPCIONAL

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON

    DECLARE @PENDIENTE VARCHAR
    DECLARE @CANTIDAD_PEDIDO INT
    DECLARE @NOMBRE_OLD VARCHAR(255)
    DECLARE @NOMBRE_NEW VARCHAR(255)
	DECLARE @rolDescripcion VARCHAR(255)
	
	DECLARE @EVENTO_REASIGNACION VARCHAR(2)
	DECLARE @FECHA_REASIGNACION DATETIME
    
	-- VARIABLES DEL CURSOR DE APROBACIONES
	DECLARE @codigoRol VARCHAR(255)
	DECLARE @aprobacion_id INT

    SET @PENDIENTE = 'P'      
	SET @FECHA_REASIGNACION = GETDATE()
	SET @EVENTO_REASIGNACION = 'H'

    IF (@usuario_old IS NULL)
    BEGIN
       RAISERROR (N'DEBE INGRESAR EL USUARIO ORIGINAL',
					15,
					1)
	   WITH SETERROR
	   
	   RETURN
    END

    IF (@usuario_new IS NULL)
    BEGIN
       RAISERROR (N'DEBE INGRESAR EL USUARIO NUEVO',
					15,
					1)
	   WITH SETERROR
	   
	   RETURN
    END
    
    SET @CANTIDAD_PEDIDO = (       
    SELECT COUNT(*) 
      FROM abstract_pedido
     WHERE ID = @pedido_id)
     
    IF (@CANTIDAD_PEDIDO = 0) 
    BEGIN
       RAISERROR (N'NO EXISTE EL PEDIDO %d',
					15,
					1,
					@pedido_id)
	   WITH SETERROR
	   
	   RETURN
    END

    SET @NOMBRE_OLD = (       
    SELECT nombre 
      FROM ume..tbUsuarios usr
     WHERE usr.legajo = @usuario_old)
     
    IF (@NOMBRE_OLD IS NULL) 
    BEGIN
       RAISERROR (N'NO EXISTE EL LEGAJO %s',
					15,
					1,
					@usuario_old)
	   WITH SETERROR
	   
	   RETURN
    END

    SET @NOMBRE_NEW = (       
    SELECT nombre
      FROM ume..tbUsuarios usr
     WHERE usr.legajo = @usuario_new)
     
    IF (@NOMBRE_NEW IS NULL) 
    BEGIN
       RAISERROR (N'NO EXISTE EL LEGAJO %s',
					15,
					1,
					@usuario_new)
	   WITH SETERROR
	   
	   RETURN
    END

	IF (@rol IS NOT NULL) 
	BEGIN
		exec actualizarResponsable @pedido_id, @usuario_old, @usuario_new, @rol
	END
	
    CREATE TABLE #aprobacionesDeUsuario
    (Aprobacion_id INT,
     Pedido_id INT,
     Rol VARCHAR(5))
    
    INSERT INTO #aprobacionesDeUsuario
    SELECT id,
           pedido_id,
           rol
      FROM aprobacion
     WHERE (rol = @rol OR @rol IS NULL)
       AND pedido_id = @pedido_id
       AND usuario = @usuario_old
       AND estado = @PENDIENTE

    UPDATE aprobacion
       SET usuario = @usuario_new    
     WHERE id IN (SELECT Aprobacion_id FROM #aprobacionesDeUsuario)

	DECLARE cursorAprobaciones CURSOR FOR  
	SELECT Aprobacion_id, 
		rol
	FROM #aprobacionesDeUsuario 

	OPEN cursorAprobaciones   
	FETCH NEXT FROM cursorAprobaciones INTO @aprobacion_id, @codigoRol

	WHILE @@FETCH_STATUS = 0   
	BEGIN   
		SET @rolDescripcion = (
		SELECT descripcion
		  FROM rol_aplicacion rol
		 WHERE rol.codigo_rol = @codigoRol
		  )

		exec actualizarResponsable @pedido_id, @usuario_old, @usuario_new, @codigoRol

	    INSERT INTO log_modificaciones 
		    (version, 
	        descripcion_evento, 
	        rol,
			fase_id, 
			fecha_desde, 
			fecha_hasta, 
			pedido_id, 
	        tipo_evento, 
			log_modificaciones_idx)
		SELECT         
	        0, 
			'Se reasigna ' + @rolDescripcion + ' de ' + @NOMBRE_OLD + ' a ' + @NOMBRE_NEW, 
			@codigoRol,
			(select fase_actual_id from abstract_pedido where id = @pedido_id), 
			@FECHA_REASIGNACION,
			@FECHA_REASIGNACION,
			@pedido_id,
			@EVENTO_REASIGNACION,
	        (select MAX(log_modificaciones_idx) + 1 from log_modificaciones where pedido_id = @pedido_id)

		FETCH NEXT FROM cursorAprobaciones INTO @aprobacion_id, @codigoRol
	END   

	CLOSE cursorAprobaciones   
	DEALLOCATE cursorAprobaciones

END
