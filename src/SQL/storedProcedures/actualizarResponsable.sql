USE [needItPI]
GO
/****** Object:  StoredProcedure [dbo].[cancelarPedido]    Script Date: 12/20/2012 10:18:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Dodain
-- Create date: 20/12/2012
-- Description:	Actualiza el responsable de un pedido según un rol
-- =============================================
IF EXISTS (SELECT name 
	   FROM   sysobjects 
	   WHERE  name = N'actualizarResponsable' 
	   AND 	  type = 'P')
    DROP PROCEDURE actualizarResponsable
GO

CREATE PROCEDURE actualizarResponsable
	@pedido_id INT,
	@usuario_old VARCHAR(255),
	@usuario_new VARCHAR(255),
	@codigoRol VARCHAR(255)  
AS
BEGIN	
		IF (@codigoRol = 'UF')
		    BEGIN
				UPDATE abstract_pedido SET legajo_usuario_creador = @usuario_new WHERE id = @pedido_id AND legajo_usuario_creador = @usuario_old
		    END

		IF (@codigoRol = 'GU')
		    BEGIN
				UPDATE abstract_pedido SET legajo_gerente_usuario = @usuario_new WHERE id = @pedido_id AND legajo_gerente_usuario = @usuario_old
		    END

		IF (@codigoRol = 'IU')
		    BEGIN
				UPDATE abstract_pedido SET legajo_interlocutor_usuario = @usuario_new WHERE id = @pedido_id AND legajo_interlocutor_usuario = @usuario_old
		    END

		IF (@codigoRol = 'FGD')
		    BEGIN
				UPDATE abstract_pedido SET legajo_usuario_gestion_demanda = @usuario_new WHERE id = @pedido_id AND legajo_usuario_gestion_demanda = @usuario_old
		    END

		IF (@codigoRol = 'CC')
		    BEGIN
				UPDATE abstract_pedido SET legajo_coordinador_cambio = @usuario_new WHERE id = @pedido_id AND legajo_coordinador_cambio = @usuario_old
		    END

		IF (@codigoRol IN ('RSWF', 'AF', 'AS'))
		    BEGIN
				UPDATE abstract_pedido SET legajo_usuario_responsable = @usuario_new WHERE id = @pedido_id AND legajo_usuario_responsable = @usuario_old
		    END

		IF (@codigoRol = 'AI')
		    BEGIN
				UPDATE abstract_pedido SET legajo_usuario_aprobador_economico = @usuario_new WHERE id = @pedido_id AND legajo_usuario_aprobador_economico = @usuario_old
		    END

		IF (@codigoRol = 'RACT')
		    BEGIN
				UPDATE actividad SET legajo_usuario_responsable = @usuario_new WHERE pedido_id = @pedido_id AND legajo_usuario_responsable = @usuario_old AND estado_id = (SELECT ID FROM estado_actividad WHERE descripcion = 'Abierta')
		    END

END