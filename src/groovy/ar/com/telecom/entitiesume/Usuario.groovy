package ar.com.telecom.entitiesume

import ar.com.telecom.pcs.entities.ParametrosSistema

class Usuario {
	String legajo = ""
	String nombreApellido
	String email
	List jerarquias
	String gerenciaCodigo
	String gerencia
	String direccionCodigo
	String direccion
	
	public boolean equals(Object o) {
		try {
			Usuario otro = (Usuario) o
			return otro.legajo.equals(legajo)
		} catch (ClassCastException e) {
			return false
		}
	}
	
	public Usuario(){
		jerarquias = new ArrayList()
	}
	
	public int hashCode() {
		return legajo.hashCode()
	}
	
	public String toString() {
		return "Usuario " + legajo
	}
	
	def toHTML = {
		"<li id=\"$legajo\" class=\"ui-menu-item\">$legajo-$nombreApellido</li>"
	}
	
	def filtrarJerarquias(int jerarquiaDesde, int jerarquiaHasta) {
		def jerarquiasFiltradas = jerarquias.grep{
			
			it.toInteger()>= jerarquiaDesde && it.toInteger()<= jerarquiaHasta 
		}
		return jerarquiasFiltradas
	}

	def tieneJerarquias(int jerarquia) {
		tieneJerarquias(jerarquia, jerarquia)	
	}
	
	def tieneJerarquias(int jerarquiaDesde, int jerarquiaHasta) {
		def tieneJerarquias = !filtrarJerarquias(jerarquiaDesde, jerarquiaHasta).isEmpty()
		//println "Tiene jerarquias el user: " + tieneJerarquias
		return tieneJerarquias
	}
	
	def getJerarquia() {
		// TODO: Definir con Clara
		if (jerarquias.isEmpty()) {
			return null
		}
		return jerarquias.sort { it.toInteger() }.first()
	}
	
	boolean isGerente(){
		def parametro = ParametrosSistema.list().first()
		def listado = jerarquias.findAll { jerarquia ->
			jerarquia?.toInteger() >= parametro?.minimoJerarquiaGerente && jerarquia?.toInteger() <= parametro?.maximoJerarquiaGerente
		}
		return !listado.isEmpty()
	}
	
	boolean isDirector() {
		def parametro = ParametrosSistema.list().first()
		// TODO: Ponerlo en un parámetro aparte
		def jerarquiaDirector = parametro?.minimoJerarquiaAprobador
		tieneJerarquias(jerarquiaDirector)
	}
	
	boolean isAprobadorImpacto(){
		def parametro = ParametrosSistema.list().first()
		def listado = jerarquias.findAll { jerarquia ->
			jerarquia?.toInteger() >= parametro?.minimoJerarquiaAprobador && jerarquia?.toInteger() <= parametro?.maximoJerarquiaAprobador
		}
		return !listado.isEmpty()
	}
	
	boolean esDeDireccion(unaDireccion){
		if(!unaDireccion){
			return false
		}
		return direccionCodigo?.equalsIgnoreCase(unaDireccion)
	}
}
