package ar.com.telecom.entitiesume


class Estructura {

	String codigo
	String codigoPadre
	String codigoDireccion
	String descripcion
	String responsable
	String jerarquia
	boolean esDireccion
	
	public static int MAXIMO_JERARQUIA_DIRECCION = 40
	
	boolean cumpleJerarquia() {
		def valorJerarquia = jerarquia as int
		return valorJerarquia > MAXIMO_JERARQUIA_DIRECCION
	}
	
	public String toString() {
		return codigo + " - " + descripcion ?: "" 
	}
	
}
