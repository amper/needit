package ar.com.telecom.pedido.consulta

/**
 * 
 * Representa un criterio de búsqueda para encontrar pedidos
 * @author u193151
 *
 */
class ConsultaInbox extends Consulta{
 
	//
	private String tipoConsulta
	private String rol
	private String legajo
	// 
	
	def ingresoTipoConsulta() {
		if (tipoConsulta) {
			return true
		} else {
			return false
		}
	}
		
	def ingresoLegajo() {
		if (legajo) {
			return true
		} else {
			return false
		}
	}
	
	def ingresoRol() {
		if (rol) {
			return true
		} else {
			return false
		}
	}
}
