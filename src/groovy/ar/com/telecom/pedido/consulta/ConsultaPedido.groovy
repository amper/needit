package ar.com.telecom.pedido.consulta

/**
 * 
 * Representa un criterio de búsqueda para encontrar pedidos
 * @author u193151
 *
 */
class ConsultaPedido extends Consulta{
 
	//
	private Integer id
	private String titulo
	// 
	
	def ingresoId() {
		if (id) {
			return true
		} else {
			return false
		}
	}

	def ingresoTitulo() {
		if (titulo) {
			return true
		} else {
			return false
		}
	}

		
}
