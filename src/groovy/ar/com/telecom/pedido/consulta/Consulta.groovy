package ar.com.telecom.pedido.consulta

class Consulta {
	public static final String MIS_PENDIENTES = "MP"
	public static final String EN_SEGUIMIENTO_PROPIOS = "ESP"
	public static final String PENDIENTES_MI_GRUPO = "PMG"
	public static final String INBOX_APROBACIONES = "IA"
}
