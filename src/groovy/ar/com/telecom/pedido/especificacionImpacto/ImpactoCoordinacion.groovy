package ar.com.telecom.pedido.especificacionImpacto

import ar.com.telecom.pcs.entities.SoftwareFactory;

class ImpactoCoordinacion extends ImpactoPadre {

	String toString() {
		return "Planificaci\u00F3n de coordinaci\u00F3n"
	}
	
	def getId() {
		return Impacto.IMPACTO_COORDINACION
	}
	
   def esDeAreaDeSoporte() {
	   return false
   }

   def puedeIngresarOtrosCostos() {
	   return true
   }
   
   def tieneEstrategiaPrueba() {
	   return false
   }

   def tieneDisenioExterno() {
	   return false
   }

   def puedeEditar(usuarioLogueado) {
	   return pedido.puedeEditarConsolidacionImpacto(usuarioLogueado)
   }

   def getGruposPlanificacion(){
   		return pedido.getGruposPlanificacion() + SoftwareFactory.grupoLDAPHorasCoordinacion
	 }
   
   boolean editaRealesIncurridos(usuarioLogueado){
	   return pedido.esElCoordinadorCambio(usuarioLogueado)
   }

   boolean esDeCoordinacion(){
	   return true
   }
}
