package ar.com.telecom.pedido.especificacionImpacto


class Impacto {

	public static String IMPACTO_CONSOLIDADO = "C"
	public static String IMPACTO_COORDINACION = "A"
	public static String IMPACTO_HIJO = "H"

	private CriterioImpacto criterioImpacto

	Impacto(tipoImpacto, unPedido) {
		if (!tipoImpacto) {
			tipoImpacto = IMPACTO_CONSOLIDADO
		}
		if (tipoImpacto.equalsIgnoreCase(IMPACTO_CONSOLIDADO)) {
			criterioImpacto = new ImpactoConsolidado(pedido: unPedido)
		}
		if (tipoImpacto.equalsIgnoreCase(IMPACTO_COORDINACION)) {
			criterioImpacto = new ImpactoCoordinacion(pedido: unPedido)
		}
		if (tipoImpacto.equalsIgnoreCase(IMPACTO_HIJO) || tipoImpacto.isNumber()) {
			criterioImpacto = new ImpactoHijo(pedido: unPedido)
		}
	}

	String toString() {
		return criterioImpacto.toString()
	}

	def getId() {
		return criterioImpacto.getId()
	}

	/**
	 * Define si un impacto puede ser modificado por el usuario logueado
	 */
	def puedeEditarSistema(usuario) {
		return criterioImpacto.puedeEditarSistema(usuario)
	}

	/**
	 * Define si un impacto puede ser modificado por el usuario logueado
	 */
	def puedeVisualizarSistema(usuario) {
		return criterioImpacto.puedeVisualizarSistema(usuario)
	}

	/**
	 * Define si un impacto puede ser modificado por el usuario logueado
	 */
	def puedeEditarAreaSoporte(usuario) {
		return criterioImpacto.puedeEditarAreaSoporte(usuario)
	}

	/**
	 * Define si un impacto puede ser modificado por el usuario logueado
	 */
	def puedeVisualizarAreaSoporte(usuario) {
		return criterioImpacto.puedeVisualizarAreaSoporte(usuario)
	}

	/**
	 * Define si un impacto está asociado a sistemas
	 * @return
	 */
	def esDeSistema() {
		return criterioImpacto.esDeSistema()
	}

	/**
	 * Define si un impacto está asociado a un área de soporte
	 * @return
	 */
	def esDeAreaDeSoporte() {
		//return criterioImpacto.planificaTareasSoporte()
		return criterioImpacto.esDeAreaDeSoporte()
	}

	/**
	 * Muestra el costo total del cambio (en $)
	 * @return
	 */
	def costoTotalCambio() {
		return criterioImpacto.costoTotalCambio()
	}

	/**
	 * Indica si se debe cargar el número de referencia SOLMAN	
	 * @return
	 */
	def cargaReferenciaSOLMAN() {
		return criterioImpacto.cargaReferenciaSOLMAN()
	}

	/**
	 * Indica si se debe cargar el número de release
	 * @return
	 */
	def cargaRelease() {
		return criterioImpacto.cargaRelease()
	}

	/**
	 * Indica si se puede cargar el número de release
	 * (depende además de que ese sistema tenga cargados releases)
	 * @return
	 */
	def puedeIngresarOtrosCostos() {
		return criterioImpacto.puedeIngresarOtrosCostos()
	}

	/**
	 * Define si una clave aplica al hijo o a un padre
	 * @param clave
	 * @return
	 */
	static def aplicaAHijo(clave) {
		return !clave?.equalsIgnoreCase(IMPACTO_CONSOLIDADO) && !clave?.equalsIgnoreCase(IMPACTO_COORDINACION)
	}

	static def aplicaAPadre(clave) {
		return clave.equalsIgnoreCase(IMPACTO_CONSOLIDADO)
	}
	static def aplicaACoordinacion(clave) {
		return clave.equalsIgnoreCase(IMPACTO_COORDINACION)
	}

	/**
	 * Define si un impacto tiene planificación y esfuerzo
	 */
	def tienePlanificacionYEsfuerzo() {
		return criterioImpacto.tienePlanificacionYEsfuerzo()
	}

	/**
	 * Define si un impacto tiene estrategia de prueba
	 */
	def tieneEstrategiaPrueba() {
		return criterioImpacto.tieneEstrategiaPrueba()
	}

	/**
	 * Define si un impacto tiene disenio externo
	 */
	def tieneDisenioExterno() {
		return criterioImpacto.tieneDisenioExterno()
	}

	def tipoImpacto() {
		return criterioImpacto.tipoImpacto()
	}

	def getPedido() {
		return criterioImpacto.pedido
	}

	def esConsolidado() {
		return criterioImpacto.esConsolidado()
	}

	def puedeEditar(usuarioLogueado) {
		return criterioImpacto.puedeEditar(usuarioLogueado)
	}

	def puedeAgregarOtraPrueba(usuarioLogueado) {
		return this.puedeEditar(usuarioLogueado) && criterioImpacto.puedeAgregarOtraPrueba()
	}


	def habilitaAcciones(usuarioLogueado) {
		return this.puedeEditar(usuarioLogueado) && this.criterioImpacto.pedido.habilitaAcciones(usuarioLogueado)
	}

	def habilitaGuardar(usuarioLogueado) {
		return criterioImpacto.habilitaGuardar(usuarioLogueado)
	}

	def inicializaAsignatario(usuarioLogueado){
		return this.puedeEditar(usuarioLogueado) && this.pedido.inicializaAsignatario(usuarioLogueado)
	}

	def getDescripcion(fase) {
		return criterioImpacto.getDescripcion(fase)
	}

	def generarAprobaciones(aprueba, justificacion, usuarioLogueado){
		return criterioImpacto.generarAprobaciones(aprueba, justificacion, usuarioLogueado)
	}

	def getEstilo() {
		return criterioImpacto.estilo
	}

	def getEstiloDescripcion() {
		return criterioImpacto.estiloDescripcion
	}

	def getGruposPlanificacion(){
		return criterioImpacto.getGruposPlanificacion()
	}

	boolean apruebaPAU() {
		return criterioImpacto.apruebaPAU()
	}

	boolean editaRealesIncurridos(usuarioLogueado){
		return criterioImpacto.editaRealesIncurridos(usuarioLogueado)
	}

	boolean puedeReasignar(usuarioLogueado) {
		return criterioImpacto.puedeReasignar(usuarioLogueado)
	}
	
	boolean puedeReenviar(usuarioLogueado) {
		return criterioImpacto.puedeReenviar(usuarioLogueado)
	}
	boolean esDeCoordinacion(){
		return criterioImpacto.esDeCoordinacion()
	}
	
}
