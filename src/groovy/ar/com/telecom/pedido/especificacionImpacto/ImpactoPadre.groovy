package ar.com.telecom.pedido.especificacionImpacto

class ImpactoPadre extends CriterioImpacto {

   def esDeSistema() {
	   return true
   }
   
   def puedeReasignar(usuarioLogueado) {
	   return false
   }
   
   def puedeAgregarOtraPrueba(){
	   return false
   }
   
}
