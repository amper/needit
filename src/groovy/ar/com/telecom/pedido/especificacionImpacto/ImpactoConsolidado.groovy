package ar.com.telecom.pedido.especificacionImpacto

import ar.com.telecom.pedido.AprobacionUI

class ImpactoConsolidado extends ImpactoPadre {

	String toString() {
		return "Consolidado"
	}

	def getId() {
		return Impacto.IMPACTO_CONSOLIDADO
	}

	def puedeEditarSistema(usuario) {
		return false
	}

	def puedeVisualizarSistema(usuario) {
		return true
	}

	def tieneEstrategiaPrueba() {
		return true
	}

	def puedeEditarAreaSoporte(usuario) {
		return false
	}

	def puedeVisualizarAreaSoporte(usuario) {
		return false
	}

	def esDeAreaDeSoporte() {
		return false
	}

	def puedeIngresarOtrosCostos() {
		return false
	}

	def esConsolidado() {
		return true
	}

	def puedeEditar(usuarioLogueado) {
		return pedido.puedeEditarConsolidacionImpacto(usuarioLogueado)
	}

	def costoTotalCambio() {
		return pedido.parent.costoTotalCambioConsolidado()
	}

	def generarAprobaciones(aprueba, justificacion, usuarioLogueado){
		return pedido.pedidosHijosQueConsolidanPAU().collect { pedidoHijoConsolidado ->  new AprobacionUI(justificacion: justificacion, aprueba: aprueba, pedido: pedidoHijoConsolidado, asignatario: usuarioLogueado, usuario: usuarioLogueado) }
	}

	def getGruposPlanificacion(){
		def grupos = new HashSet( pedido.pedidosHijos.collect { pedidoHijo ->  pedidoHijo.getGruposPlanificacion()}.flatten() )
		return new ArrayList(grupos)
	}
	
	def puedeAgregarOtraPrueba(){
		return false
	}
	
	boolean editaRealesIncurridos(usuarioLogueado){
		return false
	}
}
