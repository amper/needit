package ar.com.telecom.pedido.especificacionImpacto

import ar.com.telecom.pedido.AprobacionUI
import ar.com.telecom.util.DateUtil

class CriterioImpacto {

	def pedido

	def puedeEditarSistema(usuario) {
		return pedido.puedeEditarConsolidacionImpacto(usuario)
	}

	def puedeVisualizarSistema(usuario) {
		return pedido.getPlanificacionActual().estaCumplido()
	}

	def puedeEditarAreaSoporte(usuario) {
		return pedido.puedeEditarConsolidacionImpacto(usuario)
	}

	def puedeVisualizarAreaSoporte(usuario) {
		return pedido.getPlanificacionActual().estaCumplido()
	}
	
	def costoTotalCambio() {
		return pedido.costoTotalCambio()
	}

	def cargaReferenciaSOLMAN() {
		return false
	}

	def cargaRelease() {
		return false
	}
	
	def tienePlanificacionYEsfuerzo() {
		return true
	}
	
	def planificaTareasSoporte(){
		return pedido.planificaTareasSoporte()
	}
	
	def tipoImpacto() {
		return null
	}
	
	def esConsolidado() {
		return false
	}
	
	def habilitaGuardar(usuarioLogueado) {
		return false
	}
	
	def tieneDisenioExterno() {
		return pedido.mostrarSolapaDE()
	}
	
	def getDescripcion() {
		return this.toString()
	}
	
	def getDescripcion(fase) {
		return this.toString()
	}
		
	def generarAprobacionUI(aprueba, justificacion, usuarioLogueado){
		return new AprobacionUI(justificacion: justificacion, aprueba: aprueba, pedido: pedido, asignatario: usuarioLogueado, usuario: usuarioLogueado)
	}
	
	def getEstilo() {
		return pedido.getEstilo()
	}
	
	def getEstiloDescripcion() {
		return pedido.getEstiloDescripcion()
	}

	boolean apruebaPAU() {
		return pedido.apruebaPAU()
	}

	boolean puedeReenviar(usuarioLogueado) {
		return false
	}
	
	
	boolean esDeCoordinacion(){
		return false
	}
}
