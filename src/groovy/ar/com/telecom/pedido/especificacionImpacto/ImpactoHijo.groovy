package ar.com.telecom.pedido.especificacionImpacto

class ImpactoHijo extends CriterioImpacto {

	String toString() {
		StringBuffer result = new StringBuffer(pedido.toString())
		return result
	}
	
	String getDescripcion(fase){
		StringBuffer result = new StringBuffer(pedido.toString())
		/*if (pedido.cumplioFecha(fase)) {
			result.append " (cumplido)"
		} else {
			result.append " (pendiente)"
		}*/
		return result
	}
	
	String getDescripcion(){
		StringBuffer result = new StringBuffer(pedido.toString())
		if (pedido.cumplioFecha()) {
			result.append " (cumplido)"
		} else {
			result.append " (pendiente)"
		}
		return result
	}
	
	/*def getDescripcion() {
		return pedido.toString()
	}*/

	def getId() {
		return pedido.id
	}

	def puedeEditarSistema(usuario) {
		return pedido.puedeEditarEspecificacionImpacto(usuario)
	}

	def puedeEditarAreaSoporte(usuario) {
		return pedido.puedeEditarEspecificacionImpacto(usuario)
	}
	
	def esDeSistema() {
		return pedido.planificaActividadesSistema()
	}
	
	def esDeAreaDeSoporte() {
		return pedido.planificaTareasSoporte()
	}
 
	def cargaReferenciaSOLMAN() {
		return pedido.ingresaTicketSolman()
	}
	
	def cargaRelease() {
		return pedido.cargaRelease()
	}
	
	def puedeIngresarOtrosCostos() {
		return pedido.permiteIngresarOtrosCostos()
	}

	def tienePlanificacionYEsfuerzo() {
		return pedido.mostrarSolapaPlanificacion()
	}
	
	def tieneEstrategiaPrueba() {
		return pedido.mostrarSolapaEstrategiaPrueba()
	}
	
	def tipoImpacto() {
		return pedido.tipoImpacto
	}
	
	def puedeEditar(usuarioLogueado) {
		return pedido.puedeEditarEspecificacionImpacto(usuarioLogueado)
	}

	def puedeReasignar(usuarioLogueado) {
		return pedido.puedeReasignarEspecificacionImpacto(usuarioLogueado)
	}

	def habilitaGuardar(usuarioLogueado) {
		return pedido.puedeReasignarEspecificacionImpacto(usuarioLogueado)
	}
	
	def generarAprobaciones(aprueba, justificacion, usuarioLogueado){
		return [generarAprobacionUI(aprueba, justificacion, usuarioLogueado)]
	}

	def getGruposPlanificacion(){
		return pedido.getGruposPlanificacion()
	}

	def puedeAgregarOtraPrueba(){
		return pedido.puedeAgregarOtraPrueba()
	}
	
	boolean editaRealesIncurridos(usuarioLogueado){
		return pedido.esResponsable(usuarioLogueado)
	}
	
	boolean puedeReenviar(usuarioLogueado) {
		return pedido.puedeReenviar(usuarioLogueado)
	}

}
