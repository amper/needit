package ar.com.telecom.pedido.estrategiaPrueba

class EstrategiaAdapter {

	List tiposPrueba
	
	EstrategiaAdapter() {
		initialize()
	}
	
	def initialize() {
		tiposPrueba = []	
	}
	
	def agregarTipoPrueba(TipoPruebaAdapter tipoPrueba) {
		tiposPrueba.add(tipoPrueba)
	}
	
	def eliminarTipoPrueba(TipoPruebaAdapter tipoPrueba) {
		tiposPrueba.remove(tipoPrueba)
	}
	
}
