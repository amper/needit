package ar.com.telecom.pedido.observers

import java.util.Date

import ar.com.telecom.pcs.entities.LogModificaciones
import ar.com.telecom.pcs.entities.Sistema
import ar.com.telecom.pcs.entities.TipoPedido
import ar.com.telecom.pcs.entities.TipoReferencia
import ar.com.telecom.util.DateUtil

class PedidoModificacionObserver {
	String titulo
	String descripcion
	String alcance
	String objetivo
	TipoPedido tipoPedido
	Date fechaDeseadaImplementacion
	Sistema sistemaSugerido
	TipoReferencia tipoReferencia
	String referenciaOrigen

	/**
	 * *****************************************************************************************
	 *                                  N E G O C I O
	 * *****************************************************************************************
	 */
	def initialize(unPedido) {
		titulo = unPedido.titulo
		descripcion = unPedido.descripcion
		alcance = unPedido.alcance
		objetivo = unPedido.objetivo
		tipoPedido = unPedido.tipoPedido
		fechaDeseadaImplementacion = unPedido.fechaDeseadaImplementacion
		sistemaSugerido = unPedido.sistemaSugerido
		tipoReferencia = unPedido.tipoReferencia
		referenciaOrigen = unPedido.referenciaOrigen
	}

	def notifyGuardar(mapaInfo, usuarioLogueado) {
		def unPedido = mapaInfo.get("pedido")
		
		if (usuarioLogueado.equalsIgnoreCase(unPedido.legajoUsuarioCreador)) {
			return	
		}
		
		def diferencia = new StringBuffer("")

		diferencia.append(verificarDiferencia(titulo, unPedido.titulo, "título"))
		diferencia.append(verificarDiferencia(descripcion, unPedido.descripcion, "descripción"))
		diferencia.append(verificarDiferencia(alcance, unPedido.alcance, "alcance"))
		diferencia.append(verificarDiferencia(objetivo, unPedido.objetivo, "objetivo"))
		diferencia.append(verificarDiferencia(tipoPedido?.descripcion, unPedido.tipoPedido?.descripcion, "tipo de pedido"))
		diferencia.append(verificarDiferencia(DateUtil.toString(fechaDeseadaImplementacion), DateUtil.toString(unPedido.fechaDeseadaImplementacion), "fecha implementación deseada"))
		diferencia.append(verificarDiferencia(sistemaSugerido?.descripcion, unPedido.sistemaSugerido?.descripcion, "sistema sugerido"))
		diferencia.append(verificarDiferencia(tipoReferencia?.descripcion, unPedido.tipoReferencia?.descripcion, "tipo de referencia origen"))
		diferencia.append(verificarDiferencia(referenciaOrigen, unPedido.referenciaOrigen, "referencia origen"))
		//println this.toString() + " --->"
		if (!diferencia.toString().equals("")) {
			// hubo modificaciones
			def fecha = new Date()
			//unPedido.logModificaciones.add(new LogModificaciones(legajo: usuarioLogueado, fechaDesde: fecha, fechaHasta: fecha, fase: unPedido.faseActual, tipoEvento: LogModificaciones.MODIFICACION_DATOS, descripcionEvento: "Modificación de datos - " + diferencia))
			unPedido.addToLogModificaciones(new LogModificaciones(legajo: usuarioLogueado, fechaDesde: fecha, fechaHasta: fecha, fase: unPedido.faseActual, tipoEvento: LogModificaciones.MODIFICACION_DATOS, descripcionEvento: "Modificación de datos - " + diferencia))
			
		} else {
			//println "No las hubo"
		}
		initialize(unPedido)
	}

	def notifyCambioEstado(mapaInfo) {
		// acá no hacemos nada porque al cambiar de estado también se guarda	
	}
	
	/**
	 * *****************************************************************************************
	 *                        M E T O D O S      P R I V A D O S
	 * *****************************************************************************************
	 */
	private def verificarDiferencia(original, nuevo, campo) {
		if (original && !original.equals(nuevo)) {
			return campo + ": de " + original + " a " + nuevo + " "
		} else {
			return ""
		}
	}
	
	def setConfiguracionMail(configuracionMail) {
		// 	no hace nada
	}
	
	public int hashCode() {
		return 130000L //+ configuracionMail.id
	}

	public boolean equals(otroObserver) {
		return this.class.equals(otroObserver.class) && otroObserver.hashCode() == this.hashCode()
	}
	
}
