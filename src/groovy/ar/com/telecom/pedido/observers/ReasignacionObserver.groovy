package ar.com.telecom.pedido.observers

import ar.com.telecom.pcs.entities.ConfiguracionEMailReasignacion
import ar.com.telecom.ume.UmeService

class ReasignacionObserver {
	
	def notifyCambioEstado(mapaInfo) {
		// acá no hacemos nada porque al cambiar de estado también se guarda
	}

	def procesarCambioUsuario(usuarioLogueado, unPedido, legajoOriginal, legajoNuevo, descripcion, codigoRol) {
		//println "procesarCambioUsuario legajoOriginal: " + legajoOriginal
		if ((legajoNuevo || legajoOriginal) && !legajoNuevo?.equalsIgnoreCase(legajoOriginal)) {
			//println "procesarCambioUsuario | Reasigno..."
			unPedido.reasignar(usuarioLogueado, legajoOriginal, legajoNuevo, descripcion, codigoRol)
		}
	}
	
}
