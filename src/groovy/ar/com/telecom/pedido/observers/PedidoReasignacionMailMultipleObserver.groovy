package ar.com.telecom.pedido.observers

import ar.com.telecom.pcs.entities.ConfiguracionEMailReasignacion
import ar.com.telecom.pcs.entities.RolAplicacion
import ar.com.telecom.ume.UmeService

class PedidoReasignacionMailMultipleObserver {

	ConfiguracionEMailReasignacion configuracionMail
	Map legajosOriginales = [ : ]

	def initialize(unPedido) {
		if (unPedido.faseActual.equals(configuracionMail.fase)) {
			if (configuracionMail.rol.codigoRol.equals(RolAplicacion.RESPONSABLE_ACTIVIDAD)) {
				unPedido.actividades.each { actividad ->
					legajosOriginales.putAt(actividad, actividad.legajoUsuarioResponsable)
				}
				if (configuracionMail.rol.codigoRol.equals(RolAplicacion.APROBADOR_FASE)) {
					aprobaciones(unPedido).each { aprobacion ->
						legajosOriginales.putAt(aprobacion, aprobacion.usuario)
					}
				}
			}
		}
	}

	def notifyCambioEstado(mapaInfo) {
		// acá no hacemos nada porque al cambiar de estado también se guarda
	}

	def notifyGuardar(mapaInfo, usuarioLogueado) {
		def unPedido = mapaInfo.get("pedido")
		
		def asignaciones = []
		if (configuracionMail.rol.codigoRol.equals(RolAplicacion.RESPONSABLE_ACTIVIDAD)) {
			asignaciones = unPedido.actividades
		}
		if (configuracionMail.rol.codigoRol.equals(RolAplicacion.APROBADOR_FASE)) {
			asignaciones = aprobaciones(unPedido)
		}
		asignaciones.each { asignacion ->
			def legajoNuevo = asignacion.legajoUsuarioResponsable
			def legajoOriginal = legajosOriginales.get(asignacion)
			
			if (legajoOriginal && !legajoNuevo?.equalsIgnoreCase(legajoOriginal)) {
				// asignacion se comporta en forma polimorfica para objeto actividad o aprobacion
				mapaInfo.put("asignacion", asignacion)
				new MailBuffer(asunto: configuracionMail?.asunto, texto: configuracionMail?.texto, mails: mailsANotificar(legajoOriginal, legajoNuevo)).enviarMail(mapaInfo)
			}
		}
		initialize(unPedido)
	}

	private def mailsANotificar(legajoOriginal, legajoNuevo) {
		def umeService = new UmeService()
		def legajo = ""

		if (configuracionMail.actual) {
			legajo = legajoNuevo
		} else {
			legajo = legajoOriginal
		}
		
		def mail = umeService.getUsuario(legajo)?.email
		
		if (mail) {
			return [mail]
		} else {
			return []
		}
	}

	private def aprobaciones(unPedido) {
		return unPedido.aprobacionesPendientesDeFase(unPedido.faseActual)
	}
	
	public int hashCode() {
		return 110000L + configuracionMail.id
	}
	
	public boolean equals(otroObserver) {
		return this.class.equals(otroObserver.class) && otroObserver.hashCode() == this.hashCode()
	}

}
