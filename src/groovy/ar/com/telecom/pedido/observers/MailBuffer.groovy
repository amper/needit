package ar.com.telecom.pedido.observers

import ar.com.telecom.EnvioMail
import ar.com.telecom.pcs.entities.AbstractPedido
import ar.com.telecom.pcs.entities.ParametrosSistema

class MailBuffer {

	String asunto
	String texto
	List mails

	public void enviarMail(Map mapaInfo) {
		def unPedido = mapaInfo.get("pedido")
		
		def textoSinEnters = texto.replaceAll("\n", "")
		def textoNuevo = parsearMail(mapaInfo, textoSinEnters)
		def asuntoSinEnters = asunto.replaceAll("\n", "")
		def asuntoNuevo = parsearMail(mapaInfo, asuntoSinEnters)
		String from = ParametrosSistema.list().first().mailEmisorPCS
		unPedido.agregarMailPendiente(new EnvioMail(to: mails, from: from, subject: asuntoNuevo, mail: textoNuevo))
	}
		
	private def parsearMail(Map mapaInfo, String textoOriginal) {
		try {
			def unPedido = mapaInfo.get("pedido")
			
			def textoNuevo = textoOriginal.replaceAll("\\{", "" + /\$/)
			textoNuevo = textoNuevo.replaceAll("}", "")
			Binding binding = new Binding()
			// binding.setVariable("pedido", unPedido)
			// se reemplaza por
			mapaInfo.each { info -> binding.setVariable(info.key, info.value) }
			
			GroovyShell shell = new GroovyShell(binding)
			def codigo = "def texto = \"" + textoNuevo + "\""
			return shell.evaluate(codigo)
		} catch (Exception e) {
			return "Al parsear: " + textoOriginal + " - error: " + e.getMessage()
		}
	}

	public String toString() {
		return "Mail a " + mails + ", asunto: " + asunto
	}
}
