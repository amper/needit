package ar.com.telecom.pedido.observers

import ar.com.telecom.pcs.entities.ConfiguracionEMailReasignacion
import ar.com.telecom.ume.UmeService

class PedidoReasignacionMailObserver {

	ConfiguracionEMailReasignacion configuracionMail
	String legajoOriginal
	
	def initialize(unPedido) {
		legajoOriginal = unPedido.getLegajo(configuracionMail.rol)
	}
	
	def notifyCambioEstado(mapaInfo) {
		// acá no hacemos nada porque al cambiar de estado también se guarda
	}

	def notifyGuardar(mapaInfo, usuarioLogueado) {
		def unPedido = mapaInfo.get("pedido")
		
		def legajoNuevo = unPedido.getLegajo(configuracionMail.rol)
		if (!legajoNuevo?.equalsIgnoreCase(legajoOriginal)) {
			mapaInfo.put("legajoOriginal", legajoOriginal)
			mapaInfo.put("legajoNuevo", legajoNuevo)
			//println "Reasignacion Mail Observer. Legajo original " + legajoOriginal + " - nuevo: " + legajoNuevo
			if (meInteresa(legajoNuevo)) {
				//println "Me interesa, configuracion: " + configuracionMail.id
				def mailBuffer = new MailBuffer(asunto: configuracionMail?.asunto, texto: configuracionMail?.texto, mails: mailsANotificar(legajoOriginal, legajoNuevo))
				mailBuffer.enviarMail(mapaInfo)
			} else {
				//println "No me interesa"
			}
		}
		//println "************************************"
		initialize(unPedido)
	}
	
	private def mailsANotificar(legajoOriginal, legajoNuevo) {
		def umeService = new UmeService()
		def legajo = ""
		
		if (configuracionMail.actual) {
			legajo = legajoNuevo
		} else {
			legajo = legajoOriginal
		}
		def usuario = umeService.getUsuario(legajo)
		if (usuario) {
			return [umeService.getUsuario(legajo)?.email]
		} else {
			return []
		}
	}
	
	private def meInteresa(legajoNuevo) {
		return (legajoOriginal && !configuracionMail.actual) || (legajoNuevo && configuracionMail.actual)
	}
}
