package ar.com.telecom.pedido.observers

import ar.com.telecom.pcs.entities.Aprobacion
import ar.com.telecom.pcs.entities.AreaSoporteAprobarFase

class PedidoAprobacionesCambioEstadoObserver {

	def initialize(unPedido) {
	}

	def notifyGuardar(mapaInfo, usuarioLogueado) {
		// No hacemos nada por default
	}

	def notifyCambioEstado(mapaInfo) {
		def unPedido = mapaInfo.get("pedido")
		
		AreaSoporteAprobarFase.findAllByFase(unPedido.faseActual).each { areaFase ->
			def areaSoporteAAprobar = areaFase.areaSoporte
			if (unPedido.parent.areasImpactadas().contains(areaSoporteAAprobar)) {
				def aprobacion = new Aprobacion(grupo: areaSoporteAAprobar.grupoAdministrador, usuario: unPedido.getUsuarioAprobadorAreaSoporte(areaSoporteAAprobar), areaSoporte: areaSoporteAAprobar, estado: Aprobacion.PENDIENTE, fase: areaFase.fase, fecha: new Date(), descripcionAprobacionSolicitada: "Aprobaci\u00F3n del AS asociado a la fase")
				unPedido.addToAprobaciones(aprobacion)
			}
		}
	}
	
	public int hashCode() {
		return 70000L
	}
	
	public boolean equals(otroObserver) {
		return this.class.equals(otroObserver.class) && otroObserver.hashCode() == this.hashCode()
	}
}
