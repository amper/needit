package ar.com.telecom.pedido.observers

import ar.com.telecom.EnvioMail
import ar.com.telecom.exceptions.BusinessException
import ar.com.telecom.pcs.entities.AbstractPedido;
import ar.com.telecom.pcs.entities.ConfiguracionEmailCambioEstado
import ar.com.telecom.pcs.entities.Fase
import ar.com.telecom.pcs.entities.RolAplicacion;
import ar.com.telecom.ume.UmeService

class PedidoAprobacionObserver {

	ConfiguracionEmailCambioEstado configuracionMail
	Fase faseOriginal

	/**
	 * *****************************************************************************************
	 *                                  N E G O C I O
	 * *****************************************************************************************
	 */
	def initialize(unPedido) {
		faseOriginal = unPedido.faseActual
	}

	def notifyGuardar(mapaInfo, usuarioLogueado) {
		// No hacemos nada por default
	}

	def notifyCambioEstado(mapaInfo) {
		if (configuracionMail.faseActual.equals(faseOriginal) && configuracionMail.faseSiguiente.equals(unPedido.faseActual)) {
			def umeService = new UmeService()

			Set legajos = new HashSet()
			configuracionMail.roles.each { rol ->
				def legajosAux = this.usuariosANotificar(unPedido, rol)
				if (legajosAux.isEmpty()) {
					legajosAux = this.usuariosDeGruposANotificar(unPedido, rol)
				}
				legajos.addAll(legajosAux)
			}
			def mails = legajos.collect { legajo -> umeService.getUsuario(legajo).email }.asList()

			new MailBuffer(asunto: configuracionMail.asunto, texto: configuracionMail.texto, mails: mails).enviarMail(mapaInfo)
		}
	}

	private def usuariosANotificar(AbstractPedido unPedido, RolAplicacion rol) {
		String methodGetterEnvioMails = rol.propiedadUsuariosEnvioMails
		if (!methodGetterEnvioMails) {
			throw new BusinessException("El rol " + rol.descripcion + " no tiene definida la propiedad para env\u00EDo de mails al usuario")
		}
		try {
			return unPedido."$methodGetterEnvioMails"
		} catch (Exception e) {
			throw new BusinessException("El rol " + rol.descripcion + " tiene definida una propiedad inv\u00E1lida para el env\u00EDo de mails: [" + methodGetterEnvioMails + "]")
		}
	}

	private def usuariosDeGruposANotificar(unPedido, rol) {
		// Viene vac\u00EDa la lista de legajos, es porque todav\u00EDa no fue asignado en forma particular
		// Tiene que haber un grupo definido
		if (!rol.propiedadGrupoEnvioMails) {
			throw new BusinessException("El rol " + rol.descripcion + " no tiene definida la propiedad para env\u00EDo de mails al grupo y tampoco tiene usuarios asignados en la propiedad " + rol.propiedadUsuariosEnvioMails)
		}
		String methodGetterEnvioMails = rol.propiedadGrupoEnvioMails
		def grupo
		try {
			grupo = unPedido."$methodGetterEnvioMails"
		} catch (Exception e) {
			throw new BusinessException("El rol " + rol.descripcion + " tiene definida una propiedad inv\u00E1lida para el env\u00EDo de mails: [" + methodGetterEnvioMails + "]")
		}
		def umeService = new UmeService()
		def usuarios = umeService.getUsuariosGrupoLDAP(grupo, null)
		return usuarios.legajo
	}
	
	public String toString() {
		return "PedidoCambioEstadoObserver de fase " + configuracionMail.faseActual + " a fase " + configuracionMail.faseSiguiente
	}
	
	public int hashCode() {
		return 80000L
	}

}
