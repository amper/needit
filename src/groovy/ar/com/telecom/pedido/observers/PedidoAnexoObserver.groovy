package ar.com.telecom.pedido.observers

import ar.com.telecom.pcs.entities.AbstractPedido;
import ar.com.telecom.pcs.entities.ConfiguracionEmailCambioEstado;
import ar.com.telecom.pcs.entities.ConfiguracionEmailTipoEvento;
import ar.com.telecom.pcs.entities.Fase;
import ar.com.telecom.pcs.entities.RolAplicacion;
import ar.com.telecom.util.ListUtil


class PedidoAnexoObserver extends PedidoMailObserver {
	
	List anexos
	ConfiguracionEmailTipoEvento configuracionMail
	
	/**
	 * *****************************************************************************************
	 *                                  N E G O C I O
	 * *****************************************************************************************
	 */
	def initialize(unPedido) {
		anexos = new ArrayList(getAnexos(unPedido))
	}

	def getAnexos(unPedido) {
		return unPedido.anexos	
	}
	
	def notifyGuardar(mapaInfo, usuarioLogueado) {
		def unPedido = mapaInfo.get("pedido")
		
		// intención original: si alguno de los elementos cambia... entonces se debe notificar
		if (!ListUtil.equals(getAnexos(unPedido), anexos)) {
			this.prepararMail(mapaInfo, configuracionMail)
		}
		initialize(unPedido)
	}

	def notifyCambioEstado(mapaInfo) {
		// no hacemos nada
	}
	
	public String toString() {
		return "PedidoAnexoObserver de fase " + configuracionMail.fase
	}
	
	public int hashCode() {
		return 30000L + configuracionMail.id
	}
	
}
