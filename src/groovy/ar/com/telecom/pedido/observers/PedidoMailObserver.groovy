package ar.com.telecom.pedido.observers

import ar.com.telecom.exceptions.BusinessException
import ar.com.telecom.pcs.entities.AbstractPedido
import ar.com.telecom.pcs.entities.RolAplicacion
import ar.com.telecom.ume.UmeService

class PedidoMailObserver {


	def usuariosANotificar(AbstractPedido unPedido, RolAplicacion rol) {
		String methodGetterEnvioMails = rol.propiedadUsuariosEnvioMails
		if (!methodGetterEnvioMails) {
			throw new BusinessException("El rol " + rol.descripcion + " no tiene definida la propiedad para env\u00EDo de mails al usuario")
		}
		try {
			def result = unPedido."$methodGetterEnvioMails"
//			println "rol: " + rol.propiedadUsuariosEnvioMails + " - " + rol.descripcion
//			println "usuariosANotificar: " + result
			if (result) {
				return result
			} else {
				return []
			}
		} catch (Exception e) {
			//throw new BusinessException("El rol " + rol.descripcion + " tiene definida una propiedad inválida para el env\u00EDo de mails: [" + methodGetterEnvioMails + "] - " + e.getMessage())
			throw e
		}
	}

	def usuariosDeGruposANotificar(unPedido, rol) {
		// Viene vac\u00EDa la lista de legajos, es porque todav\u00EDa no fue asignado en forma particular
		// Tiene que haber un grupo definido

		//		if (!rol.propiedadGrupoEnvioMails) {
		//throw new BusinessException("El rol " + rol.descripcion + " no tiene definida la propiedad para env\u00EDo de mails al grupo y tampoco tiene usuarios asignados en la propiedad " + rol.propiedadUsuariosEnvioMails)
		//			return []
		//		}
		String methodGetterEnvioMails = rol.propiedadGrupoEnvioMails
		def grupo
		try {
			if (methodGetterEnvioMails) {
				grupo = unPedido."$methodGetterEnvioMails"
			} else {
				grupo = rol.descripcion
			}
		} catch (Exception e) {
			//throw new BusinessException("El rol " + rol.descripcion + " tiene definida una propiedad inválida para el env\u00EDo de mails: [" + methodGetterEnvioMails + "]")
			throw e
		}
		def umeService = new UmeService()
		def usuarios = umeService.getUsuariosGrupoLDAP(grupo, null)
//		println "usuariosDeGruposANotificar: " + usuarios.legajo + " | grupo: " + grupo
		return usuarios.legajo
	}

	def prepararMail(mapaInfo, configuracionMail) {
		def umeService = new UmeService()
		def unPedido = mapaInfo.get("pedido")

		Set legajos = new HashSet()
		//No va al final
		//this.getRolesInteresados(unPedido, configuracionMail)
		configuracionMail.roles.each { rol ->
			def legajosAux = []

			try {
				if (rol?.codigoRol?.equalsIgnoreCase(RolAplicacion.RESPONSABLE_ACTIVIDAD)) {
//					println "Responsables actividades"
					def actividad = mapaInfo.get("actividad")
					if (actividad) {
						legajosAux = actividad?.usuariosResponsables
					} else {
						legajosAux = unPedido.actividades.collect { itActividad -> itActividad.usuariosResponsables }.flatten();
					}
				} else {
					legajosAux = this.usuariosANotificar(unPedido, rol).flatten()
				}
			} catch (Exception e) {
				// TODO: escondemos todo abajo de la alfombra? No está bueno...
			}
			if (legajosAux.isEmpty()) {
				try {
					legajosAux = this.usuariosDeGruposANotificar(unPedido, rol).flatten()
				} catch (Exception e) {
					// TODO: escondemos todo abajo de la alfombra? No está bueno...
				}
			}
			legajos = legajos + legajosAux
			
			legajos = legajos.findAll { legajo -> legajo != null }
		}
		try {
			if (!legajos.isEmpty()) {
				def mails = new HashSet(legajos.collect { legajo ->
					def usuario = umeService.getUsuario(legajo)
					usuario?.email }).asList()

//				println "Mail a enviar: " + configuracionMail.texto + " - personas: " + legajos
				new MailBuffer(asunto: configuracionMail.asunto, texto: configuracionMail.texto, mails: mails).enviarMail(mapaInfo)
			} else {
//				println "No hay nadie a quien mandar mail"
			}
		} catch (Exception e) {
			throw new BusinessException("Error en la configuraci\u00F3n " + configuracionMail, e)
		}
	}

	public boolean equals(otroObserver) {
		return this.class.equals(otroObserver.class) && otroObserver.hashCode() == this.hashCode()
	}
	
//	def getRolesInteresados(pedido, configuracionMail) {
//		return configuracionMail.roles.findAll { rol -> !(rol.aplicaPadre ^ pedido.esPadre()) }
//	}

}
