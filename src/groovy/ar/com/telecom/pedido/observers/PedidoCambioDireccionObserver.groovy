package ar.com.telecom.pedido.observers

import ar.com.telecom.pcs.entities.LogModificaciones

class PedidoCambioDireccionObserver {
	String codigoDireccionGerencia
	
	def initialize(unPedido) {
		if(!codigoDireccionGerencia){
			codigoDireccionGerencia = unPedido.codigoDireccionGerencia                                    
		}
	}

	def notifyGuardar(mapaInfo, usuarioLogueado) {
		def unPedido = mapaInfo.get("pedido")
		def fecha = new Date()
		
		def descripcionModificacion
		
		if(!codigoDireccionGerencia){
			if(unPedido.codigoDireccionGerencia){
				descripcionModificacion = "Se asigna al pedido la dirección "+unPedido.codigoDireccionGerencia
			}else{
				codigoDireccionGerencia = null
			}
		}else{
			if(!codigoDireccionGerencia.equalsIgnoreCase(unPedido.codigoDireccionGerencia)){
				descripcionModificacion = "Se actualiza la dirección del pedido de " + codigoDireccionGerencia + " a " + unPedido.codigoDireccionGerencia
			}
		}
		
		if(descripcionModificacion){
			unPedido.addToLogModificaciones(new LogModificaciones(legajo: usuarioLogueado, fechaDesde: fecha, rol: unPedido.getRol(unPedido.faseActual, usuarioLogueado), fechaHasta: fecha, fase: unPedido.fase, tipoEvento: LogModificaciones.ACTUALIZACION_DIRECCION_PEDIDO, 
					descripcionEvento: descripcionModificacion))
		}
	}

	def notifyCambioEstado(mapaInfo) {
	}
	
	public int hashCode() {
		return 200000L
	}
	
	public boolean equals(otroObserver) {
		return this.class.equals(otroObserver.class) && otroObserver.hashCode() == this.hashCode()
	}
}
