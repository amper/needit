package ar.com.telecom.pedido.observers

import ar.com.telecom.pcs.entities.ConfiguracionEmailTipoEvento
import ar.com.telecom.pcs.entities.Fase
import ar.com.telecom.pcs.entities.LogModificaciones


class PedidoEventoLogObserver extends PedidoMailObserver {
	
	int logSize
	Fase faseOriginal
	ConfiguracionEmailTipoEvento configuracionMail
	
	/**
	 * *****************************************************************************************
	 *                                  N E G O C I O
	 * *****************************************************************************************
	 */
	def initialize(unPedido) {
		logSize = unPedido.logModificaciones.size()
		faseOriginal = unPedido.faseActual
	}

	def logsNuevos(unPedido) {
		def logs = unPedido.logModificaciones
		def tamanioActual = logs.size()
		if (tamanioActual - logSize > 0) {
			return unPedido.logModificaciones.subList(logSize, tamanioActual)	
		} else {
			return []
		}
	}
	
	def notifyGuardar(mapaInfo, usuarioLogueado) {
		verificarCambios(mapaInfo)
	}

	def notifyCambioEstado(mapaInfo) {
		verificarCambios(mapaInfo)
	}
	
	private def verificarCambios(mapaInfo) {
		def unPedido = mapaInfo.get("pedido")
		def logsNuevos = logsNuevos(unPedido)
		// por cada registro en el log que me interese genero una notificación por mail
		logsNuevos.findAll { logModificacion -> 
			logModificacion.tipoEvento.equalsIgnoreCase(configuracionMail.tipoEvento) }.each { logModificacion ->
			if (!logModificacion.esAutomatico()) {
				def aprobaciones = unPedido.aprobacionesDeFase(faseOriginal, logModificacion.legajo).sort { it.fecha }
				if (!aprobaciones.isEmpty()) {
					mapaInfo.put("aprobacion", aprobaciones.last())
				}
			}
			
			def actividad = logModificacion.actividad
			if (actividad) {
				mapaInfo.put("actividad", actividad)
			}
			mapaInfo.put("ultimoLog", logModificacion)
			this.prepararMail(mapaInfo, configuracionMail)
		}
//		println "**********************************"
		initialize(unPedido)
	}

	private def getActividad(logsNuevos) {
		def actividades = logsNuevos.findAll { logNuevo -> logNuevo.actividad }.collect { logNuevo -> logNuevo.actividad }
		def actividad = null
		if (!actividades.isEmpty()) {
			actividad = actividades.first()
		}
		return actividad
	}	
	
	public String toString() {
		return "PedidoEventoLogObserver de evento " + LogModificaciones.getTipoEvento(configuracionMail.tipoEvento)
	}

	public int hashCode() {
		return 10000L + configuracionMail.id
	}
	
}
