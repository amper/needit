package ar.com.telecom.pedido.observers

import ar.com.telecom.pcs.entities.RolAplicacion

class PedidoReasignacionObserver extends ReasignacionObserver {

	String legajoGerente
	String legajoInterlocutor
	String legajoGestionDemanda
	String legajoCoordinadorCambio
	String legajoUsuarioAprobadorEconomico

	/**
	 * *****************************************************************************************
	 *                                  N E G O C I O
	 * *****************************************************************************************
	 */
	def initialize(unPedido) {
		legajoGerente = unPedido.legajoGerenteUsuario
		legajoInterlocutor = unPedido.legajoInterlocutorUsuario
		legajoGestionDemanda = unPedido.legajoUsuarioGestionDemanda
		legajoCoordinadorCambio = unPedido.legajoCoordinadorCambio
		legajoUsuarioAprobadorEconomico = unPedido.legajoUsuarioAprobadorEconomico
	}

	def notifyGuardar(mapaInfo, usuarioLogueado) {
		def unPedido = mapaInfo.get("pedido")
		
		procesarCambioUsuario(usuarioLogueado, unPedido, legajoGerente, unPedido.legajoGerenteUsuario, "gerente usuario", RolAplicacion.GERENTE_USUARIO)
		procesarCambioUsuario(usuarioLogueado, unPedido, legajoInterlocutor, unPedido.legajoInterlocutorUsuario, "interlocutor usuario", RolAplicacion.INTERLOCUTOR_USUARIO)
		procesarCambioUsuario(usuarioLogueado, unPedido, legajoGestionDemanda, unPedido.legajoUsuarioGestionDemanda, "asignatario gestión demanda", RolAplicacion.GESTION_DEMANDA)
		procesarCambioUsuario(usuarioLogueado, unPedido, legajoCoordinadorCambio, unPedido.legajoCoordinadorCambio, "coordinador cambio", RolAplicacion.COORDINADOR_CAMBIO)
		if (unPedido.legajoUsuarioAprobadorEconomico) {
			procesarCambioUsuario(usuarioLogueado, unPedido, legajoUsuarioAprobadorEconomico, unPedido.legajoUsuarioAprobadorEconomico, "aprobador de impacto", RolAplicacion.APROBADOR_IMPACTO)
		} else {
			procesarCambioUsuario(null, unPedido, legajoUsuarioAprobadorEconomico, unPedido.legajoUsuarioAprobadorEconomico, "aprobador de impacto", null)
		}
		initialize(unPedido)
	}

	public int hashCode() {
		return 100000L
	}

	public boolean equals(otroObserver) {
		return this.class.equals(otroObserver.class) && otroObserver.hashCode() == this.hashCode()
	}
	
}
