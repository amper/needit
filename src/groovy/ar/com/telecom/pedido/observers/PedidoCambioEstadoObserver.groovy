package ar.com.telecom.pedido.observers

import ar.com.telecom.EnvioMail
import ar.com.telecom.exceptions.BusinessException
import ar.com.telecom.pcs.entities.AbstractPedido;
import ar.com.telecom.pcs.entities.ConfiguracionEmailCambioEstado
import ar.com.telecom.pcs.entities.Fase
import ar.com.telecom.pcs.entities.RolAplicacion;
import ar.com.telecom.ume.UmeService

class PedidoCambioEstadoObserver extends PedidoMailObserver {

	ConfiguracionEmailCambioEstado configuracionMail
	Fase faseOriginal

	/**
	 * *****************************************************************************************
	 *                                  N E G O C I O
	 * *****************************************************************************************
	 */
	def initialize(unPedido) {
		faseOriginal = unPedido.faseActual
	}

	def notifyGuardar(mapaInfo, usuarioLogueado) {
		// No hacemos nada por default
	}

	def notifyCambioEstado(mapaInfo) {
		def unPedido = mapaInfo.get("pedido")

		//println "Cambio de estado observer"
		if (configuracionMail.faseActual.equals(faseOriginal) && configuracionMail.faseSiguiente.equals(unPedido.faseActual)) {
			def aprobaciones = unPedido.aprobacionesDeFase(faseOriginal).sort { it.fecha }
			if (!aprobaciones.isEmpty()) {
				mapaInfo.put("aprobacion", aprobaciones.last())
			}
	
			def logsNuevos = unPedido.logModificaciones
			if (!logsNuevos.isEmpty()) {
				mapaInfo.put("ultimoLog", logsNuevos.last())
			}
			
//			println "Preparo mail cambioEstado"
			this.prepararMail(mapaInfo, configuracionMail)
			//println "Pedido id: " + unPedido.id
		}
		//println "*********************************************"
		initialize(unPedido)
	}

	public String toString() {
		return "PedidoCambioEstadoObserver de fase " + configuracionMail.faseActual + " a fase " + configuracionMail.faseSiguiente
	}
	
	public int hashCode() {
		return 50000L + configuracionMail.id
	}
}
