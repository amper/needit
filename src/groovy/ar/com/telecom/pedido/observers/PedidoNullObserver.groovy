package ar.com.telecom.pedido.observers

/**
 * Esta clase representa la implementación default de un observer del pedido 
 * que no tiene comportamiento asociado (es la implementación del NullObject
 * pattern para evitar manejar con ifs cuando no corresponde observar al pedido)
 * 
 * @author u193151
 *
 */
class PedidoNullObserver {

	def initialize(unPedido) {
	}

	def notifyGuardar(mapaInfo, usuarioLogueado) {
	}

	def notifyCambioEstado(mapaInfo) {
	}

	def setConfiguracionMail(configuracionMail) {
		// 	no hace nada
	}

	public int hashCode() {
		return 150000L + configuracionMail.id
	}

	public boolean equals(otroObserver) {
		return this.class.equals(otroObserver.class) && otroObserver.hashCode() == this.hashCode()
	}
	
}
