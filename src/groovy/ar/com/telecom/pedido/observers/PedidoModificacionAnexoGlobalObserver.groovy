package ar.com.telecom.pedido.observers

import java.util.Date

import ar.com.telecom.pcs.entities.LogModificaciones
import ar.com.telecom.pcs.entities.Sistema
import ar.com.telecom.pcs.entities.TipoPedido
import ar.com.telecom.pcs.entities.TipoReferencia
import ar.com.telecom.util.DateUtil

class PedidoModificacionAnexoGlobalObserver extends PedidoAnexoObserver {
	
	/**
	 * *****************************************************************************************
	 *                                  N E G O C I O
	 * *****************************************************************************************
	 */
	def getAnexos(unPedido) {
		return unPedido.anexos.findAll { anexo -> anexo.global }	
	}
	
}
