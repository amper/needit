package ar.com.telecom.pedido.observers

import ar.com.telecom.pcs.entities.RolAplicacion

class PedidoHijoReasignacionObserver extends ReasignacionObserver {

	String legajoUsuarioResponsable
	Map responsablesActividades = [ : ]
	Map aprobadores = [ : ]

	/**
	 * *****************************************************************************************
	 *                                  N E G O C I O
	 * *****************************************************************************************
	 */
	def initialize(unPedido) {
		legajoUsuarioResponsable = unPedido.legajoUsuarioResponsable
		actividades(unPedido).each {  
			actividad -> responsablesActividades.putAt(actividad, actividad.legajoUsuarioResponsable)
		}
		if (unPedido.faseActual.equals(unPedido.faseAprobacionImplantacionHijo())) {
			aprobaciones(unPedido).each {
				aprobacion -> aprobadores.putAt(aprobacion, aprobacion.usuario)
			}
		}
		unPedido
	}

	def notifyGuardar(mapaInfo, usuarioLogueado) {
		def unPedido = mapaInfo.get("pedido")
		
		def rol = unPedido.rolDefault 
		
		procesarCambioUsuario(usuarioLogueado, unPedido, legajoUsuarioResponsable, unPedido.legajoUsuarioResponsable, "responsable " + unPedido, rol)
		
		actividades(unPedido).each { actividad ->
			def responsableOriginal = responsablesActividades.get(actividad)
			// Sólo queremos ver la reasignación de actividades, no cuando se genera automáticamente
			if (actividad.legajoUsuarioResponsable && actividad.tieneFechaGrabacion()) {
				procesarCambioUsuario(usuarioLogueado, unPedido, responsableOriginal, actividad.legajoUsuarioResponsable, "responsable de actividad " + actividad, RolAplicacion.RESPONSABLE_ACTIVIDAD)
			} 
		}
		aprobaciones(unPedido).each { aprobacion ->
			def responsableOriginal = aprobadores.get(aprobacion)
			if (responsableOriginal) {
				procesarCambioUsuario(usuarioLogueado, unPedido, responsableOriginal, aprobacion.legajoUsuarioResponsable, "aprobador", RolAplicacion.APROBADOR_FASE)
			} 
		}
		initialize(unPedido)
	}

	// Sólo queremos ver la reasignación de actividades, no cuando se genera automáticamente
	private def actividades(unPedido) {
		return unPedido.actividades
	}

	private def aprobaciones(unPedido) {
		return unPedido.aprobacionesPendientesDeFase(unPedido.faseActual)
	}

	public int hashCode() {
		return 90000L
	}
	
	public boolean equals(otroObserver) {
		return this.class.equals(otroObserver.class) && otroObserver.hashCode() == this.hashCode()
	}

}
