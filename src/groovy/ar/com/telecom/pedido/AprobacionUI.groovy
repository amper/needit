package ar.com.telecom.pedido

import ar.com.telecom.ume.UmeService

class AprobacionUI {

	boolean aprueba
	def usuario     // usuarioAprobador
	def justificacion
	def pedido 
	def grupo       // grupo asignatario
	def asignatario // usuario asignatario

	boolean validar() {
		if (!aprueba && !justificacion) {
			pedido.errors.reject("Debe ingresar justificaci\u00F3n para denegar")
			return false
		}
		return true
	}

	def actualizarDireccion() {
		actualizarDireccion(pedido.legajoGerenteUsuario)
	}
	
	def actualizarDireccion(legajo) {
		def usuario = new UmeService().getUsuario(legajo)
		pedido.setearDireccionDe(usuario)
	}

}
