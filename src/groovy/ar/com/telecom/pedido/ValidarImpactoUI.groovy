package ar.com.telecom.pedido

import ar.com.telecom.pcs.entities.AbstractPedido

class ValidarImpactoUI extends AprobacionUI {

	def legajoAprobadorImpacto

	def boolean validar(AbstractPedido unPedido) {
		if (aprueba && !legajoAprobadorImpacto) {
			unPedido.errors.reject "Debe ingresar un aprobador para el impacto"
		}
	}
}
