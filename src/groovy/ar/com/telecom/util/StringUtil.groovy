package ar.com.telecom.util

class StringUtil {

	public static String hasta(String text, int max) {
		if (text.length() > max) {
			return text.substring(0, max - 1)
		} else {
			return text
		}
	}
}
