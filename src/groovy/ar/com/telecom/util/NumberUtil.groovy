package ar.com.telecom.util

class NumberUtil {
	
	public final static int DEFAULT_DECIMALES = 2

	/**
	 * Recibe numero como BigDecimal y cantidad de decimales como entero
	 */
	static setearDecimales(numero, cantidadDecimales) {
		if (!numero) {
			return null
		}
		return numero.setScale(cantidadDecimales, BigDecimal.ROUND_HALF_DOWN)
	}

	static setearDecimalesDefault(numero) {
		return setearDecimales(numero, DEFAULT_DECIMALES)
	}

	static setearDecimalesDefaultConComa(numero) {
		return seteaComas(setearDecimales(numero, DEFAULT_DECIMALES))
	}
	
	static seteaComas(numero){
		if (numero != null) {
			return numero.toString().replace('.', ',')
		} else {
			return ""
		}
	}
	
	static toString(numero){
		if (numero == null) {
			return ""
		}
		def nuevoNumero = new BigDecimal(numero)
		
		return seteaComas(setearDecimales(nuevoNumero, DEFAULT_DECIMALES))
	}
	
	static orZero(numero) {
		if (!numero) {
			return 0
		} else {
			return numero
		}
	}
	
	static int cantidadDigitosEnteros(numero) {
		if (!numero) {
			return 0
		}
		return numero?.intValue().toString().length()
	}
}
