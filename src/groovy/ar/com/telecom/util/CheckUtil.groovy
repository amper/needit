package ar.com.telecom.util

class CheckUtil {

	/**
	 * Permite obtener el valor de un check con tres estados posibles
	 * true
	 * false
	 * null
	 */
	static Boolean getValue(Map params, String controlName) {
		Boolean value = null
		def _check = params.get("_" + controlName)
		if (_check != null) {
			def chkRealiza = params.get(controlName)
			value = chkRealiza != null
		}
		return value
	}
}
