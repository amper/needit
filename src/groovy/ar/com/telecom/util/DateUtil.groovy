package ar.com.telecom.util

import java.text.SimpleDateFormat

class DateUtil {

	static toString(aDate) {
		if (aDate) {
			return new SimpleDateFormat("dd/MM/yyyy").format(aDate)
		} else {
			return null
		}
	} 

	static toString(aDate, full) {
		if (aDate) {
			def dateFormat
			if (full) {
				dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm")
			} else {
				dateFormat = new SimpleDateFormat("dd/MM/yyyy")
			}
			return dateFormat.format(aDate)
		} else {
			return null
		}
	}

	static toDate(aString) {
		if (aString instanceof Date) {
			return aString
		} else {
			if (aString) {
				try {
					return new SimpleDateFormat("dd/MM/yyyy").parse(aString)
				} catch (Exception e) {
					return null
				}
			} else {
				return null
			}
		}
	} 

}
