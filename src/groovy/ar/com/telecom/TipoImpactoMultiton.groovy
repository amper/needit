package ar.com.telecom

import ar.com.telecom.pcs.entities.TipoImpacto

static class TipoImpactoMultiton {

	private static final Map<String, TipoImpacto> tiposImpacto = new HashMap<String, TipoImpacto>()

	/**
	 * Constantes para fases padre
	 */
	public static String AREA_SOPORTE = "Area de soporte"

	private TipoImpactoMultiton(){
	}

	static TipoImpacto getTipoImpacto(String key) {
		synchronized (tiposImpacto) {
			TipoImpacto tipoImpacto = tiposImpacto.get(key)

			if (tipoImpacto == null) {
				tipoImpacto = TipoImpacto.findByDescripcion(key)
				tiposImpacto.put(key, tipoImpacto)
			}

			return tipoImpacto
		}
	}
	
	static areaSoporte() {
		return getTipoImpacto(AREA_SOPORTE) 
	}
	
}