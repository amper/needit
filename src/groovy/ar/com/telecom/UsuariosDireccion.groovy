package ar.com.telecom

import java.util.Map

import ar.com.telecom.exceptions.BusinessException
import ar.com.telecom.ume.UmeService

class UsuariosDireccion {
	
	private static UsuariosDireccion instance = null

	private Map usuariosDireccion
	private Map direccionUsuarios
	private Map usuariosDireccionTemp
	
	private UsuariosDireccion() {
		usuariosDireccion = new HashMap()
		usuariosDireccionTemp = new HashMap()
		direccionUsuarios = new HashMap()
	}

	static UsuariosDireccion getInstance() {
		if (!instance){
			instance = new UsuariosDireccion()
		}
		return instance
	}
	
	def getDireccion(usuario) {
		return usuariosDireccion.get(usuario)
	}

	def getUsuarios(direccion) {
		return direccionUsuarios.get(direccion)
	}

	def getGerentes(direccion) {
		return direccionUsuarios.get(direccion).findAll { it.isGerente() }
	}

	def getAprobadoresImpacto(direccion) {
		return direccionUsuarios.get(direccion).findAll { it.isAprobadorImpacto() }
	}
	
	def relate(legajo, direccion) {
		usuariosDireccionTemp.put(legajo, direccion)
	}
	
	def commit(ume) {
		usuariosDireccion = usuariosDireccionTemp
		usuariosDireccionTemp = new HashMap()
		
		usuariosDireccion.each { legajo, direccion ->
			def listaUsuarios = direccionUsuarios.get(direccion) ?: []
			listaUsuarios.addAll(ume.getUsuario(legajo))
			direccionUsuarios.put(direccion, listaUsuarios)
		}
	}
	
	def getUsuariosDireccion() {
		return usuariosDireccion
	}

	def getDireccionUsuarios() {
		return direccionUsuarios
	}

	boolean direccionesCargadas() {
		return this.usuariosDireccion.isEmpty()
	}
	
}
