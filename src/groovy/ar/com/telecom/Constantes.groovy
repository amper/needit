package ar.com.telecom

import ar.com.telecom.pcs.entities.ParametrosSistema

static class Constantes {

	def parametros

	static instance

	static getInstance() {
		if (!instance) {
			instance = new Constantes()
		}
		return instance
	}
	
	private Constantes() {
	
	}
	
	public def getParametros() {
		return ParametrosSistema.list().first()
	}

}
