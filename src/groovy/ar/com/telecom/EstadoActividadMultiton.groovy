package ar.com.telecom

import ar.com.telecom.pcs.entities.EstadoActividad
import ar.com.telecom.pcs.entities.Fase

static class EstadoActividadMultiton {

	private static final Map<String, EstadoActividad> estadosActividad = new HashMap<String, EstadoActividad>()

	private EstadoActividadMultiton(){
	}

	static EstadoActividad getEstadosActividad(String key) {
		synchronized (estadosActividad) {
			EstadoActividad estadoActividad = estadosActividad.get(key)

			if (estadoActividad == null) {
				estadoActividad = EstadoActividad.findByDescripcion(key)
				estadosActividad.put(key, estadoActividad)
			}

			return estadoActividad
		}
	}
	
}