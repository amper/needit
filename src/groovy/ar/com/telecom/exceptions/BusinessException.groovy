package ar.com.telecom.exceptions

class BusinessException extends RuntimeException {

	/***
	 * Representa un error del negocio, ya sea por mala parametrización, error de usuario o 
	 * algo del circuito que está mal definido 
	 */
	
	public BusinessException(String msg) {
		super(msg)
	}
	
	public BusinessException(String msg, Throwable e) {
		super(msg, e)
	}
	
}
