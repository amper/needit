package ar.com.telecom.exceptions

class UserWithoutRolesException extends RuntimeException {

	/***
	 * Ocurre cuando el usuario se loguea pero no tiene roles definidos 
	 */
	
	public UserWithoutRolesException(String msg) {
		super(msg)
	}
	
	public UserWithoutRolesException(String msg, Throwable e) {
		super(msg, e)
	}
	
}
