package ar.com.telecom.exceptions

class ConcurrentAccessException extends RuntimeException {

	/***
	* Representa el error que se produce cuando hay un acceso a una actividad o aprobación y otro usuario modificó previamente los datos que tenía el usuario logueado
	*/

   public ConcurrentAccessException() {
	   super()
   }
   
   public ConcurrentAccessException(String msg) {
	   super(msg)
   }
   
   public ConcurrentAccessException(String msg, Throwable e) {
	   super(msg, e)
   }
   
}
