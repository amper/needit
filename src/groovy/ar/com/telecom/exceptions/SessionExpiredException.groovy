package ar.com.telecom.exceptions

class SessionExpiredException extends RuntimeException {

	/***
	 * Representa el error que se produce cuando expira la sesión de usuario 
	 */

	public SessionExpiredException() {
		super()
	}
	
	public SessionExpiredException(String msg) {
		super(msg)
	}
	
	public SessionExpiredException(String msg, Throwable e) {
		super(msg, e)
	}
	
}
