package ar.com.telecom.exceptions

class SystemException extends RuntimeException {

	/***
	 * Representa un error del sistema, por mala programación o por condiciones externas al contexto del programa
	 */
	
	public SystemException(String msg) {
		super(msg)
	}
	
	public SystemException(String msg, Throwable e) {
		super(msg, e)
	}
	
}
