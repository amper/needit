package ar.com.telecom

import ar.com.telecom.pcs.entities.Fase

static class FaseMultiton {

	private static final Map<String, Fase> fases = new HashMap<String, Fase>()

	/**
	 * Constantes para fases padre
	 */
	public static String REGISTRACION_PEDIDO = "REG"
	public static String VALIDACION_PEDIDO = "VAL"
	public static String APROBACION_PEDIDO = "APR"
	public static String EVALUACION_CAMBIO = "EVC"
	public static String ESPECIFICACION_CAMBIO = "ECA"
	public static String APROBACION_CAMBIO = "ACA"
	public static String CONSOLIDACION_IMPACTO = "CIM"
	public static String VALIDACION_IMPACTO = "VIM"
	public static String APROBACION_IMPACTO = "AIM"
	public static String CONSTRUCCION = "CON"	
	public static String FINALIZACION = "FIN"

	/**
	 * Constantes para fases hijas
	 */
	public static String ESPECIFICACION_IMPACTO_HIJO = "EIMH"
	public static String CONSTRUCCION_CAMBIO_HIJO = "CCAH"
	public static String EJECUCION_SOPORTE_PAU_HIJO = "ESPH"
	public static String APROBACION_PAU_HIJO = "APAUH"
	public static String APROBACION_IMPLANTACION_HIJO = "AIMPH"
	public static String IMPLANTACION_HIJO = "IMPLH"
	public static String NORMALIZACION_HIJO = "NORMH"
	public static String CONSTRUCCION_PU_HIJO = "CSAPH"
	public static String ADMINISTRAR_ACTIVIDADES_HIJO = "AACTH"

	private FaseMultiton(){
	}

	static Fase getFase(String key) {
		synchronized (fases) {
			Fase fase = fases.get(key)

			if (fase == null) {
				fase = Fase.findByCodigoFase(key)
				fase?.traerDatosRelacionados()
				fases.put(key, fase)
			}

			return fase
		}
	}
	
}