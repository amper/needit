package ar.com.telecom

import ar.com.telecom.util.NumberUtil

class Reporte {

	String direccionCodigo
	List usuarios
	List pedidosEstandar
	List pedidosEmergencia
	
	public String toString() {
		return "direccionCodigo $direccionCodigo"
	}
		
	public Reporte() {
		usuarios = new ArrayList()
		pedidosEstandar = new ArrayList()
		pedidosEmergencia = new ArrayList()
	}
	
	public getTotal(pedidos){
		//def pedidos = pedidosEstandar + pedidosEmergencia
		return (!pedidos || pedidos.isEmpty())?0:pedidos.collect{ it?.totalGeneral()}.sum()
	}
	
	public getTotalHoras(pedidos){
		//def pedidos = pedidosEstandar + pedidosEmergencia
		return (!pedidos || pedidos.isEmpty())?0:pedidos.collect{ it?.totalHorasPlanificadas()}.sum()
	}
}
