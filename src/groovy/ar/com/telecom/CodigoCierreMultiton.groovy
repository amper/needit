package ar.com.telecom

import java.util.Map;

import ar.com.telecom.pcs.entities.CodigoCierre;

class CodigoCierreMultiton {
	private static final Map<String, CodigoCierre> codigosCierre = new HashMap<String, CodigoCierre>()
	
		private CodigoCierreMultiton(){
		}
	
		static CodigoCierre getCodigosCierre(String key) {
			synchronized (codigosCierre) {
				CodigoCierre codigoCierre = codigosCierre.get(key)
	
				if (codigoCierre == null) {
					codigoCierre = CodigoCierre.findByDescripcion(key)
					codigosCierre.put(key, codigoCierre)
				}
	
				return codigoCierre
			}
		}
	
}
