jQuery(document)
		.ready(

				function($) {

					/* Botones y datepickers */

					$("input:button").button({
						icons : {
							primary : "ui-icon-close"
						}
					});

					$("#datepicker-fechaDesde-rpid").datepicker();
					$("#datepicker-fechaDesde-rpid").datepicker("option",
							"dateFormat", "dd/mm/yy");
					$("#datepicker-fechaDesde-rpid").val(
							document.getElementById("fechaDesdeHidden").value);

					$("#datepicker-fechaHasta-rpid").datepicker();
					$("#datepicker-fechaHasta-rpid").datepicker("option",
							"dateFormat", "dd/mm/yy");
					$("#datepicker-fechaHasta-rpid").val(
							document.getElementById("fechaHastaHidden").value);

					
					$("#comboboxFase").multiselect({
						checkAllText: 'Todos',
						uncheckAllText: 'Ninguno',
						noneSelectedText: 'Seleccione una Fase',
						selectedText: function(numChecked, numTotal, checkedItems){
							return numChecked + ' de ' + numTotal + ' seleccionadas';
						},
						create : function(event, ui) {
							$(this).multiselect("uncheckAll");
							var contentDiv = $(this).multiselect().parent().find("div")[0];
							contentDiv.style.width = 350;
						}
					}).multiselectfilter({
						label: 'Buscar',
						width: 80
					});
					
					$("#comboboxTiposAnexos").multiselect({
						checkAllText: 'Todos',
						uncheckAllText: 'Ninguno',
						noneSelectedText: 'Seleccione un Tipo de Anexo',
						selectedText: function(numChecked, numTotal, checkedItems){
							return numChecked + ' de ' + numTotal + ' seleccionados';
						},
						create : function(event, ui) {
							$(this).multiselect("uncheckAll");
							var contentDiv = $(this).multiselect().parent().find("div")[0];
							contentDiv.style.width = 350;
						}
					}).multiselectfilter({
						label: 'Buscar',
						width: 80
					});
					
					
					$("#comboboxRolAplicacion").multiselect({
						checkAllText: 'Todos',
						uncheckAllText: 'Ninguno',
						noneSelectedText: 'Seleccione un Rol',
						selectedText: function(numChecked, numTotal, checkedItems){
							return numChecked + ' de ' + numTotal + ' seleccionados';
						},
						create : function(event, ui) {
							$(this).multiselect("uncheckAll");
							var contentDiv = $(this).multiselect().parent().find("div")[0];
							contentDiv.style.width = 350;
						}
					}).multiselectfilter({
						label: 'Buscar',
						width: 80
					});
					
					$(function() {
						
						
					});

					/* combobox con autocomplete */

					(function($) {
						$
								.widget(
										"ui.combobox",
										{
											_create : function() {
												var self = this, select = this.element
														.hide(), selected = select
														.children(":selected"), value = selected
														.val() ? selected
														.text() : "";
												var input = this.input = $(
														"<input>")
														.insertAfter(select)
														.attr(
																'id',
																$(select).attr(
																		'id')
																		+ '-input')
														.val(value)
														.autocomplete(
																{
																	delay : 0,
																	minLength : 0,
																	source : function(
																			request,
																			response) {
																		var matcher = new RegExp(
																				$.ui.autocomplete
																						.escapeRegex(request.term),
																				"i");
																		response(select
																				.children(
																						"option")
																				.map(
																						function() {
																							var text = $(
																									this)
																									.text();
																							if (this.value
																									&& (!request.term || matcher
																											.test(text)))
																								return {
																									label : text
																											.replace(
																													new RegExp(
																															"(?![^&;]+;)(?!<[^<>]*)("
																																	+ $.ui.autocomplete
																																			.escapeRegex(request.term)
																																	+ ")(?![^<>]*>)(?![^&;]+;)",
																															"gi"),
																													"<strong>$1</strong>"),
																									value : text,
																									option : this
																								};
																						}));
																	},
																	select : function(
																			event,
																			ui) {
																		ui.item.option.selected = true;
																		self
																				._trigger(
																						"selected",
																						event,
																						{
																							item : ui.item.option
																						});
																		select
																				.trigger("change");
																	},
																	change : function(
																			event,
																			ui) {
																		if (!ui.item) {
																			var matcher = new RegExp(
																					"^"
																							+ $.ui.autocomplete
																									.escapeRegex($(
																											this)
																											.val())
																							+ "$",
																					"i"), valid = false;
																			select
																					.children(
																							"option")
																					.each(
																							function() {
																								if ($(
																										this)
																										.text()
																										.match(
																												matcher)) {
																									this.selected = valid = true;
																									return false;
																								}
																							});
																			if (!valid) {
																				// remove
																				// invalid
																				// value,
																				// as
																				// it
																				// didn't
																				// match
																				// anything
																				$(
																						this)
																						.val(
																								"");
																				select
																						.val("");
																				input
																						.data("autocomplete").term = "";
																				return false;
																			}
																		}
																	}
																})
														.addClass(
																"ui-widget ui-widget-content ui-corner-left");

												input.data("autocomplete")._renderItem = function(
														ul, item) {
													return $("<li></li>")
															.data(
																	"item.autocomplete",
																	item)
															.append(
																	"<a>"
																			+ item.label
																			+ "</a>")
															.appendTo(ul);
												};

												this.button = $(
														"<button type='button'>&nbsp;</button>")
														.attr("tabIndex", -1)
														.attr("title",
																"Show All Items")
														.insertAfter(input)
														.button(
																{
																	icons : {
																		primary : "ui-icon-triangle-1-s"
																	},
																	text : false
																})
														.removeClass(
																"ui-corner-all")
														.addClass(
																"ui-corner-right ui-button-icon")
														.click(
																function() {
																	// close if
																	// already
																	// visible
																	if (input
																			.autocomplete(
																					"widget")
																			.is(
																					":visible")) {
																		input
																				.autocomplete("close");
																		return;
																	}

																	// work
																	// around a
																	// bug
																	// (likely
																	// same
																	// cause as
																	// #5265)
																	$(this)
																			.blur();

																	// pass
																	// empty
																	// string as
																	// value to
																	// search
																	// for,
																	// displaying
																	// all
																	// results
																	input
																			.autocomplete(
																					"search",
																					"");
																	input
																			.focus();
																});
											},

											destroy : function() {
												this.input.remove();
												this.button.remove();
												this.element.show();
												$.Widget.prototype.destroy
														.call(this);
											}
										});
					})(jQuery);

					/* fin combobox */
				});

function clear_form_elements() {
	$('fechaDesde').value = '';
	$('fechaDesdeHidden').value = '';
	$('descripcion').value = '';
	$('fechaHasta').value = '';
	$('fechaHastaHidden').value = '';
	
//	ajaxCall({
//		container: 'legajoUsuario',
//		controller: controllerActual,
//		action:'completarAsignatarios',
//		parameters : {
//			grupoGestionDemanda: grupoLDAP, 
//			box:'legajoUsuarioGestionDemanda'
//		}
//	});
	
	new Ajax.Updater('legajoUsuario', '/' + gNameApp + '/anexo/completarUsuario/anexo', {
		asynchronous : true,
		evalScripts : true,
		parameters : 'username=&box=legajoUsuario&pedidoId='
				+ $('pedidoId').value
	});
	
	jQuery("#comboboxFase").multiselect("uncheckAll");
	jQuery("#comboboxTiposAnexos").multiselect("uncheckAll");
	jQuery("#comboboxRolAplicacion").multiselect("uncheckAll");
	
	return false;
}

function validarFormulario() {
	if ($('datepicker-fechaDesde-rpid').value > $('datepicker-fechaHasta-rpid').value) {
		alert("La Fecha Desde no puede ser mayor a la Fecha Hasta");
	} else {
		document.formListAnexo.submit();
	}
}

function validarBusqueda() {
	document.getElementById('formListAnexo').submit();
}

function confirmaBorrarAnexoGlobal(url) {
	if (confirm("¿Est\u00E1 seguro que desea eliminar el anexo global?")) {
		document.location = url;
	}
}