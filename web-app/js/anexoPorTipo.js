var idGlobal = '';
var gFecha = '';
//var gPedidoID = 0;

jQuery(document).ready(
		
		function($) {
			$("#dialog-anexos").dialog({
				autoOpen : false,
				resizable : false,
				height : "100%",
				width : 500,
				modal : true
			});
			
			$("#nuevoAnexo").click(function() {
				displayAnexoArchivo("");
				//openPopUpAnexo();
				return false;
			});
			
			$("#nuevoAnexoGlobal").click(function() {
				//$("#dialog-anexos").dialog("open");
				openPopUpAnexo();
				return false;
			});

			$("#nuevoAnexoEstrategiaPrueba").click(function() {
				//displayAnexoArchivo();
				displayAnexoArchivo("EP");
				return false;
			});
			
			$("#nuevoAnexoDisenioExterno").click(function() {
				//displayAnexoArchivo();
				displayAnexoArchivo("DE");
				return false;
			});
			
			$("#cerrarAnexo").click(function() {
				$("#dialog-anexos").dialog("close");
				return false;
			});
			
			$(function() {
				$( "#comboboxTipoAnexo" ).combobox();
			});
			
			$("#dialog:ui-dialog").dialog("destroy");
			/*combobox con autocomplete*/
			
			(function( $ ) {
				$.widget( "ui.combobox", {
					_create: function() {
						var self = this,
							select = this.element.hide(),
							selected = select.children( ":selected" ),
							value = selected.val() ? selected.text() : "";
						var input = this.input = $( "<input>" )
							.insertAfter( select )
							.attr( 'id', $(select).attr( 'id' )+'-input' )
							.val( value )
							.autocomplete({
								delay: 0,
								minLength: 0,
								source: function( request, response ) {
									var matcher = new RegExp( $.ui.autocomplete.escapeRegex(request.term), "i" );
									response( select.children( "option" ).map(function() {
										var text = $( this ).text();
										if ( this.value && ( !request.term || matcher.test(text) ) )
											return {
												label: text.replace(
													new RegExp(
														"(?![^&;]+;)(?!<[^<>]*)(" +
														$.ui.autocomplete.escapeRegex(request.term) +
														")(?![^<>]*>)(?![^&;]+;)", "gi"
													), "<strong>$1</strong>" ),
												value: text,
												option: this
											};
									}) );
								},
								select: function( event, ui ) {
									ui.item.option.selected = true;
									self._trigger( "selected", event, {
										item: ui.item.option
									});
									select.trigger("change");   
								},
								change: function( event, ui ) {
									if ( !ui.item ) {
										var matcher = new RegExp( "^" + $.ui.autocomplete.escapeRegex( $(this).val() ) + "$", "i" ),
											valid = false;
										select.children( "option" ).each(function() {
											if ( $( this ).text().match( matcher ) ) {
												this.selected = valid = true;
												return false;
											}
										});
										if ( !valid ) {
											// remove invalid value, as it didn't match anything
											$( this ).val( "" );
											select.val( "" );
											input.data( "autocomplete" ).term = "";
											return false;
										}
									}
								},
								maxItemsToShow:5
							})
							.addClass( "ui-widget ui-widget-content ui-corner-left" );

						input.data( "autocomplete" )._renderItem = function( ul, item ) {
							return $( "<li></li>" )
								.data( "item.autocomplete", item )
								.append( "<a>" + item.label + "</a>" )
								.appendTo( ul );
						};

						this.button = $( "<button type='button'>&nbsp;</button>" )
							.attr( "tabIndex", -1 )
							.attr( "title", "Show All Items" )
							.insertAfter( input )
							.button({
								icons: {
									primary: "ui-icon-triangle-1-s"
								},
								text: false
							})
							.removeClass( "ui-corner-all" )
							.addClass( "ui-corner-right ui-button-icon" )
							.click(function() {
								// close if already visible
								if ( input.autocomplete( "widget" ).is( ":visible" ) ) {
									input.autocomplete( "close" );
									return;
								}

								// work around a bug (likely same cause as #5265)
								$( this ).blur();

								// pass empty string as value to search for, displaying all results
								input.autocomplete( "search", "" );
								input.focus();
							});
					},

					destroy: function() {
						this.input.remove();
						this.button.remove();
						this.element.show();
						$.Widget.prototype.destroy.call( this );
					}
				});
			})( jQuery );
			
			/*fin combobox*/			
		}
);

function openDialog(){
	jQuery("#dialog-anexos").dialog("open");
}

function displayAnexoArchivo(from) {
//	$('hVengoPorAnexos').value = 'true';
	var formValue = window.parent.document.getElementById('hForm');
	var form = window.parent.document.getElementById(formValue.value);
	var action = 'grabarFormulario';
	
	var formulario = Form.serialize(form);
	formulario = quitarParametro(formulario, "_action_navegarFase");
	
	if (from != ''){
		$('anexoFrom').value = from;
	} 
	
	if ($('hVengoPorAnexos').value=='especificacionCambio'){
		
		formulario = quitarParametro(formulario, "especificacionDetallada");
		formulario = quitarParametro(formulario, "fueraAlcance");
		formulario = quitarParametro(formulario, "alcanceDetallado");
		formulario = quitarParametro(formulario, "notas");
		formulario = quitarParametro(formulario, "beneficiosEsperados");
		
		if (tinyMCE.get('especificacionDetallada').getContent()!=''){
			formulario += "&especificacionDetallada=" + escape(tinyMCE.get('especificacionDetallada').getContent());
		}
		if (tinyMCE.get('fueraAlcance').getContent()!=''){
			formulario += "&fueraAlcance=" + escape(tinyMCE.get('fueraAlcance').getContent());
		}
		if (tinyMCE.get('alcanceDetallado').getContent()!=''){
			formulario += "&alcanceDetallado=" + escape(tinyMCE.get('alcanceDetallado').getContent());
		}
		if (tinyMCE.get('notas').getContent()!=''){
			formulario += "&notas=" + escape(tinyMCE.get('notas').getContent());
		}
		if (tinyMCE.get('beneficiosEsperados').getContent()!=''){
			formulario += "&beneficiosEsperados=" + escape(tinyMCE.get('beneficiosEsperados').getContent());
		}
	}else if ($('hVengoPorAnexos').value=='registroPedido'){
		
		formulario = quitarParametro(formulario, "descripcion");
		formulario = quitarParametro(formulario, "alcance");
		formulario = quitarParametro(formulario, "objetivo");
		
		if (tinyMCE.get('inputDescripcion').getContent()!=''){
			formulario += "&descripcion=" + escape(tinyMCE.get('inputDescripcion').getContent());
		}
		if (tinyMCE.get('inputAlcance').getContent()!=''){
			formulario += "&alcance=" + escape(tinyMCE.get('inputAlcance').getContent());
		}
		if (tinyMCE.get('inputObjetivo').getContent()!=''){
			formulario += "&objetivo=" + escape(tinyMCE.get('inputObjetivo').getContent());
		}
		action = 'guardarFormulario';
	}
	
	ajaxCallRequest({
		controller: $('hVengoPorAnexos').value,
		action: action,
		parameters : formulario,
		onComplete: function (transport){
			if ($('pedidoId').value == ""){
				var pagina = transport.responseText;
				var idPedido = "";
				var pattern = "?pedidoId=";
				if (pagina.indexOf(pattern, 0) != -1){
					var indexInicial = pagina.indexOf(pattern ,0);
					var indexFinal = pagina.indexOf('"' ,indexInicial);
					idPedido = pagina.substring(indexInicial + pattern.length , indexFinal);
					if (idPedido){
						$('pedidoId').value = idPedido;
					}
				}
			}
			openPopUpAnexo();
		}
	});
	//form.submit();
}

/*function displayAnexoArchivoConsolidacion(from){
//	$('hVengoPorAnexos').value = 'true';
	$('anexoFrom').value = from;
	var formValue = window.parent.document.getElementById('hForm');
	var form = window.parent.document.getElementById(formValue.value);
	
	var formulario = Form.serialize(form).replace('_action_navegarFase=index&', '');
		
	ajaxCallRequest({
		controller: 'consolidacionImpacto',
		action:'grabarFormulario',
		parameters : formulario,
		onComplete: function (event){
			openPopUpAnexo();
		}
	});
	
	//form.submit();
}*/

function openPopUpAnexo(){
	
	var formulario = "";
	if ($('pedidoId')){
		formulario += "pedidoId=" + $('pedidoId').value;
	}
	if ($('tabSeleccionado') && $('tabSeleccionado').value){
		formulario+= "&tabSeleccionado=" + $('tabSeleccionado').value;
	}
	if ($('impacto') && $('impacto').value){
		formulario+= "&impacto=" + $('impacto').value;
	}
	if ($('anexoFrom') && $('anexoFrom').value){
		formulario+= "&anexoFrom=" + $('anexoFrom').value;
	}
	
	var controllerAnexo = "anexo";
	if ($('hVengoPorAnexos') && $('hVengoPorAnexos').value != ''){
		controllerAnexo = $('hVengoPorAnexos').value;
	}
	
	
	// Solo para cuando cargo anexos por el popup de suspension
	if ($('hAnexoSuspension') && $('hAnexoSuspension').value != ''){
		controllerAnexo = $('hAnexoSuspension').value;
		formulario+= "&popSuspension=true";
		
		var comentarioSuspension = "";
		if($('comentarioSuspension')){
			comentarioSuspension = $('comentarioSuspension').value;
			formulario+= "&comentarioSuspension="+ comentarioSuspension;
		}

		var comentarioReanudacion = "";
		if($('comentarioReanudacion')){
			comentarioReanudacion = $('comentarioReanudacion').value;
			formulario+= "&comentarioReanudacion="+ comentarioReanudacion;
		}
				
		
		var motivo = "";
		if($('motivo')){
			motivo = $('motivo').value;
			formulario+= "&motivoSuspension.id="+ motivo
		}
		
		
//		formulario+= "&motivoSuspension[="+ $('motivo').value;
//		motivoSuspension:[id:1]
		
		
	}
	ajaxCall({
		container: 'dialog-anexos',
		controller: controllerAnexo,
		action:'popupAnexo',
		parameters : formulario,
		onComplete: function (event){
			jQuery('#dialog-anexos').dialog("open");
			jQuery('#dialog-anexos').dialog("option","height", "100%");
		}
	});
}

function openPopUpAnexoSuspension(){
	jQuery("#dialog-anexos").dialog({
		autoOpen : false,
		resizable : false,
		height : "100%",
		width : 500,
		modal : true
	});
	
	openPopUpAnexo();
}

function validarAnexo(extensionesHabilitadas){
	var arch = document.getElementById('anexoArchivo').value;
	var valueCombo = document.getElementById('comboboxTipoAnexo').value;
	var esValido = esArchivoValido(arch, extensionesHabilitadas);
	
	if (valueCombo != 'null' && arch != '' && esValido=='') {
		document.formAnexo.submit();
	}else{
		var strError = esValido;
		if (arch==''){
			strError += "Debe ingresar un archivo.\n";
		}
		if(valueCombo=='null'){
			strError += "Debe ingresar un tipo de anexo.\n";
		}
		alert (strError);
	}
}

function esArchivoValido(archivo, extensionesHabilitadas){
	var name = archivo.split("").reverse().join("");
	var punto = name.indexOf('.');
	if (punto==-1){
		return "Archivo no valido.\n";
	}
	
	var ext = archivo;
	ext = ext.substring(ext.length-punto,ext.length);
	ext = ext.toLowerCase();
	if(jQuery.inArray(ext, extensionesHabilitadas) != -1){
		return ""; 
	}else{
		return "Extensi\u00F3n no v\u00E1lida. Solo son posibles: " + extensionesHabilitadas + ".\n";
	}
}