function mostrarMenuUsuario(id, ocultar){
	if (ocultar){
		$('MenuUsuario').style.backgroundColor = '#444';
		$('MenuUsuario').style.border = '1px solid #444';
		$(id).style.display = 'none';
	}else{
		$('MenuUsuario').style.backgroundColor = '#666';
		$('MenuUsuario').style.border = '1px solid #444';
		$(id).style.display = 'block';	
	}
	
}


jQuery(document).ready(
function($) {

	// independently show and hide
	// $('div.seccionHide:eq(0) > div').hide();
	$('div.seccionHide:eq(0) > p').click(function() {
		$(this).next().slideToggle('fast');
	});

	$('div.seccionHide2:eq(0) > p').click(function() {
		$(this).next().slideToggle('fast');
	});
	
	$('div.seccionHideLateral:eq(0) > p').click(function() {
		$(this).next().slideToggle('fast');
	});
	
	$('div.seccionHide3:eq(0) > p').click(function() {
		$(this).next().slideToggle('fast');
	});
	
	$('div.seccionHide4:eq(0) > p').click(function() {
		$(this).next().slideToggle('fast');
	});
	
	$('div.seccionHide5:eq(0) > p').click(function() {
		$(this).next().slideToggle('fast');
	});
	
	$('div.seccionHide3:eq(0) > div').hide();
	$('div.seccionHide4:eq(0) > div').hide();
	$('div.seccionHide5:eq(0) > div').hide();
	
	$('#ocultarPanel').click(function(event) {
		event.preventDefault();
		$("#contenedorDcho").animate({
			right : -180
		}, 500);
		$("#contenedorIzq").animate({
			width : 858
		}, 500);
		$("#contenedorDchoMin").animate({
			right : 0
		}, 500);
	});

	$('#mostrarPanel').click(function(event) {
		event.preventDefault();
		$("#contenedorDchoMin").animate({
			right : -42
		}, 500);
		$("#contenedorDcho").animate({
			right : 0
		}, 500);
		$("#contenedorIzq").animate({
			width : 678
		}, 500);
	});

	$("#changeInterloc").dialog({
		autoOpen: false,
		resizable : false,
		height : 200,
		width: 400,
		modal : true,
		buttons : {
			"Si" : function() {
				
				//navegarFase(controllerNombre);
				$(this).dialog("close");
			},
			"No" : function() {
				$(this).dialog("close");
			}
		}
	});
	
	$("#interChange").click(function() {
		$("#changeInterloc").dialog("open");
	});
	
	//modal MI PERFIL en SubMenuUsuario
	$("#MiPerfilModal").dialog({
		autoOpen: false,
		resizable : false,
		height : 200,
		width: 400,
		modal : true,
		buttons : {
			"Cerrar" : function() {
				$(this).dialog("close");
			}
		}
	});
	
	$("#MiPerilClick").click(function() {
		$("#MiPerfilModal").dialog("open");
	});

	
});

//workflow Diagram

function quitarParametro(parametros, clave){
	var parametrosFinal = "";
	var spliteado = parametros.split("&");
	for (var i = 0; i < spliteado.size(); i++){
		if (spliteado[i].indexOf(clave, 0)==-1){
			parametrosFinal += spliteado[i] + "&";
		}
	}
	return parametrosFinal;
}

function obtenerParametroHTML(pagina, id){
	var idActividad = "";
	var pattern = "=\""+id+"\" value=\"";
	if (pagina.indexOf(pattern, 0) != -1){
		var indexInicial = pagina.indexOf(pattern ,0) ;
		if (indexInicial != -1){
			indexInicial = indexInicial + pattern.length;
			var indexFinal = pagina.indexOf('"' ,indexInicial);
			return pagina.substring(indexInicial, indexFinal);
		}
	}
	return "";
}

function cierreVentana(controller, parametros){
	var syncToken = window.opener.document.getElementById("org.codehaus.groovy.grails.SYNCHRONIZER_TOKEN");
	if (syncToken){
		new Ajax.Updater(list, '/' + gNameApp + '/' + controller + '/actualizarToken',{
			asynchronous:false,
			evalScripts:true,
			parameters:	parametros,
			onComplete: function (e){
				if(e.responseText){
					syncToken.value = e.responseText;
				}
			}
		});
	}
}

function updateTemplateToken(controller){
	new ajaxCall({
		container: 'token',
		controller:controller,
		action:'updateToken',
		parameters: {
			controller : controller
		}
	});
}


function confirmarCambioDireccion(){
	// Si existen los botones de la botonera, muestro popup
	if((document.getElementById('aprobar')!=null || document.getElementById('Guardar')!=null || document.getElementById('Denegar')) && (document.getElementById('muestraPopDireccion').value=='true')){
		
		var continuar = confirm('Se modificar\u00E1 la direcci\u00F3n del pedido acorde a la gerencia actual. \u00BFDesea continuar?');
		if (continuar) {
			var actionFrom = document.getElementById('actionFromDireccion').value;
			document.getElementById('confirmaCambioDireccion').value = true;
			
			
			if(actionFrom == 'aprobarPedido'){
				document.getElementById('aprobar').click();
			}
			if(actionFrom == 'grabarFormulario'){
				document.getElementById('Guardar').click();
			}
			if(actionFrom == 'denegarPedido'){
				document.getElementById('Denegar').click();
			}
		}
	}
}