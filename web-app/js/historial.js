jQuery(document)
		.ready(

				function($) {

					/* Botones y datepickers */

					$("input:button").button({
						icons : {
							primary : "ui-icon-close"
						}
					});

					var fecha = document.getElementById('fechaHoyHidden').value;
					var dt1  = fecha.substring(0,2); 
			        var mon1 = fecha.substring(3,5); 
			        var yr1  = fecha.substring(6,10);  
			        
					var tiempoMax = Date.parse(mon1 + "/" + dt1 + "/" + yr1);
					
					$("#datepicker-fechaDesde-rpid").datepicker({
						maxDate : new Date(tiempoMax)
					});
					$("#datepicker-fechaDesde-rpid").datepicker("option","dateFormat", "dd/mm/yy");
					$("#datepicker-fechaDesde-rpid").val(document.getElementById("fechaDesdeHidden").value);

					$("#datepicker-fechaHasta-rpid").datepicker({
						maxDate : new Date(tiempoMax)
					});
					$("#datepicker-fechaHasta-rpid").datepicker("option", "dateFormat", "dd/mm/yy");
					$("#datepicker-fechaHasta-rpid").val(document.getElementById("fechaHastaHidden").value);
		
					$("#comboboxMacroestado").multiselect({
						checkAllText: 'Todos',
						uncheckAllText: 'Ninguno',
						noneSelectedText: 'Seleccione un Macroestado',
						selectedText: function(numChecked, numTotal, checkedItems){
							obtenerFases('');
							return numChecked + ' de ' + numTotal + ' seleccionados';
						},
						create : function(event, ui) {
							$(this).multiselect("uncheckAll");
							var contentDiv = $(this).multiselect().parent().find("div")[0];
							contentDiv.style.width = 350;
							// Set Defaults
							if(macroEstados != null) {
								$(this).multiselect("widget").find(":checkbox").each(function(i, ele) {
									if($.inArray(parseInt(ele.value), macroEstados) != -1) {
										this.click();
									}
								});
							}
						}
					}).multiselectfilter({
						label: 'Buscar',
						width: 80
					});
					
					// Select Manually
					/*$("select").multiselect("widget").find(":checkbox").each(function(){
						this.click();
					});*/
					
					$("#comboboxRolAplicacion").multiselect({
						checkAllText: 'Todos',
						uncheckAllText: 'Ninguno',
						noneSelectedText: 'Seleccione un Rol',
						selectedText: function(numChecked, numTotal, checkedItems){
							return numChecked + ' de ' + numTotal + ' seleccionados';
						},
						create : function(event, ui) {
							$(this).multiselect("uncheckAll");
							var contentDiv = $(this).multiselect().parent().find("div")[0];
							contentDiv.style.width = 350;
							// Set Defaults
							if(rolAplicaciones != null) {
								$(this).multiselect("widget").find(":checkbox").each(function(i, ele) {
									if($.inArray(parseInt(ele.value), rolAplicaciones) != -1) {
										this.click();
									}
								});
							}
						}
					}).multiselectfilter({
						label: 'Buscar',
						width: 80
					});
					
					/*
					$("#comboboxTiposAnexos").multiselect({
						checkAllText: 'Todos',
						uncheckAllText: 'Ninguno',
						noneSelectedText: 'Seleccione un Tipo de Anexo',
						selectedText: function(numChecked, numTotal, checkedItems){
							return numChecked + ' de ' + numTotal + ' seleccionados';
						},
						create : function(event, ui) {
							$(this).multiselect("uncheckAll");
							var contentDiv = $(this).multiselect().parent().find("div")[0];
							contentDiv.style.width = 350;
						}
					}).multiselectfilter({
						label: 'Buscar',
						width: 80
					});
					*/
					
					$("#comboboxTipoEvento").multiselect({
						checkAllText: 'Todos',
						uncheckAllText: 'Ninguno',
						noneSelectedText: 'Seleccione un Tipo de Evento',
						selectedText: function(numChecked, numTotal, checkedItems){
							return numChecked + ' de ' + numTotal + ' seleccionados';
						},
						create : function(event, ui) {
							$(this).multiselect("uncheckAll");
							var contentDiv = $(this).multiselect().parent().find("div")[0];
							contentDiv.style.width = 350;
							// Set Defaults
							if(tipoEventos != null) {
								$(this).multiselect("widget").find(":checkbox").each(function(i, ele) {
									if($.inArray(ele.value, tipoEventos) != -1) {
										this.click();
									}
								});
							}
						}
					}).multiselectfilter({
						label: 'Buscar',
						width: 80
					});
					
					$(function() {
						//$("#comboboxMacroestado").combobox();
						//$("#comboboxFase").combobox();
//						$("#comboboxRol").combobox();
						//$("#comboboxTipoEvento").combobox();
						//$("#comboboxTipoEvento2").combobox();
					});

					/* combobox con autocomplete */

					(function($) {
						$
								.widget(
										"ui.combobox",
										{
											_create : function() {
												var self = this, select = this.element
														.hide(), selected = select
														.children(":selected"), value = selected
														.val() ? selected
														.text() : "";
												var input = this.input = $(
														"<input>")
														.insertAfter(select)
														.attr(
																'id',
																$(select).attr(
																		'id')
																		+ '-input')
														.val(value)
														.autocomplete(
																{
																	delay : 0,
																	minLength : 0,
																	source : function(
																			request,
																			response) {
																		var matcher = new RegExp(
																				$.ui.autocomplete
																						.escapeRegex(request.term),
																				"i");
																		response(select
																				.children(
																						"option")
																				.map(
																						function() {
																							var text = $(
																									this)
																									.text();
																							if (this.value
																									&& (!request.term || matcher
																											.test(text)))
																								return {
																									label : text
																											.replace(
																													new RegExp(
																															"(?![^&;]+;)(?!<[^<>]*)("
																																	+ $.ui.autocomplete
																																			.escapeRegex(request.term)
																																	+ ")(?![^<>]*>)(?![^&;]+;)",
																															"gi"),
																													"<strong>$1</strong>"),
																									value : text,
																									option : this
																								};
																						}));
																	},
																	select : function(
																			event,
																			ui) {
																		ui.item.option.selected = true;
																		self
																				._trigger(
																						"selected",
																						event,
																						{
																							item : ui.item.option
																						});
																		select
																				.trigger("change");
																	},
																	change : function(
																			event,
																			ui) {
																		if (!ui.item) {
																			var matcher = new RegExp(
																					"^"
																							+ $.ui.autocomplete
																									.escapeRegex($(
																											this)
																											.val())
																							+ "$",
																					"i"), valid = false;
																			select
																					.children(
																							"option")
																					.each(
																							function() {
																								if ($(
																										this)
																										.text()
																										.match(
																												matcher)) {
																									this.selected = valid = true;
																									return false;
																								}
																							});
																			if (!valid) {
																				// remove
																				// invalid
																				// value,
																				// as
																				// it
																				// didn't
																				// match
																				// anything
																				$(
																						this)
																						.val(
																								"");
																				select
																						.val("");
																				input
																						.data("autocomplete").term = "";
																				return false;
																			}
																		}
																	}
																})
														.addClass(
																"ui-widget ui-widget-content ui-corner-left");

												input.data("autocomplete")._renderItem = function(
														ul, item) {
													return $("<li></li>")
															.data(
																	"item.autocomplete",
																	item)
															.append(
																	"<a>"
																			+ item.label
																			+ "</a>")
															.appendTo(ul);
												};

												this.button = $(
														"<button type='button'>&nbsp;</button>")
														.attr("tabIndex", -1)
														.attr("title",
																"Show All Items")
														.insertAfter(input)
														.button(
																{
																	icons : {
																		primary : "ui-icon-triangle-1-s"
																	},
																	text : false
																})
														.removeClass(
																"ui-corner-all")
														.addClass(
																"ui-corner-right ui-button-icon")
														.click(
																function() {
																	// close if
																	// already
																	// visible
																	if (input
																			.autocomplete(
																					"widget")
																			.is(
																					":visible")) {
																		input
																				.autocomplete("close");
																		return;
																	}

																	// work
																	// around a
																	// bug
																	// (likely
																	// same
																	// cause as
																	// #5265)
																	$(this)
																			.blur();

																	// pass
																	// empty
																	// string as
																	// value to
																	// search
																	// for,
																	// displaying
																	// all
																	// results
																	input
																			.autocomplete(
																					"search",
																					"");
																	input
																			.focus();
																});
											},

											destroy : function() {
												this.input.remove();
												this.button.remove();
												this.element.show();
												$.Widget.prototype.destroy
														.call(this);
											}
										});
					})(jQuery);

					/* fin combobox */
				});

function clear_form_elements() {
	$('fechaDesde').value = '';
	$('fechaDesdeHidden').value = '';
	$('descripcion').value = '';
	$('fechaHasta').value = '';
	$('fechaHastaHidden').value = '';
	
	ajaxCall({
		container: "legajoUsuario",
		controller: "historial",
		action:'completarUsuario',
		parameters : {
			id: "historial",
			pedidoId : $('pedidoId').value,
			box : "legajoUsuario"
		}
	});
	
	jQuery("#comboboxMacroestado").multiselect("uncheckAll");
	obtenerFases('');
	jQuery("#comboboxRolAplicacion").multiselect("uncheckAll");
	jQuery("#comboboxTipoEvento").multiselect("uncheckAll");
	return false;
}

function obtenerFases(params) {
	
	//NICO
	//Encontre este documento sobre multiselect que parece muy bueno: http://www.erichynds.com/jquery/jquery-ui-multiselect-widget/
	var array_of_checked_values = jQuery("#comboboxMacroestado").multiselect("getChecked").map(function(){
	   return this.value;	
	}).get();
	
	//alert("array_of_checked_values: " + array_of_checked_values);
	//alert(params);
	new Ajax.Updater(comboboxFaseHistorial, '/' + gNameApp + '/historial/obtenerFases',
			{
				asynchronous : true,
				evalScripts : true,
				parameters : 'macroEstadoID=' + array_of_checked_values
			});
	return false;
}

function validarFormulario() {
	
	var fechaDesde = stringToDate($('datepicker-fechaDesde-rpid').value);
	var fechaHasta = stringToDate($('datepicker-fechaHasta-rpid').value);
	var fechaHoy = stringToDate($('fechaHoyHidden').value);
	
	if ( fechaDesde > fechaHoy || fechaHasta > fechaHoy){
		alert("La Fecha Desde y Fecha Hasta no pueden ser superiores a la Fecha Actual");
	}else{
		if (fechaDesde > fechaHasta ) {
			alert("La Fecha Desde no puede ser mayor a la Fecha Hasta");
		} else {
			document.formHistorial.submit();
		}
	}
}
