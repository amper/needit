// Globales
var g_cantidadDecimales = 2;
var g_tabSeleccionado = 0;

jQuery(document).ready(
function($) {
	$(function() {
		// COMBOS
		$("#comboImpactos").combobox();
		$("#comboImpactos").change(function() {
			completaGrupoRSWF(this.value);
		});

		$("#comboSistemasEstPrueba").combobox();
		$("#comboSistemasEstPrueba").change(function() {
		});
		
		$("#comboSistemasDE").combobox();
		$("#comboSistemasDE").change(function() {
		});
		
		$("#comboJustificacionDE").combobox();
		
		$("#numeroRelease").combobox();
		$("#comboPrioridad").combobox();
		
		$("#comboSoftwareFactories").combobox();
		$("#comboPeriodo").combobox();
								
		// COLLAPSABLE
		animatedcollapse.ontoggle=function($, divobj, state){ myOnToggle(divobj, state); };
		animatedcollapse.init();
		
	});

	// fin combobox
});

function completaGrupoRSWF(impacto) {
	updateTemplateToken('replanificacion');
	$('impacto').value = impacto;
	$('refreshView').click();
}

function myOnToggle(divobj, state) {
	if (divobj) {
		var objId = divobj.id;
		objId = objId.replace('content', 'indicator');
		
		if (state == 'none') {
			//$(objId).innerHTML = '[+]';
			$(objId).src='../../images/RightTriangleIcon.gif';
		} else  {
			//$(objId).innerHTML = '[-]';		
			$(objId).src='../../images/DownTriangleIcon.gif';
		}
	}
}

function toggleDivs(id){
	animatedcollapse.toggle(id);
}

function grabarHoras(pedidoId, impacto) {
	//var miArray = $$('.agregaHoras');
	var miArray = $$('.detalleReplanificacion');
	var idBtn = "";
	var idSplit = "";
	var fDesde  = "";
	var fHasta = "";
	var horas = "";
	var display;
	var detalles = "";
	miArray.each (function(el, idx) {
		idElement = el.id;
		idSplit = idElement.split('_');
		if(el && document.getElementById('desde_' + idSplit[1]).value && document.getElementById('hasta_' + idSplit[1]).value && document.getElementById('cargaHoras_' + idSplit[1]).value) {
			detalles += idSplit[1] + ",";
			fDesde += document.getElementById('desde_' + idSplit[1]).value + ",";
			fHasta += document.getElementById('hasta_' + idSplit[1]).value + ",";
			horas += document.getElementById('cargaHoras_' + idSplit[1]).value + ",";
		}
	});
	if (miArray.length && detalles!=''){
		ajaxCall({
			container: 'planificacionYEsfuerzo',
			controller:'replanificacion',
			action:'grabaFechasDetalles',
			parameters : {
				detalles: detalles,
				fechasDesde: fDesde,
				fechasHasta: fHasta,
				horas: horas,
				impacto: impacto,
				pedidoId: pedidoId
			}, onComplete: function (e){
				//grabadoHorasCompletasSuccess(faseId, sistema, idDetalle, idPlanificacion,impacto,pedidoId, aprobacionId);
			}
		});
	}else{
		return false
	}
}

function soloNumeros(evt, conComa) {
	  var theEvent = evt || window.event;
	  var key = theEvent.keyCode || theEvent.which;
	  key = String.fromCharCode( key );
	  
	  var regex 
	  if(conComa){
		  regex = /[0-9]|\,/;
	  }else{
		  regex = /[0-9]/;
	  }
		  
	  if( !regex.test(key) ) {
	    theEvent.returnValue = false;
	    if(theEvent.preventDefault) theEvent.preventDefault();
	  }
}

function soloNumerosSinSimbolos(evt) {
	  var theEvent = evt || window.event;
	  var key = theEvent.keyCode || theEvent.which;
	  key = String.fromCharCode( key );
	  var regex = /[0-9]/;
	  if( !regex.test(key) ) {
	    theEvent.returnValue = false;
	    if(theEvent.preventDefault) theEvent.preventDefault();
	  }
}

function cerrarPopUpOtrosCostos(){
	$('btnCerrarOtrosCostos').click();	
}

function setearDecimales(numero){
	var numeroConDecimales = numero.toFixed(g_cantidadDecimales);
	if (numeroConDecimales)
		return numeroConDecimales.replace('.',',');
	else
		return numero
}

function isInteger(numero){
	return (numero%1===0);
}

function showDatePicker(name, detalleId, esDeSistema) {
	if(name == 'hasta') {
		var fecha = document.getElementById('desde_'+detalleId).value;
		var dt1  = fecha.substring(0,2); 
        var mon1 = fecha.substring(3,5); 
        var yr1  = fecha.substring(6,10);  
        
		var tiempo = Date.parse(mon1 + "/" + dt1 + "/" + yr1);
		jQuery('#'+name+'_'+detalleId).datepicker("destroy");
	
		jQuery('#'+name+'_'+detalleId).datepicker({
		    dateFormat: 'dd/mm/yy',
		    //minDate: new Date(document.getElementById('desde_'+detalleId).value),
		    minDate: new Date(tiempo),
			showAnim: ''
		});
		
	} else {
		var desde;
		if(esDeSistema){
			desde = null;
		}else{
			desde = new Date();
		}
		jQuery('#hasta_'+detalleId).datepicker("destroy");
		
		jQuery('#'+name+'_'+detalleId).datepicker({
		    dateFormat: 'dd/mm/yy',
		    minDate: desde,
		    showAnim: ''
		});
	}
	jQuery('#'+name+'_'+detalleId).datepicker("show");
	
}

function showDatePickerTarea(id) {
	jQuery('#'+id).datepicker();
	jQuery('#'+id).datepicker({
	    dateFormat: 'dd/mm/yy',
	    showAnim: ''
	});
	
}
function openPopUpReenviar(pedidoId, impacto, aprobacionId){
	ajaxCall({
		container: 'dialog-Reenviar',
		controller:'consolidacionImpacto',
		action:'mostrarPopUpReenviar',
		parameters : {
			pedidoId : pedidoId,
			impacto : impacto,
			aprobacionId:aprobacionId
		}, onComplete: function (e){
			jQuery('#dialog-Reenviar').dialog("open");
		}
	});
}

function cerrarPopUpReenviar(){
	jQuery('#dialog-Reenviar').dialog("close");
}

function reenviar(formulario){
	if ($('observaciones').value==''){
		$('errores').show();
	}else{
		cerrarPopUpReenviar();
		formulario.submit();
	}
	return false;
}

function recargaCostoTotal(impacto, pedidoId){
	ajaxCall({
		container: 'totalCambio',
		controller:'consolidacionImpacto',
		action:'recargaCostoTotalCambio',
		parameters : {
			pedidoId : pedidoId,
			impacto : impacto
		}
	});
	
}

function guardarPedido(){
	var formulario = Form.serialize(document.getElementById('formPedido'));
	formulario = quitarParametro(formulario, "_action_navegarFase");

	ajaxCallRequest({
		controller: 'consolidacionImpacto', 
		action: 'grabarFormulario', 
		parameters: formulario
	});	
	
}

function enviarReplanificacion(impacto){
	var campo = $('comentario'+impacto).value;
	if (campo != ''){
		ajaxCall({
			container: 'tablaImpactos',
			controller:'replanificacion',
			action:'enviarReplanificacion',
			parameters : {
				pedidoId : $('pedidoId').value,
				impacto : impacto,
				comentario : campo
			}
		});
	}else{
		alert("Debe ingresar un comentario para poder iniciar la replanificación");
	}
}