function ajaxCall(options, url){
	url =  '/' + gNameApp + '/' + options.controller + '/' + options.action;
	
	var parametros = options.parameters;
	options.parameters[gTokenKey] = $(gTokenKey).value;
	if(Object.isString(parametros)){
		parametros = obtenerParametros(parametros, $(gTokenKey).value);
	}
	//En caso de que venga [Object object] hay 
	//que obtener el string de adentro para agregarle la aprobacion UI
	
	var asynchronous = false;
	if (options.asynchronous){
		asynchronous = options.asynchronous;
	}
	
	//Si se desea enviar un error particular, hay que reescribir el onFailure
	//En el metodo donde se quiere implementar.
	var failure = setFailure(options);
	
	new Ajax.Updater(options.container, url, {
		asynchronous : asynchronous,
		evalScripts : true,
		parameters : parametros,
		onComplete: options.onComplete,
		onFailure: failure,
		onLoading: options.onLoading,
		onSuccess: function(response){
//			options.onSuccess;
			$(gTokenKey).value = response.getHeader(gTokenKey);
			if ($("pedidoVersion") && response.getHeader("pedidoVersion")) {
				$("pedidoVersion").value = response.getHeader("pedidoVersion");
			}
			if ($("pedidoHijoVersion") && response.getHeader("pedidoHijoVersion")) {
				$("pedidoHijoVersion").value = response.getHeader("pedidoHijoVersion");
			}			
		}
	});
}

function ajaxCallRequest(options, url){
	url =  '/' + gNameApp + '/' + options.controller + '/' + options.action;
	
	var parametros = options.parameters;
	options.parameters[gTokenKey] = $(gTokenKey).value;
	if(Object.isString(parametros)){
		parametros = obtenerParametros(parametros, $(gTokenKey).value);
	}
	
	var asynchronous = false;
	if (options.asynchronous){
		asynchronous = options.asynchronous;
	}
	
	var failure = setFailure(options);
	
	new Ajax.Request(url, {
		method: 'get',
		asynchronous : asynchronous,
		parameters : parametros,
		onSuccess: function(response){
			$(gTokenKey).value = response.getHeader(gTokenKey);
			if ($("pedidoVersion") && response.getHeader("pedidoVersion")) {
				$("pedidoVersion").value = response.getHeader("pedidoVersion");
			}
			if ($("pedidoHijoVersion") && response.getHeader("pedidoHijoVersion")) {
				$("pedidoHijoVersion").value = response.getHeader("pedidoHijoVersion");
			}			
		},
		onFailure: failure,
		onComplete: options.onComplete
	});
}

function obtenerParametros(parametros, otraClave){
	var parametrosFinal = "";
	var spliteado = parametros.split("&");
	for (var i = 0; i < spliteado.size(); i++){
		if (spliteado[i].indexOf(gTokenKey, 0)==-1){
			parametrosFinal += spliteado[i];
		}else{
			parametrosFinal += gTokenKey + "=" + otraClave;
		}
		parametrosFinal += "&";
	}
	if (parametrosFinal.indexOf(gTokenKey, 0)==-1){
		parametrosFinal += gTokenKey + "=" + otraClave;
	}else{
		parametrosFinal = parametrosFinal.substring(0, parametrosFinal.length-1);
	}
	if (parametrosFinal.indexOf("aprobacionId", 0)==-1){
		if ($('aprobacionId')){
			parametrosFinal += "&aprobacionId=" + $('aprobacionId').value;
		}
	}
	return parametrosFinal;
}

function setFailure(options){
	var failure;
	if (options.onFailure){
		failure = options.onFailure; 
	}else{
		failure = function (transport){
			var responseJSON;
			try{
				responseJSON = jQuery.parseJSON(transport.responseText);
				errorAjaxRedirect(responseJSON.title, responseJSON.msg, responseJSON.type);
			}catch(e){
				document.body.innerHTML = e;
			}
		};
	}
	
	if (options.onFailure){
		options.onComplete = null;
	}
	return failure;
}
