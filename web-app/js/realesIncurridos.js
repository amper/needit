// Globales
var g_cantidadDecimales = 2;
var g_tabSeleccionado = 0;

jQuery(document)
		.ready(
				function($) {
					$(function() {
						// COMBOS
						$("#impacto").combobox();
						$("#impacto").change(function() {
							seleccionImpacto(this.value);
						});

						$("#grupo").combobox();
						$("#grupo").change(function() {
							seleccionGrupo( document.getElementById('pedidoId').value, document.getElementById('impacto').value, this.value ) ;
						});
						
					});
					
					// combobox con autocomplete
					(function($) {
						$
								.widget(
										"ui.combobox",
										{
											_create : function() {
												var self = this, select = this.element
														.hide(), selected = select
														.children(":selected"), value = selected
														.val() ? selected
														.text() : "";
												var input = this.input = $(
														"<input>")
														.insertAfter(select)
														.attr(
																'id',
																$(select).attr(
																		'id')
																		+ '-input')
														.val(value)
														.autocomplete(
																{
																	delay : 0,
																	minLength : 0,
																	source : function(
																			request,
																			response) {
																		var matcher = new RegExp(
																				$.ui.autocomplete
																						.escapeRegex(request.term),
																				"i");
																		response(select
																				.children(
																						"option")
																				.map(
																						function() {
																							var text = $(
																									this)
																									.text();
																							if (this.value
																									&& (!request.term || matcher
																											.test(text)))
																								return {
																									label : text
																											.replace(
																													new RegExp(
																															"(?![^&;]+;)(?!<[^<>]*)("
																																	+ $.ui.autocomplete
																																			.escapeRegex(request.term)
																																	+ ")(?![^<>]*>)(?![^&;]+;)",
																															"gi"),
																													"<strong>$1</strong>"),
																									value : text,
																									option : this
																								};
																						}));
																	},
																	select : function(
																			event,
																			ui) {
																		ui.item.option.selected = true;
																		self
																				._trigger(
																						"selected",
																						event,
																						{
																							item : ui.item.option
																						});
																		select
																				.trigger("change");
																	},
																	change : function(
																			event,
																			ui) {
																		if (!ui.item) {
																			var matcher = new RegExp(
																					"^"
																							+ $.ui.autocomplete
																									.escapeRegex($(
																											this)
																											.val())
																							+ "$",
																					"i"), valid = false;
																			select
																					.children(
																							"option")
																					.each(
																							function() {
																								if ($(
																										this)
																										.text()
																										.match(
																												matcher)) {
																									this.selected = valid = true;
																									return false;
																								}
																							});
																			if (!valid) {
																				// remove
																				// invalid
																				// value,
																				// as
																				// it
																				// didn't
																				// match
																				// anything
																				$(
																						this)
																						.val(
																								"");
																				select
																						.val("");
																				input
																						.data("autocomplete").term = "";
																				return false;
																			}
																		}
																	},
																	maxItemsToShow : 5
																})
														.addClass(
																"ui-widget ui-widget-content ui-corner-left");

												input.data("autocomplete")._renderItem = function(
														ul, item) {
													return $("<li></li>")
															.data(
																	"item.autocomplete",
																	item)
															.append(
																	"<a>"
																			+ item.label
																			+ "</a>")
															.appendTo(ul);
												};

												this.button = $(
														"<button type='button'>&nbsp;</button>")
														.attr("tabIndex", -1)
														.attr("title",
																"Show All Items")
														.insertAfter(input)
														.button(
																{
																	icons : {
																		primary : "ui-icon-triangle-1-s"
																	},
																	text : false
																})
														.removeClass(
																"ui-corner-all")
														.addClass(
																"ui-corner-right ui-button-icon")
														.click(
																function() {
																	// close if
																	// already
																	// visible
																	if (input
																			.autocomplete(
																					"widget")
																			.is(
																					":visible")) {
																		input
																				.autocomplete("close");
																		return;
																	}

																	// work
																	// around a
																	// bug
																	// (likely
																	// same
																	// cause as
																	// #5265)
																	$(this)
																			.blur();

																	// pass
																	// empty
																	// string as
																	// value to
																	// search
																	// for,
																	// displaying
																	// all
																	// results
																	input
																			.autocomplete(
																					"search",
																					"");
																	input
																			.focus();
																});
											},

											destroy : function() {
												this.input.remove();
												this.button.remove();
												this.element.show();
												$.Widget.prototype.destroy
														.call(this);
											}
										});
					})(jQuery);

					// fin combobox
				});

function seleccionImpacto(idPedidoHijo) {
	$('pedidoHijoId').value = idPedidoHijo;
	$('refreshView').click();
}

function seleccionGrupo(pedidoId, impacto, grupo) {
//	new Ajax.Updater('datosImpactoSeleccionado', '/' + gNameApp + '/realesIncurridos/seleccionGrupo', {
//		asynchronous : true,
//		evalScripts : true,
//		parameters : {
//			pedidoId : pedidoId,
//			impacto : impacto,
//			grupo : grupo
//		}
//	});	
//	
	
	ajaxCall({
		container: "datosImpactoSeleccionado",
		controller: 'realesIncurridos',
		action:'seleccionGrupo',
		parameters : {
			pedidoId : pedidoId,
			impacto : impacto,
			grupo : grupo
		}
	});	

}

function soloNumeros(evt, conComa) {
	  var theEvent = evt || window.event;
	  var key = theEvent.keyCode || theEvent.which;
	  key = String.fromCharCode( key );
	  
	  var regex 
	  if(conComa){
		  regex = /[0-9]|\,/;
	  }else{
		  regex = /[0-9]/;
	  }
		  
	  if( !regex.test(key) ) {
	    theEvent.returnValue = false;
	    if(theEvent.preventDefault) theEvent.preventDefault();
	  }
}

function confirmaHorasIncurridas(detalleId, grupo, impacto, pedidoId){
	var filaAUpdetear = 'inc_'+detalleId
	var horasIncurridas = $('horasIncurridas_'+detalleId).value
	
//	new Ajax.Updater(filaAUpdetear, '/' + gNameApp + '/realesIncurridos/confirmaDetalle', {
//		asynchronous : true,
//		evalScripts : true,
//		parameters : {
//			detalleId : detalleId,
//			horasIncurridas : horasIncurridas,
//			grupo : grupo,
//			impacto: impacto,
//			pedidoId : pedidoId
//		},
//		onComplete : function(e){
//			actualizaGrupos(impacto, pedidoId);
//		}
//	});	
	
	ajaxCall({
		container: filaAUpdetear,
		controller: 'realesIncurridos',
		action:'confirmaDetalle',
		parameters : {
			detalleId : detalleId,
			horasIncurridas : horasIncurridas,
			grupo : grupo,
			impacto: impacto,
			pedidoId : pedidoId
		},
		onComplete : function(e){
			actualizaGrupos(impacto, pedidoId);
		}
	});	
}

function actualizaGrupos(impacto, pedidoId){
	
//	new Ajax.Updater('gruposCargados', '/' + gNameApp + '/realesIncurridos/actualizaGrupos', {
//		asynchronous : true,
//		evalScripts : true,
//		parameters : {
//			impacto: impacto,
//			pedidoId : pedidoId
//		}
//	});	
	
	ajaxCall({
		container: 'gruposCargados',
		controller: 'realesIncurridos',
		action:'actualizaGrupos',
		parameters : {
			impacto: impacto,
			pedidoId : pedidoId
		}
	});	
}


function myOnToggle(divobj, state) {
	//alert("3");
	if (divobj) {
		var objId = divobj.id;
		objId = objId.replace('content', 'indicator');
		
		if (state == 'none') {
			//$(objId).innerHTML = '[+]';
			$(objId).src='../../images/RightTriangleIcon.gif';
		} else  {
			//$(objId).innerHTML = '[-]';		
			$(objId).src='../../images/DownTriangleIcon.gif';
		}
	}
	//alert("4");
}

function toggleDivs(id){
//	alert("1");
	animatedcollapse.toggle(id);
	//alert("2");
}