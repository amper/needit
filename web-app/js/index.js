var idGlobal = '';
var gFecha = '';

function clearCombo(id){
		var a = id + '-input'
		$(a).value = '';
		$(id).value = '';
}

function mostrarBoxOption(id){
	var a = id + "-Option";
	document.getElementById(a).style.display='block';
}
function ocultarBoxOption(id){
	var a = id + "-Option";
	document.getElementById(a).style.display='none';
}

jQuery(document).ready(
		
		function($) {
			$("#registroPedidoForm").validate();
			
			// modal popup ayuda

			$("#dialog-help").dialog({
				autoOpen : false,
				resizable : true,
				height : 160,
				width : 450,
				modal : true,
				buttons : {
					"OK" : function() {
						$(this).dialog("close");
					}
				}
			});
			
			$("#dialog-sistema").dialog({
				autoOpen : false,
				resizable : false,
				height : 130,
				width : 320,
				modal : true,
				buttons : {
					"Finalizado" : function() {
						$(this).dialog("close");
					}
				}
			});
			
			
			//modals popup eventos
			$("#abrirAyuda").click(function() {
				$("#dialog-help").dialog("open");
				return false;
			});
			
			$("#nuevoSistema").click(function() {
				$("#dialog-sistema").dialog("open");
				return false;
			});
			
			//fin de modal popups eventos
			
			$('#ocultarPanel').click(function(event) {
				event.preventDefault();
				$("#contenedorDcho").animate({
					right : -180
				}, 500);
				$("#contenedorIzq").animate({
					width : 858
				}, 500);
				$("#contenedorDchoMin").animate({
					right : 0
				}, 500);
			});

			$('#mostrarPanel').click(function(event) {
				event.preventDefault();
				$("#contenedorDchoMin").animate({
					right : -40
				}, 500);
				$("#contenedorDcho").animate({
					right : 0
				}, 500);
				$("#contenedorIzq").animate({
					width : 678
				}, 500);
			});

			
			/*Botones y datepickers*/
			
			
			$("#buttonSistemas").button();
			$("#nuevoSistema").button({
	            icons: {
	            	primary: "ui-icon-plus"
	            },
	            text:false
	        });
			
			$("input:button").button({
	            icons: {
	            	primary: "ui-icon-close"
	            }
	        });
			$("#Cancelar").button({
	            icons: {
	            	primary: "ui-icon-close"
	            }
	        });
			$("#Enviar").button({
	            icons: {
	            	primary: "ui-icon-circle-arrow-e"
	            }
	        });
			
			$("#datepicker-rpid").datepicker({
				minDate : +1
			});
			$( "#datepicker-rpid" ).datepicker( "option", "dateFormat", "dd/mm/yy");
			
			$("#datepicker-rpid").val(document.getElementById("fechaHidden").value);

			 
			$(function() {
				$( "#combobox" ).combobox();
				$("#comboboxTipoReferencia").combobox();
				$( "#comboboxTipoPedido" ).combobox();
				$( "#comboboxSistemas" ).combobox();
				$( "#comboboxTipoImpactoSistema" ).combobox();
				
				
				
			});
			
			$("#dialog:ui-dialog").dialog("destroy");
			
			
			/*combobox con autocomplete*/
			
			(function( $ ) {
				$.widget( "ui.combobox", {
					_create: function() {
						var self = this,
							select = this.element.hide(),
							selected = select.children( ":selected" ),
							value = selected.val() ? selected.text() : "";
						var input = this.input = $( "<input>" )
							.insertAfter( select )
							.attr( 'id', $(select).attr( 'id' )+'-input' )
							.val( value )
							.autocomplete({
								delay: 0,
								minLength: 0,
								source: function( request, response ) {
									var matcher = new RegExp( $.ui.autocomplete.escapeRegex(request.term), "i" );
									response( select.children( "option" ).map(function() {
										var text = $( this ).text();
										if ( this.value && ( !request.term || matcher.test(text) ) )
											return {
												label: text.replace(
													new RegExp(
														"(?![^&;]+;)(?!<[^<>]*)(" +
														$.ui.autocomplete.escapeRegex(request.term) +
														")(?![^<>]*>)(?![^&;]+;)", "gi"
													), "<strong>$1</strong>" ),
												value: text,
												option: this
											};
									}) );
								},
								select: function( event, ui ) {
									ui.item.option.selected = true;
									self._trigger( "selected", event, {
										item: ui.item.option
									});
								},
								change: function( event, ui ) {
									if ( !ui.item ) {
										var matcher = new RegExp( "^" + $.ui.autocomplete.escapeRegex( $(this).val() ) + "$", "i" ),
											valid = false;
										select.children( "option" ).each(function() {
											if ( $( this ).text().match( matcher ) ) {
												this.selected = valid = true;
												return false;
											}
										});
										if ( !valid ) {
											// remove invalid value, as it didn't match anything
											$( this ).val( "" );
											select.val( "" );
											input.data( "autocomplete" ).term = "";
											return false;
										}
									}
								}
							})
							.addClass( "ui-widget ui-widget-content ui-corner-left" );

						input.data( "autocomplete" )._renderItem = function( ul, item ) {
							return $( "<li></li>" )
								.data( "item.autocomplete", item )
								.append( "<a>" + item.label + "</a>" )
								.appendTo( ul );
						};

						this.button = $( "<button type='button'>&nbsp;</button>" )
							.attr( "tabIndex", -1 )
							.attr( "title", "Show All Items" )
							.insertAfter( input )
							.button({
								icons: {
									primary: "ui-icon-triangle-1-s"
								},
								text: false
							})
							.removeClass( "ui-corner-all" )
							.addClass( "ui-corner-right ui-button-icon" )
							.click(function() {
								// close if already visible
								if ( input.autocomplete( "widget" ).is( ":visible" ) ) {
									input.autocomplete( "close" );
									return;
								}

								// work around a bug (likely same cause as #5265)
								$( this ).blur();

								// pass empty string as value to search for, displaying all results
								input.autocomplete( "search", "" );
								input.focus();
							});
					},

					destroy: function() {
						this.input.remove();
						this.button.remove();
						this.element.show();
						$.Widget.prototype.destroy.call( this );
					}
				});
			})( jQuery );
			
			/*fin combobox*/			
		}
);

function UrlNotValid(id) {
	document.getElementById(id).src = "http://guiainterna/webdynpro/resources/local/HR_GuiaInterna/Components/ar.com.telecom.hr.guiainterna.app.GuiaInterna/nophoto.jpg";
}


//Pantalla Registro Pedido
function completarUserBox(username, box) {
	new Ajax.Updater(box, '/' + gNameApp + '/registroPedido/completarUsuario', {
		asynchronous : true,
		evalScripts : true,
		parameters : {username: username , box: box}
	});
}