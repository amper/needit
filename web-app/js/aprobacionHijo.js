// Globales
var g_cantidadDecimales = 2;
var g_tabSeleccionado = 0;

jQuery(document)
		.ready(
				function($) {
					$(function() {
						// TABS
						$("#tabs").tabs({ selected: g_tabSeleccionado });
	
						// COLLAPSABLE
						animatedcollapse.ontoggle=function($, divobj, state){ myOnToggle(divobj, state); };
						animatedcollapse.init();
						
						// COMBOBOX
						$("#comboImpacto").combobox();

						$("#comboImpacto").change(function() {
							refrescar(this.value);
						});
						
						$("#comboHijosNoConsolidados").combobox();
						
						$("#comboHijosNoConsolidados").change(function() {
							//alert('ingrese al cambio del combo');
							document.getElementById('botoneraPau').style.display = 'none';
							var hijoSeleccionado = this.value;
							new Ajax.Updater('datosHijoSeleccionado', '/' + gNameApp + '/'+ controllerActual + '/seleccionaHijo',
							{
								asynchronous : true,
								evalScripts : true,
								parameters : {
									pedidoId : document.getElementById('pedidoId').value,
									idPedidoHijo : this.value
								},
								onComplete : function(e) {
									//alert('complete');
									document.getElementById('botoneraPau').style.display = 'block';
									/*
									new Ajax.Updater('botonera', '/' + gNameApp + '/'+ controllerActual + '/recargaBotonera', {
										asynchronous : true,
										evalScripts : true,
										parameters : {username: document.getElementById('inputlegajoResponsableConstruccionPadre').value, idPedidoHijo: hijoSeleccionado}
									});					
									
									 */
								}
							});
							
						});	
						
						
						
						
						
					});
					
					// combobox con autocomplete
					(function($) {
						$
								.widget(
										"ui.combobox",
										{
											_create : function() {
												var self = this, select = this.element
														.hide(), selected = select
														.children(":selected"), value = selected
														.val() ? selected
														.text() : "";
												var input = this.input = $(
														"<input>")
														.insertAfter(select)
														.attr(
																'id',
																$(select).attr(
																		'id')
																		+ '-input')
														.val(value)
														.autocomplete(
																{
																	delay : 0,
																	minLength : 0,
																	source : function(
																			request,
																			response) {
																		var matcher = new RegExp(
																				$.ui.autocomplete
																						.escapeRegex(request.term),
																				"i");
																		response(select
																				.children(
																						"option")
																				.map(
																						function() {
																							var text = $(
																									this)
																									.text();
																							if (this.value
																									&& (!request.term || matcher
																											.test(text)))
																								return {
																									label : text
																											.replace(
																													new RegExp(
																															"(?![^&;]+;)(?!<[^<>]*)("
																																	+ $.ui.autocomplete
																																			.escapeRegex(request.term)
																																	+ ")(?![^<>]*>)(?![^&;]+;)",
																															"gi"),
																													"<strong>$1</strong>"),
																									value : text,
																									option : this
																								};
																						}));
																	},
																	select : function(
																			event,
																			ui) {
																		ui.item.option.selected = true;
																		self
																				._trigger(
																						"selected",
																						event,
																						{
																							item : ui.item.option
																						});
																		select
																				.trigger("change");
																	},
																	change : function(
																			event,
																			ui) {
																		if (!ui.item) {
																			var matcher = new RegExp(
																					"^"
																							+ $.ui.autocomplete
																									.escapeRegex($(
																											this)
																											.val())
																							+ "$",
																					"i"), valid = false;
																			select
																					.children(
																							"option")
																					.each(
																							function() {
																								if ($(
																										this)
																										.text()
																										.match(
																												matcher)) {
																									this.selected = valid = true;
																									return false;
																								}
																							});
																			if (!valid) {
																				// remove
																				// invalid
																				// value,
																				// as
																				// it
																				// didn't
																				// match
																				// anything
																				$(
																						this)
																						.val(
																								"");
																				select
																						.val("");
																				input
																						.data("autocomplete").term = "";
																				return false;
																			}
																		}
																	},
																	maxItemsToShow : 5
																})
														.addClass(
																"ui-widget ui-widget-content ui-corner-left");

												input.data("autocomplete")._renderItem = function(
														ul, item) {
													return $("<li></li>")
															.data(
																	"item.autocomplete",
																	item)
															.append(
																	"<a>"
																			+ item.label
																			+ "</a>")
															.appendTo(ul);
												};

												this.button = $(
														"<button type='button'>&nbsp;</button>")
														.attr("tabIndex", -1)
														.attr("title",
																"Show All Items")
														.insertAfter(input)
														.button(
																{
																	icons : {
																		primary : "ui-icon-triangle-1-s"
																	},
																	text : false
																})
														.removeClass(
																"ui-corner-all")
														.addClass(
																"ui-corner-right ui-button-icon")
														.click(
																function() {
																	// close if
																	// already
																	// visible
																	if (input
																			.autocomplete(
																					"widget")
																			.is(
																					":visible")) {
																		input
																				.autocomplete("close");
																		return;
																	}

																	// work
																	// around a
																	// bug
																	// (likely
																	// same
																	// cause as
																	// #5265)
																	$(this)
																			.blur();

																	// pass
																	// empty
																	// string as
																	// value to
																	// search
																	// for,
																	// displaying
																	// all
																	// results
																	input
																			.autocomplete(
																					"search",
																					"");
																	input
																			.focus();
																});
											},

											destroy : function() {
												this.input.remove();
												this.button.remove();
												this.element.show();
												$.Widget.prototype.destroy
														.call(this);
											}
										});
					})(jQuery);

					// fin combobox
				});

function myOnToggle(divobj, state) {
	if (divobj) {
		var objId = divobj.id;
		objId = objId.replace('content', 'indicator');
		
		if (state == 'none') {
			//$(objId).innerHTML = '[+]';
			$(objId).src='../../images/RightTriangleIcon.gif';
		} else  {
			//$(objId).innerHTML = '[-]';		
			$(objId).src='../../images/DownTriangleIcon.gif';
		}
	}
}

function toggleDivs(id){
	animatedcollapse.toggle(id);
} 

function seleccionImpacto(hijo) {
	var opcionSeleccionada = document.getElementById('comboImpacto').value;
	var impactoSeleccionado = document.getElementById('impacto').value;
	
	new Ajax.Updater('datosImpactoSeleccionado', '/' + gNameApp + '/'+ controllerActual + '/seleccionaImpacto', {
		asynchronous : true,
		evalScripts : true,
		parameters : {
			pedidoId : document.getElementById('pedidoId').value,
			comboImpacto : opcionSeleccionada,
			//impacto: hijoSeleccionado
			impacto: impactoSeleccionado
		},
		onComplete : function(e) {
			new Ajax.Updater('botonera', '/' + gNameApp + '/'+ controllerActual + '/recargaBotonera', {
				asynchronous : true,
				evalScripts : true,
				parameters : {impacto: impactoSeleccionado, pedidoId : document.getElementById('pedidoId').value}
			});					
		}
	});	
}

function refrescar(impactoSeleccionado){
	$('comboImpacto').value = impactoSeleccionado;
	if (impactoSeleccionado != 'C') {
		$('impacto').value = impactoSeleccionado;
	}
	$('refreshView').click();
}
