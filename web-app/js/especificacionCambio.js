jQuery(document).ready(
		function($) {
		$(function() {
			$("#dialog-NuevaPrueba").dialog({
				autoOpen : false,
				resizable : false,
				height : "100%",
				width : 350,
				modal : true
			});
			
			$("#dialog-ModificaPrueba").dialog({
				autoOpen : false,
				resizable : false,
				height : "100%",
				width : 350,
				modal : true
			});
			
			$("#dialog-NuevaAreaSoporte").dialog({
				autoOpen : false,
				resizable : false,
				height : "100%",
				width : 400,
				modal : true
			});			
			
			$("#dialog-NuevoSistemaImpactado").dialog({
				autoOpen : false,
				resizable : false,
				height : "100%",
				width : 400,
				modal : true
			});		
			/*Boutons*/
			$("#cierraModificaPrueba").click(function() {
				$("#dialog-ModificaPrueba").dialog("close");
				return false;
			});
			
			$("#cierraSistImpactado").click(function() {
				$("#dialog-NuevoSistemaImpactado").dialog("close");
				return false;
			});

			$("#nuevaPrueba").click(function() {
				//$("#dialog-NuevaPrueba").dialog("open");
				abrirPopUpPruebaOpcional();
				return false;
			});

			$("#modificaPrueba").click(function() {
				$("#dialog-ModificaPrueba").dialog("open");
				return false;
			});

			$("#nuevaArea").click(function() {
				abrirPopUpAreaSoporte();
				return false;
			});
			
//			$("#nuevoSistema").click(function() {
//				$("#dialog-NuevoSistemaImpactado").dialog("open");
//				blanquearPopup();
//				$('#refDiv').hide();
//				
//				return false;
//			});
			
			$("#aceptaModificarPrueba").click(function() {
				aceptaModificacion();
				return false;
			});
			
			$("#btnCerrarPopAreaSoporte").click(function() {
				$("#dialog-NuevaAreaSoporte").dialog("close");
				return false;
			});	

			/*Combobox*/
			$("#comboboxTipoPedido").combobox();
			$("#comboboxTipoGestion").combobox();
			$("#comboPrioridad").combobox();
			
			//busqueda rapida
			
			
			$("#comboboxTipoGestion").change(function() {
				actualizaEstrategia(this.value);
				//completaEstrategiasPrueba(this.value, false);
			});
//
//			
//			$("#comboSistemaImpacto").combobox();
//			$("#comboSistemaImpacto").change(function() {
//				alert("2");
//				blanquearTipoImpacto();
//				$("#refDiv").hide();
//			});
//			
//			$("#comboTipoImpacto").combobox();
//			$("#comboTipoImpacto").change(function() {
//				alert("1");
//				completaGrupoReferente(this.value);
//				$('#refDiv').show();
//			});
			
			$("#comboJustificacion1").combobox();
			$("#comboJustificacion2").combobox();
			$("#comboAreasSoporte").combobox();
			
			$("#realizaPI").change(function() {
				toggleJustificacionPI(this.checked);
			});

			$("#realizaPAU").change(function() {
				toggleJustificacionPAU(this.checked);
			});
			if (document.getElementById('comboboxTipoGestion')){
				actualizaEstrategia(document.getElementById('comboboxTipoGestion').value);
			}
		});
		
		//combobox con autocomplete
		(function( $ ) {
			$.widget( "ui.combobox", {
				_create: function() {
					var self = this,
						select = this.element.hide(),
						selected = select.children( ":selected" ),
						value = selected.val() ? selected.text() : "";
					var input = this.input = $( "<input>" )
						.insertAfter( select )
						.attr( 'id', $(select).attr( 'id' )+'-input' )
						.val( value )
						.autocomplete({
							delay: 0,
							minLength: 0,
							source: function( request, response ) {
								var matcher = new RegExp( $.ui.autocomplete.escapeRegex(request.term), "i" );
								response( select.children( "option" ).map(function() {
									var text = $( this ).text();
									if ( this.value && ( !request.term || matcher.test(text) ) )
										return {
											label: text.replace(
												new RegExp(
													"(?![^&;]+;)(?!<[^<>]*)(" +
													$.ui.autocomplete.escapeRegex(request.term) +
													")(?![^<>]*>)(?![^&;]+;)", "gi"
												), "<strong>$1</strong>" ),
											value: text,
											option: this
										};
								}) );
							},
							select: function( event, ui ) {
								ui.item.option.selected = true;
								self._trigger( "selected", event, {
									item: ui.item.option
								});
								select.trigger("change");   
							},
							change: function( event, ui ) {
								if ( !ui.item ) {
									var matcher = new RegExp( "^" + $.ui.autocomplete.escapeRegex( $(this).val() ) + "$", "i" ),
										valid = false;
									select.children( "option" ).each(function() {
										if ( $( this ).text().match( matcher ) ) {
											this.selected = valid = true;
											return false;
										}
									});
									if ( !valid ) {
										// remove invalid value, as it didn't match anything
										$( this ).val( "" );
										select.val( "" );
										input.data( "autocomplete" ).term = "";
										return false;
									}
								}
							},
							maxItemsToShow:5
						})
						.addClass( "ui-widget ui-widget-content ui-corner-left" );

					input.data( "autocomplete" )._renderItem = function( ul, item ) {
						return $( "<li></li>" )
							.data( "item.autocomplete", item )
							.append( "<a>" + item.label + "</a>" )
							.appendTo( ul );
					};

					this.button = $( "<button type='button'>&nbsp;</button>" )
						.attr( "tabIndex", -1 )
						.attr( "title", "Show All Items" )
						.insertAfter( input )
						.button({
							icons: {
								primary: "ui-icon-triangle-1-s"
							},
							text: false
						})
						.removeClass( "ui-corner-all" )
						.addClass( "ui-corner-right ui-button-icon" )
						.click(function() {
							// close if already visible
							if ( input.autocomplete( "widget" ).is( ":visible" ) ) {
								input.autocomplete( "close" );
								return;
							}

							// work around a bug (likely same cause as #5265)
							$( this ).blur();

							// pass empty string as value to search for, displaying all results
							input.autocomplete( "search", "" );
							input.focus();
						});
				},

				destroy: function() {
					this.input.remove();
					this.button.remove();
					this.element.show();
					$.Widget.prototype.destroy.call( this );
				}
			});
		})( jQuery );
		
		//fin combobox
		}
);

function agregaAreaSoporte(form){
	ajaxCall({
		container: 'dialog-NuevaAreaSoporte',
		controller: 'especificacionCambio',
		action:'agregarNuevaAreaSoporte',
		parameters : Form.serialize(form),
		onComplete: function(e){
			recargaAreasSoporte()
		}
	});
	
}

function agregarSistema(form){
	ajaxCall({
		container: 'dialog-NuevoSistemaImpactado',
		controller: 'especificacionCambio',
		action:'agregarNuevoSistemaImpactado',
		parameters : Form.serialize(form),
		onComplete:function(e){
			recargaSistemasImpactados()
		}
	});
	
}
// TipoGestion = 1 : Estandar
// TipoGestion = 2 : Emergencia

function actualizaEstrategia(tipoGestion){
	ajaxCall({
		container: 'estrategiaPruebas',
		controller:'especificacionCambio',
		action:'actualizaEstrategia',
		parameters : {
			tipoGestion: tipoGestion,
			pedidoId : $( 'pedidoId' ).value
		},
		onComplete: function(e){
			//veremos
		}
	});
}

function completaEstrategiasPrueba(tipoGestion, initial){
	if (!tipoGestion) {
		tipoGestion = '0';
	}

	if ($('EstPruebas_seccion3')){
		if (tipoGestion=='0'){
			$('EstPruebas_seccion3').hide();
			return
		}
	
		if(tipoGestion=='1'){
			$('EstPruebas_seccion3').show();
		}		
	
		if(tipoGestion=='2'){
			$('EstPruebas_seccion3').hide();
		}
	}
	
	if ($('JustificacionPI') && $('PIConsolidadasdiv')){
		if($('realizaPI') && !$('realizaPI').checked){
			$('JustificacionPI').show();
			$('PIConsolidadasdiv').hide();
		}else{
			$('JustificacionPI').hide();
			$('PIConsolidadasdiv').show();
		}
	}
	if ($('JustificacionPAU') && $('PAUConsolidadasdiv')){
		if($('realizaPAU') && !$('realizaPAU').checked){
			$('JustificacionPAU').show();
			$('PAUConsolidadasdiv').hide();
		}else{
			$('JustificacionPAU').hide();
			$('PAUConsolidadasdiv').show();
		}
	}
	
}
function toggleJustificacionPI(valor){
	if(valor){
		$('JustificacionPI').hide();
		$('PIConsolidadasdiv').show();
	}else{
		$('JustificacionPI').show();
		$('PIConsolidadasdiv').hide();
	}
}
function toggleJustificacionPAU(valor){
	if(valor){
		$('JustificacionPAU').hide();
		$('PAUConsolidadasdiv').show();
	}else{
		$('JustificacionPAU').show();
		$('PAUConsolidadasdiv').hide();
	}
}


function completaGrupoReferente(valor){
	var sistema;
	if(!$('sistemaImpactoId').value){
		sistema = null;
	}else{
		sistema = $('sistemaImpactoId').value; 
	}
	
	ajaxCall({
		container: grupoReferenteDiv,
		controller:'especificacionCambio',
		action:'cambiaReferente',
		parameters : {
			sistemaImpacto: sistema, 
			tipoImpacto:valor
		},
		onComplete: function(e){
			ajaxCall({
				container: legajoUsuarioReferente,
				controller:'workflow',
				action:'completarUsuario',
				parameters : {
					id: 'RSI',
					grupoReferente: $('grupoReferenteLDAP').value,
					box: 'legajoUsuarioReferente'
				}
			});
		}
	});
	
}


function deleteUserBox(){
	ajaxCall({
		container: legajoUsuarioReferente,
		controller:'workflow',
		action:'completarUsuario',
		parameters : {
			id: 'RSI', 
			grupoReferente: $('grupoReferenteLDAP').value, 
			box: 'legajoUsuarioReferente'
		}
	});
	
}

function editOtrasPruebasPopUp(tipoPrueba, consolida, posicion){
	// Completo campos para modificar
	$('otraPruebaAModificar').innerHTML = tipoPrueba;
//	$('otraPruebaAModificar').disabled="disabled";
	$('posPruebaModif').value = posicion;
	
	if(consolida){
		$('consolidaAModificar').checked = "checked";		
	}else{
		$('consolidaAModificar').checked = "";
	}
	//disparo popup de otras pruebas
	$('modificaPrueba').click();
}

function aceptaModificacion(consolida){
	var filaAModificar = "fila"+$('posPruebaModif').value;
	
	ajaxCall({
		container: filaAModificar,
		controller:'especificacionCambio',
		action:'modificaFilaOtraPrueba',
		parameters : {
			pos: $('posPruebaModif').value, 
			pedidoId: $('pedidoId').value, 
			consolida: $('consolidaAModificar').checked
		}
	});
	
	$('cierraModificaPrueba').click();
}

function blanquearPopup(){
	//$('comboSistemaImpacto').value = "";
	//$('comboSistemaImpacto-input').value = "";
	$('sistemaImpactoField').value = "";
	blanquearTipoImpacto();
}
function blanquearTipoImpacto(){
	$('comboTipoImpacto').value = "";
	$('comboTipoImpacto-input').value = "";
}

function recargaAreasSoporte(){
	ajaxCall({
		container: areasSoporte,
		controller:'especificacionCambio',
		action:'recargaAreasSoporte',
		parameters : {
			pedidoId :  $( "pedidoId" ).value
		}
	});
}

function recargaSistemasImpactados(){
	ajaxCall({
		container: sistemasImpactados,
		controller:'especificacionCambio',
		action:'recargaSistemasImpactados',
		parameters : {
			pedidoId : $( "pedidoId" ).value
		},
		onComplete : function(event) {
			prepararPopUpSistemas();
			if(!($('errors'))){
				blanquearPopUpSistemas();
			}else{
				jQuery('#refDiv').hide();
				blanquearTipoImpacto();
			}
		}
	});
	
}

function cerrarPopUpAreasSoporte(){
	$('btnCerrarPopAreaSoporte').click();	
}

function cerrarPopUpNuevoSistImpactado(){
	$('cierraSistImpactado').click();	
}

function cerrarPopUpPruebaOpcional(){
	jQuery('#dialog-NuevaPrueba').dialog("close");
}

function showPopUpSistemas(pedidoId){
	ajaxCall({
		container: "dialog-NuevoSistemaImpactado",
		controller:'especificacionCambio',
		action:'popupSistemas',
		parameters : {
			pedidoId : pedidoId
		},onComplete : function(event) {
			jQuery('#dialog-NuevoSistemaImpactado').dialog("open");
			jQuery('#dialog-NuevoSistemaImpactado').dialog("option","height", "100%");
			prepararPopUpSistemas();
			blanquearPopUpSistemas();
		}
	});

}


function prepararPopUpSistemas(){
	jQuery('#comboboxTipoGestion').change(function() {
		completaEstrategiasPrueba(this.value, false);
	});
	
//	jQuery('#comboSistemaImpacto').combobox();
//	jQuery('#comboSistemaImpacto').change(function() {
//		blanquearTipoImpacto();
//		jQuery('#refDiv').hide();
//	});
	
	jQuery('#sistemaImpactoField').busquedaRapida({
		select: function(evento, ui){
			blanquearTipoImpacto();
			jQuery('#refDiv').hide();
		}
	});
	
	jQuery('#comboTipoImpacto').combobox();
	jQuery('#comboTipoImpacto').change(function() {
		completaGrupoReferente(this.value);
		jQuery('#refDiv').show();
	});
	
}

function blanquearPopUpSistemas(){
	blanquearPopup();
	jQuery('#refDiv').hide();
}

function abrirPopUpSistemasImpactados(){
	
}

function abrirPopUpAreaSoporte(){
	ajaxCall({
		container: 'dialog-NuevaAreaSoporte',
		controller: 'especificacionCambio',
		action: 'popupAreaSoporte',
		parameters: {
			pedidoId : $( 'pedidoId' ).value
		},
		onComplete: function(event) {
			jQuery("#dialog-NuevaAreaSoporte").dialog("open");
			jQuery('#dialog-NuevaAreaSoporte').dialog("option","height", "100%");
		}
	});

}

function abrirPopUpPruebaOpcional(){
	ajaxCall({
		container: 'dialog-NuevaPrueba',
		controller: 'especificacionCambio',
		action: 'popUpPruebaOpcional',
		parameters: {
			pedidoId : $( 'pedidoId' ).value
		},
		onComplete: function(event) {
			jQuery("#dialog-NuevaPrueba").dialog("open");
			jQuery('#dialog-NuevaPrueba').dialog("option","height", "100%");
		}
	});
}

function recargaTablaPruebaOpcional(){
	ajaxCall({
		container: 'pruebasAdicionales',
		controller: 'especificacionCambio',
		action: 'actualizarTablaOtraPrueba',
		parameters: {
			pedidoId : $( 'pedidoId' ).value
		}
	});

}