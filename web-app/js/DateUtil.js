function stringToDate(s) {
	return (/^(\d\d?)\D(\d\d?)\D(\d{4})$/).test(s) ? new Date(RegExp.$3, RegExp.$2 - 1, RegExp.$1) : new Date (s)
}

//Debe recibir un DDMMYYYY
function dateToString() {
	return isNaN (this) ? 'NaN' : 
		[this.getDate() > 9 ? this.getDate() : '0' + this.getDate(),
		this.getMonth() > 8 ? this.getMonth() + 1 : '0' + (this.getMonth() + 1), 
		this.getFullYear()]
			.join('/')
}
