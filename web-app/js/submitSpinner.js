function executeSubmitSpinner(controller, action, form, update) {

	//var url = '/needIt/' + controller + '/' + action + '?' + $(form).serialize();
	var url = '/' + gNameApp + '/' + controller + '/' + action + '?' + $(form).serialize();

	new Ajax.Request(url, {

		onLoading: function() {
			$(update).update();
			$('imgWait').show();
		},

		onFailure: function() {
			$('imgWait').hide();
			$(update).update('Se produjo un error al realizar la consulta');
		},

		onSuccess: function(response) {
			$(update).update(response.responseText);
		},

		onComplete: function(response) {
			$('imgWait').hide();
		}

	});
	
}

function showSpinner(spinner, visible) {
	if (visible){
		$(spinner).style.top = jQuery(window).scrollTop() + jQuery(window).height()/2;
	}
    $(spinner).style.display = visible ? "inline" : "none";
}