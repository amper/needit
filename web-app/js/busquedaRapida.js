(function( $ ){

  $.fn.busquedaRapida = function(options) {
  
	return this.each(function() {
	  
	  var t;
	  var $this = $(this);
	  var name = $this.data("name");
	  var nameField = name + "Field";
	  var mapName = "mapa_" + name;
	  var pedidoId = $this.data("pedido-id");
	  var methodName = $this.data("method-name");
	  var thisUrl = ($this.data("url")) ? $this.data("url") : "/registroPedido/busquedaAutocomplete";
	  var url = "/" + gNameApp + thisUrl;
	  
	  //merge de las opciones para tener valores default
	  var settings = $.extend( {
	      'select': null
	    }, options);
	  
	  var valorField = "";
	  var valorId = "";
	  if ($this.val()){
		valorField = $this.val();
		valorId = $this.data("valor-id");//attrs.value?.id;
	  } 

	  if( !$this.attr("id") ) $this.attr("id", nameField);
	  
	    $this.autocomplete({
			source: new Array(),
			select: onAutoCompleteSelected,
			focus: function() { return false; }
		});
			
		function serviceCall() {
			var fullUrl = '';
			var word = $("#"+nameField).val();
		
			if (pedidoId){
				fullUrl = url+'?pedidoId='+pedidoId+'&word='+word+'&methodName='+methodName;
			}else{
				fullUrl = url+'?word='+word+'&methodName='+methodName;
			}
			
			$.getJSON(fullUrl, function(data) {
				var result = new Array();
				$.each(data, function(index, item) {
					result.push({label: item.descripcion, value: item.id});
				});
				$("#"+nameField).autocomplete({
					source: result,
					select: onAutoCompleteSelected,
					focus: function() { return false; }
				});

				$("#"+nameField).autocomplete("search");
			});
		};
			
		function onAutoCompleteSelected(event, ui) {
			$("#"+nameField).val(ui.item.label);
			$("#"+name+"Id").val(ui.item.value);
			$("#"+mapName).val('[id:' + ui.item.value + ']');
			
			if(settings.select){
				settings.select(event, ui);
			}
			
			return false;
		};

		$this.keyup(function(event) {
			var isAlphaNumeric = String.fromCharCode(event.keyCode).match(/[A-Za-z0-9]+/g);
			if(isAlphaNumeric || event.keyCode == 8 || event.keyCode == 32) {
				clearTimeout(t);
				t = setTimeout( serviceCall, 200);	
			}
		});
		
		var inputHiddenName = '<input type="hidden" id="'+name+'Id" name="'+name+'.id" value="'+valorId+'" />';	
		var inputHiddenMap = '<input type="hidden" id="'+mapName+'" name="'+mapName+'" value="" />';
		
		$this.parent().append(inputHiddenName).append(inputHiddenMap);

	});
  };
  
})( jQuery );
