 jQuery(document).ready(
	function($) {
		animatedcollapse.ontoggle=function($, divobj, state){ myOnToggle(divobj, state); };
		animatedcollapse.init();
	}
);

function myOnToggle(divobj, state) {

	if (divobj) {
		var objId = divobj.id;
		objId = objId.replace('content', 'indicator');

		
		if ($(objId)){
			if (state == 'none') {
				$(objId).removeClassName('indicatorValoracion-open');
				$(objId).addClassName('indicatorValoracion-close');
			} else  {
				$(objId).removeClassName('indicatorValoracion-close');
				$(objId).addClassName('indicatorValoracion-open');
			}		
		}
	}

}

function toggleDivs(id){
	animatedcollapse.toggle(id);
}

function expandItem(id){
	toggleDivs(id);
	return false;
}
