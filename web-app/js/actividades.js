// Globales
var g_cantidadDecimales = 2;
var g_tabSeleccionado = 0;

jQuery(document)
		.ready(
				function($) {
					$(function() {
						// DIALOG
						
						$("#dialog-popupAnexoPorTipoActividad").dialog({
							autoOpen : false,
							resizable : false,
							height : 530,
							width : 500,
							modal : true
						});
						
						$("#dialog-EditarActividad").dialog({
							autoOpen : false,
							resizable : false,
							height : "auto",
							width : 720,
							modal : true,
							open : function(event, ui) {
								jQuery('#dialog-EditarActividad').dialog("option","height", jQuery('#dialog-EditarActividad').height());
							}
						});						

						$("#dialog-FinalizarActividad").dialog({
							autoOpen : false,
							resizable : false,
							height : 270,
							width :500,
							modal : true
						});						

						// COMBOS
						$("#impacto").combobox();
						$("#impacto").change(function() {
							//var hijoSeleccionado = this.value;
							//seleccionaHijo(hijoSeleccionado);
							refrescar(this.value);
						});						
						
						$("#comboTipoActividad").combobox();
						$("#comboGrupoAsignatario").combobox();
						$("#comboCodigoCierre").combobox();
						
						
						// BOTONES
						$("#agregaActividadPop").click(function() {
							$("#dialog-EditarActividad").dialog("open");
							return false;
						});
						
						//DATEPICKERS

						//$("#fechaFinSugeridaDesc").datepicker();

						$("#fechaCierre").datepicker({
							minDate : +1
						});
						$( "#fechaCierre" ).datepicker( "option", "dateFormat", "dd/mm/yy");
						
						//TABS
						$("#tabs").tabs();
						
					});
					
					// combobox con autocomplete
					(function($) {
						$
								.widget(
										"ui.combobox",
										{
											_create : function() {
												var self = this, select = this.element
														.hide(), selected = select
														.children(":selected"), value = selected
														.val() ? selected
														.text() : "";
												var input = this.input = $(
														"<input>")
														.insertAfter(select)
														.attr(
																'id',
																$(select).attr(
																		'id')
																		+ '-input')
														.val(value)
														.autocomplete(
																{
																	delay : 0,
																	minLength : 0,
																	source : function(
																			request,
																			response) {
																		var matcher = new RegExp(
																				$.ui.autocomplete
																						.escapeRegex(request.term),
																				"i");
																		response(select
																				.children(
																						"option")
																				.map(
																						function() {
																							var text = $(
																									this)
																									.text();
																							if (this.value
																									&& (!request.term || matcher
																											.test(text)))
																								return {
																									label : text
																											.replace(
																													new RegExp(
																															"(?![^&;]+;)(?!<[^<>]*)("
																																	+ $.ui.autocomplete
																																			.escapeRegex(request.term)
																																	+ ")(?![^<>]*>)(?![^&;]+;)",
																															"gi"),
																													"<strong>$1</strong>"),
																									value : text,
																									option : this
																								};
																						}));
																	},
																	select : function(
																			event,
																			ui) {
																		ui.item.option.selected = true;
																		self
																				._trigger(
																						"selected",
																						event,
																						{
																							item : ui.item.option
																						});
																		select
																				.trigger("change");
																	},
																	change : function(
																			event,
																			ui) {
																		if (!ui.item) {
																			var matcher = new RegExp(
																					"^"
																							+ $.ui.autocomplete
																									.escapeRegex($(
																											this)
																											.val())
																							+ "$",
																					"i"), valid = false;
																			select
																					.children(
																							"option")
																					.each(
																							function() {
																								if ($(
																										this)
																										.text()
																										.match(
																												matcher)) {
																									this.selected = valid = true;
																									return false;
																								}
																							});
																			if (!valid) {
																				// remove
																				// invalid
																				// value,
																				// as
																				// it
																				// didn't
																				// match
																				// anything
																				$(
																						this)
																						.val(
																								"");
																				select
																						.val("");
																				input
																						.data("autocomplete").term = "";
																				return false;
																			}
																		}
																	},
																	maxItemsToShow : 5
																})
														.addClass(
																"ui-widget ui-widget-content ui-corner-left");

												input.data("autocomplete")._renderItem = function(
														ul, item) {
													return $("<li></li>")
															.data(
																	"item.autocomplete",
																	item)
															.append(
																	"<a>"
																			+ item.label
																			+ "</a>")
															.appendTo(ul);
												};

												this.button = $(
														"<button type='button'>&nbsp;</button>")
														.attr("tabIndex", -1)
														.attr("title",
																"Show All Items")
														.insertAfter(input)
														.button(
																{
																	icons : {
																		primary : "ui-icon-triangle-1-s"
																	},
																	text : false
																})
														.removeClass(
																"ui-corner-all")
														.addClass(
																"ui-corner-right ui-button-icon")
														.click(
																function() {
																	// close if
																	// already
																	// visible
																	if (input
																			.autocomplete(
																					"widget")
																			.is(
																					":visible")) {
																		input
																				.autocomplete("close");
																		return;
																	}

																	// work
																	// around a
																	// bug
																	// (likely
																	// same
																	// cause as
																	// #5265)
																	$(this)
																			.blur();

																	// pass
																	// empty
																	// string as
																	// value to
																	// search
																	// for,
																	// displaying
																	// all
																	// results
																	input
																			.autocomplete(
																					"search",
																					"");
																	input
																			.focus();
																});
											},

											destroy : function() {
												this.input.remove();
												this.button.remove();
												this.element.show();
												$.Widget.prototype.destroy
														.call(this);
											}
										});
					})(jQuery);
					
					// fin combobox
				});

function seleccionaHijo(hijoSeleccionado, botonera){
	ajaxCall({
		container: 'datosHijoSeleccionado',
		controller: controllerActual,
		action:'seleccionaHijo',
		parameters : {
			pedidoId : document.getElementById('pedidoId').value,
			//idPedidoHijo : hijoSeleccionado,
			impacto : hijoSeleccionado,
			faseATrabajar : document.getElementById('faseATrabajar').value 
		},
		onComplete : function(e){
			ajaxCall({
				container: 'botonera',
				controller: controllerActual,
				action:'recargaBotonera',
				parameters : {
					username: document.getElementById('inputlegajoResponsableConstruccionPadre').value,
					//idPedidoHijo: hijoSeleccionado,
					impacto: hijoSeleccionado,
					faseATrabajar: document.getElementById('faseATrabajar').value
				}
			});
		}
	});
}

function actualizarBotonera(sHTML) {
	var obj = document.getElementById('message');
	//alert(sHTML);
	if(obj){
		obj.innerHTML = sHTML;
	}	
}

function cerrarPopUpEditarActividad(){
	jQuery('#dialog-EditarActividad').dialog("close");
	jQuery('#dialog-EditarActividad').dialog("option","height", jQuery('#dialog-EditarActividad').height());
}

function cerrarPopUpFinalizaActividad(){
		jQuery('#dialog-FinalizarActividad').dialog("close");
}

function showPopUpActividad(idPedidoHijo, idActividad, edita, show){
	ajaxCall({
//		asynchronous: true,
		container: "dialog-EditarActividad",
		controller: controllerActual,
		action:'editarActividad',
		parameters : {
			//idPedidoHijo : idPedidoHijo,
			impacto : idPedidoHijo,
			idActividad : idActividad,
			edita : edita,
			show: show
		}, onLoading: function(e){
            showSpinner('spinner', true);
        },onComplete : function(event) {
			jQuery('#dialog-EditarActividad').dialog("open");
			jQuery('#dialog-EditarActividad').dialog("option","height", jQuery('#dialog-EditarActividad').height());
			initPopUpEditarActividad();
			initPopUpEditarActividadForm();
			recargaTablaActividades(idPedidoHijo);
			showSpinner('spinner', false);
		}
	});
}

/***************************************/
function showPopUpActividadAnexos(pedidoId, idPedidoHijo, idActividad){
	ajaxCall({
		container: "dialog-popupAnexoPorTipoActividad",
		controller: controllerActual,
		action:'editarActividadAnexo',
		parameters : {
			pedidoId : pedidoId,
			//idPedidoHijo : idPedidoHijo,
			impacto : idPedidoHijo,
			idActividad : idActividad
		},
		onComplete : function(event) {
			jQuery('#comboboxTipoAnexo').autocomplete();
			jQuery('#dialog-popupAnexoPorTipoActividad').dialog("open");
			jQuery('#dialog-popupAnexoPorTipoActividad').dialog("option", "height", 530);
			jQuery('#comboboxTipoAnexo').combobox();
		}
	});
}

function mostrarComboAnexos(){
	//alert($('comboboxTipoAnexoActividad'));
	//alert(jQuery('comboboxTipoAnexoActividad'));
	
	//alert(jQuery('#comboboxTipoAnexoActividad1'));
//	jQuery('#comboboxTipoAnexo').autocomplete();	
	//alert(jQuery('#comboboxTipoAnexoActividad1'));

}

function cerrarPopUpAnexoPorTipoActividad(){
	jQuery('#dialog-popupAnexoPorTipoActividad').dialog("close");
}

function validarAnexo(extensionesHabilitadas){
	var arch = document.getElementById('anexoArchivo').value;
	var valueCombo = document.getElementById('comboboxTipoAnexo').value;
	var esValido = esArchivoValido(arch, extensionesHabilitadas);
	
	if (valueCombo != 'null' && arch != '' && esValido=='') {
		document.formAnexo.submit();
	}else{
		var strError = esValido;
		if (arch==''){
			strError += "Debe ingresar un archivo.\n";
		}
		if(valueCombo=='null'){
			strError += "Debe ingresar un tipo de anexo.\n";
		}
		alert (strError);
	}
}
function esArchivoValido(archivo, extensionesHabilitadas){
	var name = archivo.split("").reverse().join("");
	var punto = name.indexOf('.');
	if (punto==-1){
		return "Archivo no valido.\n";
	}
	
	var ext = archivo;
	ext = ext.substring(ext.length-punto,ext.length);
	ext = ext.toLowerCase();
	if(jQuery.inArray(ext, extensionesHabilitadas) != -1){
		return ""; 
	}else{
		return "Extensi\u00F3n no v\u00E1lida. Solo son posibles: " + extensionesHabilitadas + ".\n";
	}
}
/**************************************/

function initPopUpEditarActividad(){
	jQuery('#comboTipoActividad').combobox();
	jQuery('#comboTipoActividad').change(function() {
		var idActividad = document.getElementById('idActividad').value;
		//var idPedidoHijo = document.getElementById('idPedidoHijo').value;
		var impacto = document.getElementById('impacto').value;
		var idTipoActividad = this.value;
		ajaxCall({
			container: "formularioSolapaDescripcion",
			controller: controllerActual,
			action:'cambiaTipoActividad',
			parameters : {
				//idPedidoHijo : idPedidoHijo,
				impacto:impacto,
				idActividad : idActividad,
				idTipoActividad : idTipoActividad
			},
			onComplete : function(event) {
				initPopUpEditarActividadForm();
				jQuery('#dialog-EditarActividad').dialog("open");
				jQuery('#dialog-EditarActividad').dialog("option","height", jQuery('#dialog-EditarActividad').height());
				showSpinner('spinner', false);
			}
		});
	});	
}

function initPopUpEditarActividadForm(){
	jQuery('#comboGrupoAsignatario').combobox();
	jQuery('#comboGrupoAsignatario').change(function() {
		var idActividad = document.getElementById('idActividad').value;
		var grupo = this.value;
		
		//if (!document.getElementById('idActividad').value){
		//}
		ajaxCall({
			container: "responsableActividad",
			controller: controllerActual,
			action:'cambiaGrupoResponsable',
			parameters : {
				idActividad : idActividad,
				grupoResponsable : this.value,
				pedidoId : document.getElementById('pedidoId').value,
				impacto: document.getElementById('impacto').value,
				idTipoActividad: document.getElementById('comboTipoActividad').value
			},
			onComplete : function(event) {
				initFormularioCamposActividades(this.value, document.getElementById('comboTipoActividad').value);
				jQuery('#dialog-EditarActividad').dialog("open");
				jQuery('#dialog-EditarActividad').dialog("option","height", jQuery('#dialog-EditarActividad').height());
				recargaTablaActividades($('impacto').value);
			}
		});			
	});
	
	jQuery('#comboVersionador').combobox();
	jQuery('#comboRelease').combobox();
	jQuery('#datepicker-fechaSugerida').datepicker();
	jQuery('#datepicker-fechaSugerida').datepicker( "option", "dateFormat", "dd/mm/yy");
	
}

function initFormularioCamposActividades(grupoResponsable, tipoActividad){
	updateTemplateToken('construccionPadre');
	ajaxCall({
		container: 'formularioCamposActividades', 
		controller: 'construccionPadre', 
		action: 'cambiaFormularioCamposActividades',
		//asynchronous: true,
		parameters : {
			idActividad : document.getElementById('idActividad').value,
			grupoResponsable : grupoResponsable,
			pedidoId : document.getElementById('pedidoId').value,
			impacto: document.getElementById('impacto').value,
			idTipoActividad: tipoActividad,
			responsableActividadTemporal: document.getElementById('responsableActividadTemporal').value,
			show: document.getElementById('show').value
		}, onComplete: function(response){
			jQuery('#dialog-EditarActividad').dialog("open");
			jQuery('#dialog-EditarActividad').dialog("option","height", jQuery('#dialog-EditarActividad').height());
		}
	});
	
}

function botonActividad(formularioSerializado,idPedidoHijo, actividadId, botonPresionado){
	
	ajaxCall({
		container: 'dialog-EditarActividad', 
		controller: 'construccionPadre', 
		action: 'grabaActividad', 
		parameters: formularioSerializado,
		onLoading: function(e){
			showSpinner('spinner', true);
		}, onComplete: function(response){
			initPopUpEditarActividad();
			initPopUpEditarActividadForm();
			ajaxCall({
				container: "actividades",
				controller: controllerActual,
				action:'recargaTablaActividades',
				parameters : {
					impacto : idPedidoHijo,
					faseATrabajar : document.getElementById('faseATrabajar').value
				}, onComplete: function (e){
					showPopUpFinalizarActividad(idPedidoHijo, actividadId, true, botonPresionado);	
					showSpinner('spinner', false);
				}
			});
		}
	});
	
}

function botonNuevoAnexo(formularioSerializado, impacto){
	
	ajaxCallRequest({
		//container: 'dialog-EditarActividad', 
		controller: 'construccionPadre', 
		action: 'grabaActividad', 
		parameters: formularioSerializado,
		onComplete: function(response){
			initPopUpEditarActividad();
			initPopUpEditarActividadForm();
			recargaTablaActividades(impacto);
			if (document.getElementById('idActividad').value == ""){
				obtenerIdActividad(response.responseText);
				recargaBotonera();
			}	
			showPopUpActividadAnexos(document.getElementById('pedidoId').value,impacto, document.getElementById('idActividad').value);
		}
	});
}

//Deprecated para actividades - NED
function grabaActividad(idTipoActividad){
	var formulario = Form.serialize(document.getElementById("formActividad"));
	formulario += "&legajoUsuarioResponsable=" + document.getElementById('responsableActividadTemporal').value;
	ajaxCall({
		container: 'formularioCamposActividades',
		controller: 'construccionPadre', 
		action: 'grabaActividad', 
		parameters: formulario,
		onComplete: function(response){
			if (document.getElementById('idActividad').value == ""){
				obtenerIdActividad(response.responseText);
			}	
			if ($('grupoResponsable').value && idTipoActividad){
				initFormularioCamposActividades($('grupoResponsable').value, idTipoActividad);
			}
			recargaTablaActividades($('impacto').value);
		}
	});
}

function initFormularioCamposActividadesSinGrabar(idTipoActividad){
	initFormularioCamposActividades($('grupoResponsable').value,idTipoActividad);
}


//Deprecated para actividades - NED
function obtenerIdActividad(page){
	if ($('actividadId').value){
		$('idActividad').value = $('actividadId').value;
		return;
	}
	
	var pagina = page; //transport.responseText;
	var idActividad = "";
	var pattern = "name=\"actividadId\" value=\"";
	if (pagina.indexOf(pattern, 0) == -1){
		pattern = "name=\"idActividad\" value=\"";
	}
	if (pagina.indexOf(pattern, 0) != -1){
		var indexInicial = pagina.indexOf(pattern ,0) ;
		if (indexInicial != -1){
			indexInicial = indexInicial + pattern.length
			var indexFinal = pagina.indexOf('"' ,indexInicial);
			idActividad = pagina.substring(indexInicial, indexFinal);
			if (idActividad){
				$('idActividad').value = idActividad;
				$('actividadId').value = idActividad;
			}
		}
	}
}

function botonFinalizarActividad(formulario,idPedidoHijo, actividadId,estaFinalizada){
	ajaxCall({
		container: 'dialog-FinalizarActividad',
		controller: 'construccionPadre',
		action: 'finalizarActividad',
		parameters: formulario,
		onComplete: function(response){ 
			showPopUpActividad(idPedidoHijo,actividadId,estaFinalizada, false);
		}
	});
}

function showPopUpFinalizarActividad(idPedidoHijo, idActividad, recargaEditarPop, botonOrigen){
	ajaxCall({
		container: "dialog-FinalizarActividad",
		controller: controllerActual,
		action:'popFinalizaActividad',
		parameters : {
			impacto : idPedidoHijo,
			idActividad : idActividad,
			recargaEditarPop : recargaEditarPop,
			botonOrigen : botonOrigen
		},
		onComplete : function(event) {
			jQuery('#dialog-FinalizarActividad').dialog("open");
			jQuery('#dialog-FinalizarActividad').dialog("option","height", jQuery('#dialog-FinalizarActividad').dialog().height());
			//alert("2");
			recargaBotonera();
		}
	});
}

function recargaBotonera(){
	ajaxCall({
		container: 'botonera',
		controller: controllerActual,
		action:'recargaBotonera',
		parameters : {
			username: document.getElementById('inputlegajoResponsableConstruccionPadre').value,
			//idPedidoHijo: document.getElementById('idPedidoHijo').value,
			impacto: document.getElementById('impacto').value,
			faseATrabajar: document.getElementById('faseATrabajar').value 
		}
	});

}

function recargaTablaActividades(idPedidoHijo){
	ajaxCall({
		container: "actividades",
		controller: controllerActual,
		action:'recargaTablaActividades',
		parameters : {
			//idPedidoHijo : idPedidoHijo,
			impacto : idPedidoHijo,
			faseATrabajar : document.getElementById('faseATrabajar').value
		}, onComplete: function (e){
			recargaBotonera();
		}
	});
	
}

function recargaPopUpEditarActividad(idPedidoHijo, idActividad){
	ajaxCall({
		container: "dialog-EditarActividad",
		controller: controllerActual,
		action:'editarActividad',
		parameters : {
			//idPedidoHijo : idPedidoHijo,
			impacto : idPedidoHijo,
			idActividad : idActividad
		},
		onComplete : function(event) {
			initPopUpEditarActividad();
			initPopUpEditarActividadForm();
			jQuery('#dialog-FinalizarActividad').dialog("open");
			jQuery('#dialog-FinalizarActividad').dialog("option","height", jQuery('#dialog-FinalizarActividad').dialog().height());

		}
	});
	
}
	
	
function refrescar(hijoSeleccionado){
	$('impacto').value = hijoSeleccionado;
	$('refreshView').click();
}

function mostrarDivDisenioInterno(value){
	var comboTipoActividad = document.getElementById('comboTipoActividad');
	if ((comboTipoActividad && comboTipoActividad.value!='null') || value){
		if ((comboTipoActividad && comboTipoActividad.options[comboTipoActividad.selectedIndex].text == 'Documentar Diseño Interno') || value == 'Documentar Diseño Interno'){
			jQuery('#divDisenioInterno').show();
		}
	}
}                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                