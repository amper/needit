

function UrlNotValid(id) {
	document.getElementById(id).src = "/" + gNameApp + "/images/sinfoto.png";
}

//Pantalla Registro Pedido
function completarUserBox(username, box, boxParalelo, pedidoId, grupoResponsable, actividadId) {
	if(boxParalelo){
		ajaxCall({
			container: box,
			controller:'registroPedido',
			action:'completarUsuario',
			parameters:{
				username: username,
				box: box,
				id:'GU'
			},
			onComplete : function(e){
				ajaxCall({
					container: boxParalelo,
					controller:'registroPedido',
					action:'completarUsuario',
					parameters:{
						username:'', 
						box: boxParalelo, 
						id:'IU', 
						gerente:username
					}
				});
				return false;
			}
		});
		
	} else{
		var id = null;
		if (box == 'legajoUsuarioResponsable'){
			ajaxCall({
				container: box,
				controller:'consolidacionImpacto',
				action:'completarUsuario',
				parameters:{
					username: username , box: box, pedidoId: $("pedidoId").value, impacto: $("impacto").value
				},
				onComplete : function(e){
					ajaxCall({
						container: botonera,
						controller:'consolidacionImpacto',
						action:'recargaBotonera',
						parameters:{
							username: username, pedidoId: $("pedidoId").value, impacto: $("impacto").value
						}
					});
					return false;
				}				
			});
			
			return;
		} 
		if (box == 'legajoResponsableConstruccionPadre'){
			ajaxCall({
				container: box,
				controller:'registroPedido',
				action:'completarUsuario',
				parameters:{
					username: username , box: box, impacto: $("impacto").value, grupoResponsable: grupoResponsable
				},
				onComplete : function(e){
					ajaxCall({
						container: botonera,
						controller:'construccionPadre',
						action:'recargaBotonera',
						parameters:{
							username: username, pedidoId: pedidoId, impacto: $("impacto").value,
							faseATrabajar: document.getElementById('faseATrabajar').value
						}
					});
					return false;
				}
			});
			
			return;
		}
		if (box == 'legajoUsuarioResponsablePop'){
			var origenActividad;
		    if (document.getElementById('comboTipoActividad')) {
		    	origenActividad = document.getElementById('comboTipoActividad').value;
			} else {
				origenActividad = document.getElementById('tipoActividad').value;
			}
			ajaxCall({
				container: box,
				controller:'registroPedido',
				action:'completarUsuario',
				parameters:{
					username: username , box: box, actividadId: actividadId, origenActividad: origenActividad
				}
			});
			
			return;
		}
		if (box == 'legajoUsuarioAprobadorEconomico') {
			ajaxCall({
				container: box,
				controller:'registroPedido',
				action:'completarUsuario',
				parameters:{
					username: username , box: box, pedidoId: $("pedidoId").value, id: 'aprobImp'
				}, onComplete : function(e){
					ajaxCall({
						container: 'botonera',
						controller:'validacionImpacto',
						action:'recargaBotonera',
						parameters:{
							username: document.getElementById('inputlegajoUsuarioAprobadorEconomico').value,
							pedidoId: $("pedidoId").value
						}
					});
					return false;
				}
			});

			return;
		}
		ajaxCall({
			container: box,
			controller:'registroPedido',
			action:'completarUsuario',
			parameters:{
				username: username , box: box, pedidoId: pedidoId, id: id
			}
		});
		
		}
	}


function completarUserBoxHabilitador(username, box, userLogged) {
	ajaxCall({
		container: box,
		controller:'registroPedido',
		action:'completarUsuario',
		parameters : {username: username , box: box},
		onComplete : function(e){
			if(username != userLogged) {
				initializeForm(username, userLogged, ['botoneraAll', 'botoneraReducida'], ['divPrioridad', 'divCoordinador', 'divJustificacion', 'botonera']);
			} else {
				initializeForm(username, userLogged, ['divPrioridad', 'divCoordinador', 'divJustificacion', 'botoneraAll', 'botonera'], ['botoneraReducida']);
			}
		}
	});
}

function mensajeGerenteIT(muestra){
	if($("mensajeGerenteIT")){
		if(!muestra){
			$("mensajeGerenteIT").show();
		}else{
			$("mensajeGerenteIT").hide();
		}
	}
}

function recargaBotonera(legajoNuevo, logueado, impacto, pedidoId){
	ajaxCall({
		container: 'botonera',
		controller:'consolidacionImpacto',
		action:'recargaBotonera',
		parameters : {
			legajoNuevo: legajoNuevo,
			logueado: logueado,
			impacto: impacto,
			pedidoId:pedidoId
		}
	});
}

function recargaBotoneraEvaluacionCambio(legajoNuevo, logueado, pedidoId){
	ajaxCall({
		container: 'botoneraAll',
		controller: 'evaluacionPedido',
		action: 'recargaBotonera',
		parameters : {
			legajoNuevo: legajoNuevo,
			logueado: logueado,
			pedidoId: $('pedidoId').value
		}
	});
	
}

function recargaBotoneraActividades(hijoSeleccionado){
	ajaxCall({
		container: 'botonera',
		controller: controllerActual,
		action:'recargaBotonera',
		parameters : {
			username: document.getElementById('inputlegajoResponsableConstruccionPadre').value,
			impacto: hijoSeleccionado,
			faseATrabajar: document.getElementById('faseATrabajar').value
		}
	});

}

function permisosResponsableConstruccionPadre(legajoNuevo, idPedidoHijo){
	ajaxCall({
		container: 'datosHijoSeleccionado',
		controller: controllerActual,
		action: 'cambiaResponsable',
		parameters : {
			legajoNuevo: legajoNuevo, 
			impacto: $("impacto").value,
			faseATrabajar: document.getElementById('faseATrabajar').value 
		}
	});

}

function grabarActividad() {
	//$(name='buttonGrabaActividad').click();
}

function reasignaAprobador(idImpacto, sUrl, legajoAnterior, legajoNuevo) {

	new Ajax.Request(sUrl, {
		parameters : {impacto:idImpacto, legajoAnterior: legajoAnterior, legajoNuevo:legajoNuevo},
		onSuccess: function(transport) {
			//$(id).innerHTML = transport.responseText;
		},
		onComplete : function(e) {
			new Ajax.Updater('botonera', '/' + gNameApp + '/aprobacionImplantacionHijo/recargaBotonera', {
				asynchronous : true,
				evalScripts : true,
				parameters : {impacto:idImpacto, pedidoId : document.getElementById('pedidoId').value, legajoNuevo: legajoNuevo}
			});					
		}
	});
}