// Globales
var g_cantidadDecimales = 2;
var g_tabSeleccionado = 0;

jQuery(document)
		.ready(
				function($) {
					$(function() {
						// DIALOG
						$("#dialog-CargaHoras").dialog({
							autoOpen : false,
							resizable : false,
							height : "100%",
							width : 820,
							modal : true
						});
						
						
						$("#dialog-OtrosCostos").dialog({
							autoOpen : false,
							resizable : true,
							height : "100%",
							width : "400px",
							modal : true
						});
						
						$("#dialog-CargaTareas").dialog({
							autoOpen : false,
							resizable : true,
							height : "100%",
							width : "650px",
							modal : true
						});
						
						$("#dialog-NuevaPrueba").dialog({
							autoOpen : false,
							resizable : true,
							height : "100%",
							width : 450,
							modal : true
						});
						
						$("#dialog-Reenviar").dialog({
							autoOpen : false,
							resizable : false,
							height : "100%",
							width : 400,
							modal : true
						});
						
						// TABS
//						$("#tabs").tabs({ selected: g_tabSeleccionado });
//						$("#tabs").tabs({ show: function(event, ui) {
//								document.getElementById('tabSeleccionado').value = $("#tabs").tabs( "option", "selected" );
//							}
//						});
//						$("#tabs").tabs('load', g_tabSeleccionado);

						// COMBOS
						$("#impacto").combobox();
						$("#impacto").change(function() {
							completaGrupoRSWF(this.value);
						});

						$("#comboSistemasEstPrueba").combobox();
						$("#comboSistemasEstPrueba").change(function() {
						});
						
						$("#comboSistemasDE").combobox();
						$("#comboSistemasDE").change(function() {
						});
						
						$("#comboJustificacionDE").combobox();
						
						$("#numeroRelease").combobox();
						$("#comboPrioridad").combobox();
						
						$("#comboSoftwareFactories").combobox();
						$("#comboPeriodo").combobox();
						
//						var combosEstrategiaPrueba = $("[name*='comboJustificacionNoPrueba_']");
//						combosEstrategiaPrueba.each (function(index, element) {
//							if ($(element).css('display') != 'none') {
//								$(element).combobox();
//							}
//						});

						// BUTTONS
						$("#showCargaHoras").click(function() {
							$("#dialog-CargaHoras").dialog("open");
							return false;
						});
						
						$("#addOtrosCostos").click(function() {
							abrirPopUpOtroCosto();
							return false;
						});
						
						$("#addCargaTareas").click(function() {
							abrirPopUpCargaTarea();
							return false;
						});
						
						//Cuando abre el popup no muestro los mensajes 
						//de error que quedaron del close del popup (si lo hizo) 
						$("#dialog-CargaTareas").dialog({
							open: function(event, ui) {
								$("#mensajes").hide();
								$("#errores").hide();
								limpiarPopUpCargaTareaCompleto();
							}
						});
						
						$("#nuevaPrueba").click(function() {
							abrirPopUpOtrasPruebas();
							//$("#dialog-NuevaPrueba").dialog("open");
							return false;
						});			
						
						$("#btnCerrarOtrosCostos").click(function() {
							$("#dialog-OtrosCostos").dialog("close");
							return false;
						});	

						// COLLAPSABLE
						animatedcollapse.ontoggle=function($, divobj, state){ myOnToggle(divobj, state); };
						animatedcollapse.init();
						
						// DATEPICKERS
						$("#fechaDesdeDatepicker-rpid").datepicker();
						$("#fechaDesdeDatepicker-rpid" ).datepicker( "option", "dateFormat", "dd/mm/yy");
						if (document.getElementById("fechaDesdeHidden"))
							$("#fechaDesdeDatepicker-rpid").val(document.getElementById("fechaDesdeHidden").value);
						
						$("#fechaHastaDatepicker-rpid").datepicker();
						$("#fechaHastaDatepicker-rpid" ).datepicker( "option", "dateFormat", "dd/mm/yy");
						if (document.getElementById("fechaHastaHidden"))
							$("#fechaHastaDatepicker-rpid").val(document.getElementById("fechaHastaHidden").value);
						
						// Obtengo todos los objetos cuyo class sea datePicker
						$("#fechaRelease").datepicker({
							minDate : +1
						});
						$( "#fechaRelease" ).datepicker( "option", "dateFormat", "dd/mm/yy");
						if (document.getElementById("ffechaRelease"))
							$("#fechaRelease").val(document.getElementById("ffechaRelease").value);
						
						$("#fechaVencimientoPlanificacion-rpid").datepicker({
							minDate : +1
						});
						$("#fechaVencimientoPlanificacion-rpid" ).datepicker( "option", "dateFormat", "dd/mm/yy");
						if (document.getElementById("ffechaVencimientoPlanificacion"))
							$("#fechaVencimientoPlanificacion-rpid").val(document.getElementById("ffechaVencimientoPlanificacion").value);
						
					});
					
					// combobox con autocomplete
					(function($) {
						$
								.widget(
										"ui.combobox",
										{
											_create : function() {
												var self = this, select = this.element
														.hide(), selected = select
														.children(":selected"), value = selected
														.val() ? selected
														.text() : "";
												var input = this.input = $(
														"<input>")
														.insertAfter(select)
														.attr(
																'id',
																$(select).attr(
																		'id')
																		+ '-input')
														.val(value)
														.autocomplete(
																{
																	delay : 0,
																	minLength : 0,
																	source : function(
																			request,
																			response) {
																		var matcher = new RegExp(
																				$.ui.autocomplete
																						.escapeRegex(request.term),
																				"i");
																		response(select
																				.children(
																						"option")
																				.map(
																						function() {
																							var text = $(
																									this)
																									.text();
																							if (this.value
																									&& (!request.term || matcher
																											.test(text)))
																								return {
																									label : text
																											.replace(
																													new RegExp(
																															"(?![^&;]+;)(?!<[^<>]*)("
																																	+ $.ui.autocomplete
																																			.escapeRegex(request.term)
																																	+ ")(?![^<>]*>)(?![^&;]+;)",
																															"gi"),
																													"<strong>$1</strong>"),
																									value : text,
																									option : this
																								};
																						}));
																	},
																	select : function(
																			event,
																			ui) {
																		ui.item.option.selected = true;
																		self
																				._trigger(
																						"selected",
																						event,
																						{
																							item : ui.item.option
																						});
																		select
																				.trigger("change");
																	},
																	change : function(
																			event,
																			ui) {
																		if (!ui.item) {
																			var matcher = new RegExp(
																					"^"
																							+ $.ui.autocomplete
																									.escapeRegex($(
																											this)
																											.val())
																							+ "$",
																					"i"), valid = false;
																			select
																					.children(
																							"option")
																					.each(
																							function() {
																								if ($(
																										this)
																										.text()
																										.match(
																												matcher)) {
																									this.selected = valid = true;
																									return false;
																								}
																							});
																			if (!valid) {
																				// remove
																				// invalid
																				// value,
																				// as
																				// it
																				// didn't
																				// match
																				// anything
																				$(
																						this)
																						.val(
																								"");
																				select
																						.val("");
																				input
																						.data("autocomplete").term = "";
																				return false;
																			}
																		}
																	},
																	maxItemsToShow : 5
																})
														.addClass(
																"ui-widget ui-widget-content ui-corner-left");

												input.data("autocomplete")._renderItem = function(
														ul, item) {
													return $("<li></li>")
															.data(
																	"item.autocomplete",
																	item)
															.append(
																	"<a>"
																			+ item.label
																			+ "</a>")
															.appendTo(ul);
												};

												this.button = $(
														"<button type='button'>&nbsp;</button>")
														.attr("tabIndex", -1)
														.attr("title",
																"Show All Items")
														.insertAfter(input)
														.button(
																{
																	icons : {
																		primary : "ui-icon-triangle-1-s"
																	},
																	text : false
																})
														.removeClass(
																"ui-corner-all")
														.addClass(
																"ui-corner-right ui-button-icon")
														.click(
																function() {
																	// close if
																	// already
																	// visible
																	if (input
																			.autocomplete(
																					"widget")
																			.is(
																					":visible")) {
																		input
																				.autocomplete("close");
																		return;
																	}

																	// work
																	// around a
																	// bug
																	// (likely
																	// same
																	// cause as
																	// #5265)
																	$(this)
																			.blur();

																	// pass
																	// empty
																	// string as
																	// value to
																	// search
																	// for,
																	// displaying
																	// all
																	// results
																	input
																			.autocomplete(
																					"search",
																					"");
																	input
																			.focus();
																});
											},

											destroy : function() {
												this.input.remove();
												this.button.remove();
												this.element.show();
												$.Widget.prototype.destroy
														.call(this);
											}
										});
					})(jQuery);

					// fin combobox
				});

function completaGrupoRSWF(idPedidoHijo) {
//	new Ajax.Updater(grupoRSWF, '/' + gNameApp + '/consolidacionImpacto/completaGrupoRSWF',
//			{
//				asynchronous : true,
//				evalScripts : true,
//				parameters : {
//					pedidoHijoId : idPedidoHijo
//				}
//			});
	$('pedidoHijoId').value = idPedidoHijo;
	$('refreshView').click();
}

//function cargarHorasSistemaPopup(faseId, sistema, idDetalle, idPlanificacion,impacto,pedidoId) {
//	iteroBotonesFecha(idDetalle);
	
//	return false;
//}

function grabadoHorasCompletasSuccess(faseId, sistema, idDetalle, idPlanificacion,impacto,pedidoId, aprobacionId){
	$('showCargaHoras').click();//abrir popup
	$('detalleId').value = idDetalle;
	
	ajaxCall({
		container: bodypopupcargahoras,
		controller:'consolidacionImpacto',
		action:'popCargaHorasSistema',
		parameters : {
			sistema : sistema,
			fechaDesde : $('desde_'+idDetalle).value,
			fechaHasta :  $('hasta_'+idDetalle).value,
			idDetalle : idDetalle,
			idPlanificacion : idPlanificacion,
			impacto:impacto,
			pedidoId:pedidoId,
			pedidoATrabajar:pedidoId,
			show: false,
			aprobacionId: aprobacionId
		}, onComplete : function (e){
			asignarValorHoraDefault(impacto);
		}
	});

}

function myOnToggle(divobj, state) {
	if (divobj) {
		var objId = divobj.id;
		objId = objId.replace('content', 'indicator');
		
		if (state == 'none') {
			//$(objId).innerHTML = '[+]';
			$(objId).src='../../images/RightTriangleIcon.gif';
		} else  {
			//$(objId).innerHTML = '[-]';		
			$(objId).src='../../images/DownTriangleIcon.gif';
		}
	}
}

function toggleDivs(id){
	animatedcollapse.toggle(id);
}

function validaFechasCompletas(idDetalle){
	if($('desde_'+idDetalle).value != '' && $('hasta_'+idDetalle).value != ''){
		$('agregaHoras_'+idDetalle).show();
		$('horas_'+idDetalle).show();
	}else{
		$('horas_'+idDetalle).hide();
		$('agregaHoras_'+idDetalle).hide();
	}
}

function recargaOtrosCostos(pedidoId, impacto){
	ajaxCall({
		container: otrosCostos,
		controller:'consolidacionImpacto',
		action:'recargaOtrosCostos',
		parameters : {
			pedidoId : pedidoId,
			impacto : impacto
		}
	});

//	new Ajax.Updater(otrosCostos, '/' + gNameApp + '/consolidacionImpacto/recargaOtrosCostos',
//	{
//		asynchronous : true,
//		evalScripts : true,
//		parameters : {
//			pedidoId : pedidoId,
//			impacto : impacto
//		}
//	});
}

function calculaTotalCargaHoraPop(){
//	var cantidad = parseInt($('cantidadHoras').value);
//	var valor = parseInt($('valorHora').value.replace(',', '.'));
//	
//	if (isNaN(cantidad) || isNaN(valor)) {
//		$('labelTotal').innerHTML = "";
//	} else {
//		//Cambie para que muestre decimales
//		//$('labelTotal').innerHTML = cantidad * valor;
//		$('labelTotal').innerHTML = setearDecimales(cantidad * valor);
//	}
	
	calcularTotal('formCargaHoras' , 'cantidadHoras', 'valorHora', 'labelTotal');
}


function calcularTotal(form, cantidad, unitario, total){
	var form = $(form);
	var oCantidadHora = form[cantidad];
	var oValorHora = $('valorHora');//form[unitario];

	var valorCantidadHora = oCantidadHora.value;
	var valorValorHora = oValorHora.value.replace(',', '.');
	var valorTOTAL = "0";
	
	if (isInteger(valorCantidadHora)){
		if (valorCantidadHora%1===0){}
		valorValorTOTAL = eval(valorCantidadHora * valorValorHora);
		if (!isNaN(valorValorTOTAL)){
			$(total).innerHTML = setearDecimales(valorValorTOTAL);
		} 
	}else{
		oCantidadHora.value = "";
		$(total).innerHTML = "";
		alert("Debe ingresar una cantidad num\u00E9rica");
	}
	if ($(total).innerHTML == "") {
		$(total).hide();
	} else {
		$(total).show();
	}
}

function calcularTotalCargaTareas(valor){
//	var form = $('cargaTarea');
//	var oCantidadHora = form['cantidadHoras'];
//	var oValorHora = form['valorHora'];
//
//	var valorCantidadHora = oCantidadHora.value;
//	var valorValorHora = oValorHora.value.replace(',', '.');
//	var valorTOTAL = "0";
//	
//	if (isInteger(valorCantidadHora)){
//		if (valorCantidadHora%1===0){}
//		valorValorTOTAL = eval(valorCantidadHora * valorValorHora);
//		if (!isNaN(valorValorTOTAL)){
//			//$('totalCargaHora').innerHTML = valorValorTOTAL.toFixed(getCantidadDecimales());
//			$('totalCargaHora').innerHTML = setearDecimales(valorValorTOTAL);
//		}
//	}else{
//		oCantidadHora.value = "";
//		$('totalCargaHora').innerHTML = "";
//		alert("Debe ingresar una cantidad numérica");
//	}
	
	calcularTotal('cargaTarea' , 'cantidadHoras', 'valorHora', 'totalCargaHora');
}

function setValorHora(valor){
	$('valorHora').value = valor;
}

function setCantidadHora(valor){
	$('cantidadHoras').value = valor;
}

function asignarValorHoraDefault(impacto) {
	
	var grupo = 'null';
	if($('comboSoftwareFactories')){
		grupo = $('comboSoftwareFactories').value;
	}
	
	var periodo = $('comboPeriodo').value;
		if(grupo=='null'){
			grupo = null;
		}
		if(periodo=='null'){
			periodo=null;
		}
		
		ajaxCall({
			container: 'valorDeHora',
			controller:'consolidacionImpacto',
			action:'obtenerHorasDefault',
			parameters : {
				idSWF : grupo,
				idPeriodo : periodo,
				impacto:impacto
			}, onComplete: function (e){
				calculaTotalCargaHoraPop();
			}
		});
		
//		new Ajax.Updater('valorDeHora', '/' + gNameApp + '/consolidacionImpacto/obtenerHorasDefault',
//			{
//			asynchronous : true,
//			evalScripts : true,
//			parameters : {
//				idSWF : grupo,
//				idPeriodo : periodo,
//				impacto:impacto
//			}
//		});
	
	
}

function soloNumeros(evt, conComa) {
	  var theEvent = evt || window.event;
	  var key = theEvent.keyCode || theEvent.which;
	  key = String.fromCharCode( key );
	  
	  var regex 
	  if(conComa){
		  regex = /[0-9]|\,/;
	  }else{
		  regex = /[0-9]/;
	  }
		  
	  if( !regex.test(key) ) {
	    theEvent.returnValue = false;
	    if(theEvent.preventDefault) theEvent.preventDefault();
	  }
}

function soloNumerosSinSimbolos(evt) {
	  var theEvent = evt || window.event;
	  var key = theEvent.keyCode || theEvent.which;
	  key = String.fromCharCode( key );
	  var regex = /[0-9]/;
	  if( !regex.test(key) ) {
	    theEvent.returnValue = false;
	    if(theEvent.preventDefault) theEvent.preventDefault();
	  }
}

/*function soloDinero(evt) {
	  var theEvent = evt || window.event;
	  var key = theEvent.keyCode || theEvent.which;
	  key = String.fromCharCode( key );
	  var regex = /[0-9]/;
	  if( !regex.test(key) ) {
	    theEvent.returnValue = false;
	    if(theEvent.preventDefault) theEvent.preventDefault();
	  }
}*/

function cerrarPopUpOtrosCostos(){
	$('btnCerrarOtrosCostos').click();	
}

function actualizaFilaDetallePlanificacionSist(detalleId, idPlanificacion, impacto, pedidoId){
	
	var fila = 'fila_'+detalleId;
	ajaxCall({
		container: fila,
		controller:'consolidacionImpacto',
		action:'actualizaFilaDetallePlanificacionSist',
		parameters : {
			pedidoId : pedidoId,
			detalleId : detalleId,
			idPlanificacion : idPlanificacion,
			impacto: impacto
		}
	});


//		new Ajax.Updater(fila, '/' + gNameApp + '/consolidacionImpacto/actualizaFilaDetallePlanificacionSist',
//				{
//			asynchronous : true,
//			evalScripts : true,
//			parameters : {
//				pedidoId : pedidoId,
//				detalleId : detalleId,
//				idPlanificacion : idPlanificacion,
//				impacto: impacto
//			}
//				});					
}

function setearDecimales(numero){
	var numeroConDecimales = numero.toFixed(g_cantidadDecimales);
	if (numeroConDecimales)
		return numeroConDecimales.replace('.',',');
	else
		return numero
}

function isInteger(numero){
	return (numero%1===0);
}

function cerrarPopUpAgregarCargar(){
	jQuery('#dialog-CargaTareas').dialog("close");
}

function limpiaYCierra(){
//	alert(jQuery('#errores').text());
	if (jQuery('#errores').text()==""){
		limpiarPopUpCargaTarea();
		cerrarPopUpAgregarCargar();
	}
}


function limpiarPopUpCargaTarea(){
	document.getElementById('valorHora').value = document.getElementById('valorDefaultHora').value;
	document.getElementById('cantidadHoras').value = '0';
//	document.getElementById('comboTareaSoporte').selectedIndex = 0;
//	document.getElementById('fechaDesde').value = '';
//	document.getElementById('fechaHasta').value = '';
//	document.getElementById('totalHoras').value = '';
//	document.getElementById('valorHora').value = document.getElementById('valorDefaultHora').value;
//	document.getElementById('cantidadHoras').value = '0';
//	document.getElementById('comentarioDetalleAreaSoporte').value = '';
}

function limpiarPopUpCargaTareaCompleto(){
	document.getElementById('comboTareaSoporte').selectedIndex = 0;
	document.getElementById('fechaDesde').value = '';
	document.getElementById('fechaHasta').value = '';
	document.getElementById('totalCargaHora').value = '';
	document.getElementById('valorHora').value = document.getElementById('valorDefaultHora').value;
	document.getElementById('cantidadHoras').value = '0';
	document.getElementById('comentarioDetalleAreaSoporte').value = '';
}

function showDatePicker(name, detalleId, esDeSistema) {
	if(name == 'hasta') {
		var fecha = document.getElementById('desde_'+detalleId).value;
		var dt1  = fecha.substring(0,2); 
        var mon1 = fecha.substring(3,5); 
        var yr1  = fecha.substring(6,10);  
        
		var tiempo = Date.parse(mon1 + "/" + dt1 + "/" + yr1);
		jQuery('#'+name+'_'+detalleId).datepicker("destroy");
	
		jQuery('#'+name+'_'+detalleId).datepicker({
		    dateFormat: 'dd/mm/yy',
		    //minDate: new Date(document.getElementById('desde_'+detalleId).value),
		    minDate: new Date(tiempo),
			showAnim: ''
		});
		
	} else {
		var desde;
		if(esDeSistema){
			desde = null;
		}else{
			desde = new Date();
		}
		jQuery('#hasta_'+detalleId).datepicker("destroy");
		
		jQuery('#'+name+'_'+detalleId).datepicker({
		    dateFormat: 'dd/mm/yy',
		    minDate: desde,
		    showAnim: ''
		});
	}
	jQuery('#'+name+'_'+detalleId).datepicker("show");
	
}

function showDatePickerTarea(id) {
	jQuery('#'+id).datepicker();
	jQuery('#'+id).datepicker({
	    dateFormat: 'dd/mm/yy',
	    showAnim: ''
	});
	
}

function cambiarJustificacion(idDetalle) {
	
	if ($('chkRealiza_' + idDetalle).checked) {
		if ($('divComboJustificacionNoPrueba_' + idDetalle)){
			$('divComboJustificacionNoPrueba_' + idDetalle).hide();
		}
		if ($('chkConsolida_' + idDetalle)){
			$('chkConsolida_' + idDetalle).show();
		}
	} else {
		if ($('divComboJustificacionNoPrueba_' + idDetalle)){
			$('divComboJustificacionNoPrueba_' + idDetalle).show();
		}
		if ($('chkConsolida_' + idDetalle)){
			$('chkConsolida_' + idDetalle).checked = false;
			$('chkConsolida_' + idDetalle).hide();
		}
	}
}

function recargaTablaOtrasPruebas(estrategiaPruebaId, impacto, pedidoId){
	ajaxCall({
		container: tiposDePrueba,
		controller:'consolidacionImpacto',
		action:'recargaTablaOtrasPruebas',
		parameters : {
			estrategiaPruebaId : estrategiaPruebaId,
			impacto : impacto,
			pedidoId : pedidoId
		}
	});
	
//	new Ajax.Updater(tiposDePrueba, '/' + gNameApp + '/consolidacionImpacto/recargaTablaOtrasPruebas',
//			{
//		asynchronous : true,
//		evalScripts : true,
//		parameters : {
//			estrategiaPruebaId : estrategiaPruebaId,
//			impacto : impacto,
//			pedidoId : pedidoId
//		}
//	});		
}

function recargarTabla(impacto, grupo){
	ajaxCall({
		container: 'tareas',
		controller:'consolidacionImpacto',
		action:'recargaTablaTareas',
		parameters : {
			impacto: impacto, 
			grupo: grupo,
			pedidoId: $('pedidoId').value
		}
	});

	return false;
}

function cargarHorasSistemaPopup(faseId, sistema, idDetalle, idPlanificacion, impacto, pedidoId, aprobacionId) {
	var miArray = $$('.agregaHoras');
	var url = '/' + gNameApp + '/consolidacionImpacto/grabaFechasDetalles';
	var idBtn = "";
	var idSplit = "";
	var fDesde  = "";
	var fHasta = "";
	var display;
	var detalles = "";
	
	miArray.each (function(el, idx) {

		display = el.style.display;

		if(display == '') {
			idBtn = el.id;
			idSplit = idBtn.split('_');
			
			detalles += idSplit[1] + ",";
			fDesde += document.getElementById('desde_' + idSplit[1]).value + ",";
			fHasta += document.getElementById('hasta_' + idSplit[1]).value + ",";
		}
	});

	if (miArray.length){
		ajaxCall({
			container: 'token',
			controller:'consolidacionImpacto',
			action:'grabaFechasDetalles',
			parameters : {
				detalles: detalles,
				fechasDesde: fDesde,
				fechasHasta: fHasta,
				impacto: impacto,
				pedidoId: pedidoId,
				aprobacionId: aprobacionId
			}, onComplete: function (e){
				grabadoHorasCompletasSuccess(faseId, sistema, idDetalle, idPlanificacion,impacto,pedidoId, aprobacionId);
			}
		});
	}
//	if(!error){
		
	//}
}

function mostrarHorasSistemaPopup(faseId, sistema, fechaDesde, fechaHasta, idDetalle, idPlanificacion,impacto,pedidoId){
	//$('showCargaHoras').click();//abrir popup
	$('detalleId').value = idDetalle;
	
	ajaxCall({
		container: 'bodypopupcargahoras',
		controller:'consolidacionImpacto',
		action:'popCargaHorasSistema',
		parameters : {
			sistema : sistema,
			fechaDesde : fechaDesde,
			fechaHasta :  fechaHasta,
			idDetalle : idDetalle,
			idPlanificacion : idPlanificacion,
			impacto: impacto,
			pedidoId: pedidoId,
			pedidoATrabajar: pedidoId,
			show: true
		}, onComplete : function (e){
			jQuery("#dialog-CargaHoras").dialog("open");
			jQuery("#dialog-CargaHoras").dialog("option","height", "100%");
			jQuery("#dialog-CargaHoras").dialog("option","title", "Visualizar detalle");
//			jQuery('#bodypopupcargahoras').dialog("option","width", "700px");
		}
	});

}

function cerrarPopUpCargaOtrasPruebas(){
	jQuery('#dialog-NuevaPrueba').dialog("close");
}

function cerrarPopUpCargaHoras(){
	jQuery('#dialog-CargaHoras').dialog("close");
}

function limpiarComboJustificacion(){
	$('comboJustificacionDE').value = null;
	$('comboJustificacionDE-input').value = "";
}

function openPopUpReenviar(pedidoId, impacto, aprobacionId){
	ajaxCall({
		container: 'dialog-Reenviar',
		controller:'consolidacionImpacto',
		action:'mostrarPopUpReenviar',
		parameters : {
			pedidoId : pedidoId,
			impacto : impacto,
			aprobacionId:aprobacionId
		}, onComplete: function (e){
			jQuery('#dialog-Reenviar').dialog("open");
		}
	});
}

function cerrarPopUpReenviar(){
	jQuery('#dialog-Reenviar').dialog("close");
}

function reenviar(formulario){
	if ($('observaciones').value==''){
		$('errores').show();
	}else{
		cerrarPopUpReenviar();
		formulario.submit();
	}
	return false;
}

function recargaCostoTotal(impacto, pedidoId){
	ajaxCall({
		container: 'totalCambio',
		controller:'consolidacionImpacto',
		action:'recargaCostoTotalCambio',
		parameters : {
			pedidoId : pedidoId,
			impacto : impacto
		}
	});
	
}

function abrirPopUpOtroCosto(){
	ajaxCall({
		container: 'dialog-OtrosCostos',
		controller: 'consolidacionImpacto',
		action: 'popOtrosCostos',
		parameters: {
			pedidoId : $( 'pedidoId' ).value,
			impacto: $( 'impacto' ).value
		},
		onComplete: function(event) {
			jQuery("#dialog-OtrosCostos").dialog("open");
			jQuery('#dialog-OtrosCostos').dialog("option","height", "100%");
		}
	});

}

function abrirPopUpOtrasPruebas(){
	ajaxCall({
		asynchronous: false,
		container: 'dialog-NuevaPrueba',
		controller: 'consolidacionImpacto',
		action: 'popCargaOtrasPruebas',
		parameters: {
			pedidoId : $( 'pedidoId' ).value,
			impacto: $( 'impacto' ).value
		},
		onComplete: function(event) {
			jQuery("#dialog-NuevaPrueba").dialog("open");
			jQuery('#dialog-NuevaPrueba').dialog("option","height", "100%");
			guardarPedido();
		}
	});
}

function abrirPopUpCargaTarea(){
	ajaxCall({
		container: 'dialog-CargaTareas',
		controller: 'consolidacionImpacto',
		action: 'popCargaTarea',
		parameters: {
			pedidoId : $( 'pedidoId' ).value,
			impacto: $( 'impacto' ).value
		},
		onComplete: function(event) {
			jQuery("#dialog-CargaTareas").dialog("open");
			jQuery('#dialog-CargaTareas').dialog("option","height", "100%");
		}
	});

}

function guardarPedido(){
	var formulario = Form.serialize(document.getElementById('formPedido'));
	formulario = quitarParametro(formulario, "_action_navegarFase");

	ajaxCallRequest({
		controller: 'consolidacionImpacto', 
		action: 'grabarFormulario', 
		parameters: formulario
	});	
	
}                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                              