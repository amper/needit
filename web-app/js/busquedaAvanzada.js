jQuery(document).ready(
			
	function($) {

	/* Botones y datepickers */
	/*$("input:button").button({
		icons : {
			primary : "ui-icon-close"
		}
	});*/
	$("#datepicker-fechaDesde-rpid").datepicker();
	$("#datepicker-fechaDesde-rpid").datepicker("option","dateFormat", "dd/mm/yy");
	$("#datepicker-fechaDesde-rpid").val(document.getElementById("fechaDesdeHidden").value);

	$("#datepicker-fechaHasta-rpid").datepicker();
	$("#datepicker-fechaHasta-rpid").datepicker("option", "dateFormat", "dd/mm/yy");
	$("#datepicker-fechaHasta-rpid").val(document.getElementById("fechaHastaHidden").value);
	
	$("#comboboxMacroestado").multiselect({
		checkAllText: 'Todos',
		uncheckAllText: 'Ninguno',
		noneSelectedText: 'Seleccione un Macro Estado',
		selectedText: function(numChecked, numTotal, checkedItems){
			obtenerFases('');
			return numChecked + ' de ' + numTotal + ' seleccionados';
		},
		create : function(event, ui) {
			$(this).multiselect("uncheckAll");
			var contentDiv = $(this).multiselect().parent().find("div")[0];
			contentDiv.style.width = 350;
			
			if(macroEstados != null) {
				$(this).multiselect("widget").find(":checkbox").each(function(i, ele) {
					if($.inArray(parseInt(ele.value), macroEstados) != -1) {
						this.click();
					}
				});
			}
			obtenerFases('');
		},
		close: function(event, ui) {
			var macroEstadoSeleccionados = $(this).multiselect("getChecked").map(function(){
			   return this.value;
			}).get();
			//cambioMacroestado(macroEstadoSeleccionados);
		},
		click: function(event, ui) {
			obtenerFases('');
		}
	}).multiselectfilter({
		label: 'Buscar',
		width: 100
	});
	
	$("#comboboxFase").multiselect({
		checkAllText: 'Todos',
		uncheckAllText: 'Ninguno',
		noneSelectedText: 'Seleccione una Fase',
		selectedText: function(numChecked, numTotal, checkedItems){
			return numChecked + ' de ' + numTotal + ' seleccionados';
		},
		create : function(event, ui) {
			$(this).multiselect("uncheckAll");
		},
		beforeopen : function(event, ui) {
			var contentDiv = $(this).multiselect().parent().find("div")[0];
			contentDiv.style.width = 350;
			
			if(fases != null) {
				$(this).multiselect("widget").find(":checkbox").each(function(i, ele) {
					if($.inArray(parseInt(ele.value), fases) != -1) {
						this.click();
					}
				});
			}
		}
	}).multiselectfilter({
		label: 'Buscar',
		width: 80
	});
	
	$("#comboboxTipoImpacto").multiselect({
		checkAllText: 'Todos',
		uncheckAllText: 'Ninguno',
		noneSelectedText: 'Seleccione un Tipo de Impacto',
		selectedText: function(numChecked, numTotal, checkedItems){
			return numChecked + ' de ' + numTotal + ' seleccionados';
		},
		create : function(event, ui) {
			$(this).multiselect("uncheckAll");
			var contentDiv = $(this).multiselect().parent().find("div")[0];
			contentDiv.style.width = 350;
			
			if(tiposImpacto != null) {
				$(this).multiselect("widget").find(":checkbox").each(function(i, ele) {
					if($.inArray(parseInt(ele.value), tiposImpacto) != -1) {
						this.click();
					}
				});
			}
		}
	}).multiselectfilter({
		label: 'Buscar',
		width: 80
	});
	
	$("#comboboxAreaSoporte").multiselect({
		checkAllText: 'Todos',
		uncheckAllText: 'Ninguno',
		noneSelectedText: 'Seleccione Area de Soporte',
		selectedText: function(numChecked, numTotal, checkedItems){
			return numChecked + ' de ' + numTotal + ' seleccionados';
		},
		create : function(event, ui) {
			$(this).multiselect("uncheckAll");
			var contentDiv = $(this).multiselect().parent().find("div")[0];
			contentDiv.style.width = 380;
			
			if(areaSoporte != null) {
				$(this).multiselect("widget").find(":checkbox").each(function(i, ele) {
					if($.inArray(parseInt(ele.value), areaSoporte) != -1) {
						this.click();
					}
				});
			}
		}
	}).multiselectfilter({
		label: 'Buscar',
		width: 80
	});
	
	$("#comboboxRol").multiselect({
		checkAllText: 'Todos',
		uncheckAllText: 'Ninguno',
		noneSelectedText: 'Seleccione un Rol',
		selectedText: function(numChecked, numTotal, checkedItems){
			return numChecked + ' de ' + numTotal + ' seleccionados';
		},
		create : function(event, ui) {
			$(this).multiselect("uncheckAll");
			var contentDiv = $(this).multiselect().parent().find("div")[0];
			contentDiv.style.width = 350;
			
			if(roles != null) {
				$(this).multiselect("widget").find(":checkbox").each(function(i, ele) {
					if($.inArray(ele.value, roles) != -1) {
						this.click();
					}
				});
			}
		}
	}).multiselectfilter({
		label: 'Buscar',
		width: 80
	});
	
	$("#comboboxGrupo").multiselect({
		checkAllText: 'Todos',
		uncheckAllText: 'Ninguno',
		noneSelectedText: 'Seleccione un Grupo',
		selectedText: function(numChecked, numTotal, checkedItems){
			return numChecked + ' de ' + numTotal + ' seleccionados';
		},
		create : function(event, ui) {
			$(this).multiselect("uncheckAll");
			var contentDiv = $(this).multiselect().parent().find("div")[0];
			contentDiv.style.width = 350;
			
			if(grupos != null) {
				$(this).multiselect("widget").find(":checkbox").each(function(i, ele) {
					if($.inArray(ele.value, grupos) != -1) {
						this.click();
					}
				});
			}
		},click: function(event, ui){
			var checkedValues = $(this).multiselect("getChecked").map(function(){
				   return this.value;	
			}).get();
			if(checkedValues.length > 10) {
				event.preventDefault();
				alert("No puede seleccionar m\u00E1s de 10 grupos");
			}
		}
	}).multiselectfilter({
		label: 'Buscar',
		width: 80
	});
	
	$("#comboboxEstado").multiselect({
		checkAllText: 'Todos',
		uncheckAllText: 'Ninguno',
		noneSelectedText: 'Seleccione un Estado',
		selectedText: function(numChecked, numTotal, checkedItems){
			return numChecked + ' de ' + numTotal + ' seleccionados';
		},
		create : function(event, ui) {
			$(this).multiselect("uncheckAll");
			var contentDiv = $(this).multiselect().parent().find("div")[0];
			contentDiv.style.width = 350;
			
			if(estados != null) {
				$(this).multiselect("widget").find(":checkbox").each(function(i, ele) {
					if($.inArray(ele.value, estados) != -1) {
						this.click();
					}
				});
			}
		}
	}).multiselectfilter({
		label: 'Buscar',
		width: 80
	});
	
	$(function() {
		$("#comboboxTipoBusqueda").combobox();
		$("#comboboxTipoGestion").combobox();
		$("#comboboxTipoReferencia").combobox();
		$("#comboboxOpcionesTipoReferencia").combobox();
		$("#comboboxOpcionesSistemasImpactados").combobox();
		$("#comboboxOpcionesSistemasSugeridos").combobox();
		$("#comboboxOpcionesPrioridad").combobox();
		$("#comboboxOpcionesSolman").combobox();
		$("#comboboxOpcionesRelease").combobox();
		$("#comboboxOpcionesSimplitPaP").combobox();
		$("#comboboxOpcionesSimplitPAU").combobox();
		
		$("#dialog-popupListadoVariantes").dialog({
			autoOpen : false,
			resizable : false,
			height : 530,
			width : 300,
			modal : true
		});
		
		$("#dialog-popupGuardarVariantes").dialog({
			autoOpen : false,
			resizable : false,
			height : "100%",
			width : 400,
			modal : true
		});
		reiniciarBotonera();
	});
	/* combobox con autocomplete */

	(function($) {
		$
		.widget(
				"ui.combobox",
				{
					_create : function() {
						var self = this, select = this.element
								.hide(), selected = select
								.children(":selected"), value = selected
								.val() ? selected
								.text() : "";
						var input = this.input = $(
								"<input>")
								.insertAfter(select)
								.attr(
										'id',
										$(select).attr(
												'id')
												+ '-input')
								.val(value)
								.autocomplete(
										{
											delay : 0,
											minLength : 0,
											source : function(
													request,
													response) {
												var matcher = new RegExp(
														$.ui.autocomplete
																.escapeRegex(request.term),
														"i");
												response(select
														.children(
																"option")
														.map(
																function() {
																	var text = $(
																			this)
																			.text();
																	if (this.value
																			&& (!request.term || matcher
																					.test(text)))
																		return {
																			label : text
																					.replace(
																							new RegExp(
																									"(?![^&;]+;)(?!<[^<>]*)("
																											+ $.ui.autocomplete
																													.escapeRegex(request.term)
																											+ ")(?![^<>]*>)(?![^&;]+;)",
																									"gi"),
																							"<strong>$1</strong>"),
																			value : text,
																			option : this
																		};
																}));
											},
											select : function(
													event,
													ui) {
												ui.item.option.selected = true;
												self
														._trigger(
																"selected",
																event,
																{
																	item : ui.item.option
																});
												select
														.trigger("change");
											},
											change : function(
													event,
													ui) {
												if (!ui.item) {
													var matcher = new RegExp(
															"^"
																	+ $.ui.autocomplete
																			.escapeRegex($(
																					this)
																					.val())
																	+ "$",
															"i"), valid = false;
													select
															.children(
																	"option")
															.each(
																	function() {
																		if ($(
																				this)
																				.text()
																				.match(
																						matcher)) {
																			this.selected = valid = true;
																			return false;
																		}
																	});
													if (!valid) {
														// remove
														// invalid
														// value,
														// as
														// it
														// didn't
														// match
														// anything
														$(
																this)
																.val(
																		"");
														select
																.val("");
														input
																.data("autocomplete").term = "";
														return false;
													}
												}
											}
										})
								.addClass(
										"ui-widget ui-widget-content ui-corner-left");

						input.data("autocomplete")._renderItem = function(
								ul, item) {
							return $("<li></li>")
									.data(
											"item.autocomplete",
											item)
									.append(
											"<a>"
													+ item.label
													+ "</a>")
									.appendTo(ul);
						};

						this.button = $(
								"<button type='button'>&nbsp;</button>")
								.attr("tabIndex", -1)
								.attr("title",
										"Show All Items")
								.insertAfter(input)
								.button(
										{
											icons : {
												primary : "ui-icon-triangle-1-s"
											},
											text : false
										})
								.removeClass(
										"ui-corner-all")
								.addClass(
										"ui-corner-right ui-button-icon")
								.click(
										function() {
											// close if
											// already
											// visible
											if (input
													.autocomplete(
															"widget")
													.is(
															":visible")) {
												input
														.autocomplete("close");
												return;
											}

											// work
											// around a
											// bug
											// (likely
											// same
											// cause as
											// #5265)
											$(this)
													.blur();

											// pass
											// empty
											// string as
											// value to
											// search
											// for,
											// displaying
											// all
											// results
											input
													.autocomplete(
															"search",
															"");
											input
													.focus();
										});
					},

					destroy : function() {
						this.input.remove();
						this.button.remove();
						this.element.show();
						$.Widget.prototype.destroy
								.call(this);
					}
				});
	})(jQuery);

	/* fin combobox */
});

function mostrarDivs(valor){
	$('table-busqueda-pedidos-fila1').show();
	if (valor === 'S' || valor === '') {
		$('table-busqueda-pedidos-fila2').hide();
	}else{
		$('comboboxTipoBusqueda').selectedIndex = 1; //Para que aparezca seleccionado el combo en "Avanzada"
		$('table-busqueda-pedidos-fila2').show();
	}
}

function mostrarDivGuardarVariante(boolMostrar){
	if (boolMostrar) {
		$('divGuardarVariante').show();
	}else{
		$('divGuardarVariante').hide();
	}
}

function obtenerFases(params) {
	
	//alert(jQuery("#comboboxMacroestado").multiselect("getChecked").length);
	//NICO
	//Encontre este documento sobre multiselect que parece muy bueno: http://www.erichynds.com/jquery/jquery-ui-multiselect-widget/
	var parametro = "";
	var array_of_checked_values = jQuery("#comboboxMacroestado").multiselect("getChecked").map(function(){
		parametro = parametro + "macroEstadoID=" + this.value + "&"
	   return this.value;	
	}).get();

	//alert(jQuery('#comboboxMacroestado').val());
	//alert(jQuery("#comboboxMacroestado option:selected").val());
	//alert(jQuery("#comboboxMacroestado option:selected").size());
	
	//alert(array_of_checked_values);
	
	ajaxCall({
		container: 'comboboxFaseBusqueda',
		controller: 'busqueda',
		action:'obtenerFases',
		parameters : parametro,
		onComplete : function(event) {
			reiniciarBotonera();
		}
	});
	
	return false;
}

function guardarVariante(){
	var nombre = $('nombreVariante').value;
	var formulario = Form.serialize(document.getElementById('formBusqueda'));
	formulario = formulario + '&nombreVariante=' + nombre;
	ajaxCall({
		container: 'dialog-popupGuardarVariantes',
		controller: 'busqueda',
		action:'guardarVariante',
		parameters: formulario,
		onComplete : function(event) {
			closePopUp('#dialog-popupGuardarVariantes');
			$('message').innerHTML = "Variante con el nombre '" + nombre + "' guardada correctamente";
			$('message').show();
			reiniciarBotonera();
		}
	});
	
}

function cerrarPopUp(variante){
	jQuery('#dialog-popupListadoVariantes').dialog("close");
	
	ajaxCallRequest({
		controller: 'busqueda',
		action:'buscarPedidos',
		parameters : {
			id: variante
		}
	});
}

function closePopUp(id){
	jQuery(id).dialog("close");
}

function showPopUpVariantes(legajo){
	ajaxCall({
		container: "dialog-popupListadoVariantes",
		controller: 'busqueda',
		action:'obtenerVariantes',
		parameters : {
			legajo: legajo
		},
		onComplete : function(event) {
			jQuery("#dialog-popupListadoVariantes").dialog("open");
			jQuery("#dialog-popupListadoVariantes").dialog("option", "height", "100%");
			reiniciarBotonera();
		}
	});
}

function showPopUpNuevaVariantes(){
	ajaxCall({
		container: "dialog-popupGuardarVariantes",
		controller: 'busqueda',
		action:'popUpNuevaVariantes',
		parameters : Form.serialize(document.getElementById('formBusqueda')),
		onComplete : function(event) {
			jQuery("#dialog-popupGuardarVariantes").dialog("open");
			jQuery("#dialog-popupGuardarVariantes").dialog("option", "height", "auto");
			reiniciarBotonera();
		}
	});
}

function resizeListadoVariantes(){
	jQuery("#dialog-popupListadoVariantes").dialog("open");
	jQuery("#dialog-popupListadoVariantes").dialog("option", "height", "auto");
}

function validarFormulario(){
	document.formBusqueda.submit();
}

function cargarVariante(varianteId){
	$('varianteId').value = varianteId;
	document.formListadoVariante.submit();
}

function limpiarFormulario(){

	document.getElementById('titulo').value = '';
	document.getElementById('comboboxTipoGestion').value = '';
	document.getElementById('comboboxTipoGestion-input').value = '';
	document.getElementById('comboboxOpcionesSistemasImpactados').value = '';
	document.getElementById('comboboxOpcionesSistemasImpactados-input').value = '';
	document.getElementById('valorSistemaImpactado').value = '';
	document.getElementById('estructuraRequirente').value = '';
	document.getElementById('comboboxTipoReferencia').value = '';
	document.getElementById('comboboxTipoReferencia-input').value = '';
	document.getElementById('comboboxOpcionesTipoReferencia').value = '';
	document.getElementById('comboboxOpcionesTipoReferencia-input').value = '';
	document.getElementById('valorTipoReferencia').value = '';
	document.getElementById('comboboxOpcionesSolman').value = '';
	document.getElementById('comboboxOpcionesSolman-input').value = '';
	document.getElementById('valorSolman').value = '';
	document.getElementById('comboboxOpcionesRelease').value = '';
	document.getElementById('comboboxOpcionesRelease-input').value = '';
	document.getElementById('valorRelease').value = '';
	document.getElementById('comboboxOpcionesSimplitPaP').value = '';
	document.getElementById('comboboxOpcionesSimplitPaP-input').value = '';
	document.getElementById('valorSimplitPaP').value = '';
	document.getElementById('comboboxOpcionesSimplitPAU').value = '';
	document.getElementById('comboboxOpcionesSimplitPAU-input').value = '';
	document.getElementById('valorSimplitPAU').value = '';
	document.getElementById('comboboxOpcionesSistemasSugeridos').value = '';
	document.getElementById('comboboxOpcionesSistemasSugeridos-input').value = '';
	document.getElementById('valorSistemaSugerido').value = '';
	document.getElementById('comboboxOpcionesPrioridad').value = '';
	document.getElementById('comboboxOpcionesPrioridad-input').value = '';
	document.getElementById('valorPrioridad').value = '';
	
	document.getElementById('datepicker-fechaDesde-rpid').value = '';
	document.getElementById('datepicker-fechaHasta-rpid').value = '';
	
    jQuery("#comboboxMacroestado").multiselect("uncheckAll");
    jQuery("#comboboxFase").multiselect("uncheckAll");
    jQuery("#comboboxTipoImpacto").multiselect("uncheckAll");
    jQuery("#comboboxAreaSoporte").multiselect("uncheckAll");
    jQuery("#comboboxRol").multiselect("uncheckAll");
    jQuery("#comboboxGrupo").multiselect("uncheckAll");
    jQuery("#comboboxEstado").multiselect("uncheckAll");

}

function mostrarValor(){
	//alert($("org.codehaus.groovy.grails.SYNCHRONIZER_TOKEN").value);
}

function reiniciarBotonera(){
	//jQuery(document).ready(function() {
	/*limpiarLink();

	var token = $("org.codehaus.groovy.grails.SYNCHRONIZER_TOKEN").value;
	alert($$('.nextLink'));
	alert($$('.step'));
	var linkObjs = $$('.nextLink', '.step', '.prevLink');
	if(linkObjs){
		linkObjs.each(function(ele, idx){
			if ($(ele).href && $(ele).href!=''){
				$(ele).href = $(ele).href + '&org.codehaus.groovy.grails.SYNCHRONIZER_TOKEN=' + token;
			}
		});
	}*/
//	var steps = jQuery(".paginateButtons .step");
//	jQuery.each(steps, function(i, ele) {
//		var stepAttr = jQuery(ele).attr('href');
//		if ($(ele).href != ''){
//			jQuery(ele).attr('href', stepAttr + '&org.codehaus.groovy.grails.SYNCHRONIZER_TOKEN=' + token);
//		}
//	});
//	
//	var prevLinkAttr = jQuery(".paginateButtons .prevLink").attr('href');
//	jQuery(".paginateButtons .prevLink").attr('href', prevLinkAttr + '&org.codehaus.groovy.grails.SYNCHRONIZER_TOKEN=' + token);
//	var nextLinkAttr = jQuery(".paginateButtons .nextLink").attr('href');
//	jQuery(".paginateButtons .nextLink").attr('href', nextLinkAttr + '&org.codehaus.groovy.grails.SYNCHRONIZER_TOKEN=' + token);
	//});
	limpiarLink();
	var token = $("org.codehaus.groovy.grails.SYNCHRONIZER_TOKEN").value;
	var tags = jQuery(".sortable a");
	
	jQuery.each(tags, function(i, ele) {
		var stepAttr = jQuery(ele).attr('href');
		
		if(stepAttr && (stepAttr.indexOf("SYNCHRONIZER_TOKEN") ==-1)){
			jQuery(ele).attr('href', stepAttr + '&org.codehaus.groovy.grails.SYNCHRONIZER_TOKEN=' + token);
		}else{
			if(stepAttr){
				jQuery(ele).attr('href', stepAttr.substring(0, stepAttr.lastIndexOf("SYNCHRONIZER_TOKEN="))+ "SYNCHRONIZER_TOKEN="+token );
			}
		}
	});
	
	var steps = jQuery(".paginateButtons .step");
	jQuery.each(steps, function(i, ele) {
		var stepAttr = jQuery(ele).attr('href');
		jQuery(ele).attr('href', stepAttr + '&org.codehaus.groovy.grails.SYNCHRONIZER_TOKEN=' + token);
	});
	
	var prevLinkAttr = jQuery(".paginateButtons .prevLink").attr('href');
	jQuery(".paginateButtons .prevLink").attr('href', prevLinkAttr + '&org.codehaus.groovy.grails.SYNCHRONIZER_TOKEN=' + token);
	var nextLinkAttr = jQuery(".paginateButtons .nextLink").attr('href');
	jQuery(".paginateButtons .nextLink").attr('href', nextLinkAttr + '&org.codehaus.groovy.grails.SYNCHRONIZER_TOKEN=' + token);

}

function limpiarLink(){
	var linkObjs = $$('.nextLink', '.step', '.prevLink');
	if (linkObjs){
		linkObjs.each(function(ele, idx){
			
			if ($(ele).href && $(ele).href!=''){
				if ($(ele).href.indexOf(gTokenKey)!=-1){
					var href = '';
					var spliteado = $(ele).href.split("&");
					for (var i = 0; i < spliteado.size(); i++){
						if (spliteado[i].indexOf(gTokenKey, 0)==-1){
							href += spliteado[i] + "&";
						}
					}
					$(ele).href = href;
				}
			}
		});
	}
}                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                       