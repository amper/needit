jQuery(document).ready(
		function($) {
		$(function() {
			$("#dialog-SuspenderPedido").dialog({
				autoOpen : false,
				resizable : false,
				height : "100%",
				width : 650,
				modal : true,
				closeOnEscape: false,
				open: function(event, ui) {
					  $(this).closest('.ui-dialog').find('.ui-dialog-titlebar-close').hide();
					}
			});
			
			
			
			//ComboBox
			(function( $ ) {
				$.widget( "ui.combobox", {
					_create: function() {
						var self = this,
							select = this.element.hide(),
							selected = select.children( ":selected" ),
							value = selected.val() ? selected.text() : "";
						var input = this.input = $( "<input>" )
							.insertAfter( select )
							.attr( 'id', $(select).attr( 'id' )+'-input' )
							.val( value )
							.autocomplete({
								delay: 0,
								minLength: 0,
								source: function( request, response ) {
									var matcher = new RegExp( $.ui.autocomplete.escapeRegex(request.term), "i" );
									response( select.children( "option" ).map(function() {
										var text = $( this ).text();
										if ( this.value && ( !request.term || matcher.test(text) ) )
											return {
												label: text.replace(
													new RegExp(
														"(?![^&;]+;)(?!<[^<>]*)(" +
														$.ui.autocomplete.escapeRegex(request.term) +
														")(?![^<>]*>)(?![^&;]+;)", "gi"
													), "<strong>$1</strong>" ),
												value: text,
												option: this
											};
									}) );
								},
								select: function( event, ui ) {
									ui.item.option.selected = true;
									self._trigger( "selected", event, {
										item: ui.item.option
									});
									select.trigger("change");   
								},
								change: function( event, ui ) {
									if ( !ui.item ) {
										var matcher = new RegExp( "^" + $.ui.autocomplete.escapeRegex( $(this).val() ) + "$", "i" ),
											valid = false;
										select.children( "option" ).each(function() {
											if ( $( this ).text().match( matcher ) ) {
												this.selected = valid = true;
												return false;
											}
										});
										if ( !valid ) {
											// remove invalid value, as it didn't match anything
											$( this ).val( "" );
											select.val( "" );
											input.data( "autocomplete" ).term = "";
											return false;
										}
									}
								},
								maxItemsToShow:5
							})
							.addClass( "ui-widget ui-widget-content ui-corner-left" );

						input.data( "autocomplete" )._renderItem = function( ul, item ) {
							return $( "<li></li>" )
								.data( "item.autocomplete", item )
								.append( "<a>" + item.label + "</a>" )
								.appendTo( ul );
						};

						this.button = $( "<button type='button'>&nbsp;</button>" )
							.attr( "tabIndex", -1 )
							.attr( "title", "Show All Items" )
							.insertAfter( input )
							.button({
								icons: {
									primary: "ui-icon-triangle-1-s"
								},
								text: false
							})
							.removeClass( "ui-corner-all" )
							.addClass( "ui-corner-right ui-button-icon" )
							.click(function() {
								// close if already visible
								if ( input.autocomplete( "widget" ).is( ":visible" ) ) {
									input.autocomplete( "close" );
									return;
								}

								// work around a bug (likely same cause as #5265)
								$( this ).blur();

								// pass empty string as value to search for, displaying all results
								input.autocomplete( "search", "" );
								input.focus();
							});
					},

					destroy: function() {
						this.input.remove();
						this.button.remove();
						this.element.show();
						$.Widget.prototype.destroy.call( this );
					}
				});
			})( jQuery );
			
			//fin combobox			
			
			
		});
		
		
		}
);

function popSuspenderPedido(pedidoId){
	ajaxCall({
		container: 'dialog-SuspenderPedido',
		controller: 'suspension',
		action: 'popupSuspender',
		parameters: {
			pedidoId : pedidoId
		},
		onComplete: function(event) {
			jQuery("#dialog-SuspenderPedido").dialog("open");
			jQuery('#dialog-SuspenderPedido').dialog("option","height", "100%");
		}
	});
	return false;
}

function cancelarPopUpSuspension(){
	document.getElementById('suspensionPop').value = false;
	document.getElementById('hAnexoSuspension').value = '';
	
	jQuery('#dialog-SuspenderPedido').dialog("close");
}

function aceptaPopUpConfirmacion(formSer, suspende){
	var action;
	var continuar
 
	if(suspende){
		action = 'suspenderPedido'
		continuar = confirm("El pedido quedar\u00E1 suspendido, solo podrá retomarse mediante el proceso de soporte 'Reanudar Pedido'.¿Desea continuar?");
	}else{
		action = 'reanudarPedido'
		continuar = confirm("El pedido se reanudar\u00E1.¿Desea continuar?");
	}
	
    if (continuar) {
    	//var formSer = Form.serialize(formulario);
    	ajaxCall({
    		container: 'dialog-SuspenderPedido',
    		controller: 'suspension',
    		action: action,
    		parameters: formSer
    	});
    	return false;
    }	
}

function validaPopUpConfirmacion(formulario, suspende){
	if(jQuery('#divErrors')){
		jQuery('#divErrors').text("");
	}
	var formSer = Form.serialize(formulario);
	ajaxCall({
		container: 'dialog-SuspenderPedido',
		controller: 'suspension',
		action: 'validarSuspension',
		parameters: formSer,
		onComplete: function(event) {
			if(jQuery('#divErrors').text()==""){
				aceptaPopUpConfirmacion(formSer, suspende);
			}
		}
	});
	return false;
}
